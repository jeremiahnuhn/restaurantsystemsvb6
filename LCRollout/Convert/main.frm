VERSION 5.00
Begin VB.Form Main 
   Caption         =   "XcelleNet to LiveConnect Conversion"
   ClientHeight    =   7095
   ClientLeft      =   6015
   ClientTop       =   4125
   ClientWidth     =   7380
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   14.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "main.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7095
   ScaleWidth      =   7380
   Begin VB.CommandButton Command2 
      Caption         =   "Process"
      Height          =   3255
      Left            =   4560
      TabIndex        =   3
      Top             =   960
      Width           =   2415
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      Height          =   975
      Left            =   4560
      TabIndex        =   2
      Top             =   4680
      Width           =   2415
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5460
      Left            =   360
      TabIndex        =   0
      Top             =   600
      Width           =   3735
   End
   Begin VB.Label Label2 
      Caption         =   "Version 2"
      Height          =   375
      Left            =   5880
      TabIndex        =   4
      Top             =   6600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   360
      TabIndex        =   1
      Top             =   6240
      Width           =   3255
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
End
End Sub

Private Sub Command2_Click()

Main.MousePointer = vbHourglass
If Dir("\\xnet\vol1\xnet\sysfiles\forms\lc\test.lc") = "" Then
    X = MsgBox("Your computer is not mapped properly to run this program.  You need to be able to access the \\XNET\VOL1 and \\XNET\VOL2.", vbCritical, "Program Error")
    Main.MousePointer = 0
    End
End If

    For I = 0 To List1.ListCount - 1
        If List1.Selected(I) Then
            POLLUNIT = Mid$(List1.List(I), 1, 4)
            pollall = List1.List(I)
            RemoveItem = I
            I = List1.ListCount - 1
        End If
    Next I
    response = MsgBox("You selected restaurant #" + pollall + "." + Chr(10) + Chr(10) + "Do you want to process this restaurant?", vbYesNo, "Process")
    If response = vbNo Then
        Main.MousePointer = 0
        Exit Sub
    End If
    ' Begin Processing
    Open "J:\LCROLLOUT\FECONVERT.CSV" For Append Access Write As 9
        LC = Format$(Now, "MM/DD/YYYY")
        Print #9, Str(LTrim(POLLUNIT)) + "," + LC
    Close 9
    NODE = LTrim(POLLUNIT)
    Open "\\XNET\VOL2\POLLING\NODE\POSITRAN\LC.DST" For Append Access Write As 9
        Print #9, Str(LTrim(POLLUNIT))
    Close 9
    
        List1.RemoveItem (RemoveItem)
        Open "J:\LCROLLOUT\ToConvert.csv" For Output As 1
        For I = 0 To List1.ListCount - 1
          POLLUNIT = Mid$(List1.List(I), 1, 4)
          POLLNAME = Mid$(List1.List(I), 6, 25)
          Print #1, POLLUNIT + "," + LTrim(POLLNAME)
        Next I
        Close 1
    
    FileCopy "\\XNET\VOL1\XNET\NODE\EMAIL2\GROUPS\XMAILGP.GP", "\\XNET\VOL1\XNET\NODE\EMAIL2\GROUPS\XMAILGP.PRE"
    Open "\\XNET\VOL1\XNET\NODE\EMAIL2\GROUPS\XMAILGP.PRE" For Input As 1
    Open "\\XNET\VOL1\XNET\NODE\EMAIL2\GROUPS\XMAILGP.GP" For Output As 2
    Do While Not EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 4, 4) = NODE Or Mid$(linedata, 3, 4) = NODE Then
            X = NODE
        Else
            Print #2, linedata
        End If
    Loop
    Close 1
    Close 2
    
    
    
    FNAME = "\\XNET\VOL1\XNET\ECF\" + NODE + ".ECF"
    'FNAME = "\\XNET\VOL1\XNET\ECF\" + NODE + ".X"
    Open FNAME For Output As 1
    Print #1, "TYPE=NODE"
    Print #1, "NODES=" + NODE
    Print #1, "ACTION=UPDATE"
    Print #1, "STATUS=DISABLE"
    Print #1,
    Print #1, "TYPE=USER_GROUP"
    Print #1, "USER_GROUPS=DIST-1,DIST-2,DIST-4,DIST-5,DIST-6,GM&HO"
    Print #1, "ACTION=UPDATE"
    If NODE = "1647" Or NODE = "1648" Or NODE = "1649" Or NODE = "1650" Or NODE = "1350" Or NODE = "3096" Or NODE = "2997" Or NODE = "2998" Then
        Print #1, "UNASSIGN_USERS=GM" + NODE + ", A1" + NODE + ", A2" + NODE + ", A3" + NODE + ", SC" + NODE
    Else
        Print #1, "UNASSIGN_USERS=GM-" + NODE + ", A1-" + NODE + ", A2-" + NODE + ", A3-" + NODE + ", SC-" + NODE
    End If
    Print #1,
    Close 1
    Main.MousePointer = vbNormal
    X = MsgBox("Restaurant removed from XcelleNet Mail Distributions Groups." + Chr(10) + "Restaurant removed from XcelleNet Mail Groups." + Chr(10) + "Restaurant's XcelleNet Node Disabled." + Chr(10) + "Conversion tracking files updated." + Chr(10) + "Repoll Scheduler files updated." + Chr(10) + Chr(10) + "                          Process complete!", vbInformation, "Complete")
    List1.Clear
    Call Form_Load

End Sub

Private Sub Command3_Click()
End Sub

Private Sub Form_Load()
 Main.MousePointer = vbHourglass

If Dir("J:\LCROLLOUT\TEST.LC") = "" Then
    X = MsgBox("Your computer is not mapped properly to run this program.  You need to be able to access the J:(BNE Share) drive.", vbCritical, "Program Error")
    Main.MousePointer = 0
    End
End If
    Main.MousePointer = 0
    
    
    
    Dim UnitNumber As String, Unitname As String
    Open "J:\LCROLLOUT\ToConvert.csv" For Input As 1
    Do While Not EOF(1)
        Line Input #1, linedata
        UnitNumber = Mid$(linedata, 1, 4)
        Unitname = Mid$(linedata, 6, 25)
        List1.AddItem UnitNumber + "  " + Unitname
    Loop
    Close 1
    PerComplete = (308 - List1.ListCount) / 308 * 100
Label1 = "Restaurants to convert: " + Str(List1.ListCount) + " " + Str(Format(PerComplete, 0)) + "% Complete"
End Sub


