VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "LiveConnect Rollout: Notify FE Program"
   ClientHeight    =   6375
   ClientLeft      =   6615
   ClientTop       =   4005
   ClientWidth     =   6675
   Icon            =   "NotifyFE.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6375
   ScaleWidth      =   6675
   Begin VB.CommandButton Command1 
      Caption         =   "Process"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   240
      TabIndex        =   4
      Top             =   5520
      Width           =   2055
   End
   Begin VB.CommandButton PrintButton 
      Caption         =   "Print List"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2400
      TabIndex        =   3
      Top             =   5520
      Width           =   1935
   End
   Begin VB.ListBox List1 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4860
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   6135
   End
   Begin VB.CommandButton ExitButton 
      Caption         =   "Exit"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4440
      TabIndex        =   0
      Top             =   5520
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "Restaurant #           General Manager (Use as the Caller Name in CrossForms)"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   5775
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Form1.MousePointer = 11
Command1.Enabled = False
List1.Enabled = True

If Dir("\\xnet\vol1\xnet\sysfiles\forms\lc\test.lc") = "" Then
    x = MsgBox("Your computer is not mapped properly to run this program.  Please contact someone in Technicial Services.  You need to be able to access the \\XNET\VOL1\XNET\SYSFILES\FORMS\LC folder.", vbCritical, "Program Error")
    Form1.MousePointer = 0
    End
End If

If Dir("J:\LCROLLOUT\TEST.LC") = "" Then
    x = MsgBox("Your computer is not mapped properly to run this program.  Please contact someone in Technicial Services.  You need to be able to access the J:(BNE Share) drive.", vbCritical, "Program Error")
    Form1.MousePointer = 0
    End
End If


If Dir("\\xnet\vol1\xnet\sysfiles\forms\lc\LC.V04") = "" Then
    x = MsgBox("No restaurants to notify at this time.", vbExclamation, "FE Notify")
    Form1.MousePointer = 0
    End
End If

Open "\\xnet\vol1\xnet\sysfiles\forms\lc\LC.PRE" For Output Access Write As 7
Open "\\xnet\vol1\xnet\sysfiles\forms\lc\LC.HST" For Append Access Write As 9
Open "\\xnet\vol1\xnet\sysfiles\forms\lc\LC.V04" For Input Access Read As 8
Do While Not EOF(8)
    LINEDATA = Input(137, 8)
    CRLF = Input(2, 8)
    Print #7, LINEDATA
    Print #9, LINEDATA
Loop
Close 7, 8, 9
Kill "\\xnet\vol1\xnet\sysfiles\forms\lc\LC.V04"
Open "\\xnet\vol1\xnet\sysfiles\forms\lc\LC.PRE" For Input As 1
Open "J:\LCROLLOUT\FENOTIFY.CSV" For Append Access Write As 9
    NUMUNITS = 1
    Do While Not EOF(1)
        Input #1, LINEDATA
        UNITNUM = Mid$(LINEDATA, 71, 4)
        GM = Mid$(LINEDATA, 103, 24)
        LC = Mid$(LINEDATA, 129, 8)
        List1.AddItem UNITNUM + "             " + GM
        Print #9, Str(UNITNUM) + "," + Trim(GM); "," + Mid$(LC, 1, 2) + "/" + Mid$(LC, 3, 2) + "/" + Mid$(LC, 5, 4)
    Loop
    Close 1, 9
    Form1.MousePointer = 0
    PrintButton.Enabled = True
    ExitButton.Enabled = True
End Sub

Private Sub ExitButton_Click()
End
End Sub

Private Sub Print_Click()
End Sub

Private Sub PrintButton_Click()
            Printer.Print
            Printer.Print
            Printer.Print
            Printer.FontSize = 12
            Printer.Print "       LiveConnect: FE Notify Program                                                                         " + Str(Now())
            Printer.Print "______________________________________________________________________________________"
            Printer.Print
            Printer.Print "            Work Order #    Restaurant #    General Manager (Use as Caller Name in CrossForms)"
            Printer.Print
            For W = 0 To List1.ListCount
                If List1.List(W) <> "" Then
                    Printer.Print "         _____________       " + List1.List(W)
                    Printer.Print
                End If
            Next W
            Printer.EndDoc
            Close 1
                x = MsgBox("Printing complete.", vbExclamation, "Print")

End Sub
