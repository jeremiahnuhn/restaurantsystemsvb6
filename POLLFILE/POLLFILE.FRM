VERSION 4.00
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   BackColor       =   &H00FF0000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Creating Polling Data File ... Please wait!"
   ClientHeight    =   375
   ClientLeft      =   2325
   ClientTop       =   2685
   ClientWidth     =   5250
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      name            =   "MS Sans Serif"
      charset         =   1
      weight          =   700
      size            =   18
      underline       =   0   'False
      italic          =   0   'False
      strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Height          =   780
   Icon            =   "POLLFILE.frx":0000
   Left            =   2265
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   375
   ScaleWidth      =   5250
   Top             =   2340
   Width           =   5370
End
Attribute VB_Name = "Form1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub Form_Paint()
' Find month and day to get file name
' fday = file day
' fmonth = file month
'
' VALUE RPT ARRAY 2dIM ARRAY COL1 = COUNT  COL2 = AMOUNT
'  1 = NET SALES
'  2 = TAX 1
'  3 = TAX 2

MAXARRAY = 45
FDAY = Format(Now, "dd")
FMONTH = Format(Now, "mm")
FYEAR = Format(Now, "yy")
If FDAY = "24" And FMONTH = "12" Then
    If Format(Now, "HH") >= "18" Then
        FDAY = "25"
    End If
End If
FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "DCR.POL"
ReDim RPT(MAXARRAY, 3)
Dim Storedate As String * 8
' OPEN DAILY CASH REPORT CSV
'
TOTAL = 0
BURGER = 0
CHEESE = 0
BACON = 0
COMBO = 0
Open FNAME For Input As #1
    Do While Not EOF(1) ' Check for end of file.
        Input #1, CAT
        ' CASE FOR FIRST VALUE OR CATEGORY
        Select Case CAT
        Case "DATE"
            Storedate = Input(8, #1)
            Storemm = Mid$(Storedate, 1, 2)
            Storedd = Mid$(Storedate, 4, 2)
            Storeyy = Mid$(Storedate, 7, 2)
            Call SREAD1(JUNK)
        Case "STORE"
            Call SREAD1(STORENUM)
        Case "SALES"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)
        Case "TOTAL DEPARTMENT"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)
        Case "TENDER DEPARTMENT"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)
        Case "COUPON DEPARTMENT"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)
        Case "DISCOUNT DEPARTMENT"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)
        Case "ITEM DEPARTMENT"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)
        Case "CONTROL TOTALS"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)
        Case "FINANCIAL"
            Call SREAD1(DESC)
            Call NREAD2(V1, V2)

        Case Else
            Line Input #1, JUNK
        End Select
        
        ' CASE FOR SECOND VALUE OR DESCRIPTION
        
        Select Case DESC
            Case "NET SALES"
                RPT(1, 1) = V1: RPT(1, 2) = V2
            Case "COUPONS"
                RPT(2, 1) = V1: RPT(2, 2) = V2
            Case "GROSS"
                RPT(3, 1) = V1: RPT(3, 2) = V2
            Case "TAX"
                RPT(4, 1) = V1: RPT(4, 2) = V2
            Case "EAT-IN"
                RPT(5, 1) = V1: RPT(5, 2) = V2
            Case "TAKE-OUT"
                RPT(6, 1) = V1: RPT(6, 2) = V2
            Case "DRIVE-THRU"
                RPT(7, 1) = V1: RPT(7, 2) = V2
            Case "CASH"
                RPT(8, 1) = V1: RPT(8, 2) = V2
            Case "CREDIT CARDS"
                RPT(9, 1) = V1: RPT(9, 2) = V2
            Case "GIFT CERTIFICATES"
                RPT(10, 1) = V1: RPT(10, 2) = V2
            Case "TRAVELERS CHECKS"
                RPT(11, 1) = V1: RPT(11, 2) = V2
            Case "BURGER OPEN OFFERS"
                RPT(12, 1) = V1: RPT(12, 2) = V2
            Case "BURGER COUPONS"
                RPT(13, 1) = V1: RPT(13, 2) = V2
            Case "SANDWICH OPEN OFFER"
                RPT(14, 1) = V1: RPT(14, 2) = V2
            Case "SANDWICH COUPONS"
                RPT(15, 1) = V1: RPT(15, 2) = V2
            Case "FRESH CHICKEN OFFERS"
                RPT(16, 1) = V1: RPT(16, 2) = V2
            Case "FRESH CHICKEN COUPNS"
                RPT(17, 1) = V1: RPT(17, 2) = V2
            Case "BREAKFST OPEN OFFERS"
                RPT(18, 1) = V1: RPT(18, 2) = V2
            Case "BREAKFAST COUPONS"
                RPT(19, 1) = V1: RPT(19, 2) = V2
            Case "CENTS OFF COUPONS"
                RPT(37, 1) = V1: RPT(37, 2) = V2
            Case "MISC. OPEN OFFERS"
                RPT(39, 1) = V1: RPT(39, 2) = V2
                COMBO = V1
            Case "MISC.TWO OPEN OFFERS"
                RPT(45, 1) = V1: RPT(45, 2) = V2
            Case "DAILY SPECIALS"
                RPT(40, 1) = V1: RPT(40, 2) = V2
            Case "MISC. BRK OPEN OFFER"
                RPT(41, 1) = V1: RPT(41, 2) = V2
            Case "MEALS WITH PREMIUMS"
                RPT(42, 1) = V1: RPT(42, 2) = V2
            Case "BRK OPEN OFFER CRED."
                RPT(44, 1) = V1: RPT(44, 2) = V2
            Case "CUSTOMER DISCOUNTS"
                RPT(20, 1) = V1: RPT(20, 2) = V2
            Case "EMPLOYEE DISCOUNTS"
                RPT(21, 1) = V1: RPT(21, 2) = V2
            Case "FREE ITEM COUPONS"
                RPT(38, 1) = V1: RPT(38, 2) = V2
            Case "CHICKEN BULK"
                RPT(22, 1) = V1: RPT(22, 2) = V2
            Case "PREMIUMS"
                RPT(33, 1) = V1: RPT(33, 2) = V2
            Case "SR. CITIZEN BEVS"
                RPT(34, 1) = V1: RPT(34, 2) = V2
            Case "ALLOWANCES"
                RPT(23, 1) = V1: RPT(23, 2) = V2
            Case "DISCOUNTS"
                RPT(24, 1) = V1: RPT(24, 2) = V2
            Case "CUSTOMER REFUND"
                RPT(25, 1) = V1: RPT(25, 2) = V2
            Case "MANAGER VOIDS"
                RPT(26, 1) = V1: RPT(26, 2) = V2
            Case "CANCEL ANY/LAST"
                RPT(35, 1) = V1: RPT(35, 2) = V2
            Case "CANCEL ORDER"
                RPT(36, 1) = V1: RPT(36, 2) = V2
            Case "TRAINING MODE"
                RPT(27, 1) = V1: RPT(27, 2) = V2
            Case "PAIDINS"
                RPT(28, 1) = V1: RPT(28, 2) = V2
            Case "PAIDOUTS"
                RPT(29, 1) = V1: RPT(29, 2) = V2
            Case "ACTUAL DEPOSITS"
                RPT(30, 1) = V1: RPT(30, 2) = V2
            Case "= THEO DEPOSITS"
                RPT(31, 1) = V1: RPT(31, 2) = V2
            'Case "OVER/SHORT"
             '   RPT(32, 1) = V1: RPT(32, 2) = V2
         End Select
    Loop
    Close #1    ' Close file.

'====================== MIX FILE =========================
FDAY = Format(Now, "dd")
FMONTH = Format(Now, "mm")
FYEAR = Format(Now, "yy")
If FDAY = "24" And FMONTH = "12" Then
    If Format(Now, "HH") >= "18" Then
       FDAY = "25"
    End If
End If
FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "MIX.POL"
Open FNAME For Input As #1
    Do While Not EOF(1) ' Check for end of file.
        Input #1, CAT
        ' CASE FOR FIRST VALUE OR CATEGORY
        Select Case CAT
        Case "MIX"
            Call SREAD3(DESC)
                Select Case DESC
                    Case "CHIC 8 PC"
                        Call NREAD2(CHK8P, CHK8N)
                    Case "CHIC 10 PC"
                        Call NREAD2(CHK10P, CHK10N)
                    Case "CHIC 12 PC"
                        Call NREAD2(CHK12P, CHK12N)
                    Case "CHIC 16 PC"
                        Call NREAD2(CHK16P, CHK16N)
                    Case "CHIC 20 PC"
                        Call NREAD2(CHK20P, CHK20N)
                    Case "CHIC 50 PC"
                        Call NREAD2(CHK50P, CHK50N)
                    Case "MEAL 8 PC CHICKEN"
                        Call NREAD2(MEAL8P, MEAL8N)
                    Case "FAMILY MEAL 12 PC"
                        Call NREAD2(MEAL12P, MEAL12N)
                    Case "MEAL 10 PC CHICKEN"
                        Call NREAD2(MEAL10P, MEAL10N)
                    Case "FAMILY MEAL 8 PC"
                        Call NREAD2(FAMILY8P, FAMILY8N)
                    Case "SR. CIT SM COFFEE"
                        Call NREAD2(COFFEEP, COFFEEN)
                    Case "SR. CIT. SM DECAFE"
                        Call NREAD2(DECAFEP, DECAFEN)
                    Case "SR. CITIZEN SM COKE"
                        Call NREAD2(COKEP, COKEN)
                    Case "SR. CITIZEN SM DIET"
                        Call NREAD2(DIETP, DIETN)
                    Case "SR. CIT. SM MELYELL"
                        Call NREAD2(MELP, MELN)
                    Case "SR. CIT. SM MR. PIBB"
                        Call NREAD2(PIBBP, PIBBN)
                    Case "SR. CIT. SM ORANGE"
                        Call NREAD2(ORANGEP, ORANGEN)
                    Case "SR. CIT. SM SPRITE"
                        Call NREAD2(SPRITEP, SPRITEN)
                    Case "SR. CIT. ICE TEA"
                        Call NREAD2(TEAP, TEAN)
                    Case "SR. CIT. UNSW TEA"
                        Call NREAD2(UNSWP, UNSWN)
                    Case "KIDS MEAL TOY"
                        Call NREAD2(TOYP, TOYN)
                    Case "GIFT CERTIFICATE"
                        Call NREAD2(GIFTP, GIFTN)
                    Case "POOF FOOTBALL"
                        Call NREAD2(PFBP, PFBQ)
                    End Select
       End Select
    Loop
' ======== CALUCLATED FIELDS ===================================
   '  GLASSWARE PROMOTION
   TEMPGLASS = RPT(42, 1) * 0.8
   TOTGLASS = RPT(42, 2) - TEMPGLASS
   TEMPGLASS2 = RPT(43, 1) * 0.8
   TOTGLASS2 = RPT(43, 2) - TEMPGLASS2
   PREUMBQTY = RPT(42, 1) + RPT(43, 1)
   PREUMBAMT = PREUMBQTY * 1.59
    
    ' SENIOR CITIZEN ADJUSTMENT
    SRAMT = (0.5 - COFFEEP)
    SRADJ = (COFFEEN + DECAFEN) * SRAMT
    SRAMT = (0.5 - TEAP)
    SRADJ = ((TEAN + UNSWN) * SRAMT) + SRADJ
    SRAMT = (0.5 - COKEP)
    SRADJ = ((COKEN + DIETN + MELN + PIBBN + ORANGEN + SPRITEN) * SRAMT) + SRADJ
    SRADJF = Format$(SRADJ, "$#,##0.00")
    
    
    ' BULK CHICKEN ADJUSTMENT
    CREDIT = 7.59 - CHK8P
    BULK = CREDIT * CHK8N
    BULK = (CREDIT * CHK8MN) + BULK
    CREDIT = 9.49 - CHK10P
    BULK = (CREDIT * CHK10N) + BULK
    CREDIT = 11.38 - CHK12P
    BULK = (CREDIT * CHK12N) + BULK
    CREDIT = 15.18 - CHK16P
    BULK = (CREDIT * CHK16N) + BULK
    CREDIT = 18.97 - CHK20P
    BULK = (CREDIT * CHK20N) + BULK
    CREDIT = 47.43 - CHK50P
    BULK = (CREDIT * CHK50N) + BULK
    CREDIT = 11.97 - MEAL8P
    BULK = (CREDIT * MEAL8N) + BULK
    CREDIT = 17.95 - MEAL12P
    BULK = (CREDIT * MEAL12N) + BULK
    CREDIT = 14.46 - FAMILY8P
    BULK = (CREDIT * FAMILY8N) + BULK
    BULK = 0
    BULKF = Format$(BULK, "$#,##0.00")
    RPT(22, 1) = 0
    
    
    GIFTTOTAL = GIFTN * GIFTP
    GIFTTOTALF = Format$(GIFTTOTAL, "$#,##0.00")


   COUPNUM = RPT(13, 1) + RPT(15, 1) + RPT(17, 1) + RPT(19, 1) + RPT(37, 1) + RPT(38, 1)
   COUPDOL = Format$(RPT(13, 2) + RPT(15, 2) + RPT(17, 2) + RPT(19, 2) + RPT(37, 2) + RPT(38, 2), "$#,##0.00")
   COUPAMT = RPT(13, 2) + RPT(15, 2) + RPT(17, 2) + RPT(19, 2) + RPT(37, 2) + RPT(38, 2)
   PREYNUM = RPT(13, 1) + RPT(15, 1) + RPT(17, 1) + RPT(19, 1)
   PREYDOL = Format$(RPT(13, 2) + RPT(15, 2) + RPT(17, 2) + RPT(19, 2), "$#,##0.00")
   PREYAMT = RPT(13, 2) + RPT(15, 2) + RPT(17, 2) + RPT(19, 2)
   OPENNUM = RPT(12, 1) + RPT(14, 1) + RPT(16, 1) + RPT(18, 1) + RPT(34, 1) + RPT(39, 1) + RPT(40, 1) + RPT(41, 1) + RPT(42, 1) + RPT(43, 1) + RPT(44, 1) + RPT(45, 1)
   OPENDOL = Format$(RPT(12, 2) + RPT(14, 2) + RPT(16, 2) + SRADJ + RPT(18, 2) + RPT(39, 2) + RPT(40, 2) + RPT(41, 2) + RPT(44, 2) + RPT(45, 2), "$#,##0.00")
   OPENAMT = RPT(12, 2) + RPT(14, 2) + RPT(16, 2) + RPT(18, 2) + SRADJ + RPT(39, 2) + RPT(40, 2) + RPT(41, 2) + RPT(44, 2) + RPT(45, 2)
   PRENNUM = RPT(12, 1) + RPT(14, 1) + RPT(16, 1) + RPT(18, 1) + RPT(39, 1) + RPT(40, 1) + RPT(41, 1) + RPT(44, 1) + RPT(45, 1)
   PRENDOL = Format$(RPT(12, 2) + RPT(14, 2) + RPT(16, 2) + RPT(18, 2) + RPT(39, 2) + RPT(40, 2) + RPT(41, 2) + RPT(44, 2) + RPT(45, 2), "$#,##0.00")
   PRENAMT = RPT(12, 2) + RPT(14, 2) + RPT(16, 2) + RPT(18, 2) + RPT(39, 2) + RPT(40, 2) + RPT(41, 2) + RPT(44, 2) + RPT(45, 2)
   CLRNUM = RPT(35, 1) + RPT(36, 1)
   CLRDOL = Format$(RPT(35, 2) + RPT(36, 2), "$#,##0.00")
   CLRAMT = RPT(35, 2) + RPT(36, 2)
   PAIDOUTNUM = RPT(29, 1) + RPT(25, 1)
   PAIDOUTDOL = Format$(RPT(29, 2) + RPT(25, 2), "$#,##0.00")
   PAIDOUTAMT = RPT(29, 2) + RPT(25, 2)
   CASHOS = Format$(RPT(30, 2) - RPT(31, 2), "$#,##0.00;($#,##0.00)")
   CASHOSAMT = RPT(30, 2) - RPT(31, 2)
   BKOPENNUM = RPT(41, 1) + RPT(18, 1)
   BKOPENDOL = Format$(RPT(41, 2) + RPT(18, 2), "$#,##0.00;($#,##0.00)")
   BKOPENAMT = RPT(41, 2) + RPT(18, 2)
   
   RGOPENNUM = RPT(12, 1) + RPT(14, 1) + RPT(16, 1) + RPT(39, 1)
   RGOPENDOL = Format$(RPT(12, 2) + RPT(14, 2) + RPT(16, 2) + RPT(39, 2), "$#,##0.00;($#,##0.00)")
   RGOPENAMT = RPT(12, 2) + RPT(14, 2) + RPT(16, 2) + RPT(39, 2)
   
   
   VBT = VBQ * VBP
   VBTD = Format$(VBT)

' FORMAT ARRAY
For X = 1 To MAXARRAY
   
        TEMP = RPT(X, 2)
        RPT(X, 3) = TEMP
        RPT(X, 2) = Format$(TEMP, "$#,##0.00;($#,##0.00)")
    
Next X
    
     Close
' ======== REPORT TO ACCOUNTING SPECIALIST =====================
    FNAME = "C:\AMWS\POLLING\TODAY.POL"
    MName = "C:\AMWS\POLLING\TODAY.CPL"
    Open FNAME For Output As 1    ' Open file.
    Open MName For Output As 3 'Open Mainframe File
    Dim DX As String * 14
    Dim KEY As String * 12
    Dim MMDDYY As String * 6
    Dim FID As String * 2
    sq = 0
    FID = "SA"
    MMDDYY = Storeyy + Storemm + Storedd
    KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
    Print #1,
    Print #1,
    Print #1, "              BNE-SALES FROM COMPRIS RESTAURANT"
    Print #1,
    Print #1, "      UNIT:  "; STORENUM;
    Print #1, "      MONTH:  "; FMONTH; " DAY: "; FDAY; " YEAR: "; FYEAR
    Print #1, "      ========================================"
    Print #1, "      COUNT", "DESCRIPTION", "AMOUNT"
    Print #1, "          ", "NET SALES", RPT(1, 2)
    Call MWRITE(KEY, 0, "NET SALES", RPT(1, 3))
    Print #1, "          ", "TAX 1    ", RPT(4, 2)
    Call MWRITE(KEY, 0, "TAX 1", RPT(4, 3))
    Print #1, "          ", "TAX 2                     "
    Call MWRITE(KEY, 0, "TAX 2", 0)
    Print #1, "     "; COUPNUM, "COUPONS  ", COUPDOL
    Call MWRITE(KEY, COUPNUM, "COUPONS", COUPAMT)
    Print #1, "     "; OPENNUM, "OPEN PROMO", OPENDOL
    Call MWRITE(KEY, OPENNUM, "OPEN PROMO", OPENAMT)
    Print #1, "          ", "DISCOUNT A"
    Call MWRITE(KEY, 0, "DISCOUNT A", 0)
    Print #1, "     "; RPT(20, 1), "DISCOUNT B", RPT(20, 2)
    Call MWRITE(KEY, RPT(20, 1), "DISCOUNT B", RPT(20, 3))
    Print #1, "          ", "DISCOUNT C"
    Call MWRITE(KEY, 0, "DISCOUNT C", 0)
    Print #1, "     "; RPT(21, 1), "EMP DISC A", RPT(21, 2)
    Call MWRITE(KEY, RPT(21, 1), "EMP DISC A", RPT(21, 3))
    Print #1, "          ", "MGR DISC"
    Call MWRITE(KEY, 0, "MGR DISC", 0)
    Print #1, "          ", "EMP CHARGE"
    Call MWRITE(KEY, 0, "EMP CHARGE", 0)
    Print #1, "     "; RPT(26, 1), "OVERRINGS", RPT(26, 2)
    Call MWRITE(KEY, RPT(26, 1), "OVERRINGS", RPT(26, 3))
    Print #1, "     "; CLRNUM, "CLEAR SALES", CLRDOL
    Call MWRITE(KEY, CLRNUM, "CLEAR SALES", CLRAMT)
    Print #1, "     "; RPT(5, 1), "HERE SALES", RPT(5, 2)
    Call MWRITE(KEY, RPT(5, 1), "HERE SALES", RPT(5, 3))
    Print #1, "     "; RPT(6, 1), "TOGO SALES", RPT(6, 2)
    Call MWRITE(KEY, RPT(6, 1), "TOGO SALES", RPT(6, 3))
    Print #1, "     "; RPT(7, 1), "DR TH SALES", RPT(7, 2)
    Call MWRITE(KEY, RPT(7, 1), "DR TH SALES", RPT(7, 3))
    Print #1, "     "; RPT(28, 1), "PAID INS", RPT(28, 2)
    Call MWRITE(KEY, RPT(28, 1), "PAID INS", RPT(28, 3))
    Print #1, "     "; PAIDOUTNUM, "PAID OUTS", PAIDOUTDOL
    '; "       GUEST REFUND "; RPT(25, 2)
    Call MWRITE(KEY, PAIDOUTNUM, "PAID OUTS", PAIDOUTAMT)
    Call MWRITE(KEY, 0, "GUEST REFUND", RPT(25, 3))
    Print #1, "     "; "    ", "DEPOSITS", RPT(30, 2)
    Call MWRITE(KEY, 0, "DEPOSITS", RPT(30, 3))
'    Print #1, "    *"; RPT(25, 1), "CUST REFUND", RPT(25, 2)
    Print #1, "     "; "    ", "CASH O/S   ", CASHOS
    Call MWRITE(KEY, 0, "CASH O/S", CASHOSAMT)
    Print #1, "     "; RPT(33, 1), "PREMIUM A", RPT(33, 2)
    Call MWRITE(KEY, RPT(33, 1), "PREMIUM A", RPT(33, 3))
    'Print #1, "     "; VBQ, "COUPON BK", VBTD
    'Call MWRITE(KEY, VBQ, "PREMIUM B", VBT)
    Print #1, "     "; RPT(38, 1), "FREE ITEMS", RPT(38, 2)
    Call MWRITE(KEY, RPT(38, 1), "FREE ITEMS", RPT(38, 3))
    Print #1, "     "; "    ", "10% ITEMS"
    Call MWRITE(KEY, 0, "10% ITEMS", 0)
    Print #1, "     "; "    ", "50% ITEMS"
    Call MWRITE(KEY, 0, "50% ITEMS", 0)
    Print #1, "     "; RPT(37, 1), "CENTS OFF", RPT(37, 2)
    Call MWRITE(KEY, RPT(37, 1), "CENTS OFF", RPT(37, 3))
    Print #1, "     "; RPT(34, 1), "SR CITIZENS", SRADJF
    Call MWRITE(KEY, RPT(34, 1), "SR CITIZENS", SRADJ)
    Print #1, "     "; RPT(9, 1), "CRED CARD", RPT(9, 2)
    Call MWRITE(KEY, RPT(9, 1), "CRED CARD", RPT(9, 3))
    Print #1, "     "; RPT(8, 1), "TOTAL CASH", RPT(8, 2)
    Call MWRITE(KEY, RPT(8, 1), "TOTAL CASH", RPT(8, 3))
    Print #1, "     "; RPT(22, 1), "BULK CHICKEN", BULKF
    Call MWRITE(KEY, RPT(22, 1), "BULK CHICKEN", BULK)
    Print #1, "     "; PREYNUM, "PRESET Y", PREYDOL
    Call MWRITE(KEY, PREYNUM, "PRESET Y", PREYAMT)
    Print #1, "     "; PRENNUM, "PRESET N", PRENDOL
    Call MWRITE(KEY, PRENNUM, "PRESET N", PRENAMT)
    Print #1, "     "; GIFTN, "GIFT CERT", GIFTTOTALF
    Call MWRITE(KEY, GIFTN, "GIFT CERT", GIFTTOTAL)
    Print #1, "     "; BKOPENNUM, "BRKFST OPEN", BKOPENDOL
    Call MWRITE(KEY, BKOPENNUM, "BKFST OPEN", BKOPENAMT)
    Print #1, "     "; RGOPENNUM, "REGULAR OPEN", RGOPENDOL
    Call MWRITE(KEY, RGOPENNUM, "REGULAR OPEN", RGOPENAMT)



FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "DRR.POL"
ReDim RDR(6, 3)
ReDim HR(24, 3)
' OPEN DAILY CASH REPORT CSV
'

Open FNAME For Input As #2
    X = 1
    Do While Not EOF(2) ' Check for end of file.
        Input #2, CAT
        ' CASE FOR FIRST VALUE OR CATEGORY
        Select Case CAT
            Case "TIME"
                Call NREAD3(V1, V2, V3)
                HR(X, 1) = V2: HR(X, 2) = V3
                X = X + 1
            Case "LATE NIGHT SALES"
                Call NREAD3(V1, V2, V3)
                RDR(1, 1) = V1: RDR(1, 2) = V2
            Case "REGULAR BREAKFAST"
                Call NREAD3(V1, V2, V3)
                RDR(2, 1) = V1: RDR(2, 2) = V2
            Case "LUNCH"
                Call NREAD3(V1, V2, V3)
                RDR(3, 1) = V1: RDR(3, 2) = V2
            Case "LATE AFTERNOON"
                Call NREAD3(V1, V2, V3)
                RDR(4, 1) = V1: RDR(4, 2) = V2
            Case "DINNER"
                Call NREAD3(V1, V2, V3)
                RDR(5, 1) = V1: RDR(5, 2) = V2
            Case "LATE DINNER"
                Call NREAD3(V1, V2, V3)
                RDR(6, 1) = V1: RDR(6, 2) = V2
            End Select
       Loop

For X = 1 To 6
   
        TEMP = RDR(X, 2)
        RDR(X, 3) = TEMP
        RDR(X, 2) = Format$(TEMP, "$#,##0.00;($#,##0.00)")
    
Next X
        

    Print #1, "     "; RDR(1, 1), "00 - 05 SALES", RDR(1, 2)
    Call MWRITE(KEY, RDR(1, 1), "00 - 05 SALES", RDR(1, 3))
    Print #1, "     "; RDR(2, 1), "05 - 11 SALES", RDR(2, 2)
    Call MWRITE(KEY, RDR(2, 1), "05 - 11 SALES", RDR(2, 3))
    Print #1, "     "; RDR(3, 1), "11 - 15 SALES", RDR(3, 2)
    Call MWRITE(KEY, RDR(3, 1), "11 - 15 SALES", RDR(3, 3))
    Print #1, "     "; RDR(4, 1), "15 - 17 SALES", RDR(4, 2)
    Call MWRITE(KEY, RDR(4, 1), "15 - 17 SALES", RDR(4, 3))
    Print #1, "     "; RDR(5, 1), "17 - 20 SALES", RDR(5, 2)
    Call MWRITE(KEY, RDR(5, 1), "17 - 20 SALES", RDR(5, 3))
    Print #1, "     "; RDR(6, 1), "20 - 00 SALES", RDR(6, 2)
    Call MWRITE(KEY, RDR(6, 1), "20 - 00 SALES", RDR(6, 3))
    
    FID = "HS"
    KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
    sq = 0
    For X = 1 To 24
        TEMPP = "HOUR " + Format$(X, "##")
        Call MWRITE(KEY, HR(X, 1), TEMPP, HR(X, 2))
    Next X
    Close #2 'Close DRR File
    Close #1 'SALES REPORT
' ************** payroll ****************
FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "ESUM.POL"

' OPEN DAILY CASH REPORT CSV
'
Dim LNAME As String
Dim FIRST As String
Dim SSNUM As String
Dim REGHRS As String
Dim OTHRS As String
ReDim RPT(150, 5)
Open FNAME For Input As #1 'open Compris Pay File
    X = 0
    Do While Not EOF(1) ' Check for end of file.
        Input #1, CAT
        ' CASE FOR FIRST VALUE OR CATEGORY
        Select Case CAT
        Case "DATE"
            Storedate = Input(8, #1)
            Storemm = Mid$(Storedate, 1, 2)
            Storedd = Mid$(Storedate, 4, 2)
            Storeyy = Mid$(Storedate, 7, 2)
            Call SREAD1(JUNK)
        Case "STORE"
            Call SREAD1(STORENUM)
        Case "SUMMARY"
           X = X + 1
           Input #1, LNAME, FIRST, SSNUM, REGHRS, OTHRS, JUNK, JUNK, JUNK, JUNK
           RPT(X, 1) = LNAME
           RPT(X, 2) = FIRST
           RPT(X, 3) = SSNUM
           RPT(X, 4) = REGHRS
           RPT(X, 5) = OTHRS
        End Select
    Loop 'End While

  Close #1
' ======== REPORT TO ACCOUNTING SPECIALIST =====================
       sq = 0
       FID = "DT"
       MMDDYY = Storeyy + Storemm + Storedd
       KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
    'OutPut MainFrame File
    For C = 1 To X
      S = Mid$(RPT(C, 3), 1, 3) & Mid$(RPT(C, 3), 5, 2) & Mid$(RPT(C, 3), 8, 4)
      Call MWRITEP(KEY, S, RPT(C, 4), RPT(C, 5))
    Next C
 ' ************ end payroll ******************
    Close #1 'Close Daily Time Records File
    Close #5 'ASCII Selection List Forms
 
 '  ********** FILE FOR ANGELA LANGLEY ******************
    'Open "C:\NODESYS\UNIT.ID" For Input As #1
    'Line Input #1, FILEDATA
    'Close 1
    'UNITNAME = Mid$(FILEDATA, 5, 19)

    'FNAME = "C:\AMWS\POLLING\MARKETNG.POL"
    'Open FNAME For Append As 1    ' Open file.
    'TOTAL8 = MEAL8N + FAMILY8N
    'TOTAL10 = MEAL10N + FAMILY10N
    'BULKCHK = CHK8N + CHK10N + CHK12N + CHK16N + CHK20N + CHK50N + MEAL8N + MEAL12N + MEAL10N + FAMILY8N
    'Print #1, STORENUM; UNITNAME; MMDDYY; " "; Format$(TOTAL8, "@@@@"); Format$(TOTAL10, "@@@@"); Format$(MEAL12N, "@@@@"); Format$(PFBQ, "@@@@"); Format$(BULKCHK, "@@@@")
    'Close 1
'====================== MIX FILE FOR PRODUCT MIX =================
FDAY = Format(Now, "dd")
FMONTH = Format(Now, "mm")
FYEAR = Format(Now, "yy")
If FDAY = "24" And FMONTH = "12" Then
    If Format(Now, "HH") >= "18" Then
       FDAY = "25"
    End If
End If
Dim MIXNAME As String * 20
FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "MIX.POL"
     sq = 1
     FID = "PM"
     KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
      Open FNAME For Input As #1 'open Compris Pay File
      MName = "C:\AMWS\POLLING\TODAY.CPL"
      'Open MName For Append As 3 'Open Mainframe File
      Do While Not EOF(1) ' Check for end of file.
        Input #1, CAT
        ' CASE FOR FIRST VALUE OR CATEGORY
        Select Case CAT
        Case "MIX"
            Input #1, JUNK, MIXNAME, MIXCOST, MIXQTY, MIXTOTAL
            If MIXTOTAL <> 0 Then
                PLINE = KEY + Format$(sq, "000")
                FMIXCOST = Format$(MIXCOST, "+00.00;-00.00")
                FMIXQTY = Format$(MIXQTY, "+0000;-0000")
                FMIXTOTAL = Format$(MIXTOTAL, "+0000.00;-0000.00")
                PLINE = PLINE + UCase(MIXNAME) + FMIXQTY + FMIXCOST + FMIXTOTAL
                Print #3, PLINE
                sq = sq + 1
            End If
        End Select
    Loop
 
 
 
 ' Do Weekly Time Records!
   If WeekDay(Now) = 2 Then
      FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "ESUM.WK"
      ' Process Weekly Time Reocrd Files
      ReDim RPT(150, 5)
      Close
      Open FNAME For Input As #1 'open Compris Pay File
      MName = "C:\AMWS\POLLING\TODAY.CPL"
      Open MName For Append As 3 'Open Mainframe File
      X = 0
      Do While Not EOF(1) ' Check for end of file.
        Input #1, CAT
        ' CASE FOR FIRST VALUE OR CATEGORY
        Select Case CAT
        Case "DATE"
            Weekdate = Input(19, #1)
            Storemm = Mid$(Weekdate, 12, 2)
            Storedd = Mid$(Weekdate, 15, 2)
            Storeyy = Mid$(Weekdate, 18, 2)
            Call SREAD1(JUNK)
            'Storedate = Input(8, #1)
            'Storemm = Mid$(Storedate, 1, 2)
            'Storedd = Mid$(Storedate, 4, 2)
            'Storeyy = Mid$(Storedate, 7, 2)
            'Call SREAD1(JUNK)
        Case "STORE"
            Call SREAD1(STORENUM)
        Case "SUMMARY"
           X = X + 1
           Input #1, LNAME, FIRST, SSNUM, REGHRS, OTHRS, JUNK, JUNK, JUNK, JUNK
           RPT(X, 1) = LNAME
           RPT(X, 2) = FIRST
           RPT(X, 3) = SSNUM
           RPT(X, 4) = REGHRS
           RPT(X, 5) = OTHRS
        End Select
       Loop 'End While
     Close #1
     sq = 0
     FID = "WT"
     MMDDYY = Storeyy + Storemm + Storedd
     KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
    'OutPut MainFrame File
     For C = 1 To X
      S = Mid$(RPT(C, 3), 1, 3) & Mid$(RPT(C, 3), 5, 2) & Mid$(RPT(C, 3), 8, 4)
      Call MWRITEP(KEY, S, RPT(C, 4), RPT(C, 5))
     Next C
    Close #3 'Close MainFrame File

'   ========== FILE FOR DMSS INVENTORY CONTROL ======================
If WeekDay(Now) = 2 Then
    ReDim INV(60, 2)
    Open "C:\AMWS\POLLING\INVENTOR.LST" For Input As #1
    X = 0
    Do While Not EOF(1) ' Check for end of file.
        Line Input #1, TEMP
        INV(X, 1) = Mid$(TEMP, 1, 22)
        INV(X, 2) = Mid$(TEMP, 23, 4)
        X = X + 1
    Loop
    Close 1
    
    TUVAR = 0
    FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "COG.WK"
    Open FNAME For Input As #1
    FNAME = "C:\AMWS\POLLING\INVENTOR.POL"
    Open FNAME For Output As 2
    Print #2, "COMPRIS WEEKLY COST OF GOODS REPORT"
    Print #2, "HARDEE'S OF " + Format$(UNITNAME, ">")
    Print #2,
    Print #2, "ITEM               WASTE  ACTUAL   THEO    UNIT     AMOUNT"
    Print #2, "DESCRIPTION               USAGE    USAGE   VAR.     VAR."
    Print #2, "-----------------------------------------------------------"
    Line Input #1, FILEDATA
    Line Input #1, FILEDATA
    Line Input #1, FILEDATA
    Do While Not EOF(1) ' Check for end of file.
        Input #1, JUNK, DEPT, INVNAME, JUNK, JUNK, JUNK, JUNK, WASTE, ENDINV, usage, TUSAGE, UVAR, AVAR, JUNK, JUNK, JUNK
        TESTNAME2 = Trim(Mid$(INVNAME, 1, 15))
            For X = 0 To 60
                    TESTNAME1 = Trim(Mid$(INV(X, 1), 1, 15))
                    If TESTNAME1 = TESTNAME2 Then
                         CASECOUNT = INV(X, 2)
                         X = 60
                    End If
             Next X
         QWASTE = 0
         QENDINV = 0
         Qusage = 0
         QTUSAGE = 0
         QUVAR = 0
         QWASTE = WASTE * CASECOUNT
         QENDINV = ENDINV * CASECOUNT
         Qusage = usage * CASECOUNT
         QTUSAGE = TUSAGE * CASECOUNT
         QUVAR = UVAR * CASECOUNT
         TUVAR = TUVAR + QUVAR
         If usage <> 0 Then
            Print #2, Format$(Trim(INVNAME), "!@@@@@@@@@@@@@@@@@@@@"); Format$(QWASTE, "0000"); "   "; Format$(Qusage, "00000"); "   "; Format$(QTUSAGE, "00000"); "   "; Format$(QUVAR, "00000"); "   "; Format$(AVAR, "0000.00")
         End If
    Loop
    Print #2,
    Print #2, "TOTAL                                              "; TUVAR
    Print #2,
    Print #2,
    Close
End If

    '============================================================
    '  FILE FOR DAILY PAYROLL HOURS
    '============================================================
   If WeekDay(Now) = 2 Then
     sq = 1
     FID = "PT"
     KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
     FNAME = "C:\AMWS\POLLING\" + FMONTH + FDAY + "TIME.POL"
     Open FNAME For Input As #1 'open Compris Pay File
      MName = "C:\AMWS\POLLING\TODAY.CPL"
      Open MName For Append As 3 'Open Mainframe File
      Do While Not EOF(1) ' Check for end of file.
        Input #1, CAT
        ' CASE FOR FIRST VALUE OR CATEGORY
        Select Case CAT
        Case "TIMECARD"
            Input #1, NAME1, NAME2, SSNUM
            Line Input #1, JUNK
        Case "SHIFT"
            Input #1, NAME1, NAME2, JUNK, WDAY, JUNK, JUNK
            Line Input #1, DHRS
            PLINE = KEY + Format$(sq, "000")
            TSSNUM = Mid$(SSNUM, 1, 3) + Mid$(SSNUM, 5, 2) + Mid$(SSNUM, 8, 4)
            PLINE = PLINE + TSSNUM
            HWORK = Format$(Mid$(DHRS, 1, 2) + Mid$(DHRS, 4, 2), "+000000000;-000000000")
            JOBCODE = Mid$(DHRS, 7, 1)
            Z = Mid$(DHRS, 9, 1)
            If Z <> "" Then
                CORRT = "Y"
                Else
                CORRT = "N"
            End If
            PLINE = PLINE + HWORK + Mid$(WDAY, 1, 3) + JOBCODE + CORRT
            Print #3, PLINE
            sq = sq + 1
        End Select

      Loop
    End If ' IF WEEKDAY = 5 (THURSDAY)
    FNAME = "C:\AMWS\POLLING\PAYROLL.POL"
    MName = "C:\NEWPOS\CAT04.DIF"
       Open FNAME For Output As 2 ' Open Payroll Report file.
     ' MMDDYY = FYEAR + FMONTH + FDAY
       Print #2,
       Print #2,
       Print #2, "              BNE-PAYROLL FROM COMPRIS RESTAURANT"
       Print #2,
       Print #2, "      UNIT:        "; STORENUM; " "; RESTNAME
       Print #2, "      WEEK ENDING:  "; Storedate, , "PAGE:  1"
       Print #2, "      ============================================================"
       Print #2, "      SOC SEC #     REG HRS", "OT HRS", "EMPLOYEE"
       Print #2,
       PAGENUM = 2
       LINECOUNT = 0
       For C = 1 To X
       LINECOUNT = 1 + LINECOUNT
       FULLNAME = Trim(RPT(C, 2)) + " " + RPT(C, 1)
       Print #2, "      "; RPT(C, 3); "   "; RPT(C, 4), RPT(C, 5), FULLNAME
       If LINECOUNT > 50 Then
          Print #2, Chr(12)
          Print #2,
          Print #2,
          Print #2, "              BNE-PAYROLL FROM COMPRIS RESTAURANT"
          Print #2,
          Print #2, "      UNIT:  "; STORENUM; "  "; RESTNAME
          Print #2, "      WEEK ENDING:  "; Storedate, , "PAGE: "; PAGENUM
          Print #2, "      ============================================================"
          Print #2, "      SOC SEC #     REG HRS", "OT HRS", "EMPLOYEE"
          Print #2,
          LINECOUNT = 0
          PAGENUM = 1 + PAGENUM
        End If
       Next C
    End If 'End Weekly Time Records Process
    Close


    Open "C:\AMWS\C2MACRO.DAT" For Output As 4    ' Open file.
    Print #4, "ECHO NEXT TASK"
    Close #4

End

End Sub

