VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Credit Card Adjustment Log"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8265
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   8265
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox NetAdj 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   3240
      TabIndex        =   1
      Top             =   480
      Width           =   1335
   End
   Begin VB.OptionButton Bob 
      Caption         =   "Bob"
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   480
      Width           =   1095
   End
   Begin VB.OptionButton Brad 
      Caption         =   "Brad"
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   840
      Width           =   1095
   End
   Begin VB.OptionButton Dannie 
      Caption         =   "Dannie"
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   1200
      Width           =   1095
   End
   Begin VB.OptionButton Jody 
      Caption         =   "Jody"
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Top             =   1560
      Width           =   1095
   End
   Begin VB.OptionButton Sheila 
      Caption         =   "Sheila"
      Height          =   375
      Left            =   240
      TabIndex        =   6
      Top             =   1920
      Value           =   -1  'True
      Width           =   1095
   End
   Begin VB.OptionButton Default 
      Caption         =   "Default"
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   2280
      Width           =   1095
   End
   Begin VB.CommandButton Close 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4440
      TabIndex        =   4
      Top             =   2160
      Width           =   1935
   End
   Begin VB.CommandButton CreateFiles 
      Caption         =   "Create Log"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1800
      TabIndex        =   3
      Top             =   2160
      Width           =   1935
   End
   Begin VB.TextBox RestNum 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   1560
      TabIndex        =   0
      Top             =   480
      Width           =   1335
   End
   Begin VB.TextBox Comments 
      Height          =   735
      Left            =   1560
      TabIndex        =   2
      Top             =   1320
      Width           =   6495
   End
   Begin VB.Label Label5 
      Caption         =   "Ver 1"
      Height          =   255
      Left            =   7800
      TabIndex        =   16
      Top             =   2760
      Width           =   375
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Net Adjustment"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3240
      TabIndex        =   15
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Completd By"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   0
      TabIndex        =   14
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Restaurant Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1440
      TabIndex        =   13
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Comments for Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1560
      TabIndex        =   12
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label8 
      Caption         =   "(No commas)"
      Height          =   255
      Left            =   3000
      TabIndex        =   11
      Top             =   1080
      Width           =   1815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Close_Click()
    End
End Sub

Private Sub CreateFiles_Click()

    If RestNum.Text = "" Then
            Response = MsgBox("Restaurant Number Missing", vbInformation, "Error")
            Exit Sub
    End If
    If NetAdj.Text = "" Then
            Response = MsgBox("Net Adjustment Information Missing", vbInformation, "Error")
            Exit Sub
    End If

    If Bob.Value = True Then CBY = "Bob"
    If Brad.Value = True Then CBY = "Brad"
    If Dannie.Value = True Then CBY = "Dannie"
    If Sheila.Value = True Then CBY = "Sheila"
    If Jody.Value = True Then CBY = "Jody"


NetBal = Val(NetAdj.Text)
RestNumber = RestNum.Text
If Dir("\\bnewest\share1\polling\Data\Credit Card Adjustments.csv") = "" Then
    Open ("\\bnewest\share1\polling\Data\Credit Card Adjustments.csv") For Output As #9
        Print #9, "####,DateTime,Org Visa,Adj Visa,Org MC,Adj MC,Org AE,Adj AE,Org D,Adj D,Net Adj,User,Comment"
    Close #9
End If

Open ("\\bnewest\share1\polling\Data\Credit Card Adjustments.csv") For Append As #9
Print #9, Str(RestNumber) + "," + Str(Now()) + ",0,0,0,0,0,0,0,0,";
Print #9, Str(NetBal) + "," + CBY + ","; Comments.Text

Close #9
RestNum.Text = ""
NetAdj.Text = ""
Comments.Text = ""
End Sub
