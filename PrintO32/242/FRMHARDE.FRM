VERSION 5.00
Begin VB.Form frmhardees 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Crew Person Orientation Workbook Print Utility"
   ClientHeight    =   8010
   ClientLeft      =   1155
   ClientTop       =   1545
   ClientWidth     =   9780
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8010
   ScaleWidth      =   9780
   WindowState     =   2  'Maximized
   Begin VB.CommandButton SPrint 
      Caption         =   "Print (Spanish)"
      Height          =   495
      Left            =   2160
      TabIndex        =   10
      Top             =   5760
      Width           =   2295
   End
   Begin VB.CommandButton CmdGovernment 
      Caption         =   "Print State and Federal Forms"
      Height          =   495
      Left            =   960
      MousePointer    =   1  'Arrow
      TabIndex        =   7
      Top             =   7320
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton cmdexit 
      Caption         =   "&Exit"
      Default         =   -1  'True
      Height          =   495
      Left            =   5280
      TabIndex        =   0
      Top             =   5280
      Width           =   2415
   End
   Begin VB.CommandButton PrintCommand 
      Caption         =   "Print (English)"
      Height          =   495
      Left            =   2160
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   4680
      Width           =   2295
   End
   Begin VB.Label Label4 
      Caption         =   "November       2004           IRIS 2"
      Height          =   735
      Left            =   7920
      TabIndex        =   11
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "Spanish Version"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2280
      TabIndex        =   9
      Top             =   5400
      Width           =   2175
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "English Verson"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   8
      Top             =   4320
      Width           =   2295
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "To Exit this program press Exit or press Enter Key"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4800
      TabIndex        =   6
      Top             =   4320
      Width           =   3255
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "To Print the manual, press Print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2040
      TabIndex        =   4
      Top             =   3840
      Width           =   2895
   End
   Begin VB.Label lbllegal 
      Caption         =   "The Federal and State Forms now print as part of the standard workbook."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   960
      TabIndex        =   3
      Top             =   2760
      Width           =   7935
   End
   Begin VB.Label frmcrewperson 
      Alignment       =   2  'Center
      Caption         =   "CREW PERSON ORIENTATION WORKBOOK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   2880
      TabIndex        =   2
      Top             =   840
      Width           =   3255
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "HARDEE'S"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3240
      TabIndex        =   1
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmhardees"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim X As Single
Dim Y As Single





Private Sub port1()
         SendKeys "%FP" + "{ENTER}", True
For X = 1 To 1000
     For Y = 1 To 150
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 100 ' Was 1000
    DoEvents
Next X
For X = 1 To 100 ' Was 10000
frmWhilePrinting.Show
Next X
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
End Sub

Private Sub portP1()
Dim page As Integer
page = 1
         SendKeys "%FP{TAB}{TAB}" + Str(page) + "{ENTER}"
For X = 1 To 1000
     For Y = 1 To 150
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 1000
    DoEvents
Next X
For X = 1 To 10000
frmWhilePrinting.Show
Next X
End Sub

Private Sub port2()
Dim page As Integer
page = 2
         SendKeys "%FP{TAB}" + Str(page) + "{ENTER}"
For X = 1 To 1000
     For Y = 1 To 150
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 1000
    DoEvents
Next X
For X = 1 To 10000
frmWhilePrinting.Show
Next X
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
End Sub




Private Sub Pause()
For X = 1 To 10000
     For Y = 1 To 400
        DoEvents
     Next Y
Next X
For X = 1 To 10000
   DoEvents
Next X
End Sub

Private Sub land1()
        SendKeys "%FP" + "{ENTER}"
For X = 1 To 1000
     For Y = 1 To 200
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 100 ' was 100
   DoEvents
Next X
For X = 1 To 100 ' was 10000
frmWhilePrinting.Show
Next X
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
End Sub





Private Sub cmdexit_Click()
Dim tempfilename As String
tempfilename = Dir("C:\FLAGPRNT.*")
  If tempfilename <> "" Then
   Kill "C:\Flagprnt.*"
  End If
End
End Sub

Private Sub CmdGovernment_Click()
Dim tempfilename As String
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.Label3.Caption = "5 Minutes"
frmWhilePrinting.hscroll1.Value = 1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\statefrm.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
portP1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00138.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
port1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00139.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
port1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\i-9.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
port1
For X = 1 To 1000
        DoEvents
Next X
tempfilename = Dir("C:\FLAGPRNT.*")
   If tempfilename <> "" Then
   Kill "C:\Flagprnt.*"
   End If
End
End Sub

Private Sub Command1_Click()
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.hscroll1.Value = 1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\statefrm.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
port2
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00138.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 2
port2
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00139.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 2
port2
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\i-9.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 2
port2
For X = 1 To 10000
        DoEvents
Next X
frmWhilePrinting.Hide
Load frmsecond
frmsecond.Show
End Sub

Private Sub Form_Load()
Dim Filedata As String
Open "C:\Flagprnt.dat" For Append As #1
     Filedata = "I am Printing. Do not poll"
     Print #1, Filedata
Close #1
End Sub

Private Sub PrintCommand_Click()
frmWhilePrinting.hscroll1.Value = 1
Dim tempfilename As String
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.Label3.Caption = "10 Minutes"
' Page 1 - 4 Page Maker File
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\training\CPORIEN1.PDF"
X = Shell(progName, 1)
port1

' Page 5 - 6 Federal W 4
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00229.000"
X = Shell(progName, 1)
port1
' Page 7 - 8 State W 4
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00230.000"
X = Shell(progName, 1)
port1
' Page 9 New Employee PCN Information Sheet
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00147.000"
X = Shell(progName, 1)
port1
' Page 10 Position History
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00125.000"
X = Shell(progName, 1)
port1
' Page 11 - 14 Starbridge
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00239.000"
X = Shell(progName, 1)
port1
' Page 15 Sales Representative Cash Accountability
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00015.000"
X = Shell(progName, 1)
'SendKeys "%FRR" + "{ENTER}", True
port1
For X = 1 To 10000
     For Y = 1 To 7
         DoEvents
     Next Y
Next X
' Page 16 State Specific Forms!!
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\statefrm.dp"
X = Shell(progName, 1)
port1

For X = 1 To 10000
     For Y = 1 To 7
         DoEvents
     Next Y
Next X

' Page 17 - 18 Harassment / Sexual Harassment
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\training\SEXHAR.PDF"
X = Shell(progName, 1)
port1
' Page 19 - 21 Diversity
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\training\CPORIEN2.PDF"
X = Shell(progName, 1)
port1
' Page 22 - 23 Grooming Task Outline
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00175.000"
X = Shell(progName, 1)
port1
' Page 24 - 25 Training and Development Task Outline
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00181.000"
X = Shell(progName, 1)
port1
' Page 26 - 27 Safety
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00178.000"
X = Shell(progName, 1)
port1

' Page 28 - 29 Security
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00177.000"
X = Shell(progName, 1)
port1
' Page 30 Serious About Service
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00223.000"
X = Shell(progName, 1)
port1

' Page 31 - 33 Guest Services
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00179.000"
X = Shell(progName, 1)
port1
' Page 35 - 36 Cleaning and Sanitation
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00176.000"
X = Shell(progName, 1)
port1
' Page 37 - 38 Receiving and Storage
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00180.000"
X = Shell(progName, 1)
port1
' Page 39 - 40 Employee File Statement
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00148.000"
X = Shell(progName, 1)
port1
' Page 41 Employee File Jacket Check List
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00232.000"
X = Shell(progName, 1)
port1

' Page 42 Crew Training Certification
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00103.000"
X = Shell(progName, 1)
SendKeys "%FRR" + "{ENTER}", True
land1
' Page 43 Completion Page
progName = "C:\ACROBAT3\READER\ACRORD32.EXE C:\training\cporien3.pdf"
X = Shell(progName, 1)
port1
tempfilename = Dir("C:\FLAGPRNT.*")
   If tempfilename <> "" Then
   Kill "C:\Flagprnt.*"
   End If
End
End Sub


Private Sub SPrint_Click()
Dim tempfilename As String
Dim Arc51 As Boolean
Dim Arc52 As Boolean
Dim Arc53 As Boolean
Dim Arc23 As Boolean
Dim Arc37 As Boolean
Dim Arc38 As Boolean
Dim Arc54 As Boolean
Dim Arc34 As Boolean
Dim Arc36 As Boolean
Dim Arc32 As Boolean
Dim Arc33 As Boolean
Dim Arc40 As Boolean
Dim Arc55 As Boolean
Dim Arc56 As Boolean
Dim Arc57 As Boolean
Dim Arc142 As Boolean
Dim ShowError As Boolean
ShowError = False
Arc51 = True
Arc52 = True
Arc53 = True
Arc23 = True
Arc37 = True
Arc38 = True
Arc54 = True
Arc34 = True
Arc36 = True
Arc32 = True
Arc33 = True
Arc40 = True
Arc55 = True
Arc56 = True
Arc57 = True
Arc142 = True
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00051.000") = "" Then
   Arc51 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00052.000") = "" Then
   Arc52 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00053.000") = "" Then
   Arc53 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00023.000") = "" Then
   Arc23 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00037.000") = "" Then
   Arc37 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00038.000") = "" Then
   Arc38 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00054.000") = "" Then
   Arc54 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00034.000") = "" Then
   Arc34 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00142.000") = "" Then
   Arc142 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00036.000") = "" Then
   Arc36 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00032.000") = "" Then
   Arc32 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00033.000") = "" Then
   Arc33 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00040.000") = "" Then
   Arc40 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00055.000") = "" Then
   Arc55 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00056.000") = "" Then
   Arc56 = False
   ShowError = True
End If
If Dir("C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00057.000") = "" Then
   Arc57 = False
   ShowError = True
End If


If ShowError = True Then
   Me.Hide
   FrmMissing.Show modal
   If Arc51 = False Then
      FrmMissing.Missing.AddItem "Cover Page"
   End If
   If Arc52 = False Then
      FrmMissing.Missing.AddItem "Welcome to Hardee's"
   End If
   If Arc53 = False Then
      FrmMissing.Missing.AddItem "Your Orientation"
   End If
   If Arc23 = False Then
      FrmMissing.Missing.AddItem "Grooming, Hygiene and Uniform"
   End If
   If Arc37 = False Then
      FrmMissing.Missing.AddItem "Safety"
   End If
   If Arc38 = False Then
      FrmMissing.Missing.AddItem "Security"
   End If
   If Arc54 = False Then
      FrmMissing.Missing.AddItem "New Employee PCN Info."
   End If
   If Arc34 = False Then
      FrmMissing.Missing.AddItem "Medical Assistance Information"
   End If
   If Arc142 = False Then
      FrmMissing.Missing.AddItem "Medical Assistance Enrollment App"
   End If
   If Arc36 = False Then
      FrmMissing.Missing.AddItem "Sales Rep Cash Accountability"
   End If
   If Arc32 = False Then
      FrmMissing.Missing.AddItem "Harrassment Policy"
   End If
   If Arc33 = False Then
      FrmMissing.Missing.AddItem "Harrassment Reporting"
   End If
   If Arc40 = False Then
      FrmMissing.Missing.AddItem "Employee File Statement"
   End If
   If Arc55 = False Then
      FrmMissing.Missing.AddItem "BNE Policy & Discipline"
   End If
   If Arc56 = False Then
      FrmMissing.Missing.AddItem "Workbook Completion Form"
   End If
   If Arc57 = False Then
      FrmMissing.Missing.AddItem "Child Support Disclosure"
   End If
   GoTo SkipCode
End If
frmWhilePrinting.hscroll1.Value = 1
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.Label3.Caption = "10 Minutes"
' Page 1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00051.000"
X = Shell(progName, 1)
port1
' page 2
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00052.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 3
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00053.000"
X = Shell(progName, 1)
port1
' Page 4
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00023.000"

X = Shell(progName, 1)
SendKeys "%FRL" + "{ENTER}", True
land1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 5
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00037.000"
X = Shell(progName, 1)
SendKeys "%FRL" + "{ENTER}", True
land1
' Page 7
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00038.000"
X = Shell(progName, 1)
SendKeys "%FRL" + "{ENTER}", True
land1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 9
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00054.000"
X = Shell(progName, 1)
SendKeys "%FRR" + "{ENTER}", True
port1
' Page 11
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00034.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00142.000"
X = Shell(progName, 1)
port1
' Page 12
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00036.000"
X = Shell(progName, 1)
port1
' Page 13
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00032.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 15
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00033.000"
X = Shell(progName, 1)
port1
' Page 16
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00040.000"
X = Shell(progName, 1)
SendKeys "%FRR" + "{ENTER}", True
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
For X = 1 To 10000
     For Y = 1 To 7
         DoEvents
     Next Y
Next X
' Page 17
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00055.000"
X = Shell(progName, 1)
port1
' Page 18
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00056.000"
X = Shell(progName, 1)
port1
' Page 18
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00057.000"
X = Shell(progName, 1)
port1
tempfilename = Dir("C:\FLAGPRNT.*")
   If tempfilename <> "" Then
   Kill "C:\Flagprnt.*"
   End If
End
SkipCode:
End Sub


