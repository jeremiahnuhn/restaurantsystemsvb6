VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Type LARGE_INTEGER
    lowpart As Long
    highpart As Long
End Type
    Dim lResult As Long
    Dim liAvailable As LARGE_INTEGER
    Dim liTotal As LARGE_INTEGER
    Dim liFree As LARGE_INTEGER
    Dim dblAvailable As Double
    Dim dblTotal As Double
    Dim dblFree As Double
    
Private Declare Function GetDiskFreeSpaceEx Lib "kernel32" Alias "GetDiskFreeSpaceExA" (ByVal lpRootPathName As String, lpFreeBytesAvailableToCaller As LARGE_INTEGER, lpTotalNumberOfBytes As LARGE_INTEGER, lpTotalNumberOfFreeBytes As LARGE_INTEGER) As Long

Private Function CLargeInt(Lo As Long, Hi As Long) As Double
    'This function converts the LARGE_INTEGER data type to a double
    Dim dblLo As Double, dblHi As Double
    
    If Lo < 0 Then
        dblLo = 2 ^ 32 + Lo
    Else
        dblLo = Lo
    End If
    
    If Hi < 0 Then
        dblHi = 2 ^ 32 + Hi
    Else
        dblHi = Hi
    End If
    CLargeInt = dblLo + dblHi * 2 ^ 32
End Function



Public Function GetDiskSpace(sDrive As String) As String

    If Right(sDrive, 1) <> "" Then sDrive = sDrive & ""
    'Determine the Available Space, Total Size and Free Space of a drive
    lResult = GetDiskFreeSpaceEx(sDrive, liAvailable, liTotal, liFree)
    
    'Convert the return values from LARGE_INTEGER to doubles
    dblAvailable = CLargeInt(liAvailable.lowpart, liAvailable.highpart)
    dblTotal = CLargeInt(liTotal.lowpart, liTotal.highpart)
    dblFree = CLargeInt(liFree.lowpart, liFree.highpart)
    
    'Display the results
    GetDiskSpace = "Available Space on " & sDrive & ":  " & dblAvailable & " bytes (" & _
                Format(dblAvailable / 1024 ^ 3, "0.00") & " G) " & vbCr & _
                "Total Space on " & sDrive & ":      " & dblTotal & " bytes (" & _
                Format(dblTotal / 1024 ^ 3, "0.00") & " G) " & vbCr & _
                "Free Space on " & sDrive & ":       " & dblFree & " bytes (" & _
                Format(dblFree / 1024 ^ 3, "0.00") & " G) "
End Function

Private Sub Form_Load()

' May 2012 - Added Disk Space Check


On Error Resume Next

GetDiskSpace ("C:")

Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
 Input #1, unitnumber, uname, email, junk
 Input #1, unitnumber, uname, email, junk
Close #1
If Hour(Now()) < 15 Then
     PDATE = Format(Date - 1, "yyyymmdd")
Else
     PDATE = Format(Date, "yyyymmdd")
End If


Open "C:\Program Files\xpient solutions\PSIExporter\Out\Settlement.txt" For Output As #3
Write #3, "BneRefNum", "StoreNum", "BusinessDate", "TotalSpace", "FreeSpace"
    Write #3, "X500", ;
    Write #3, Val(unitnumber), ;
    Write #3, Val(PDATE), ;
    Write #3, Val(Format(dblTotal / 1024 ^ 3, "0.00")), ;
    Write #3, Val(Format(dblFree / 1024 ^ 3, "0.00"))
Close #3



'x = Shell("C:\nodesys\termuse.bat", vbMinimizedFocus)




If Dir("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT") = "" Then
    For x = 1 To 10000000
        DoEvents
    Next x
End If

If Dir("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT") = "" Then
    For x = 1 To 10000000
        DoEvents
    Next x
End If

If Dir("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT") = "" Then
    For x = 1 To 10000000
        DoEvents
    Next x
End If

If Dir("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT") = "" Then
    For x = 1 To 10000000
        DoEvents
    Next x
End If

If Dir("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT") = "" Then
    For x = 1 To 10000000
        DoEvents
    Next x
End If

If Dir("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT") = "" Then
    For x = 1 To 10000000
        DoEvents
    Next x
End If

If Dir("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT") = "" Then
    GoTo Version
End If





Open "C:\Program Files\xpient solutions\credit card application\CCASettleHistory.TXT" For Append As #2
'Open "C:\Program Files\xpient solutions\PSIExporter\Out\Settlement.txt" For Append As #3
Open "C:\Program Files\xpient solutions\PSIExporter\Out\Settlement.txt" For Append As #3
Open "C:\Program Files\xpient solutions\credit card application\CCASettle.TXT" For Input As #1
Write #3, "BneRefNum", "StoreNum", "BusinessDate", "TransactionTime", "HostName", "BatchNumber", "TransSuccess", "TransStatusCode", "HostReponseCode", "Remark", "BatchTotal", "BatchTransCount"
Do While Not EOF(1)
    Input #1, TransactionTime, HostName, BatchNumber, TransSuccess, TransStatusCode, HostReponseCode, Remark, BatchTotal, BatchTransCount
    Print #2, Trim(TransactionTime) + "," + Trim(HostName) + "," + Trim(BatchNumber) + "," + Trim(TransSuccess) + "," + Trim(TransStatusCode) + "," + Trim(HostReponseCode) + "," + Trim(Remark) + "," + Trim(BatchTotal) + "," + Trim(BatchTransCount)
    Write #3, "X100", ;
    Write #3, Val(unitnumber), ;
    Write #3, Val(PDATE), ;
    Write #3, TransactionTime, ;
    Write #3, Mid$(HostName, 10, 4), ;
    Write #3, BatchNumber, ;
    Write #3, TransSuccess, ;
    Write #3, TransStatusCode, ;
    Write #3, HostReponseCode, ;
    Write #3, Remark, ;
    Write #3, BatchTotal, ;
    Write #3, BatchTransCount
Loop
Close #1
Close #2
Close #3
Kill ("C:\Program Files\xpient solutions\credit card application\CCASettle.TXT")
End
Version:
End
End Sub




