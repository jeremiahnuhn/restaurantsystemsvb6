VERSION 5.00
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   2760
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   1440
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
MAPISession1.SignOn
MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
MAPIMessages1.RecipDisplayName = "Jody Smith"
MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Test"
MAPIMessages1.MsgNoteText = "Test Message"
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
MAPISession1.SignOff
End
End Sub
