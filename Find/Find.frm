VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Find Restaurant"
   ClientHeight    =   5955
   ClientLeft      =   240
   ClientTop       =   525
   ClientWidth     =   8010
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   14.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Find.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5955
   ScaleWidth      =   8010
   Begin VB.CommandButton Command1 
      Caption         =   "Find"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2280
      Picture         =   "Find.frx":0442
      TabIndex        =   1
      Top             =   240
      Width           =   855
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3240
      Picture         =   "Find.frx":0884
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   240
      Width           =   975
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Clipboard + CKE #"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6960
      TabIndex        =   14
      Top             =   720
      Width           =   855
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1380
      Left            =   960
      TabIndex        =   10
      Top             =   4200
      Width           =   6375
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6960
      Picture         =   "Find.frx":0CC6
      TabIndex        =   5
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox FindNum 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1935
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4440
      TabIndex        =   12
      Top             =   360
      Width           =   2415
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      Caption         =   "IRIS Server Version"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   13
      Top             =   120
      Width           =   2415
   End
   Begin VB.Label Label6 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   960
      TabIndex        =   11
      Top             =   3840
      Width           =   5895
   End
   Begin VB.Label Label9 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   2280
      Width           =   5055
   End
   Begin VB.Label Label3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   2760
      Width           =   7815
   End
   Begin VB.Label Label8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   8
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   7
      Top             =   1800
      Width           =   2295
   End
   Begin VB.Label Label2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   4935
   End
   Begin VB.Label Label1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   1320
      Width           =   7815
   End
   Begin VB.Label Label4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   3240
      Width           =   7815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
On Error Resume Next
Clipboard.Clear
List1.Clear

Open "S:\find\Directory.TXT" For Input As #1
Dim OfficeNumb As String
Dim Unit As String
Dim RestName As String
Dim CKE As String
Dim TID1 As String
Dim TID2 As String
Dim TID3 As String
Dim TID4 As String


Do While Not EOF(1)
    Input #1, Unit, RestName, SPEEDDIAL, OfficeNumb, VP, Director, DM, CKE, Payroll, Accounting, FE, TID1, TID2, TID3, TID4, ID

    If FindNum = Unit Or FindNum = CKE Then
        Label1.Caption = RestName + " " + "#" + Unit + " " + CKE
        Label2.Caption = OfficeNumb + " Spdial: " + Str(SPEEDDIAL)
        Label4.Caption = "Payroll: " + Payroll + " Accounting: " + Accounting
        Label3.Caption = "VP: " + VP + " D: " + Director
        Label9.Caption = "DM: " + DM
        Label5.Caption = "FE: " + Mid$(FE, 12, 20)
        
TID1 = "No File"
TID2 = "No File"
TID3 = "No File"
TID4 = "No File"
        
        If Dir("V:\poll\HeartSIP\" + Unit + "-S.ver") <> "" Then
        Open "V:\poll\HeartSIP\" + Unit + "-S.ver" For Input As #2
             Line Input #2, IndividualLine2
             FileInput = Split(IndividualLine2, ",")
             IRISSVer = FileInput(5)
            Close #2
        End If
        
        If Dir("V:\poll\HeartSIP\" + Unit + "-T1.ver") <> "" Then
        Open "V:\poll\HeartSIP\" + Unit + "-T1.ver" For Input As #2
             Line Input #2, IndividualLine2
             FileInput = Split(IndividualLine2, ",")
             TID1 = FileInput(2)
             TIDVer1 = Mid$(FileInput(4), 1, 6)
             POSVer1 = FileInput(5)
            Close #2
        End If
        
        If Dir("V:\poll\HeartSIP\" + Unit + "-T2.ver") <> "" Then
        Open "V:\poll\HeartSIP\" + Unit + "-T2.ver" For Input As #2
             Line Input #2, IndividualLine2
             FileInput = Split(IndividualLine2, ",")
             TID2 = FileInput(2)
             TIDVer2 = Mid$(FileInput(4), 1, 6)
             POSVer2 = FileInput(5)
            Close #2
        End If
        
        If Dir("V:\poll\HeartSIP\" + Unit + "-T3.ver") <> "" Then
        Open "V:\poll\HeartSIP\" + Unit + "-T3.ver" For Input As #2
             Line Input #2, IndividualLine2
             FileInput = Split(IndividualLine2, ",")
             TID3 = FileInput(2)
             TIDVer3 = Mid$(FileInput(4), 1, 6)
             POSVer3 = FileInput(5)
            Close #2
        End If
        
        If Dir("V:\poll\HeartSIP\" + Unit + "-T4.ver") <> "" Then
        Open "V:\poll\HeartSIP\" + Unit + "-T4.ver" For Input As #2
             Line Input #2, IndividualLine2
             FileInput = Split(IndividualLine2, ",")
             TID4 = FileInput(2)
             TIDVer4 = Mid$(FileInput(4), 1, 6)
             POSVer4 = FileInput(5)
            Close #2
        End If
        If TID4 = "T4" Then TID4 = "     "
        If TIDVer4 = "T4" Then TIDVer4 = "        "
        
        If Dir("V:\poll\HeartSIP\" + Unit + "-T5.ver") <> "" Then
        Open "V:\poll\HeartSIP\" + Unit + "-T5.ver" For Input As #2
             Line Input #2, IndividualLine2
             FileInput = Split(IndividualLine2, ",")
             TID5 = FileInput(2)
             TIDVer5 = Mid$(FileInput(4), 1, 6)
             POSVer5 = FileInput(5)
            Close #2
        End If
        If TID5 = "" Then TID5 = "No Terminal"
        If VP = "Johnny Ramsey" Then Region = "Region 2"
        If VP = "Dwight Searson" Then Region = "Region 3"
        If VP = "Chuck Burke" Then Region = "Region 4"
        If VP = "Andre Jumpp" Then Region = "Region 1"
        Label8.Caption = Region
        
        'Select Case POSVer1
        POSVerA1 = POSVer1
        Open "s:\IRIS\Version\IRISVersion.csv" For Input As 8
            Do While Not EOF(8)
                Input #8, IDate, IVersion
                CheckDate = Mid$(IDate, 1, 2) + "/" + Mid$(IDate, 3, 2) + "/" + Mid$(IDate, 5, 4)
                If POSVer1 = CheckDate Then
                    POSVerA1 = IVersion
                End If
            Loop
            Close (8)
        
        'Select Case POSVer2
        POSVerA2 = POSVer2
        Open "s:\IRIS\Version\IRISVersion.csv" For Input As 8
            Do While Not EOF(8)
                Input #8, IDate, IVersion
                CheckDate = Mid$(IDate, 1, 2) + "/" + Mid$(IDate, 3, 2) + "/" + Mid$(IDate, 5, 4)
                If POSVer2 = CheckDate Then
                    POSVerA2 = IVersion
                End If
            Loop
            Close (8)
        
        'Select Case POSVer3
        POSVerA3 = POSVer3
        Open "s:\IRIS\Version\IRISVersion.csv" For Input As 8
            Do While Not EOF(8)
                Input #8, IDate, IVersion
                CheckDate = Mid$(IDate, 1, 2) + "/" + Mid$(IDate, 3, 2) + "/" + Mid$(IDate, 5, 4)
                
                If POSVer3 = CheckDate Then
                    POSVerA3 = IVersion
                End If
            Loop
            Close (8)
        
        'Select Case POSVer4
        POSVerA4 = POSVer4
        Open "s:\IRIS\Version\IRISVersion.csv" For Input As 8
            Do While Not EOF(8)
                Input #8, IDate, IVersion
                CheckDate = Mid$(IDate, 1, 2) + "/" + Mid$(IDate, 3, 2) + "/" + Mid$(IDate, 5, 4)
                If POSVer4 = CheckDate Then
                    POSVerA4 = IVersion
                End If
            Loop
            Close (8)
        
        'Select Case POSVer5
        POSVerA5 = POSVer5
        Open "s:\IRIS\Version\IRISVersion.csv" For Input As 8
            Do While Not EOF(8)
                Input #8, IDate, IVersion
                 CheckDate = Mid$(IDate, 1, 2) + "/" + Mid$(IDate, 3, 2) + "/" + Mid$(IDate, 5, 4)
               
                If POSVer5 = CheckDate Then
                    POSVerA5 = IVersion
                End If
            Loop
            Close (8)
        
        'Select Case IRISSVer
        Form1.Label7.Caption = IRISSVer
        Open "s:\IRIS\Version\IRISVersion.csv" For Input As 8
            Do While Not EOF(8)
                Input #8, IDate, IVersion
                                CheckDate = Mid$(IDate, 1, 2) + "/" + Mid$(IDate, 3, 2) + "/" + Mid$(IDate, 5, 4)

                If IRISSVer = CheckDate Then
                    Form1.Label7.Caption = IVersion
                End If
            Loop
            Close (8)
        
        

        
       ' If POSVer5 = "05/17/2019" Then POSVerA5 = "4.1.551.300" Else POSVerA5 = POSVer5
       ' If POSVer4 = "05/17/2019" Then POSVerA4 = "4.1.551.300" Else POSVerA4 = POSVer4
       ' If POSVer3 = "05/17/2019" Then POSVerA3 = "4.1.551.300" Else POSVerA3 = POSVer3
       ' If POSVer2 = "05/17/2019" Then POSVerA2 = "4.1.551.300" Else POSVerA2 = POSVer2
       ' If POSVer1 = "05/17/2019" Then POSVerA1 = "4.1.551.300" Else POSVerA1 = POSVer1
       ' If IRISSVer = "05/17/2019" Then Label7.Caption = "4.1.551.300" Else Label7.Caption = IRISSVer
        
       ' If POSVer5 = "09/24/2019" Then POSVerA5 = "4.1.610.600" Else POSVerA5 = POSVer5
       ' If POSVer4 = "09/24/2019" Then POSVerA4 = "4.1.610.600" Else POSVerA4 = POSVer4
       ' If POSVer3 = "09/24/2019" Then POSVerA3 = "4.1.610.600" Else POSVerA3 = POSVer3
       ' If POSVer2 = "09/24/2019" Then POSVerA2 = "4.1.610.600" Else POSVerA2 = POSVer2
       ' If POSVer1 = "09/24/2019" Then POSVerA1 = "4.1.610.600" Else POSVerA1 = POSVer1
       ' If IRISSVer = "09/24/2019" Then Label7.Caption = "4.1.610.600" Else Label7.Caption = IRISSVer
        
        
        
        List1.AddItem " 1  " + TID1 + "    " + TIDVer1 + "     " + POSVerA1
        List1.AddItem " 2  " + TID2 + "    " + TIDVer2 + "     " + POSVerA2
        List1.AddItem " 3  " + TID3 + "    " + TIDVer3 + "     " + POSVerA3
        List1.AddItem " 4  " + TID4 + "    " + TIDVer4 + "      " + POSVerA4
        List1.AddItem " 5  " + TID5 + "    " + TIDVer5 + "     " + POSVerA5
        Label6 = " T#   TID      HeartSIP       IRIS"
        'Clipboard.SetText RestName + " #" + Unit + Chr(13)
        Clipboard.SetText RestName + " #" + Unit + " "
        ClipCKE = RestName + " #" + Unit + " (" + CKE + ")"
        Form1.FindNum.Text = ""
        Form1.FindNum.SetFocus
        'Form1.Command1.SetFocus
        'Command4.Enabled = True
        Close #1

    Exit Sub

    End If




Loop
Close #1
        X = MsgBox(FindNum.Text + " not found!", vbInformation, "Try again (Version 3)")
        Form1.FindNum.Text = ""
        Form1.Command1.SetFocus
        'Command4.Enabled = True

End Sub

Private Sub Command2_Click()
End
End Sub

Private Sub Command3_Click()
        Clipboard.Clear
        Clipboard.SetText Label1.Caption
        FindNum.Text = ""
        FindNum.SetFocus
End Sub

Private Sub Command4_Click()
    FindName.Show
    Form1.Hide
    
End Sub

Private Sub FindNum_Change()
'    Command4.Enabled = False

End Sub

