VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Find Restaurant"
   ClientHeight    =   3735
   ClientLeft      =   240
   ClientTop       =   525
   ClientWidth     =   7950
   Icon            =   "Find.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3735
   ScaleWidth      =   7950
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6960
      Picture         =   "Find.frx":0442
      TabIndex        =   5
      Top             =   120
      Width           =   855
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Find"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2280
      Picture         =   "Find.frx":0884
      TabIndex        =   1
      Top             =   240
      Width           =   855
   End
   Begin VB.TextBox FindNum 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1935
   End
   Begin VB.Label Label9 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   2280
      Width           =   5055
   End
   Begin VB.Label Label3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   2760
      Width           =   7815
   End
   Begin VB.Label Label8 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   10
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   4200
      Width           =   7815
   End
   Begin VB.Label Label5 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   7
      Top             =   1800
      Width           =   2295
   End
   Begin VB.Label Label2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1800
      Width           =   4935
   End
   Begin VB.Label Label1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   1320
      Width           =   7815
   End
   Begin VB.Label Label6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   3720
      Width           =   7815
   End
   Begin VB.Label Label4 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   3240
      Width           =   7815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Clipboard.Clear

Open "S:\Find\Directory.TXT" For Input As #1
Dim OfficeNumb As String
Dim Unit As String
Dim RestName As String
Dim CKE As String

Do While Not EOF(1)
    Input #1, Unit, RestName, SPEEDDIAL, OfficeNumb, VP, Director, DM, CKE, Payroll, Accounting, FE, TID1, TID2, TID3, TID4, ID

    If FindNum = Unit Or FindNum = CKE Then
        Label1.Caption = RestName + " " + "#" + Unit + " " + CKE
        Label2.Caption = OfficeNumb + " Spdial: " + Str(SPEEDDIAL)
        Label4.Caption = "Payroll: " + UCase(Payroll) + " Accounting: " + Accounting
        Label3.Caption = "VP: " + VP + " D: " + Director
        Label9.Caption = "DM: " + DM


        Label5.Caption = "FE: " + Mid$(FE, 12, 20)
        Label6.Caption = "TID 1: " + Str(TID1) + " TID 2: " + Str(TID2)
        Label7.Caption = "TID 3: " + Str(TID3) + " TID 4: " + Str(TID4)
        If VP = "REG 2 INT VP" Then Region = "Region 2"
        If VP = "JOHN SMITH" Then Region = "Region 3"
        If VP = "CHUCK BURKE" Then Region = "Region 4"
        If VP = "ANDRE JUMPP" Then Region = "Region 1"
        Label8.Caption = Region
        
        
        
        'Clipboard.SetText RestName + " #" + Unit + Chr(13)
        Clipboard.SetText RestName + " #" + Unit + " "
        FindNum.Text = ""
        FindNum.SetFocus
        Close #1

    Exit Sub

    End If




Loop
Close #1
        X = MsgBox(FindNum.Text + " not found!", vbInformation, "Try again (Version 2)")
        FindNum.Text = ""
        FindNum.SetFocus

End Sub

Private Sub Command2_Click()
End
End Sub

