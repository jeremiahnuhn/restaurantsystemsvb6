VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "LiveReporter (BNE)"
   ClientHeight    =   9525
   ClientLeft      =   240
   ClientTop       =   345
   ClientWidth     =   13275
   LinkTopic       =   "Form1"
   ScaleHeight     =   9525
   ScaleWidth      =   13275
   Begin VB.CommandButton Command2 
      Caption         =   "Exit Search"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   8640
      TabIndex        =   2
      Top             =   360
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Search Tips"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   6360
      TabIndex        =   1
      Top             =   360
      Width           =   1935
   End
   Begin VB.CommandButton Command5 
      Caption         =   "New Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   11280
      TabIndex        =   12
      Top             =   1560
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Close Viewer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   10920
      TabIndex        =   4
      Top             =   360
      Width           =   1935
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4080
      TabIndex        =   0
      Top             =   360
      Width           =   1935
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   7
      Top             =   480
      Width           =   3495
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7620
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Visible         =   0   'False
      Width           =   12975
   End
   Begin VB.DirListBox Dir1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7590
      Left            =   120
      TabIndex        =   3
      Top             =   1680
      Width           =   3615
   End
   Begin VB.FileListBox File1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7650
      Left            =   3960
      TabIndex        =   5
      Top             =   1680
      Width           =   9135
   End
   Begin VB.Label Label4 
      Caption         =   "Double Click to change folder."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   1320
      Width           =   3735
   End
   Begin VB.Label Label3 
      Caption         =   "Click to view file."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4080
      TabIndex        =   10
      Top             =   1320
      Width           =   2655
   End
   Begin VB.Label Label2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   9
      Top             =   1200
      Visible         =   0   'False
      Width           =   11295
   End
   Begin VB.Label Label1 
      Caption         =   "Type in your search word"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   0
      Width           =   3855
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False





Private Sub Command1_Click()
    File1.Visible = False
    Label3.Visible = False
    Label4.Visible = False
    Command3.Visible = True
    Text1.Visible = True
    Label1.Visible = True
    List1.Visible = True
    Label2.Visible = True
    Command4.Visible = True
    Text1.SetFocus
 
    
    
    List1.Clear
    List1.AddItem "Search Tips"
    List1.AddItem " "
    List1.AddItem "Enter worksheet for the Daily Cash Worksheet"
    List1.AddItem " "
    List1.AddItem "For Period 8 reports enter _08 (Use underscore _)"
    List1.AddItem " "
    List1.AddItem " "
    List1.AddItem " "
    List1.AddItem " "
    List1.AddItem " "
    List1.AddItem " "
    List1.AddItem " "
    
End Sub

Private Sub Command2_Click()


    File1.Visible = True
    Dir1.Visible = True
    Label3.Visible = True
    Label4.Visible = True
    List1.Visible = False
    Label2.Visible = False

 
    
    
End Sub

Private Sub Command3_Click()
Dir1.Visible = False
    File1.Visible = False
    Label3.Visible = False
    Label4.Visible = False
    Command3.Visible = True
    Text1.Visible = True
    Label1.Visible = True
    List1.Visible = True
    Label2.Visible = True
    Command4.Visible = True
    Text1.SetFocus
 
    
    
    List1.Clear
    Label2.Caption = "Search results for " + Text1.Text + ".  Click to view file."

    
    Dim colFiles As New Collection
    SearchVar = "*" + RTrim(LTrim(Text1.Text)) + "*.*"
    RecursiveDir colFiles, "C:\Reports", "*" + RTrim(LTrim(Text1.Text)) + "*.*", True

    Dim vFile As Variant
    For Each vFile In colFiles
        sFile = Mid$(vFile, 12, 200)
        List1.AddItem sFile
    Next vFile
        Text1.Text = ""
        Text1.SetFocus
End Sub

Private Sub Command4_Click()
    End
End Sub

Private Sub Command5_Click()
Dir1.Visible = False
    File1.Visible = False
    Label3.Visible = False
    Label4.Visible = False
    Command3.Visible = True
    Text1.Visible = True
    Label1.Visible = True
    List1.Visible = True
    Label2.Visible = True
    Command4.Visible = True
    Text1.SetFocus
 
    
    
    List1.Clear
    Label2.Caption = "View files created in the last five days."

    
    Dim colFiles As New Collection

    RecursiveDir colFiles, "C:\Reports", "*" + RTrim(LTrim(Text1.Text)) + "*.*", True

    Dim vFile As Variant
    For Each vFile In colFiles
        sFile = Mid$(vFile, 12, 200)
        List1.AddItem sFile
    Next vFile
        Text1.Text = ""
        Text1.SetFocus
End Sub

Private Sub Dir1_Change()

    File1.Path = Dir1.Path
End Sub




Private Sub File1_Click()
Set wShell = CreateObject("Shell.Application")

If LCase(Mid$(File1.Path, 4, 7)) = "reports" Then
    wShell.Open File1.Path & "\" & File1.FileName
Else
x = MsgBox("You are not allowed to open this file.", vbCritical, "Access Denied")
End If
End Sub

Private Sub Form_Load()
On Error Resume Next
ChDir ("c:\reports")
File1.Path = "C:\Reports"
End Sub


Public Function RecursiveDir(colFiles As Collection, _
                             strFolder As String, _
                             strFileSpec As String, _
                             bIncludeSubfolders As Boolean)
    On Error Resume Next
    Dim strTemp As String
    Dim colFolders As New Collection
    Dim vFolderName As Variant

    'Add files in strFolder matching strFileSpec to colFiles
    strFolder = TrailingSlash(strFolder)
    strTemp = Dir(strFolder & strFileSpec)
    Do While strTemp <> vbNullString
        colFiles.Add strFolder & strTemp
        strTemp = Dir
    Loop

    If bIncludeSubfolders Then
        'Fill colFolders with list of subdirectories of strFolder
        strTemp = Dir(strFolder, vbDirectory)
        Do While strTemp <> vbNullString
            If (strTemp <> ".") And (strTemp <> "..") Then
                If (GetAttr(strFolder & strTemp) And vbDirectory) <> 0 Then
                    colFolders.Add strTemp
                End If
            End If
            strTemp = Dir
        Loop

        'Call RecursiveDir for each subfolder in colFolders
        For Each vFolderName In colFolders
            Call RecursiveDir(colFiles, strFolder & vFolderName, strFileSpec, True)
        Next vFolderName
    End If

End Function


Public Function TrailingSlash(strFolder As String) As String
    If Len(strFolder) > 0 Then
        If Right(strFolder, 1) = "\" Then
            TrailingSlash = strFolder
        Else
            TrailingSlash = strFolder & "\"
        End If
    End If
End Function

Private Sub List1_Click()
Set wShell = CreateObject("Shell.Application")

If LCase(Mid$(List1.Text, 1, 20)) <> "daily reports\secure" Then
    wShell.Open "c:\reports\" + List1.Text
Else
x = MsgBox("You are not allowed to open this file.", vbCritical, "Access Denied")
End If
End Sub


