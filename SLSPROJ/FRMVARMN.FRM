VERSION 4.00
Begin VB.Form frmVarMnt2 
   BackColor       =   &H00C0FFFF&
   Caption         =   "Variance Table Maintenance"
   ClientHeight    =   6510
   ClientLeft      =   1365
   ClientTop       =   1095
   ClientWidth     =   9195
   ControlBox      =   0   'False
   Height          =   6915
   Left            =   1305
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6510
   ScaleWidth      =   9195
   Top             =   750
   Width           =   9315
   Begin VB.CommandButton CmdEnterData 
      Caption         =   "Enter Data"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3480
      TabIndex        =   19
      Top             =   6000
      Width           =   3135
   End
   Begin VB.TextBox txtVarParm 
      Height          =   315
      Left            =   7620
      TabIndex        =   16
      Top             =   4080
      Width           =   1215
   End
   Begin VB.ComboBox cmbVarParm 
      Height          =   315
      Index           =   4
      Left            =   7680
      TabIndex        =   15
      Text            =   "Combo1"
      Top             =   3240
      Width           =   1095
   End
   Begin VB.ComboBox cmbVarParm 
      Height          =   315
      Index           =   3
      Left            =   7620
      TabIndex        =   14
      Text            =   "Combo1"
      Top             =   2520
      Width           =   1095
   End
   Begin VB.ComboBox cmbVarParm 
      Height          =   315
      Index           =   2
      Left            =   5400
      TabIndex        =   13
      Text            =   "Combo1"
      Top             =   4080
      Width           =   1815
   End
   Begin VB.ComboBox cmbVarParm 
      Height          =   315
      Index           =   1
      Left            =   5400
      TabIndex        =   12
      Text            =   "Combo1"
      Top             =   3240
      Width           =   1815
   End
   Begin VB.ComboBox cmbVarParm 
      Height          =   315
      Index           =   0
      Left            =   5400
      TabIndex        =   11
      Text            =   "Combo1"
      Top             =   2520
      Width           =   1815
   End
   Begin VB.PictureBox SSPanel1 
      BackColor       =   &H00800000&
      Height          =   1335
      Left            =   5340
      ScaleHeight     =   1275
      ScaleWidth      =   3435
      TabIndex        =   10
      Top             =   4560
      Width           =   3495
      Begin VB.CommandButton Command2 
         Caption         =   "Cancel"
         Height          =   435
         Index           =   3
         Left            =   240
         TabIndex        =   3
         Top             =   660
         Width           =   3015
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Update"
         Height          =   435
         Index           =   2
         Left            =   1800
         TabIndex        =   2
         Top             =   180
         Width           =   1455
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Delete"
         Height          =   435
         Index           =   1
         Left            =   240
         TabIndex        =   1
         Top             =   180
         Width           =   1455
      End
   End
   Begin VB.Label Label3 
      BackColor       =   &H00C0FFFF&
      Caption         =   $"FRMVARMN.frx":0000
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   18
      Top             =   120
      Width           =   8895
   End
   Begin VB.Label Label2 
      BackColor       =   &H00C0FFFF&
      Caption         =   "(Use numbers only)"
      Height          =   255
      Left            =   7560
      TabIndex        =   17
      Top             =   3840
      Width           =   1455
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date:"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   0
      Left            =   5400
      TabIndex        =   9
      Top             =   2160
      Width           =   675
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Hour:"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   1
      Left            =   5400
      TabIndex        =   8
      Top             =   2880
      Width           =   675
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Duration:"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   2
      Left            =   5400
      TabIndex        =   7
      Top             =   3720
      Width           =   1125
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "$ or % :"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   3
      Left            =   7620
      TabIndex        =   6
      Top             =   2160
      Width           =   930
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "+ or - :"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   4
      Left            =   7620
      TabIndex        =   5
      Top             =   2880
      Width           =   795
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Amount: "
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   7680
      TabIndex        =   4
      Top             =   3600
      Width           =   1095
   End
   Begin MSGrid.Grid Grid1 
      Height          =   3795
      Left            =   420
      TabIndex        =   0
      Top             =   2040
      Width           =   4695
      _version        =   65536
      _extentx        =   8281
      _extenty        =   6694
      _stockprops     =   77
      backcolor       =   16777215
      rows            =   50
      cols            =   7
      mouseicon       =   "FRMVARMN.frx":0196
   End
End
Attribute VB_Name = "frmVarMnt2"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Option Explicit

Dim mbAddFlag As Boolean 'flag: update Tbls.Mdb:Vari table
Dim mbUpdFlag As Boolean 'flag: update Tble.Mdb:Vari


Public Sub UpdVar()
'update a Variance Record
On Error GoTo UpdVarError
Dim sTemp As String, dtTemp As Date
Dim nTemp As Integer

dtTemp = CDate(cmbVarParm(0))
sTemp = cmbVarParm(1)
Select Case sTemp
    Case "00-01": sTemp = 0: Case "12-13": sTemp = 12
    Case "01-02": sTemp = 1: Case "13-14": sTemp = 13
    Case "02-03": sTemp = 2: Case "14-15": sTemp = 14
    Case "03-04": sTemp = 3: Case "15-16": sTemp = 15
    Case "04-05": sTemp = 4: Case "16-17": sTemp = 16
    Case "05-06": sTemp = 5: Case "17-18": sTemp = 17
    Case "06-07": sTemp = 6: Case "18-19": sTemp = 18
    Case "07-08": sTemp = 7: Case "19-20": sTemp = 19
    Case "08-09": sTemp = 8: Case "20-21": sTemp = 20
    Case "09-10": sTemp = 9: Case "21-22": sTemp = 21
    Case "10-11": sTemp = 10: Case "22-23": sTemp = 22
    Case "11-12": sTemp = 11: Case "23-24": sTemp = 23
End Select
grsVar.FindFirst "dt = #" & dtTemp & "# AND hr = " & sTemp

If grsVar.NoMatch = True Then
    grsVar.AddNew
Else
    grsVar.Edit
End If
grsVar!dt = CDate(cmbVarParm(0))
grsVar!hr = sTemp
sTemp = cmbVarParm(2)
Select Case sTemp
    Case "Day": grsVar!duration = "D"
    Case "Hour": grsVar!duration = "H"
    Case "Week": grsVar!duration = "W"
End Select
Select Case cmbVarParm(3)
    Case "$": cmbVarParm(3) = cmbVarParm(3)
    Case "%": cmbVarParm(3) = cmbVarParm(3)
    Case Else: MsgBox "Please enter $ or %": cmbVarParm(3).Clear: cmbVarParm(3) = "$": Exit Sub
End Select
grsVar!Type = cmbVarParm(3)
Select Case cmbVarParm(4)
    Case "+": cmbVarParm(4) = cmbVarParm(4)
    Case "-": cmbVarParm(4) = cmbVarParm(4)
    Case Else: MsgBox "Please enter + or -": cmbVarParm(4).Clear: cmbVarParm(4) = "+": Exit Sub
End Select
grsVar!IncDec = cmbVarParm(4)
grsVar!amt = Str(txtVarParm)
grsVar.Update
ExitUpdVar:
    Exit Sub

UpdVarError:
    If Err = 3021 Then Resume ExitUpdVar
    MsgBox "err = " & Str(Err) & " " & Error(Err) & vbCrLf & vbCrLf _
    & "Please delete last entry into varance table and reenter" & vbCrLf _
    & "If further assistance is needed call help desk as 252-937-2800 ext 1767"
    Resume Next

End Sub

Public Sub DispVar()
Dim sTemp As String
Dim nRecCnt As Integer

If grsVar.EOF Or grsVar.BOF Then
    Exit Sub
End If

cmbVarParm(0) = Format(CDate(grsVar!dt), "m-d-yyyy")
Select Case grsVar!hr
    Case 0: sTemp = "00-01": Case 12: sTemp = "12-13"
    Case 1: sTemp = "01-02": Case 13: sTemp = "13-14"
    Case 2: sTemp = "02-03": Case 14: sTemp = "14-15"
    Case 3: sTemp = "03-04": Case 15: sTemp = "15-16"
    Case 4: sTemp = "04-05": Case 16: sTemp = "16-17"
    Case 5: sTemp = "05-06": Case 17: sTemp = "17-18"
    Case 6: sTemp = "06-07": Case 18: sTemp = "18-19"
    Case 7: sTemp = "07-08": Case 19: sTemp = "19-20"
    Case 8: sTemp = "08-09": Case 20: sTemp = "20-21"
    Case 9: sTemp = "09-10": Case 21: sTemp = "21-22"
    Case 10: sTemp = "10-11": Case 22: sTemp = "22-23"
    Case 11: sTemp = "11-12": Case 23: sTemp = "23-24"
End Select
cmbVarParm(1) = sTemp
Select Case grsVar!duration
    Case "W": sTemp = "Week"
    Case "D": sTemp = "Day"
    Case "H": sTemp = "Hour"
End Select
cmbVarParm(2) = sTemp
cmbVarParm(3) = grsVar!Type
cmbVarParm(4) = grsVar!IncDec
If grsVar!amt <> "" Then
   txtVarParm = grsVar!amt
Else
   txtVarParm = 0
End If


End Sub




Private Sub CmdBack_Click()
FrmHelp.Show
End Sub

Private Sub Cmdback3_Click(Index As Integer)
frmVarMnt2.Hide
Unload frmVarMnt2
frmWeightMaint.Show
End Sub

Private Sub CmdEnterData_Click()
Dim nSched As Integer
Dim ni As Integer, nJ As Integer, sMsg As String
frmVarMnt2.Hide
Unload frmVarMnt2
ReCalcSales
CalcSales
FrmWeeklyForecast.Show
End Sub

Private Sub Command1_Click(Index As Integer)
 On Error GoTo CmdClickError
'            grsVar.MoveFirst
            On Error GoTo 0
'            UpdVar
            
Dim nSched As Integer
Dim ni As Integer, nJ As Integer, sMsg As String
Select Case Index
    Case 0: 'Back
        Unload frmCurrProj
        frmVarMnt2.Show 1
    Case 1: 'Finished
        Unload frmCurrProj
        FrmCalculating.Show
        DoEvents
        frmCurrProj.Show
End Select
            
            
            
            
            
            
frmVarMnt2.Hide
Exit Sub
CmdClickError:
    MsgBox "Update Variance Record Module:" & vbCrLf _
    & "err = " & Str(Err) & " " & Error(Err)
   Resume Next

End Sub

Private Sub Command2_Click(Index As Integer)
On Error GoTo Command2_ClickError

Dim sTemp As String, ni As Integer

Select Case Index
    Case 1 'delete
        If MsgBox("Do you really want to delete?" & vbCrLf _
            & "the Variance Record for " _
            & cmbVarParm(0), vbYesNo) = vbYes Then
            
            sTemp = cmbVarParm(1)
            Select Case sTemp
                Case "00-01": sTemp = 0: Case "12-13": sTemp = 12
                Case "01-02": sTemp = 1: Case "13-14": sTemp = 13
                Case "02-03": sTemp = 2: Case "14-15": sTemp = 14
                Case "03-04": sTemp = 3: Case "15-16": sTemp = 15
                Case "04-05": sTemp = 4: Case "16-17": sTemp = 16
                Case "05-06": sTemp = 5: Case "17-18": sTemp = 17
                Case "06-07": sTemp = 6: Case "18-19": sTemp = 18
                Case "07-08": sTemp = 7: Case "19-20": sTemp = 19
                Case "08-09": sTemp = 8: Case "20-21": sTemp = 20
                Case "09-10": sTemp = 9: Case "21-22": sTemp = 21
                Case "10-11": sTemp = 10: Case "22-23": sTemp = 22
                Case "11-12": sTemp = 11: Case "23-24": sTemp = 23
            End Select
            grsVar.FindFirst "dt = #" & cmbVarParm(0) & "# AND hr = " & sTemp
            grsVar.Delete
            grsVar.MoveFirst
    
            
            Grid1.Col = 1: Grid1.Text = ""
            Grid1.Col = 2: Grid1.Text = ""
            Grid1.Col = 3: Grid1.Text = ""
            Grid1.Col = 4: Grid1.Text = ""
            Grid1.Col = 5: Grid1.Text = ""
            Grid1.Col = 6: Grid1.Text = ""
            
            Grid1.Row = 1
            ClearGrid1
            DispVar
            DispGrid
        End If
        mbAddFlag = False: mbUpdFlag = False
    Case 2 'update
        UpdVar
        DispVar
        DispGrid
    Case 3 'Cancel
        For ni = 0 To 4: cmbVarParm(ni) = "": Next ni
        txtVarParm = ""
   '     grsVar.MoveFirst
        DispVar
        DispGrid
End Select

ExitCommand2_Click:
    Exit Sub
    
Command2_ClickError:
    If Err = 3021 Then
        Resume Next
    Else
       MsgBox "An error has occured. Please reenter data. If problem persists" & Chr(10) & Chr(13) _
               & "call Help Desk at 252-937-2800, ext 1767 for assistance"
    End If

End Sub

Private Sub Form_Load()
Dim ni As Integer, I As Integer
On Error GoTo LoadError

frmVarMnt2.Top = (Screen.Height - frmVarMnt2.Height) / 4
frmVarMnt2.Left = (Screen.Width - frmVarMnt2.Width) / 2
If SecondWeek = True Then
   gdtLabSchedDate = SecondWeekDate
Else
   gdtLabSchedDate = FirstWeekDate
End If
frmVarMnt2.Caption = "Edit Sales Projection: Variance Table Maintenance"

Dim RecCnt As Integer
Dim sTemp As String
Grid1.Row = 0
Grid1.Col = 0: Grid1.ColWidth(0) = 350
Grid1.Col = 1: Grid1.ColWidth(1) = 1000: 'Grid1.Text = "Date"
Grid1.Col = 2: Grid1.ColWidth(2) = 500: 'Grid1.Text = "Hour"
Grid1.Col = 3: Grid1.ColWidth(3) = 800: 'Grid1.Text = "Duration"
Grid1.Col = 4: Grid1.ColWidth(4) = 500: 'Grid1.Text = "Type"
Grid1.Col = 5: Grid1.ColWidth(5) = 550: 'Grid1.Text = "IncDec"
Grid1.Col = 6: Grid1.ColWidth(6) = 650: 'Grid1.Text = "Amt"

'available dates
For ni = 0 To 6
    cmbVarParm(0).AddItem Format(gdtLabSchedDate + ni, "m-d-yyyy")
Next ni
cmbVarParm(0) = Format(gdtLabSchedDate, "m-d-yyyy")

'grsVar.MoveFirst
'Do Until grsVar.EOF
'   If grsvat!dt < Format(gdtLabSchedDate - 14, "m-d-yyyy") Then
'      grsVar.Delete
'   End If
'   grsVar.MoveNext
'Loop


'available times
For I = 0 To 23
    sTemp = Format(I, "0#-")
    sTemp = sTemp + Format(I + 1, "0#")
    cmbVarParm(1).AddItem sTemp
Next I
cmbVarParm(1) = "00-01"

'available durations
cmbVarParm(2).AddItem "Hour"
cmbVarParm(2).AddItem "Day"
cmbVarParm(2).AddItem "Week"
cmbVarParm(2) = "Hour"

'$ or%
cmbVarParm(3).AddItem "$"
cmbVarParm(3).AddItem "%"
cmbVarParm(3) = "$"

'+ or -
cmbVarParm(4).AddItem "+"
cmbVarParm(4).AddItem "-"
cmbVarParm(4) = "+"

txtVarParm = 0

grsVar.MoveFirst
DispVar
DispGrid
ExitLoad:
    Exit Sub
    
LoadError:
    If Err = 3021 Then
        Resume ExitLoad
    Else
        MsgBox "err=" + Str(Err) + " " + Error(Err)
    End If

End Sub


Private Sub Grid1_Click()
Dim sTemp As String, dtTemp As Date

Grid1.Col = 1
If Grid1.Text = "" Then
    Exit Sub
Else
    dtTemp = Grid1.Text
End If

Grid1.Col = 2
Select Case Grid1.Text
    Case "00-01": sTemp = 0: Case "12-13": sTemp = 12
    Case "01-02": sTemp = 1: Case "13-14": sTemp = 13
    Case "02-03": sTemp = 2: Case "14-15": sTemp = 14
    Case "03-04": sTemp = 3: Case "15-16": sTemp = 15
    Case "04-05": sTemp = 4: Case "16-17": sTemp = 16
    Case "05-06": sTemp = 5: Case "17-18": sTemp = 17
    Case "06-07": sTemp = 6: Case "18-19": sTemp = 18
    Case "07-08": sTemp = 7: Case "19-20": sTemp = 19
    Case "08-09": sTemp = 8: Case "20-21": sTemp = 20
    Case "09-10": sTemp = 9: Case "21-22": sTemp = 21
    Case "10-11": sTemp = 10: Case "22-23": sTemp = 22
    Case "11-12": sTemp = 11: Case "23-24": sTemp = 23
End Select

grsVar.FindFirst "dt = #" & dtTemp & "# AND hr = " & sTemp
DispVar

End Sub


Public Sub DispGrid()
On Error GoTo DispGridError
Dim nRecCnt As Integer, sTemp As String

grsVar.MoveFirst
While grsVar.EOF = False
    nRecCnt = nRecCnt + 1
    Grid1.Row = nRecCnt
    Grid1.Col = 0: Grid1.Text = nRecCnt
    Grid1.Col = 1: Grid1.Text = Format(grsVar!dt, "m-d-yyyy")
    
    Select Case grsVar!hr
        Case 0: sTemp = "00-01": Case 12: sTemp = "12-13"
        Case 1: sTemp = "01-02": Case 13: sTemp = "13-14"
        Case 2: sTemp = "02-03": Case 14: sTemp = "14-15"
        Case 3: sTemp = "03-04": Case 15: sTemp = "15-16"
        Case 4: sTemp = "04-05": Case 16: sTemp = "16-17"
        Case 5: sTemp = "05-06": Case 17: sTemp = "17-18"
        Case 6: sTemp = "06-07": Case 18: sTemp = "18-19"
        Case 7: sTemp = "07-08": Case 19: sTemp = "19-20"
        Case 8: sTemp = "08-09": Case 20: sTemp = "20-21"
        Case 9: sTemp = "09-10": Case 21: sTemp = "21-22"
        Case 10: sTemp = "10-11": Case 22: sTemp = "22-23"
        Case 11: sTemp = "11-12": Case 23: sTemp = "23-24"
    End Select
    Grid1.Col = 2: Grid1.Text = sTemp
    
    Select Case grsVar!duration
        Case "W": sTemp = "Week"
        Case "D": sTemp = "Day"
        Case "H": sTemp = "Hour"
    End Select
    Grid1.Col = 3: Grid1.Text = sTemp
    
    Grid1.Col = 4: Grid1.Text = grsVar!Type
    Grid1.Col = 5: Grid1.Text = grsVar!IncDec
    
    Grid1.Col = 6
    If grsVar!Type = "%" Then
        Grid1.Text = grsVar!amt
    Else
        Grid1.Text = Format(grsVar!amt, "###.##")
    End If
    
    grsVar.MoveNext
Wend

ExitDispGrid:
    Exit Sub
    
DispGridError:
    'MsgBox "err=" + Str(Err) + " " + Error(Err)
    If Err = 3021 Then Resume ExitDispGrid

End Sub

Public Sub ClearGrid1()
Dim nTemp As Integer, I As Integer
For I = 1 To 49
    Grid1.Row = I
    Grid1.Col = 0: Grid1.Text = ""
    Grid1.Col = 1: Grid1.Text = ""
    Grid1.Col = 2: Grid1.Text = ""
    Grid1.Col = 3: Grid1.Text = ""
    Grid1.Col = 4: Grid1.Text = ""
    Grid1.Col = 5: Grid1.Text = ""
    Grid1.Col = 6: Grid1.Text = ""
Next I

End Sub

