VERSION 4.00
Begin VB.Form frmWeightMaint 
   BackColor       =   &H00C0FFFF&
   Caption         =   "Weights Table Maintenance"
   ClientHeight    =   6900
   ClientLeft      =   -1530
   ClientTop       =   1410
   ClientWidth     =   9585
   ControlBox      =   0   'False
   Height          =   7305
   Left            =   -1590
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   9585
   Top             =   1065
   Width           =   9705
   Begin VB.CommandButton CmdEnterData 
      Caption         =   "Enter Data"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3600
      TabIndex        =   28
      Top             =   6120
      Width           =   2775
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   10
      Left            =   7920
      TabIndex        =   20
      Top             =   5640
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   6240
      TabIndex        =   18
      Top             =   5640
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   4560
      TabIndex        =   16
      Top             =   5640
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   2760
      TabIndex        =   14
      Top             =   5640
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   1080
      TabIndex        =   12
      Top             =   5640
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   7920
      TabIndex        =   10
      Top             =   4800
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   6240
      TabIndex        =   8
      Top             =   4800
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   4560
      TabIndex        =   6
      Top             =   4800
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   2760
      TabIndex        =   4
      Top             =   4800
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   1080
      TabIndex        =   2
      Top             =   4800
      Width           =   615
   End
   Begin VB.TextBox txtWgts 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   6000
      TabIndex        =   0
      Top             =   3840
      Width           =   615
   End
   Begin VB.Label Label5 
      BackColor       =   &H00C0FFFF&
      Caption         =   "How much influence, or emphasis do you want"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   3960
      TabIndex        =   27
      Top             =   120
      Width           =   5415
   End
   Begin VB.Label Label4 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Statistical Weighted Average -"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   26
      Top             =   120
      Width           =   3495
   End
   Begin VB.Label Label3 
      BackColor       =   &H00C0FFFF&
      Caption         =   $"FRMWEIGH.frx":0000
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   240
      TabIndex        =   25
      Top             =   1920
      Width           =   9375
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "10 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   12
      Left            =   7680
      TabIndex        =   24
      Top             =   5400
      Width           =   1320
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Weeks"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   11
      Left            =   5040
      TabIndex        =   23
      Top             =   4080
      Width           =   600
   End
   Begin VB.Label LblHelp 
      BackColor       =   &H00C0FFFF&
      Caption         =   $"FRMWEIGH.frx":01D7
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1335
      Left            =   240
      TabIndex        =   22
      Top             =   480
      Width           =   9135
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "WEIGHTS"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   600
      TabIndex        =   21
      Top             =   3840
      Width           =   1425
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "9 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   9
      Left            =   5880
      TabIndex        =   19
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "8 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   8
      Left            =   4200
      TabIndex        =   17
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "7 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   2400
      TabIndex        =   15
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "6 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   6
      Left            =   720
      TabIndex        =   13
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "5 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   5
      Left            =   7560
      TabIndex        =   11
      Top             =   4560
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "4 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   4
      Left            =   5880
      TabIndex        =   9
      Top             =   4560
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "3 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   3
      Left            =   4200
      TabIndex        =   7
      Top             =   4560
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "2 Weeks Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   2400
      TabIndex        =   5
      Top             =   4560
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "1 Week Prior"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   720
      TabIndex        =   3
      Top             =   4560
      Width           =   1125
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Number of"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   5040
      TabIndex        =   1
      Top             =   3840
      Width           =   885
   End
End
Attribute VB_Name = "frmWeightMaint"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit


Private Sub CmdEnterData_Click()
UpdateWeights
Unload frmWeightMaint
ReCalcSales
CalcSales
FrmWeeklyForecast.Show
grsWgts.Edit
    grsWgts!NumWks = Val(txtWgts(0).Text)
grsWgts.Update
End Sub


Private Sub Command1_Click(Index As Integer)
Select Case Index
    Case 0 'back
        Unload frmWeightMaint
        frmSchedDate.Show
    Case 1 'next
        UpdateWeights
        Unload frmWeightMaint
        frmVarMnt2.Show
        
End Select
grsWgts.Edit
    grsWgts!NumWks = Val(txtWgts(0).Text)
grsWgts.Update
End Sub


Private Sub Form_Load()
Dim I As Integer
 'Maintain Tbls.Mdb:Weight

frmWeightMaint.Top = (Screen.Height - frmWeightMaint.Height) / 4
frmWeightMaint.Left = (Screen.Width - frmWeightMaint.Width) / 2
frmWeightMaint.Caption = "Edit Sales Projection: Weights Table Maintenance"


'If grsWgts!NumWks < 11 Then
'    For I = grsWgts!NumWks + 1 To 10
'        Label1(I).Visible = False
'        txtWgts(I).Visible = False
'    Next I
'End If

txtWgts(0).Text = grsWgts!NumWks
txtWgts(1) = grsWgts!wk1
txtWgts(2) = grsWgts!wk2
txtWgts(3) = grsWgts!wk3
txtWgts(4) = grsWgts!wk4
txtWgts(5) = grsWgts!wk5
txtWgts(6) = grsWgts!wk6
txtWgts(7) = grsWgts!wk7
txtWgts(8) = grsWgts!wk8
txtWgts(9) = grsWgts!wk9
txtWgts(10) = grsWgts!wk10
If SecondWeek = False Then
   FirstNewSales = True
Else
   SecondNewSales = True
End If
End Sub



Public Sub UpdateWeights()
 'Write to the Weight table of Tbls.Mdb
grsWgts.Edit
    grsWgts!NumWks = Val(txtWgts(0).Text)
    Select Case Val(txtWgts(0).Text)
           Case 0
             grsWgts!wk1 = 0
             grsWgts!wk2 = 0
             grsWgts!wk3 = 0
             grsWgts!wk4 = 0
             grsWgts!wk5 = 0
             grsWgts!wk6 = 0
             grsWgts!wk7 = 0
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 1
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = 0
             grsWgts!wk3 = 0
             grsWgts!wk4 = 0
             grsWgts!wk5 = 0
             grsWgts!wk6 = 0
             grsWgts!wk7 = 0
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 2
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = 0
             grsWgts!wk4 = 0
             grsWgts!wk5 = 0
             grsWgts!wk6 = 0
             grsWgts!wk7 = 0
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 3
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = 0
             grsWgts!wk5 = 0
             grsWgts!wk6 = 0
             grsWgts!wk7 = 0
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 4
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = Val(txtWgts(4))
             grsWgts!wk5 = 0
             grsWgts!wk6 = 0
             grsWgts!wk7 = 0
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 5
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = Val(txtWgts(4))
             grsWgts!wk5 = Val(txtWgts(5))
             grsWgts!wk6 = 0
             grsWgts!wk7 = 0
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 6
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = Val(txtWgts(4))
             grsWgts!wk5 = Val(txtWgts(5))
             grsWgts!wk6 = Val(txtWgts(6))
             grsWgts!wk7 = 0
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 7
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = Val(txtWgts(4))
             grsWgts!wk5 = Val(txtWgts(5))
             grsWgts!wk6 = Val(txtWgts(6))
             grsWgts!wk7 = Val(txtWgts(7))
             grsWgts!wk8 = 0
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 8
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = Val(txtWgts(4))
             grsWgts!wk5 = Val(txtWgts(5))
             grsWgts!wk6 = Val(txtWgts(6))
             grsWgts!wk7 = Val(txtWgts(7))
             grsWgts!wk8 = Val(txtWgts(8))
             grsWgts!wk9 = 0
             grsWgts!wk10 = 0
           Case 9
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = Val(txtWgts(4))
             grsWgts!wk5 = Val(txtWgts(5))
             grsWgts!wk6 = Val(txtWgts(6))
             grsWgts!wk7 = Val(txtWgts(7))
             grsWgts!wk8 = Val(txtWgts(8))
             grsWgts!wk9 = Val(txtWgts(9))
             grsWgts!wk10 = 0
           Case 10
             grsWgts!wk1 = Val(txtWgts(1))
             grsWgts!wk2 = Val(txtWgts(2))
             grsWgts!wk3 = Val(txtWgts(3))
             grsWgts!wk4 = Val(txtWgts(4))
             grsWgts!wk5 = Val(txtWgts(5))
             grsWgts!wk6 = Val(txtWgts(6))
             grsWgts!wk7 = Val(txtWgts(7))
             grsWgts!wk8 = Val(txtWgts(8))
             grsWgts!wk9 = Val(txtWgts(9))
             grsWgts!wk10 = Val(txtWgts(10))
          End Select
          grsWgts.Update

End Sub

Private Sub txtWgts_LostFocus(Index As Integer)
If Index = 0 Then
    If txtWgts(0) > 10 Then
        MsgBox "Invalid number of weeks"
        txtWgts(0) = 0
        txtWgts(0).SetFocus
    End If
End If

End Sub


