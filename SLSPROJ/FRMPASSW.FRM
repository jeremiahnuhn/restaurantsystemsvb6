VERSION 4.00
Begin VB.Form FrmPassword 
   Caption         =   "Enter Password"
   ClientHeight    =   3150
   ClientLeft      =   1815
   ClientTop       =   1590
   ClientWidth     =   6690
   ControlBox      =   0   'False
   Height          =   3555
   Left            =   1755
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   6690
   Top             =   1245
   Width           =   6810
   Begin VB.CommandButton Command1 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4680
      TabIndex        =   4
      Top             =   1440
      Width           =   1815
   End
   Begin VB.CommandButton CmdCreate 
      Caption         =   "Change Password"
      Height          =   375
      Left            =   4680
      TabIndex        =   3
      Top             =   840
      Width           =   1815
   End
   Begin VB.CommandButton CmdEnter 
      Caption         =   "Enter Password"
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   $"FRMPASSW.frx":0000
      Height          =   1095
      Left            =   120
      TabIndex        =   5
      Top             =   2040
      Width           =   6375
   End
   Begin VB.Label LblMessage 
      Caption         =   $"FRMPASSW.frx":017B
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   4215
   End
   Begin MSMask.MaskEdBox MaskPassword 
      Height          =   375
      Left            =   960
      TabIndex        =   0
      Top             =   1080
      Width           =   2415
      _version        =   65536
      _extentx        =   4260
      _extenty        =   661
      _stockprops     =   109
      forecolor       =   -2147483640
      backcolor       =   -2147483643
      borderstyle     =   1
      allowprompt     =   -1  'True
      promptchar      =   "*"
   End
End
Attribute VB_Name = "FrmPassword"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub CmdCreate_Click()
TempPassword = MaskPassword
TempPassword = Left(TempPassword, 4)
If MgrPassword = True Then
   FrmPassword.Hide
   Unload FrmPassword
   FrmNewPassword.Show
   Exit Sub
End If
If TempPassword = "" Then
   MsgBox "You must enter old password to create new password"
End If
If (TempPassword = Password) Then
  MgrPassword = True
  FrmPassword.Hide
  Unload FrmPassword
  FrmNewPassword.Show
Else
   LblMessage.Caption = "Incorrect Password,  Please enter Correct Password"
End If
End Sub

Private Sub CmdEnter_Click()
Dim TempPassword As String
TempPassword = MaskPassword
TempPassword = Format(Trim(Left(TempPassword, 4)), "<&&&&")
If (TempPassword = Password) Then
   MgrPassword = True
   FrmPassword.Hide
   FrmWeeklyForecast.Show
Else
   LblMessage.Caption = "Incorrect Password,  Please enter Correct Password"
End If
MaskPassword = ""
End Sub

Private Sub CmdReturn_Click()
FrmPassword.Hide
Unload FrmPassword
frmSchedDate.Show
End Sub

Private Sub Command1_Click()
   FrmPassword.Hide
   FrmWeeklyForecast.Show
End Sub

Private Sub Form_Load()
FrmPassword.Top = (Screen.Height - FrmPassword.Height) / 2
FrmPassword.Left = (Screen.Width - FrmPassword.Width) / 2
Dim TempVar As String
If Dir("C:\Labor\SalesPro.txt") <> "" Then
     Open "C:\Labor\SalesPro.txt" For Input As #1
          Line Input #1, TempVar
     Close #1
End If
Password = Format(Trim(Left(TempVar, 4)), "<&&&&")
If Password = "" Then
   LblMessage.Caption = "No password is available. Please Call Help Desk for reset of password. 252-937-2800 ext.1767"
End If
End Sub

