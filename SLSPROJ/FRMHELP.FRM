VERSION 4.00
Begin VB.Form FrmHelp 
   BackColor       =   &H00C0FFFF&
   Caption         =   "Instructions"
   ClientHeight    =   6645
   ClientLeft      =   -1935
   ClientTop       =   1350
   ClientWidth     =   9600
   ControlBox      =   0   'False
   Height          =   7050
   Left            =   -1995
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6645
   ScaleWidth      =   9600
   Top             =   1005
   Width           =   9720
   Begin VB.CommandButton CmdNext 
      Caption         =   "Next"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7800
      TabIndex        =   2
      Top             =   6120
      Width           =   1695
   End
   Begin VB.CommandButton CmdBack 
      Caption         =   "Back"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   6120
      Width           =   1695
   End
   Begin VB.Label LblHelp 
      Alignment       =   2  'Center
      BackColor       =   &H00C0FFFF&
      Caption         =   "HELP AREA"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   24
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1800
      TabIndex        =   0
      Top             =   360
      Width           =   6015
   End
End
Attribute VB_Name = "FrmHelp"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub CmdBack_Click()
Unload FrmHelp
frmWeightMaint.Show
End Sub

Private Sub CmdNext_Click()
frmVarMnt2.Show
FrmHelp.Hide
End Sub


