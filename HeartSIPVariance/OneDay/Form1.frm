VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "HeartSIP Variance"
   ClientHeight    =   7590
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10665
   LinkTopic       =   "Form1"
   ScaleHeight     =   7590
   ScaleWidth      =   10665
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8400
      TabIndex        =   7
      Top             =   6960
      Width           =   2055
   End
   Begin VB.ListBox HeartSIP 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   240
      TabIndex        =   4
      Top             =   4200
      Width           =   10095
   End
   Begin VB.ListBox IRIS 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   240
      TabIndex        =   3
      Top             =   1680
      Width           =   10095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Find Variance"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2640
      TabIndex        =   2
      Top             =   240
      Width           =   2055
   End
   Begin VB.TextBox RestNum 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1560
      TabIndex        =   0
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label8 
      Caption         =   "Input Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   14
      Top             =   0
      Width           =   2055
   End
   Begin VB.Label Label10 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9240
      TabIndex        =   13
      Top             =   3840
      Width           =   1215
   End
   Begin VB.Label Label9 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9240
      TabIndex        =   12
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Label Label7 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5880
      TabIndex        =   11
      Top             =   3840
      Width           =   3135
   End
   Begin VB.Label Label6 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5880
      TabIndex        =   10
      Top             =   1320
      Width           =   2775
   End
   Begin VB.Label Label5 
      Caption         =   "Found in the InfoCentral file but not in the IRIS file"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   3840
      Width           =   5295
   End
   Begin VB.Label Label4 
      Caption         =   "Found in the IRIS file but not in the InfoCentral file"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   1320
      Width           =   5295
   End
   Begin VB.Label Label3 
      Caption         =   "S:\HeartSIPVariance\####\INFOC.CSV"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5640
      TabIndex        =   6
      Top             =   600
      Width           =   4335
   End
   Begin VB.Label Label2 
      Caption         =   "S:\HeartSIPVariance\####\IRIS.TXT"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5640
      TabIndex        =   5
      Top             =   240
      Width           =   4215
   End
   Begin VB.Label Label1 
      Caption         =   "Restaurant Number"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
'Find Variance
IRIS.Clear
HeartSIP.Clear

Screen.MousePointer = vbHourglass

'IRIS File

Dim InfoLast4 As String
Dim InfoDateTime As String
Dim IRISCount As Integer
Dim InfoCCount As Integer
IRISFound = False
INFOFound = False

IRISFile = "s:\HeartSIPVariance\" + RestNum.Text + "\IRIS.TXT"
INFOFile = "s:\HeartSIPVariance\" + RestNum.Text + "\INFOC.csv"
IRISCount = -2
InfoCCouunt = 0
Open IRISFile For Input As #1
        Line Input #1, LineIn
        Line Input #1, LineIn
Do While Not EOF(1)
    IRISCount = IRISCount + 1
    IRISFound = False
    Line Input #1, LineIn
    OrderNum = Mid$(LineIn, 5, 7)
    IRISAmount = Mid$(LineIn, 23, 9)
    IRISType = Mid$(LineIn, 295, 4)
    If IRISType = "203" Then
            IRISLast4 = Mid$(LineIn, 46, 4)
    Else
            IRISLast4 = Mid$(LineIn, 47, 4)
    End If
    If IRISLast4 <> "" Then
    'Check Against InfoCentral
    
Open INFOFile For Input As #2
        Input #2, InfoDateTime, InfoLast4, InfoExpDate, InfoCardType, InfoAmount, InfoTip, Junk1, Junk2, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Junk9, Junk10, Junk11, Junk12
    
    Do While Not EOF(2)
        Input #2, InfoDateTime, InfoLast4, InfoExpDate, InfoCardType, InfoAmount, InfoTip, Junk1, Junk2, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Junk9, Junk10, Junk11, Junk12, Junk13
        
        If Mid$(InfoCardType, 1, 1) = "A" Then
            InfoCLast4 = Mid$(InfoLast4, 12, 4)
        Else
            InfoCLast4 = Mid$(InfoLast4, 13, 4)
        End If
        
        
         If IRISLast4 = InfoCLast4 And Val(IRISAmount) = Val(Mid$(InfoAmount, 2, 6)) Then
            IRISFound = True
         End If
        Label6.Caption = Str(IRISCount) + " Transactions"
        'Form1.Refresh
    Loop
    Close 2
    If IRISFound = False Then
        If IRISType = "201" Then
            IRISCard = "Visa"
        End If
        If IRISType = "202" Then
            IRISCard = "Master Card"
        End If
        If IRISType = "203" Then
            IRISCard = "American Express"
        End If
        If IRISType = "204" Then
            IRISCard = "Discover"
        End If
        
        
        IRIS.AddItem (OrderNum + "  " + IRISAmount + "   " + IRISLast4 + " " + IRISCard)
    End If
End If
Loop
Close 1

'InfoCentral File

Open INFOFile For Input As #2
        Input #2, InfoDateTime, InfoLast4, InfoExpDate, InfoCardType, InfoAmount, InfoTip, Junk1, Junk2, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Junk9, Junk10, Junk11, Junk12
    
    Do While Not EOF(2)
        InfoCCount = InfoCCount + 1
        INFOFound = False
        Input #2, InfoDateTime, InfoLast4, InfoExpDate, InfoCardType, InfoAmount, InfoTip, Junk1, Junk2, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Junk9, Junk10, Junk11, Junk12, Junk13

        If Mid$(InfoCardType, 1, 1) = "A" Then
            InfoCLast4 = Mid$(InfoLast4, 12, 4)
        Else
            InfoCLast4 = Mid$(InfoLast4, 13, 4)
        End If


        Open IRISFile For Input As #1
            Line Input #1, LineIn
            Line Input #1, LineIn
        Do While Not EOF(1)
            IRISFound = False
            Line Input #1, LineIn
            OrderNum = Mid$(LineIn, 5, 7)
            IRISAmount = Mid$(LineIn, 23, 9)
            IRISLast4 = Mid$(LineIn, 47, 4)
            IRISType = Mid$(LineIn, 295, 4)
            If IRISType = "203" Then
                IRISLast4 = Mid$(LineIn, 46, 4)
            Else
                IRISLast4 = Mid$(LineIn, 47, 4)
            End If
            If IRISLast4 <> "" Then
                 If IRISLast4 = InfoCLast4 And Val(IRISAmount) = Val(Mid$(InfoAmount, 2, 6)) Then
                    INFOFound = True
                End If
            End If
        Loop
        Close 1
            If INFOFound = False Then
                HeartSIP.AddItem (Format$(InfoAmount, "@@@@@@@@") + "  " + (Format$(InfoLast4, "@@@@@@@@@@@@@@@@@")) + "  " + InfoCardType)
            End If
        Label7.Caption = Str(InfoCCount) + " Transactions"
        'Form1.Refresh
    Loop
    Close 2
    Label6.Caption = Str(IRISCount) + " IRIS Transactions"
    Label7.Caption = Str(InfoCCount) + " InfoC Transactions"
    Label9.Caption = "Found " + Str(IRIS.ListCount)
    Label10.Caption = "Found " + Str(HeartSIP.ListCount)
    Screen.MousePointer = vbNormal
    Form1.Refresh
x = MsgBox("Search complete!", vbInformation, "Status")
End Sub

Private Sub Command2_Click()
    End
End Sub

