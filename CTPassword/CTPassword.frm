VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "CrunchTime (Net-Chef) User Password Reset"
   ClientHeight    =   3660
   ClientLeft      =   4305
   ClientTop       =   3075
   ClientWidth     =   7920
   Icon            =   "CTPassword.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3660
   ScaleWidth      =   7920
   Begin VB.CommandButton ExitPW 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4680
      TabIndex        =   4
      Top             =   2640
      Width           =   1815
   End
   Begin VB.CommandButton ResetPW 
      Caption         =   "Reset Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1320
      TabIndex        =   3
      Top             =   2640
      Width           =   1815
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3720
      TabIndex        =   0
      Top             =   600
      Width           =   2895
   End
   Begin VB.Label Label3 
      Caption         =   "Version 2"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   3360
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "The password will be reset back to the default password of Pic123! "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   240
      TabIndex        =   2
      Top             =   1440
      Width           =   7455
   End
   Begin VB.Label Label1 
      Caption         =   "Enter Employee Number "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   1
      Top             =   720
      Width           =   3255
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ExitPW_Click()
    End
End Sub

Private Sub Form_Load()
On Error Resume Next
End Sub

Private Sub ResetPW_Click()
On Error Resume Next

Open "\\bns1\share1\polling\Data\CrunchTime\employeeinformation.csv" For Input As #1
    Found = False


Do While Not EOF(1)
    Dim Unit As String
    Dim TimePW As String
    Dim EmpName As String
    TimePW = Now()
    'Input #1, Unit, EmpNum, LName, Fname, EmpName

    Line Input #1, IndividualLine
    FileInput = Split(IndividualLine, ",")
    Unit = FileInput(0)
    EmpNum = FileInput(1)
    LName = FileInput(2)
    FName = FileInput(3)
    EmpName = FileInput(4)

    If Text1.Text = EmpNum Then
    Response = MsgBox("Do you want to reset the password for " + EmpName + "?", vbYesNo, "Confirm Name")
        If Response = vbYes Then
        Found = True
         'Open "\\bns1\share1\crunchtime\cdp\264ApplicationUsers.txt" For Output As #2
         Open "\\bns1\share1\crunchtime\cdp\upload\264ApplicationUsers.txt" For Output As #2
            
            Print #2, "H" & vbTab & "000000" & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab
            Print #2, "2" & vbTab & Str(EmpNum) & vbTab; "Pic123!"
        Close #2
        
        Open "\\bns1\share1\polling\data\crunchtime\Password Log.csv" For Append As #3
            
            Print #3, TimePW + "," + Unit + "," + Str(EmpNum) + "," + EmpName
            
        Close #3

        Else
            Text1.Text = ""
            Text1.SetFocus
            Close #1
            Exit Sub
        End If
    End If
Loop
If Found = False Then
    Response = MsgBox("The Employee Number entered " + Text1.Text + " could not be found.  Please confirm and if correct, please assign the call to Restaurant Systems to handle.", vbInformation, "Employee Number Not Found")
End If
Close #1
Text1.Text = ""
Text1.SetFocus
End Sub
