sc config "POSAgentUpdater" start= disabled
sc config "POSAgent" start= disabled
taskkill /F /IM "pos_agent.exe"
taskkill /F /IM "agent_updater.exe"
taskkill /F /IM "xsPOSServer.exe"
sc config "POSAgent" start= delayed-auto
sc config "POSAgentUpdater" start= delayed-auto
sc start "POSAgent"
sc start "POSAgentUpdater"
