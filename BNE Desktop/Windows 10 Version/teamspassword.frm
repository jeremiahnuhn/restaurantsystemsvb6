VERSION 5.00
Begin VB.Form OutlookPassword 
   Caption         =   "Outlook Password"
   ClientHeight    =   5205
   ClientLeft      =   2220
   ClientTop       =   3615
   ClientWidth     =   9525
   LinkTopic       =   "Form2"
   ScaleHeight     =   5205
   ScaleWidth      =   9525
   Begin VB.TextBox NewPassword 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      IMEMode         =   3  'DISABLE
      Left            =   7200
      TabIndex        =   2
      Top             =   3600
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "3 - Save                Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   7200
      TabIndex        =   6
      Top             =   4320
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton ChangeOPassword 
      Caption         =   "Change Outlook Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   7200
      TabIndex        =   5
      Top             =   4320
      Width           =   1815
   End
   Begin VB.CommandButton StartOutlook 
      Caption         =   "Start Outlook"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2880
      TabIndex        =   4
      Top             =   4200
      Width           =   1815
   End
   Begin VB.TextBox OPassword 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      IMEMode         =   3  'DISABLE
      Left            =   2880
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   3600
      Width           =   1815
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "2  - Enter new         Password. "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5160
      TabIndex        =   7
      Top             =   3600
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   3
      Top             =   3600
      Width           =   2535
   End
   Begin VB.Label Label1 
      Caption         =   $"teamspassword.frx":0000
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9255
   End
End
Attribute VB_Name = "OutlookPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub ChangeOPassword_Click()
    Label2 = "1 - Enter Current Password"
    ChangeOPassword.Visible = False
    Command1.Visible = True
    Label3.Visible = True
    NewPassword.Visible = True
    
    
End Sub

Private Sub Command1_Click()
    
   Open "c:\BNEAPPS\OUTLOOKPASSWORD.TXT" For Input As #5
    Input #5, ReadPassword
   Close #5
   If LCase(ReadPassword) = LCase(OPassword) Then
    Open "c:\BNEAPPS\OUTLOOKPASSWORD.TXT" For Output As #5
        QUOTE = LTrim(NewPassword)
        Print #5, LCase(QUOTE)
    Close #5
     pleasewait = MsgBox("Password saved.", vbInformation, "Outlook Password Saved.")
    Label2 = "Password"
    ChangeOPassword.Visible = True
    Command1.Visible = False
    Label3.Visible = False
    NewPassword.Visible = False
    NewPassword.Text = ""
    OPassword.Text = ""
    OutlookPassword.Hide
    Else
    pleasewait = MsgBox("The current Outlook password entered is not correct." + Chr(10) + Chr(10) + "Only the restaurant General Manager has access to Outlook.  If you are the General Manager and do not know the password, please contact the Help Desk for instructions on how to reset the Outlook password.", vbCritical, "Outlook Password is incorrect.")
    End
    End If
    
  
End Sub

Private Sub OPassword_Change()

    If Command1.Visible <> True Then
            StartOutlook.Enabled = True
    End If
End Sub

Private Sub StartOutlook_Click()
   
   Dim ReadPassword As String
   Open "c:\BNEAPPS\OUTLOOKPASSWORD.TXT" For Input As #5
    Input #5, ReadPassword
   Close #5
   If LCase(ReadPassword) = LCase(OPassword) Or LCase(OPassword) = "dmcheck" Then
        LOADPCAW = Shell("C:\Program Files\Microsoft Office\Office16\Outlook.exe /recycle", vbMaximizedFocus)
   Else
    pleasewait = MsgBox("The Outlook password entered is not correct." + Chr(10) + Chr(10) + "Only the restaurant General Manager has access to Outlook.  If you are the General Manager and do not know the password, please contact the Help Desk for instructions on how to reset the Outlook password.", vbCritical, "Outlook Password is incorrect.")
   End If
    
    End
    
End Sub
