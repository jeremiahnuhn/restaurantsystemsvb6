VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Missing Settlements"
   ClientHeight    =   8985
   ClientLeft      =   4125
   ClientTop       =   1980
   ClientWidth     =   7665
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "VersionCheck.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8985
   ScaleWidth      =   7665
   Begin VB.CommandButton Command4 
      Caption         =   "Process File"
      Height          =   495
      Left            =   3720
      TabIndex        =   8
      Top             =   7440
      Width           =   1695
   End
   Begin VB.TextBox FileDate 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1320
      TabIndex        =   7
      Top             =   7800
      Width           =   1935
   End
   Begin VB.ListBox List2 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2220
      Left            =   240
      TabIndex        =   6
      Top             =   5040
      Width           =   7215
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5640
      TabIndex        =   4
      Top             =   7440
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Copy to Clipboard"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3720
      TabIndex        =   2
      Top             =   8040
      Width           =   1695
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4380
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   7215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5640
      TabIndex        =   0
      Top             =   8040
      Width           =   1695
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Settlement Date To Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   9
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Label Label2 
      Caption         =   "Version 2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   7320
      Width           =   855
   End
   Begin VB.Label Label1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   120
      Width           =   13695
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
  Clipboard.Clear
  Copytoclip = ""
For I = 0 To List1.ListCount - 1
Copytoclip = Copytoclip & List1.List(I) & Chr$(13) & Chr$(10)
Next I
Clipboard.SetText Copytoclip
m = MsgBox("Copy to Clipboard complete.", vbInformation, "Clipboard Status")
End Sub

Private Sub Command3_Click()
Printer.Print
Printer.Print
   Printer.FontName = "COURIER NEW"
   Printer.FontSize = 12
      Printer.CurrentX = 1000
   Printer.Print "####   T    TID    Last Settled   # of Days"
      Printer.CurrentX = 1000
   Printer.Print "--------------------------------------------------"
For I = 0 To List1.ListCount - 1
Printer.CurrentX = 1000
Printer.Print List1.List(I)
Printer.Print
Printer.Print
Next I
Printer.Print
Printer.FontSize = 10
Printer.CurrentX = 2600
Printer.Print Str(Now())
Printer.EndDoc
m = MsgBox("Printed", vbInformation, "Printing Status")
End Sub

Private Sub Command4_Click()

Label1 = "####   T     TID     Last Settled  # Days"
On Error GoTo ErrorHandler

List1.Clear
List2.Clear
Dim LastBatch As Date
Dim UnitNumber As String
Dim TID As String
Dim TIDFound As Boolean




Open "s:\HeartSIP\Settlement\MissingSettlement.csv" For Output As #4
Print #4, "####,Terminal,TID,Last Settlement,# of Days"

Open "s:\HeartSIP\Settlement\Settlement.csv" For Input As #1
    Line Input #1, IndividualLine
    Line Input #1, IndividualLine
    Do While Not EOF(1)

    Line Input #1, IndividualLine
    FileInput = Split(IndividualLine, ",")
    UnitNumber = Mid$(FileInput(2), 9, 4)
    TID = FileInput(3)
    LastBatch = FileInput(6)
    
    TIDFound = False
    If LastBatch > Now() - 14 Then
    
            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T1.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID1 = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If TID1 = TID Then
                TIDFound = True
                If LastBatch < FileDate Then
                
                List2.AddItem UnitNumber + "   1   " + TID + "    " + Format$(LastBatch, "MM/DD/YY") + "      " + Format$(Now() - LastBatch, "##")
                Print #4, UnitNumber + ",1," + TID + "," + Str(LastBatch) + "," + Format$(Now() - LastBatch, "##")
                End If
            End If

            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T2.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID2 = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If TID2 = TID Then
                TIDFound = True
                If LastBatch < FileDate Then
            
                List2.AddItem UnitNumber + "   2   " + TID + "    " + Format$(LastBatch, "MM/DD/YY") + "      " + Format$(Now() - LastBatch, "##")
                Print #4, UnitNumber + ",2," + TID + "," + Str(LastBatch) + "," + Format$(Now() - LastBatch, "##")
                End If
            End If

            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T3.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID3 = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If TID3 = TID Then
                TIDFound = True
                If LastBatch < FileDate Then
            
                List1.AddItem UnitNumber + "   3   " + TID + "    " + Format$(LastBatch, "MM/DD/YY") + "      " + Format$(Now() - LastBatch, "##")
                Print #4, UnitNumber + ",3," + TID + "," + Str(LastBatch) + "," + Format$(Now() - LastBatch, "##")
                End If
            End If
            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T5.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID5 = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If TID5 = TID Then
                TIDFound = True
                If LastBatch < FileDate Then
            
                List2.AddItem UnitNumber + "   5   " + TID + "    " + Format$(LastBatch, "MM/DD/YY") + "      " + Format$(Now() - LastBatch, "##")
                Print #4, UnitNumber + ",5," + TID + "," + Str(LastBatch) + "," + Format$(Now() - LastBatch, "##")
                End If
            End If

    If Mid$(TID, 1, 2) <> "01" Then
        If LastBatch < FileDate Then

            If TIDFound = False Then
                List1.AddItem UnitNumber + "  ***  " + TID + "    " + Format$(LastBatch, "MM/DD/YY") + "      " + Format$(Now() - LastBatch, "##")
                Print #4, UnitNumber + ",*," + TID + "," + Str(LastBatch) + "," + Format$(Now() - LastBatch, "##")
            End If
        End If
    End If
 End If
Loop
Close #1

Close 4

Exit Sub

ErrorHandler:
  ' List1.AddItem UnitNumber + "  " + TempName + " " + Temp + " Version file missing."

Resume Next


End Sub

Private Sub Form_Load()
    FileDate.Text = Format(Now() - 2, "mm/dd/yyyy")
End Sub
