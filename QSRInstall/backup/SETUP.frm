VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "QSR Kitchen Video System Installation Utility"
   ClientHeight    =   9540
   ClientLeft      =   300
   ClientTop       =   345
   ClientWidth     =   9765
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   15.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SETUP.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9540
   ScaleWidth      =   9765
   Begin VB.CommandButton Command6 
      Caption         =   "Execute Control Point Client Setup"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   19
      Top             =   4800
      Width           =   6255
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Execute Control Point Server Setup"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   16
      Top             =   4200
      Width           =   6255
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Copy Installation Files"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   12
      Top             =   2880
      Width           =   6255
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Verify Terminal 4 has the correct image"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   8
      Top             =   2280
      Width           =   6255
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Create Backup for Disaster Recovery"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   6
      Top             =   1680
      Width           =   6255
   End
   Begin VB.CommandButton Command1 
      Caption         =   "EXIT"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8640
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   " Preparation "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3135
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   8895
      Begin VB.CommandButton UpdateTerminals 
         Caption         =   "Print Instructions"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1320
         TabIndex        =   2
         Top             =   600
         Width           =   6255
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   7680
         TabIndex        =   14
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "Step 4"
         Height          =   495
         Left            =   120
         TabIndex        =   13
         Top             =   2400
         Width           =   1095
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   7680
         TabIndex        =   11
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   7680
         TabIndex        =   10
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "Step 3"
         Height          =   495
         Left            =   120
         TabIndex        =   9
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Step 2"
         Height          =   495
         Left            =   120
         TabIndex        =   7
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Step 1"
         Height          =   495
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   7680
         TabIndex        =   3
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Installation "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   5415
      Left            =   360
      TabIndex        =   15
      Top             =   3720
      Width           =   8895
      Begin VB.CommandButton Command12 
         Caption         =   "Copy IRIS files and configure IRIS"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1320
         TabIndex        =   33
         Top             =   4680
         Width           =   6255
      End
      Begin VB.CommandButton Command11 
         Caption         =   "Configure service for QSR Automations"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1320
         TabIndex        =   32
         Top             =   4080
         Width           =   6255
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Copy config files for RDS"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1320
         TabIndex        =   31
         Top             =   3480
         Width           =   6255
      End
      Begin VB.CommandButton Command9 
         Caption         =   "Execute RDS Setup"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1320
         TabIndex        =   30
         Top             =   2880
         Width           =   6255
      End
      Begin VB.CommandButton Command8 
         Caption         =   "Execute Control Point Builder"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1320
         TabIndex        =   29
         Top             =   2280
         Width           =   6255
      End
      Begin VB.CommandButton Command7 
         Caption         =   "Copy config files for Control Point"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1320
         TabIndex        =   28
         Top             =   1680
         Width           =   6255
      End
      Begin VB.Label Label27 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   39
         Top             =   4680
         Width           =   1095
      End
      Begin VB.Label Label26 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   38
         Top             =   4080
         Width           =   1095
      End
      Begin VB.Label Label25 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   37
         Top             =   3480
         Width           =   1095
      End
      Begin VB.Label Label24 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   36
         Top             =   2880
         Width           =   1095
      End
      Begin VB.Label Label23 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   35
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label10 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   34
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label Label20 
         Caption         =   "Step 12"
         Height          =   495
         Left            =   120
         TabIndex        =   27
         Top             =   4680
         Width           =   1095
      End
      Begin VB.Label Label19 
         Caption         =   "Step 11"
         Height          =   495
         Left            =   120
         TabIndex        =   26
         Top             =   4080
         Width           =   1095
      End
      Begin VB.Label Label18 
         Caption         =   "Step 10"
         Height          =   495
         Left            =   120
         TabIndex        =   25
         Top             =   3480
         Width           =   1095
      End
      Begin VB.Label Label17 
         Caption         =   "Step 9"
         Height          =   495
         Left            =   120
         TabIndex        =   24
         Top             =   2880
         Width           =   975
      End
      Begin VB.Label Label16 
         Caption         =   "Step 8"
         Height          =   495
         Left            =   120
         TabIndex        =   23
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label15 
         Caption         =   "Step 7"
         Height          =   495
         Left            =   120
         TabIndex        =   22
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   21
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label13 
         Caption         =   "Step 6"
         Height          =   495
         Left            =   120
         TabIndex        =   20
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label Label12 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   7680
         TabIndex        =   18
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label11 
         Caption         =   "Step 5"
         Height          =   495
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   975
      End
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Version 2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8280
      TabIndex        =   1
      Top             =   9240
      Width           =   975
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command10_Click()
        x = Shell("C:\Support\QSR\RDS_cfg.bat", vbMaximizedFocus)
        Label25.Caption = "P"
        JobLog ("Step10=1")
        Command10.Enabled = False
        Command11.Enabled = True
End Sub

Private Sub Command11_Click()
    Open "C:\ul.bat" For Output As 1
    Print #1, "services.msc"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
        Label26.Caption = "P"
        JobLog ("Step11=1")
        Command11.Enabled = False
        Command12.Enabled = True
End Sub

Private Sub Command12_Click()
    Kill "c:\support\QSR\IRIS\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG1\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG2\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG3\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG4\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG5\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG6\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG7\INI\Sockets.INI"
    Kill "c:\support\QSR\IRIS\REGINFO\REG8\INI\Sockets.INI"

'Socket.INI Changes due to Heartland change

'Reg 1
FileCopy "c:\iris\reginfo\reg1\ini\sockets.ini", "c:\support\QSR\reg1.ini"
Open "c:\support\QSR\reg1.ini" For Input As #1
Open "c:\iris\reginfo\reg1\ini\sockets.ini" For Output As #3

Do While Not EOF(1)
    Line Input #1, INIDATA
    Select Case UCase(Mid$(INIDATA, 1, 8))
            Case "[SECONDA"
                Print #3, "; QSR Installation " & Now()
                Print #3, INIDATA
                Print #3, "Port=7503"
                Print #3, "Addr=100.100.100.100"
                Line Input #1, INIDATA
                Line Input #1, INIDATA
            Case Else
                Print #3, INIDATA
            End Select
        Loop
        Close #2
        Close #3

Close #1

' End Reg 1

'Reg 2
FileCopy "c:\iris\reginfo\reg2\ini\sockets.ini", "c:\support\QSR\reg2.ini"
Open "c:\support\QSR\reg2.ini" For Input As #1
Open "c:\iris\reginfo\reg2\ini\sockets.ini" For Output As #3

Do While Not EOF(1)
    Line Input #1, INIDATA
    Select Case UCase(Mid$(INIDATA, 1, 8))
            Case "[SECONDA"
                Print #3, "; QSR Installation " & Now()
                Print #3, INIDATA
                Print #3, "Port=7503"
                Print #3, "Addr=100.100.100.100"
                Line Input #1, INIDATA
                Line Input #1, INIDATA
            Case Else
                Print #3, INIDATA
            End Select
        Loop
        Close #2
        Close #3

Close #1

' End Reg 2
        
'Reg 3
FileCopy "c:\iris\reginfo\reg3\ini\sockets.ini", "c:\support\QSR\reg3.ini"
Open "c:\support\QSR\reg3.ini" For Input As #1
Open "c:\iris\reginfo\reg3\ini\sockets.ini" For Output As #3

Do While Not EOF(1)
    Line Input #1, INIDATA
    Select Case UCase(Mid$(INIDATA, 1, 8))
            Case "[SECONDA"
                Print #3, "; QSR Installation " & Now()
                Print #3, INIDATA
                Print #3, "Port=7503"
                Print #3, "Addr=100.100.100.100"
                Line Input #1, INIDATA
                Line Input #1, INIDATA
            Case Else
                Print #3, INIDATA
            End Select
        Loop
        Close #2
        Close #3

Close #1

' End Reg 3
        
'Reg 4
FileCopy "c:\iris\reginfo\reg4\ini\sockets.ini", "c:\support\QSR\reg4.ini"
Open "c:\support\QSR\reg4.ini" For Input As #1
Open "c:\iris\reginfo\reg4\ini\sockets.ini" For Output As #3

Do While Not EOF(1)
    Line Input #1, INIDATA
    Select Case UCase(Mid$(INIDATA, 1, 8))
            Case "[SECONDA"
                Print #3, "; QSR Installation " & Now()
                Print #3, INIDATA
                Print #3, "Port=7503"
                Print #3, "Addr=100.100.100.100"
                Line Input #1, INIDATA
                Line Input #1, INIDATA
            Case Else
                Print #3, INIDATA
            End Select
        Loop
        Close #2
        Close #3

Close #1

' End Reg 4

'Reg 5
If Dir("c:\iris\reginfo\reg5\ini\sockets.ini") <> "" Then
FileCopy "c:\iris\reginfo\reg5\ini\sockets.ini", "c:\support\QSR\reg5.ini"
Open "c:\support\QSR\reg5.ini" For Input As #1
Open "c:\iris\reginfo\reg5\ini\sockets.ini" For Output As #3

Do While Not EOF(1)
    Line Input #1, INIDATA
    Select Case UCase(Mid$(INIDATA, 1, 8))
            Case "[SECONDA"
                Print #3, "; QSR Installation " & Now()
                Print #3, INIDATA
                Print #3, "Port=7503"
                Print #3, "Addr=100.100.100.100"
                Line Input #1, INIDATA
                Line Input #1, INIDATA
            Case Else
                Print #3, INIDATA
            End Select
        Loop
        Close #2
        Close #3

Close #1
End If
' End Reg 5

'Reg 6
If Dir("c:\iris\reginfo\reg6\ini\sockets.ini") <> "" Then
FileCopy "c:\iris\reginfo\reg6\ini\sockets.ini", "c:\support\QSR\reg6.ini"
Open "c:\support\QSR\reg6.ini" For Input As #1
Open "c:\iris\reginfo\reg6\ini\sockets.ini" For Output As #3

Do While Not EOF(1)
    Line Input #1, INIDATA
    Select Case UCase(Mid$(INIDATA, 1, 8))
            Case "[SECONDA"
                Print #3, "; QSR Installation " & Now()
                Print #3, INIDATA
                Print #3, "Port=7503"
                Print #3, "Addr=100.100.100.100"
                Line Input #1, INIDATA
                Line Input #1, INIDATA
            Case Else
                Print #3, INIDATA
            End Select
        Loop
        Close #2
        Close #3

Close #1
End If
' End Reg 6

'SERVER
FileCopy "c:\iris\ini\sockets.ini", "c:\support\QSR\regs.ini"
Open "c:\support\QSR\regs.ini" For Input As #1
Open "c:\iris\ini\sockets.ini" For Output As #3

Do While Not EOF(1)
    Line Input #1, INIDATA
    Select Case UCase(Mid$(INIDATA, 1, 8))
            Case "[SECONDA"
                Print #3, "; QSR Installation " & Now()
                Print #3, INIDATA
                Print #3, "Port=7503"
                Print #3, "Addr=100.100.100.100"
                Line Input #1, INIDATA
                Line Input #1, INIDATA
            Case Else
                Print #3, INIDATA
            End Select
        Loop
        Close #2
        Close #3

Close #1

' End SERVER
        
        
        
        
        
        x = Shell("C:\Support\QSR\IRIS.bat", vbMaximizedFocus)
        Label27.Caption = "P"
        JobLog ("Step5=1")
        Command12.Enabled = False

            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, Uname, email, junk
                    Input #6, unumber, Uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + Uname + "," + DATENOW + "," + TIMENOW + "," + "QSR Install Complete"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, Uname, email, junk
                    Input #6, unumber, Uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + Uname + "," + DATENOW + "," + TIMENOW + "," + "QSR Install Complete"""
            Close 4

End Sub

Private Sub Command13_Click()
        'x = Shell("C:\Support\QSR\ControlPointServerSetup.exe", vbMaximizedFocus)
        Label28.Caption = "P"
        JobLog ("Step13=1")

End Sub

Private Sub Command14_Click()
        x = Shell("c:\windows\system32\shutdown.exe -r -f ", vbMaximizedFocus)
        Label12.Caption = "P"
        JobLog ("Step5=1")

End Sub

Private Sub Command2_Click()
    x = Shell("C:\Program Files\Acronis\BackupAndRecoveryConsole\ManagementConsole.exe")
    Label5.Caption = "P"
    JobLog ("Step2=1")
    Command2.Enabled = False
    Command3.Enabled = True
    
End Sub

Private Sub Command3_Click()
        
        Label7.Caption = "P"
        JobLog ("Step3=1")
        Command3.Enabled = False
        Command4.Enabled = True

End Sub




Private Sub Command4_Click()

x = Shell("e:\QSR_Prep.bat", vbMaximizedFocus)


        Label9.Caption = "P"
        JobLog ("Step4=1")
        Command4.Enabled = False
        Command5.Enabled = True
End Sub

Private Sub Command5_Click()
        x = Shell("C:\Support\QSR\ControlPointServerSetup.exe", vbMaximizedFocus)
        Label12.Caption = "P"
        JobLog ("Step5=1")
        Command5.Enabled = False
        Command6.Enabled = True
End Sub

Private Sub Command6_Click()
        x = Shell("C:\Support\QSR\ControlPointClientSetup.exe", vbMaximizedFocus)
        Label14.Caption = "P"
        JobLog ("Step6=1")
        Command6.Enabled = False
        Command7.Enabled = True
End Sub

Private Sub Command7_Click()
        x = Shell("C:\Support\QSR\QSR_cfg.bat", vbMaximizedFocus)
        Label10.Caption = "P"
        JobLog ("Step7=1")
        Command7.Enabled = False
        Command8.Enabled = True
End Sub

Private Sub Command8_Click()
        x = Shell("C:\Program Files\Qsr Automations\ControlPoint\ControlPointServer\bin\ControlPointBuilder.exe", vbMaximizedFocus)
        Label23.Caption = "P"
        JobLog ("Step8=1")
        Command8.Enabled = False
        Command9.Enabled = True
End Sub

Private Sub Command9_Click()
        x = Shell("C:\Support\QSR\RDSSetup.exe", vbMaximizedFocus)
        Label24.Caption = "P"
        JobLog ("Step9=1")
        Command9.Enabled = False
        Command10.Enabled = True
End Sub

Private Sub Form_Load()

'On Error Resume Next
If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
    Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
    Input #1, unumber, Uname, email, junk
    Input #1, unumber, Uname, email, junk
    Close #1
    'Form1.Caption = "Restaurant:   " + Uname + "  #" + unumber
End If

temp = Now()
JobLog (temp)

'**** SETUP CHECKS
    LastStep = 0
    fn = "c:\support\QSR" + unumber + ".txt"
If Dir(fn) <> "" Then

    
    
    
    Open fn For Input As 1
    Do While Not EOF(1)
        Input #1, Stepread
        Select Case Stepread
            Case "Step1=1"
                Label2.Caption = "P"
                LastStep = 1
            Case "Step2=1"
                Label5.Caption = "P"
                Command2.Enabled = False
                LastStep = 2
            Case "Step3=1"
                Label7.Caption = "P"
                Command3.Enabled = False
                LastStep = 3
            Case "Step4=1"
                Label9.Caption = "P"
                Command4.Enabled = False
                LastStep = 4
            Case "Step5=1"
                Label12.Caption = "P"
                Command5.Enabled = False
                LastStep = 5
            Case "Step6=1"
                Label14.Caption = "P"
                Command6.Enabled = False
                LastStep = 6
            Case "Step7=1"
                Label10.Caption = "P"
                Command7.Enabled = False
                LastStep = 7
            Case "Step8=1"
                Label23.Caption = "P"
                Command8.Enabled = False
                LastStep = 8
            Case "Step9=1"
                Label24.Caption = "P"
                Command9.Enabled = False
                LastStep = 9
            Case "Step10=1"
                Label25.Caption = "P"
                Command10.Enabled = False
                LastStep = 10
            Case "Step11=1"
                Label26.Caption = "P"
                Command11.Enabled = False
                LastStep = 11
            Case "Step12=1"
                Label27.Caption = "P"
                Command12.Enabled = False
                LastStep = 12
        End Select
    Loop
    Close 1
If LastStep = 2 Then
    Command3.Enabled = True
End If
If LastStep = 3 Then
    Command4.Enabled = True
End If

If LastStep = 4 Then
    Command5.Enabled = True
End If
If LastStep = 5 Then
    Command6.Enabled = True
End If
If LastStep = 6 Then
    Command7.Enabled = True
End If
If LastStep = 7 Then
    Command8.Enabled = True
End If
If LastStep = 8 Then
    Command9.Enabled = True
End If
If LastStep = 9 Then
    Command10.Enabled = True
End If
If LastStep = 10 Then
    Command11.Enabled = True
End If
If LastStep = 11 Then
    Command12.Enabled = True
End If










End If
End Sub

Private Sub UpdateTerminals_Click()
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Support\QSR_Inst.PDF" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Support\QSR_Inst.PDF" + Chr(34)

    End If
    x = Shell(exe)
    Label2.Caption = "P"
    JobLog ("Step1=1")

End Sub
