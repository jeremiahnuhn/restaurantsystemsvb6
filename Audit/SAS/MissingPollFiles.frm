VERSION 5.00
Begin VB.Form MissingPollFiles 
   Caption         =   "SAS Historical Files"
   ClientHeight    =   7830
   ClientLeft      =   5595
   ClientTop       =   1620
   ClientWidth     =   7995
   Icon            =   "MissingPollFiles.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7830
   ScaleWidth      =   7995
   Begin VB.CommandButton Command1 
      Caption         =   "Add CKE # and Copy"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   4200
      TabIndex        =   16
      Top             =   1680
      Visible         =   0   'False
      Width           =   3135
   End
   Begin VB.TextBox TextYear 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5400
      TabIndex        =   15
      Top             =   960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox TextMonth 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4440
      TabIndex        =   14
      Top             =   960
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.FileListBox AS400Directory 
      Height          =   1455
      Left            =   10680
      TabIndex        =   7
      Top             =   4920
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.FileListBox FlagDirectory 
      Height          =   1455
      Left            =   8400
      Pattern         =   "*.txt"
      TabIndex        =   5
      Top             =   4920
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.FileListBox PollingDirectory 
      Height          =   1650
      Left            =   4800
      Pattern         =   "????_SAS_tblOrderItem.hst"
      TabIndex        =   4
      Top             =   5160
      Width           =   2895
   End
   Begin VB.ListBox MissingList 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3840
      Left            =   240
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   360
      Width           =   2775
   End
   Begin VB.ListBox RestaurantListing 
      Height          =   1620
      Left            =   240
      TabIndex        =   2
      Top             =   5160
      Width           =   4335
   End
   Begin VB.CommandButton UpdateButton 
      Caption         =   "&Update"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   6360
      TabIndex        =   1
      Top             =   7200
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton ExitButton 
      Caption         =   "&Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4680
      TabIndex        =   0
      Top             =   3120
      Width           =   1935
   End
   Begin VB.Label Label7 
      Caption         =   "Year"
      Height          =   375
      Left            =   5400
      TabIndex        =   18
      Top             =   600
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label6 
      Caption         =   "Period"
      Height          =   255
      Left            =   4440
      TabIndex        =   17
      Top             =   600
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label5 
      Caption         =   "\\bns1\share1\BNEShare \IntranetExports\SAS\Polling\Historical\Missing January Dates"
      Height          =   735
      Left            =   4800
      TabIndex        =   13
      Top             =   4320
      Width           =   2535
   End
   Begin VB.Label Label4 
      Caption         =   "Restaurant List"
      Height          =   375
      Left            =   240
      TabIndex        =   12
      Top             =   4800
      Width           =   2655
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   4920
      TabIndex        =   11
      Top             =   6960
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   360
      TabIndex        =   10
      Top             =   6960
      Width           =   1935
   End
   Begin VB.Label MissingLabel 
      Height          =   375
      Left            =   360
      TabIndex        =   9
      Top             =   4320
      Width           =   2655
   End
   Begin VB.Label AS400Label 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   10200
      TabIndex        =   8
      Top             =   4320
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "Restaurants Missing Audit File"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "MissingPollFiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
MissingPollFiles.MousePointer = 11




Z = 0
For X = 0 To RestaurantListing.ListCount - 1



BNEText = Mid$(RestaurantListing.List(X), 1, 4)
CKEText = Mid$(RestaurantListing.List(X), 6, 7)



SFile = "\\bns1\share1\BNEShare\Restaurant Accounting\Audit\" + BNEText + "-" + TextMonth + "-" + TextYear + ".PDF"
DFile = "\\bns1\share1\BNEShare\Restaurant Accounting\Audit\CKE\" + CKEText + "-" + BNEText + "-" + TextMonth + "-" + TextYear + ".PDF"

FileCopy SFile, DFile
Z = Z + 1
Next X
MissingPollFiles.MousePointer = 0

    m = MsgBox(Str(Z) + " files copied.", vbInformational, "Copy Confirmation")

End Sub

Private Sub ExitButton_Click()
End
End Sub

Private Sub Form_Load()
'If Hour(Now) >= 15 Then HardeesOnly.Value = 1
On Error Resume Next
'If Dir("\\bns1\share1\polling\Data\Restaurant Listing.csv") = "" Then
'    m = MsgBox("The BNS1 erver is not available, please contact Technical Servcies for assistance.", vbCritical, "Missing Poll Files: Server Access Error")
'    End
'End If
Command1.Enabled = True
MissingPollFiles.MousePointer = 11
AS400Label.Caption = ""
RestaurantListing.Clear
MissingList.Clear
MissingList.FontSize = 12
MissingList.ForeColor = vbBlack
MissingList.Height = 3840
MissingList.FontBold = False
FlagDirectory.FileName = "\\bns1\share1\polling\Poll\Flag"
AS400Directory.FileName = "\\bns1\share1\BNESHare\IntranetExports\SAS\Polling\Historical\Missing January Dates"
PollingDirectory.FileName = "\\bns1\share1\BNESHare\IntranetExports\SAS\Polling\Historical\Missing January Dates"
FlagDirectory.Refresh
PollingDirectory.Refresh
AS400Directory.Refresh
TextYear.Text = Year(Now())
TextMonth.Text = Month(Now())


'Build Restaurant Listing

  Open "\\bns1\share1\BNEShare\Restaurant Accounting\Audit\program\unitlist.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
           RestaurantListing.AddItem unt
  Loop
  Close (1)
' Build missing information
    MissingPollFiles.Refresh
For X = 0 To RestaurantListing.ListCount - 1
    Found = False
    For Y = 0 To PollingDirectory.ListCount - 1
        If Mid$(RestaurantListing.List(X), 1, 4) = Mid$(PollingDirectory.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
            MissingList.AddItem Mid$(RestaurantListing.List(X), 1, 4)
        End If
Next X
MissingList.Refresh
DoEvents
If MissingList.ListCount = 0 Then
        MissingList.FontSize = 20
        MissingList.ForeColor = vbBlue
        MissingList.Height = 4200
        MissingList.FontBold = True
        MissingList.AddItem "100%"
        MissingLabel.Caption = "100% Received"
Else
    MissingLabel.Caption = Str(MissingList.ListCount) + " Restaurants Missing"
End If

Label2.Caption = Str(RestaurantListing.ListCount) + " Restaurants"
Label3.Caption = Str(PollingDirectory.ListCount) + " Audit Files"
MissingPollFiles.MousePointer = 0


End Sub

Private Sub ftp_Click()
If ftp.Value = 1 Then
    m = MsgBox("Reminder to make sure you FTP the files to the \\BNS1\Share1\Polling\Poll\FTPCheck folder.", vbInformational, "FTP Reminder")
End If
    Call UpdateButton_Click
End Sub

Private Sub HardeesOnly_Click()
Call UpdateButton_Click
End Sub

Private Sub PrintButton_Click()
MissingPollFiles.MousePointer = 11
Dim today As Date
Dim PageNum As Integer
Dim V As Integer
Dim W As Integer
Dim X As Integer
Dim Y As Integer
Dim Z As Integer
W = 1
Z = 5
V = 0
PageNum = 1
Y = 2200
   Printer.FontName = "COURIER NEW"
   Printer.FontBold = True
   Printer.FontSize = 14
   today = Format(Now, "ddddd")
   Printer.CurrentX = 3400
   Printer.CurrentY = 500
   Printer.Print "BODDIE-NOELL ENTERPRISES, INC."
   Printer.FontSize = 10
   Printer.Print "                  Reminder: All concepts are Priority 1 if missed on Tuesday."
   Printer.Print "                       "
   Printer.FontSize = 12
   Printer.CurrentX = 2400
   Printer.CurrentY = 1400
   Printer.Print "Missing Poll Files as of " + Str(Now)
   Printer.Print
   Printer.FontSize = 10: Printer.FontBold = False
   Printer.CurrentX = 1215
   Printer.CurrentY = 1800
   Printer.FontUnderline = True
   Printer.Print " Concept  ####    Restaurant         Speed #  Error Condition   "
   Printer.FontUnderline = False
   Printer.Print " "
   Z = 0
For I = 0 To MissingList.ListCount - 1
   Z = Z + 1
   Printer.FontSize = 10
   Printer.FontBold = True
   Printer.Print "           " + MissingList.List(I)
If Mid$(MissingList.List(I), 6, 2) <> "10" Then
   Printer.FontBold = False
   Printer.CurrentX = 1200
   Printer.CurrentY = Y + 800
   Printer.FontSize = 8
   Printer.Print "Manager on Duty"
   Printer.Line (2800, Y + 950)-(4800, Y + 950)
   Printer.CurrentX = 5000
   Printer.CurrentY = Y + 800
   Printer.Print "Title"
   Printer.Line (5600, Y + 950)-(6900, Y + 950)
   Printer.CurrentX = 7100
   Printer.CurrentY = Y + 800
   Printer.Print "Days Missed"
   Printer.Line (8300, Y + 950)-(9200, Y + 950)
   Printer.CurrentX = 9300
   Printer.CurrentY = Y + 800
   Printer.Print " "
   Printer.CurrentX = 1200
   Printer.CurrentY = Y + 1400
   Printer.Print "Solution"
   Printer.Line (2100, Y + 1550)-(8700, Y + 1550)
   Printer.CurrentX = 8800
   Printer.CurrentY = Y + 1400
   Printer.Print "Missed  Y  N"
   Printer.Print " "
End If
   Y = Y + 1900
    If Z = 6 Then
       Printer.CurrentX = 10000
       Printer.CurrentY = 14500
       Printer.Print "Page " & PageNum
       PageNum = PageNum + 1
       Printer.NewPage
   Printer.FontSize = 10: Printer.FontBold = False
   Printer.CurrentX = 1215
   Printer.CurrentY = 1800
   Printer.FontUnderline = True
   Printer.Print " Concept  ####    Restaurant Name    Speed #  Error Condition   "
   Printer.FontUnderline = False
   Printer.Print " "
   Z = 0
   Y = 2200
    End If
Next I
Printer.CurrentX = 1200
Printer.CurrentY = 13500
Printer.Print "Help Desk Operator"
Printer.Line (3200, 13650)-(7600, 13650)
Printer.CurrentX = 7700
Printer.CurrentY = 13500
Printer.Print "Time Completed"
Printer.Line (9200, 13650)-(10200, 13650)
Printer.CurrentX = 10000
Printer.CurrentY = 14500
Printer.Print "Page " & PageNum
Printer.EndDoc
  m = MsgBox("Printing complete.", vbInformation, "Missing Poll Files: Print Status")

MissingPollFiles.MousePointer = 0
End Sub

Private Sub UpdateButton_Click()
Command1.Enabled = True
MissingPollFiles.MousePointer = 11
AS400Label.Caption = ""
RestaurantListing.Clear
MissingList.Clear
MissingList.FontSize = 12
MissingList.ForeColor = vbBlack
MissingList.Height = 3840
MissingList.FontBold = False
FlagDirectory.FileName = "\\bns1\share1\polling\Poll\Flag"
AS400Directory.FileName = "\\bns1\share1\BNEShare\Restaurant Accounting\Audit"
PollingDirectory.FileName = "\\bns1\share1\BNEShare\Restaurant Accounting\Audit"
FlagDirectory.Refresh
PollingDirectory.Refresh
AS400Directory.Refresh
TextYear.Text = Year(Now())
TextMonth.Text = Month(Now())


'Build Restaurant Listing

  Open "\\bns1\share1\BNEShare\Restaurant Accounting\Audit\program\unitlist.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
           RestaurantListing.AddItem unt
  Loop
  Close (1)
' Build missing information
    MissingPollFiles.Refresh
For X = 0 To RestaurantListing.ListCount - 1
    Found = False
    For Y = 0 To PollingDirectory.ListCount - 1
        If Mid$(RestaurantListing.List(X), 1, 4) = Mid$(PollingDirectory.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
            MissingList.AddItem Mid$(RestaurantListing.List(X), 1, 4)
        End If
Next X
MissingList.Refresh
DoEvents
If MissingList.ListCount = 0 Then
        MissingList.FontSize = 20
        MissingList.ForeColor = vbBlue
        MissingList.Height = 4200
        MissingList.FontBold = True
        MissingList.AddItem "100%"
        MissingLabel.Caption = "100% Received"
Else
    MissingLabel.Caption = Str(MissingList.ListCount) + " Restaurants Missing"
End If

Label2.Caption = Str(RestaurantListing.ListCount) + " Restaurants"
Label3.Caption = Str(PollingDirectory.ListCount) + " Audit Files"
MissingPollFiles.MousePointer = 0
End Sub
