VERSION 5.00
Begin VB.Form frmMain 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   0  'None
   ClientHeight    =   6795
   ClientLeft      =   105
   ClientTop       =   105
   ClientWidth     =   10485
   LinkTopic       =   "Form1"
   ScaleHeight     =   6795
   ScaleWidth      =   10485
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Label LblMain 
      Alignment       =   2  'Center
      Caption         =   "The inventory program is currently running. This will take a few minutes to complete. No assistance is required."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   3120
      TabIndex        =   0
      Top             =   2280
      Width           =   6495
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
Dim FirstLine As Boolean
Dim ItemNumber As Integer
Dim TempItemNumber As Integer
Dim period As Integer
Dim TempTestDate As Date
Dim StartDate As Date
Dim EndDate As Date
Dim TempBusinessday As Date
Dim I As Integer
Dim period1Begin As Date
Dim period1End As Date
Dim period2Begin As Date
Dim period2End As Date
Dim period3Begin As Date
Dim period3End As Date
Dim period4Begin As Date
Dim period4End As Date
Dim period5Begin As Date
Dim period5End As Date
Dim period6Begin As Date
Dim period6End As Date
Dim period7Begin As Date
Dim period7End As Date
Dim period8Begin As Date
Dim period8End As Date
Dim period9Begin As Date
Dim period9End As Date
Dim period10Begin As Date
Dim period10End As Date
Dim period11Begin As Date
Dim period11End As Date
Dim period12Begin As Date
Dim period12End As Date
Dim period13Begin As Date
Dim oldmenuitemsalesqty1 As Integer
Dim oldmenuitemsalesqty2 As Integer
Dim oldmenuitemsalesqty3 As Integer
Dim oldmenuitemsalesqty4 As Integer
Dim menuitemsalesqty1 As Integer
Dim menuitemsalesqty2 As Integer
Dim menuitemsalesqty3 As Integer
Dim menuitemsalesqty4 As Integer
Dim menusalestotal1 As Single
Dim menusalestotal2 As Single
Dim menusalestotal3 As Single
Dim menusalestotal4 As Single
               
FirstLine = True
frmMain.Show
DoEvents
StartDate = #12/31/1998#
TempTestDate = Date
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If

Errorcounter = 0
   period1Begin = StartDate
   period2Begin = DateAdd("ww", 4, StartDate)
   period1End = DateAdd("d", -1, period2Begin)
   period3Begin = DateAdd("ww", 4, period2Begin)
   period2End = DateAdd("d", -1, period3Begin)
   period4Begin = DateAdd("ww", 5, period3Begin)
   period3End = DateAdd("d", -1, period4Begin)
   period5Begin = DateAdd("ww", 4, period4Begin)
   period4End = DateAdd("d", -1, period5Begin)
   period6Begin = DateAdd("ww", 4, period5Begin)
   period5End = DateAdd("d", -1, period6Begin)
   period7Begin = DateAdd("ww", 5, period6Begin)
   period6End = DateAdd("d", -1, period7Begin)
   period8Begin = DateAdd("ww", 4, period7Begin)
   period7End = DateAdd("d", -1, period8Begin)
   period9Begin = DateAdd("ww", 4, period8Begin)
   period8End = DateAdd("d", -1, period9Begin)
   period10Begin = DateAdd("ww", 5, period9Begin)
   period9End = DateAdd("d", -1, period10Begin)
   period11Begin = DateAdd("ww", 4, period10Begin)
   period10End = DateAdd("d", -1, period11Begin)
   period12Begin = DateAdd("ww", 4, period11Begin)
   period11End = DateAdd("d", -1, period12Begin)
   period13Begin = DateAdd("ww", 5, period12Begin)
   period12End = DateAdd("d", -1, period13Begin)

    frmMain.MousePointer = 11
    Dim Connection As ADODB.Connection
    Set Connection = New ADODB.Connection
    Dim Update As New ADODB.Command             'UPDATE CMD LINE
    
    Dim OpenStoreId As New ADODB.Command        'EXTRACT THE STORE ID CMD
    Dim StoreId As New ADODB.Recordset          'EXTRACT THE STORE ID RS
    
    Dim OpenViewTime As New ADODB.Command       'EXTRACT TIME RS CMD
    Dim ViewRsTime As New ADODB.Recordset       'TIME DATA RS
    
    Dim OpenViewDate As New ADODB.Command       'EXTRACT DATE RS CMD
    Dim ViewRsDate As New ADODB.Recordset       'DATE DATA RS
    
    'SET THE CONNECT STRING
    Connection.ConnectionString = ("DSN=micros;UID=support;PWD=support")
    'OPEN CONNECTION AS NEW ODBC CONNECTION
    Connection.Open
    'SET THE ACTIVE CONNECTION FOR UPDATE
    Update.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR TIME
    OpenViewTime.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR SALES
    OpenViewDate.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR SALES
    OpenStoreId.ActiveConnection = Connection
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS A STORED PROCEDURE. IT WILL EXECUTE THE UPDATE THE TTLS TABLES
    Update.CommandType = adCmdStoredProc
    Update.CommandText = "micros.sp_postall"
    Update.Execute

Open "C:\TexRock\Invent.dat" For Output As #1 Len = 1000
Select Case TempTestDate
       Case Is >= period12Begin
                 StartDate = period11Begin
                 EndDate = period11End
                 period = 11
       Case Is >= period11Begin
                 StartDate = period10Begin
                 EndDate = period10End
                 period = 10
       Case Is >= period10Begin
                 StartDate = period9Begin
                 EndDate = period9End
                 period = 9
       Case Is >= period9Begin
                 StartDate = period8Begin
                 EndDate = period8End
                 period = 8
       Case Is >= period8Begin
                 StartDate = period7Begin
                 EndDate = period7End
                 period = 7
       Case Is >= period7Begin
                 StartDate = period6Begin
                 EndDate = period6End
                 period = 6
       Case Is >= period6Begin
                 StartDate = period5Begin
                 EndDate = period5End
                 period = 5
       Case Is >= period5Begin
                 StartDate = period4Begin
                 EndDate = period4End
                 period = 4
       Case Is >= period4Begin
                 StartDate = period3Begin
                 EndDate = period3End
                 period = 3
       Case Is >= period3Begin
                 StartDate = period2Begin
                 EndDate = period2End
                 period = 2
       Case Is >= period2Begin
                 StartDate = period1Begin
                 EndDate = period1End
                 period = 1
       Case Is >= period1Begin
                 StartDate = DateAdd("WW", -52, period12Begin)
                 EndDate = DateAdd("WW", -52, period12End)
                 period = 12
End Select

    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE TIME VIEW FOR ONE WEEK OF DATA
   ' OpenViewTime.CommandText = "select employee_number, last_name, first_name, payroll_id , job_number, override_regular_rate, override_overtime_rate, regular_hours, default_regular_rate, default_overtime_rate, overtime_hours from micros.v_r_employee_time_card where labor_week = (select max(labor_week)from micros.time_card_dtl) order by employee_number"   Old Line
OpenViewDate.CommandText = "select business_date from micros.rest_status"
OpenViewDate.CommandType = adCmdText
'CREATE THE RECORDSET FOR BUSINESS DATE
Set ViewRsDate = OpenViewDate.Execute
OpenViewTime.CommandText = "select menuitem_number,main_mlvl_name1,main_mlvl_name2,main_mlvl_name3,main_mlvl_name4,sub_mlvl_name1,sub_mlvl_name2,sub_mlvl_name3,sub_mlvl_name4,menuitem_name1,Price1,Price2,Price3,Price4,P1_sales_qty,P2_sales_qty,P3_sales_qty,P4_sales_qty,P1_sales_total,P2_sales_total,P3_sales_total,P4_sales_total,group_name, group_number,business_date from micros.v_R_rvc_menuitem_fam_grp where date(business_date) > date(now(*)) - 66 order by menuitem_number"
OpenViewTime.CommandType = adCmdText
    'CREATE THE RECORDSET FOR TIME DATA
Set ViewRsTime = OpenViewTime.Execute
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS A STORED PROCEDURE. IT WILL GET THE STORE ID
OpenStoreId.CommandText = "select location_name_1 from micros.rest_def"
OpenStoreId.CommandType = adCmdText
    'CREATE THE RECORDSET FOR TIME DATA
Set StoreId = OpenStoreId.Execute
Do While Not ViewRsTime.EOF
   TempBusinessday = ViewRsTime("business_date")
    If TempBusinessday >= StartDate And TempBusinessday <= EndDate Then
       If Not ViewRsTime.EOF Then
          ItemNumber = ViewRsTime("menuitem_number")
          If TempItemNumber <> ItemNumber Then
             If FirstLine = False Then
                If menuitemsalesqty1 > 0 Then
                   Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "1"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty1, "00000"), 5); Tab(73); Right(Format(menusalestotal1, "00000.00"), 8); Tab(83); Right(Format(menuitemprice1, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
                End If
                If menuitemsalesqty2 > 0 Then
                   Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "2"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty2, "00000"), 5); Tab(73); Right(Format(menusalestotal2, "00000.00"), 8); Tab(83); Right(Format(menuitemprice2, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
                End If
                If menuitemsalesqty3 > 0 Then
                   Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "3"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty3, "00000"), 5); Tab(73); Right(Format(menusalestotal3, "00000.00"), 8); Tab(83); Right(Format(menuitemprice3, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
                End If
                If menuitemsalesqty4 > 0 Then
                   Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "4"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty4, "00000"), 5); Tab(73); Right(Format(menusalestotal4, "00000.00"), 8); Tab(83); Right(Format(menuitemprice4, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
                End If
             End If
             menusalestotal1 = 0
             menusalestotal2 = 0
             menusalestotal3 = 0
             menusalestotal4 = 0
             menuitemsalesqty1 = 0
             menuitemsalesqty2 = 0
             menuitemsalesqty3 = 0
             menuitemsalesqty4 = 0
             menuitemprice1 = 0
             menuitemprice2 = 0
             menuitemprice3 = 0
             menuitemprice4 = 0
             oldmenuitemsalesqty1 = 0
             oldmenuitemsalesqty2 = 0
             oldmenuitemsalesqty3 = 0
             oldmenuitemsalesqty4 = 0
          End If
          FirstLine = False
          If ItemNumber = TempItemNumber Then
                menuitemsalesqty1 = (oldmenuitemsalesqty1 + ViewRsTime("P1_sales_qty"))
                menuitemsalesqty1 = Trim(menuitemsalesqty1)
                menuitemsalesqty2 = (oldmenuitemsalesqty2 + ViewRsTime("P2_sales_qty"))
                menuitemsalesqty2 = Trim(menuitemsalesqty2)
                menuitemsalesqty3 = (oldmenuitemsalesqty3 + ViewRsTime("P3_sales_qty"))
                menuitemsalesqty3 = Trim(menuitemsalesqty3)
                menuitemsalesqty4 = (oldmenuitemsalesqty3 + ViewRsTime("P4_sales_qty"))
                menuitemsalesqty4 = Trim(menuitemsalesqty4)
                If menuitemsalesqty1 > 0 Then
                   menusalestotal1 = (menusalestotal1 + ViewRsTime("P1_sales_total"))
                   menusalestotal1 = Trim(menusalestotal1)
                   menuitemprice1 = ViewRsTime("Price1")
                   If IsNull(menuitemprice1) Then
                      menuitemprice1 = "0.00"
                   End If
                   menuitemprice1 = Trim(menuitemprice1)
                   
                   oldmenuitemsalesqty1 = menuitemsalesqty1
                End If
                If menuitemsalesqty2 > 0 Then
                      menusalestotal2 = (menusalestotal2 + ViewRsTime("P2_sales_total"))
                      menusalestotal2 = Trim(menusalestotal2)
                      menuitemprice2 = ViewRsTime("Price2")
                      If IsNull(menuitemprice2) Then
                         menuitemprice2 = "0.00"
                      End If
                      menuitemprice2 = Trim(menuitemprice2)
                      oldmenuitemsalesqty2 = menuitemsalesqty2
                End If
                If menuitemsalesqty3 > 0 Then
                         menusalestotal3 = (menusalestotal3 + ViewRsTime("P3_sales_total"))
                         menusalestotal3 = Trim(menusalestotal3)
                         menuitemprice3 = ViewRsTime("Price3")
                         If IsNull(menuitemprice3) Then
                            menuitemprice3 = "0.00"
                         End If
                         menuitemprice3 = Trim(menuitemprice3)
                         oldmenuitemsalesqty3 = menuitemsalesqty3
                End If
                If menuitemsalesqty4 > 0 Then
                            menusalestotal4 = (menusalestotal4 + ViewRsTime("P4_sales_total"))
                            menusalestotal4 = Trim(menusalestotal4)
                            menuitemprice4 = ViewRsTime("Price4")
                            If IsNull(menuitemprice1) Then
                               menuitemprice4 = "0.00"
                            End If
                            menuitemprice4 = Trim(menuitemprice4)
                            oldmenuitemsalesqty4 = menuitemsalesqty4
                End If
                      
          Else
                menuitemnumber = ViewRsTime("menuitem_number")
                menuitemnumber = Trim(menuitemnumber)
                menuitemname = ViewRsTime("menuitem_name1")
                menuitemname = Trim(menuitemname)
                menuitemsalesqty1 = ViewRsTime("P1_sales_qty")
                menuitemsalesqty1 = Trim(menuitemsalesqty1)
                menuitemsalesqty2 = ViewRsTime("P2_sales_qty")
                menuitemsalesqty2 = Trim(menuitemsalesqty2)
                menuitemsalesqty3 = ViewRsTime("P3_sales_qty")
                menuitemsalesqty3 = Trim(menuitemsalesqty3)
                menuitemsalesqty4 = ViewRsTime("P4_sales_qty")
                menuitemsalesqty4 = Trim(menuitemsalesqty4)
                If menuitemsalesqty1 > 0 Then
                   menusalestotal1 = ViewRsTime("P1_sales_total")
                   menusalestotal1 = Trim(menusalestotal1)
                   menuitemprice1 = ViewRsTime("Price1")
                   If IsNull(menuitemprice1) Then
                      menuitemprice1 = "0.00"
                   End If
                   menuitemprice1 = Trim(menuitemprice1)
                   oldmenuitemsalesqty1 = menuitemsalesqty1
                End If
                If menuitemsalesqty2 > 0 Then
                   menusalestotal2 = ViewRsTime("P2_sales_total")
                   menusalestotal2 = Trim(menusalestotal2)
                   menuitemprice2 = ViewRsTime("Price2")
                   If IsNull(menuitemprice2) Then
                      menuitemprice2 = "0.00"
                   End If
                   menuitemprice2 = Trim(menuitemprice2)
                   oldmenuitemsalesqty2 = menuitemsalesqty2
                End If
                If menuitemsalesqty3 > 0 Then
                   menusalestotal3 = ViewRsTime("P3_sales_total")
                   menusalestotal3 = Trim(menusalestotal3)
                   menuitemprice3 = ViewRsTime("Price3")
                   If IsNull(menuitemprice3) Then
                      menuitemprice3 = "0.00"
                   End If
                   menuitemprice3 = Trim(menuitemprice3)
                   oldmenuitemsalesqty3 = menuitemsalesqty3
                End If
                If menuitemsalesqty4 > 0 Then
                   menusalestotal4 = ViewRsTime("P4_sales_total")
                   menusalestotal4 = Trim(menusalestotal4)
                   menuitemprice4 = ViewRsTime("Price4")
                   If IsNull(menuitemprice4) Then
                      menuitemprice4 = "0.00"
                   End If
                   menuitemprice4 = Trim(menuitemprice4)
                   oldmenuitemsalesqty4 = menuitemsalesqty4
                End If
                If menuitemprice > 0 Then
                   menuitemprice = menuitemprice
                Else
                   menuitemprice = "00.00"
                End If
                Groupnumber = ViewRsTime("group_number")
                Groupnumber = Trim(Groupnumber)
                If IsNull(Groupnumber) Then
                   Groupnumber = "00000"
                End If
                GroupName = ViewRsTime("group_name")
                GroupName = Trim(GroupName)
                If IsNull(GroupName) Then
                   GroupName = "No Name"
                End If
                TempStoreID = StoreId("location_name_1")
          End If
       End If
    End If
    TempItemNumber = ItemNumber
    ViewRsTime.MoveNext
Loop
If menuitemsalesqty1 > 0 Then
   Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "1"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty1, "00000"), 5); Tab(73); Right(Format(menusalestotal1, "00000.00"), 8); Tab(83); Right(Format(menuitemprice1, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
Else
   If menuitemsalesqty2 > 0 Then
      Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "2"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty2, "00000"), 5); Tab(73); Right(Format(menusalestotal2, "00000.00"), 8); Tab(83); Right(Format(menuitemprice2, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
   Else
      If menuitemsalesqty3 > 0 Then
         Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "3"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty3, "00000"), 5); Tab(73); Right(Format(menusalestotal3, "00000.00"), 8); Tab(83); Right(Format(menuitemprice3, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
      Else
         If menuitemsalesqty4 > 0 Then
            Print #1, Tab(2); Right(TempStoreID, 4); Tab(8); Format(period, "00"); Tab(12); Right(Format(Groupnumber, "00000"), 5); Tab(20); Left(GroupName, 15); Tab(37); "4"; Tab(40); Right(Format(menuitemnumber, "00000"), 5); Tab(48); Left(menuitemname, 15); Tab(65); Right(Format(menuitemsalesqty4, "00000"), 5); Tab(73); Right(Format(menusalestotal4, "00000.00"), 8); Tab(83); Right(Format(menuitemprice4, "000.00"), 5); Tab(90); Format(TempBusinessday, "mm/dd/yyyy")
         End If
      End If
   End If
End If
Close #1
frmMain.MousePointer = 0
frmMain.Hide
End
error_handler:
       Err.Clear
       Resume Next
End Sub














Private Sub test_Click()

End Sub
