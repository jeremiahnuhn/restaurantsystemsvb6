VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FF0000&
   BorderStyle     =   0  'None
   ClientHeight    =   13320
   ClientLeft      =   -600
   ClientTop       =   -615
   ClientWidth     =   17865
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   13320
   ScaleWidth      =   17865
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   6000
      Left            =   1320
      Top             =   1800
   End
   Begin VB.Timer Timer1 
      Interval        =   6000
      Left            =   1200
      Top             =   5520
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Print Food Prep Tags"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   1920
      TabIndex        =   1
      Top             =   720
      Width           =   8055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Return to the IRIS POS System"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   1920
      TabIndex        =   0
      Top             =   4680
      Width           =   8055
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00FF0000&
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   27.75
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1575
      Left            =   1920
      TabIndex        =   3
      Top             =   3000
      Width           =   8055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FF0000&
      Caption         =   "Version 5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1920
      TabIndex        =   2
      Top             =   7080
      Width           =   8055
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
        Label2.Caption = "Loading IRIS," + Chr(10) + "please wait..."
        Command1.Enabled = False
        Command2.Enabled = False
        X = Shell("C:\IRIS\Setup\RunVBScript.exe wscript.exe c:\iris\scripts\logon.vbs /startiris", vbMaximizedFocus)
        Timer1.Enabled = True
        X = 0
End Sub

Private Sub Command2_Click()
        Label2.Caption = "Loading Print Food Prep Tags, please wait..."
        Form1.Refresh
        Command2.Enabled = False
        Command1.Enabled = False
        X = Shell("c:\program files\lucas\foodpreptags.exe", vbMaximizedFocus)
        Timer2.Enabled = True
        X = 0
End Sub

Private Sub Form_Load()
        Label2.Caption = "Loading IRIS," + Chr(10) + "please wait..."
        Form1.Refresh
        X = 0

  
End Sub

Private Sub Timer1_Timer()

    X = X + 1
        'Label2.Caption = X
        'Form1.Refresh
If X = 30 Then
'If X = 2 Then
        Label2.Caption = ""
        Command1.Enabled = True
        Command2.Enabled = True
        Timer1.Enabled = False
    End If
    DoEvents
    Timer1.Enabled = True

End Sub

Private Sub Timer2_Timer()
    X = X + 1
        'Label2.Caption = X
        'Form1.Refresh
    If X = 4 Then
        Label2.Caption = ""
        Command1.Enabled = True
        Command2.Enabled = True
        Timer2.Enabled = False
    End If
    DoEvents
    Timer2.Enabled = True
End Sub
