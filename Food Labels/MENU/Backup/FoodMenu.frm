VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FF0000&
   BorderStyle     =   0  'None
   ClientHeight    =   13320
   ClientLeft      =   -600
   ClientTop       =   -615
   ClientWidth     =   17865
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   13320
   ScaleWidth      =   17865
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command2 
      Caption         =   "Print Food Prep Tags"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   1920
      TabIndex        =   1
      Top             =   1200
      Width           =   8055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Return to the IRIS POS System"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   1920
      TabIndex        =   0
      Top             =   3960
      Width           =   8055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FF0000&
      Caption         =   "Version 1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1920
      TabIndex        =   2
      Top             =   6360
      Width           =   8055
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
        X = Shell("C:\IRIS\Setup\RunVBScript.exe wscript.exe c:\iris\scripts\logon.vbs /startiris", vbMaximizedFocus)
End Sub

Private Sub Command2_Click()
        X = Shell("c:\program files\lucas\foodpreptags.exe", vbMaximizedFocus)
End Sub

Private Sub Form_Load()
'        X = Shell("C:\IRIS\Setup\RunVBScript.exe wscript.exe c:\iris\scripts\logon.vbs /startiris", vbMinimizedFocus)
End Sub
