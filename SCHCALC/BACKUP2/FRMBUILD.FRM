VERSION 4.00
Begin VB.Form FrmBuild 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Build Schedule"
   ClientHeight    =   6150
   ClientLeft      =   1005
   ClientTop       =   1485
   ClientWidth     =   9015
   ControlBox      =   0   'False
   Height          =   6555
   Left            =   945
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6150
   ScaleWidth      =   9015
   Top             =   1140
   Width           =   9135
   Begin VB.CommandButton CmdPrintAllocationWeek2 
      Caption         =   "Print Schedule Allocation"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4680
      TabIndex        =   7
      Top             =   3240
      Width           =   3975
   End
   Begin VB.CommandButton CmdPrintAllocationWeek1 
      Caption         =   "Print Schedule Allocation"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   6
      Top             =   3240
      Width           =   3975
   End
   Begin VB.CommandButton CmdPrintWeek2 
      Caption         =   "Print Schedule"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4680
      TabIndex        =   5
      Top             =   2160
      Width           =   3975
   End
   Begin VB.CommandButton CmdPrintWeek1 
      Caption         =   "Print Schedule"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   4
      Top             =   2160
      Width           =   3975
   End
   Begin VB.CommandButton CmdBuildWeek2 
      Caption         =   "Build Schedule"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4680
      TabIndex        =   3
      Top             =   1080
      Width           =   3975
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "E&xit Program"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   14.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2160
      TabIndex        =   1
      Top             =   4320
      Width           =   3975
   End
   Begin VB.CommandButton CmdBuildWeek1 
      Caption         =   "Build Schedule"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   0
      Top             =   1080
      Width           =   3975
   End
   Begin VB.Label LblVersion 
      BackColor       =   &H00C0C0C0&
      Caption         =   "V1.4"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8400
      TabIndex        =   10
      Top             =   5760
      Width           =   495
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Schedule should be finalized each week by Sunday at noon."
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   375
      Left            =   960
      TabIndex        =   9
      Top             =   5760
      Width           =   6735
   End
   Begin VB.Label LblRestName 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      TabIndex        =   8
      Top             =   0
      Width           =   5295
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Schedule Calculator"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   18
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1800
      TabIndex        =   2
      Top             =   360
      Width           =   5535
   End
End
Attribute VB_Name = "FrmBuild"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Public FirstWeekMonday As String
Public SecondWeekMonday As String
Private Sub CmdBuild_Click()
FrmEmpSc.Show
FrmBuild.Hide
Unload FrmBuild
End Sub

Private Sub CmdBuildWeek1_Click()
WeekSelected = FirstWeekMonday
ReadTimeFile
FrmBuild.Hide
Unload FrmBuild
FrmEmpSc.Show
End Sub

Private Sub CmdBuildWeek2_Click()
WeekSelected = SecondWeekMonday
ReadTimeFile
FrmBuild.Hide
Unload FrmBuild
FrmEmpSc.Show
End Sub

Private Sub CmdExit_Click()
End
End Sub

Private Sub CmdPrintAllocationWeek1_Click()
FrmMessage.Show
Screen.MousePointer = 11
FrmMessage.Caption = "                                                                   Printing"
WeekSelected = FirstWeekMonday
ReadTimeFile
CalculateScheduleAllocation
PrintScheduleAllocation
End Sub

Private Sub CmdPrintAllocationWeek2_Click()
FrmMessage.Show
Screen.MousePointer = 11
FrmMessage.Caption = "                                                                   Printing"
WeekSelected = SecondWeekMonday
ReadTimeFile
CalculateScheduleAllocation
PrintScheduleAllocation
End Sub

Private Sub CmdPrintLabor_Click()
WeekSelected = FirstWeekMonday
ReadTimeFile
ReadCat08
PrintLaborReport
End Sub

Private Sub CmdPrintSales_Click()
WeekSelected = FirstWeekMonday
ReadTimeFile
ReadCat07
ReadSalesProjection
PrintSalesAndLabor
End Sub

Private Sub CmdPrintWeek1_Click()
FrmMessage.Show
Screen.MousePointer = 11
FrmMessage.Caption = "                                                                 Calculating"
WeekSelected = FirstWeekMonday
ReadTimeFile
ReadSalesProjectionFile
If WeeklyAllowedLabor = 0 Then
    Screen.MousePointer = 0
    FrmMessage.Hide
    Unload FrmMessage
   Exit Sub
End If
CalculateScheduleAllocation
WriteLaborFile
FrmBuild.Hide
FrmFinal.Show
End Sub

Private Sub CmdPrintWeek2_Click()
FrmMessage.Show
Screen.MousePointer = 11
FrmMessage.Caption = "                                                                 Calculating"
WeekSelected = SecondWeekMonday
ReadTimeFile
ReadSalesProjectionFile
If WeeklyAllowedLabor = 0 Then
    Screen.MousePointer = 0
    FrmMessage.Hide
    Unload FrmMessage
   Exit Sub
End If
CalculateScheduleAllocation
WriteLaborFile
FrmBuild.Hide
FrmFinal.Show
End Sub

Private Sub CmdRestType_Click()

End Sub

Private Sub Form_Load()
Dim TempDate As String
Dim StartTime As String
Dim RestNum As Single
Screen.MousePointer = 0
Draft = False
Override = False
FrmBuild.Top = (Screen.Height - FrmBuild.Height) / 2
FrmBuild.Left = (Screen.Width - FrmBuild.Width) / 2
TempDate = Format(Date, "mm/dd")
Currentday = WeekDay(Date)
Select Case Currentday
   Case 1
      TempDate = DateAdd("d", "-6", TempDate)
   Case 2
      TempDate = DateAdd("d", "-7", TempDate)
   Case 3
      TempDate = DateAdd("d", "-1", TempDate)
   Case 4
      TempDate = DateAdd("d", "-2", TempDate)
   Case 5
      TempDate = DateAdd("d", "-3", TempDate)
   Case 6
      TempDate = DateAdd("d", "-4", TempDate)
   Case 7
      TempDate = DateAdd("d", "-5", TempDate)
End Select
FirstWeekMonday = TempDate
SecondWeekMonday = DateAdd("d", "7", FirstWeekMonday)
CmdBuildWeek1.Caption = "Build Week Schedule " & DateAdd("d", 1, FirstWeekMonday)
CmdBuildWeek2.Caption = "Build Week Schedule " & DateAdd("d", 1, SecondWeekMonday)
CmdPrintWeek1.Caption = "Print Week Schedule " & DateAdd("d", 1, FirstWeekMonday)
CmdPrintWeek2.Caption = "Print Week Schedule " & DateAdd("d", 1, SecondWeekMonday)
CmdPrintAllocationWeek1.Caption = "Print Schedule Allocation " & DateAdd("d", 1, FirstWeekMonday)
CmdPrintAllocationWeek2.Caption = "Print Schedule Allocation " & DateAdd("d", 1, SecondWeekMonday)
StartTime = #12:00:00 AM#
TempIn(1) = Format(StartTime, "hh:mm AM/PM")
TempOut(1) = Format(StartTime, "hh:mm AM/PM")
For A = 1 To 95
   TempIn(A + 1) = Format(DateAdd("n", "15", TempIn(A)), "hh:mm AM/PM")
   TempOut(A + 1) = Format(DateAdd("n", "15", TempOut(A)), "hh:mm AM/PM")
Next A
If Dir("C:\Nodesys\unitid.dat") <> "" Then
   Open "C:\Nodesys\UnitId.dat" For Input As #1
      Line Input #1, Restname
      RestNum = Val(Left(Restname, 4))
      Restname = Mid(Restname, 5, 24)
   Close #1
Else
   Restname = ""
End If
If RestNum >= 6500 And RestNum < 6600 Then
   RestType = 2
Else
   RestType = 1
End If
ReadPOSFile
LblRestName.Caption = Restname
End Sub


Private Sub RestType_Click()

End Sub


