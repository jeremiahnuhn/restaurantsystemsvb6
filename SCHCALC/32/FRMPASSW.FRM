VERSION 5.00
Begin VB.Form FrmPassW 
   Caption         =   "Password"
   ClientHeight    =   3585
   ClientLeft      =   1185
   ClientTop       =   1770
   ClientWidth     =   7335
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3585
   ScaleWidth      =   7335
   Begin VB.CommandButton CmdExit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4560
      TabIndex        =   2
      Top             =   2520
      Width           =   2175
   End
   Begin VB.CommandButton CmdEnter 
      Caption         =   "Enter Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   600
      TabIndex        =   1
      Top             =   2520
      Width           =   2415
   End
   Begin VB.TextBox TxtPassword 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2760
      TabIndex        =   0
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label Label1 
      Caption         =   $"FRMPASSW.frx":0000
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   720
      TabIndex        =   3
      Top             =   240
      Width           =   6135
   End
End
Attribute VB_Name = "FrmPassW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdEnter_Click()
Dim Password As Integer
Password = Month(Now) * Day(Now)
If IsNumeric(TxtPassword.Text) Then
   If Val(TxtPassword.Text) = Password Then
      Override = True
   Else
      Override = False
      MsgBox "Incorrect Password"
   End If
   FrmPassW.Hide
   Unload FrmPassW
   FrmFinal.Show
Else
   MsgBox "Please enter a whole number"
End If
End Sub

Private Sub CmdExit_Click()
FrmPassW.Hide
Unload FrmPassW
FrmBuild.Show
End Sub


Private Sub Form_Load()
FrmPassW.Top = (Screen.Height - FrmPassW.Height) / 2
FrmPassW.Left = (Screen.Width - FrmPassW.Width) / 2
End Sub


