VERSION 5.00
Begin VB.Form MissingPollFiles 
   Caption         =   "Missing Poll Files"
   ClientHeight    =   4680
   ClientLeft      =   735
   ClientTop       =   1200
   ClientWidth     =   12705
   Icon            =   "MissingPollFiles.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4680
   ScaleWidth      =   12705
   Begin VB.CheckBox ftp 
      Caption         =   "Use FTP Backup"
      Height          =   375
      Left            =   2400
      TabIndex        =   14
      Top             =   4200
      Width           =   1695
   End
   Begin VB.FileListBox AS400Directory 
      Height          =   1455
      Left            =   10680
      TabIndex        =   12
      Top             =   4920
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.FileListBox FlagDirectory 
      Height          =   1455
      Left            =   8400
      Pattern         =   "*.txt"
      TabIndex        =   7
      Top             =   4920
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.FileListBox PollingDirectory 
      Height          =   1455
      Left            =   5280
      Pattern         =   "*.IRS;*.EPL"
      TabIndex        =   6
      Top             =   4920
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CheckBox HardeesOnly 
      Caption         =   "Only Hardee's Concept"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   4200
      Width           =   2055
   End
   Begin VB.ListBox MissingList 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3840
      Left            =   240
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   360
      Width           =   9855
   End
   Begin VB.ListBox RestaurantListing 
      Height          =   1425
      Left            =   240
      TabIndex        =   4
      Top             =   4920
      Visible         =   0   'False
      Width           =   4335
   End
   Begin VB.CommandButton UpdateButton 
      Caption         =   "&Update"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   10440
      TabIndex        =   1
      Top             =   360
      Width           =   1935
   End
   Begin VB.CommandButton PrintButton 
      Caption         =   "&Print"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   10440
      TabIndex        =   2
      Top             =   1800
      Width           =   1935
   End
   Begin VB.CommandButton ExitButton 
      Caption         =   "&Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   10440
      TabIndex        =   0
      Top             =   3240
      Width           =   1935
   End
   Begin VB.Label AS400Label 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   10200
      TabIndex        =   13
      Top             =   4320
      Width           =   2415
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Version 7"
      Height          =   255
      Left            =   11520
      TabIndex        =   11
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Please contact Jody Smith for program support."
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   4200
      TabIndex        =   10
      Top             =   4320
      Width           =   3615
   End
   Begin VB.Label Label1 
      Caption         =   $"MissingPollFiles.frx":0442
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   120
      Width           =   9855
   End
   Begin VB.Label MissingLabel 
      Alignment       =   1  'Right Justify
      Caption         =   "Click on update to continue..."
      Height          =   255
      Left            =   7920
      TabIndex        =   8
      Top             =   4320
      Width           =   2175
   End
End
Attribute VB_Name = "MissingPollFiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ExitButton_Click()
End
End Sub

Private Sub Form_Load()
'If Hour(Now) >= 15 Then HardeesOnly.Value = 1
On Error Resume Next
If Dir("\\bns1\share1\polling\Data\Restaurant Listing.csv") = "" Then
    m = MsgBox("The BNS1 erver is not available, please contact Technical Servcies for assistance.", vbCritical, "Missing Poll Files: Server Access Error")
    End
End If
End Sub

Private Sub ftp_Click()
If ftp.Value = 1 Then
    m = MsgBox("Reminder to make sure you FTP the files to the \\BNS1\Share1\Polling\Poll\FTPCheck folder.", vbInformational, "FTP Reminder")
End If
    Call UpdateButton_Click
End Sub

Private Sub HardeesOnly_Click()
Call UpdateButton_Click
End Sub

Private Sub PrintButton_Click()
MissingPollFiles.MousePointer = 11
Dim today As Date
Dim PageNum As Integer
Dim V As Integer
Dim W As Integer
Dim X As Integer
Dim Y As Integer
Dim Z As Integer
W = 1
Z = 5
V = 0
PageNum = 1
Y = 2200
   Printer.FontName = "COURIER NEW"
   Printer.FontBold = True
   Printer.FontSize = 14
   today = Format(Now, "ddddd")
   Printer.CurrentX = 3400
   Printer.CurrentY = 500
   Printer.Print "BODDIE-NOELL ENTERPRISES, INC."
   Printer.FontSize = 10
   Printer.Print "                  Reminder: All concepts are Priority 1 if missed on Tuesday."
   Printer.Print "                       "
   Printer.FontSize = 12
   Printer.CurrentX = 2400
   Printer.CurrentY = 1400
   Printer.Print "Missing Poll Files as of " + Str(Now)
   Printer.Print
   Printer.FontSize = 10: Printer.FontBold = False
   Printer.CurrentX = 1215
   Printer.CurrentY = 1800
   Printer.FontUnderline = True
   Printer.Print " Concept  ####    Restaurant         Speed #  Error Condition   "
   Printer.FontUnderline = False
   Printer.Print " "
   Z = 0
For I = 0 To MissingList.ListCount - 1
   Z = Z + 1
   Printer.FontSize = 10
   Printer.FontBold = True
   Printer.Print "           " + MissingList.List(I)
If Mid$(MissingList.List(I), 6, 2) <> "10" Then
   Printer.FontBold = False
   Printer.CurrentX = 1200
   Printer.CurrentY = Y + 800
   Printer.FontSize = 8
   Printer.Print "Manager on Duty"
   Printer.Line (2800, Y + 950)-(4800, Y + 950)
   Printer.CurrentX = 5000
   Printer.CurrentY = Y + 800
   Printer.Print "Title"
   Printer.Line (5600, Y + 950)-(6900, Y + 950)
   Printer.CurrentX = 7100
   Printer.CurrentY = Y + 800
   Printer.Print "Days Missed"
   Printer.Line (8300, Y + 950)-(9200, Y + 950)
   Printer.CurrentX = 9300
   Printer.CurrentY = Y + 800
   Printer.Print " "
   Printer.CurrentX = 1200
   Printer.CurrentY = Y + 1400
   Printer.Print "Solution"
   Printer.Line (2100, Y + 1550)-(8700, Y + 1550)
   Printer.CurrentX = 8800
   Printer.CurrentY = Y + 1400
   Printer.Print "Missed  Y  N"
   Printer.Print " "
End If
   Y = Y + 1900
    If Z = 6 Then
       Printer.CurrentX = 10000
       Printer.CurrentY = 14500
       Printer.Print "Page " & PageNum
       PageNum = PageNum + 1
       Printer.NewPage
   Printer.FontSize = 10: Printer.FontBold = False
   Printer.CurrentX = 1215
   Printer.CurrentY = 1800
   Printer.FontUnderline = True
   Printer.Print " Concept  ####    Restaurant Name    Speed #  Error Condition   "
   Printer.FontUnderline = False
   Printer.Print " "
   Z = 0
   Y = 2200
    End If
Next I
Printer.CurrentX = 1200
Printer.CurrentY = 13500
Printer.Print "Help Desk Operator"
Printer.Line (3200, 13650)-(7600, 13650)
Printer.CurrentX = 7700
Printer.CurrentY = 13500
Printer.Print "Time Completed"
Printer.Line (9200, 13650)-(10200, 13650)
Printer.CurrentX = 10000
Printer.CurrentY = 14500
Printer.Print "Page " & PageNum
Printer.EndDoc
  m = MsgBox("Printing complete.", vbInformation, "Missing Poll Files: Print Status")

MissingPollFiles.MousePointer = 0
End Sub

Private Sub UpdateButton_Click()
MissingPollFiles.MousePointer = 11
MissingLabel.Caption = "Updating ."
AS400Label.Caption = ""
RestaurantListing.Clear
MissingList.Clear
MissingList.FontSize = 12
MissingList.ForeColor = vbBlack
MissingList.Height = 3840
MissingList.FontBold = False
If ftp.Value = 1 Then
    PollingDirectory.FileName = "\\bns1\share1\polling\Poll\FTPCheck"
Else
    PollingDirectory.FileName = "\\bns1\share1\polling\Poll\Processed"
End If
FlagDirectory.FileName = "\\bns1\share1\polling\Poll\Flag"
AS400Directory.FileName = "\\bns1\share1\polling\AS400\"
FlagDirectory.Refresh
PollingDirectory.Refresh
AS400Directory.Refresh


'Build Restaurant Listing

  Open "\\bns1\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
    If HardeesOnly.Value = 1 Then
        If Mid$(unt, 1, 1) = "H" Then
            RestaurantListing.AddItem unt
        End If
    Else
           RestaurantListing.AddItem unt
    End If
  Loop
  Close (1)
' Build missing information
    MissingLabel.Caption = "Updating . . ."
    MissingPollFiles.Refresh
For X = 0 To RestaurantListing.ListCount - 1
    Found = False
    For Y = 0 To PollingDirectory.ListCount - 1
        If Mid$(RestaurantListing.List(X), 4, 4) = Mid$(PollingDirectory.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        Select Case Mid$(RestaurantListing.List(X), 1, 1)
            Case "D"
                RestaurantType = "RoseHill "
            Case "T"
                RestaurantType = "Texas    "
            Case "H"
                RestaurantType = "Hardee's "
            Case "M"
                RestaurantType = "Moe's    "
            Case "C"
                RestaurantType = "Cafe     "
            Case "F"
                RestaurantType = "Diner    "
            Case "W"
                RestaurantType = "Winery   "
            End Select
        Found = False
        For Y = 0 To FlagDirectory.ListCount - 1
            If Mid$(RestaurantListing.List(X), 4, 4) = Mid$(FlagDirectory.List(Y), 1, 4) Then
                Found = True
            End If
        Next Y
        If Found = True Then
            Errormsg = "No POS Data        "
            Else
            Errormsg = "Communication Error"
        End If
        If ftp.Value = 1 Then
            Errormsg = " "
        End If
        MissingList.AddItem RestaurantType + Mid$(RestaurantListing.List(X), 4, 4) + "  " + Mid$(RestaurantListing.List(X), 9, 21) + " " + Mid$(RestaurantListing.List(X), 40, 4) + "  " + Errormsg
    End If
    MissingLabel.Caption = "Updating . . ."
Next X
MissingList.Refresh
DoEvents
If AS400Directory.ListCount > 0 Then
    AS400Label.Caption = Str(AS400Directory.ListCount) + " Files to Transfer"
End If
MissingLabel.Caption = Str(MissingList.ListCount) + " Restaurants Missing"
If MissingList.ListCount = 0 Then
        MissingList.FontSize = 20
        MissingList.ForeColor = vbBlue
        MissingList.Height = 4200
        MissingList.FontBold = True
        MissingList.AddItem "     100% of the restaurants received."
        MissingLabel.Caption = "No restaurants missing."
Else
    MissingLabel.Caption = Str(MissingList.ListCount) + " Restaurants Missing"
End If
If MissingList.ListCount = 1 And Mid$(MissingList.List(0), 1, 1) <> " " Then
    MissingLabel.Caption = Str(MissingList.ListCount) + " Restaurant Missing"
End If
PrintButton.Enabled = True
MissingPollFiles.MousePointer = 0
End Sub
