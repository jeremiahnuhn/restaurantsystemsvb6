VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form DeskTop 
   Appearance      =   0  'Flat
   BackColor       =   &H0000FFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hardee's Desktop"
   ClientHeight    =   6540
   ClientLeft      =   600
   ClientTop       =   1095
   ClientWidth     =   13050
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   FillColor       =   &H00FFFFFF&
   FillStyle       =   0  'Solid
   ForeColor       =   &H00FF0000&
   Icon            =   "Desktop.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6540
   ScaleWidth      =   13050
   Begin VB.CommandButton Command1 
      Caption         =   "Old XcelleNet Mail"
      Height          =   495
      Left            =   6720
      TabIndex        =   8
      ToolTipText     =   "Print forms needed for new employee orientation."
      Top             =   5400
      Width           =   2175
   End
   Begin VB.CommandButton PrintO 
      Caption         =   "Print &Orientation Forms"
      Height          =   495
      Left            =   10080
      TabIndex        =   7
      ToolTipText     =   "Print forms needed for new employee orientation."
      Top             =   5400
      Width           =   2175
   End
   Begin VB.CommandButton LiveReporter 
      Caption         =   "Live&Reporter"
      Height          =   495
      Left            =   10560
      TabIndex        =   6
      ToolTipText     =   "Print Reports, Paper Forms, and access Electronic Manuals."
      Top             =   3240
      Width           =   2175
   End
   Begin VB.CommandButton Exit 
      Caption         =   "EXI&T"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   360
      TabIndex        =   4
      Top             =   4320
      Width           =   5895
   End
   Begin VB.CommandButton LoadLiveConnect 
      Caption         =   "&Live Connect"
      Height          =   495
      Left            =   9720
      TabIndex        =   3
      ToolTipText     =   "Update Outlook (Send and receive mail)"
      Top             =   4680
      Width           =   2175
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Left            =   9960
      TabIndex        =   0
      Top             =   -360
      Width           =   2655
      Begin VB.CommandButton CmdOutlook 
         Caption         =   "&Outlook"
         Height          =   495
         Left            =   240
         TabIndex        =   1
         ToolTipText     =   "Electronic mail, calendar, task list, update with Live Connect."
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   3360
      TabIndex        =   2
      Top             =   6480
      Visible         =   0   'False
      Width           =   1575
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4335
      Left            =   600
      TabIndex        =   9
      Top             =   0
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   7646
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Communication Tools"
      TabPicture(0)   =   "Desktop.frx":0442
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Command3"
      Tab(0).Control(1)=   "Command2"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "Desktop.frx":045E
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Command4"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin VB.CommandButton Command4 
         Caption         =   "Command4"
         Height          =   1815
         Left            =   2280
         TabIndex        =   12
         Top             =   1560
         Width           =   5535
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Command3"
         Height          =   1095
         Left            =   -74040
         TabIndex        =   11
         Top             =   2400
         Width           =   1935
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   855
         Left            =   -74280
         TabIndex        =   10
         Top             =   1080
         Width           =   1455
      End
   End
   Begin VB.Label LblVersion 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0000FFFF&
      Caption         =   "Version 1.0"
      Height          =   255
      Left            =   5160
      TabIndex        =   5
      Top             =   5040
      Width           =   975
   End
End
Attribute VB_Name = "DeskTop"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Public unitnum As Integer
 Public IJ As Integer
 Public SSDone As Boolean
 'Option Explicit

      Private Type LUID
         UsedPart As Long
         IgnoredForNowHigh32BitPart As Long
      End Type
      
      Private Type TOKEN_PRIVILEGES
        PrivilegeCount As Long
        TheLuid As LUID
        Attributes As Long
      End Type

      Private Const EWX_SHUTDOWN As Long = 1
      Private Const EWX_FORCE As Long = 4
      Private Const EWX_REBOOT = 2
      Private Declare Function ExitWindowsEx Lib "user32" (ByVal _
           dwOptions As Long, ByVal dwReserved As Long) As Long

      Private Declare Function GetCurrentProcess Lib "kernel32" () As Long
      Private Declare Function OpenProcessToken Lib "advapi32" (ByVal _
         ProcessHandle As Long, _
         ByVal DesiredAccess As Long, TokenHandle As Long) As Long
      Private Declare Function LookupPrivilegeValue Lib "advapi32" _
         Alias "LookupPrivilegeValueA" _
         (ByVal lpSystemName As String, ByVal lpName As String, lpLuid _
         As LUID) As Long
      Private Declare Function AdjustTokenPrivileges Lib "advapi32" _
         (ByVal TokenHandle As Long, _
         ByVal DisableAllPrivileges As Long, NewState As TOKEN_PRIVILEGES _
         , ByVal BufferLength As Long, _
      PreviousState As TOKEN_PRIVILEGES, ReturnLength As Long) As Long
Public Function GetAppHwnd(Optional ByVal Class As String = vbNullString, Optional ByVal Caption As String = vbNullString) As Long
GetAppHwnd = FindWindow(Class, Caption)
End Function


Private Sub Command1_Click()

ChDir "C:\nodesys"
Y = Shell("C:\Nodesys\mail.exe 30", vbNormalFocus)
End

End Sub

Private Sub Exit_Click()
End
End Sub
Private Sub CmdExcel_Click()
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("c:\Program Files\Microsoft Office\Office10\Excel.exe", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub
Private Sub CmdIE_Click()
'Y = Shell("c:\NODESYS\WINXNODE.EXE -R", vbNormalFocus)
'SendKeys "{ENTER}", True

 'IntMsg = MsgBox("Remember to click on the Disconnect button after you have finished using the Internet." + Chr(10) + Chr(10) + "After you click on the Disconnect button, then click on the Hang Up button, then answer Yes, and then OK.", 6, "Internet")
Y = Shell("C:\PROGRAM FILES\Dial-Up Monitor\DMONITOR.EXE /dial:InternetConnection", vbNormalFocus)
End
End Sub
Private Sub CmdOutlook_Click()
    If Dir("C:\Program Files\Microsoft Office\Office11\OUTLOOK.EXE") <> "" Then
        LOADPCAW = Shell("C:\Program Files\Microsoft Office\Office11\Outlook.exe /recycle", vbMaximizedFocus)
    Else
        LOADPCAW = Shell("c:\Program Files\Microsoft Office\Office10\outlook.exe  /recycle", vbMaximizedFocus)
    End If
    End
End Sub
Private Sub CmdSpreadCurrent_Click()
Dim Tempperiod As String
Tempperiod = period
If Tempperiod < 10 Then
   Tempperiod = "0" & Tempperiod
End If
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("C:\Program Files\Microsoft Office\Office10\Excel.exe C:\MSOffice\Excel\SI" & Tempperiod & ".xls", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub

Private Sub CmdSpreadPrevious_Click()
Dim Tempperiod As String
If period = 1 Then
   Tempperiod = 12
Else
   Tempperiod = period - 1
End If
If Tempperiod < 10 Then
   Tempperiod = "0" & Tempperiod
End If
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
        Y = Shell("C:\Program Files\Microsoft Office\Office10\Excel.exe C:\MSOffice\Excel\SI" & Tempperiod & ".xls", vbMaximizedFocus)
        End
Else
   Err.Clear
End If
End Sub

Private Sub CmdWord_Click()
On Error GoTo BypassApp
AppActivate "Microsoft Word"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("c:\Program Files\Microsoft Office\Office10\Winword.exe", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub

Private Sub CmdXcellenet_Click()
Y = Shell("C:\Nodesys\NDeskTop.exe 30 gm" & unitnum & " gm" & unitnum, vbNormalFocus)
End
End Sub
Private Sub Form_Load()
Dim Message As String
Dim TempTestDate As Date
Dim StartDate As Date
Dim EndDate As Date
Dim TempBusinessday As Date
Dim period1Begin As Date
Dim period1End As Date
Dim period2Begin As Date
Dim period2End As Date
Dim period3Begin As Date
Dim period3End As Date
Dim period4Begin As Date
Dim period4End As Date
Dim period5Begin As Date
Dim period5End As Date
Dim period6Begin As Date
Dim period6End As Date
Dim period7Begin As Date
Dim period7End As Date
Dim period8Begin As Date
Dim period8End As Date
Dim period9Begin As Date
Dim period9End As Date
Dim period10Begin As Date
Dim period10End As Date
Dim period11Begin As Date
Dim period11End As Date
Dim period12Begin As Date
Dim period12End As Date
Dim period13Begin As Date
Dim tempunitnum As String
Check = True
POS2800 = False
SSDone = False
If Dir("C:\Nodesys\Unitid.dat") <> "" Then
   Open "C:\Nodesys\Unitid.dat" For Input As #1
      Input #1, tempunitnum
   Close #1
Else
   MsgBox "Unit id does not exist, Please call Help Desk at (252) 937-2800, ext 1437"
End If
unitnum = Int(Left(tempunitnum, 4))
'Picture1.Picture = LoadPicture("C:\Windows\TexLogo.jpg")
StartDate = #12/31/1998#
TempTestDate = Date
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
   period1Begin = StartDate
   period2Begin = DateAdd("ww", 4, StartDate)
   period1End = DateAdd("d", -1, period2Begin)
   period3Begin = DateAdd("ww", 4, period2Begin)
   period2End = DateAdd("d", -1, period3Begin)
   period4Begin = DateAdd("ww", 5, period3Begin)
   period3End = DateAdd("d", -1, period4Begin)
   period5Begin = DateAdd("ww", 4, period4Begin)
   period4End = DateAdd("d", -1, period5Begin)
   period6Begin = DateAdd("ww", 4, period5Begin)
   period5End = DateAdd("d", -1, period6Begin)
   period7Begin = DateAdd("ww", 5, period6Begin)
   period6End = DateAdd("d", -1, period7Begin)
   period8Begin = DateAdd("ww", 4, period7Begin)
   period7End = DateAdd("d", -1, period8Begin)
   period9Begin = DateAdd("ww", 4, period8Begin)
   period8End = DateAdd("d", -1, period9Begin)
   period10Begin = DateAdd("ww", 5, period9Begin)
   period9End = DateAdd("d", -1, period10Begin)
   period11Begin = DateAdd("ww", 4, period10Begin)
   period10End = DateAdd("d", -1, period11Begin)
   period12Begin = DateAdd("ww", 4, period11Begin)
   period11End = DateAdd("d", -1, period12Begin)
   period13Begin = DateAdd("ww", 5, period12Begin)
   period12End = DateAdd("d", -1, period13Begin)

Select Case TempTestDate
       Case Is >= period12Begin
                 StartDate = period11Begin
                 EndDate = period11End
                 period = 12
       Case Is >= period11Begin
                 StartDate = period10Begin
                 EndDate = period10End
                 period = 11
       Case Is >= period10Begin
                 StartDate = period9Begin
                 EndDate = period9End
                 period = 10
       Case Is >= period9Begin
                 StartDate = period8Begin
                 EndDate = period8End
                 period = 9
       Case Is >= period8Begin
                 StartDate = period7Begin
                 EndDate = period7End
                 period = 8
       Case Is >= period7Begin
                 StartDate = period6Begin
                 EndDate = period6End
                 period = 7
       Case Is >= period6Begin
                 StartDate = period5Begin
                 EndDate = period5End
                 period = 6
       Case Is >= period5Begin
                 StartDate = period4Begin
                 EndDate = period4End
                 period = 5
       Case Is >= period4Begin
                 StartDate = period3Begin
                 EndDate = period3End
                 period = 4
       Case Is >= period3Begin
                 StartDate = period2Begin
                 EndDate = period2End
                 period = 3
       Case Is >= period2Begin
                 StartDate = period1Begin
                 EndDate = period1End
                 period = 2
       Case Is >= period1Begin
                 StartDate = DateAdd("WW", -52, period12Begin)
                 EndDate = DateAdd("WW", -52, period12End)
                 period = 1
End Select

End Sub



Private Sub LiveReporter_Click()
        LOADPCAW = Shell("C:\Program Files\Religent\LiveReporter Client\LiveReporter Client.exe", vbMaximizedFocus)
        End
End Sub

Private Sub LoadLiveConnect_Click()
    ChDir "c:\Program Files\Religent\LiveConnect 5.2 BNE"
    progname = "\Program Files\Religent\LiveConnect 5.2 BNE\startup.exe -skin LiveConnect"
    ' -sp Profile Execution Complete
    X = Shell(progname, vbNormalFocus)
    End
End Sub

Private Sub Timer1_Timer()
Dim I As Integer
Dim J As Integer
Dim TempMessage As String
Dim Message As String
Dim ID As Long
Dim TempTestDate As Date
Dim StartDate As Date
Dim EndDate As Date
Dim TempBusinessday As Date
Dim period1Begin As Date
Dim period1End As Date
Dim period2Begin As Date
Dim period2End As Date
Dim period3Begin As Date
Dim period3End As Date
Dim period4Begin As Date
Dim period4End As Date
Dim period5Begin As Date
Dim period5End As Date
Dim period6Begin As Date
Dim period6End As Date
Dim period7Begin As Date
Dim period7End As Date
Dim period8Begin As Date
Dim period8End As Date
Dim period9Begin As Date
Dim period9End As Date
Dim period10Begin As Date
Dim period10End As Date
Dim period11Begin As Date
Dim period11End As Date
Dim period12Begin As Date
Dim period12End As Date
Dim period13Begin As Date
Dim CheckDate As String
Dim TempStore As String
Dim YearNum As Integer
Dim MonthNum As Integer
Dim DayNum As Integer
Dim FileNum As String
Dim WrongBDate As Boolean

StartDate = #12/31/1998#
TempTestDate = Date
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If

   period1Begin = StartDate
   period2Begin = DateAdd("ww", 4, StartDate)
   period1End = DateAdd("d", -1, period2Begin)
   period3Begin = DateAdd("ww", 4, period2Begin)
   period2End = DateAdd("d", -1, period3Begin)
   period4Begin = DateAdd("ww", 5, period3Begin)
   period3End = DateAdd("d", -1, period4Begin)
   period5Begin = DateAdd("ww", 4, period4Begin)
   period4End = DateAdd("d", -1, period5Begin)
   period6Begin = DateAdd("ww", 4, period5Begin)
   period5End = DateAdd("d", -1, period6Begin)
   period7Begin = DateAdd("ww", 5, period6Begin)
   period6End = DateAdd("d", -1, period7Begin)
   period8Begin = DateAdd("ww", 4, period7Begin)
   period7End = DateAdd("d", -1, period8Begin)
   period9Begin = DateAdd("ww", 4, period8Begin)
   period8End = DateAdd("d", -1, period9Begin)
   period10Begin = DateAdd("ww", 5, period9Begin)
   period9End = DateAdd("d", -1, period10Begin)
   period11Begin = DateAdd("ww", 4, period10Begin)
   period10End = DateAdd("d", -1, period11Begin)
   period12Begin = DateAdd("ww", 4, period11Begin)
   period11End = DateAdd("d", -1, period12Begin)
   period13Begin = DateAdd("ww", 5, period12Begin)
   period12End = DateAdd("d", -1, period13Begin)

Select Case TempTestDate
       Case Is >= period12Begin
                 StartDate = period11Begin
                 EndDate = period11End
                 period = 12
       Case Is >= period11Begin
                 StartDate = period10Begin
                 EndDate = period10End
                 period = 11
       Case Is >= period10Begin
                 StartDate = period9Begin
                 EndDate = period9End
                 period = 10
       Case Is >= period9Begin
                 StartDate = period8Begin
                 EndDate = period8End
                 period = 9
       Case Is >= period8Begin
                 StartDate = period7Begin
                 EndDate = period7End
                 period = 8
       Case Is >= period7Begin
                 StartDate = period6Begin
                 EndDate = period6End
                 period = 7
       Case Is >= period6Begin
                 StartDate = period5Begin
                 EndDate = period5End
                 period = 6
       Case Is >= period5Begin
                 StartDate = period4Begin
                 EndDate = period4End
                 period = 5
       Case Is >= period4Begin
                 StartDate = period3Begin
                 EndDate = period3End
                 period = 4
       Case Is >= period3Begin
                 StartDate = period2Begin
                 EndDate = period2End
                 period = 3
       Case Is >= period2Begin
                 StartDate = period1Begin
                 EndDate = period1End
                 period = 2
       Case Is >= period1Begin
                 StartDate = DateAdd("WW", -52, period12Begin)
                 EndDate = DateAdd("WW", -52, period12End)
                 period = 1
End Select
If period = 1 Then
   CmdSpreadPrevious.Caption = "Period 12"
Else
   CmdSpreadPrevious.Caption = "Period " & period - 1
End If
    CmdSpreadCurrent.Caption = "Period " & period


'Jody's Code


Exit Sub
messagerror:
LstMessage.AddItem "Message system is not functioning correctly"
LstMessage.AddItem "Call Help Desk at (800) 773-8983 ext. 1437"
End Sub



Private Sub PrintO_Click()
Y = Shell("C:\windows\printo.exe", vbNormalFocus)
End
End Sub

