VERSION 5.00
Begin VB.Form FrmTimeSplash 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5640
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   8460
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   8460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   5595
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8145
      Begin VB.CommandButton Cmd2800 
         Caption         =   "2800 POS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   5040
         TabIndex        =   5
         Top             =   4680
         Width           =   2535
      End
      Begin VB.CommandButton Cmd3700 
         Caption         =   "3700 POS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1080
         TabIndex        =   4
         Top             =   4680
         Width           =   2655
      End
      Begin VB.Label Label4 
         Caption         =   "Calendar Day:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5160
         TabIndex        =   9
         Top             =   2640
         Width           =   1335
      End
      Begin VB.Label Label3 
         Caption         =   "Business Day:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2040
         TabIndex        =   8
         Top             =   2640
         Width           =   1335
      End
      Begin VB.Label Lbltest2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6600
         TabIndex        =   7
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Lbltest1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   6
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Your business date is incorrect. Please click on the button below that refers to your POS system to correct this problem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   840
         TabIndex        =   3
         Top             =   3600
         Width           =   7215
      End
      Begin VB.Label Label1 
         Caption         =   "Warning!"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   3600
         TabIndex        =   2
         Top             =   1920
         Width           =   2175
      End
      Begin VB.Image imgLogo 
         Height          =   2385
         Left            =   360
         Stretch         =   -1  'True
         Top             =   795
         Width           =   1815
      End
      Begin VB.Label lblCompanyProduct 
         AutoSize        =   -1  'True
         Caption         =   "Boddie - Noell Enterprises"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   2355
         TabIndex        =   1
         Top             =   705
         Width           =   4515
      End
   End
End
Attribute VB_Name = "frmTimeSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit



Private Sub Cmd2800_Click()
frmTimeSplash.Hide
Unload frmTimeSplash
Frm2800.Show
End Sub

Private Sub lblCompany_Click()

End Sub

Private Sub Cmd3700_Click()
frmTimeSplash.Hide
Unload frmTimeSplash
Frm3700.Show
End Sub

