VERSION 5.00
Begin VB.Form frmNoEmployee 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4245
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   7380
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   7380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   4050
      Left            =   150
      TabIndex        =   0
      Top             =   60
      Width           =   7080
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   2055
         Left            =   480
         ScaleHeight     =   2055
         ScaleWidth      =   2655
         TabIndex        =   5
         Top             =   840
         Width           =   2655
      End
      Begin VB.ListBox LstEmp 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Height          =   810
         Left            =   3600
         TabIndex        =   3
         Top             =   2760
         Width           =   3255
      End
      Begin VB.CommandButton CmdExit 
         Caption         =   "E&xit"
         Height          =   615
         Left            =   600
         TabIndex        =   2
         Top             =   3000
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "The Employee(s) Listed below has no Employee Status listed in the System Configurator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   3600
         TabIndex        =   4
         Top             =   1560
         Width           =   3255
      End
      Begin VB.Label lblCompanyProduct 
         AutoSize        =   -1  'True
         Caption         =   "Warning !!"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   2640
         TabIndex        =   1
         Top             =   360
         Width           =   1770
      End
   End
End
Attribute VB_Name = "frmNoEmployee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CmdExit_Click()
End
End Sub

