VERSION 5.00
Begin VB.Form frmRequest 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4920
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   7380
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmRequest.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4920
   ScaleWidth      =   7380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "&View"
      Height          =   495
      Left            =   2280
      TabIndex        =   1
      Top             =   3360
      Width           =   2775
   End
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   240
      Top             =   3360
   End
   Begin VB.Frame Frame1 
      Height          =   1755
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7065
      Begin VB.PictureBox Picture1 
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H8000000E&
         Height          =   1335
         Left            =   480
         Picture         =   "frmRequest.frx":000C
         ScaleHeight     =   1335
         ScaleWidth      =   1695
         TabIndex        =   3
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Mininum Wage Exception Report ($6.15)"
         BeginProperty Font 
            Name            =   "Copperplate Gothic Light"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   2280
         TabIndex        =   4
         Top             =   360
         Width           =   4695
      End
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "To View report, Press button below or report will automatically print"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   2
      Top             =   2280
      Width           =   7095
   End
End
Attribute VB_Name = "frmRequest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RestNum As String
Dim errfile As String
Public Errorcounter As Integer
Dim TimeCounter As Integer
Dim Employee_Seq As Integer
Dim Store_Id As Variant
Dim Tips_Mask_TTL As String 'VARIABLE THAT FORMATS THE TIP OUTPUT AS A PERCENT
Dim Tips_mask_DLY As String 'VARIABLE THAT FORMATS THE TIP OUTPUT AS A PERCENT
'SALES VARIABLES (TOTAL)
Dim NetSalesTTL As Currency
Dim ServiceChgTTL As Currency
Dim CreditTTL As Currency
Dim GrosRcptsTTL As Currency
Dim ChgdRcptsTTL As Currency
Dim ChgdTipsTTL As Currency
Dim TipsSvcTTL As Currency
Dim TipsPdTTL As Currency
Dim TipsDcldTTL As Currency
Dim TipPctTTL As Single
'SALES VARIABLES (DAILY)
Dim NetSalesDLY As Currency
Dim ServiceChgDLY As Currency
Dim CreditDLY As Currency
Dim GrosRcptDLY As Currency
Dim ChgdRcptsDLY As Currency
Dim ChgdTipsDLY As Currency
Dim TipsSvcDLY As Currency
Dim TipsPdDLY As Currency
Dim TipsDcldDLY As Currency
Dim TipPctDLY As Single
Dim TotalTipSales(100) As Currency
Dim TotalPay(100) As Currency
Dim TotalSales(100) As Currency
Dim TotalTips(100) As Currency
Dim TipPercent(100) As Currency
Dim TotalHours(100) As Currency
Dim TotalPayTips(100) As Currency
Dim AmountBelow(100) As Currency
Dim EmployeeNumber(100) As Currency
Dim EmployeeFirstName(100) As String
Dim EmployeeLastName(100) As String
Dim maxcounter As Integer
Dim counter As Integer
Dim I As Integer
Private Type Job_Code_8
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
    Job_6 As Variant
    Job_7 As Variant
    job_8 As Variant
End Type
Dim jobcode8 As Job_Code_8
Private Type Job_Code_7
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
    Job_6 As Variant
    Job_7 As Variant
End Type
Dim jobcode7 As Job_Code_7
Private Type Job_Code_6
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
    Job_6 As Variant
End Type
Dim jobcode6 As Job_Code_6
Private Type Job_Code_5
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
End Type
Dim jobcode5 As Job_Code_5
Private Type Job_Code_4
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
End Type
Dim jobcode4 As Job_Code_4
Private Type Job_Code_3
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
End Type
Dim jobcode3 As Job_Code_3
Private Type Job_Code_2
    Job_1 As Variant
    Job_2 As Variant
End Type
Dim jobcode2 As Job_Code_2
Private Type Job_Code_1
    Job_1 As Variant
End Type
Dim jobcode1 As Job_Code_1
'TEMP VARIABLES
Dim MinWage As Single
Dim RecordCnt As Long
Dim Last_Name As String * 16
Dim First_Name As String * 8
Dim Payroll_Id As String
Dim Emp_Num As String
Dim TmpJob As Integer
Dim TmpReg As Single
Dim TmpOvt As Single
Dim TmpRegRate As Currency
Dim TmpOvtRate As Currency
Dim TmpDefRegRate As Currency
Dim TmpDefOvtRate As Currency
'JOB CODE VARIABLES
Dim Job1 As Variant
Dim Job2 As Variant
Dim Job3 As Variant
Dim Job4 As Variant
Dim Job5 As Variant
Dim Job6 As Variant
Dim Job7 As Variant
Dim Job8 As Variant
'JOB REG HOURS VARIABLES
Dim Job1Reg As Single
Dim Job2Reg As Single
Dim Job3Reg As Single
Dim Job4Reg As Single
Dim Job5Reg As Single
Dim Job6Reg As Single
Dim Job7Reg As Single
Dim Job8Reg As Single
'JOB OVT HOURS VARIABLES
Dim Job1Ovt As Single
Dim Job2Ovt As Single
Dim Job3Ovt As Single
Dim Job4Ovt As Single
Dim Job5Ovt As Single
Dim Job6Ovt As Single
Dim Job7Ovt As Single
Dim Job8Ovt As Single
'JOB REG HOURS VARIABLES
Dim Job1Reg_DLY As Single
Dim Job2Reg_DLY As Single
Dim Job3Reg_DLY As Single
Dim Job4Reg_DLY As Single
Dim Job5Reg_DLY As Single
Dim Job6Reg_DLY As Single
Dim Job7Reg_DLY As Single
Dim Job8Reg_DLY As Single
'JOB OVT HOURS VARIABLES
Dim Job1Ovt_DLY As Single
Dim Job2Ovt_DLY As Single
Dim Job3Ovt_DLY As Single
Dim Job4Ovt_DLY As Single
Dim Job5Ovt_DLY As Single
Dim Job6Ovt_DLY As Single
Dim Job7Ovt_DLY As Single
Dim Job8Ovt_DLY As Single
'JOB REG RATE VARIABLES
Dim RegRate1 As Currency
Dim RegRate2 As Currency
Dim RegRate3 As Currency
Dim RegRate4 As Currency
Dim RegRate5 As Currency
Dim RegRate6 As Currency
Dim RegRate7 As Currency
Dim RegRate8 As Currency
'JOB OVT RATE VARIABLES
Dim OvtRate1 As Currency
Dim OvtRate2 As Currency
Dim OvtRate3 As Currency
Dim OvtRate4 As Currency
Dim OvtRate5 As Currency
Dim OvtRate6 As Currency
Dim OvtRate7 As Currency
Dim OvtRate8 As Currency
'FORMAT VARIABLES FOR THE OUTPUT FILE
Dim RegRate1_Mask As String * 8
Dim RegRate2_Mask As String * 8
Dim RegRate3_Mask As String * 8
Dim RegRate4_Mask As String * 8
Dim RegRate5_Mask As String * 8
Dim RegRate6_Mask As String * 8
Dim RegRate7_Mask As String * 8
Dim RegRate8_Mask As String * 8
Dim OvtRate1_Mask As String * 8
Dim OvtRate2_Mask As String * 8
Dim OvtRate3_Mask As String * 8
Dim OvtRate4_Mask As String * 8
Dim OvtRate5_Mask As String * 8
Dim OvtRate6_Mask As String * 8
Dim OvtRate7_Mask As String * 8
Dim OvtRate8_Mask As String * 8
Dim X As Integer
Dim DayVar As Integer
Dim TempDay As Date
Dim BlankEmp As Boolean


Private Sub Command1_Click()
frmRequest.Hide
Unload frmRequest
frmMain.Show
End Sub

Private Sub VScroll1_Change()
Lname.Caption = Left(Format(EmployeeLastName(VScroll1.Value), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value), "@@@"), 3) + Chr(13) + Chr(13) & _
                        Left(Format(EmployeeLastName(VScroll1.Value + 1), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value + 1), "@@@"), 3) + Chr(13) + Chr(13) & _
                        Left(Format(EmployeeLastName(VScroll1.Value + 2), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value + 2), "@@@"), 3) + Chr(13) + Chr(13) & _
                        Left(Format(EmployeeLastName(VScroll1.Value + 3), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value + 3), "@@@"), 3) + Chr(13) + Chr(13) & _
                        Left(Format(EmployeeLastName(VScroll1.Value + 4), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value + 4), "@@@"), 3) + Chr(13) + Chr(13) & _
                        Left(Format(EmployeeLastName(VScroll1.Value + 5), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value + 5), "@@@"), 3) + Chr(13) + Chr(13) & _
                        Left(Format(EmployeeLastName(VScroll1.Value + 6), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value + 6), "@@@"), 3) + Chr(13) + Chr(13) & _
                        Left(Format(EmployeeLastName(VScroll1.Value + 7), "@@@@@@@"), 7) & "  " & Left(Format(EmployeeFirstName(VScroll1.Value + 7), "@@@"), 3)
        LID.Caption = Format(EmployeeNumber(VScroll1.Value), "0000") + Chr(13) + Chr(13) & _
                      Format(EmployeeNumber(VScroll1.Value + 1), "0000") + Chr(13) + Chr(13) & _
                      Format(EmployeeNumber(VScroll1.Value + 2), "0000") + Chr(13) + Chr(13) & _
                      Format(EmployeeNumber(VScroll1.Value + 3), "0000") + Chr(13) + Chr(13) & _
                      Format(EmployeeNumber(VScroll1.Value + 4), "0000") + Chr(13) + Chr(13) & _
                      Format(EmployeeNumber(VScroll1.Value + 5), "0000") + Chr(13) + Chr(13) & _
                      Format(EmployeeNumber(VScroll1.Value + 6), "0000") + Chr(13) + Chr(13) & _
                      Format(EmployeeNumber(VScroll1.Value + 7), "0000")
        LTotalSales.Caption = Format(TotalSales(VScroll1.Value), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalSales(VScroll1.Value + 1), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalSales(VScroll1.Value + 2), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalSales(VScroll1.Value + 3), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalSales(VScroll1.Value + 4), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalSales(VScroll1.Value + 5), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalSales(VScroll1.Value + 6), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalSales(VScroll1.Value + 7), "0000.00")
        LTotalTips.Caption = Format(TotalTips(VScroll1.Value), "0000.00") + Chr(13) + Chr(13) & _
                             Format(TotalTips(VScroll1.Value + 1), "0000.00") + Chr(13) + Chr(13) & _
                             Format(TotalTips(VScroll1.Value + 2), "0000.00") + Chr(13) + Chr(13) & _
                             Format(TotalTips(VScroll1.Value + 3), "0000.00") + Chr(13) + Chr(13) & _
                             Format(TotalTips(VScroll1.Value + 4), "0000.00") + Chr(13) + Chr(13) & _
                             Format(TotalTips(VScroll1.Value + 5), "0000.00") + Chr(13) + Chr(13) & _
                             Format(TotalTips(VScroll1.Value + 6), "0000.00") + Chr(13) + Chr(13) & _
                             Format(TotalTips(VScroll1.Value + 7), "0000.00")
        LTipPercent.Caption = Format(TipPercent(VScroll1.Value), "00.00%") + Chr(13) + Chr(13) & _
                              Format(TipPercent(VScroll1.Value + 1), "00.00%") + Chr(13) + Chr(13) & _
                              Format(TipPercent(VScroll1.Value + 2), "00.00%") + Chr(13) + Chr(13) & _
                              Format(TipPercent(VScroll1.Value + 3), "00.00%") + Chr(13) + Chr(13) & _
                              Format(TipPercent(VScroll1.Value + 4), "00.00%") + Chr(13) + Chr(13) & _
                              Format(TipPercent(VScroll1.Value + 5), "00.00%") + Chr(13) + Chr(13) & _
                              Format(TipPercent(VScroll1.Value + 6), "00.00%") + Chr(13) + Chr(13) & _
                              Format(TipPercent(VScroll1.Value + 7), "00.00%")
        LTotalWages.Caption = Format(TotalPayTips(VScroll1.Value), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalPayTips(VScroll1.Value + 1), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalPayTips(VScroll1.Value + 2), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalPayTips(VScroll1.Value + 3), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalPayTips(VScroll1.Value + 4), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalPayTips(VScroll1.Value + 5), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalPayTips(VScroll1.Value + 6), "0000.00") + Chr(13) + Chr(13) & _
                              Format(TotalPayTips(VScroll1.Value + 7), "0000.00")
        LTotalHours.Caption = Format(TotalHours(VScroll1.Value), "#00.00") + Chr(13) + Chr(13) & _
                              Format(TotalHours(VScroll1.Value + 1), "#00.00") + Chr(13) + Chr(13) & _
                              Format(TotalHours(VScroll1.Value + 2), "#00.00") + Chr(13) + Chr(13) & _
                              Format(TotalHours(VScroll1.Value + 3), "#00.00") + Chr(13) + Chr(13) & _
                              Format(TotalHours(VScroll1.Value + 4), "#00.00") + Chr(13) + Chr(13) & _
                              Format(TotalHours(VScroll1.Value + 5), "#00.00") + Chr(13) + Chr(13) & _
                              Format(TotalHours(VScroll1.Value + 6), "#00.00") + Chr(13) + Chr(13) & _
                              Format(TotalHours(VScroll1.Value + 7), "#00.00")
        LBelow.Caption = Format(AmountBelow(VScroll1.Value), "0000.00") + Chr(13) + Chr(13) & _
                         Format(AmountBelow(VScroll1.Value + 1), "0000.00") + Chr(13) + Chr(13) & _
                         Format(AmountBelow(VScroll1.Value + 2), "0000.00") + Chr(13) + Chr(13) & _
                         Format(AmountBelow(VScroll1.Value + 3), "0000.00") + Chr(13) + Chr(13) & _
                         Format(AmountBelow(VScroll1.Value + 4), "0000.00") + Chr(13) + Chr(13) & _
                         Format(AmountBelow(VScroll1.Value + 5), "0000.00") + Chr(13) + Chr(13) & _
                         Format(AmountBelow(VScroll1.Value + 6), "0000.00") + Chr(13) + Chr(13) & _
                         Format(AmountBelow(VScroll1.Value + 7), "0000.00")
End Sub

Private Sub Picture2_Click()

End Sub

Private Sub Command2_Click()
End
End Sub

Private Sub Form_Load()
frmRequest.Width = 7400
frmRequest.Height = 5000
Dim I As Integer
Dim J As Integer
End Sub

Private Sub Timer1_Timer()
frmRequest.Hide
frmMain.Width = 10200
frmMain.Height = 7200
frmMain.MousePointer = 11
frmMain.Show
frmMain.Picture1.Visible = True
frmMain.LblDate.Visible = False
frmMain.VScroll1.Visible = False
frmMain.LblReportDate.Visible = False
frmMain.Label1.Visible = False
frmMain.LblDate2.Visible = False
frmMain.LblName.Visible = False
frmMain.LblID.Visible = False
frmMain.LblTotalSales.Visible = False
frmMain.LblTotalTips.Visible = False
frmMain.LblPercentTips.Visible = False
frmMain.LblTotalWages.Visible = False
frmMain.LblTotalHours.Visible = False
frmMain.LblBelowMin.Visible = False
frmMain.Lname.Visible = False
frmMain.LID.Visible = False
frmMain.LTotalSales.Visible = False
frmMain.LTotalTips.Visible = False
frmMain.LTipPercent.Visible = False
frmMain.LTotalWages.Visible = False
frmMain.LTotalHours.Visible = False
frmMain.LBelow.Visible = False
frmMain.CmdPrint.Visible = False
frmMain.CmdEnd.Visible = False
frmMain.LblProcessing.Visible = True
DoEvents
TempDay = Date
DayVar = Weekday(TempDay, vbThursday) - 1
BlankEmp = False
    Dim TempStore As String
    Dim Connection As ADODB.Connection
    Set Connection = New ADODB.Connection
    Dim Update As New ADODB.Command             'UPDATE CMD LINE
    
    Dim OpenStoreId As New ADODB.Command        'EXTRACT THE STORE ID CMD
    Dim StoreId As New ADODB.Recordset          'EXTRACT THE STORE ID RS
    
    Dim OpenViewTime As New ADODB.Command       'EXTRACT TIME RS CMD
    Dim ViewRsTime As New ADODB.Recordset       'TIME DATA RS
    
    Dim OpenViewSales As New ADODB.Command      'EXTRACT SALES RS CMD
    Dim ViewRsSales As New ADODB.Recordset      'SALES DATA RS
    
    Dim ViewJobCodeCount As New ADODB.Recordset 'THE JOBCODE RECORDSET
    Dim OpenJobCodeCount As New ADODB.Command   'THE JOBCODE COMMAND
    
    Dim ViewJobCodeArray As New ADODB.Recordset 'JOB CODE RECORDSET
    Dim OpenJobCodeArray As New ADODB.Command   'EXTRACT JOB CODES COMMAND
    
    Dim OpenDailySales As New ADODB.Command     'EXTRACT THE DAILY SALES COMMAND
    Dim DailySales As New ADODB.Recordset       'EXTRACT THE DAILY SALES RECORDSET
    
    Dim OpenDailyHours As New ADODB.Command     'EXTRACT THE DAILY HOURS
    Dim DailyHours As New ADODB.Recordset       'EXTRACT THE DAILY HOURS RECORDSET
    
    Dim OpenTimeCount As New ADODB.Command      'COUNT THE EMPLOYEES TIME RECORDS
    Dim TimeCount As New ADODB.Recordset
    
    'SET THE CONNECT STRING
    Connection.ConnectionString = ("DSN=micros;UID=support;PWD=support")
    'OPEN CONNECTION AS NEW ODBC CONNECTION
    Connection.Open
    'SET THE ACTIVE CONNECTION FOR UPDATE
    Update.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR TIME
    OpenViewTime.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR SALES
    OpenViewSales.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR JOBCODE
    OpenJobCodeCount.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR THE JOB CODE ARRAY
    OpenJobCodeArray.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR THE STORE ID
    OpenStoreId.ActiveConnection = Connection
    'SET THE ACTIVECONNECTION FOR THE DAILY SALES
    OpenDailySales.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR THE DAILY HOURS
    OpenDailySales.ActiveConnection = Connection
    'SET THE CONNECTION FOR THE DAILY SALES
    OpenDailyHours.ActiveConnection = Connection
    'SET THE CONNECTION FOR TIME RECORD COUNT
    OpenTimeCount.ActiveConnection = Connection
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS A STORED PROCEDURE. IT WILL EXECUTE THE UPDATE THE TTLS TABLES
    Update.CommandType = adCmdStoredProc
    Update.CommandText = "micros.sp_postall"
    Update.Execute
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE TIME VIEW FOR ONE WEEK OF DATA
    OpenViewTime.CommandText = "select employee_number, last_name, first_name, payroll_id , job_number, override_regular_rate, regular_hours, default_regular_rate, overtime_hours from micros.v_r_employee_time_card where labor_week = (select max(labor_week)from micros.time_card_dtl) order by employee_number"
    OpenViewTime.CommandType = adCmdText
    'CREATE THE RECORDSET FOR TIME DATA
    Set ViewRsTime = OpenViewTime.Execute
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS A STORED PROCEDURE. IT WILL GET THE STORE ID
    OpenStoreId.CommandText = "select location_name_1 from micros.rest_def"
    OpenStoreId.CommandType = adCmdText
    'CREATE THE RECORDSET FOR TIME DATA
    Set StoreId = OpenStoreId.Execute
    Store_Id = StoreId(0)
    TempStore = Right(Store_Id, 4)
    If Left(TempStore, 1) = "8" Then
       DayVar = Weekday(TempDay, vbThursday) - 1
    ElseIf Left(TempStore, 1) = "6" Then
       DayVar = Weekday(TempDay, vbTuesday) - 1
    End If

    'OPEN THE OUTPUT FILE
    'Replace output file "C:\time_rec.dat" with "C:\TexRock\Time_rec.dat"
'    Open "C:\TexRock\time_rec.dat" For Output As #1 Len = 100
    counter = 0

Do While Not ViewRsTime.EOF
    
        Job1Reg = 0
        Job2Reg = 0
        Job3Reg = 0
        Job4Reg = 0
        Job5Reg = 0
        Job6Reg = 0
        Job7Reg = 0
        Job8Reg = 0
        Job1Ovt = 0
        Job2Ovt = 0
        Job3Ovt = 0
        Job4Ovt = 0
        Job5Ovt = 0
        Job6Ovt = 0
        Job7Ovt = 0
        Job8Ovt = 0
        
        Job1Reg_DLY = 0
        Job2Reg_DLY = 0
        Job3Reg_DLY = 0
        Job4Reg_DLY = 0
        Job5Reg_DLY = 0
        Job6Reg_DLY = 0
        Job7Reg_DLY = 0
        Job8Reg_DLY = 0
        Job1Ovt_DLY = 0
        Job2Ovt_DLY = 0
        Job3Ovt_DLY = 0
        Job4Ovt_DLY = 0
        Job5Ovt_DLY = 0
        Job6Ovt_DLY = 0
        Job7Ovt_DLY = 0
        Job8Ovt_DLY = 0
        
        RSet RegRate1_Mask = "0.00"
        RSet RegRate2_Mask = "0.00"
        RSet RegRate3_Mask = "0.00"
        RSet RegRate4_Mask = "0.00"
        RSet RegRate5_Mask = "0.00"
        RSet RegRate6_Mask = "0.00"
        RSet RegRate7_Mask = "0.00"
        RSet RegRate8_Mask = "0.00"
        RSet OvtRate1_Mask = "0.00"
        RSet OvtRate2_Mask = "0.00"
        RSet OvtRate3_Mask = "0.00"
        RSet OvtRate4_Mask = "0.00"
        RSet OvtRate5_Mask = "0.00"
        RSet OvtRate6_Mask = "0.00"
        RSet OvtRate7_Mask = "0.00"
        RSet OvtRate8_Mask = "0.00"
    
    'INITIALIZE THE JOB CODES
    jobcode_1 = 0
    jobcode_2 = 0
    jobcode_3 = 0
    jobcode_4 = 0
    jobcode_5 = 0
    jobcode_6 = 0
    jobcode_7 = 0
    jobcode_8 = 0

    NetSalesTTL = 0
    ServiceChgTTL = 0
    CreditTTL = 0
    GrosRcptsTTL = 0
    ChgdRcptsTTL = 0
    ChgdTipsTTL = 0
    TipsSvcTTL = 0
    TipsPdTTL = 0
    TipsDcldTTL = 0
    TipPctTTL = 0
    Tips_Mask_TTL = "0.00%"
    
    Job1Reg_DLY = 0
    Job2Reg_DLY = 0
    Job3Reg_DLY = 0
    Job4Reg_DLY = 0
    Job5Reg_DLY = 0
    Job6Reg_DLY = 0
    Job7Reg_DLY = 0
    Job8Reg_DLY = 0
    Job1Ovt_DLY = 0
    Job2Ovt_DLY = 0
    Job3Ovt_DLY = 0
    Job4Ovt_DLY = 0
    Job5Ovt_DLY = 0
    Job6Ovt_DLY = 0
    Job7Ovt_DLY = 0
    Job8Ovt_DLY = 0
    
    NetSalesDLY = 0
    ServiceChgDLY = 0
    CreditDLY = 0
    GrosRcptsDLY = 0
    ChgdRcptsDLY = 0
    ChgdTipsDLY = 0
    TipsSvcDLY = 0
    TipsPdDLY = 0
    TipsDcldDLY = 0
    TipPctDLY = 0
    Tips_mask_DLY = "0.00%"
    
    
On Error GoTo error_handler
    'MOVE THE VIEW TO VARIABLES
    Emp_Num = ViewRsTime("employee_number")
    Last_Name = ViewRsTime("last_name")
    First_Name = ViewRsTime("first_name")
    If ViewRsTime("payroll_id") > 0 Then
         Payroll_Id = ViewRsTime("payroll_id")
    Else
         Payroll_Id = "000000000"
    End If
    TmpJob = ViewRsTime("job_number")
    TmpRegRate = ViewRsTime("override_regular_rate")
    TmpDefRegRate = ViewRsTime("default_regular_rate")
    TmpDefOvtRate = ViewRsTime("default_overtime_rate")
    
    'OPEN THE TIME COUNT RECORDSET
    OpenTimeCount.CommandText = "Select COUNT(DISTINCT time_card_seq) from micros.v_r_employee_time_card Where employee_number =" & Emp_Num & "and labor_week = (select max(labor_week)from micros.time_card_dtl)"
    OpenTimeCount.CommandType = adCmdText
    'CREATE THE RECORDSET FOR THE TIME RECORD COUNT
    Set TimeCount = OpenTimeCount.Execute
    'VARIABLE HOLDING THE NUMBER OF DISTIMCT TIME RECORDS FOR EACH EMPLOYEE
    TimeCounter = TimeCount(0)
              
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE RECORD COUNT VIEW
    OpenJobCodeCount.CommandText = "Select COUNT(DISTINCT job_number) from micros.v_R_employee_time_card Where v_r_employee_time_card.employee_number = " & Emp_Num & "and labor_week = (select max(labor_week)from micros.time_card_dtl)"
    OpenJobCodeCount.CommandType = adCmdText
    
    'CREATE THE RECORDSET FOR THE JOB CODE COUNT
    Set ViewJobCodeCount = OpenJobCodeCount.Execute
    RecordCnt = ViewJobCodeCount(0) 'NUMBER OF JOB CODES STORED HERE

    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE RECORD ARRAY VIEW
    OpenJobCodeArray.CommandText = "Select DISTINCT job_number from micros.v_R_employee_time_card where employee_number = " & Emp_Num & "and labor_week = (select max(labor_week)from micros.time_card_dtl)"
    OpenJobCodeArray.CommandType = adCmdText
    'CREATE THE RECORDSET FOR THE JOB CODE ARRAY
    'Set ViewJobCodeArray = OpenJobCodeArray.Execute
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE SALES VIEW FOR SEVEN DAYS
    If DayVar = 0 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -1 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    ElseIf DayVar = 1 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -2 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    ElseIf DayVar = 2 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -3 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    ElseIf DayVar = 3 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -4 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    ElseIf DayVar = 4 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -5 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    ElseIf DayVar = 5 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -6 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    ElseIf DayVar = 6 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -7 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    ElseIf DayVar = 7 Then
      OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) <= date(now(*)) and date(start_time) > date(now(*)) -8 order by emp_seq"
      OpenViewSales.CommandType = adCmdText
    End If
    
    'CREATE THE RECORDSET FOR SALES DATA
    Set ViewRsSales = OpenViewSales.Execute
    
    Employee_Seq = -1   'INITIALIZE THIS VARIABLE
    'GATHER THE TOTAL SALES
    If Not ViewRsSales.EOF Then
    
        NetSalesTTL = ViewRsSales(1)
        ServiceChgTTL = ViewRsSales(2)
        GrosRcptsTTL = ViewRsSales(3)
        ChgdRcptsTTL = ViewRsSales(4)
        ChgdTipsTTL = ViewRsSales(5)
        TipsSvcTTL = ViewRsSales(6)
        TipsPdTTL = ViewRsSales(7)
        TipsDcldTTL = ViewRsSales(8)
        
        ViewRsSales.MoveNext

        Do While Not ViewRsSales.EOF
        
            Employee_Seq = (0)
            NetSalesTTL = ViewRsSales(1) + NetSalesTTL
            ServiceChgTTL = ViewRsSales(2) + ServiceChgTTL
            GrosRcptsTTL = ViewRsSales(3) + GrosRcptsTTL
            ChgdRcptsTTL = ViewRsSales(4) + ChgdRcptsTTL
            ChgdTipsTTL = ViewRsSales(5) + ChgdTipsTTL
            TipsSvcTTL = ViewRsSales(6) + TipsSvcTTL
            TipsPdTTL = ViewRsSales(7) + TipsPdTTL
            TipsDcldTTL = ViewRsSales(8) + TipsDcldTTL
        
            ViewRsSales.MoveNext
        Loop
    End If
    If GrosRcptsTTL > 0 Then
        TipPctTTL = TipsDcldTTL / GrosRcptsTTL
        Tips_Mask_TTL = Format(TipPctTTL, "percent")    'STRING VARIABLE THAT STORES THE TIPS PERCENT (DAILY)
    End If


    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY SALES CALL
    If DayVar = 0 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-1"
      OpenDailySales.CommandType = adCmdText
    ElseIf DayVar = 1 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-2"
      OpenDailySales.CommandType = adCmdText
    ElseIf DayVar = 2 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-3"
      OpenDailySales.CommandType = adCmdText
    ElseIf DayVar = 3 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-4"
      OpenDailySales.CommandType = adCmdText
    ElseIf DayVar = 4 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-5"
      OpenDailySales.CommandType = adCmdText
    ElseIf DayVar = 5 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-5"
      OpenDailySales.CommandType = adCmdText
    ElseIf DayVar = 6 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-7"
      OpenDailySales.CommandType = adCmdText
    ElseIf DayVar = 7 Then
      OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) = date(now(*)) and date(end_time) > date(now(*))-8"
      OpenDailySales.CommandType = adCmdText
    End If
    'CREATE THE RECORDSET FOR THE DAILY SALES
    Set DailySales = OpenDailySales.Execute
       
    If Not DailySales.EOF Then
    
        NetSalesDLY = DailySales(1)
        ServiceChgDLY = DailySales(2)
        GrosRcptsDLY = DailySales(3)
        ChgdRcptsDLY = DailySales(4)
        ChgdTipsDLY = DailySales(5)
        TipsSvcDLY = DailySales(6)
        TipsPdDLY = DailySales(7)
        TipsDcldDLY = DailySales(8)
    
    End If

    If GrosRcptsDLY > 0 Then
        TipPctDLY = TipsDcldDLY / GrosRcptsDLY
        Tips_mask_DLY = Format(TipPctDLY, "percent")    'STRING VARIABLE THAT STORES THE TIPS PERCENT (DAILY)
    End If
    
    If RecordCnt > 0 Then   'THE EMPLOYEE IS VIABLE IN THE SYSTEM
        If RecordCnt = 8 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode8.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_5 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_6 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_7 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.job_8 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 7 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode7.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_5 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_6 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_7 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 6 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode6.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_5 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_6 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 5 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode5.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_5 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 4 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode4.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode4.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode4.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode4.Job_4 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 3 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode3.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode3.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode3.Job_3 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 2 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode2.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode2.Job_2 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 1 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode1.Job_1 = ViewJobCodeArray(0)
        End If
    End If
        
        If jobcode1.Job_1 > 0 Then
             jobcode_1 = jobcode1.Job_1
             jobcode1.Job_1 = 0
        Else: jobcode1.Job_1 = 0
        End If
        If jobcode2.Job_1 > 0 Then
             jobcode_1 = jobcode2.Job_1
             jobcode2.Job_1 = 0
        Else: jobcode2.Job_1 = 0
        End If
        If jobcode3.Job_1 > 0 Then
             jobcode_1 = jobcode3.Job_1
             jobcode3.Job_1 = 0
        Else: jobcode3.Job_1 = 0
        End If
        If jobcode4.Job_1 > 0 Then
             jobcode_1 = jobcode4.Job_1
             jobcode4.Job_1 = 0
        Else: jobcode4.Job_1 = 0
        End If
        If jobcode5.Job_1 > 0 Then
             jobcode_1 = jobcode5.Job_1
             jobcode5.Job_1 = 0
        Else: jobcode5.Job_1 = 0
        End If
        If jobcode6.Job_1 > 0 Then
             jobcode_1 = jobcode6.Job_1
             jobcode6.Job_1 = 0
        Else: jobcode6.Job_1 = 0
        End If
        If jobcode7.Job_1 > 0 Then
             jobcode_1 = jobcode7.Job_1
             jobcode7.Job_1 = 0
        Else: jobcode7.Job_1 = 0
        End If
        If jobcode8.Job_1 > 0 Then
             jobcode_1 = jobcode8.Job_1
             jobcode8.Job_1 = 0
        Else: jobcode8.Job_1 = 0
        End If
        
        If jobcode2.Job_2 > 0 Then
             jobcode_2 = jobcode2.Job_2
             jobcode2.Job_2 = 0
        Else: jobcode2.Job_2 = 0
        End If
        If jobcode3.Job_2 > 0 Then
             jobcode_2 = jobcode3.Job_2
             jobcode3.Job_2 = 0
        Else: jobcode3.Job_2 = 0
        End If
        If jobcode4.Job_2 > 0 Then
             jobcode_2 = jobcode4.Job_2
             jobcode4.Job_2 = 0
        Else: jobcode4.Job_2 = 0
        End If
        If jobcode5.Job_2 > 0 Then
             jobcode_2 = jobcode5.Job_2
             jobcode5.Job_2 = 0
        Else: jobcode5.Job_2 = 0
        End If
        If jobcode6.Job_2 > 0 Then
             jobcode_2 = jobcode6.Job_2
             jobcode6.Job_2 = 0
        Else: jobcode6.Job_2 = 0
        End If
        If jobcode7.Job_2 > 0 Then
             jobcode_2 = jobcode7.Job_2
             jobcode7.Job_2 = 0
        Else: jobcode7.Job_2 = 0
        End If
        If jobcode8.Job_2 > 0 Then
             jobcode_2 = jobcode8.Job_2
             jobcode8.Job_2 = 0
        Else: jobcode8.Job_2 = 0
        End If
        
        If jobcode3.Job_3 > 0 Then
             jobcode_3 = jobcode3.Job_3
             jobcode3.Job_3 = 0
        Else: jobcode3.Job_3 = 0
        End If
        If jobcode4.Job_3 > 0 Then
             jobcode_3 = jobcode4.Job_3
             jobcode4.Job_3 = 0
        Else: jobcode4.Job_3 = 0
        End If
        If jobcode5.Job_3 > 0 Then
             jobcode_3 = jobcode5.Job_3
             jobcode5.Job_3 = 0
        Else: jobcode5.Job_3 = 0
        End If
        If jobcode6.Job_3 > 0 Then
             jobcode_3 = jobcode6.Job_3
             jobcode6.Job_3 = 0
        Else: jobcode6.Job_3 = 0
        End If
        If jobcode7.Job_3 > 0 Then
             jobcode_3 = jobcode7.Job_3
             jobcode7.Job_3 = 0
        Else: jobcode7.Job_3 = 0
        End If
        If jobcode8.Job_3 > 0 Then
             jobcode_3 = jobcode8.Job_3
             jobcode8.Job_3 = 0
        Else: jobcode8.Job_3 = 0
        End If
        
        If jobcode4.Job_4 > 0 Then
             jobcode_4 = jobcode4.Job_4
             jobcode4.Job_4 = 0
        Else: jobcode4.Job_4 = 0
        End If
        If jobcode5.Job_4 > 0 Then
             jobcode_4 = jobcode5.Job_4
             jobcode5.Job_4 = 0
        Else: jobcode5.Job_4 = 0
        End If
        If jobcode6.Job_4 > 0 Then
             jobcode_4 = jobcode6.Job_4
             jobcode6.Job_4 = 0
        Else: jobcode6.Job_4 = 0
        End If
        If jobcode7.Job_4 > 0 Then
             jobcode_4 = jobcode7.Job_4
             jobcode7.Job_4 = 0
        Else: jobcode7.Job_4 = 0
        End If
        If jobcode8.Job_4 > 0 Then
             jobcode_4 = jobcode8.Job_4
             jobcode8.Job_4 = 0
        Else: jobcode8.Job_4 = 0
        End If
    
        If jobcode5.Job_5 > 0 Then
             jobcode_5 = jobcode5.Job_5
             jobcode5.Job_5 = 0
        Else: jobcode5.Job_5 = 0
        End If
        If jobcode6.Job_5 > 0 Then
             jobcode_5 = jobcode6.Job_5
             jobcode6.Job_5 = 0
        Else: jobcode6.Job_5 = 0
        End If
        If jobcode7.Job_5 > 0 Then
             jobcode_5 = jobcode7.Job_5
             jobcode7.Job_5 = 0
        Else: jobcode7.Job_5 = 0
        End If
        If jobcode8.Job_5 > 0 Then
             jobcode_5 = jobcode8.Job_5
             jobcode8.Job_5 = 0
        Else: jobcode8.Job_5 = 0
        End If
        
        If jobcode6.Job_6 > 0 Then
             jobcode_6 = jobcode6.Job_6
             jobcode6.Job_6 = 0
        Else: jobcode6.Job_6 = 0
        End If
        If jobcode7.Job_6 > 0 Then
             jobcode_6 = jobcode7.Job_6
             jobcode7.Job_6 = 0
        Else: jobcode7.Job_6 = 0
        End If
        If jobcode8.Job_6 > 0 Then
             jobcode_6 = jobcode8.Job_6
             jobcode8.Job_6 = 0
        Else: jobcode8.Job_6 = 0
        End If
        
        If jobcode7.Job_7 > 0 Then
             jobcode_7 = jobcode7.Job_7
             jobcode7.Job_7 = 0
        Else: jobcode7.Job_7 = 0
        End If
        If jobcode8.Job_7 > 0 Then
             jobcode_7 = jobcode8.Job_7
             jobcode8.Job_7 = 0
        Else: jobcode8.Job_7 = 0
        End If
        
        If jobcode8.job_8 > 0 Then
             jobcode_8 = jobcode8.job_8
             jobcode8.job_8 = 0
        Else: jobcode8.job_8 = 0
        End If
        
        
     'End If
            
        Do While TimeCounter <> 0
            
          TmpReg = ViewRsTime("regular_hours")
          TmpOvt = ViewRsTime("Overtime_hours")
            
        'PLACES THE NUMBER OF HOURS IN THE PROPER TIME BUCKET
        If ViewRsTime("job_number") = jobcode_1 Then
                                            RegRate1 = 0
                                            OvtRate1 = 0
                                            Job1Reg = Job1Reg + TmpReg
                                            Job1Ovt = Job1Ovt + TmpOvt
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate1 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate1 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate1 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate1 = ViewRsTime("default_overtime_rate")
                                            End If
                                            'RegRate1 = ViewRsTime("override_regular_rate")
                                            'OvtRate1 = ViewRsTime("override_overtime_rate")
                                            TmpReg = 0
                                            RSet RegRate1_Mask = CStr(Format(RegRate1, "#.00"))
                                            RSet OvtRate1_Mask = CStr(Format(OvtRate1, "#.00"))
                                            Job1Reg_DLY = 0
                                            Job1Ovt_DLY = 0
                                            
                                            'If Job1Ovt > 0 Then
                                                'Job1Reg = 40
                                            'End If
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY REGULAR HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_1
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job1Reg_DLY = DailyHours(0)
                                            End If
                        
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_1
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job1Ovt_DLY = DailyHours(0)
                                            End If
            
        ElseIf ViewRsTime("job_number") = jobcode_2 Then
                                                RegRate2 = 0
                                                OvtRate2 = 0
                                                Job2Reg = Job2Reg + TmpReg
                                                Job2Ovt = Job2Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate2 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate2 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate2 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate2 = ViewRsTime("default_overtime_rate")
                                            End If
                                                'RegRate2 = ViewRsTime("override_regular_rate")
                                                'OvtRate2 = ViewRsTime("override_overtime_rate")
                                                TmpReg = 0
                                                RSet RegRate2_Mask = CStr(Format(RegRate2, "#.00"))
                                                RSet OvtRate2_Mask = CStr(Format(OvtRate2, "#.00"))
                                                Job2Reg_DLY = 0
                                                Job2Ovt_DLY = 0
        
                                            'If Job2Ovt > 0 Then
                                                'Job2Reg = 40
                                            'End If
        
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_2
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job2Reg_DLY = DailyHours(0)
                                            End If
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_2
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job2Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_3 Then
                                                RegRate3 = 0
                                                OvtRate3 = 0
                                                Job3Reg = Job3Reg + TmpReg
                                                Job3Ovt = Job3Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate3 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate3 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate3 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate3 = ViewRsTime("default_overtime_rate")
                                            End If
                                                'RegRate3 = ViewRsTime("override_regular_rate")
                                                'OvtRate3 = ViewRsTime("override_overtime_rate")
                                                TmpReg = 0
                                                RSet RegRate3_Mask = CStr(Format(RegRate3, "#.00"))
                                                RSet OvtRate3_Mask = CStr(Format(OvtRate3, "#.00"))
                                                Job3Reg_DLY = 0
                                                Job3Ovt_DLY = 0
                                            
                                             'If Job3Ovt > 0 Then
                                                'Job3Reg = 40
                                             'End If
                                           
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_3
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job3Reg_DLY = DailyHours(0)
                                            End If
        
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_3
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job3Ovt_DLY = DailyHours(0)
                                            End If
       
        ElseIf ViewRsTime("job_number") = jobcode_4 Then
                                                RegRate4 = 0
                                                OvtRate4 = 0
                                                Job4Reg = Job4Reg + TmpReg
                                                Job4Ovt = Job4Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate4 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate4 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate4 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate4 = ViewRsTime("default_overtime_rate")
                                            End If
                                                'RegRate4 = ViewRsTime("override_regular_rate")
                                                'OvtRate4 = ViewRsTime("override_overtime_rate")
                                                TmpReg = 0
                                                RSet RegRate4_Mask = CStr(Format(RegRate4, "#.00"))
                                                RSet OvtRate4_Mask = CStr(Format(OvtRate4, "#.00"))
                                                Job4Reg_DLY = 0
                                                Job4Ovt_DLY = 0

                                             'If Job4Ovt > 0 Then
                                                'Job4Reg = 40
                                             'End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_4
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job4Ovt_DLY = DailyHours(0)
                                            End If
       
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_4
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job4Reg_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_5 Then
                                                RegRate5 = 0
                                                OvtRate5 = 0
                                                Job5Reg = Job5Reg + TmpReg
                                                Job5Ovt = Job5Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate5 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate5 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate5 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate5 = ViewRsTime("default_overtime_rate")
                                            End If
                                                'RegRate5 = ViewRsTime("override_regular_rate")
                                                'OvtRate5 = ViewRsTime("override_overtime_rate")
                                                TmpReg = 0
                                                RSet RegRate5_Mask = CStr(Format(RegRate5, "#.00"))
                                                RSet OvtRate5_Mask = CStr(Format(OvtRate5, "#.00"))
                                                Job5Reg_DLY = 0
                                                Job5Ovt_DLY = 0
        
                                             'If Job5Ovt > 0 Then
                                                'Job5Reg = 40
                                             'End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_5
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job5Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_5
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job5Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_6 Then
                                                RegRate6 = 0
                                                OvtRate6 = 0
                                                Job6Reg = Job6Reg + TmpReg
                                                Job6Ovt = Job6Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate6 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate6 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate6 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate6 = ViewRsTime("default_overtime_rate")
                                            End If
                                                'RegRate6 = ViewRsTime("override_regular_rate")
                                                'OvtRate6 = ViewRsTime("override_overtime_rate")
                                                TmpReg = 0
                                                RSet RegRate6_Mask = CStr(Format(RegRate6, "#.00"))
                                                RSet OvtRate6_Mask = CStr(Format(OvtRate6, "#.00"))
                                                Job6Reg_DLY = 0
                                                Job6Ovt_DLY = 0
        
                                             'If Job6Ovt > 0 Then
                                                'Job6Reg = 40
                                            ' End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_6
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job6Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_6
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job6Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_7 Then
                                                RegRate7 = 0
                                                OvtRate7 = 0
                                                Job7Reg = Job7Reg + TmpReg
                                                Job7Ovt = Job7Ovt + TmpOvt
                                               
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate7 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate7 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate7 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate7 = ViewRsTime("default_overtime_rate")
                                            End If
                                                'RegRate7 = ViewRsTime("override_regular_rate")
                                                'OvtRate7 = ViewRsTime("override_overtime_rate")
                                                TmpReg = 0
                                                RSet RegRate7_Mask = CStr(Format(RegRate7, "#.00"))
                                                RSet OvtRate7_Mask = CStr(Format(OvtRate7, "#.00"))
                                                Job7Reg_DLY = 0
                                                Job7Ovt_DLY = 0
        
                                             'If Job7Ovt > 0 Then
                                                'Job7Reg = 40
                                             'End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_7
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job7Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_7
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job7Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_8 Then
                                                RegRate8 = 0
                                                OvtRate8 = 0
                                                Job8Reg = Job8Reg + TmpReg
                                                Job8Ovt = Job8Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate8 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate8 = ViewRsTime("default_regular_rate")
                                            End If
                                            If ViewRsTime("override_overtime_rate") > 0 Then
                                               OvtRate8 = ViewRsTime("override_overtime_rate")
                                            Else
                                               OvtRate = ViewRsTime("default_overtime_rate")
                                            End If
                                                'RegRate8 = ViewRsTime("override_regular_rate")
                                                'OvtRate8 = ViewRsTime("override_overtime_rate")
                                                TmpReg = 0
                                                RSet RegRate8_Mask = CStr(Format(RegRate8, "#.00"))
                                                RSet OvtRate8_Mask = CStr(Format(OvtRate8, "#.00"))
                                                Job8Reg_DLY = 0
                                                Job8Ovt_DLY = 0
                                        
                                             'If Job8Ovt > 0 Then
                                                'Job8Reg = 40
                                            ' End If
                                           
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_8
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job8Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_8
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job8Ovt_DLY = DailyHours(0)
                                            End If

        End If

    
       ViewRsTime.MoveNext 'MOVE TO THE NEXT RECORD
   
     TimeCounter = TimeCounter - 1
    Loop    'INNER LOOP
        'Replaced Print code with shorter version
        'Print #1, Store_Id, Payroll_Id, Emp_Num, Last_Name, First_Name, jobcode_1, RegRate1_Mask, Job1Reg, Job1Reg_DLY, OvtRate1_Mask, Job1Ovt, Job1Ovt_DLY, jobcode_2, RegRate2_Mask, Job2Reg, Job2Reg_DLY, OvtRate2_Mask, Job2Ovt, Job2Ovt_DLY, jobcode_3, RegRate3_Mask, Job3Reg, Job3Reg_DLY, OvtRate3_Mask, Job3Ovt, Job3Ovt_DLY, _
        'jobcode_4, RegRate4_Mask, Job4Reg, Job4Reg_DLY, OvtRate4_Mask, Job4Ovt, Job4Ovt_DLY, jobcode_5, RegRate5_Mask, Job5Reg, Job5Reg_DLY, OvtRate5_Mask, Job5Ovt, job5_ovt_dly, jobcode_6, RegRate6_Mask, Job6Reg, Job6Reg_DLY, OvtRate6_Mask, Job6Ovt, Job6Ovt_DLY, jobcode_7, RegRate7_Mask, Job7Reg, Job7Reg_DLY, OvtRate7_Mask, Job7Ovt, _
        'Job7Ovt_DLY, jobcode_8, RegRate8_Mask, Job8Reg, Job8Reg_DLY, OvtRate8_Mask, Job8Ovt, Job8Ovt_DLY, NetSalesDLY, ServiceChgDLY, GrosRcptsDLY, ChgdRcptsDLY, ChgdTipsDLY, TipsSvcDLY, TipsPdDLY, TipsDcldDLY, Tips_mask_DLY, NetSalesTTL, ServiceChgTTL, GrosRcptsTTL, _
        'ChgdRcptsTTL, ChgdTipsTTL, TipsSvcTTL, TipsPdTTL, TipsDcldTTL, Tips_Mask_TTL
       
    '   Print #1,
   TotalTipSales(totalcounter) = Format(GrosRcptsTTL, "#0000.0000")
   TotalPay(totalcounter) = Format((RegRate1_Mask * Job1Reg) + (OvtRate1_Mask * Job1Ovt) + (RegRate2_Mask * Job2Reg) + (OvtRate2_Mask * Job2Ovt) + (RegRate3_Mask * Job3Reg) + (OvtRate3_Mask * Job3Ovt) + (RegRate4_Mask * Job4Reg) + (OvtRate4_Mask * Job4Ovt) + (RegRate5_Mask * Job5Reg) + (OvtRate5_Mask * Job5Ovt) + (RegRate6_Mask * Job6Reg) + (OvtRate6_Mask * Job6Ovt), "0000.00")
   TotalSales(totalcounter) = Format(NetSalesTTL, "##00.00")
   TotalTips(totalcounter) = Format(TipsSvcTTL + TipsDcldTTL + ChgdTipsTTL, "##00.0000")
   TipPercent(totalcounter) = Format((TotalTips(totalcounter) / TotalTipSales(totalcounter)), "#00.00")
   TotalHours(totalcounter) = Format(Job1Reg + Job1Ovt + Job2Reg + Job2Ovt + Job3Reg + Job3Ovt + Job4Reg + Job4Ovt + Job5Reg + Job5Ovt + Job6Reg + Job6Ovt, "#00.00")
   TotalPayTips(totalcounter) = Format(TotalPay(totalcounter) + TotalTips(totalcounter), "##00.00")
   AmountBelow(totalcounter) = Format((TotalPayTips(totalcounter) - ((Job1Reg + Job2Reg + Job3Reg + Job4Reg + Job5Reg + Job6Reg) * 6.15) + ((Job1Ovt + Job2Ovt + Job3Ovt + Job4Ovt + Job5Ovt + Job6Ovt) * 9.225)), "#00.00")
   EmployeeNumber(totalcounter) = Emp_Num
   EmployeeFirstName(totalcounter) = Format(First_Name, ">")
   EmployeeLastName(totalcounter) = Format(Last_Name, ">")
If AmountBelow(totalcounter) < 0 Then
'   If TotalSales(totalcounter) > 1 Then
'    If TotalTipSales(totalcounter) > 1 Then
      totalcounter = totalcounter + 1
'    End If
'   End If
End If
If TmpReg > 0 Then
   If Emp_Num > 0 Then
      BlankEmp = BlankEmp
   Else
      frmNoEmployee.LstEmp.AddItem First_Name & "  " & Last_Name & "  " & TmpReg & "  " & Emp_Num
      BlankEmp = True
   End If
End If
Loop    'OUTTER LOOP
If BlankEmp = True Then
   frmMain.Hide
   frmNoEmployee.Show
   Exit Sub
End If
maxcounter = totalcounter - 1
error_handler:
Err.Clear
Resume Next
frmMain.Hide
Printer.Orientation = 2
Dim PrintPosition As Single
PrintPosition = 0
totalcounter = 1
Printer.FontName = "COURIER NEW"
Printer.FontBold = False
Printer.FontSize = 10
Printer.CurrentX = 400
Printer.CurrentY = 300
Printer.Print "Report Date Range  " & Format((Now) - DayVar, "short date") & " to " & Format((Now), "short date")
Printer.CurrentX = 11900
Printer.CurrentY = 300
Printer.Print "Date of Report  " & Format((Now), "short date")
If frmMain.ListSSN.ListCount > 0 Then
   Printer.CurrentX = 700
   Printer.CurrentY = 800
   Printer.Print "The following personnel have been entered into the MICROS incorrectly.  Please open Manager"
   Printer.CurrentX = 700
   Printer.CurrentY = 1100
   Printer.Print "Procedures and click on the Employee tab.  Click on Assignments and locate each employee"
   Printer.CurrentX = 700
   Printer.CurrentY = 1400
   Printer.Print "listed below to correct the invalid Social Security Number.  For further information refer"
   Printer.CurrentX = 700
   Printer.CurrentY = 1700
   Printer.Print " to your Payroll Section of the Information Systems Training Manual."
   Printer.CurrentX = 700
   Printer.CurrentY = 2000
   Printer.Print "REMINDER - Payroll ID is the Social Security Number without spaces or dashes"
   Printer.FontBold = True
   Printer.CurrentX = 600
   Printer.CurrentY = 2400
   Printer.Print "Employees with errors in Social Security Number"
   PrintPosition = PrintPosition + 2300
   Printer.FontBold = False
   For I = 0 To frmMain.ListSSN.ListCount - 1
      frmMain.ListSSN.ListIndex = I
      Printer.CurrentX = 700
      Printer.CurrentY = PrintPosition
      Printer.Print frmMain.ListSSN.Text
      PrintPosition = PrintPosition + 400
         If I > 5 Then
            Exit For
         End If
    Next
PrintPosition = 4700
Else
   PrintPosition = 1000
End If
Printer.FontSize = 12
Printer.FontBold = True
Printer.CurrentX = 700
Printer.CurrentY = PrintPosition
Printer.Print "Employee Name"
Printer.CurrentX = 3200
Printer.CurrentY = PrintPosition
Printer.Print "ID Number"
Printer.CurrentX = 4900
Printer.CurrentY = PrintPosition
Printer.Print "Total Sales"
Printer.CurrentX = 6700
Printer.CurrentY = PrintPosition
Printer.Print "Total Tips"
Printer.CurrentX = 8600
Printer.CurrentY = PrintPosition
Printer.Print "Tip %"
Printer.CurrentX = 10000
Printer.CurrentY = PrintPosition
Printer.Print "Total Wages"
Printer.CurrentX = 11900
Printer.CurrentY = PrintPosition
Printer.Print "Total Hrs"
Printer.CurrentX = 13600
Printer.CurrentY = PrintPosition
Printer.Print "Below Min"
Printer.FontBold = False
PrintPosition = PrintPosition + 400
   If maxcounter < 17 Then
      For totalcounter = 1 To maxcounter
         Printer.CurrentX = 400
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeLastName(totalcounter), "@@@@@@@"), 9)
         Printer.CurrentX = 1800
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeFirstName(totalcounter), "@@@"), 9)
         Printer.CurrentX = 3600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(EmployeeNumber(totalcounter), "0000")
         Printer.CurrentX = 5200
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalSales(totalcounter), "currency")
         Printer.CurrentX = 6900
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalTips(totalcounter), "currency")
         Printer.CurrentX = 8600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TipPercent(totalcounter), "percent")
         Printer.CurrentX = 10300
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalPayTips(totalcounter), "currency")
         Printer.CurrentX = 12000
         Printer.CurrentY = PrintPosition
         Printer.Print TotalHours(totalcounter)
         Printer.CurrentX = 13700
         Printer.CurrentY = PrintPosition
         Printer.Print Format(AmountBelow(totalcounter), "currency")
         PrintPosition = PrintPosition + 400
      Next
Printer.EndDoc
  ElseIf maxcounter > 16 And maxcounter < 41 Then
     If frmMain.ListSSN.ListCount > 0 Then
        PrintPosition = 5000
     Else
        PrintPosition = 1400
     End If
     For totalcounter = 1 To 16
         Printer.CurrentX = 400
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeLastName(totalcounter), "@@@@@@@"), 9)
         Printer.CurrentX = 1800
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeFirstName(totalcounter), "@@@"), 9)
         Printer.CurrentX = 3600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(EmployeeNumber(totalcounter), "0000")
         Printer.CurrentX = 5200
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalSales(totalcounter), "currency")
         Printer.CurrentX = 6900
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalTips(totalcounter), "currency")
         Printer.CurrentX = 8600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TipPercent(totalcounter), "percent")
         Printer.CurrentX = 10300
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalPayTips(totalcounter), "currency")
         Printer.CurrentX = 12000
         Printer.CurrentY = PrintPosition
         Printer.Print TotalHours(totalcounter)
         Printer.CurrentX = 13700
         Printer.CurrentY = PrintPosition
         Printer.Print Format(AmountBelow(totalcounter), "currency")
         PrintPosition = PrintPosition + 400
      Next
         Printer.EndDoc
         Printer.Orientation = 2
         PrintPosition = 1500
         totalcounter = 1
         Printer.FontName = "COURIER NEW"
         Printer.FontBold = False
         Printer.FontSize = 10
         Printer.CurrentX = 400
         Printer.CurrentY = 300
         Printer.Print "Report Date Range  " & Format((Now) - DayVar, "short date") & " to " & Format((Now), "short date")
         Printer.CurrentX = 11900
         Printer.CurrentY = 300
         Printer.Print "Date of Report  " & Format((Now), "short date")
         Printer.FontSize = 12
         Printer.FontBold = True
         Printer.CurrentX = 700
         Printer.CurrentY = 1000
         Printer.Print "Employee Name"
         Printer.CurrentX = 3200
         Printer.CurrentY = 1000
         Printer.Print "ID Number"
         Printer.CurrentX = 4900
         Printer.CurrentY = 1000
         Printer.Print "Total Sales"
         Printer.CurrentX = 6700
         Printer.CurrentY = 1000
         Printer.Print "Total Tips"
         Printer.CurrentX = 8600
         Printer.CurrentY = 1000
         Printer.Print "Tip %"
         Printer.CurrentX = 10000
         Printer.CurrentY = 1000
         Printer.Print "Total Wages"
         Printer.CurrentX = 11900
         Printer.CurrentY = 1000
         Printer.Print "Total Hrs"
         Printer.CurrentX = 13600
         Printer.CurrentY = 1000
         Printer.Print "Below Min"
         Printer.FontBold = False
   For totalcounter = 17 To maxcounter
         Printer.CurrentX = 400
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeLastName(totalcounter), "@@@@@@@"), 9)
         Printer.CurrentX = 1800
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeFirstName(totalcounter), "@@@"), 9)
         Printer.CurrentX = 3600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(EmployeeNumber(totalcounter), "0000")
         Printer.CurrentX = 5200
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalSales(totalcounter), "currency")
         Printer.CurrentX = 6900
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalTips(totalcounter), "currency")
         Printer.CurrentX = 8600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TipPercent(totalcounter), "percent")
         Printer.CurrentX = 10300
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalPayTips(totalcounter), "currency")
         Printer.CurrentX = 12000
         Printer.CurrentY = PrintPosition
         Printer.Print TotalHours(totalcounter)
         Printer.CurrentX = 13700
         Printer.CurrentY = PrintPosition
         Printer.Print Format(AmountBelow(totalcounter), "currency")
         PrintPosition = PrintPosition + 400
      Next
         Printer.EndDoc
  Else
         If frmMain.ListSSN.ListCount > 0 Then
            PrintPosition = 5000
         Else
            PrintPosition = 1400
         End If
         For totalcounter = 1 To 16
         Printer.CurrentX = 400
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeLastName(totalcounter), "@@@@@@@"), 9)
         Printer.CurrentX = 1800
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeFirstName(totalcounter), "@@@"), 9)
         Printer.CurrentX = 3600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(EmployeeNumber(totalcounter), "0000")
         Printer.CurrentX = 5200
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalSales(totalcounter), "currency")
         Printer.CurrentX = 6900
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalTips(totalcounter), "currency")
         Printer.CurrentX = 8600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TipPercent(totalcounter), "percent")
         Printer.CurrentX = 10300
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalPayTips(totalcounter), "currency")
         Printer.CurrentX = 12000
         Printer.CurrentY = PrintPosition
         Printer.Print TotalHours(totalcounter)
         Printer.CurrentX = 13700
         Printer.CurrentY = PrintPosition
         Printer.Print Format(AmountBelow(totalcounter), "currency")
         PrintPosition = PrintPosition + 400
      Next
         Printer.EndDoc
         Printer.Orientation = 2
         PrintPosition = 1500
         totalcounter = 1
         Printer.FontName = "COURIER NEW"
         Printer.FontBold = False
         Printer.FontSize = 10
         Printer.CurrentX = 400
         Printer.CurrentY = 300
         Printer.Print "Report Date Range  " & Format((Now) - DayVar, "short date") & " to " & Format((Now), "short date")
         Printer.CurrentX = 11900
         Printer.CurrentY = 300
         Printer.Print "Date of Report  " & Format((Now), "short date")
         Printer.FontSize = 12
         Printer.FontBold = True
         Printer.CurrentX = 700
         Printer.CurrentY = 1000
         Printer.Print "Employee Name"
         Printer.CurrentX = 3200
         Printer.CurrentY = 1000
         Printer.Print "ID Number"
         Printer.CurrentX = 4900
         Printer.CurrentY = 1000
         Printer.Print "Total Sales"
         Printer.CurrentX = 6700
         Printer.CurrentY = 1000
         Printer.Print "Total Tips"
         Printer.CurrentX = 8600
         Printer.CurrentY = 1000
         Printer.Print "Tip %"
         Printer.CurrentX = 10000
         Printer.CurrentY = 1000
         Printer.Print "Total Wages"
         Printer.CurrentX = 11900
         Printer.CurrentY = 1000
         Printer.Print "Total Hrs"
         Printer.CurrentX = 13600
         Printer.CurrentY = 1000
         Printer.Print "Below Min"
         Printer.FontBold = False
   For totalcounter = 17 To 40
         Printer.CurrentX = 400
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeLastName(totalcounter), "@@@@@@@"), 9)
         Printer.CurrentX = 1800
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeFirstName(totalcounter), "@@@"), 9)
         Printer.CurrentX = 3600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(EmployeeNumber(totalcounter), "0000")
         Printer.CurrentX = 5200
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalSales(totalcounter), "currency")
         Printer.CurrentX = 6900
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalTips(totalcounter), "currency")
         Printer.CurrentX = 8600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TipPercent(totalcounter), "percent")
         Printer.CurrentX = 10300
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalPayTips(totalcounter), "currency")
         Printer.CurrentX = 12000
         Printer.CurrentY = PrintPosition
         Printer.Print TotalHours(totalcounter)
         Printer.CurrentX = 13700
         Printer.CurrentY = PrintPosition
         Printer.Print Format(AmountBelow(totalcounter), "currency")
         PrintPosition = PrintPosition + 400
      Next
         Printer.EndDoc
         Printer.Orientation = 2
         PrintPosition = 1500
         totalcounter = 1
         Printer.FontName = "COURIER NEW"
         Printer.FontBold = False
         Printer.FontSize = 10
         Printer.CurrentX = 400
         Printer.CurrentY = 300
         Printer.Print "Report Date Range  " & Format((Now) - DayVar, "short date") & " to " & Format((Now), "short date")
         Printer.CurrentX = 11900
         Printer.CurrentY = 300
         Printer.Print "Date of Report  " & Format((Now), "short date")
         Printer.FontSize = 12
         Printer.FontBold = True
         Printer.CurrentX = 700
         Printer.CurrentY = 1000
         Printer.Print "Employee Name"
         Printer.CurrentX = 3200
         Printer.CurrentY = 1000
         Printer.Print "ID Number"
         Printer.CurrentX = 4900
         Printer.CurrentY = 1000
         Printer.Print "Total Sales"
         Printer.CurrentX = 6700
         Printer.CurrentY = 1000
         Printer.Print "Total Tips"
         Printer.CurrentX = 8600
         Printer.CurrentY = 1000
         Printer.Print "Tip %"
         Printer.CurrentX = 10000
         Printer.CurrentY = 1000
         Printer.Print "Total Wages"
         Printer.CurrentX = 11900
         Printer.CurrentY = 1000
         Printer.Print "Total Hrs"
         Printer.CurrentX = 13600
         Printer.CurrentY = 1000
         Printer.Print "Below Min"
         Printer.FontBold = False
   For totalcounter = 41 To maxcounter
         Printer.CurrentX = 400
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeLastName(totalcounter), "@@@@@@@"), 9)
         Printer.CurrentX = 1800
         Printer.CurrentY = PrintPosition
         Printer.Print Left(Format(EmployeeFirstName(totalcounter), "@@@"), 9)
         Printer.CurrentX = 3600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(EmployeeNumber(totalcounter), "0000")
         Printer.CurrentX = 5200
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalSales(totalcounter), "currency")
         Printer.CurrentX = 6900
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalTips(totalcounter), "currency")
         Printer.CurrentX = 8600
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TipPercent(totalcounter), "percent")
         Printer.CurrentX = 10300
         Printer.CurrentY = PrintPosition
         Printer.Print Format(TotalPayTips(totalcounter), "currency")
         Printer.CurrentX = 12000
         Printer.CurrentY = PrintPosition
         Printer.Print TotalHours(totalcounter)
         Printer.CurrentX = 13700
         Printer.CurrentY = PrintPosition
         Printer.Print Format(AmountBelow(totalcounter), "currency")
         PrintPosition = PrintPosition + 400
      Next
      Printer.EndDoc
   End If
Unload frmRequest
Unload frmMain
End
End Sub

