VERSION 5.00
Begin VB.Form FrmSSN 
   BorderStyle     =   0  'None
   ClientHeight    =   4635
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7410
   LinkTopic       =   "Form1"
   ScaleHeight     =   4635
   ScaleWidth      =   7410
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Social Security Error(s)"
      Height          =   3855
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   6975
      Begin VB.ListBox List3 
         Height          =   1230
         ItemData        =   "FrmSSN.frx":0000
         Left            =   4800
         List            =   "FrmSSN.frx":0002
         TabIndex        =   4
         Top             =   1800
         Width           =   1935
      End
      Begin VB.ListBox List2 
         Height          =   1230
         ItemData        =   "FrmSSN.frx":0004
         Left            =   2640
         List            =   "FrmSSN.frx":0006
         TabIndex        =   3
         Top             =   1800
         Width           =   1695
      End
      Begin VB.ListBox List1 
         Height          =   1230
         ItemData        =   "FrmSSN.frx":0008
         Left            =   360
         List            =   "FrmSSN.frx":000A
         TabIndex        =   2
         Top             =   1800
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Social Security"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4800
         TabIndex        =   7
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label3 
         Caption         =   "Last Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2640
         TabIndex        =   6
         Top             =   1560
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "First Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "The following employees have social security number errors."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   360
         TabIndex        =   1
         Top             =   600
         Width           =   6015
      End
   End
End
Attribute VB_Name = "FrmSSN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
