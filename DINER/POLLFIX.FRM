VERSION 4.00
Begin VB.Form Form1 
   Caption         =   "Diner Common Poll File Fix"
   ClientHeight    =   735
   ClientLeft      =   1425
   ClientTop       =   1965
   ClientWidth     =   6405
   Height          =   1245
   Left            =   1365
   LinkTopic       =   "Form1"
   ScaleHeight     =   735
   ScaleWidth      =   6405
   Top             =   1515
   Width           =   6525
End
Attribute VB_Name = "Form1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub Form_Load()
FileCopy "C:\POLLING\TEXDLY.POL", "C:\POLLING\TEXDLY.PRE"
Open "C:\POLLING\TEXDLY.POL" For Output As #2
Open "C:\POLLING\TEXDLY.PRE" For Input As #1 ' Open file.
Do While Not EOF(1) ' Loop until end of file.
    Line Input #1, Textline ' Read line into variable.
    TextLen = Len(Textline)
    If Mid$(Textline, 2, 4) = "R102" Then
        NewInsert = Mid$(Textline, 8, 13)
        Print #2, Textline
    ElseIf Mid$(Textline, 2, 4) = "BneR" Then
        Print #2, Textline
    ElseIf Mid$(Textline, 8, 4) = "6011" Then
        Print #2, Textline
    Else
        Print #2, Mid$(Textline, 1, 7) + NewInsert + Mid$(Textline, 9, TextLen)
    End If
Loop
Close #1    ' Close file.
Close #2
End
End Sub


