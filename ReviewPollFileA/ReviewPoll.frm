VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Review IRIS Poll Fles"
   ClientHeight    =   8505
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11025
   Icon            =   "ReviewPoll.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8505
   ScaleWidth      =   11025
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox HideSalaried 
      Caption         =   "Hide Salaried"
      Height          =   255
      Left            =   1800
      TabIndex        =   10
      Top             =   8040
      Width           =   1815
   End
   Begin VB.CheckBox HidePayroll 
      Caption         =   "Hide Payroll"
      Height          =   255
      Left            =   4080
      TabIndex        =   9
      Top             =   8040
      Width           =   1335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Copy to Clipboard"
      Enabled         =   0   'False
      Height          =   495
      Left            =   3600
      TabIndex        =   7
      Top             =   7320
      Width           =   1575
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Print"
      Enabled         =   0   'False
      Height          =   495
      Left            =   1920
      TabIndex        =   6
      Top             =   7320
      Width           =   1575
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6810
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   10695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      Height          =   495
      Left            =   5280
      TabIndex        =   1
      Top             =   7320
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Review"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   7320
      Width           =   1575
   End
   Begin VB.FileListBox File1 
      Height          =   2040
      Left            =   600
      Pattern         =   "*.IRS"
      TabIndex        =   3
      Top             =   4080
      Width           =   6615
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Version 1.1"
      Height          =   375
      Left            =   9840
      TabIndex        =   8
      Top             =   7080
      Width           =   975
   End
   Begin VB.Label Label2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   6720
      TabIndex        =   5
      Top             =   7440
      Width           =   3975
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   375
      Left            =   6720
      TabIndex        =   4
      Top             =   7080
      Width           =   1335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
'On Error Resume Next
Screen.MousePointer = 11
List1.Clear
Label2.Caption = ""
Form1.Refresh
For y = 0 To File1.ListCount - 1

If HidePayroll.Value = 0 Then
    
    'Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "P.csv") For Output As #6
    'Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "C.csv") For Output As #7
    Open "V:\poll\Processed\" + File1.List(y) For Input As #1
    Do Until EOF(1)
        CheckHours = False
        Line Input #1, lineData
        If Mid$(lineData, 2, 4) = "P100" Then
            FileInput = Split(lineData, ",")
             
             If FileInput(10) > 1438 Then
                CheckHours = True
            End If
             If FileInput(11) > 1438 Then
                CheckHours = True
            End If
             If FileInput(12) > 1438 Then
                CheckHours = True
            End If
             If FileInput(13) > 1438 Then
                CheckHours = True
            End If
             If FileInput(14) > 1438 Then
                CheckHours = True
            End If
             If FileInput(15) > 1438 Then
                CheckHours = True
            End If
             If FileInput(16) > 1438 Then
                CheckHours = True
            End If
             EmpNumber = FileInput(4)
             FirstName = FileInput(5)
             LastName = FileInput(6)
             
             FirstNameTemp = FirstName
             FirstName = Replace(FirstNameTemp, Chr$(34), "")
             LastNameTemp = LastName
             LastName = Replace(LastNameTemp, Chr$(34), "")
             
             'Job Code
             JobCode = FileInput(8)
             
        Select Case JobCode
        Case "230824"
            JobDesc = "DMIT"
        Case "230805"
            JobDesc = "SGM "
        Case "230820"
            JobDesc = "GM  "
        Case "230822"
            JobDesc = "GMIT"
        Case "230832"
            JobDesc = "HMGR"
        Case "230855"
            JobDesc = "MGRT"
        Case "230845"
            JobDesc = "SLDR"
        Case "230810"
            JobDesc = "BRKL"
        Case "230840"
            JobDesc = "CRWT"
        Case "230825"
            JobDesc = "CREW"
        Case Else
            JobDesc = JobCode
        End Select
             
            If HideSalaried.Value = 1 Then
                If JobCode = 230805 Then
                    CheckHours = False
                End If
            End If
            
            If HideSalaried.Value = 1 Then
                If JobCode = 230820 Then
                    CheckHours = False
                End If
            End If
            
            If HideSalaried.Value = 1 Then
                If JobCode = 230824 Then
                    CheckHours = False
                End If
            End If
            
            
            If CheckHours = True Then
                AddInfo = Mid$(File1.List(y), 1, 4) + " Check Hours " + JobDesc + " " + FirstName + " " + LastName + " " + EmpNumber
                List1.AddItem AddInfo
            End If
      '      Print #6, linedata
        End If
        'If Mid$(linedata, 7, 11) = "TBLORDERPAY" Then
        '    Print #7, linedata
        'End If
    
    Loop
    Close #1
    'Close #6, #7
End If  ' Hide Payroll
'Credit Card from SAS File
'1090_warehouse-2022-10-22.IRS.processed
SASDATE1 = Now
SASDATE = Format$(SASDATE1, "YYYY") + "-" + Format$(SASDATE1, "M") + "-" + Format$(SASDATE1, "D")

SASFile = Mid$(File1.List(y), 1, 4) + "_warehouse-" + SASDATE + ".IRS.processed"

If Dir("V:\poll\prep\warehouse\" + SASFile) = "" Then
    SASFile = Mid$(File1.List(y), 1, 4) + "_warehouse-" + SASDATE + ".IRS"
End If
If Dir("V:\poll\prep\warehouse\" + SASFile) <> "" Then

    Open "V:\poll\prep\warehouse\" + SASFile For Input As #1
    Count1 = 0
    Count2 = 0
    Count3 = 0
    Count4 = 0
    Count5 = 0
    Count6 = 0
    Total1 = 0
    Total2 = 0
    Total3 = 0
    Total4 = 0
    Total5 = 0
    Total6 = 0
    Do Until EOF(1)
            Line Input #1, lineData
            If Mid$(lineData, 7, 11) = "TBLORDERPAY" Then
                FileInput = Split(lineData, ",")
                RegNum = FileInput(3)
                PayType = FileInput(6)
                ProState = FileInput(33)
                If Mid$(PayType, 1, 1) = 2 Then
                    If RegNum = 4 Then
                        Total4 = Total4 + 1
                        If ProState <> 1 Then
                            Count4 = Count4 + 1
                        End If
                    End If
 
            
                    If RegNum = 3 Then
                        Total3 = Total3 + 1
                        If ProState <> 1 Then
                            Count3 = Count3 + 1
                        End If
                    End If

                    If RegNum = 2 Then
                        Total2 = Total2 + 1
                        If ProState <> 1 Then
                            Count2 = Count2 + 1
                        End If
                    End If

                    If RegNum = 1 Then
                        Total1 = Total1 + 1
                        If ProState <> 1 Then
                            Count1 = Count1 + 1
                        End If
                    End If

                    If RegNum = 5 Then
                        Total5 = Total5 + 1
                        If ProState <> 1 Then
                            Count5 = Count5 + 1
                        End If
                    End If

                    If RegNum = 6 Then
                        Total6 = Total6 + 1
                        If ProState <> 1 Then
                            Count6 = Count6 + 1
                        End If
                    End If
                End If
            End If
        Loop
    If Total4 <> 0 Then
        Percent4 = (Count4 / Total4) * 100
        Else
        Percent4 = 0
    End If
    If Percent4 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 3 Errors on " + Format$(Str(Percent4), "###") + "% of transactions (4)"
       List1.AddItem AddInfo
    End If
    
    If Total3 <> 0 Then
        Percent3 = (Count3 / Total3) * 100
        Else
        Percent3 = 0
    End If
    If Percent3 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 3 Errors on " + Format$(Str(Percent3), "###") + "% of transactions (3)"
       List1.AddItem AddInfo
    End If

    If Total2 <> 0 Then
        Percent2 = (Count2 / Total2) * 100
        Else
        Percent2 = 0
    End If
    If Percent2 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 2 Errors on " + Format$(Str(Percent2), "###") + "% of transactions"
       List1.AddItem AddInfo
    End If
    
    If Total1 <> 0 Then
        Percent1 = (Count1 / Total1) * 100
        Else
        Percent1 = 0
    End If
    If Percent1 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 1 Errors on " + Format$(Str(Percent1), "###") + "% of transactions"
       List1.AddItem AddInfo
    End If
    
    If Total5 <> 0 Then
        Percent5 = (Count5 / Total5) * 100
        Else
        Percent5 = 0
    End If
    If Percent5 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 5 Errors on " + Format$(Str(Percent5), "###") + "% of transactions"
       List1.AddItem AddInfo
    End If
    
    If Total6 <> 0 Then
        Percent6 = (Count6 / Total6) * 100
        Else
        Percent6 = 0
    End If
    If Percent6 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 6 Errors on " + Format$(Str(Percent6), "###") + "% of transactions"
       List1.AddItem AddInfo
    End If
    
    
    Form1.Refresh

    Close #1
End If


Next y
    
    
    
    


Label2.Caption = Str(List1.ListCount) + " issues to research"
Command3.Enabled = True
Command4.Enabled = True
Screen.MousePointer = 0
End Sub

Private Sub Command2_Click()
    End
End Sub

Private Sub Command3_Click()

Printer.Print
Printer.Print
   Printer.FontName = "COURIER NEW"
   Printer.FontSize = 12
  '    Printer.CurrentX = 600
  ' Printer.Print "####   Restaurant Name       Version   File Date Time    Scheduled"
  '    Printer.CurrentX = 600
  ' Printer.Print "--------------------------------------------------------------------------"
For I = 0 To List1.ListCount - 1
Printer.CurrentX = 600
Printer.Print List1.List(I)
Printer.Print
Printer.Print
Next I
Printer.Print
Printer.FontSize = 10
Printer.CurrentX = 2600
Printer.Print Str(Now())
Printer.EndDoc
m = MsgBox("Printed", vbInformation, "Printing Status")

End Sub

Private Sub Command4_Click()
 Clipboard.Clear
  Copytoclip = ""
For I = 0 To List1.ListCount - 1
Copytoclip = Copytoclip & List1.List(I) & Chr$(13) & Chr$(10)
Next I
Clipboard.SetText Copytoclip
m = MsgBox("Copy to Clipboard complete.", vbInformation, "Clipboard Status")
End Sub

Private Sub Form_Load()
File1.FileName = "v:\Poll\Processed"
File1.Pattern = "*.irs"
Label1.Caption = Str(File1.ListCount) + " Files"
HidePayroll.Value = 1
Open "v:\poll\credit card check\Issues.csv" For Append As #7
SASDATE1 = Now
SASDATE = Format$(SASDATE1, "YYYY") + "-" + Format$(SASDATE1, "M") + "-" + Format$(SASDATE1, "D")
Print #7, SASDATE + "," + "9999,Program Executed"
'On Error Resume Next
Screen.MousePointer = 11
List1.Clear
Label2.Caption = ""
Form1.Refresh
For y = 0 To File1.ListCount - 1

If HidePayroll.Value = 0 Then
    
    'Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "P.csv") For Output As #6
    'Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "C.csv") For Output As #7
    Open "V:\poll\Processed\" + File1.List(y) For Input As #1
    Do Until EOF(1)
        CheckHours = False
        Line Input #1, lineData
        If Mid$(lineData, 2, 4) = "P100" Then
            FileInput = Split(lineData, ",")
             
             If FileInput(10) > 1438 Then
                CheckHours = True
            End If
             If FileInput(11) > 1438 Then
                CheckHours = True
            End If
             If FileInput(12) > 1438 Then
                CheckHours = True
            End If
             If FileInput(13) > 1438 Then
                CheckHours = True
            End If
             If FileInput(14) > 1438 Then
                CheckHours = True
            End If
             If FileInput(15) > 1438 Then
                CheckHours = True
            End If
             If FileInput(16) > 1438 Then
                CheckHours = True
            End If
             EmpNumber = FileInput(4)
             FirstName = FileInput(5)
             LastName = FileInput(6)
             
             FirstNameTemp = FirstName
             FirstName = Replace(FirstNameTemp, Chr$(34), "")
             LastNameTemp = LastName
             LastName = Replace(LastNameTemp, Chr$(34), "")
             
             'Job Code
             JobCode = FileInput(8)
             
        Select Case JobCode
        Case "230824"
            JobDesc = "DMIT"
        Case "230805"
            JobDesc = "SGM "
        Case "230820"
            JobDesc = "GM  "
        Case "230822"
            JobDesc = "GMIT"
        Case "230832"
            JobDesc = "HMGR"
        Case "230855"
            JobDesc = "MGRT"
        Case "230845"
            JobDesc = "SLDR"
        Case "230810"
            JobDesc = "BRKL"
        Case "230840"
            JobDesc = "CRWT"
        Case "230825"
            JobDesc = "CREW"
        Case Else
            JobDesc = JobCode
        End Select
             
            If HideSalaried.Value = 1 Then
                If JobCode = 230805 Then
                    CheckHours = False
                End If
            End If
            
            If HideSalaried.Value = 1 Then
                If JobCode = 230820 Then
                    CheckHours = False
                End If
            End If
            
            If HideSalaried.Value = 1 Then
                If JobCode = 230824 Then
                    CheckHours = False
                End If
            End If
            
            
            If CheckHours = True Then
                AddInfo = Mid$(File1.List(y), 1, 4) + " Check Hours " + JobDesc + " " + FirstName + " " + LastName + " " + EmpNumber
                List1.AddItem AddInfo
            End If
      '      Print #6, linedata
        End If
        'If Mid$(linedata, 7, 11) = "TBLORDERPAY" Then
        '    Print #7, linedata
        'End If
    
    Loop
    Close #1
    'Close #6, #7
End If  ' Hide Payroll
'Credit Card from SAS File
'1090_warehouse-2022-10-22.IRS.processed


PollDate = Now - 1
PollDateFile = Format$(PollDate, "YYYY") + "-" + Format$(PollDate, "M") + "-" + Format$(PollDate, "D")
SASDATE1 = Now
SASDATE = Format$(SASDATE1, "YYYY") + "-" + Format$(SASDATE1, "M") + "-" + Format$(SASDATE1, "D")

SASFile = Mid$(File1.List(y), 1, 4) + "_warehouse-" + SASDATE + ".IRS.processed"

If Dir("V:\poll\prep\warehouse\" + SASFile) = "" Then
    SASFile = Mid$(File1.List(y), 1, 4) + "_warehouse-" + SASDATE + ".IRS"
End If
If Dir("V:\poll\prep\warehouse\" + SASFile) <> "" Then

    Open "V:\poll\prep\warehouse\" + SASFile For Input As #1
    Count1 = 0
    Count2 = 0
    Count3 = 0
    Count4 = 0
    Count5 = 0
    Count6 = 0
    Total1 = 0
    Total2 = 0
    Total3 = 0
    Total4 = 0
    Total5 = 0
    Total6 = 0
    Do Until EOF(1)
            Line Input #1, lineData
            If Mid$(lineData, 7, 11) = "TBLORDERPAY" Then
                FileInput = Split(lineData, ",")
                RegNum = FileInput(3)
                PayType = FileInput(6)
                ProState = FileInput(33)
                If Mid$(PayType, 1, 1) = 2 Then
                    If RegNum = 4 Then
                        Total4 = Total4 + 1
                        If ProState <> 1 Then
                            Count4 = Count4 + 1
                        End If
                    End If
 
            
                    If RegNum = 3 Then
                        Total3 = Total3 + 1
                        If ProState <> 1 Then
                            Count3 = Count3 + 1
                        End If
                    End If

                    If RegNum = 2 Then
                        Total2 = Total2 + 1
                        If ProState <> 1 Then
                            Count2 = Count2 + 1
                        End If
                    End If

                    If RegNum = 1 Then
                        Total1 = Total1 + 1
                        If ProState <> 1 Then
                            Count1 = Count1 + 1
                        End If
                    End If

                    If RegNum = 5 Then
                        Total5 = Total5 + 1
                        If ProState <> 1 Then
                            Count5 = Count5 + 1
                        End If
                    End If

                    If RegNum = 6 Then
                        Total6 = Total6 + 1
                        If ProState <> 1 Then
                            Count6 = Count6 + 1
                        End If
                    End If
                End If
            End If
        Loop
    If Total4 <> 0 Then
        Percent4 = (Count4 / Total4) * 100
        Else
        Percent4 = 0
    End If
    If Percent4 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 3 Errors on " + Format$(Str(Percent4), "###") + "% of transactions (4)"
       List1.AddItem AddInfo
       Print #7, PollDateFile + "," + Mid$(File1.List(y), 1, 4) + "," + "Check Payment Terminal 3 Errors on " + Format$(Str(Percent4), "###") + "% of transactions (4)"
    
    End If
    
    If Total3 <> 0 Then
        Percent3 = (Count3 / Total3) * 100
        Else
        Percent3 = 0
    End If
    If Percent3 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 3 Errors on " + Format$(Str(Percent3), "###") + "% of transactions (3)"
       List1.AddItem AddInfo
       Print #7, PollDateFile + "," + Mid$(File1.List(y), 1, 4) + "," + "Check Payment Terminal 3 Errors on " + Format$(Str(Percent3), "###") + "% of transactions (3)"
    
    End If

    If Total2 <> 0 Then
        Percent2 = (Count2 / Total2) * 100
        Else
        Percent2 = 0
    End If
    If Percent2 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 2 Errors on " + Format$(Str(Percent2), "###") + "% of transactions"
       
       List1.AddItem AddInfo
       Print #7, PollDateFile + "," + Mid$(File1.List(y), 1, 4) + "," + "Check Payment Terminal 2 Errors on " + Format$(Str(Percent2), "###") + "% of transactions"
    
    End If
    
    If Total1 <> 0 Then
        Percent1 = (Count1 / Total1) * 100
        Else
        Percent1 = 0
    End If
    If Percent1 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 1 Errors on " + Format$(Str(Percent1), "###") + "% of transactions"
       List1.AddItem AddInfo
       Print #7, PollDateFile + "," + Mid$(File1.List(y), 1, 4) + "," + "Check Payment Terminal 1 Errors on " + Format$(Str(Percent1), "###") + "% of transactions"
    
    End If
    
    If Total5 <> 0 Then
        Percent5 = (Count5 / Total5) * 100
        Else
        Percent5 = 0
    End If
    If Percent5 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 5 Errors on " + Format$(Str(Percent5), "###") + "% of transactions"
       List1.AddItem AddInfo
       Print #7, PollDateFile + "," + Mid$(File1.List(y), 1, 4) + "," + "Check Payment Terminal 5 Errors on " + Format$(Str(Percent5), "###") + "% of transactions"
    
    End If
    
    If Total6 <> 0 Then
        Percent6 = (Count6 / Total6) * 100
        Else
        Percent6 = 0
    End If
    If Percent6 > 5 Then
    
       AddInfo = Mid$(File1.List(y), 1, 4) + " Check Payment Terminal 6 Errors on " + Format$(Str(Percent6), "###") + "% of transactions"
       List1.AddItem AddInfo
       Print #7, PollDateFile + "," + Mid$(File1.List(y), 1, 4) + "," + "Check Payment Terminal 6 Errors on " + Format$(Str(Percent6), "###") + "% of transactions"
    
    End If
    
    
    Form1.Refresh

    Close #1
End If


Next y
    
    
    
    

Close #7
Label2.Caption = Str(List1.ListCount) + " issues to research"
Command3.Enabled = True
Command4.Enabled = True
Screen.MousePointer = 0

End


End Sub

