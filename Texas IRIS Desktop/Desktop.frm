VERSION 5.00
Begin VB.Form DeskTop 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Texas IRIS Desktop"
   ClientHeight    =   5325
   ClientLeft      =   600
   ClientTop       =   1095
   ClientWidth     =   5985
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   FillColor       =   &H00FFFFFF&
   FillStyle       =   0  'Solid
   ForeColor       =   &H00FF0000&
   Icon            =   "Desktop.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5325
   ScaleWidth      =   5985
   Begin VB.CommandButton Exit 
      Caption         =   "EXI&T"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   14
      Top             =   4320
      Width           =   5655
   End
   Begin VB.Frame FrameIE 
      BackColor       =   &H00FFFFFF&
      Caption         =   " Internet "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3120
      TabIndex        =   12
      Top             =   2640
      Width           =   2655
      Begin VB.CommandButton CmdIE 
         Caption         =   "Co&nnect"
         Height          =   375
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.CommandButton LoadCommButton 
      Caption         =   "&Load Communications"
      Height          =   375
      Left            =   360
      TabIndex        =   11
      Top             =   3360
      Width           =   2175
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Caption         =   " Support "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   2520
      Width           =   2655
      Begin VB.CommandButton CmdPCAnywhere 
         Caption         =   "&PC Anywhere"
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   2175
      End
      Begin VB.Label LblVersion 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Caption         =   "Version 1.0"
         Height          =   255
         Left            =   600
         TabIndex        =   10
         Top             =   1320
         Width           =   1695
      End
   End
   Begin VB.FileListBox File1 
      Height          =   480
      Left            =   120
      TabIndex        =   9
      Top             =   4320
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00FFFFFF&
      Caption         =   " Spreadsheets "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   3120
      TabIndex        =   6
      Top             =   240
      Width           =   2655
      Begin VB.CommandButton CmdSpreadCurrent 
         Caption         =   "CurrentSpreadsheet"
         Height          =   615
         Left            =   240
         TabIndex        =   7
         Top             =   1320
         Width           =   2175
      End
      Begin VB.CommandButton CmdSpreadPrevious 
         Caption         =   "PreviousSpreadsheet"
         Height          =   615
         Left            =   240
         TabIndex        =   8
         Top             =   480
         Width           =   2175
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FFFFFF&
      Caption         =   " BNE Applications "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   2655
      Begin VB.CommandButton CmdExcel 
         Caption         =   "Microsoft &Excel"
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   1560
         Width           =   2175
      End
      Begin VB.CommandButton CmdWord 
         Caption         =   "Microsoft &Word"
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CmdXcellenet 
         Caption         =   "&XcelleNet Desktop"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   2175
      End
   End
End
Attribute VB_Name = "DeskTop"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Public unitnum As Integer
 Public IJ As Integer
 Public SSDone As Boolean
 'Option Explicit

      Private Type LUID
         UsedPart As Long
         IgnoredForNowHigh32BitPart As Long
      End Type
      
      Private Type TOKEN_PRIVILEGES
        PrivilegeCount As Long
        TheLuid As LUID
        Attributes As Long
      End Type

      Private Const EWX_SHUTDOWN As Long = 1
      Private Const EWX_FORCE As Long = 4
      Private Const EWX_REBOOT = 2
      Private Declare Function ExitWindowsEx Lib "user32" (ByVal _
           dwOptions As Long, ByVal dwReserved As Long) As Long

      Private Declare Function GetCurrentProcess Lib "kernel32" () As Long
      Private Declare Function OpenProcessToken Lib "advapi32" (ByVal _
         ProcessHandle As Long, _
         ByVal DesiredAccess As Long, TokenHandle As Long) As Long
      Private Declare Function LookupPrivilegeValue Lib "advapi32" _
         Alias "LookupPrivilegeValueA" _
         (ByVal lpSystemName As String, ByVal lpName As String, lpLuid _
         As LUID) As Long
      Private Declare Function AdjustTokenPrivileges Lib "advapi32" _
         (ByVal TokenHandle As Long, _
         ByVal DisableAllPrivileges As Long, NewState As TOKEN_PRIVILEGES _
         , ByVal BufferLength As Long, _
      PreviousState As TOKEN_PRIVILEGES, ReturnLength As Long) As Long
Public Function GetAppHwnd(Optional ByVal Class As String = vbNullString, Optional ByVal Caption As String = vbNullString) As Long
GetAppHwnd = FindWindow(Class, Caption)
End Function


Private Sub Exit_Click()
End
End Sub


Private Sub CmdAutoReports_Click()
Y = Shell("C:\MICROS\COMMON\BIN\AutoSeqExec.EXE", vbNormalFocus)
End Sub

Private Sub CmdControl_Click()
Y = Shell("C:\MICROS\RES\POS\BIN\Cpanel.exe", vbNormalFocus)
End Sub

Private Sub CmdCredit_Click()
ChDir ("C:\MICROS\RES\POS\BIN")
Y = Shell("C:\MICROS\RES\POS\BIN\CreditCards.exe", vbNormalFocus)
End Sub

Private Sub CmdExcel_Click()
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
   Y = Shell("c:\Program Files\Microsoft Office\Office\Excel.exe", vbMaximizedFocus)
   End
Else
   Err.Clear
End If
End Sub



Private Sub CmdIE_Click()
'Y = Shell("c:\NODESYS\WINXNODE.EXE -R", vbNormalFocus)
'SendKeys "{ENTER}", True

 'IntMsg = MsgBox("Remember to click on the Disconnect button after you have finished using the Internet." + Chr(10) + Chr(10) + "After you click on the Disconnect button, then click on the Hang Up button, then answer Yes, and then OK.", 6, "Internet")
Y = Shell("C:\PROGRAM FILES\Dial-Up Monitor\DMONITOR.EXE /dial:InternetConnection", vbNormalFocus)
End
End Sub

Private Sub CmdLoadsu_Click()
FrmPassword.Show
End Sub

Private Sub CmdManager_Click()
Y = Shell("C:\MICROS\RES\POS\BIN\Procedures.exe", vbNormalFocus)
End Sub

Private Sub CmdPCAnywhere_Click()
SUPPORTYESNO = MsgBox("PC Anywhere is software that allows Restaurant Systems to remotely control your computer to assist you with problems and make configuration changes.  You should only load this software if asked to do so by someone with Restaurant Systems.  Click the YES button to load PC Anywere.  Click the NO button to return to the menu.", 4)
    If SUPPORTYESNO = 6 Then
    ' REMOVE XCELLENET COMMUNICATIONS
              
              'PCanywhere = MsgBox("A grey screen will appear ....", 0, "Instructions")
'              REMOVEXNET = Shell("c:\NODESYS\WINXNODE.EXE -R", vbNormalFocus)
              'AppActivate REMOVEXNET
 '             SendKeys "{ENTER}", True
              LOADPCAW = Shell("c:\PROGRAM FILES\symantec\pcanywhere\AWHOST32.EXE", vbMaximizedFocus)
              End
    End If
End Sub

Private Sub CmdReplication_Click()
Y = Shell("C:\MICROS\RES\EM\BIN\Replication.exe", vbNormalFocus)
End Sub

Private Sub CmdShutdown_Click()
If Shutdown = False Then
        SDMSG = MsgBox("You have selected the Shut Down button.  Clicking on the Yes button will cause the Micros Control Panel to appear.  Stop all Micros processes and close the Control Panel before clicking on Shut Down again.  Information on how this affects your system is found in your Information Systems Training Manual under the Troubleshooting section." + Chr(10) + Chr(10) + "1.  To stop the processes, click on the Off button in the Status tab." + Chr(10) + "2.  Click on the OK button to confirm the Changing State of Restaurant to Off message." + Chr(10) + "3.  Once Restaurant displays a red X beside it, click on File on the menu bar, and Exit from the dropdown list." + Chr(10) + "4.  Click on Shut Down to continue resetting your computer.", 4, "Control Panel Instructions")
            If SDMSG = 6 Then
                Shutdown = True
                ' LOAD CONTROL PANEL
                Y = Shell("c:\MICROS\RES\POS\BIN\CPANEL.EXE", vbNormalFocus)
            End If
    Else
            SDMSG = MsgBox("Click on OK to shut down your back office server.  The message It is now safe to turn off your computer will appear.  Turn off the computer using the button immediately to the right of the Firecracker.  Wait at least 15 seconds before turning the computer on.  The computer will load all of the processes automatically.", 0, "Shut Down Server?")
            AdjustToken
            ExitWindowsEx (EWX_SHUTDOWN Or EWX_FORCE), &HFFFF
    End If
End Sub


Private Sub CmdSpreadCurrent_Click()
Dim Tempperiod As String
Tempperiod = period
If Tempperiod < 10 Then
   Tempperiod = "0" & Tempperiod
End If
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("C:\Program Files\Microsoft Office\Office\Excel.exe C:\MSOffice\Excel\SI" & Tempperiod & ".xls", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub

Private Sub CmdSpreadPrevious_Click()
Dim Tempperiod As String
If period = 1 Then
   Tempperiod = 12
Else
   Tempperiod = period - 1
End If
If Tempperiod < 10 Then
   Tempperiod = "0" & Tempperiod
End If
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("C:\Program Files\Microsoft Office\Office\Excel.exe C:\MSOffice\Excel\SI" & Tempperiod & ".xls", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub

Private Sub CmdSTMenu_Click()
Y = Shell("C:\Windows\STMenu.exe", vbNormalFocus)
End Sub



Private Sub CmdWord_Click()
On Error GoTo BypassApp
AppActivate "Microsoft Word"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("c:\Program Files\Microsoft Office\Office\Winword.exe", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub

Private Sub CmdXcellenet_Click()
Y = Shell("C:\Nodesys\NDeskTop.exe 30 gm" & unitnum & " gm" & unitnum, vbNormalFocus)
End
End Sub



 
Private Sub AdjustToken()
         Const TOKEN_ADJUST_PRIVILEGES = &H20
         Const TOKEN_QUERY = &H8
         Const SE_PRIVILEGE_ENABLED = &H2
         Dim hdlProcessHandle As Long
         Dim hdlTokenHandle As Long
         Dim tmpLuid As LUID
         Dim tkp As TOKEN_PRIVILEGES
         Dim tkpNewButIgnored As TOKEN_PRIVILEGES
         Dim lBufferNeeded As Long

         hdlProcessHandle = GetCurrentProcess()
         OpenProcessToken hdlProcessHandle, (TOKEN_ADJUST_PRIVILEGES Or _
            TOKEN_QUERY), hdlTokenHandle

      ' Get the LUID for shutdown privilege.
         LookupPrivilegeValue "", "SeShutdownPrivilege", tmpLuid

         tkp.PrivilegeCount = 1    ' One privilege to set
         tkp.TheLuid = tmpLuid
         tkp.Attributes = SE_PRIVILEGE_ENABLED

     ' Enable the shutdown privilege in the access token of this process.
         AdjustTokenPrivileges hdlTokenHandle, False, _
         tkp, Len(tkpNewButIgnored), tkpNewButIgnored, lBufferNeeded

End Sub


Private Sub Disconnect_Click()
    Y = Shell("c:\dunmon.bat", vbMaximizedFocus)
End Sub


Private Sub Form_Load()
Dim Message As String
Dim TempTestDate As Date
Dim StartDate As Date
Dim EndDate As Date
Dim TempBusinessday As Date
Dim period1Begin As Date
Dim period1End As Date
Dim period2Begin As Date
Dim period2End As Date
Dim period3Begin As Date
Dim period3End As Date
Dim period4Begin As Date
Dim period4End As Date
Dim period5Begin As Date
Dim period5End As Date
Dim period6Begin As Date
Dim period6End As Date
Dim period7Begin As Date
Dim period7End As Date
Dim period8Begin As Date
Dim period8End As Date
Dim period9Begin As Date
Dim period9End As Date
Dim period10Begin As Date
Dim period10End As Date
Dim period11Begin As Date
Dim period11End As Date
Dim period12Begin As Date
Dim period12End As Date
Dim period13Begin As Date
Dim tempunitnum As String
Check = True
POS2800 = False
SSDone = False
If Dir("C:\Nodesys\Unitid.dat") <> "" Then
   Open "C:\Nodesys\Unitid.dat" For Input As #1
      Input #1, tempunitnum
   Close #1
Else
   MsgBox "Unit id does not exist, Please call Help Desk at (252) 937-2800, ext 1437"
End If
unitnum = Int(Left(tempunitnum, 4))
'Picture1.Picture = LoadPicture("C:\Windows\TexLogo.jpg")
StartDate = #12/31/1998#
TempTestDate = Date
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
   period1Begin = StartDate
   period2Begin = DateAdd("ww", 4, StartDate)
   period1End = DateAdd("d", -1, period2Begin)
   period3Begin = DateAdd("ww", 4, period2Begin)
   period2End = DateAdd("d", -1, period3Begin)
   period4Begin = DateAdd("ww", 5, period3Begin)
   period3End = DateAdd("d", -1, period4Begin)
   period5Begin = DateAdd("ww", 4, period4Begin)
   period4End = DateAdd("d", -1, period5Begin)
   period6Begin = DateAdd("ww", 4, period5Begin)
   period5End = DateAdd("d", -1, period6Begin)
   period7Begin = DateAdd("ww", 5, period6Begin)
   period6End = DateAdd("d", -1, period7Begin)
   period8Begin = DateAdd("ww", 4, period7Begin)
   period7End = DateAdd("d", -1, period8Begin)
   period9Begin = DateAdd("ww", 4, period8Begin)
   period8End = DateAdd("d", -1, period9Begin)
   period10Begin = DateAdd("ww", 5, period9Begin)
   period9End = DateAdd("d", -1, period10Begin)
   period11Begin = DateAdd("ww", 4, period10Begin)
   period10End = DateAdd("d", -1, period11Begin)
   period12Begin = DateAdd("ww", 4, period11Begin)
   period11End = DateAdd("d", -1, period12Begin)
   period13Begin = DateAdd("ww", 5, period12Begin)
   period12End = DateAdd("d", -1, period13Begin)

Select Case TempTestDate
       Case Is >= period12Begin
                 StartDate = period11Begin
                 EndDate = period11End
                 period = 12
       Case Is >= period11Begin
                 StartDate = period10Begin
                 EndDate = period10End
                 period = 11
       Case Is >= period10Begin
                 StartDate = period9Begin
                 EndDate = period9End
                 period = 10
       Case Is >= period9Begin
                 StartDate = period8Begin
                 EndDate = period8End
                 period = 9
       Case Is >= period8Begin
                 StartDate = period7Begin
                 EndDate = period7End
                 period = 8
       Case Is >= period7Begin
                 StartDate = period6Begin
                 EndDate = period6End
                 period = 7
       Case Is >= period6Begin
                 StartDate = period5Begin
                 EndDate = period5End
                 period = 6
       Case Is >= period5Begin
                 StartDate = period4Begin
                 EndDate = period4End
                 period = 5
       Case Is >= period4Begin
                 StartDate = period3Begin
                 EndDate = period3End
                 period = 4
       Case Is >= period3Begin
                 StartDate = period2Begin
                 EndDate = period2End
                 period = 3
       Case Is >= period2Begin
                 StartDate = period1Begin
                 EndDate = period1End
                 period = 2
       Case Is >= period1Begin
                 StartDate = DateAdd("WW", -52, period12Begin)
                 EndDate = DateAdd("WW", -52, period12End)
                 period = 1
End Select
If period = 1 Then
   CmdSpreadPrevious.Caption = "Period 12"
Else
   CmdSpreadPrevious.Caption = "Period " & period - 1
End If
CmdSpreadCurrent.Caption = "Period " & period
End Sub



Private Sub LoadCommButton_Click()
    progname = "C:\NODESYS\Winxnode.exe"
    X = Shell(progname, vbNormalFocus)
    DoEvents
    'SetActiveWindow ("RemoteWare Node Error")
    For I = 1 To 10
        For J = 1 To 1000
            DoEvents
        Next J
    Next I
    DoEvents
    SendKeys "{ENTER}", True
    progname = "C:\NODESYS\NODECOMM.exe"
    X = Shell(progname, vbNormalFocus)
    End
End Sub

Private Sub Timer1_Timer()
Dim I As Integer
Dim J As Integer
Dim TempMessage As String
Dim Message As String
Dim ID As Long
Dim TempTestDate As Date
Dim StartDate As Date
Dim EndDate As Date
Dim TempBusinessday As Date
Dim period1Begin As Date
Dim period1End As Date
Dim period2Begin As Date
Dim period2End As Date
Dim period3Begin As Date
Dim period3End As Date
Dim period4Begin As Date
Dim period4End As Date
Dim period5Begin As Date
Dim period5End As Date
Dim period6Begin As Date
Dim period6End As Date
Dim period7Begin As Date
Dim period7End As Date
Dim period8Begin As Date
Dim period8End As Date
Dim period9Begin As Date
Dim period9End As Date
Dim period10Begin As Date
Dim period10End As Date
Dim period11Begin As Date
Dim period11End As Date
Dim period12Begin As Date
Dim period12End As Date
Dim period13Begin As Date
Dim CheckDate As String
Dim TempStore As String
Dim YearNum As Integer
Dim MonthNum As Integer
Dim DayNum As Integer
Dim FileNum As String
Dim WrongBDate As Boolean

LblDate.Caption = Format(Date, shortdate)
LblTime.Caption = Format(Now, "hh:mm AM/PM")

I = 1
If Dir("C:\Loadx.txt") <> "" Then
   IJ = IJ + 1
   If IJ = 5 Then
        Kill "C:\Loadx.txt"
        X = Shell("c:\NODESYS\WINXNODE.EXE", vbNormalFocus)
        IJ = 0
    End If
End If
If Dir("C:\LoadPC.txt") <> "" Then
   PCanywhere = True
   Kill "C:\LoadPC.txt"
   For I = 1 To 1000
      For J = 1 To 1000
      Next J
      DoEvents
   Next I
   REMOVEXNET = Shell("c:\NODESYS\WINXNODE.EXE -R", vbNormalFocus)
   'AppActivate REMOVEXNET
   SendKeys "{ENTER}", True
   For I = 1 To 1000
      DoEvents
   Next I
   LOADPCAW = Shell("c:\PROGRAM FILES\PCANYWHERE\AWHOST32.EXE", vbMaximizedFocus)
End If
On Error GoTo messagerror
 If Hour(Now) > 6 And Hour(Now) < 13 Then
   If Minute(Now) = 30 Then
      If Dir("C:\Windows\Texas.msg") <> "" Then
      LstMessage.Clear
         Open "C:\Windows\Texas.msg" For Input As #1
            Do While Not EOF(1)
               I = 1
               J = 1
               Line Input #1, Message
               If Trim(Mid(Message, 60, 5)) <> "" Then
                  TempMessage = Mid(Message, 60, 1)
                  Do While Not TempMessage = " "
                     TempMessage = Mid(Message, 60 - I, 1)
                     I = I + 1
                  Loop
                  TempMessage = Mid(Message, 62 - I, 65)
                  LstMessage.AddItem Left(Message, 61 - I)
                  J = 1
                  Do While Not Trim(Mid(TempMessage, 66 - J, 1)) = ""
                     J = J + 1
                     If J = 65 Then
                        Exit Do
                     End If
                  Loop
                  If Trim(TempMessage) <> "" Then
                     TempMessage = LTrim(Left(TempMessage, 66 - J))
                     LstMessage.AddItem TempMessage
                  End If
                  J = (66 - J) + (61 - I)
                  If Mid(Message, J, 1) <> "" Then
                     TempMessage = Trim(Mid(Message, J, 65))
                     LstMessage.AddItem TempMessage
                  End If
               Else
                  LstMessage.AddItem Message
               End If
            Loop
         Close #1
      End If
   End If
End If


If Hour(Now) = 9 And Minute(Now) < 2 Then
   Check = True
   SSDone = False

If Dir("C:\Vawv.txt") <> "" Then
   NC = False
Else
   NC = True
End If
StartDate = #12/31/1998#
TempTestDate = Date
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If

   period1Begin = StartDate
   period2Begin = DateAdd("ww", 4, StartDate)
   period1End = DateAdd("d", -1, period2Begin)
   period3Begin = DateAdd("ww", 4, period2Begin)
   period2End = DateAdd("d", -1, period3Begin)
   period4Begin = DateAdd("ww", 5, period3Begin)
   period3End = DateAdd("d", -1, period4Begin)
   period5Begin = DateAdd("ww", 4, period4Begin)
   period4End = DateAdd("d", -1, period5Begin)
   period6Begin = DateAdd("ww", 4, period5Begin)
   period5End = DateAdd("d", -1, period6Begin)
   period7Begin = DateAdd("ww", 5, period6Begin)
   period6End = DateAdd("d", -1, period7Begin)
   period8Begin = DateAdd("ww", 4, period7Begin)
   period7End = DateAdd("d", -1, period8Begin)
   period9Begin = DateAdd("ww", 4, period8Begin)
   period8End = DateAdd("d", -1, period9Begin)
   period10Begin = DateAdd("ww", 5, period9Begin)
   period9End = DateAdd("d", -1, period10Begin)
   period11Begin = DateAdd("ww", 4, period10Begin)
   period10End = DateAdd("d", -1, period11Begin)
   period12Begin = DateAdd("ww", 4, period11Begin)
   period11End = DateAdd("d", -1, period12Begin)
   period13Begin = DateAdd("ww", 5, period12Begin)
   period12End = DateAdd("d", -1, period13Begin)

Select Case TempTestDate
       Case Is >= period12Begin
                 StartDate = period11Begin
                 EndDate = period11End
                 period = 12
       Case Is >= period11Begin
                 StartDate = period10Begin
                 EndDate = period10End
                 period = 11
       Case Is >= period10Begin
                 StartDate = period9Begin
                 EndDate = period9End
                 period = 10
       Case Is >= period9Begin
                 StartDate = period8Begin
                 EndDate = period8End
                 period = 9
       Case Is >= period8Begin
                 StartDate = period7Begin
                 EndDate = period7End
                 period = 8
       Case Is >= period7Begin
                 StartDate = period6Begin
                 EndDate = period6End
                 period = 7
       Case Is >= period6Begin
                 StartDate = period5Begin
                 EndDate = period5End
                 period = 6
       Case Is >= period5Begin
                 StartDate = period4Begin
                 EndDate = period4End
                 period = 5
       Case Is >= period4Begin
                 StartDate = period3Begin
                 EndDate = period3End
                 period = 4
       Case Is >= period3Begin
                 StartDate = period2Begin
                 EndDate = period2End
                 period = 3
       Case Is >= period2Begin
                 StartDate = period1Begin
                 EndDate = period1End
                 period = 2
       Case Is >= period1Begin
                 StartDate = DateAdd("WW", -52, period12Begin)
                 EndDate = DateAdd("WW", -52, period12End)
                 period = 1
End Select
If period = 1 Then
   CmdSpreadPrevious.Caption = "Period 12"
Else
   CmdSpreadPrevious.Caption = "Period " & period - 1
End If
    CmdSpreadCurrent.Caption = "Period " & period
End If
If period = 12 Then
   If TempTestDate = DateAdd("d", "1", StartDate) Then
      If (Hour(Now) = 11) And (Minute(Now) = 5) And (SSDone = False) Then
         If Dir("C:\pkzip\zip_ss.bat") <> "" Then
            X = Shell("c:\pkzip\zip_ss.bat")
         End If
         SSDone = True
      End If
   End If
Else
   If TempTestDate = StartDate Then
      If (Hour(Now) = 11) And (Minute(Now) = 5) And (SSDone = False) Then
         If Dir("C:\pkzip\zip_ss.bat") <> "" Then
            X = Shell("c:\pkzip\zip_ss.bat")
         End If
         SSDone = True
      End If
   End If
End If

'Jody's Code

If Hour(Now) = 5 And Minute(Now) = 10 Then
    progname = "C:\NODESYS\Winxnode.exe"
    X = Shell(progname, vbNormalFocus)
    DoEvents
    'SetActiveWindow ("RemoteWare Node Error")
    For I = 1 To 10
        For J = 1 To 1000
            DoEvents
        Next J
    Next I
    DoEvents
    SendKeys "{ENTER}", True
End If

If Hour(Now) = 12 And Minute(Now) = 55 Then
    progname = "C:\NODESYS\Winxnode.exe"
    X = Shell(progname, vbNormalFocus)
    DoEvents
    'SetActiveWindow ("RemoteWare Node Error")
    For I = 1 To 10
        For J = 1 To 1000
            DoEvents
        Next J
    Next I
    DoEvents
    SendKeys "{ENTER}", True
End If

If Year(Now) = 2004 And Month(Now) = 4 And Day(Now) = 4 And Hour(Now) = 6 And Minute(Now) < 2 Then
   ChangeSpringTime
End If
If Year(Now) = 2004 And Month(Now) = 4 And Day(Now) = 4 And Hour(Now) = 8 And Minute(Now) < 2 Then
   If Dir("C:\TimeChange.txt") <> "" Then
      Kill "C:\TimeChange.txt"
   End If
End If

If Year(Now) = 2004 And Month(Now) = 10 And Day(Now) = 31 And Hour(Now) = 6 And Minute(Now) < 2 Then
    If Dir("C:\TimeChange.txt") = "" Then
        ChangeFallTime
    End If
End If
If Year(Now) = 2004 And Month(Now) = 10 And Day(Now) = 31 And Hour(Now) = 8 And Minute(Now) < 2 Then
   If Dir("C:\TimeChange.txt") <> "" Then
      Kill "C:\TimeChange.txt"
   End If
End If



Exit Sub
messagerror:
LstMessage.AddItem "Message system is not functioning correctly"
LstMessage.AddItem "Call Help Desk at (800) 773-8983 ext. 1437"
End Sub



