VERSION 5.00
Begin VB.Form FrmPassword 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3195
   ClientLeft      =   1965
   ClientTop       =   2385
   ClientWidth     =   5580
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   5580
   Begin VB.TextBox TxtPassword 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      IMEMode         =   3  'DISABLE
      Left            =   1320
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1080
      Width           =   3135
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "E&xit"
      Height          =   375
      Left            =   3120
      TabIndex        =   2
      Top             =   2280
      Width           =   1695
   End
   Begin VB.CommandButton CmdEnter 
      Caption         =   "&Enter"
      Height          =   375
      Left            =   960
      TabIndex        =   1
      Top             =   2280
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Please enter Password"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   3
      Top             =   360
      Width           =   3135
   End
End
Attribute VB_Name = "FrmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdEnter_Click()
If Trim(LCase(TxtPassword.Text)) = "swipeoff" Then
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 0 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
        Print #1, "cd\EdmWeb"
        Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "swipeon" Then
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 1 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
        Print #1, "cd\EdmWeb"
        Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "vnc" Then
      Y = Shell("C:\Documents and Settings\Administrator\Desktop\vnc-4.0-x86_win32_viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "log" Then
      If Dir("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe") = "" Then
        FileCopy "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.zip-_DisplayLogFileContents.UI.exe", "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe"
      End If
      ChDir ("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer")
      Y = Shell("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "iex" Then
      Y = Shell("C:\Program Files\Internet Explorer\iexplore.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If



If Trim(LCase(TxtPassword.Text)) = "dbe" Then
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "sa" Then
      Y = Shell("cmd.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "exp" Then
      Y = Shell("c:\WINDOWS\explorer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "oft" Then
      Y = Shell("c:\bneapps\oftload.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "runlc" Then
      Y = Shell("c:\IRIS\BIN\RUNLC.BAT", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "dbr" Then
      
        Open "C:\DBR.BAT" For Output As #5    ' Open file for output.
        QUOTE = """"
        Print #5, "CD c:\X_WIN95\X_LANSA\X_DEV\SOURCE"
        Print #5, "C:\X_WIN95\X_LANSA\EXECUTE\x_start.EXE =c:\X_WIN95\X_LANSA\X_DEV\SOURCE\STRTFILE.1"
        Print #5, "EXIT"
        Close 5

      Y = Shell("c:\dbr.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "file" Then
      If Dir("c:\program files\progressive software\psiexporter\out\iris.irs") <> "" Then
            x = MsgBox("IRIS poll file exist!  A LiveConnect session will be started.", vbInformation, "File Exists")
            Y = Shell("c:\IRIS\BIN\REPOLL.BAT", vbNormalFocus)
            End
      Else
            Open "c:\iris\bin\createpoll.BAT" For Output As #1
            Print #1, "rem Created: " + Str(Now())
            Print #1, "Title DO NOT CLOSE THIS WINDOW"
            Print #1, "cd c:\iris\bin"
            If Weekday(Now) = 2 Then
                If Hour(Now) >= 15 Then
                    Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) = 3 Then
                If Hour(Now) < 15 Then
                    Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) <> 2 And Weekday(Now) <> 3 Then
                Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
            End If
            Print #1, "c:\iris\setup\sleep.exe 10000"
            Print #1, "cd c:\Program Files\Progressive Software\psiExporter\bin"
            Print #1, "PSIExporter.exe /yesterday /q"
            Print #1, "cd C:\Program Files\Progressive Software\psiExporter\OUT"
            Print #1, "IF EXIST IRIS.IRS copy IRIS.IRS + *.txt IRIS.IRS"
            Print #1, "IF NOT EXIST IRIS.IRS COPY *.TXT IRIS.IRS"
            Print #1, "del *.txt"
            Print #1, "cd \iris\bin"
            Print #1, "REPOLL.BAT"
            Close #1
            Y = Shell("c:\iris\bin\createpoll.BAT", vbNormalFocus)
            TxtPassword.Text = ""
            FrmPassword.Hide
        End
    Exit Sub

    End If
End If

TxtPassword.Text = ""
FrmPassword.Hide
End

End Sub

Private Sub CmdExit_Click()
TxtPassword.Text = ""
FrmPassword.Hide
End Sub

