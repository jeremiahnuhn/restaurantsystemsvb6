VERSION 5.00
Begin VB.Form Frm3700 
   Caption         =   "3700 POS"
   ClientHeight    =   8100
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7725
   LinkTopic       =   "Form1"
   ScaleHeight     =   8100
   ScaleWidth      =   7725
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdPrint 
      Caption         =   "&Print"
      Height          =   615
      Left            =   5640
      TabIndex        =   8
      Top             =   7440
      Width           =   2535
   End
   Begin VB.CommandButton Cmd3700 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1920
      TabIndex        =   0
      Top             =   7440
      Width           =   2775
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Your Business Date is Incorrect !"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   720
      TabIndex        =   10
      Top             =   960
      Width           =   7455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Warning !!!"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H008080FF&
      Height          =   615
      Left            =   840
      TabIndex        =   9
      Top             =   120
      Width           =   7455
   End
   Begin VB.Label Lbl7 
      Caption         =   "7. Need Assistance? Call Data Services - (252) 937-2800 ext 1767"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   7
      Top             =   6600
      Width           =   7575
   End
   Begin VB.Label Lbl6 
      Caption         =   "6. Staff may begin using Micros system again."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   6
      Top             =   5880
      Width           =   7575
   End
   Begin VB.Label Lbl5 
      Caption         =   "5. Clock all personnel back in. Adjust their time for today to as close as their original clock-in as possible."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   600
      TabIndex        =   5
      Top             =   4920
      Width           =   7575
   End
   Begin VB.Label Lbl4 
      Caption         =   $"Frm3700.frx":0000
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   600
      TabIndex        =   4
      Top             =   3960
      Width           =   7695
   End
   Begin VB.Label Lbl3 
      Caption         =   "3. Open Autosequences && Reports and click on the Emergency End of Day button."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   3
      Top             =   3240
      Width           =   7575
   End
   Begin VB.Label Lbl2 
      Caption         =   "2. Clock all personnel out. Adjust their time for today to as close as zero as possible."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   2
      Top             =   2520
      Width           =   7575
   End
   Begin VB.Label Lbl1 
      Caption         =   "1. Inform staff not to use MICROS system."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   600
      TabIndex        =   1
      Top             =   1800
      Width           =   7575
   End
End
Attribute VB_Name = "Frm3700"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Cmd3700_Click()
Frm3700.Hide
Unload Frm3700
End Sub

Private Sub CmdPrint_Click()
PrintForm
End Sub

Private Sub Form_Load()
Frm3700.Height = Screen.Height
Frm3700.Width = Screen.Width / 1.2
Frm3700.Left = 600
Frm3700.Top = 100
End Sub
