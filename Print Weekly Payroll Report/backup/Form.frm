VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Printing Weekly Payroll Envelope Report"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6945
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   6945
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

PrintYN = "No"
If Weekday(Now) = 2 Then
    PrintYN = "Yes"
    SDATE = Format$(Date - 6, "MM/DD/YYYY")
    EDATE = Format$(Date, "MM/DD/YYYY")
End If
If Hour(Now) < 15 And Weekday(Now) = 3 Then
    PrintYN = "Yes"
        SDATE = Format$(Date - 7, "MM/DD/YYYY")
        EDATE = Format$(Date - 1, "MM/DD/YYYY")
End If
'C:\Iris\Bin\PrintRDL.exe -r="RPY_Detail Hours-BNOE.rdl" -rp="c:\iris\reports" -p="StartDate:4/1/2013-4/7/2013" /landscape
Open "c:\pwr.bat" For Output As #3

If PrintYN = "Yes" Then
        Print #3, "C:\Iris\Bin\PrintRDL.exe -r=" + Chr(34) + "RPY_Detail Hours-BNOE.rdl" + Chr(34) + " -rp=" + Chr(34) + "c:\iris\reports" + Chr(34) + " -p=" + Chr(34) + "StartDate:" + SDATE + "-" + EDATE + Chr(34) + " /Portrait"
        Print #3, "C:\Iris\Bin\PrintRDL.exe -r=" + Chr(34) + "RPY_Time Corrections Log Report.rdl" + Chr(34) + " -rp=" + Chr(34) + "c:\iris\reports" + Chr(34) + " -p=" + Chr(34) + "StartDate:" + SDATE + "-" + EDATE + Chr(34); " /landscape"
End If
        Print #3, "Exit"
Close #3
x = Shell("c:\pwr.bat", vbHide)
End
End Sub
