VERSION 5.00
Begin VB.Form FrmStatRpt 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Communication Error Report"
   ClientHeight    =   5940
   ClientLeft      =   6495
   ClientTop       =   1530
   ClientWidth     =   9555
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5940
   ScaleWidth      =   9555
   Begin VB.CommandButton CmdCancel 
      Caption         =   "&Cancel"
      Height          =   495
      Left            =   6720
      TabIndex        =   4
      Top             =   4920
      Width           =   1935
   End
   Begin VB.CommandButton CmdPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   1080
      TabIndex        =   3
      Top             =   4920
      Width           =   1935
   End
   Begin VB.ListBox LstMP 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2580
      Left            =   480
      TabIndex        =   2
      Top             =   1560
      Width           =   8415
   End
   Begin VB.Label LblDate 
      Height          =   495
      Left            =   5040
      TabIndex        =   1
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label LblCommErr 
      Height          =   495
      Left            =   720
      TabIndex        =   0
      Top             =   240
      Width           =   3975
   End
End
Attribute VB_Name = "FrmStatRpt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdCancel_Click()
FrmStatRpt.Hide
Unload FrmStatRpt
Repoll.Show
End Sub

Private Sub CmdPrint_Click()
Dim today As Date
Dim PageNum As Integer
Dim V As Integer
Dim W As Integer
Dim X As Integer
Dim Y As Integer
Dim Z As Integer
W = 1
Z = 5
V = 0
PageNum = 1
Y = 2200
For I = 2 To LstMP.ListCount - 1
   Printer.FontName = "COURIER NEW"
   Printer.FontBold = True
   Printer.FontSize = 14
   today = Format(Now, "ddddd")
   Printer.CurrentX = 3400
   Printer.CurrentY = 500
   Printer.Print "BODDIE-NOELL ENTERPRISES, INC."
   Printer.FontSize = 10
   Printer.Print "                  Reminder: All concepts are Priority 1 if Missed on Tuesday."
   Printer.Print "                       "
   Printer.FontSize = 12
   Printer.CurrentX = 2400
   Printer.CurrentY = 1400
   Printer.Print "Communications Errors as of " + Str(Now)
   Printer.Print
   Printer.FontSize = 10: Printer.FontBold = False
   Printer.CurrentX = 1200
   Printer.CurrentY = 1800
   Printer.FontUnderline = True
   Printer.Print "Number"
   Printer.CurrentX = 2900
   Printer.CurrentY = 1800
   Printer.Print "Location"
   Printer.CurrentX = 5700
   Printer.CurrentY = 1800
   Printer.Print "Speed Dial #"
   Printer.CurrentX = 7600
   Printer.CurrentY = 1800
   Printer.Print "Communication"
   Printer.CurrentX = 9700
   Printer.CurrentY = 1800
   Printer.Print "Error Type"
   Printer.FontUnderline = False
   Printer.FontSize = 10
   Printer.CurrentX = 1000
   Printer.CurrentY = Y
   Printer.FontBold = True
   Printer.Print Left(LstMP.List(I), 6)
   Printer.CurrentX = 2900
   Printer.CurrentY = Y
   Printer.Print Mid(LstMP.List(I), 8, 20)
   Printer.CurrentX = 5200
   Printer.CurrentY = Y
   Printer.Print Mid(LstMP.List(I), 30, 10)
   Printer.CurrentX = 7600
   Printer.CurrentY = Y
   Printer.Print Mid(LstMP.List(I), 40, 12)
   Printer.CurrentX = 9400
   Printer.CurrentY = Y
   Printer.Print Mid(LstMP.List(I), 52, 15)
   Printer.FontBold = False
   Printer.CurrentX = 1200
   Printer.CurrentY = Y + 800
   Printer.FontSize = 8
   Printer.Print "Manager on Duty"
   Printer.Line (2800, Y + 950)-(4800, Y + 950)
   Printer.CurrentX = 5000
   Printer.CurrentY = Y + 800
   Printer.Print "Title"
   Printer.Line (5600, Y + 950)-(6900, Y + 950)
   Printer.CurrentX = 7100
   Printer.CurrentY = Y + 800
   Printer.Print "Days Missed"
   Printer.Line (8300, Y + 950)-(9200, Y + 950)
   Printer.CurrentX = 9300
   Printer.CurrentY = Y + 800
   Printer.Print " "
   Printer.CurrentX = 1200
   Printer.CurrentY = Y + 1400
   Printer.Print "Solution"
   Printer.Line (2100, Y + 1550)-(8700, Y + 1550)
   Printer.CurrentX = 8800
   Printer.CurrentY = Y + 1400
   Printer.Print "Missed  Y  N"
   Y = Y + 1900
    If I > Z Then
       Printer.CurrentX = 10000
       Printer.CurrentY = 14500
       Printer.Print "Page " & PageNum
       PageNum = PageNum + 1
       Printer.NewPage
       Z = Z + 5
       Y = 2200
    End If
Next I
Printer.CurrentX = 1200
Printer.CurrentY = 13500
Printer.Print "Help Desk Operator"
Printer.Line (3200, 13650)-(7600, 13650)
Printer.CurrentX = 7700
Printer.CurrentY = 13500
Printer.Print "Time Completed"
Printer.Line (9200, 13650)-(10200, 13650)
Printer.CurrentX = 10000
Printer.CurrentY = 14500
Printer.Print "Page " & PageNum
Printer.EndDoc
FrmStatRpt.Hide
Unload FrmStatRpt
Repoll.Show
End Sub

Private Sub Form_Load()
FrmStatRpt.Top = Screen.Height - (Screen.Height / 1.1)
FrmStatRpt.Left = Screen.Width - (Screen.Width - (Screen.Width / 10))
End Sub


