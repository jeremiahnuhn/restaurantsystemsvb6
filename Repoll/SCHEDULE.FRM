VERSION 4.00
Begin VB.Form Sched 
   Appearance      =   0  'Flat
   BackColor       =   &H00800080&
   Caption         =   "Schedule"
   ClientHeight    =   3210
   ClientLeft      =   3435
   ClientTop       =   2310
   ClientWidth     =   2550
   ControlBox      =   0   'False
   BeginProperty Font 
      name            =   "MS Sans Serif"
      charset         =   1
      weight          =   700
      size            =   8.25
      underline       =   0   'False
      italic          =   0   'False
      strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Height          =   3615
   Left            =   3375
   LinkTopic       =   "Form1"
   ScaleHeight     =   3210
   ScaleWidth      =   2550
   Top             =   1965
   Width           =   2670
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Close"
      Height          =   375
      Left            =   720
      TabIndex        =   13
      Top             =   2760
      Width           =   1095
   End
   Begin VB.HScrollBar MM_Scroll 
      Height          =   255
      Left            =   615
      Max             =   12
      TabIndex        =   3
      Top             =   2160
      Width           =   495
   End
   Begin VB.HScrollBar DD_Scroll 
      Height          =   255
      Left            =   1215
      Max             =   31
      TabIndex        =   2
      Top             =   2160
      Width           =   510
   End
   Begin VB.HScrollBar HH_Scroll 
      Height          =   255
      Left            =   600
      Max             =   24
      TabIndex        =   1
      Top             =   840
      Width           =   495
   End
   Begin VB.HScrollBar Min_Scroll 
      Height          =   255
      Left            =   1230
      Max             =   59
      TabIndex        =   0
      Top             =   840
      Width           =   525
   End
   Begin VB.Label MM_Lab 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "00"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   735
      TabIndex        =   12
      Top             =   1920
      Width           =   255
   End
   Begin VB.Label DD_Lab 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "00"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1335
      TabIndex        =   11
      Top             =   1920
      Width           =   255
   End
   Begin VB.Label YY_Lab 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "00"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1920
      TabIndex        =   10
      Top             =   1920
      Width           =   255
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   " Month    Day    Year"
      ForeColor       =   &H0000FFFF&
      Height          =   255
      Left            =   495
      TabIndex        =   9
      Top             =   1680
      Width           =   1815
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "  Start Polling On  "
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   -1  'True
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   8
      Top             =   1440
      Width           =   1560
   End
   Begin VB.Label HH_Lab 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "00"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   720
      TabIndex        =   7
      Top             =   600
      Width           =   255
   End
   Begin VB.Label Min_Lab 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      Caption         =   "00"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1320
      TabIndex        =   6
      Top             =   600
      Width           =   255
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Hour  Minute"
      ForeColor       =   &H0000FFFF&
      Height          =   255
      Left            =   660
      TabIndex        =   5
      Top             =   375
      Width           =   1215
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "  Start Polling At  "
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   -1  'True
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "Sched"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub Command1_Click()
   RHH = HH_Lab.Caption
   RMI = Min_Lab.Caption
   RMM = MM_Lab.Caption
   RDD = DD_Lab.Caption
   RYY = YY_Lab.Caption
   Unload Sched
End Sub

Private Sub DD_Scroll_Change()
   Txt = DD_Scroll.Value
   DD_Lab = Format(Txt, "00")

End Sub

Private Sub HH_Scroll_Change()
   Txt = HH_Scroll.Value
   HH_Lab = Format(Txt, "00")
  End Sub

Private Sub Min_Scroll_Change()
   Txt = Min_Scroll.Value
   Min_Lab = Format(Txt, "00")
End Sub

Private Sub MM_Scroll_Change()
   Txt = MM_Scroll.Value
   MM_Lab = Format(Txt, "00")
   If MM_Lab = "02" Then
     DD_Scroll.Max = 29
   ElseIf MM_Lab = "00" Then
     DD_Scroll.Max = 0
   Else
     DD_Scroll.Max = 31
   End If
   If MM_Lab = "00" Then
     YY_Lab = "00"
     DD_Scroll.Min = 0
   Else
     YY_Lab = Format(Now, "yy")
     DD_Scroll.Min = 1
   End If

End Sub

