VERSION 4.00
Begin VB.Form ExpRepol 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   Caption         =   "Missing Restaurants Detail View"
   ClientHeight    =   4335
   ClientLeft      =   195
   ClientTop       =   1650
   ClientWidth     =   9150
   ControlBox      =   0   'False
   BeginProperty Font 
      name            =   "Times New Roman"
      charset         =   1
      weight          =   700
      size            =   18
      underline       =   0   'False
      italic          =   0   'False
      strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Height          =   4740
   Left            =   135
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   9150
   Top             =   1305
   Width           =   9270
   Begin VB.CommandButton CancelCmd 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Cancel"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4860
      TabIndex        =   0
      Top             =   3720
      Width           =   1575
   End
   Begin VB.CommandButton PrintCmd 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Print List"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2940
      TabIndex        =   2
      Top             =   3720
      Width           =   1575
   End
   Begin VB.ListBox MP 
      Appearance      =   0  'Flat
      BeginProperty Font 
         name            =   "Fixedsys"
         charset         =   1
         weight          =   400
         size            =   9
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   3180
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   8610
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   8535
   End
End
Attribute VB_Name = "ExpRepol"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub CancelCmd_Click()
  Unload Me
End Sub

Private Sub Command1_Click()
  PrintForm
End Sub

Private Sub PrintCmd_Click()
    Printer.FontSize = 20
    Printer.FontName = "COURIER NEW"
    Printer.FontBold = True
    Printer.Print
    Printer.Print
    X = 0
    For I = 0 To (MP.ListCount - 1)
    X = X + 1
    If I = 0 Then
        Printer.FontSize = 14
    Else
        Printer.FontSize = 12
    End If
    Printer.FontBold = True
    Printer.Print MP.List(I)
    If I > 1 Then
        Printer.Print
        Printer.FontSize = 8
        Printer.FontBold = False
        Printer.Print
        Printer.Print
        Printer.Print "Manager on Duty  ____________________________________  Title  ____________________  Days Missed  ______  PCNs  Y  N"
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print "Solution ____________________________________________________________________________________________  Missed  Y  N  "
        Printer.Print
        Printer.Print
    Else
        Printer.Print
        X = X - 1
    End If
  If X = 6 Then
    X = 0
    Printer.NewPage
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
  End If
  Next I
  Printer.Print
  Printer.Print
  Printer.FontSize = 8
  Printer.Print
  Printer.Print "Help Desk Operator _________________________________  Time Completed ___________ "
  Printer.EndDoc
  Unload Me
End Sub

