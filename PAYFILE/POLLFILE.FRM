VERSION 2.00
Begin Form Form1 
   BackColor       =   &H00FF0000&
   BorderStyle     =   3  'Fixed Double
   Caption         =   " "
   ClientHeight    =   1290
   ClientLeft      =   2040
   ClientTop       =   1620
   ClientWidth     =   5670
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   FontBold        =   -1  'True
   FontItalic      =   0   'False
   FontName        =   "MS Sans Serif"
   FontSize        =   18
   FontStrikethru  =   0   'False
   FontUnderline   =   0   'False
   Height          =   1695
   Icon            =   POLLFILE.FRX:0000
   Left            =   1980
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1290
   ScaleWidth      =   5670
   Top             =   1275
   Width           =   5790
   Begin Label Label1 
      BackColor       =   &H00FF0000&
      Caption         =   "Please wait ..."
      FontBold        =   -1  'True
      FontItalic      =   0   'False
      FontName        =   "MS Serif"
      FontSize        =   24
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   1320
      TabIndex        =   0
      Top             =   240
      Width           =   3735
   End
End

Sub Form_Paint ()
' Find month and day to get file name
' fday = file day
' fmonth = file month
'
' VALUE RPT ARRAY 2dIM ARRAY COL1 = COUNT  COL2 = AMOUNT
'  1 = NET SALES
'  2 = TAX 1
'  3 = TAX 2

MAXARRAY = 37
FDAY = Format(Now, "dd")
FMONTH = Format(Now, "mm")
FYEAR = Format(Now, "yy")
Fname = "C:\AMWS\POLLING\" + FMONTH + FDAY + "DCR.POL"
ReDim rpt(MAXARRAY, 3)
Dim Storedate As String * 8
' OPEN DAILY CASH REPORT CSV
'
Open Fname For Input As #1
    Do While Not EOF(1) ' Check for end of file.
	Input #1, cat
	' CASE FOR FIRST VALUE OR CATEGORY
	Select Case cat
	Case "DATE"
	    Storedate = Input(8, #1)
	    Storemm = Mid$(Storedate, 1, 2)
	    Storedd = Mid$(Storedate, 4, 2)
	    Storeyy = Mid$(Storedate, 7, 2)
	    Call SREAD1(JUNK)
	Case "STORE"
	    Call SREAD1(STORENUM)
	Case "SALES"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)
	Case "TOTAL DEPARTMENT"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)
	Case "TENDER DEPARTMENT"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)
	Case "COUPON DEPARTMENT"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)
	Case "DISCOUNT DEPARTMENT"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)
	Case "ITEM DEPARTMENT"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)
	Case "CONTROL TOTALS"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)
	Case "FINANCIAL"
	    Call SREAD1(DESC)
	    Call NREAD2(V1, V2)

	Case Else
	    Line Input #1, JUNK
	End Select
	
	' CASE FOR SECOND VALUE OR DESCRIPTION
	
	Select Case DESC
	    Case "NET SALES"
		rpt(1, 1) = V1: rpt(1, 2) = V2
	    Case "COUPONS"
		rpt(2, 1) = V1: rpt(2, 2) = V2
	    Case "GROSS"
		rpt(3, 1) = V1: rpt(3, 2) = V2
	    Case "TAX"
		rpt(4, 1) = V1: rpt(4, 2) = V2
	End Select

	Select Case DESC
	    Case "EAT-IN"
		rpt(5, 1) = V1: rpt(5, 2) = V2
	    Case "TAKE-OUT"
		rpt(6, 1) = V1: rpt(6, 2) = V2
	    Case "DRIVE-THRU"
		rpt(7, 1) = V1: rpt(7, 2) = V2
	End Select

	Select Case DESC
	    Case "CASH"
		rpt(8, 1) = V1: rpt(8, 2) = V2
	    Case "CREDIT CARDS"
		rpt(9, 1) = V1: rpt(9, 2) = V2
	    Case "GIFT CERTIFICATES"
		rpt(10, 1) = V1: rpt(10, 2) = V2
	    Case "TRAVELERS CHECKS"
		rpt(11, 1) = V1: rpt(11, 2) = V2
	 End Select

	Select Case DESC
	    Case "BURGER OPEN OFFERS"
		rpt(12, 1) = V1: rpt(12, 2) = V2
	    Case "BURGER COUPONS"
		rpt(13, 1) = V1: rpt(13, 2) = V2
	    Case "SANDWICH OPEN OFFER"
		rpt(14, 1) = V1: rpt(14, 2) = V2
	    Case "SANDWICH COUPONS"
		rpt(15, 1) = V1: rpt(15, 2) = V2
	    Case "FRESH CHICKEN OFFERS"
		rpt(16, 1) = V1: rpt(16, 2) = V2
	    Case "FRESH CHICKEN COUPNS"
		rpt(17, 1) = V1: rpt(17, 2) = V2
	    Case "BREAKFST OPEN OFFERS"
		rpt(18, 1) = V1: rpt(18, 2) = V2
	    Case "BREAKFAST COUPONS"
		rpt(19, 1) = V1: rpt(19, 2) = V2
	    Case "CENTS OFF COUPONS"
		rpt(37, 1) = V1: rpt(37, 2) = V2
	 End Select

	 Select Case DESC
	    Case "CUSTOMER DISCOUNTS"
		rpt(20, 1) = V1: rpt(20, 2) = V2
	    Case "EMPLOYEE DISCOUNTS"
		rpt(21, 1) = V1: rpt(21, 2) = V2
	 End Select

	 Select Case DESC
	    Case "CHICKEN BULK"
		rpt(22, 1) = V1: rpt(22, 2) = V2
	    Case "PREMIUMS"
		rpt(33, 1) = V1: rpt(33, 2) = V2
	    Case "SR. CITIZEN BEVS"
		rpt(34, 1) = V1: rpt(34, 2) = V2
	 End Select
	 
	 Select Case DESC
	    Case "ALLOWANCES"
		rpt(23, 1) = V1: rpt(23, 2) = V2
	    Case "DISCOUNTS"
		rpt(24, 1) = V1: rpt(24, 2) = V2
	    Case "CUSTOMER REFUND"
		rpt(25, 1) = V1: rpt(25, 2) = V2
	    Case "MANAGER VOIDS"
		rpt(26, 1) = V1: rpt(26, 2) = V2
	    Case "CANCEL ANY/LAST"
		rpt(35, 1) = V1: rpt(35, 2) = V2
	    Case "CANCEL ORDER"
		rpt(36, 1) = V1: rpt(36, 2) = V2
	    Case "TRAINING MODE"
		rpt(27, 1) = V1: rpt(27, 2) = V2
	 End Select

	 Select Case DESC
	    Case "PAIDINS"
		rpt(28, 1) = V1: rpt(28, 2) = V2
	    Case "PAIDOUTS"
		rpt(29, 1) = V1: rpt(29, 2) = V2
	    Case "ACTUAL DEPOSITS"
		rpt(30, 1) = V1: rpt(30, 2) = V2
	    Case "= THEO DEPOSITS"
		rpt(31, 1) = V1: rpt(31, 2) = V2
	    'Case "OVER/SHORT"
	     '   RPT(32, 1) = V1: RPT(32, 2) = V2
	 End Select
    Loop
    Close #1    ' Close file.

' ======== CALUCLATED FIELDS ===================================

   COUPNUM = rpt(13, 1) + rpt(15, 1) + rpt(17, 1) + rpt(19, 1)
   COUPDOL = Format$(rpt(13, 2) + rpt(15, 2) + rpt(17, 2) + rpt(19, 2), "$#,##0.00")
   COUPAMT = rpt(13, 2) + rpt(15, 2) + rpt(17, 2) + rpt(19, 2)
   OPENNUM = rpt(12, 1) + rpt(14, 1) + rpt(16, 1) + rpt(18, 1)
   OPENDOL = Format$(rpt(12, 2) + rpt(14, 2) + rpt(16, 2) + rpt(18, 2), "$#,##0.00")
   OPENAMT = rpt(12, 2) + rpt(14, 2) + rpt(16, 2) + rpt(18, 2)
   CLRNUM = rpt(35, 1) + rpt(36, 1)
   CLRDOL = Format$(rpt(35, 2) + rpt(36, 2), "$#,##0.00")
   CLRAMT = rpt(35, 2) + rpt(36, 2)
   PAIDOUTNUM = rpt(29, 1) + rpt(25, 1)
   PAIDOUTDOL = Format$(rpt(29, 2) + rpt(25, 2), "$#,##0.00")
   PAIDOUTAMT = rpt(29, 2) + rpt(25, 2)
   CASHOS = Format$(rpt(30, 2) - rpt(31, 2), "$#,##0.00;($#,##0.00)")
   CASHOSAMT = rpt(30, 2) - rpt(31, 2)
' FORMAT ARRAY
For x = 1 To MAXARRAY
   
	TEMP = rpt(x, 2)
	rpt(x, 3) = TEMP
	rpt(x, 2) = Format$(TEMP, "$#,##0.00;($#,##0.00)")
    
Next x
    
    Select Case STORENUM
	    Case "2401"
		CLERK = "MARY LEE DENTON"
		RESTNAME = "SOUTH HILL"
	    Case "2989"
		CLERK = "KIM WALL"
		RESTNAME = "FLOYD"
	    Case "1181"
		CLERK = "WENDY ROSE"
		RESTNAME = "THOMASVILLE"
	    Case "1643"
		CLERK = "DEBBIE DICKENS"
		RESTNAME = "JACKSONVILLE"
	    Case "3093"
		CLERK = "BRENDA VIVERETTE"
		RESTNAME = "TIMMONSVILLE, SC"
	    Case "1644"
		CLERK = "DEBBIE DICKENS"
		RESTNAME = "SURF CITY"
	    Case "1041"
		CLERK = "LISA BAKER"
		RESTNAME = "KANNAPOLIS"
    End Select

' ======== REPORT TO ACCOUNTING SPECIALIST =====================
    Fname = "C:\AMWS\POLLING\TODAY.POL"
    MName = "C:\AMWS\POLLING\TODAY.CPL"
    Open Fname For Output As 1    ' Open file.
    Open MName For Output As 3 'Open Mainframe File
    Dim DX As String * 14
    Dim KEY As String * 12
    Dim MMDDYY As String * 6
    Dim FID As String * 2
    sq = 0
    FID = "SA"
    MMDDYY = Storeyy + Storemm + Storedd
    KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
    Print #1, "      *******   PLEASE DELIVER TO "; CLERK; "   *******"
    Print #1,
    Print #1,
    Print #1, "              BNE-SALES FROM COMPRIS RESTAURANT"
    Print #1,
    Print #1, "      UNIT:  "; STORENUM; "  "; RESTNAME
    Print #1, "      MONTH:  "; FMONTH; " DAY: "; FDAY; " YEAR: "; FYEAR
    Print #1, "      ========================================"
    Print #1, "      COUNT", "DESCRIPTION", "AMOUNT"
    Print #1, "          ", "NET SALES", rpt(1, 2)
    Call MWRITE(KEY, 0, "NET SALES", rpt(1, 3))
    Print #1, "          ", "TAX 1    ", rpt(4, 2)
    Call MWRITE(KEY, 0, "TAX 1", rpt(4, 3))
    Print #1, "          ", "TAX 2                     "
    Call MWRITE(KEY, 0, "TAX 2", 0)
    Print #1, "     "; COUPNUM, "COUPONS  ", COUPDOL
    Call MWRITE(KEY, COUPNUM, "COUPONS", COUPAMT)
    Print #1, "     "; OPENNUM, "OPEN PROMO", OPENDOL
    Call MWRITE(KEY, OPENNUM, "OPEN PROMO", OPENAMT)
    Print #1, "          ", "DISCOUNT A"
    Call MWRITE(KEY, 0, "DISCOUNT A", 0)
    Print #1, "     "; rpt(20, 1), "DISCOUNT B", rpt(20, 2)
    Call MWRITE(KEY, rpt(20, 1), "DISCOUNT B", rpt(20, 3))
    Print #1, "          ", "DISCOUNT C"
    Call MWRITE(KEY, 0, "DISCOUNT C", 0)
    Print #1, "     "; rpt(21, 1), "EMP DISC A", rpt(21, 2)
    Call MWRITE(KEY, rpt(21, 1), "EMP DISC A", rpt(21, 3))
    Print #1, "          ", "MGR DISC"
    Call MWRITE(KEY, 0, "MGR DISC", 0)
    Print #1, "          ", "EMP CHARGE"
    Call MWRITE(KEY, 0, "EMP CHARGE", 0)
    Print #1, "     "; rpt(26, 1), "OVERRINGS", rpt(26, 2)
    Call MWRITE(KEY, rpt(26, 1), "OVERRINGS", rpt(26, 3))
    Print #1, "     "; CLRNUM, "CLEAR SALES", CLRDOL
    Call MWRITE(KEY, CLRNUM, "CLEAR SALES", CLRAMT)
    Print #1, "     "; rpt(5, 1), "HERE SALES", rpt(5, 2)
    Call MWRITE(KEY, rpt(5, 1), "HERE SALES", rpt(5, 3))
    Print #1, "     "; rpt(6, 1), "TOGO SALES", rpt(6, 2)
    Call MWRITE(KEY, rpt(6, 1), "TOGO SALES", rpt(6, 3))
    Print #1, "     "; rpt(7, 1), "DR TH SALES", rpt(7, 2)
    Call MWRITE(KEY, rpt(7, 1), "DR TH SALES", rpt(7, 3))
    Print #1, "     "; rpt(28, 1), "PAID INS", rpt(28, 2)
    Call MWRITE(KEY, rpt(28, 1), "PAID INS", rpt(28, 3))
    Print #1, "     "; PAIDOUTNUM, "PAID OUTS", PAIDOUTDOL; "       GUEST REFUND "; rpt(25, 2)
    Call MWRITE(KEY, PAIDOUTNUM, "PAID OUTS", PAIDOUTAMT)
    Call MWRITE(KEY, 0, "GUEST REFUND", rpt(25, 3))
    Print #1, "     "; "    ", "DEPOSITS", rpt(30, 2)
    Call MWRITE(KEY, 0, "DEPOSITS", rpt(30, 3))
'    Print #1, "    *"; RPT(25, 1), "CUST REFUND", RPT(25, 2)
    Print #1, "     "; "    ", "CASH O/S   ", CASHOS
    Call MWRITE(KEY, 0, "CASH O/S", CASHOSAMT)
    Print #1, "     "; rpt(33, 1), "PREMIUM A", rpt(33, 2)
    Call MWRITE(KEY, rpt(33, 1), "PREMIUM A", rpt(33, 3))
    Print #1, "     "; "    ", "PREMIUM B"
    Call MWRITE(KEY, 0, "PREMIUM B", 0)
    Print #1, "     "; "    ", "FREE ITEMS"
    Call MWRITE(KEY, 0, "FREE ITEMS", 0)
    Print #1, "     "; "    ", "10% ITEMS"
    Call MWRITE(KEY, 0, "10% ITEMS", 0)
    Print #1, "     "; "    ", "50% ITEMS"
    Call MWRITE(KEY, 0, "50% ITEMS", 0)
    Print #1, "     "; rpt(37, 1), "CENTS OFF", rpt(37, 2)
    Call MWRITE(KEY, rpt(37, 1), "CENTS OFF", rpt(37, 3))
    Print #1, "     "; rpt(34, 1), "SR CITIZENS", rpt(34, 2)
    Call MWRITE(KEY, rpt(34, 1), "SR CITIZENS", rpt(34, 3))
    Print #1, "     "; rpt(9, 1), "CRED CARD", rpt(9, 2)
    Call MWRITE(KEY, rpt(9, 1), "CRED CARD", rpt(9, 3))
    Print #1, "     "; rpt(8, 1), "TOTAL CASH", rpt(8, 2)
    Call MWRITE(KEY, rpt(8, 1), "TOTAL CASH", rpt(8, 3))
    Print #1, "     "; rpt(22, 1), "BULK CHICKEN", rpt(22, 2)
    Call MWRITE(KEY, rpt(22, 1), "BULK CHICKEN", rpt(22, 3))
    Print #1, "     "; "    ", "PRESET Y"
    Call MWRITE(KEY, 0, "PRESET Y", 0)
    Print #1, "     "; "    ", "PRESET N"
    Call MWRITE(KEY, 0, "PRESET N", 0)
    



Fname = "C:\AMWS\POLLING\" + FMONTH + FDAY + "DRR.POL"
ReDim RDR(6, 3)

' OPEN DAILY CASH REPORT CSV
'

Open Fname For Input As #2
    Do While Not EOF(2) ' Check for end of file.
	Input #2, cat
	' CASE FOR FIRST VALUE OR CATEGORY
	Select Case cat
	    Case "LATE NIGHT SALES"
		Call NREAD3(V1, V2, V3)
		RDR(1, 1) = V1: RDR(1, 2) = V2
	    Case "REGULAR BREAKFAST"
		Call NREAD3(V1, V2, V3)
		RDR(2, 1) = V1: RDR(2, 2) = V2
	    Case "LUNCH"
		Call NREAD3(V1, V2, V3)
		RDR(3, 1) = V1: RDR(3, 2) = V2
	    Case "LATE AFTERNOON"
		Call NREAD3(V1, V2, V3)
		RDR(4, 1) = V1: RDR(4, 2) = V2
	    Case "DINNER"
		Call NREAD3(V1, V2, V3)
		RDR(5, 1) = V1: RDR(5, 2) = V2
	    Case "LATE DINNER"
		Call NREAD3(V1, V2, V3)
		RDR(6, 1) = V1: RDR(6, 2) = V2
	    End Select
       Loop
For x = 1 To 6
   
	TEMP = RDR(x, 2)
	RDR(x, 3) = TEMP
	RDR(x, 2) = Format$(TEMP, "$#,##0.00;($#,##0.00)")
    
Next x
	

    Print #1, "     "; RDR(1, 1), "00 - 05 SALES", RDR(1, 2)
    Call MWRITE(KEY, RDR(1, 1), "00 - 05 SALES", RDR(1, 3))
    Print #1, "     "; RDR(2, 1), "05 - 11 SALES", RDR(2, 2)
    Call MWRITE(KEY, RDR(2, 1), "05 - 11 SALES", RDR(2, 3))
    Print #1, "     "; RDR(3, 1), "11 - 15 SALES", RDR(3, 2)
    Call MWRITE(KEY, RDR(3, 1), "11 - 15 SALES", RDR(3, 3))
    Print #1, "     "; RDR(4, 1), "15 - 17 SALES", RDR(4, 2)
    Call MWRITE(KEY, RDR(4, 1), "15 - 17 SALES", RDR(4, 3))
    Print #1, "     "; RDR(5, 1), "17 - 20 SALES", RDR(5, 2)
    Call MWRITE(KEY, RDR(5, 1), "17 - 20 SALES", RDR(5, 3))
    Print #1, "     "; RDR(6, 1), "20 - 00 SALES", RDR(6, 2)
    Call MWRITE(KEY, RDR(6, 1), "20 - 00 SALES", RDR(6, 3))
    Close #1 'Close Sales Report
    Close #2 'Close DRR File
' ************** payroll ****************
Fname = "C:\AMWS\POLLING\" + FMONTH + FDAY + "DSUM.POL"

' OPEN DAILY CASH REPORT CSV
'
Dim LNAME As String
Dim FIRST As String
Dim SSNUM As String
Dim REGHRS As String
Dim OTHRS As String
ReDim rpt(150, 5)
Open Fname For Input As #1 'open Compris Pay File
    x = 0
    Do While Not EOF(1) ' Check for end of file.
	Input #1, cat
	' CASE FOR FIRST VALUE OR CATEGORY
	Select Case cat
	Case "DATE"
	    Storedate = Input(8, #1)
	    Storemm = Mid$(Storedate, 1, 2)
	    Storedd = Mid$(Storedate, 4, 2)
	    Storeyy = Mid$(Storedate, 7, 2)
	    Call SREAD1(JUNK)
	Case "STORE"
	    Call SREAD1(STORENUM)
	Case "SUMMARY"
	   x = x + 1
	   Input #1, LNAME, FIRST, SSNUM, REGHRS, OTHRS, JUNK, JUNK, JUNK, JUNK
	   rpt(x, 1) = LNAME
	   rpt(x, 2) = FIRST
	   rpt(x, 3) = SSNUM
	   rpt(x, 4) = REGHRS
	   rpt(x, 5) = OTHRS
	End Select
    Loop 'End While

  Close #1
' ======== REPORT TO ACCOUNTING SPECIALIST =====================
    Fname = "C:\AMWS\POLLING\PAY.POL"
    MName = "C:\NEWPOS\CAT04.DIF"
    Open Fname For Output As 2 ' Open Payroll Report file.
    Open MName For Output As 5 ' Open ASCII for forms File
    sq = 0
    FID = "DT"
    MMDDYY = Storeyy + Storemm + Storedd
    KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
   ' MMDDYY = FYEAR + FMONTH + FDAY
    Print #2,
    Print #2,
    Print #2, "              BNE-PAYROLL FROM COMPRIS RESTAURANT"
    Print #2,
    Print #2, "      UNIT:        "; STORENUM; " "; RESTNAME
    Print #2, "      WEEK ENDING:  "; Now, , "PAGE:  1"
    Print #2, "      ============================================================"
    Print #2, "      SOC SEC #     REG HRS", "OT HRS", "EMPLOYEE"
    Print #2,
    PAGENUM = 2
    LINECOUNT = 0
    For c = 1 To x
      LINECOUNT = 1 + LINECOUNT
      FULLNAME = Trim(rpt(c, 2)) + " " + rpt(c, 1)
      Print #2, "      "; rpt(c, 3); "   "; rpt(c, 4), rpt(c, 5), FULLNAME
      Print #5, Format$(Mid$(FULLNAME, 1, 11), "!@@@@@@@@@@@"); Mid$(rpt(c, 3), 1, 3); Mid$(rpt(c, 3), 5, 2); Mid$(rpt(c, 3), 8, 4)
      If LINECOUNT > 50 Then

	Print #2, Chr(12)
	Print #2,
	Print #2,
	Print #2, "              BNE-PAYROLL FROM COMPRIS RESTAURANT"
	Print #2,
	Print #2, "      UNIT:  "; STORENUM; "  "; RESTNAME
	Print #2, "      WEEK ENDING:  "; Now, , "PAGE: "; PAGENUM
	Print #2, "      ============================================================"
	Print #2, "      SOC SEC #     REG HRS", "OT HRS", "EMPLOYEE"
	Print #2,
	LINECOUNT = 0
	PAGENUM = 1 + PAGENUM
      End If

    Next c
    'OutPut MainFrame File
    For c = 1 To x
      S = Mid$(rpt(c, 3), 1, 3) & Mid$(rpt(c, 3), 5, 2) & Mid$(rpt(c, 3), 8, 4)
      Call MWRITEP(KEY, S, rpt(c, 4), rpt(c, 5))
    Next c
 ' ************ end payroll ******************
    Close #1 'Close Daily Time Records File
    Close #5 'ASCII Selection List Forms
 ' Do Weekly Time Records!
   If Weekday(Now) = 5 Then
      Fname = "C:\AMWS\POLLING\" + FMONTH + FDAY + "ESUM.POL"
      ' Process Weekly Time Reocrd Files
      ReDim rpt(150, 5)
      Close
      Open Fname For Input As #1 'open Compris Pay File
      MName = "C:\AMWS\POLLING\TODAY.CPL"
      Open MName For Append As 3 'Open Mainframe File
      x = 0
      Do While Not EOF(1) ' Check for end of file.
	Input #1, cat
	' CASE FOR FIRST VALUE OR CATEGORY
	Select Case cat
	Case "DATE"
	    Storedate = Input(8, #1)
	    Storemm = Mid$(Storedate, 1, 2)
	    Storedd = Mid$(Storedate, 4, 2)
	    Storeyy = Mid$(Storedate, 7, 2)
	    Call SREAD1(JUNK)
	Case "STORE"
	    Call SREAD1(STORENUM)
	Case "SUMMARY"
	   x = x + 1
	   Input #1, LNAME, FIRST, SSNUM, REGHRS, OTHRS, JUNK, JUNK, JUNK, JUNK
	   rpt(x, 1) = LNAME
	   rpt(x, 2) = FIRST
	   rpt(x, 3) = SSNUM
	   rpt(x, 4) = REGHRS
	   rpt(x, 5) = OTHRS
	End Select
       Loop 'End While
     Close #1
     sq = 0
     FID = "WT"
     MMDDYY = Storeyy + Storemm + Storedd
     KEY = FID + LTrim$(Str$(STORENUM) + MMDDYY)
    'OutPut MainFrame File
     For c = 1 To x
      S = Mid$(rpt(c, 3), 1, 3) & Mid$(rpt(c, 3), 5, 2) & Mid$(rpt(c, 3), 8, 4)
      Call MWRITEP(KEY, S, rpt(c, 4), rpt(c, 5))
     Next c
    End If 'End Weekly Time Records Process
    Close #3 'Close MainFrame File
    Open "C:\AMWS\C2MACRO.DAT" For Output As 4    ' Open file.
    Print #4, "ECHO NEXT TASK"
    Close #4
    Close
End

End Sub

