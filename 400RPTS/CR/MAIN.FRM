VERSION 4.00
Begin VB.Form Main 
   Caption         =   "AS/400 -> XcelleNet File Server Report Distribution"
   ClientHeight    =   4350
   ClientLeft      =   795
   ClientTop       =   1515
   ClientWidth     =   7275
   Height          =   4755
   Icon            =   "MAIN.frx":0000
   Left            =   735
   LinkTopic       =   "Form1"
   ScaleHeight     =   4350
   ScaleWidth      =   7275
   Top             =   1170
   Width           =   7395
   Begin VB.PictureBox LightGreen 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   6600
      Picture         =   "MAIN.frx":0442
      ScaleHeight     =   615
      ScaleWidth      =   495
      TabIndex        =   15
      Top             =   1320
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.PictureBox LightRed 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   6600
      Picture         =   "MAIN.frx":0884
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   14
      Top             =   1320
      Width           =   495
   End
   Begin VB.CommandButton AutoProcess 
      Caption         =   "Start Auto Process"
      Height          =   375
      Left            =   4560
      TabIndex        =   12
      Top             =   1320
      Width           =   1935
   End
   Begin VB.Timer Timer 
      Left            =   6720
      Top             =   120
   End
   Begin VB.CheckBox Delay 
      Caption         =   "Delay"
      Height          =   195
      Left            =   3960
      TabIndex        =   10
      Top             =   3960
      Width           =   255
   End
   Begin VB.Frame Status 
      Caption         =   " Status "
      Height          =   1455
      Left            =   3840
      TabIndex        =   8
      Top             =   2400
      Width           =   3255
      Begin VB.Label StatusLabel 
         Caption         =   "idle"
         Height          =   1095
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.CommandButton Exit 
      Caption         =   "Exit"
      Height          =   375
      Left            =   4560
      TabIndex        =   7
      Top             =   1920
      Width           =   1935
   End
   Begin VB.CommandButton UpdateToProcess 
      Caption         =   "Update To Process List"
      Height          =   375
      Left            =   4560
      TabIndex        =   6
      Top             =   720
      Width           =   1935
   End
   Begin VB.CommandButton StartButton 
      Caption         =   "Start Distribution Once"
      Height          =   375
      Left            =   4560
      TabIndex        =   5
      Top             =   120
      Width           =   1935
   End
   Begin VB.ListBox CopyList 
      Height          =   3375
      Left            =   1920
      TabIndex        =   3
      Top             =   480
      Width           =   1815
   End
   Begin VB.FileListBox ToProcess 
      Height          =   3375
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   1455
   End
   Begin VB.CommandButton StopProcess 
      Caption         =   "Stop Auto Process"
      Height          =   375
      Left            =   4560
      TabIndex        =   13
      Top             =   1320
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label DelayLabel 
      Caption         =   "Click here to delay Status screen."
      Height          =   255
      Left            =   4200
      TabIndex        =   11
      Top             =   3960
      Width           =   2775
   End
   Begin VB.Label Label3 
      Caption         =   "Ext        Copy Directory"
      Height          =   255
      Left            =   1920
      TabIndex        =   4
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   3840
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "I:\XNET\"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "Main"
Attribute VB_Creatable = False
Attribute VB_Exposed = False



Private Sub AutoProcess_Click()
    AutoProcess.Visible = False
    StopProcess.Visible = True
    LightRed.Visible = False
    LightGreen.Visible = True
    StartButton.Enabled = False
    SelectCopyFile
    Timer.Enabled = True
    'Main.Icon = LoadPicture("X:\400RPTS\DATA\TIMER01.ICO")
    DoEvents
End Sub


Private Sub Exit_Click()
    End
End Sub


Private Sub Form_Load()
I = 0
Timer.Interval = 60000
Timer.Enabled = False
ToProcess.Path = "I:\XNET"
LABEL2.Caption = Str(ToProcess.ListCount) + " To Process "
'ReDim CopyTable(1000, 2)
Open "X:\400RPTS\DATA\400RPTS.DST" For Input As #1
Counter = 1
Do Until EOF(1)
    Line Input #1, linedata
    If Mid$(linedata, 1, 1) <> ";" Then
        CopyTable(Counter, 1) = Mid$(linedata, 1, 8)
        CopyTable(Counter, 2) = Mid$(linedata, 10, 3)
        CopyList.AddItem (CopyTable(Counter, 2) + " -> " + CopyTable(Counter, 1))
        Counter = Counter + 1
    End If
Loop
Close 1
MaxExt = Counter
Open "X:\400rpts\data\6lcond.HP" For Input As 1
Input #1, FontString
Close 1
Open "X:\400rpts\data\8lcond.HP" For Input As 1
Input #1, FontString2
Close 1

    AutoProcess.Visible = False
    StopProcess.Visible = True
    LightRed.Visible = False
    LightGreen.Visible = True
    StartButton.Enabled = False
    SelectCopyFile
    Timer.Enabled = True
    'Main.Icon = LoadPicture("X:\400RPTS\DATA\TIMER01.ICO")
    DoEvents

End Sub





Public Sub SelectCopyFile()
On Error GoTo errorhandler2
ToProcess.Refresh
ToDo = ToProcess.ListCount - 1
TDate = LTrim(Str(Month(Now))) + LTrim(Str(Day(Now))) + LTrim(Str(Year(Now)))
Open "X:\400RPTS\DATA\" + TDate + ".LOG" For Append As #4
For Counter = 0 To ToDo
    Main.Icon = LoadPicture("X:\400RPTS\DATA\FILES07.ICO")
    StatusLabel.Caption = "Processing " + Str(Counter + 1) + " of " + Str(ToDo + 1)
    Main.Caption = "Processing " + Str(Counter + 1) + " of " + Str(ToDo + 1)
    Main.Refresh
    FileName = UCase(Mid$(ToProcess.List(Counter), 1, 8))
    FileExt = UCase(Mid$(ToProcess.List(Counter), 10, 3))
    FindDir (FileExt)
    If FileName <> "TESTTEST" Then
        Select Case Mid$(FileExt, 1, 1)
        Case "R"
            CopyExt = "RSS"
        Case "V"
            CopyExt = "VP"
        Case "D"
            CopyExt = "DM"
        Case "S"
            CopyExt = "ST"
        Case Else
            CopyExt = "R"
    End Select
    Select Case FileName
        Case "GLM060P1"   'Restaurant P&L
            PrevMonth
        Case "GLM105P3"   'P&L Summary DMSS
            PrevMonth
        Case "GL_M062P"   'DM Consolidated P&L
            PrevMonth
        Case "APM045P0"   'RSS AP/IM Repairs and Maint. PO/Work Code
            PrevMonth
        Case "GLM110P0"   'RSS Description 2 Analysis
            PrevMonth
        Case "GL_M160L"   'RSS Budget vs Actual
            PrevMonth
        Case "ICM342R1"   'Theoretical Food Cost
            PrevMonth
        Case "ICM342X2"   'Theoretical Food Cost DM
            PrevMonth
        Case "SAMM80B0"   'Restaurant Sales Journal
            PrevMonth
        Case "SAMM80Y0"   'DM Sales Journal
            PrevMonth
        Case Else
            CopyNormal
    End Select
    End If
    DoEvents
    'FileCopy "I:\XNET\" + FileName + "." + FileExt, "X:\400RPTS\TESTRPTS\" + FileName + "." + FileExt
    Kill "I:\XNET\" + FileName + "." + FileExt
Next
ToProcess.Refresh
LABEL2.Caption = Str(ToProcess.ListCount) + " To Process "
StatusLabel.Caption = " idle "
Main.Icon = LoadPicture("X:\400RPTS\DATA\NOTE12.ICO")
Main.Caption = "AS/400 -> XcelleNet Server Report Distribution"
Main.Refresh
Close #4
    Exit Sub
errorhandler2:
   Open "X:\400RPTS\DATA\ERROR.LOG" For Append As #10
   Print #10, Str(Now) + " SelectCopyFile ERROR: " + Str(Err.Number) + "  " + FileName + "." + FileExt + " X:\NODE\" + Trim(FileDir) + "\" + FileName + "." + CopyExt
   Close 4
   Close 10
   Resume Next
   Exit Sub
End Sub







Private Sub Picture1_Click()

End Sub

Private Sub StartButton_Click()
        
    SelectCopyFile
End Sub



Public Sub CopyNormal()
    On Error GoTo errorhandler
    Dim Top As Boolean
    Top = True
    'FileCopy "X:\400RPTS\" + FileName + "." + FileExt, "U:\" + FileDir + "\" + FileName + "." + CopyExt
    StatusLabel.Caption = "Processing " + Str(Counter + 1) + " of " + Str(ToDo + 1) + Chr(10) + Chr(10) + "From: " + FileName + "." + FileExt + Chr(10) + Chr(10) + "To: X:\NODE\" + Trim(FileDir) + "\" + FileName + "." + CopyExt
    Print #4, Str(Now) + " Copy " + FileName + "." + FileExt + " X:\NODE\" + Trim(FileDir) + "\" + FileName + "." + CopyExt
    Main.Refresh
    If Delay.Value = 1 Then
        For X = 1 To 10000
        DoEvents
        Next X
    End If
    LineCount = 0
    Open ("I:\XNET\" + FileName + "." + FileExt) For Input As #1
    Open ("X:\NODE\" + FileDir + "\" + FileName + "." + CopyExt) For Output As #2
    Do Until EOF(1)
        LineCount = LineCount + 1
        Line Input #1, linedata
        If (Mid$(linedata, 1, 1) = "1") And (Top = True) Then
          Top = False
          Select Case FileName
            Case "POW035X3"   'WEEKLY INVENTORY CONTROL VP
              Print #2, FontString2
            Case "POW035B2"   'WEEKLY INVENTORY CONTROL VP
              Print #2, FontString2
            Case "POW035B1"   'WEEKLY INVENTORY CONTROL DM AND REST.
              Print #2, FontString2
            Case "SAW09B2"   'WEEKLY SALES AND PAYROLL CONTROL
              Print #2, FontString2
            Case Else
              Print #2, FontString
          End Select
        'linedata = " " + FontString + Mid$(linedata, 2, Len(linedata))
        ElseIf Mid$(linedata, 1, 1) = "1" Then
                'If LineCount < 59 Then  Removed due to Weekly Inventory Control
                '    Print #2, Chr(12)
                'Else
                '    Print #2, Chr(12)
                'End If
                Print #2, Chr(12)
                LineCount = 0
        End If
        If Mid$(linedata, 1, 1) = "0" Then
                Print #2,
                LineCount = LineCount + 1
        End If
        Print #2, Mid$(linedata, 2, Len(linedata))

    Loop
    Close 1
    Close 2
    Exit Sub
errorhandler:
   Open "X:\400RPTS\DATA\ERROR.LOG" For Append As #10
   Print #10, Str(Now) + " CopyNormal ERROR: " + Str(Err.Number) + "  " + FileName + "." + FileExt + " X:\NODE\" + Trim(FileDir) + "\" + FileName + "." + CopyExt
   Close 1
   Close 2
   Close 10
   Exit Sub
End Sub

Public Sub FindDir(Ext)
    FileDir = "BADRPTS"
    For X = 1 To MaxExt
        If CopyTable(X, 2) = Ext Then
            FileDir = CopyTable(X, 1)
            X = MaxExt
        End If
    Next X
End Sub

Private Sub StopProcess_Click()
    AutoProcess.Visible = True
    StopProcess.Visible = False
    LightGreen.Visible = False
    LightRed.Visible = True
    StartButton.Enabled = True
    Timer.Enabled = False
    Main.Icon = LoadPicture("X:\400RPTS\DATA\NOTE12.ICO")
End Sub

Private Sub Timer_Timer()

    I = I + 1
        Main.Caption = "AS/400 -> XNet " + Format(Now, "hh:mm AMPM")
        'Main.Icon = LoadPicture("X:\400RPTS\DATA\TIMER01.ICO")
        Main.Refresh
    DoEvents
    If I = 5 Then
        I = 0
        Timer.Enabled = False
        SelectCopyFile
        Timer.Enabled = True
    End If
End Sub

Private Sub UpdateToProcess_Click()
    ToProcess.Refresh
    LABEL2.Caption = Str(ToProcess.ListCount) + " To Process "
    Main.Refresh
End Sub



Public Sub PrevMonth()
        Dim PrevM As String
        PrevM = Month(Now) - 1
        If Len(PrevM) <> 2 Then
            PrevM = "0" + PrevM
        End If
        If PrevM = "00" Then
            PrevM = "12"
        End If
        Select Case Mid$(FileExt, 1, 1)
        Case "V"
            CopyExt = "VP"
        Case "R"
            CopyExt = "R" + PrevM
        Case "D"
            CopyExt = "D" + PrevM
        Case Else
            CopyExt = "R" + PrevM
    End Select
    StatusLabel.Caption = "Processing " + Str(Counter + 1) + " of " + Str(ToDo + 1) + Chr(10) + Chr(10) + "From: " + FileName + "." + FileExt + Chr(10) + Chr(10) + "To: X:\NODE\" + Trim(FileDir) + "\" + FileName + "." + CopyExt
    Print #4, Str(Now) + " Copy " + FileName + "." + FileExt + " X:\NODE\" + Trim(FileDir) + "\" + FileName + "." + CopyExt
    Main.Refresh
    If Delay.Value = 1 Then
        For X = 1 To 10000
        DoEvents
        Next X
    End If
    Dim Top As Boolean
    Top = True
    'FileCopy "X:\400RPTS\" + FileName + "." + FileExt, "U:\" + FileDir + "\" + FileName + "." + CopyExt
    LineCount = 0
    Open ("I:\XNET\" + FileName + "." + FileExt) For Input As #1
    Open ("X:\NODE\" + FileDir + "\" + FileName + "." + CopyExt) For Output As #2
    Do Until EOF(1)
        LineCount = LineCount + 1
        Line Input #1, linedata
        If (Mid$(linedata, 1, 1) = "1") And (Top = True) Then
          Top = False
          Print #2, FontString
          'linedata = " " + FontString + Mid$(linedata, 2, Len(linedata))
        ElseIf Mid$(linedata, 1, 1) = "1" Then
                If LineCount < 59 Then
                    Print #2, Chr(12)
                Else
                    Print #2,
                End If
                LineCount = 0
        End If
        If Mid$(linedata, 1, 1) = "0" Then
            Print #2,
            LineCount = LineCount + 1
        End If
        Print #2, Mid$(linedata, 2, Len(linedata))
    Loop
    Close 1
    Close 2

End Sub
