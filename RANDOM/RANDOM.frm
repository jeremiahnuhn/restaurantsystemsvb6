VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Random Number Generator"
   ClientHeight    =   5595
   ClientLeft      =   6660
   ClientTop       =   1530
   ClientWidth     =   4950
   BeginProperty Font 
      Name            =   "Arial Black"
      Size            =   15.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RANDOM.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5595
   ScaleWidth      =   4950
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      Height          =   855
      Left            =   360
      TabIndex        =   5
      Top             =   4320
      Width           =   4095
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00FF0000&
      Caption         =   "Generate Number"
      Height          =   855
      Left            =   360
      MaskColor       =   &H00FF0000&
      TabIndex        =   4
      Top             =   2040
      Width           =   4095
   End
   Begin VB.TextBox MaxNum 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1215
      Left            =   2400
      TabIndex        =   0
      Text            =   " "
      Top             =   720
      Width           =   2055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Winning Number"
      ForeColor       =   &H000000FF&
      Height          =   975
      Left            =   480
      TabIndex        =   3
      Top             =   3120
      Width           =   1815
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Maximum Number"
      ForeColor       =   &H00FF0000&
      Height          =   975
      Left            =   480
      TabIndex        =   2
      Top             =   840
      Width           =   1815
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   2520
      TabIndex        =   1
      Top             =   3240
      Width           =   1695
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Randomize
Label2.Caption = Int((Val(MaxNum.Text) * Rnd) + 1)
Command2.SetFocus
End Sub

Private Sub Command2_Click()
End
End Sub

