VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Import Data from Iris to RestDat"
   ClientHeight    =   5940
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5940
   ScaleWidth      =   6690
   Begin VB.ListBox ListEmployeeNum 
      Height          =   450
      Left            =   480
      TabIndex        =   6
      Top             =   1680
      Width           =   1695
   End
   Begin VB.ListBox ListStatus 
      Height          =   450
      ItemData        =   "FRMMAIN.frx":0000
      Left            =   4200
      List            =   "FRMMAIN.frx":0002
      TabIndex        =   5
      Top             =   1080
      Width           =   1575
   End
   Begin VB.ListBox ListPayType 
      Height          =   450
      Left            =   2280
      TabIndex        =   4
      Top             =   1080
      Width           =   1695
   End
   Begin VB.ListBox ListRate 
      Height          =   450
      Left            =   480
      TabIndex        =   3
      Top             =   1080
      Width           =   1695
   End
   Begin VB.ListBox ListNum 
      Height          =   450
      Left            =   4200
      TabIndex        =   2
      Top             =   480
      Width           =   1575
   End
   Begin VB.ListBox ListLname 
      Height          =   450
      Left            =   2280
      TabIndex        =   1
      Top             =   480
      Width           =   1695
   End
   Begin VB.ListBox ListFname 
      Height          =   450
      Left            =   480
      TabIndex        =   0
      Top             =   480
      Width           =   1695
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Command1_Click()
End
End Sub

Private Sub Form_Load()
On Error Resume Next
Dim FirstName As String
Dim LastName As String
Dim SSAN As String
Dim EmployeeNum As String
Dim EmployeeRate As String
Dim EmployeeSalary As Single
Dim EmployeeStatus As Integer
Dim EmployeePayType As Integer
Dim EmployeeJobCode As String
Dim EmployeeClockIn As String
Dim EmployeeClockOut As String
Dim EmployeeClockInTime(1000, 2) As String
Dim EmployeeClockOutTime(1000, 2) As String
Dim ClockInTime(1000, 2) As Single
Dim ClockOutTime(1000, 2) As Single
Dim I As Integer
Dim J As Integer
Dim A As Integer
Dim B As Integer
Dim RecordNum As Integer
Dim PreviousEmployeeNum As Integer
Dim RestDB As Database
Dim RestSales As Recordset
Dim linedata As String
Dim TestDate As String
Dim TestDate2 As Date
Dim DaysPast As Integer
Dim TestTime As String
Dim StartTime As String
Dim Filedate As String
Dim CurrDOW As Integer
Dim TestFile As String
Dim cn As ADODB.Connection
Dim rs As ADODB.Recordset
Dim ps As ADODB.Recordset
Dim FirstRun As Boolean
Dim NotFound As Boolean
Dim DayPartHour As Single
Dim DayPartMinute As Single
Dim RestType As Integer
RestType = 1
If Dir("C:\Nodesys\Unitid.dat") <> "" Then
   Open "C:\Nodesys\Unitid.dat" For Input As #1
      Input #1, linedata
   Close #1
   If Val(Left(linedate, 4)) >= 6500 Then
      RestType = 2
   End If
End If
DaysPast = Weekday(Date)
NotFound = True
FirstRun = False
Set cn = New ADODB.Connection
'Open the ADO Recordset
cn.Open "Provider=sqloledb;user id=sa;pwd=;Initial Catalog=IRIS;Server=BNE_SERVER;"
Set rs = New ADODB.Recordset
Set ps = New ADODB.Recordset
Set hs = New ADODB.Recordset
With rs
   .CursorLocation = adUseClient
   .CursorType = adOpenDynamic
   .LockType = adLockOptimistic
   .Open "Select * FROM tblEmployees ORDER BY EmployeeID", cn
End With
With ps
   .CursorLocation = adUseClient
   .CursorType = adOpenDynamic
   .LockType = adLockOptimistic
   .Open "Select * FROM tblEmployeesJobs ORDER BY EmployeeID", cn
End With
With hs
   .CursorLocation = adUseClient
   .CursorType = adOpenDynamic
   .LockType = adLockOptimistic
   .Open "Select * FROM tblEmployeesHours ORDER BY EmployeeID", cn
End With
I = 1
J = 1
rs.MoveFirst
Open "C:\Newpos\cat04.dif" For Output As #1
   Do Until rs.EOF
        EmployeeNum = rs!employeeID
        FirstName = rs!FirstName
        LastName = rs!LastName
        EmployeeStatus = rs!StatusID
        EmployeePayType = rs!PayTypeID
        SSAN = rs!SSN
        If EmployeeStatus <> 3 Then
           Print #1, Format(Left(FirstName, 6), ">"); Tab(8); Format(Left(LastName, 1), ">"); Tab(12); SSAN
        End If
        Do Until J = Val(EmployeeNum)
           ListFname.AddItem ""
           ListLname.AddItem ""
           ListNum.AddItem J
           ListPayType.AddItem 0
           ListStatus.AddItem 3
           J = J + 1
        Loop
           ListFname.AddItem FirstName
           ListLname.AddItem LastName
           ListNum.AddItem EmployeeNum
           ListPayType.AddItem EmployeePayType
           ListStatus.AddItem EmployeeStatus
           J = J + 1
        rs.MoveNext
   Loop
Close #1
rs.Close
Set rs = Nothing
PreviousEmployeeNum = 0
ps.MoveFirst
Do Until ps.EOF
    EmployeeRate = ps!PayRate
    EmployeeNum = ps!employeeID
    ListRate.AddItem EmployeeRate
    ListEmployeeNum.AddItem EmployeeNum
    ps.MoveNext
Loop
ps.Close
Set ps = Nothing
Open "C:\Newpos\cat06.dif" For Output As #1
   For I = 0 To Val(ListNum.ListCount) - 1
         If I = 7 Or I = 8 Or I = 9 Then
            Print #1, Tab(1); "!"; Tab(17); "0.00"; Tab(30); "0"
         Else
            If Val(ListPayType.List(I)) = 0 Then
               Print #1, Tab(1); "!"; Tab(17); "0.00"; Tab(30); "0"
            Else
               If Val(ListStatus.List(I)) = 3 Then
                  Print #1, Tab(1); "!"; Tab(17); "0.00"; Tab(30); "0"
               Else
                  If Val(ListPayType.List(I)) = 2 Then
                     Print #1, Tab(1); Left(ListFname.List(I), 14); Tab(17); "0.01"; Tab(30); "2"
                  Else
                     For J = 0 To ListEmployeeNum.ListCount - 1
                        If Val(ListNum.List(I)) = Val(ListEmployeeNum.List(J)) Then
                           Print #1, Tab(1); Left(ListFname.List(I), 14); Tab(17); Format(ListRate.List(J), "##0.00"); Tab(30); "1"
                           Exit For
                        End If
                     Next J
                  End If
               End If
            End If
         End If
   Next I
Close #1
If RestType = 2 Then
   TestDate = Date
Else
   TestDate = DateAdd("d", -1, Date)
End If

For A = 1 To 999
   EmployeeClockInTime(A, 1) = "0"
   EmployeeClockOutTime(A, 1) = "0"
   EmployeeClockInTime(A, 2) = "0"
   EmployeeClockOutTime(A, 2) = "0"
Next A
hs.MoveFirst
   Do Until hs.EOF
      If hs!employeeID <> "" Then
         EmployeeNum = hs!employeeID
      Else
         EmployeeNum = "0"
      End If
      If hs!Clockin <> "" Then
         EmployeeClockIn = hs!Clockin
      End If
      If hs!ClockOut <> "" Then
         EmployeeClockOut = hs!ClockOut
      End If
      A = Val(EmployeeNum)
      If DateValue(EmployeeClockIn) = DateValue(TestDate) Then
         If EmployeeClockInTime(A, 1) = "0" Then
            EmployeeClockInTime(A, 1) = EmployeeClockIn
            EmployeeClockOutTime(A, 1) = EmployeeClockOut
         Else
            If EmployeeClockInTime(A, 2) = "0" Then
               EmployeeClockInTime(A, 2) = EmployeeClockIn
               EmployeeClockOutTime(A, 2) = EmployeeClockOut
            End If
         End If
      End If
   hs.MoveNext
   Loop
hs.Close
Set hs = Nothing
Open "C:\Newpos\cat09.dif" For Output As #1
For A = 1 To 999
   If EmployeeClockInTime(A, 1) <> "0" Then
      DayPartHour = DatePart("h", EmployeeClockInTime(A, 1))
      DayPartMinute = DatePart("n", EmployeeClockInTime(A, 1))
      ClockInTime(A, 1) = (DayPartHour * 60) + DayPartMinute
      EmployeeClockInTime(A, 1) = Format(ClockInTime(A, 1), "0000")
      DayPartHour = DatePart("h", EmployeeClockOutTime(A, 1))
      DayPartMinute = DatePart("n", EmployeeClockOutTime(A, 1))
      ClockOutTime(A, 1) = (DayPartHour * 60) + DayPartMinute
      EmployeeClockOutTime(A, 1) = Format(ClockOutTime(A, 1), "0000")
      
      DayPartHour = DatePart("h", EmployeeClockInTime(A, 2))
      DayPartMinute = DatePart("n", EmployeeClockInTime(A, 2))
      ClockInTime(A, 2) = (DayPartHour * 60) + DayPartMinute
      EmployeeClockInTime(A, 2) = Format(ClockInTime(A, 2), "0000")
      DayPartHour = DatePart("h", EmployeeClockOutTime(A, 2))
      DayPartMinute = DatePart("n", EmployeeClockOutTime(A, 2))
      ClockOutTime(A, 2) = (DayPartHour * 60) + DayPartMinute
      EmployeeClockOutTime(A, 2) = Format(ClockOutTime(A, 2), "0000")
      
      EmployeeNum = Format(A, "000")
      Print #1, Tab(8); EmployeeNum; Tab(17); EmployeeClockInTime(A, 1); Tab(27); EmployeeClockInTime(A, 2); Tab(40); "0"; Tab(50); "0"; Tab(60); "0"; Tab(70); "0"; _
                Tab(77); EmployeeClockOutTime(A, 1); Tab(87); EmployeeClockOutTime(A, 2); Tab(100); "0"; Tab(110); "0"; Tab(120); "0"; Tab(130); "0"
   End If
Next A
Close #1
TestDate2 = Date
Filedate = Format(Date, "yyyymmdd")
TestFile = "C:\Iris\Polling\" & Filedate & "S101.txt"
Set RestDB = OpenDatabase("C:\Labor\RestDat.mdb", False, False)
Set RestSales = RestDB.OpenRecordset("Select * from Netsls order by dt")
If Dir(TestFile) <> "" Then
   Open TestFile For Input As #1
       Do While Not EOF(1)
          Line Input #1, linedata
       Loop
   Close #1
End If
RestSales.FindFirst "dt = #" & TestDate2 & "#"
If RestSales.NoMatch = True Then
   RestSales.AddNew
   RestSales!dt = TestDate2
   If Mid(linedata, 19, 8) <> "" Then
      RestSales!sls1 = Mid(linedata, 19, 8)
   Else
      RestSales!sls1 = 0
   End If
   If Mid(linedata, 28, 8) <> "" Then
      RestSales!sls2 = Mid(linedata, 28, 8)
   Else
      RestSales!sls2 = 0
   End If
   If Mid(linedata, 37, 8) <> "" Then
      RestSales!sls3 = Mid(linedata, 37, 8)
   Else
      RestSales!sls3 = 0
   End If
   If Mid(linedata, 46, 8) <> "" Then
      RestSales!sls4 = Mid(linedata, 46, 8)
   Else
      RestSales!sls4 = 0
   End If
   If Mid(linedata, 55, 8) <> "" Then
      RestSales!sls5 = Mid(linedata, 55, 8)
   Else
      RestSales!sls5 = 0
   End If
   If Mid(linedata, 64, 8) <> "" Then
      RestSales!sls6 = Mid(linedata, 64, 8)
   Else
      RestSales!sls6 = 0
   End If
   If Mid(linedata, 73, 8) <> "" Then
      RestSales!sls7 = Mid(linedata, 73, 8)
   Else
      RestSales!sls7 = 0
   End If
   If Mid(linedata, 82, 8) <> "" Then
      RestSales!sls8 = Mid(linedata, 82, 8)
   Else
      RestSales!sls8 = 0
   End If
   If Mid(linedata, 91, 8) <> "" Then
      RestSales!sls9 = Mid(linedata, 91, 8)
   Else
      RestSales!sls9 = 0
   End If
   If Mid(linedata, 100, 8) <> "" Then
      RestSales!sls10 = Mid(linedata, 100, 8)
   Else
      RestSales!sls10 = 0
   End If
   If Mid(linedata, 109, 8) <> "" Then
      RestSales!sls11 = Mid(linedata, 109, 8)
   Else
      RestSales!sls11 = 0
   End If
   If Mid(linedata, 118, 8) <> "" Then
      RestSales!sls12 = Mid(linedata, 118, 8)
   Else
      RestSales!sls12 = 0
   End If
   If Mid(linedata, 127, 8) <> "" Then
      RestSales!sls13 = Mid(linedata, 127, 8)
   Else
      RestSales!sls13 = 0
   End If
   If Mid(linedata, 136, 8) <> "" Then
      RestSales!sls14 = Mid(linedata, 136, 8)
   Else
      RestSales!sls14 = 0
   End If
   If Mid(linedata, 145, 8) <> "" Then
      RestSales!sls15 = Mid(linedata, 145, 8)
   Else
      RestSales!sls15 = 0
   End If
   If Mid(linedata, 154, 8) <> "" Then
      RestSales!sls16 = Mid(linedata, 154, 8)
   Else
      RestSales!sls16 = 0
   End If
   If Mid(linedata, 163, 8) <> "" Then
      RestSales!sls17 = Mid(linedata, 163, 8)
   Else
      RestSales!sls17 = 0
   End If
   If Mid(linedata, 172, 8) <> "" Then
      RestSales!sls18 = Mid(linedata, 172, 8)
   Else
      RestSales!sls18 = 0
   End If
   If Mid(linedata, 181, 8) <> "" Then
      RestSales!sls19 = Mid(linedata, 181, 8)
   Else
      RestSales!sls19 = 0
   End If
   If Mid(linedata, 190, 8) <> "" Then
      RestSales!sls20 = Mid(linedata, 190, 8)
   Else
      RestSales!sls20 = 0
   End If
   If Mid(linedata, 199, 8) <> "" Then
      RestSales!sls21 = Mid(linedata, 199, 8)
   Else
      RestSales!sls21 = 0
   End If
   If Mid(linedata, 208, 8) <> "" Then
      RestSales!sls22 = Mid(linedata, 208, 8)
   Else
      RestSales!sls22 = 0
   End If
   If Mid(linedata, 217, 8) <> "" Then
      RestSales!sls23 = Mid(linedata, 217, 8)
   Else
      RestSales!sls23 = 0
   End If
   If Mid(linedata, 226, 8) <> "" Then
      RestSales!sls24 = Mid(linedata, 226, 8)
   Else
      RestSales!sls24 = 0
   End If
   RestSales.Update
End If
End
End Sub


