VERSION 5.00
Begin VB.Form FrmPassword 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FE Utilities"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5580
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   5580
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox TxtPassword 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      IMEMode         =   3  'DISABLE
      Left            =   1200
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1080
      Width           =   3495
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3120
      TabIndex        =   2
      Top             =   2280
      Width           =   1695
   End
   Begin VB.CommandButton CmdEnter 
      Caption         =   "&Enter"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   960
      TabIndex        =   1
      Top             =   2280
      Width           =   1695
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Passwords are not case sensitive."
      Height          =   375
      Left            =   1200
      TabIndex        =   4
      Top             =   1800
      Width           =   3495
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Please enter Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   960
      TabIndex        =   3
      Top             =   600
      Width           =   3975
   End
End
Attribute VB_Name = "FrmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdEnter_Click()

If Trim(LCase(TxtPassword.Text)) = "rec" Then
    Open "C:\ul.bat" For Output As 1
    Print #1, "netsh interface set interface " + Chr(34) + "Internet" + Chr(34) + " DISABLE"
    Print #1, "c:\iris\setup\sleep.exe 3000"
    Print #1, "netsh interface set interface " + Chr(34) + "Internet" + Chr(34) + " ENABLE"
    Close 1
    Y = Shell("C:\ul.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Reconnect to BNE Network"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
                response = MsgBox("Thank You!" + Chr(10) + Chr(10) + "The password was entered to reconnect to the BNE Network.", vbInformation, "Reconnect")

       End
      Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "ih" Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "IRIS Support Functions Accessed"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "IRIS Support Functions Accessed"
            Close 4



    X = Shell("C:\nodesys\termuse.bat", vbMinimizedFocus)
    Support.Show
      TxtPassword.Text = ""
      FrmPassword.Hide
Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "msg" Then
    MgrMsg.Show
      TxtPassword.Text = ""
      FrmPassword.Hide
      DeskTop.Hide
Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "fixkvs" Then
      X = MsgBox("This process will start the process to repair the Kitchen Video System (KVS)." + Chr(10) + "Your Field Engineer will provide your the next steps to follow.", vbInformation, "FIXKVS password used correctly.")
       Open "c:\fixkvs.bat" For Output As #1
        Print #1, "REM Created " + Str(Now())
        Print #1, "COPY C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG1\C\KITDB.FLG"
        Print #1, "Copy C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG2\C\KITDB.FLG"
        Print #1, "exit"
      Close #1
      Y = Shell("C:\fixkvs.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  Kitchen DB Replaced"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
      End
      Exit Sub
End If



If Trim(LCase(TxtPassword.Text)) = "out" Then
      X = MsgBox("This process will deleted the Clock In record for the Support User Employee # 10 and/or the District Manager User # 8.", vbInformation, "Clock Out Support User/District Manager")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Delete tblEmployeesHours Where EmployeeID = 10 and ClockOut is null"
        Print #1, "Delete tblEmployeesHours Where EmployeeID = 8 and ClockOut is null"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
            MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  Time Records deleted for 10 or 8"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub
End If



If Trim(LCase(TxtPassword.Text)) = "swipeoff" Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Mgr Card Swipe Off"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Mgr Card Swipe Off"
            Close 4
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 0 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
        Else
           Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  Swipe Cards NOT required"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "swipeon" Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Mgr Card Swipe On"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Mgr Card Swipe On"
            Close 4
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 1 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Else
           Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  Swipe Cards required"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
      End
      Exit Sub
End If



'--------- Credit Card Authorization --------------------

If Trim(LCase(TxtPassword.Text)) = "caon" Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "CC Authorization On"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "CC Authorization On"
            Close 4
      
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 0 Where settingid = 544"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
        Else
           Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  CC Authorization On"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
    X = MsgBox("The IRIS system has been configured to authorize every Credit Card transaction.  The terminals will receive an update in a few minutes to make this change active.  ", vbInformation, "Credit Card Authorization Enabled")


      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "caoff" Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "CC Authorization Off"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "CC Authorization Off"
            Close 4
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 1 Where settingid = 544"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Else
           Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
           Print #1, "cd\EdmWeb"
           Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      End If
      Close #1
      Y = Shell("C:\swipe.bat", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  CC Authorization Off"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
  X = MsgBox("The IRIS system has been configured to force every Credit Card transaction.  The terminals will receive an update in a few minutes to make this change active.  " + Chr(13) + Chr(13) + "This process should only be used when your internet/broadband is not working.  This password should only be used after all troubleshooting has been completed on your internet/broadband equipment." + Chr(13) + Chr(13) + "It is important to use the password CAON, after the internet/broadband problem is resolved.  This will configure IRIS to authorize every Credit Card.", vbInformation, "Credit Card Authorization Disabled")

    
      End
      Exit Sub
End If


'--------------- Credit Card Authorization End ---------------------------------



If Trim(LCase(TxtPassword.Text)) = "logoff" Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Log Off Used"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Log Off Used"
            Close 4
      ChDir ("C:\Support")
      Open "c:\support\logon.BAT" For Output As #1
        Print #1, "Regedit /s AutoLogOn-off.reg"
      Close #1
      Y = Shell("C:\support\logon.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      X = MsgBox("You can log off and log in as the Support User.", vbInformation, "Auto Logon Disabled")
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  Support User Logged In"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
      
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "logon" Then
      ChDir ("C:\Support")
      Open "c:\support\logon.BAT" For Output As #1
        Print #1, "Regedit /s AutoLogOn-On.reg"
      Close #1
      Y = Shell("C:\support\logon.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      X = MsgBox("The system has been setup to Auto Logon the GM#### user.", vbInformation, "Auto Logon Enabled")
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  GM User Logged In"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
      
      End
      Exit Sub
End If




If Trim(LCase(TxtPassword.Text)) = "vnc" Then
      Y = Shell("C:\Documents and Settings\Administrator\Desktop\vnc-4.0-x86_win32_viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "log" Then
      If Dir("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe") = "" Then
        FileCopy "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.zip-_DisplayLogFileContents.UI.exe", "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe"
      End If
      ChDir ("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer")
      Y = Shell("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "iex" Then
      Y = Shell("C:\Program Files\Internet Explorer\iexplore.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "vpn" Then
      Y = Shell("C:\bnevpn.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "lcu" Then
      Y = Shell("c:\nodesys\lcupdate.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If


If Trim(LCase(TxtPassword.Text)) = "dbe" Then
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "sa" Then
      Y = Shell("cmd.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "exp" Then
      Y = Shell("c:\WINDOWS\explorer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "oft" Then
      Y = Shell("c:\bneapps\oftload.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "runlc" Then
      Y = Shell("c:\IRIS\BIN\RUNLC.BAT", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "file" Then
      
      If Dir("c:\program files\xpient solutions\psiexporter\out\iris.irs") <> "" Then
            X = MsgBox("IRIS poll file exist!  A LiveConnect session will be started.", vbInformation, "File Exists")
            Y = Shell("c:\IRIS\BIN\REPOLL.BAT", vbNormalFocus)
            End
      Else
              
            If Hour(Now) < 15 Then
                CheckDate = Date
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date + 1
            End If
            
            If BusinessDate <> CheckDate Then
                 X = MsgBox("Business Date is incorrect, please complete End of Day.", vbInformation, "Business Date Incorrect")
                 End
            End If
              
            Open "c:\iris\bin\createpoll.BAT" For Output As #1
            Print #1, "rem Created: " + Str(Now())
            Print #1, "Title DO NOT CLOSE THIS WINDOW"
            Print #1, "cd c:\iris\bin"
            If Weekday(Now) = 2 Then
                If Hour(Now) >= 15 Then
                    'Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) = 3 Then
                If Hour(Now) < 15 Then
                    'Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) <> 2 And Weekday(Now) <> 3 Then
                'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
            End If
            Print #1, "cd \iris\bin"
            'Print #1, "Settle~1.exe"
            'Print #1, "c:\iris\setup\sleep.exe 10000"
            Print #1, "cd c:\Program Files\xpient solutions\psiExporter\bin"
            Print #1, "PSIExporter.exe /yesterday /q"
            Print #1, "cd C:\Program Files\xpient solutions\psiExporter\OUT"
            Print #1, "IF EXIST IRIS.IRS copy IRIS.IRS + *.txt IRIS.IRS"
            Print #1, "IF NOT EXIST IRIS.IRS COPY *.TXT IRIS.IRS"
            Print #1, "del *.txt"
            Print #1, "cd \iris\bin"
            Print #1, "REPOLL.BAT"
            Close #1
            Y = Shell("c:\iris\bin\createpoll.BAT", vbMinimizedNoFocus)
        End If

            TxtPassword.Text = ""
            FrmPassword.Hide
        End
    Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "mmc" Then
      Y = Shell("C:\WINdows\system32\mmc.exe c:\iris\bin\console1.msc", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "sql" Then
    If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
          Y = Shell("c:\Program Files\Microsoft SQL Server\90\Tools\Binn\VSShell\Common7\IDE\SSMSEE.EXE", vbNormalFocus)
    Else
          Y = Shell("C:\Program Files\Microsoft SQL Server\80\Tools\Binn\isqlw.exe -S (local)\XSIRIS -U sa -P -d Iris", vbNormalFocus)
    End If
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If
'Password to copy INI files

'INI1
If Trim(LCase(TxtPassword.Text)) = "fix1" Then

    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " T1 - Copy INI Files (fix1) "
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG1\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG1\INI\*.INI \\BNE_REG1\C\IRIS\INI"
      Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
        Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Replace INI Files on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
      Close #1
      Y = Shell("c:\ih.BAT", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      response = MsgBox("The password entered will copy some configuration files from the IRIS server to the terminal." + Chr(10) + Chr(10) + "Please unplug the power to the terminal and then plug in.  This process fixes some issues where the terminal is not loading properly", vbInformation, "Replace INI Files Terminal 1")
      End
      Exit Sub
End If

'INI2
If Trim(LCase(TxtPassword.Text)) = "fix2" Then

    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " T2 - Copy INI Files (fix2) "
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG2\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG2\INI\*.INI \\BNE_REG2\C\IRIS\INI"
      Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
        Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Replace INI Files on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
      Close #1
      Y = Shell("c:\ih.BAT", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      response = MsgBox("The password entered will copy some configuration files from the IRIS server to the terminal." + Chr(10) + Chr(10) + "Please unplug the power to the terminal and then plug in.  This process fixes some issues where the terminal is not loading properly", vbInformation, "Replace INI Files Terminal 2")
      End
      Exit Sub
End If
'INI3
If Trim(LCase(TxtPassword.Text)) = "fix3" Then

    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " T3 - Copy INI Files (fix3) "
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG3\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG3\INI\*.INI \\BNE_REG3\C\IRIS\INI"
      Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
        Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Replace INI Files on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
      Close #1
      Y = Shell("c:\ih.BAT", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      response = MsgBox("The password entered will copy some configuration files from the IRIS server to the terminal." + Chr(10) + Chr(10) + "Please unplug the power to the terminal and then plug in.  This process fixes some issues where the terminal is not loading properly", vbInformation, "Replace INI Files Terminal 3")
      End
      Exit Sub
End If
'INI4
If Trim(LCase(TxtPassword.Text)) = "fix4" Then

    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " T4 - Copy INI Files (fix4) "
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG4\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG4\INI\*.INI \\BNE_REG4\C\IRIS\INI"
      Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
        Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Replace INI Files on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
      Close #1
      Y = Shell("c:\ih.BAT", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      response = MsgBox("The password entered will copy some configuration files from the IRIS server to the terminal." + Chr(10) + Chr(10) + "Please unplug the power to the terminal and then plug in.  This process fixes some issues where the terminal is not loading properly", vbInformation, "Replace INI Files Terminal 4")
      End
      Exit Sub
End If
'INI5
If Trim(LCase(TxtPassword.Text)) = "fix5" Then

    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " T5 - Copy INI Files (fix5) "
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG5\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG5\INI\*.INI \\BNE_REG5\C\IRIS\INI"
      Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
        Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Replace INI Files on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
      Close #1
      Y = Shell("c:\ih.BAT", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      response = MsgBox("The password entered will copy some configuration files from the IRIS server to the terminal." + Chr(10) + Chr(10) + "Please unplug the power to the terminal and then plug in.  This process fixes some issues where the terminal is not loading properly", vbInformation, "Replace INI Files Terminal 5")
      End
      Exit Sub
End If

'INI6
If Trim(LCase(TxtPassword.Text)) = "fix6" Then

    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " T6 - Copy INI Files (fix6) "
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG6\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG6\INI\*.INI \\BNE_REG6\C\IRIS\INI"
      Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
        Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Replace INI Files on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
      Close #1
      Y = Shell("c:\ih.BAT", vbMinimizedFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      response = MsgBox("The password entered will copy some configuration files from the IRIS server to the terminal." + Chr(10) + Chr(10) + "Please unplug the power to the terminal and then plug in.  This process fixes some issues where the terminal is not loading properly", vbInformation, "Replace INI Files Terminal 6")
      End
      Exit Sub
End If



If Trim(LCase(TxtPassword.Text)) = "adp" Then
      X = MsgBox("This process will deleted the cookies used by Internet Explorer.  This should remove the lock on the employee's ADP account.  Please wait at least two minute for the process to complete before trying again.", vbInformation, "ADP Fix")
       Open "c:\fixkvs.bat" For Output As #1
        Print #1, "REM Created " + Str(Now())
        Print #1, "c:\windows\system32\RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 2"
        Print #1, "exit"
      Close #1
      Y = Shell("C:\fixkvs.bat", vbHide)
      TxtPassword.Text = ""
      FrmPassword.Hide
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  ADP Delete Cookies"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
      End
      Exit Sub
End If
'Envysion Patch
If Trim(LCase(TxtPassword.Text)) = "evp" Then
      X = MsgBox("This process will install the necessary Envysion Patch.", vbInformation, "Envysion Patch")
      ChDir ("c:\BNEAPPS")
      Y = Shell("C:\bneapps\runEVpatch.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  Envysion Patch"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
      End
      Exit Sub
End If

'------------- END of Passwords ------

      X = MsgBox("The password entered was not found.  Please try again.", vbCritical, "Incorrect Password")

TxtPassword.Text = ""
TxtPassword.SetFocus

End Sub

Private Sub CmdExit_Click()
TxtPassword.Text = ""
FrmPassword.Hide
End Sub

