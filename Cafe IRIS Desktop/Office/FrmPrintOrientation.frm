VERSION 5.00
Begin VB.Form PrintOrientation 
   Caption         =   "Print New Employee Orientation Forms"
   ClientHeight    =   6570
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6030
   LinkTopic       =   "Form1"
   ScaleHeight     =   6570
   ScaleWidth      =   6030
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Mgmt 
      Caption         =   "Management"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   960
      TabIndex        =   4
      Top             =   3840
      Width           =   3015
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   960
      TabIndex        =   2
      Top             =   4920
      Width           =   3015
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Spanish"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   960
      TabIndex        =   1
      Top             =   2760
      Width           =   3015
   End
   Begin VB.CommandButton Command1 
      Caption         =   "English"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   960
      TabIndex        =   0
      Top             =   1680
      Width           =   3015
   End
   Begin VB.Label Label1 
      Caption         =   "Printing will begin 20 seconds after clicking on either the English or Spanish button."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   3
      Top             =   480
      Width           =   4815
   End
End
Attribute VB_Name = "PrintOrientation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\REPORTS\Paper Forms\Orientation Forms English  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
End
End Sub

Private Sub Command2_Click()
Open "C:\SPNPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\REPORTS\Paper Forms\Orientation Forms Spanish  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\SPNPRINT.BAT", 0)
End
End Sub

Private Sub Command3_Click()
End
End Sub

Private Sub Mgmt_Click()
Open "C:\SPNPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\REPORTS\Paper Forms\Orientation Forms Management  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\SPNPRINT.BAT", 0)
End
End Sub
