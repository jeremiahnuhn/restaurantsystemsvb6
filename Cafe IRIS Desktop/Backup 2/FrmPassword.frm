VERSION 5.00
Begin VB.Form FrmPassword 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5580
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   5580
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox TxtPassword 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      IMEMode         =   3  'DISABLE
      Left            =   1200
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1080
      Width           =   3495
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3120
      TabIndex        =   2
      Top             =   2040
      Width           =   1695
   End
   Begin VB.CommandButton CmdEnter 
      Caption         =   "&Enter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   960
      TabIndex        =   1
      Top             =   2040
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Please enter Password"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   3
      Top             =   360
      Width           =   3135
   End
End
Attribute VB_Name = "FrmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdEnter_Click()

If Trim(LCase(TxtPassword.Text)) = "ih" Then
    x = Shell("C:\nodesys\termuse.bat", vbMinimizedFocus)
    Support.Show
      TxtPassword.Text = ""
      FrmPassword.Hide
Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "msg" Then
    MgrMsg.Show
      TxtPassword.Text = ""
      FrmPassword.Hide
      DeskTop.Hide
Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "out" Then
      x = MsgBox("This process will deleted the Clock In record for the Support User Employee # 10.", vbInformation, "Clock Out Support User")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Delete tblEmployeesHours Where EmployeeID = 10 and ClockOut is null"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
            Print #1, "IF NOT EXIST C:\SUPPORT GOTO :NOSECURE"
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "GOTO :NEXT"
            Print #1, ":NOSECURE"
            Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, ":NEXT"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "logoff" Then
      ChDir ("C:\Support")
      Open "c:\support\logon.BAT" For Output As #1
        Print #1, "Regedit /s AutoLogOn-off.reg"
      Close #1
      Y = Shell("C:\support\logon.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      x = MsgBox("You can log off and log in as the Support User.", vbInformation, "Auto Logon Disabled")
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "logon" Then
      ChDir ("C:\Support")
      Open "c:\support\logon.BAT" For Output As #1
        Print #1, "Regedit /s AutoLogOn-On.reg"
      Close #1
      Y = Shell("C:\support\logon.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      x = MsgBox("The system has been setup to Auto Logon the GM#### user.", vbInformation, "Auto Logon Enabled")
      End
      Exit Sub
End If
If Trim(LCase(TxtPassword.Text)) = "swipeoff" Then
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 0 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
        Print #1, "cd\EdmWeb"
        Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "swipeon" Then
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 1 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
        Print #1, "cd\EdmWeb"
        Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If




If Trim(LCase(TxtPassword.Text)) = "vnc" Then
      Y = Shell("C:\Documents and Settings\Administrator\Desktop\vnc-4.0-x86_win32_viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "log" Then
      If Dir("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe") = "" Then
        FileCopy "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.zip-_DisplayLogFileContents.UI.exe", "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe"
      End If
      ChDir ("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer")
      Y = Shell("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "iex" Then
      Y = Shell("C:\Program Files\Internet Explorer\iexplore.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "vpn" Then
      Y = Shell("C:\bnevpn.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "lcu" Then
      Y = Shell("c:\nodesys\lcupdate.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If


If Trim(LCase(TxtPassword.Text)) = "dbe" Then
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "sa" Then
      Y = Shell("cmd.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "exp" Then
      Y = Shell("c:\WINDOWS\explorer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "oft" Then
      Y = Shell("c:\bneapps\oftload.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "runlc" Then
      Y = Shell("c:\IRIS\BIN\RUNLC.BAT", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "file" Then
      
      If Dir("c:\program files\xpient solutions\psiexporter\out\iris.irs") <> "" Then
            x = MsgBox("IRIS poll file exist!  A LiveConnect session will be started.", vbInformation, "File Exists")
            Y = Shell("c:\IRIS\BIN\REPOLL.BAT", vbNormalFocus)
            End
      Else
              
            If Hour(Now) < 15 Then
                CheckDate = Date
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date + 1
            End If
            
            If BusinessDate <> CheckDate Then
                 x = MsgBox("Business Date is incorrect, please complete End of Day.", vbInformation, "Business Date Incorrect")
                 End
            End If
              
            Open "c:\iris\bin\createpoll.BAT" For Output As #1
            Print #1, "rem Created: " + Str(Now())
            Print #1, "Title DO NOT CLOSE THIS WINDOW"
            Print #1, "cd c:\iris\bin"
            If Weekday(Now) = 2 Then
                If Hour(Now) >= 15 Then
                    'Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) = 3 Then
                If Hour(Now) < 15 Then
                    'Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) <> 2 And Weekday(Now) <> 3 Then
                'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
            End If
            Print #1, "cd \iris\bin"
            'Print #1, "Settle~1.exe"
            'Print #1, "c:\iris\setup\sleep.exe 10000"
            Print #1, "cd c:\Program Files\xpient solutions\psiExporter\bin"
            Print #1, "PSIExporter.exe /yesterday /q"
            Print #1, "cd C:\Program Files\xpient solutions\psiExporter\OUT"
            Print #1, "IF EXIST IRIS.IRS copy IRIS.IRS + *.txt IRIS.IRS"
            Print #1, "IF NOT EXIST IRIS.IRS COPY *.TXT IRIS.IRS"
            Print #1, "del *.txt"
            Print #1, "cd \iris\bin"
            Print #1, "REPOLL.BAT"
            Close #1
            Y = Shell("c:\iris\bin\createpoll.BAT", vbMinimizedNoFocus)
        End If

            TxtPassword.Text = ""
            FrmPassword.Hide
        End
    Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "mmc" Then
      Y = Shell("C:\WINdows\system32\mmc.exe c:\iris\bin\console1.msc", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "sql" Then
    If Dir("c:\support\winzip.reg") <> "" Then
      Y = Shell("C:\Program Files\Microsoft SQL Server\80\Tools\Binn\isqlw.exe -S (local)\XSIRIS -U sa -P -d Iris", vbNormalFocus)
    Else
      Y = Shell("C:\Program Files\Microsoft SQL Server\80\Tools\Binn\isqlw.exe -S (local) -U sa -P -d Iris", vbNormalFocus)
    End If
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If


TxtPassword.Text = ""
FrmPassword.Hide
End
End Sub

Private Sub CmdExit_Click()
TxtPassword.Text = ""
FrmPassword.Hide
End Sub

