VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   5445
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   8910
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5445
   ScaleWidth      =   8910
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Dim RESTNUM As String
Open "N:\VB\FTPFILES\ALLIRIS.TXT" For Input As #1
Do While Not EOF(1)
    Line Input #1, DATALINE
    RESTNUM = Mid$(DATALINE, 40, 4)
    INfile = "S:\Hardees\IRIS\FTP\FTPPOLL\" + RESTNUM + ".INI"
    FileSpec = "FileSpec=" + LTrim(RESTNUM) + ".IRS"


        Open INfile For Output As #3

             
               Print #3, ";***************************************************************************************************************"
               Print #3, ";BNE 5/16/2012 - Transfer poll file to the FTPPoll folder"
               Print #3, ";[Options]"
               Print #3, ";TimeOut          The number in seconds for the application to time out.      Default 30"
               Print #3, ";HostName         the host to connect to                                      Default none"
               Print #3, ";Port             the port to connect to the host on default 21 for basic FTP Default 21"
               Print #3, ";PassiveTransfers allow the use of passive file transfers                     Default 0 passive mode off"
               Print #3, ";UserID           the user name with permissions on the ftp site              Default none"
               Print #3, ";Password         The users password                                          Default none"
               Print #3, ";"
               Print #3, ";                 If both the UserID and Password are blank the application will attempt to logon annonymously"
               Print #3, ";"
               Print #3, ";FtpMode          FtpMode=SFTP (ssh ftp mode)                                 Default FTP"
               Print #3, ";                 FtpMode=FTPS (ssl ftp mode)"
               Print #3, ";                 FtpMode=FTP  (basic ftp mode)"
               Print #3, ";"
               Print #3, ";Upload           1 or blank Indicates that the application will need to upload files to the remote server    Default 0 no upload"
               Print #3, ";Download         1 or blank Indicates that the application will need to download files from the remote server    Default 0 no download"
               Print #3, ";"
               Print #3, ";Retries          The number of times to retry an upload or download before the application reports an error Default is 1"
               Print #3, ";"
               Print #3, ";[UploadOptions] And [DownloadOptions]"
               Print #3, "; NOTE: If the file list is populated then the application will perform the operation regardless of any other configuration options"
               Print #3, ";"
               Print #3, ";UseFileList      1- Look up files in UploadFiles or DownloadFiles Section to get the list of files to transfer"
               Print #3, ";"
               Print #3, ";DeleteFiles      1- delete the files from the source after successfull upload    Default (0) not to delete any files (not supported yet)"
               Print #3, ";"
               Print #3, ";NOTE: If any of the following options are blank then the application will not perform the requested action"
               Print #3, ";FolderFrom       The folder name to upload from or download from used with FileSpec option "
               Print #3, ";FolderTo         The folder name to upload to or download to"
               Print #3, ";FileSpec         The file extension to filter by default is "",  *.* = all files *.txt all files with a .txt extension"
               Print #3, ";"
               Print #3, ";"
               Print #3, ";***************************************************************************************************************"
               Print #3, "[Options]"
               Print #3, "TimeOut=45"
               Print #3, "HostName=172.20.144.90"
               Print #3, "Port=21"
               Print #3, "PassiveTransfers=1"
               Print #3, "UserID=pollftp"
               Print #3, "Password=pollftp"
               Print #3, "FtpMode=FTP"
               Print #3, "Upload=1"
               Print #3, "Download="
               Print #3, "Retries=2"
               Print #3, ""
               Print #3, ""
               Print #3, "[UploadOptions]"
               Print #3, "UseFileList=0"
               Print #3, "DeleteFiles=0"
               Print #3, "FolderFrom=C:\Program Files\xpient solutions\psiExporter\Out\"
               Print #3, "FolderTo=\"
               Print #3, FileSpec
               Print #3, ""
               Print #3, ""
               Print #3, ";********************************************************************************************************************"
               Print #3, "; NOTE: The list of files may or maynot contain the directory path of the file. If not specified then the application"
               Print #3, ";       will use the FolderFrom key as the path to the file. If the file does not exist then the application will"
               Print #3, ";       not even attempt to copy it. If the FolderTo key is blank then the application will not be able to copy the "
               Print #3, ";       file."
               Print #3, ";********************************************************************************************************************"
               Print #3, "[UploadFiles]"
               Print #3, ""
               Print #3, "[DownloadOptions]"
               Print #3, "UseFileList=0"
               Print #3, "DeleteFiles=0"
               Print #3, "FolderFrom="
               Print #3, "FolderTo="
               Print #3, "FileSpec="
               Print #3, ""
               Print #3, ""
               Print #3, ";********************************************************************************************************************"
               Print #3, "; NOTE: The list of files may or maynot contain the directory path of the file. If not specified then the application"
               Print #3, ";       will use the FolderFrom key as the path to the file."
               Print #3, ";       If the FolderTo key is blank then the application will not be able to copy the file."
               Print #3, ";********************************************************************************************************************"
               Print #3, "[DownloadFiles]"
        Close #3


    INfile = "S:\Hardees\IRIS\FTP\FTPtoBNE\" + RESTNUM + ".bbb"
    FileSpec1 = "IF EXIST " + LTrim(RESTNUM) + ".IRS copy " + LTrim(RESTNUM) + ".IRS + IRIS.IRS " + LTrim(RESTNUM) + ".IRS"
    FileSpec2 = "IF NOT EXIST " + LTrim(RESTNUM) + ".IRS COPY IRIS.IRS " + LTrim(RESTNUM) + ".IRS"
    FileSpec3 = "DEL " + LTrim(RESTNUM) + ".IRS"
        Open INfile For Output As #3
               Print #3, "REM BNE 5/15/2012"
               Print #3, "@echo off"
               Print #3, "c:"
               Print #3, "cd " + Chr(34) + "c:\program files\xpient solutions\psiexporter\out\" + Chr(34)
               Print #3, FileSpec1
               Print #3, ""
               Print #3, FileSpec2
               Print #3, ""
               Print #3, "rem We leave IRIS.IRS alone and let LC check for existence of file in \POLL folder.  If exists, it is deleted - otherwise it transfers before deleting"
               Print #3, ""
               Print #3, "rem 2 Step Process - Step1 sends to FTPPoll folder, Step2 sends to AS400 and deletes poll file if successful"
               Print #3, ""
               Print #3, "rem ftp Poll file to POLL folder."
               Print #3, ""
               Print #3, "cd " + Chr(34) + "C:\Program Files\xpient solutions\PSIFTPClient" + Chr(34)
               Print #3, Chr(34) + "C:\Program Files\xpient solutions\PSIFTPClient\PsiFTPClient" + Chr(34) + " /FILE C:\Program Files\xpient solutions\PSIFTPClient\FTPPOLL.ini /q"
               Print #3, ""
               'Print #3, "rem ftp Poll file to AS400 folder."
               'Print #3, ""
               'Print #3, "cd " + Chr(34) + "C:\Program Files\xpient solutions\PSIFTPClient" + Chr(34)
               'Print #3, Chr(34) + "C:\Program Files\xpient solutions\PSIFTPClient\PsiFTPClient" + Chr(34) + " /FILE C:\Program Files\xpient solutions\PSIFTPClient\FTPAS400.ini /q"
               'Print #3, ""
               Print #3, "cd " + Chr(34) + "c:\program files\xpient solutions\psiexporter\out\" + Chr(34)
               Print #3, FileSpec3
               Print #3, "exit"
        Close #3
Loop
Close #1
End
End Sub
