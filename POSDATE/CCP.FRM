VERSION 4.00
Begin VB.Form CCP 
   Caption         =   "Manual Reset"
   ClientHeight    =   4620
   ClientLeft      =   315
   ClientTop       =   1575
   ClientWidth     =   8475
   Height          =   5025
   Left            =   255
   LinkTopic       =   "Form2"
   ScaleHeight     =   4620
   ScaleWidth      =   8475
   Top             =   1230
   Width           =   8595
   Begin VB.CommandButton GotReport 
      Caption         =   "Report Printed"
      BeginProperty Font 
         name            =   "Times New Roman"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1080
      TabIndex        =   1
      Top             =   3720
      Width           =   2415
   End
   Begin VB.CommandButton NoReport 
      Caption         =   "Report Did Not Print"
      BeginProperty Font 
         name            =   "Times New Roman"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4320
      TabIndex        =   0
      Top             =   3720
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   $"CCP.frx":0000
      BeginProperty Font 
         name            =   "Times New Roman"
         charset         =   1
         weight          =   400
         size            =   14.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   8055
   End
End
Attribute VB_Name = "CCP"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub GotReport_Click()
    CCP.Hide
    Form1.Show

End Sub

Private Sub NoReport_Click()
    TroubleText = "The system has not generated a reset report !!"
    Load Trouble
    CCP.Hide
    Trouble.Show
End Sub
