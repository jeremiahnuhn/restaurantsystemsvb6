VERSION 5.00
Begin VB.Form frmMain 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   0  'None
   ClientHeight    =   6795
   ClientLeft      =   105
   ClientTop       =   105
   ClientWidth     =   10485
   LinkTopic       =   "Form1"
   ScaleHeight     =   6795
   ScaleWidth      =   10485
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Label LblMain 
      Alignment       =   2  'Center
      Caption         =   "The payroll program is currently running. This will take a few minutes to complete. No assistance is required."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   3120
      TabIndex        =   0
      Top             =   2280
      Width           =   6495
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim RestNum As String
Dim errfile As String
Public Errorcounter As Integer
Dim TimeCounter As Integer
Dim Employee_Seq As Integer
Dim Store_Id As Variant
Dim Tips_Mask_TTL As String 'VARIABLE THAT FORMATS THE TIP OUTPUT AS A PERCENT
Dim Tips_mask_DLY As String 'VARIABLE THAT FORMATS THE TIP OUTPUT AS A PERCENT
'SALES VARIABLES (TOTAL)
Dim NetSalesTTL As Currency
Dim ServiceChgTTL As Currency
Dim CreditTTL As Currency
Dim GrosRcptsTTL As Currency
Dim ChgdRcptsTTL As Currency
Dim ChgdTipsTTL As Currency
Dim TipsSvcTTL As Currency
Dim TipsPdTTL As Currency
Dim TipsDcldTTL As Currency
Dim TipPctTTL As Single
'SALES VARIABLES (DAILY)
Dim NetSalesDLY As Currency
Dim ServiceChgDLY As Currency
Dim CreditDLY As Currency
Dim GrosRcptDLY As Currency
Dim ChgdRcptsDLY As Currency
Dim ChgdTipsDLY As Currency
Dim TipsSvcDLY As Currency
Dim TipsPdDLY As Currency
Dim TipsDcldDLY As Currency
Dim TipPctDLY As Single
Private Type Job_Code_8
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
    Job_6 As Variant
    Job_7 As Variant
    job_8 As Variant
End Type
Dim jobcode8 As Job_Code_8
Private Type Job_Code_7
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
    Job_6 As Variant
    Job_7 As Variant
End Type
Dim jobcode7 As Job_Code_7
Private Type Job_Code_6
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
    Job_6 As Variant
End Type
Dim jobcode6 As Job_Code_6
Private Type Job_Code_5
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
    Job_5 As Variant
End Type
Dim jobcode5 As Job_Code_5
Private Type Job_Code_4
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
    Job_4 As Variant
End Type
Dim jobcode4 As Job_Code_4
Private Type Job_Code_3
    Job_1 As Variant
    Job_2 As Variant
    Job_3 As Variant
End Type
Dim jobcode3 As Job_Code_3
Private Type Job_Code_2
    Job_1 As Variant
    Job_2 As Variant
End Type
Dim jobcode2 As Job_Code_2
Private Type Job_Code_1
    Job_1 As Variant
End Type
Dim jobcode1 As Job_Code_1
'TEMP VARIABLES
Dim RecordCnt As Long
Dim Last_Name As String * 16
Dim First_Name As String * 8
Dim Payroll_Id As String
Dim Emp_Num As String
Dim TmpJob As Integer
Dim TmpReg As Single
Dim TmpOvt As Single
Dim TmpRegRate As Currency
Dim TmpOvtRate As Currency
Dim TmpDefRegRate As Currency
Dim TmpDefOvtRate As Currency
'JOB CODE VARIABLES
Dim Job1 As Variant
Dim Job2 As Variant
Dim Job3 As Variant
Dim Job4 As Variant
Dim Job5 As Variant
Dim Job6 As Variant
Dim Job7 As Variant
Dim Job8 As Variant
'JOB REG HOURS VARIABLES
Dim Job1Reg As Single
Dim Job2Reg As Single
Dim Job3Reg As Single
Dim Job4Reg As Single
Dim Job5Reg As Single
Dim Job6Reg As Single
Dim Job7Reg As Single
Dim Job8Reg As Single
'JOB OVT HOURS VARIABLES
Dim Job1Ovt As Single
Dim Job2Ovt As Single
Dim Job3Ovt As Single
Dim Job4Ovt As Single
Dim Job5Ovt As Single
Dim Job6Ovt As Single
Dim Job7Ovt As Single
Dim Job8Ovt As Single
'JOB REG HOURS VARIABLES
Dim Job1Reg_DLY As Single
Dim Job2Reg_DLY As Single
Dim Job3Reg_DLY As Single
Dim Job4Reg_DLY As Single
Dim Job5Reg_DLY As Single
Dim Job6Reg_DLY As Single
Dim Job7Reg_DLY As Single
Dim Job8Reg_DLY As Single
'JOB OVT HOURS VARIABLES
Dim Job1Ovt_DLY As Single
Dim Job2Ovt_DLY As Single
Dim Job3Ovt_DLY As Single
Dim Job4Ovt_DLY As Single
Dim Job5Ovt_DLY As Single
Dim Job6Ovt_DLY As Single
Dim Job7Ovt_DLY As Single
Dim Job8Ovt_DLY As Single
'JOB REG RATE VARIABLES
Dim RegRate1 As Currency
Dim RegRate2 As Currency
Dim RegRate3 As Currency
Dim RegRate4 As Currency
Dim RegRate5 As Currency
Dim RegRate6 As Currency
Dim RegRate7 As Currency
Dim RegRate8 As Currency
'JOB OVT RATE VARIABLES
Dim OvtRate1 As Currency
Dim OvtRate2 As Currency
Dim OvtRate3 As Currency
Dim OvtRate4 As Currency
Dim OvtRate5 As Currency
Dim OvtRate6 As Currency
Dim OvtRate7 As Currency
Dim OvtRate8 As Currency
'FORMAT VARIABLES FOR THE OUTPUT FILE
Dim RegRate1_Mask As String * 8
Dim RegRate2_Mask As String * 8
Dim RegRate3_Mask As String * 8
Dim RegRate4_Mask As String * 8
Dim RegRate5_Mask As String * 8
Dim RegRate6_Mask As String * 8
Dim RegRate7_Mask As String * 8
Dim RegRate8_Mask As String * 8
Dim OvtRate1_Mask As String * 8
Dim OvtRate2_Mask As String * 8
Dim OvtRate3_Mask As String * 8
Dim OvtRate4_Mask As String * 8
Dim OvtRate5_Mask As String * 8
Dim OvtRate6_Mask As String * 8
Dim OvtRate7_Mask As String * 8
Dim OvtRate8_Mask As String * 8
Dim x As Integer

Private Sub Form_Load()
Errorcounter = 0
    
    frmMain.MousePointer = 11
    Dim Connection As ADODB.Connection
    Set Connection = New ADODB.Connection
    Dim Update As New ADODB.Command             'UPDATE CMD LINE
    
    Dim OpenStoreId As New ADODB.Command        'EXTRACT THE STORE ID CMD
    Dim StoreId As New ADODB.Recordset          'EXTRACT THE STORE ID RS
    
    Dim OpenViewTime As New ADODB.Command       'EXTRACT TIME RS CMD
    Dim ViewRsTime As New ADODB.Recordset       'TIME DATA RS
    
    Dim OpenViewSales As New ADODB.Command      'EXTRACT SALES RS CMD
    Dim ViewRsSales As New ADODB.Recordset      'SALES DATA RS
    
    Dim ViewJobCodeCount As New ADODB.Recordset 'THE JOBCODE RECORDSET
    Dim OpenJobCodeCount As New ADODB.Command   'THE JOBCODE COMMAND
    
    Dim ViewJobCodeArray As New ADODB.Recordset 'JOB CODE RECORDSET
    Dim OpenJobCodeArray As New ADODB.Command   'EXTRACT JOB CODES COMMAND
    
    Dim OpenDailySales As New ADODB.Command     'EXTRACT THE DAILY SALES COMMAND
    Dim DailySales As New ADODB.Recordset       'EXTRACT THE DAILY SALES RECORDSET
    
    Dim OpenDailyHours As New ADODB.Command     'EXTRACT THE DAILY HOURS
    Dim DailyHours As New ADODB.Recordset       'EXTRACT THE DAILY HOURS RECORDSET
    
    Dim OpenTimeCount As New ADODB.Command      'COUNT THE EMPLOYEES TIME RECORDS
    Dim TimeCount As New ADODB.Recordset
    
    'SET THE CONNECT STRING
    Connection.ConnectionString = ("DSN=micros;UID=support;PWD=support")
    'OPEN CONNECTION AS NEW ODBC CONNECTION
    Connection.Open
    'SET THE ACTIVE CONNECTION FOR UPDATE
    Update.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR TIME
    OpenViewTime.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR SALES
    OpenViewSales.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR JOBCODE
    OpenJobCodeCount.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR THE JOB CODE ARRAY
    OpenJobCodeArray.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR THE STORE ID
    OpenStoreId.ActiveConnection = Connection
    'SET THE ACTIVECONNECTION FOR THE DAILY SALES
    OpenDailySales.ActiveConnection = Connection
    'SET THE ACTIVE CONNECTION FOR THE DAILY HOURS
    OpenDailySales.ActiveConnection = Connection
    'SET THE CONNECTION FOR THE DAILY SALES
    OpenDailyHours.ActiveConnection = Connection
    'SET THE CONNECTION FOR TIME RECORD COUNT
    OpenTimeCount.ActiveConnection = Connection
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS A STORED PROCEDURE. IT WILL EXECUTE THE UPDATE THE TTLS TABLES
    Update.CommandType = adCmdStoredProc
    Update.CommandText = "micros.sp_postall"
    Update.Execute
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE TIME VIEW FOR ONE WEEK OF DATA
   ' OpenViewTime.CommandText = "select employee_number, last_name, first_name, payroll_id , job_number, override_regular_rate, override_overtime_rate, regular_hours, default_regular_rate, default_overtime_rate, overtime_hours from micros.v_r_employee_time_card where labor_week = (select max(labor_week)from micros.time_card_dtl) order by employee_number"   Old Line
    OpenViewTime.CommandText = "select employee_number, last_name, first_name, payroll_id , job_number, override_regular_rate, regular_hours, default_regular_rate, overtime_hours from micros.v_r_employee_time_card where v_r_employee_time_card.business_date > '2004/01/05' and v_r_employee_time_card.business_date < '2004/01/13' order by employee_number"    ' New Line
    OpenViewTime.CommandType = adCmdText
    'CREATE THE RECORDSET FOR TIME DATA
    Set ViewRsTime = OpenViewTime.Execute
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS A STORED PROCEDURE. IT WILL GET THE STORE ID
    OpenStoreId.CommandText = "select location_name_1 from micros.rest_def"
    OpenStoreId.CommandType = adCmdText
    'CREATE THE RECORDSET FOR TIME DATA
    Set StoreId = OpenStoreId.Execute
    Store_Id = StoreId(0)

    'OPEN THE OUTPUT FILE
    'Replace output file "C:\time_rec.dat" with "C:\TexRock\Time_rec.dat"
    Open "C:\TexRock\time_rec.dat" For Output As #1 Len = 100
    Counter = 0
frmMain.Show
For x = 1 To 1000
   frmMain.Show
   DoEvents
Next x
Do While Not ViewRsTime.EOF
    
        Job1Reg = 0
        Job2Reg = 0
        Job3Reg = 0
        Job4Reg = 0
        Job5Reg = 0
        Job6Reg = 0
        Job7Reg = 0
        Job8Reg = 0
        Job1Ovt = 0
        Job2Ovt = 0
        Job3Ovt = 0
        Job4Ovt = 0
        Job5Ovt = 0
        Job6Ovt = 0
        Job7Ovt = 0
        Job8Ovt = 0
        
        Job1Reg_DLY = 0
        Job2Reg_DLY = 0
        Job3Reg_DLY = 0
        Job4Reg_DLY = 0
        Job5Reg_DLY = 0
        Job6Reg_DLY = 0
        Job7Reg_DLY = 0
        Job8Reg_DLY = 0
        Job1Ovt_DLY = 0
        Job2Ovt_DLY = 0
        Job3Ovt_DLY = 0
        Job4Ovt_DLY = 0
        Job5Ovt_DLY = 0
        Job6Ovt_DLY = 0
        Job7Ovt_DLY = 0
        Job8Ovt_DLY = 0
        
        RSet RegRate1_Mask = "0.00"
        RSet RegRate2_Mask = "0.00"
        RSet RegRate3_Mask = "0.00"
        RSet RegRate4_Mask = "0.00"
        RSet RegRate5_Mask = "0.00"
        RSet RegRate6_Mask = "0.00"
        RSet RegRate7_Mask = "0.00"
        RSet RegRate8_Mask = "0.00"
        RSet OvtRate1_Mask = "0.00"
        RSet OvtRate2_Mask = "0.00"
        RSet OvtRate3_Mask = "0.00"
        RSet OvtRate4_Mask = "0.00"
        RSet OvtRate5_Mask = "0.00"
        RSet OvtRate6_Mask = "0.00"
        RSet OvtRate7_Mask = "0.00"
        RSet OvtRate8_Mask = "0.00"
    
    'INITIALIZE THE JOB CODES
    jobcode_1 = 0
    jobcode_2 = 0
    jobcode_3 = 0
    jobcode_4 = 0
    jobcode_5 = 0
    jobcode_6 = 0
    jobcode_7 = 0
    jobcode_8 = 0

    NetSalesTTL = 0
    ServiceChgTTL = 0
    CreditTTL = 0
    GrosRcptsTTL = 0
    ChgdRcptsTTL = 0
    ChgdTipsTTL = 0
    TipsSvcTTL = 0
    TipsPdTTL = 0
    TipsDcldTTL = 0
    TipPctTTL = 0
    Tips_Mask_TTL = "0.00%"
    
    Job1Reg_DLY = 0
    Job2Reg_DLY = 0
    Job3Reg_DLY = 0
    Job4Reg_DLY = 0
    Job5Reg_DLY = 0
    Job6Reg_DLY = 0
    Job7Reg_DLY = 0
    Job8Reg_DLY = 0
    Job1Ovt_DLY = 0
    Job2Ovt_DLY = 0
    Job3Ovt_DLY = 0
    Job4Ovt_DLY = 0
    Job5Ovt_DLY = 0
    Job6Ovt_DLY = 0
    Job7Ovt_DLY = 0
    Job8Ovt_DLY = 0
    
    NetSalesDLY = 0
    ServiceChgDLY = 0
    CreditDLY = 0
    GrosRcptsDLY = 0
    ChgdRcptsDLY = 0
    ChgdTipsDLY = 0
    TipsSvcDLY = 0
    TipsPdDLY = 0
    TipsDcldDLY = 0
    TipPctDLY = 0
    Tips_mask_DLY = "0.00%"
    
    
On Error GoTo error_handler
    'MOVE THE VIEW TO VARIABLES
    Emp_Num = ViewRsTime("employee_number")
    Last_Name = ViewRsTime("last_name")
    First_Name = ViewRsTime("first_name")
    If ViewRsTime("payroll_id") > 0 Then
         Payroll_Id = ViewRsTime("payroll_id")
    Else
         Payroll_Id = "000000000"
    End If
    TmpJob = ViewRsTime("job_number")
    TmpRegRate = ViewRsTime("override_regular_rate")
    'TmpOvtRate = ViewRsTime("override_overtime_rate")
    TmpDefRegRate = ViewRsTime("default_regular_rate")
    'TmpDefOvtRate = ViewRsTime("default_overtime_rate")
    
    'OPEN THE TIME COUNT RECORDSET
    OpenTimeCount.CommandText = "Select COUNT(DISTINCT time_card_seq) from micros.v_r_employee_time_card Where employee_number =" & Emp_Num & "and v_r_employee_time_card.business_date >= '2004/01/06' and v_r_employee_time_card.business_date <= '2004/01/12'"
    OpenTimeCount.CommandType = adCmdText
    'CREATE THE RECORDSET FOR THE TIME RECORD COUNT
    Set TimeCount = OpenTimeCount.Execute
    'VARIABLE HOLDING THE NUMBER OF DISTIMCT TIME RECORDS FOR EACH EMPLOYEE
    TimeCounter = TimeCount(0)
              
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE RECORD COUNT VIEW
    OpenJobCodeCount.CommandText = "Select COUNT(DISTINCT job_number) from micros.v_R_employee_time_card Where v_r_employee_time_card.employee_number = " & Emp_Num & " and v_r_employee_time_card.business_date >= '2004/01/06' and v_r_employee_time_card.business_date <= '2004/01/12'"
    OpenJobCodeCount.CommandType = adCmdText
    
    'CREATE THE RECORDSET FOR THE JOB CODE COUNT
    Set ViewJobCodeCount = OpenJobCodeCount.Execute
    RecordCnt = ViewJobCodeCount(0) 'NUMBER OF JOB CODES STORED HERE

    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE RECORD ARRAY VIEW
    OpenJobCodeArray.CommandText = "Select DISTINCT job_number from micros.v_R_employee_time_card where employee_number = " & Emp_Num & " and v_r_employee_time_card.business_date >= '2004/01/06' and v_r_employee_time_card.business_date <= '2004/01/12'"
    OpenJobCodeArray.CommandType = adCmdText
    'CREATE THE RECORDSET FOR THE JOB CODE ARRAY
    'Set ViewJobCodeArray = OpenJobCodeArray.Execute
    
    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE SALES VIEW FOR SEVEN DAYS
    OpenViewSales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) >= '2004/01/06' and date(start_time) <= '2004/01/12' order by emp_seq"
    OpenViewSales.CommandType = adCmdText
    'CREATE THE RECORDSET FOR SALES DATA
    Set ViewRsSales = OpenViewSales.Execute
    
    Employee_Seq = -1   'INITIALIZE THIS VARIABLE
    'GATHER THE TOTAL SALES
    If Not ViewRsSales.EOF Then
    
        NetSalesTTL = ViewRsSales(1)
        ServiceChgTTL = ViewRsSales(2)
        GrosRcptsTTL = ViewRsSales(3)
        ChgdRcptsTTL = ViewRsSales(4)
        ChgdTipsTTL = ViewRsSales(5)
        TipsSvcTTL = ViewRsSales(6)
        TipsPdTTL = ViewRsSales(7)
        TipsDcldTTL = ViewRsSales(8)
        
        ViewRsSales.MoveNext

        Do While Not ViewRsSales.EOF
        
            Employee_Seq = (0)
            NetSalesTTL = ViewRsSales(1) + NetSalesTTL
            ServiceChgTTL = ViewRsSales(2) + ServiceChgTTL
            GrosRcptsTTL = ViewRsSales(3) + GrosRcptsTTL
            ChgdRcptsTTL = ViewRsSales(4) + ChgdRcptsTTL
            ChgdTipsTTL = ViewRsSales(5) + ChgdTipsTTL
            TipsSvcTTL = ViewRsSales(6) + TipsSvcTTL
            TipsPdTTL = ViewRsSales(7) + TipsPdTTL
            TipsDcldTTL = ViewRsSales(8) + TipsDcldTTL
        
            ViewRsSales.MoveNext
        Loop
    End If
    If GrosRcptsTTL > 0 Then
        TipPctTTL = TipsDcldTTL / GrosRcptsTTL
        Tips_Mask_TTL = Format(TipPctTTL, "percent")    'STRING VARIABLE THAT STORES THE TIPS PERCENT (DAILY)
    End If


    'SET THE COMMANDS TEXT AND SPECIFY THAT IT
    'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY SALES CALL
    OpenDailySales.CommandText = "select emp_seq, net_sls_ttl, service_chg_ttl, gross_rcpts_ttl, chgd_rcpts_ttl, chgd_tips_ttl, tip_svc_ttl, tips_paid_ttl, tips_decl_ttl from micros.v_r_employee_sales_tips where employee_number = " & Emp_Num & "and date(start_time) >= '2004/01/06' and date(start_time) <= '2004/01/12'"
    OpenDailySales.CommandType = adCmdText
    'CREATE THE RECORDSET FOR THE DAILY SALES
    Set DailySales = OpenDailySales.Execute
       
    If Not DailySales.EOF Then
    
        NetSalesDLY = DailySales(1)
        ServiceChgDLY = DailySales(2)
        GrosRcptsDLY = DailySales(3)
        ChgdRcptsDLY = DailySales(4)
        ChgdTipsDLY = DailySales(5)
        TipsSvcDLY = DailySales(6)
        TipsPdDLY = DailySales(7)
        TipsDcldDLY = DailySales(8)
    
    End If

    If GrosRcptsDLY > 0 Then
        TipPctDLY = TipsDcldDLY / GrosRcptsDLY
        Tips_mask_DLY = Format(TipPctDLY, "percent")    'STRING VARIABLE THAT STORES THE TIPS PERCENT (DAILY)
    End If
    
    If RecordCnt > 0 Then   'THE EMPLOYEE IS VIABLE IN THE SYSTEM
        If RecordCnt = 8 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode8.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_5 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_6 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.Job_7 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode8.job_8 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 7 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode7.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_5 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_6 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode7.Job_7 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 6 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode6.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_5 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode6.Job_6 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 5 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode5.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_4 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode5.Job_5 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 4 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode4.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode4.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode4.Job_3 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode4.Job_4 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 3 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode3.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode3.Job_2 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode3.Job_3 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 2 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode2.Job_1 = ViewJobCodeArray(0)
            ViewJobCodeArray.MoveNext
            jobcode2.Job_2 = ViewJobCodeArray(0)
        ElseIf RecordCnt = 1 Then
        Set ViewJobCodeArray = OpenJobCodeArray.Execute 'GET THE ARRAY OF JOB CODES VIEW
            jobcode1.Job_1 = ViewJobCodeArray(0)
        End If
    End If
        
        If jobcode1.Job_1 > 0 Then
             jobcode_1 = jobcode1.Job_1
             jobcode1.Job_1 = 0
        Else: jobcode1.Job_1 = 0
        End If
        If jobcode2.Job_1 > 0 Then
             jobcode_1 = jobcode2.Job_1
             jobcode2.Job_1 = 0
        Else: jobcode2.Job_1 = 0
        End If
        If jobcode3.Job_1 > 0 Then
             jobcode_1 = jobcode3.Job_1
             jobcode3.Job_1 = 0
        Else: jobcode3.Job_1 = 0
        End If
        If jobcode4.Job_1 > 0 Then
             jobcode_1 = jobcode4.Job_1
             jobcode4.Job_1 = 0
        Else: jobcode4.Job_1 = 0
        End If
        If jobcode5.Job_1 > 0 Then
             jobcode_1 = jobcode5.Job_1
             jobcode5.Job_1 = 0
        Else: jobcode5.Job_1 = 0
        End If
        If jobcode6.Job_1 > 0 Then
             jobcode_1 = jobcode6.Job_1
             jobcode6.Job_1 = 0
        Else: jobcode6.Job_1 = 0
        End If
        If jobcode7.Job_1 > 0 Then
             jobcode_1 = jobcode7.Job_1
             jobcode7.Job_1 = 0
        Else: jobcode7.Job_1 = 0
        End If
        If jobcode8.Job_1 > 0 Then
             jobcode_1 = jobcode8.Job_1
             jobcode8.Job_1 = 0
        Else: jobcode8.Job_1 = 0
        End If
        
        If jobcode2.Job_2 > 0 Then
             jobcode_2 = jobcode2.Job_2
             jobcode2.Job_2 = 0
        Else: jobcode2.Job_2 = 0
        End If
        If jobcode3.Job_2 > 0 Then
             jobcode_2 = jobcode3.Job_2
             jobcode3.Job_2 = 0
        Else: jobcode3.Job_2 = 0
        End If
        If jobcode4.Job_2 > 0 Then
             jobcode_2 = jobcode4.Job_2
             jobcode4.Job_2 = 0
        Else: jobcode4.Job_2 = 0
        End If
        If jobcode5.Job_2 > 0 Then
             jobcode_2 = jobcode5.Job_2
             jobcode5.Job_2 = 0
        Else: jobcode5.Job_2 = 0
        End If
        If jobcode6.Job_2 > 0 Then
             jobcode_2 = jobcode6.Job_2
             jobcode6.Job_2 = 0
        Else: jobcode6.Job_2 = 0
        End If
        If jobcode7.Job_2 > 0 Then
             jobcode_2 = jobcode7.Job_2
             jobcode7.Job_2 = 0
        Else: jobcode7.Job_2 = 0
        End If
        If jobcode8.Job_2 > 0 Then
             jobcode_2 = jobcode8.Job_2
             jobcode8.Job_2 = 0
        Else: jobcode8.Job_2 = 0
        End If
        
        If jobcode3.Job_3 > 0 Then
             jobcode_3 = jobcode3.Job_3
             jobcode3.Job_3 = 0
        Else: jobcode3.Job_3 = 0
        End If
        If jobcode4.Job_3 > 0 Then
             jobcode_3 = jobcode4.Job_3
             jobcode4.Job_3 = 0
        Else: jobcode4.Job_3 = 0
        End If
        If jobcode5.Job_3 > 0 Then
             jobcode_3 = jobcode5.Job_3
             jobcode5.Job_3 = 0
        Else: jobcode5.Job_3 = 0
        End If
        If jobcode6.Job_3 > 0 Then
             jobcode_3 = jobcode6.Job_3
             jobcode6.Job_3 = 0
        Else: jobcode6.Job_3 = 0
        End If
        If jobcode7.Job_3 > 0 Then
             jobcode_3 = jobcode7.Job_3
             jobcode7.Job_3 = 0
        Else: jobcode7.Job_3 = 0
        End If
        If jobcode8.Job_3 > 0 Then
             jobcode_3 = jobcode8.Job_3
             jobcode8.Job_3 = 0
        Else: jobcode8.Job_3 = 0
        End If
        
        If jobcode4.Job_4 > 0 Then
             jobcode_4 = jobcode4.Job_4
             jobcode4.Job_4 = 0
        Else: jobcode4.Job_4 = 0
        End If
        If jobcode5.Job_4 > 0 Then
             jobcode_4 = jobcode5.Job_4
             jobcode5.Job_4 = 0
        Else: jobcode5.Job_4 = 0
        End If
        If jobcode6.Job_4 > 0 Then
             jobcode_4 = jobcode6.Job_4
             jobcode6.Job_4 = 0
        Else: jobcode6.Job_4 = 0
        End If
        If jobcode7.Job_4 > 0 Then
             jobcode_4 = jobcode7.Job_4
             jobcode7.Job_4 = 0
        Else: jobcode7.Job_4 = 0
        End If
        If jobcode8.Job_4 > 0 Then
             jobcode_4 = jobcode8.Job_4
             jobcode8.Job_4 = 0
        Else: jobcode8.Job_4 = 0
        End If
    
        If jobcode5.Job_5 > 0 Then
             jobcode_5 = jobcode5.Job_5
             jobcode5.Job_5 = 0
        Else: jobcode5.Job_5 = 0
        End If
        If jobcode6.Job_5 > 0 Then
             jobcode_5 = jobcode6.Job_5
             jobcode6.Job_5 = 0
        Else: jobcode6.Job_5 = 0
        End If
        If jobcode7.Job_5 > 0 Then
             jobcode_5 = jobcode7.Job_5
             jobcode7.Job_5 = 0
        Else: jobcode7.Job_5 = 0
        End If
        If jobcode8.Job_5 > 0 Then
             jobcode_5 = jobcode8.Job_5
             jobcode8.Job_5 = 0
        Else: jobcode8.Job_5 = 0
        End If
        
        If jobcode6.Job_6 > 0 Then
             jobcode_6 = jobcode6.Job_6
             jobcode6.Job_6 = 0
        Else: jobcode6.Job_6 = 0
        End If
        If jobcode7.Job_6 > 0 Then
             jobcode_6 = jobcode7.Job_6
             jobcode7.Job_6 = 0
        Else: jobcode7.Job_6 = 0
        End If
        If jobcode8.Job_6 > 0 Then
             jobcode_6 = jobcode8.Job_6
             jobcode8.Job_6 = 0
        Else: jobcode8.Job_6 = 0
        End If
        
        If jobcode7.Job_7 > 0 Then
             jobcode_7 = jobcode7.Job_7
             jobcode7.Job_7 = 0
        Else: jobcode7.Job_7 = 0
        End If
        If jobcode8.Job_7 > 0 Then
             jobcode_7 = jobcode8.Job_7
             jobcode8.Job_7 = 0
        Else: jobcode8.Job_7 = 0
        End If
        
        If jobcode8.job_8 > 0 Then
             jobcode_8 = jobcode8.job_8
             jobcode8.job_8 = 0
        Else: jobcode8.job_8 = 0
        End If
        
        
     'End If
            
        Do While TimeCounter <> 0
            
          TmpReg = ViewRsTime("regular_hours")
          TmpOvt = ViewRsTime("Overtime_hours")
            
        'PLACES THE NUMBER OF HOURS IN THE PROPER TIME BUCKET
        If ViewRsTime("job_number") = jobcode_1 Then
                                            RegRate1 = 0
                                            OvtRate1 = 0
                                            Job1Reg = Job1Reg + TmpReg
                                            Job1Ovt = Job1Ovt + TmpOvt
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate1 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate1 = ViewRsTime("default_regular_rate")
                                            End If
                                         '   If ViewRsTime("override_overtime_rate") > 0 Then
                                         '      OvtRate1 = ViewRsTime("override_overtime_rate")
                                         '   Else
                                         '      OvtRate1 = ViewRsTime("default_overtime_rate")
                                         '   End If
                                            'RegRate1 = ViewRsTime("override_regular_rate")
                                            OvtRate1 = RegRate1 * 1.5
                                            TmpReg = 0
                                            RSet RegRate1_Mask = CStr(Format(RegRate1, "#.00"))
                                            RSet OvtRate1_Mask = CStr(Format(OvtRate1, "#.00"))
                                            Job1Reg_DLY = 0
                                            Job1Ovt_DLY = 0
                                            
                                            'If Job1Ovt > 0 Then
                                                'Job1Reg = 40
                                            'End If
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY REGULAR HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_1
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job1Reg_DLY = DailyHours(0)
                                            End If
                        
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_1
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job1Ovt_DLY = DailyHours(0)
                                            End If
            
        ElseIf ViewRsTime("job_number") = jobcode_2 Then
                                                RegRate2 = 0
                                                OvtRate2 = 0
                                                Job2Reg = Job2Reg + TmpReg
                                                Job2Ovt = Job2Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate2 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate2 = ViewRsTime("default_regular_rate")
                                            End If
                                         '   If ViewRsTime("override_overtime_rate") > 0 Then
                                         '      OvtRate2 = ViewRsTime("override_overtime_rate")
                                         '   Else
                                        '       OvtRate2 = ViewRsTime("default_overtime_rate")
                                        '    End If
                                                'RegRate2 = ViewRsTime("override_regular_rate")
                                                OvtRate2 = RegRate2 * 1.5
                                                TmpReg = 0
                                                RSet RegRate2_Mask = CStr(Format(RegRate2, "#.00"))
                                                RSet OvtRate2_Mask = CStr(Format(OvtRate2, "#.00"))
                                                Job2Reg_DLY = 0
                                                Job2Ovt_DLY = 0
        
                                            'If Job2Ovt > 0 Then
                                                'Job2Reg = 40
                                            'End If
        
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_2
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job2Reg_DLY = DailyHours(0)
                                            End If
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_2
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job2Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_3 Then
                                                RegRate3 = 0
                                                OvtRate3 = 0
                                                Job3Reg = Job3Reg + TmpReg
                                                Job3Ovt = Job3Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate3 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate3 = ViewRsTime("default_regular_rate")
                                            End If
                                         '   If ViewRsTime("override_overtime_rate") > 0 Then
                                         '      OvtRate3 = ViewRsTime("override_overtime_rate")
                                         '   Else
                                         '      OvtRate3 = ViewRsTime("default_overtime_rate")
                                         '   End If
                                                'RegRate3 = ViewRsTime("override_regular_rate")
                                                OvtRate3 = RegRate3 * 1.5
                                                TmpReg = 0
                                                RSet RegRate3_Mask = CStr(Format(RegRate3, "#.00"))
                                                RSet OvtRate3_Mask = CStr(Format(OvtRate3, "#.00"))
                                                Job3Reg_DLY = 0
                                                Job3Ovt_DLY = 0
                                            
                                             'If Job3Ovt > 0 Then
                                                'Job3Reg = 40
                                             'End If
                                           
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_3
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job3Reg_DLY = DailyHours(0)
                                            End If
        
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_3
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job3Ovt_DLY = DailyHours(0)
                                            End If
       
        ElseIf ViewRsTime("job_number") = jobcode_4 Then
                                                RegRate4 = 0
                                                OvtRate4 = 0
                                                Job4Reg = Job4Reg + TmpReg
                                                Job4Ovt = Job4Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate4 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate4 = ViewRsTime("default_regular_rate")
                                            End If
                                          '  If ViewRsTime("override_overtime_rate") > 0 Then
                                          '     OvtRate4 = ViewRsTime("override_overtime_rate")
                                          '  Else
                                          '     OvtRate4 = ViewRsTime("default_overtime_rate")
                                          '  End If
                                          '      RegRate4 = ViewRsTime("override_regular_rate")
                                                OvtRate4 = RegRate4 * 1.5
                                                TmpReg = 0
                                                RSet RegRate4_Mask = CStr(Format(RegRate4, "#.00"))
                                                RSet OvtRate4_Mask = CStr(Format(OvtRate4, "#.00"))
                                                Job4Reg_DLY = 0
                                                Job4Ovt_DLY = 0

                                             'If Job4Ovt > 0 Then
                                                'Job4Reg = 40
                                             'End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_4
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job4Ovt_DLY = DailyHours(0)
                                            End If
       
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_4
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job4Reg_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_5 Then
                                                RegRate5 = 0
                                                OvtRate5 = 0
                                                Job5Reg = Job5Reg + TmpReg
                                                Job5Ovt = Job5Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate5 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate5 = ViewRsTime("default_regular_rate")
                                            End If
                                         '   If ViewRsTime("override_overtime_rate") > 0 Then
                                         '      OvtRate5 = ViewRsTime("override_overtime_rate")
                                         '   Else
                                         '      OvtRate5 = ViewRsTime("default_overtime_rate")
                                         '   End If
                                                'RegRate5 = ViewRsTime("override_regular_rate")
                                                OvtRate5 = RegRate5 * 1.5
                                                TmpReg = 0
                                                RSet RegRate5_Mask = CStr(Format(RegRate5, "#.00"))
                                                RSet OvtRate5_Mask = CStr(Format(OvtRate5, "#.00"))
                                                Job5Reg_DLY = 0
                                                Job5Ovt_DLY = 0
        
                                             'If Job5Ovt > 0 Then
                                                'Job5Reg = 40
                                             'End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_5
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job5Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_5
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job5Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_6 Then
                                                RegRate6 = 0
                                                OvtRate6 = 0
                                                Job6Reg = Job6Reg + TmpReg
                                                Job6Ovt = Job6Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate6 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate6 = ViewRsTime("default_regular_rate")
                                            End If
                                         '   If ViewRsTime("override_overtime_rate") > 0 Then
                                         '      OvtRate6 = ViewRsTime("override_overtime_rate")
                                         '   Else
                                         '      OvtRate6 = ViewRsTime("default_overtime_rate")
                                         '   End If
                                                'RegRate6 = ViewRsTime("override_regular_rate")
                                                OvtRate6 = RegRate6 * 1.5
                                                TmpReg = 0
                                                RSet RegRate6_Mask = CStr(Format(RegRate6, "#.00"))
                                                RSet OvtRate6_Mask = CStr(Format(OvtRate6, "#.00"))
                                                Job6Reg_DLY = 0
                                                Job6Ovt_DLY = 0
        
                                             'If Job6Ovt > 0 Then
                                                'Job6Reg = 40
                                            ' End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_6
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job6Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_6
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job6Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_7 Then
                                                RegRate7 = 0
                                                OvtRate7 = 0
                                                Job7Reg = Job7Reg + TmpReg
                                                Job7Ovt = Job7Ovt + TmpOvt
                                               
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate7 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate7 = ViewRsTime("default_regular_rate")
                                            End If
                                          '  If ViewRsTime("override_overtime_rate") > 0 Then
                                          '     OvtRate7 = ViewRsTime("override_overtime_rate")
                                          '  Else
                                          '     OvtRate7 = ViewRsTime("default_overtime_rate")
                                          '  End If
                                                'RegRate7 = ViewRsTime("override_regular_rate")
                                                OvtRate7 = RegRate7 * 1.5
                                                TmpReg = 0
                                                RSet RegRate7_Mask = CStr(Format(RegRate7, "#.00"))
                                                RSet OvtRate7_Mask = CStr(Format(OvtRate7, "#.00"))
                                                Job7Reg_DLY = 0
                                                Job7Ovt_DLY = 0
        
                                             'If Job7Ovt > 0 Then
                                                'Job7Reg = 40
                                             'End If
                                           
                                            
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_7
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job7Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_7
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job7Ovt_DLY = DailyHours(0)
                                            End If
        ElseIf ViewRsTime("job_number") = jobcode_8 Then
                                                RegRate8 = 0
                                                OvtRate8 = 0
                                                Job8Reg = Job8Reg + TmpReg
                                                Job8Ovt = Job8Ovt + TmpOvt
                                                
                                            If ViewRsTime("override_regular_rate") > 0 Then
                                               RegRate8 = ViewRsTime("override_regular_rate")
                                            Else
                                               RegRate8 = ViewRsTime("default_regular_rate")
                                            End If
                                         '   If ViewRsTime("override_overtime_rate") > 0 Then
                                         '      OvtRate8 = ViewRsTime("override_overtime_rate")
                                         '   Else
                                         '      OvtRate8 = ViewRsTime("default_overtime_rate")
                                         '   End If
                                                'RegRate8 = ViewRsTime("override_regular_rate")
                                                OvtRate8 = RegRate8 * 1.5
                                                TmpReg = 0
                                                RSet RegRate8_Mask = CStr(Format(RegRate8, "#.00"))
                                                RSet OvtRate8_Mask = CStr(Format(OvtRate8, "#.00"))
                                                Job8Reg_DLY = 0
                                                Job8Ovt_DLY = 0
                                        
                                             'If Job8Ovt > 0 Then
                                                'Job8Reg = 40
                                            ' End If
                                           
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select regular_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_8
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job8Reg_DLY = DailyHours(0)
                                            End If
                                            'SET THE COMMANDS TEXT AND SPECIFY THAT IT
                                            'IS AN SQL STATEMENT. IT WILL EXECUTE THE DAILY OVERTIME HOURS CALL
                                            OpenDailyHours.CommandText = "select overtime_hours from micros.v_r_employee_time_card , micros.rest_status where where day(clock_in_datetime) = day(now(*))-1 and employee_number = " & Emp_Num & " and job_number = " & jobcode_8
                                            OpenDailyHours.CommandType = adCmdText
                                            'CREATE THE RECORDSET FOR THE DAILY HOURS
                                            Set DailyHours = OpenDailyHours.Execute
                                            If DailyHours.EOF = False Then
                                                Job8Ovt_DLY = DailyHours(0)
                                            End If

        End If

    
       ViewRsTime.MoveNext 'MOVE TO THE NEXT RECORD
   
     TimeCounter = TimeCounter - 1
    Loop    'INNER LOOP
        'Replaced Print code with shorter version
        'Print #1, Store_Id, Payroll_Id, Emp_Num, Last_Name, First_Name, jobcode_1, RegRate1_Mask, Job1Reg, Job1Reg_DLY, OvtRate1_Mask, Job1Ovt, Job1Ovt_DLY, jobcode_2, RegRate2_Mask, Job2Reg, Job2Reg_DLY, OvtRate2_Mask, Job2Ovt, Job2Ovt_DLY, jobcode_3, RegRate3_Mask, Job3Reg, Job3Reg_DLY, OvtRate3_Mask, Job3Ovt, Job3Ovt_DLY, _
        'jobcode_4, RegRate4_Mask, Job4Reg, Job4Reg_DLY, OvtRate4_Mask, Job4Ovt, Job4Ovt_DLY, jobcode_5, RegRate5_Mask, Job5Reg, Job5Reg_DLY, OvtRate5_Mask, Job5Ovt, job5_ovt_dly, jobcode_6, RegRate6_Mask, Job6Reg, Job6Reg_DLY, OvtRate6_Mask, Job6Ovt, Job6Ovt_DLY, jobcode_7, RegRate7_Mask, Job7Reg, Job7Reg_DLY, OvtRate7_Mask, Job7Ovt, _
        'Job7Ovt_DLY, jobcode_8, RegRate8_Mask, Job8Reg, Job8Reg_DLY, OvtRate8_Mask, Job8Ovt, Job8Ovt_DLY, NetSalesDLY, ServiceChgDLY, GrosRcptsDLY, ChgdRcptsDLY, ChgdTipsDLY, TipsSvcDLY, TipsPdDLY, TipsDcldDLY, Tips_mask_DLY, NetSalesTTL, ServiceChgTTL, GrosRcptsTTL, _
        'ChgdRcptsTTL, ChgdTipsTTL, TipsSvcTTL, TipsPdTTL, TipsDcldTTL, Tips_Mask_TTL
 If GrosRcptsDLY > 9999.99 Then
    GrosRcptsDLY = 9999.99
 End If
 If ChgdRcptsDLY > 9999.99 Then
    ChgdRcptsDLY = 9999.99
 End If
 If ChgdTipsDLY > 9999.99 Then
    ChgdTipsDLY = 9999.99
 End If
 If TipsSvcDLY > 9999.99 Then
    TipsSvcDLY = 9999.99
 End If
 If TipsPdDLY > 9999.99 Then
    TipsPdDLY = 9999.99
 End If
 If TipsDcldDLY > 9999.99 Then
    TipsDcldDLY = 9999.99
 End If
 If NetSalesTTL > 9999.99 Then
    NetSalesTTL = 9999.99
 End If
 If ChgdRcptsTTL > 9999.99 Then
    ChgdRcptsTTL = 9999.99
 End If
 If ChgdTipsTTL > 9999.99 Then
    ChgdTipsTTL = 9999.99
 End If
 If TipsDcldTTL > 9999.99 Then
    TipsDcldTTL = 9999.99
 End If
 If TipsPdTTL > 9999.99 Then
    TipsPdTTL = 9999.99
 End If
 If (TipsDcldTTL + TipsPdTTL) > 9999.99 Then
       Print #1, Tab(8); Right(Format(Store_Id, "####"), 4); Tab(19); Format(Emp_Num, "0000"); Tab(26); Format(Payroll_Id, "000000000"); Tab(38); Format(Last_Name, "@@@@@@@@@@@@@@@@"); Tab(57); Format(First_Name, "@@@@@@@@"); Tab(75); Format(jobcode_1, "00"); Tab(78); Format(RegRate1_Mask, "00.00"); Tab(84); Format(OvtRate1_Mask, "00.00"); Tab(90); Format(Job1Reg_DLY, "00.00"); Tab(96); Format(Job1Reg, "00.00"); Tab(102); Format(Job1Ovt_DLY, "00.00"); Tab(108); Format(Job1Ovt, "00.00"); _
                 Tab(123); Format(jobcode_2, "00"); Tab(126); Format(RegRate2_Mask, "00.00"); Tab(132); Format(OvtRate2_Mask, "00.00"); Tab(138); Format(Job2Reg_DLY, "00.00"); Tab(144); Format(Job2Reg, "00.00"); Tab(150); Format(Job2Ovt_DLY, "00.00"); Tab(156); Format(Job2Ovt, "00.00"); _
                 Tab(171); Format(jobcode_3, "00"); Tab(174); Format(RegRate3_Mask, "00.00"); Tab(180); Format(OvtRate3_Mask, "00.00"); Tab(186); Format(Job3Reg_DLY, "00.00"); Tab(192); Format(Job3Reg, "00.00"); Tab(198); Format(Job3Ovt_DLY, "00.00"); Tab(204); Format(Job3Ovt, "00.00"); _
                 Tab(219); Format(jobcode_4, "00"); Tab(222); Format(RegRate4_Mask, "00.00"); Tab(228); Format(OvtRate4_Mask, "00.00"); Tab(234); Format(Job4Reg_DLY, "00.00"); Tab(240); Format(Job4Reg, "00.00"); Tab(246); Format(Job4Ovt_DLY, "00.00"); Tab(252); Format(Job4Ovt, "00.00"); _
                 Tab(267); Format(jobcode_5, "00"); Tab(270); Format(RegRate5_Mask, "00.00"); Tab(276); Format(OvtRate5_Mask, "00.00"); Tab(282); Format(Job5Reg_DLY, "00.00"); Tab(288); Format(Job5Reg, "00.00"); Tab(294); Format(Job5Ovt_DLY, "00.00"); Tab(300); Format(Job5Ovt, "00.00"); _
                 Tab(315); Format(jobcode_6, "00"); Tab(318); Format(RegRate6_Mask, "00.00"); Tab(324); Format(OvtRate6_Mask, "00.00"); Tab(330); Format(Job6Reg_DLY, "00.00"); Tab(336); Format(Job6Reg, "00.00"); Tab(342); Format(Job6Ovt_DLY, "00.00"); Tab(348); Format(Job6Ovt, "00.00"); _
                 Tab(356); Format(GrosRcptsDLY, "0000.00"); Tab(365); Format(ChgdRcptsDLY, "0000.00"); Tab(374); Format(ChgdTipsDLY, "0000.00"); Tab(383); Format(TipsSvcDLY, "0000.00"); Tab(392); Format(TipsPdDLY, "0000.00"); Tab(401); Format(TipsDcldDLY, "0000.00"); Tab(411); Format("0.00", "0.00"); _
                 Tab(418); Format(NetSalesTTL, "0000.00"); Tab(427); Format(ChgdRcptsTTL, "0000.00"); Tab(436); Format(ChgdTipsTTL, "0000.00"); Tab(445); Format(TipsDcldTTL, "0000.00"); Tab(454); Format(TipsPdTTL, "0000.00"); _
                 Tab(463); "9999.99"; _
                 Tab(476); Format("0.00", "0.00"); Tab(481); Left(Format(Date - 1, "mm"), 2); Tab(493); Right(Format(Date - 1, "mm/dd/yy"), 5)

  Else
        Print #1, Tab(8); Right(Format(Store_Id, "####"), 4); Tab(19); Format(Emp_Num, "0000"); Tab(26); Format(Payroll_Id, "000000000"); Tab(38); Format(Last_Name, "@@@@@@@@@@@@@@@@"); Tab(57); Format(First_Name, "@@@@@@@@"); Tab(75); Format(jobcode_1, "00"); Tab(78); Format(RegRate1_Mask, "00.00"); Tab(84); Format(OvtRate1_Mask, "00.00"); Tab(90); Format(Job1Reg_DLY, "00.00"); Tab(96); Format(Job1Reg, "00.00"); Tab(102); Format(Job1Ovt_DLY, "00.00"); Tab(108); Format(Job1Ovt, "00.00"); _
                 Tab(123); Format(jobcode_2, "00"); Tab(126); Format(RegRate2_Mask, "00.00"); Tab(132); Format(OvtRate2_Mask, "00.00"); Tab(138); Format(Job2Reg_DLY, "00.00"); Tab(144); Format(Job2Reg, "00.00"); Tab(150); Format(Job2Ovt_DLY, "00.00"); Tab(156); Format(Job2Ovt, "00.00"); _
                 Tab(171); Format(jobcode_3, "00"); Tab(174); Format(RegRate3_Mask, "00.00"); Tab(180); Format(OvtRate3_Mask, "00.00"); Tab(186); Format(Job3Reg_DLY, "00.00"); Tab(192); Format(Job3Reg, "00.00"); Tab(198); Format(Job3Ovt_DLY, "00.00"); Tab(204); Format(Job3Ovt, "00.00"); _
                 Tab(219); Format(jobcode_4, "00"); Tab(222); Format(RegRate4_Mask, "00.00"); Tab(228); Format(OvtRate4_Mask, "00.00"); Tab(234); Format(Job4Reg_DLY, "00.00"); Tab(240); Format(Job4Reg, "00.00"); Tab(246); Format(Job4Ovt_DLY, "00.00"); Tab(252); Format(Job4Ovt, "00.00"); _
                 Tab(267); Format(jobcode_5, "00"); Tab(270); Format(RegRate5_Mask, "00.00"); Tab(276); Format(OvtRate5_Mask, "00.00"); Tab(282); Format(Job5Reg_DLY, "00.00"); Tab(288); Format(Job5Reg, "00.00"); Tab(294); Format(Job5Ovt_DLY, "00.00"); Tab(300); Format(Job5Ovt, "00.00"); _
                 Tab(315); Format(jobcode_6, "00"); Tab(318); Format(RegRate6_Mask, "00.00"); Tab(324); Format(OvtRate6_Mask, "00.00"); Tab(330); Format(Job6Reg_DLY, "00.00"); Tab(336); Format(Job6Reg, "00.00"); Tab(342); Format(Job6Ovt_DLY, "00.00"); Tab(348); Format(Job6Ovt, "00.00"); _
                 Tab(356); Format(GrosRcptsDLY, "0000.00"); Tab(365); Format(ChgdRcptsDLY, "0000.00"); Tab(374); Format(ChgdTipsDLY, "0000.00"); Tab(383); Format(TipsSvcDLY, "0000.00"); Tab(392); Format(TipsPdDLY, "0000.00"); Tab(401); Format(TipsDcldDLY, "0000.00"); Tab(411); Format("0.00", "0.00"); _
                 Tab(418); Format(NetSalesTTL, "0000.00"); Tab(427); Format(ChgdRcptsTTL, "0000.00"); Tab(436); Format(ChgdTipsTTL, "0000.00"); Tab(445); Format(TipsDcldTTL, "0000.00"); Tab(454); Format(TipsPdTTL, "0000.00"); _
                 Tab(463); Format(TipsDcldTTL + TipsPdTTL, "0000.00"); _
                 Tab(476); Format("0.00", "0.00"); Tab(481); Left(Format(Date - 1, "mm"), 2); Tab(493); Right(Format(Date - 1, "mm/dd/yy"), 5)
 End If
Loop    'OUTTER LOOP

        frmMain.MousePointer = 0
        
        End

error_handler:
Open "C:\TexRock\ErrData.lst" For Append As #2
    Print #2, Tab(8); Right(Store_Id, 4); Tab(20); Emp_Num; Tab(26); Payroll_Id; Tab(38); Last_Name
Close #2
'Errorcounter = Errorcounter + 1
       'frmMain.Hide
       'For x = 1 To 5
       'FrmError.Show
       'FrmError.LblError.Caption = " Restaurant:              " & Store_Id & Chr(13) & "  Decription of Error:  " & Err.Description & _
                ' Chr(13) & "  Error Number:           " & Err.Number & Chr(13) & "  Number of Errors:    " & Errorcounter
       'DoEvents
       'Next x
       Err.Clear
       Resume Next

End Sub














