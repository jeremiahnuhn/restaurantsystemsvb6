VERSION 4.00
Begin VB.Form STINOUT 
   BackColor       =   &H00C0C0C0&
   Caption         =   "ST Tracking System"
   ClientHeight    =   5760
   ClientLeft      =   480
   ClientTop       =   1170
   ClientWidth     =   6495
   Height          =   6450
   Icon            =   "STINOUT.frx":0000
   Left            =   420
   LinkTopic       =   "Form1"
   ScaleHeight     =   5760
   ScaleWidth      =   6495
   Top             =   540
   Width           =   6615
   Begin VB.Data BSActive 
      Caption         =   "BSActive"
      Connect         =   "Access"
      DatabaseName    =   "I:\WHERE2.MDB"
      Exclusive       =   0   'False
      Height          =   300
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "BSActive"
      Top             =   5400
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data Where2 
      Caption         =   "RMR"
      Connect         =   "Access"
      DatabaseName    =   "I:\WHERE2.MDB"
      Exclusive       =   0   'False
      Height          =   300
      Left            =   3120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "RMR"
      Top             =   5400
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Label2"
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   4680
      TabIndex        =   3
      Top             =   360
      Width           =   1815
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Service Technician"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1815
   End
   Begin MSDBCtls.DBCombo Active 
      Bindings        =   "STINOUT.frx":0442
      DataField       =   "Name"
      DataSource      =   "BSActive"
      Height          =   315
      Left            =   2040
      TabIndex        =   0
      Top             =   120
      Width           =   2655
      _version        =   65536
      _extentx        =   4683
      _extenty        =   556
      _stockprops     =   77
      forecolor       =   0
      backcolor       =   16777215
      style           =   2
      listfield       =   "Name"
      boundcolumn     =   "Name"
   End
   Begin TrueDBGrid45.TDBGrid RMRInOut 
      Bindings        =   "STINOUT.frx":0453
      Height          =   5055
      Left            =   120
      OleObjectBlob   =   "STINOUT.frx":0462
      TabIndex        =   1
      Top             =   600
      Width           =   6255
   End
   Begin VB.Menu AddNote 
      Caption         =   "&Help Desk Entry"
   End
   Begin VB.Menu Options 
      Caption         =   "&Options"
      Begin VB.Menu DatesToView 
         Caption         =   "Dates To &View"
         Begin VB.Menu CurrentDate 
            Caption         =   "&Current Date Only (Notes Always Display)"
            Checked         =   -1  'True
         End
         Begin VB.Menu AllDates 
            Caption         =   "&All Dates"
         End
      End
      Begin VB.Menu DeleteAD 
         Caption         =   "&Delete Records"
      End
      Begin VB.Menu ServerStatus 
         Caption         =   "&Server Status"
      End
   End
   Begin VB.Menu ExitProgram 
      Caption         =   "&Exit"
   End
End
Attribute VB_Name = "STINOUT"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub Active_Click(Area As Integer)
Screen.MousePointer = 11
DOPAINT = True
If AllDates.Checked = True Then
    sqltext = "Select * From RMR Where Name = " + Quote(Active.Text)
Else
    tmpdate = "#" + Str(Date) + "#"
    sqltext = "Select * From RMR Where Name = " + Quote(Active.Text) + " AND (Date = " + tmpdate + " OR Type = " + Quote(4) + ")"
End If
Where2.RecordSource = sqltext
RMRInOut.RowHeight = 225.071
Where2.Refresh
Screen.MousePointer = 1
End Sub


Private Sub AddNote_Click()
    Note.Show
   Note.NoteFor.Caption = STINOUT.Active.Text
    Note.Message.Text = ""
    Note.TimeStamp.Mask = ""
    Note.TimeStamp.Text = ""
    Note.TimeStamp.Text = Format(Time, "hh:mm ampm")
    Note.Cal1.CalDay = Day(Date)
    Note.Cal1.CalMonth = Month(Date)
    Note.Cal1.CalYear = Mid$(Year(Date), 3, 2)
    For X = 0 To 7
        Note.Option1(X) = False
    Next X
    Note.Option1(4) = True
    DOPAINT = False

End Sub


Private Sub Check1_Click()

End Sub

Private Sub Command1_Click()
End Sub

Private Sub AllDates_Click()
CurrentDate.Checked = False
AllDates.Checked = True
STINOUT.Refresh
Label2.Caption = "View All Dates"
End Sub

Private Sub CurrentDate_Click()
CurrentDate.Checked = True
AllDates.Checked = False
STINOUT.Refresh
Label2.Caption = "View Current Date Only"
End Sub

Private Sub DeleteAD_Click()
deldate = Str(Date - 1)
Msg = "Are you sure you want to delete all Tracking Information for " + (deldate) + "?" ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Are you sure!"  ' Define title.
response = MsgBox(Msg, Style, Title)
If response = vbYes Then    ' User chose Yes.
    Dim db As Database
    Set db = DBEngine.Workspaces(0).OpenDatabase("I:\WHERE2.MDB")
    sqltext = "Delete RMR.* FROM RMR WHERE RMR.DATE <= #" + deldate + "#"
    db.Execute sqltext
    STINOUT.Refresh
End If
End Sub


Private Sub ExitProgram_Click()
    Close
    Unload Note
    End
End Sub



Public Function Quote(inpstr As String) As String
' Return the argument, with double quotes around it.
Quote = Chr$(34) + inpstr + Chr$(34)
End Function

Private Sub Form_Load()
Load Note
If Hour(Now) = 5 Then
    deldate = Str(Date - 1)
Msg = "Are you sure you want to delete all Tracking Information for " + (deldate) + "?" ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Are you sure!"  ' Define title.
response = MsgBox(Msg, Style, Title)
If response = vbYes Then    ' User chose Yes.
    Dim db As Database
    Set db = DBEngine.Workspaces(0).OpenDatabase("I:\WHERE2.MDB")
    sqltext = "Delete RMR.* FROM RMR WHERE RMR.DATE <= #" + deldate + "#"
    db.Execute sqltext
    STINOUT.Refresh
End If
End If
Label2.Caption = "View Current Date Only"
End Sub


Private Sub Form_Paint()
Screen.MousePointer = 11
If AllDates.Checked = True Then
    sqltext = "Select * From RMR Where Name = " + Quote(Active.Text)
Else
    tmpdate = "#" + Str(Date) + "#"
    sqltext = "Select * From RMR Where Name = " + Quote(Active.Text) + " AND (Date = " + tmpdate + " OR Type = " + Quote(4) + ")"
End If
Where2.RecordSource = sqltext
Where2.Refresh
Screen.MousePointer = 1

End Sub


Private Sub RMRInOut_DblClick()
        RMRInOut.RowHeight = 600
End Sub


Private Sub ServerStatus_Click()
    Open "I:\STIMPORT.NOW" For Input As #10
    Input #10, status
    Close #10
    status = "The following message if the last activity by the Application Server.  If the difference between the current time and the last import/check time is greater than six minutes an error may have occured, please contact Jody Smith at extension 494." + vbCrLf + vbCrLf + status
    MsgBox status, vbInformation, "Application Server Status"
End Sub


