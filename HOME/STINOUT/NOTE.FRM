VERSION 4.00
Begin VB.Form Note 
   Caption         =   "Help Desk Entry"
   ClientHeight    =   5730
   ClientLeft      =   795
   ClientTop       =   1110
   ClientWidth     =   5835
   ControlBox      =   0   'False
   BeginProperty Font 
      name            =   "MS Sans Serif"
      charset         =   1
      weight          =   700
      size            =   8.25
      underline       =   0   'False
      italic          =   0   'False
      strikethrough   =   0   'False
   EndProperty
   Height          =   6135
   Left            =   735
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   5835
   Top             =   765
   Width           =   5955
   Begin VB.Data RestInfo 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "I:\WHERE2.MDB"
      Exclusive       =   0   'False
      Height          =   300
      Left            =   3480
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "RestInfo"
      Top             =   5520
      Visible         =   0   'False
      Width           =   1140
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Meeting"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   1320
      TabIndex        =   16
      Top             =   1320
      Width           =   975
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Vacation"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   3600
      TabIndex        =   7
      Top             =   1320
      Width           =   1095
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Off Today"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   2400
      TabIndex        =   6
      Top             =   1320
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Cancel"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4560
      TabIndex        =   10
      Top             =   4920
      Width           =   855
   End
   Begin VBX.Cal Cal1 
      BackColor       =   &H00C0C0C0&
      CalDay          =   9
      CalMonth        =   12
      CalYear         =   97
      Height          =   2655
      Left            =   240
      Top             =   2880
      Width           =   3255
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Note"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Index           =   4
      Left            =   4680
      TabIndex        =   8
      Top             =   1320
      Value           =   -1  'True
      Width           =   1095
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Scheduled"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4680
      TabIndex        =   5
      Top             =   960
      Width           =   1095
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Travel"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   3600
      TabIndex        =   4
      Top             =   960
      Width           =   1095
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Depart"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   2400
      TabIndex        =   3
      Top             =   960
      Width           =   1095
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Arrive"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   1320
      TabIndex        =   2
      Top             =   960
      Width           =   975
   End
   Begin VB.CommandButton SaveNote 
      Caption         =   "&Ok"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4560
      TabIndex        =   1
      Top             =   4320
      Width           =   855
   End
   Begin MSDBCtls.DBCombo RestList 
      Bindings        =   "NOTE.frx":0000
      DataField       =   "NAME"
      DataSource      =   "RestInfo"
      Height          =   360
      Left            =   240
      TabIndex        =   17
      Top             =   2040
      Visible         =   0   'False
      Width           =   5415
      _version        =   65536
      _extentx        =   9551
      _extenty        =   635
      _stockprops     =   77
      forecolor       =   0
      backcolor       =   16777215
      BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      style           =   2
      listfield       =   "NAME"
      boundcolumn     =   "NAME"
   End
   Begin MSMask.MaskEdBox Message 
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   2040
      Width           =   5415
      _version        =   65536
      _extentx        =   9551
      _extenty        =   661
      _stockprops     =   109
      forecolor       =   -2147483640
      backcolor       =   -2147483643
      BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      borderstyle     =   1
   End
   Begin VB.Label Label4 
      Caption         =   "Date and Time:"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   2640
      Width           =   3255
   End
   Begin MSMask.MaskEdBox TimeStamp 
      Height          =   375
      Left            =   3960
      TabIndex        =   9
      Top             =   3360
      Width           =   1095
      _version        =   65536
      _extentx        =   1931
      _extenty        =   661
      _stockprops     =   109
      forecolor       =   -2147483640
      backcolor       =   -2147483643
      BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      borderstyle     =   1
      maxlength       =   8
      format          =   "hh:mm AM/PM"
      mask            =   "##:## >Am"
   End
   Begin VB.Label Label3 
      Caption         =   "Type of Note:"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   960
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "Note For:"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   240
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "Note: (Restaurant/Location)"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   1680
      Width           =   2415
   End
   Begin VB.Label NoteFor 
      Caption         =   "NoteFor"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   11
      Top             =   480
      Width           =   5295
   End
End
Attribute VB_Name = "Note"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub Command1_Click()
Note.Hide
DOPAINT = True
End Sub


Private Sub Check1_Click(Index As Integer)

End Sub


Private Sub CBArrive_Click()
    CBArrive.Value = 1
    CBDepart.Value = 0
    CBTravel.Value = 0
    CBScheduled.Value = 0
    CBNote.Value = 0
End Sub

Private Sub CBDepart_Click()
    CBArrive.Value = 0
    CBDepart.Value = 0
    CBTravel.Value = 0
    CBScheduled.Value = 1
    CBNote.Value = 0

End Sub


Private Sub CBNote_Click()
    CBArrive.Value = 0
    CBDepart.Value = 0
    CBTravel.Value = 0
    CBScheduled.Value = 0
    CBNote.Value = 1

End Sub


Private Sub CBScheduled_Click()
    CBArrive.Value = 0
    CBDepart.Value = 0
    CBTravel.Value = 0
    CBScheduled.Value = 1
    CBNote.Value = 0

End Sub



Private Sub CBTravel_Click()
    CBArrive.Value = 0
    CBDepart.Value = 0
    CBTravel.Value = 1
    CBScheduled.Value = 0
    CBNote.Value = 0

End Sub


Private Sub Form_Paint()
If DOPAINT = True Then
    NoteFor.Caption = STINOUT.Active.Text
    Message.Text = ""
    TimeStamp.Mask = ""
    TimeStamp.Text = ""
    RestList.Text = " "
    TimeStamp.Text = Format(Time, "hh:mm ampm")
    Cal1.CalDay = Day(Date)
    Cal1.CalMonth = Month(Date)
    Cal1.CalYear = Mid$(Year(Date), 3, 2)
    For X = 0 To 7
        Option1(X) = False
    Next X
    Option1(4) = True
    DOPAINT = False
End If
End Sub


Private Sub Option1_Click(Index As Integer)
  If Option1(0) = True Then
            RestList.Visible = True
            Message.Visible = False
            End If
    If Option1(1) = True Then
            RestList.Visible = True
            Message.Visible = False
            End If
    If Option1(2) = True Then
            RestList.Visible = True
            Message.Visible = False
            End If
    If Option1(3) = True Then
            RestList.Visible = True
            Message.Visible = False
            End If
    If Option1(4) = True Then
            RestList.Visible = False
            Message.Visible = True
            End If
    If Option1(5) = True Then
            RestList.Visible = False
            Message.Visible = True
            End If
    If Option1(6) = True Then
            RestList.Visible = False
            Message.Visible = True
            End If
    If Option1(7) = True Then
            RestList.Visible = False
            Message.Visible = True
            End If

End Sub

Private Sub SaveNote_Click()
'Open database to read Rest Info
Dim db As Database
Dim tblUnit As Recordset
Set db = DBEngine.Workspaces(0).OpenDatabase("I:\WHERE2.MDB")
Set tblUnit = db.OpenRecordset("RestInfo", dbOpenTable)

MAXUNIT = tblUnit.RecordCount
ReDim UF(3, MAXUNIT)
    '1 = Unit Number
    '2 = Speed Number
    '3 = Unit Name
For X = 1 To MAXUNIT
    UF(1, X) = tblUnit("Number")
    UF(2, X) = tblUnit("SpeedNumb")
    UF(3, X) = tblUnit("Name")
    tblUnit.MoveNext
Next X



Screen.MousePointer = 11
Set db = DBEngine.Workspaces(0).OpenDatabase("I:\WHERE2.MDB")
Set tblRMR = db.OpenRecordset("RMR", dbOpenTable)
    tblRMR.AddNew
    tblRMR("NAME") = NoteFor
    tblRMR("Restaurant Number") = "9999"
    If Mid$(TimeStamp, 7, 1) = "P" Then
        TimeStamp = Mid$(TimeStamp, 1, 6) + "pm"
    End If
    If Mid$(TimeStamp, 7, 1) = "A" Then
        TimeStamp = Mid$(TimeStamp, 1, 6) + "am"
    End If
    If Mid$(TimeStamp, 1, 1) = 0 Then
        TempTime = Mid$(TimeStamp, 2)
    Else
        TempTime = TimeStamp
    End If
    tblRMR("Time Stamp") = TempTime
    tblRMR("Date") = Str(Cal1.CalMonth) + "/" + Str(Cal1.CalDay) + "/" + Str(Cal1.CalYear)


    
    
    If Option1(0) = True Then
                tblRMR("Restaurant Name") = "�" + RestList.Text
                For X = 1 To MAXUNIT
                If Trim(RestList.Text) = UF(3, X) Then
                    tblRMR("Speed") = UF(2, X)
                     X = MAXUNIT
                End If
                Next X
                tblRMR("Type") = "0"
            End If
    If Option1(1) = True Then
                tblRMR("Restaurant Name") = "�" + RestList.Text
                For X = 1 To MAXUNIT
                If Trim(RestList.Text) = UF(3, X) Then
                    tblRMR("Speed") = UF(2, X)
                     X = MAXUNIT
                End If
                Next X
                tblRMR("Type") = "1"
            End If
    If Option1(2) = True Then
                tblRMR("Restaurant Name") = "�" + RestList.Text
                For X = 1 To MAXUNIT
                If Trim(RestList.Text) = UF(3, X) Then
                    tblRMR("Speed") = UF(2, X)
                     X = MAXUNIT
                End If
                Next X
                tblRMR("Type") = "2"
            End If
    If Option1(3) = True Then
               tblRMR("Restaurant Name") = "�" + RestList.Text
                For X = 1 To MAXUNIT
                If Trim(RestList.Text) = UF(3, X) Then
                    tblRMR("Speed") = UF(2, X)
                     X = MAXUNIT
                End If
                Next X
               tblRMR("Type") = "3"
            End If
    If Option1(4) = True Then
            tblRMR("Restaurant Name") = "�" + Message
            tblRMR("Type") = "4"
            End If
    If Option1(5) = True Then
            tblRMR("Restaurant Name") = "�" + Message
            tblRMR("Type") = "5"
            End If
    If Option1(6) = True Then
            tblRMR("Restaurant Name") = "�" + Message
            tblRMR("Type") = "6"
            End If
    If Option1(7) = True Then
            tblRMR("Restaurant Name") = "�" + Message
            tblRMR("Type") = "7"
            End If
    tblRMR.Update
    Screen.MousePointer = 1
    Note.Hide

End Sub
Public Function Quote(inpstr As String) As String
' Return the argument, with double quotes around it.
Quote = Chr$(34) + inpstr + Chr$(34)
End Function

Private Sub TimeStamp_GotFocus()
On Error GoTo errorhandler
    TimeStamp.Text = TimeStamp.Text
    TimeStamp.Text = ""
    TimeStamp.Mask = "##:## >am"
    TimeStamp.Format = "hh:mm AM/PM"
Exit Sub
    
    
errorhandler:
    TimeStamp.Text = TimeStamp.Text
Resume Next
End Sub

