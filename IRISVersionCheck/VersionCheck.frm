VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "IRIS Version Check for 4.1.1138.900"
   ClientHeight    =   6975
   ClientLeft      =   4125
   ClientTop       =   1980
   ClientWidth     =   12420
   Icon            =   "VersionCheck.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6975
   ScaleWidth      =   12420
   Begin VB.CommandButton Command4 
      Caption         =   "Check All for Problems"
      Height          =   495
      Left            =   4680
      TabIndex        =   7
      Top             =   6240
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Print"
      Height          =   495
      Left            =   8520
      TabIndex        =   4
      Top             =   6240
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Copy to Clipboard"
      Height          =   495
      Left            =   6600
      TabIndex        =   2
      Top             =   6240
      Width           =   1695
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5730
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   11895
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      Height          =   495
      Left            =   10440
      TabIndex        =   0
      Top             =   6240
      Width           =   1695
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Label3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1560
      TabIndex        =   6
      Top             =   6240
      Width           =   2775
   End
   Begin VB.Label Label2 
      Caption         =   "Version 1"
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   6240
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   13815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
  Clipboard.Clear
  Copytoclip = ""
For I = 0 To List1.ListCount - 1
Copytoclip = Copytoclip & List1.List(I) & Chr$(13) & Chr$(10)
Next I
Clipboard.SetText Copytoclip
m = MsgBox("Copy to Clipboard complete.", vbInformation, "Clipboard Status")
End Sub

Private Sub Command3_Click()
Printer.Print
Printer.Print
   Printer.FontName = "COURIER NEW"
   Printer.FontSize = 12
      Printer.CurrentX = 600
   Printer.Print "####   Restaurant Name       Version   File Date Time    Scheduled"
      Printer.CurrentX = 600
   Printer.Print "--------------------------------------------------------------------------"
For I = 0 To List1.ListCount - 1
Printer.CurrentX = 600
Printer.Print List1.List(I)
Printer.Print
Printer.Print
Next I
Printer.Print
Printer.FontSize = 10
Printer.CurrentX = 2600
Printer.Print Str(Now()) + " - " + Label3.Caption + " Converted"
Printer.EndDoc
m = MsgBox("Printed", vbInformation, "Printing Status")
End Sub

Private Sub Form_Load()
Label1 = "####   Restaurant Name        POS.EXE    File Date Time   Scheduled"
On Error GoTo ErrorHandler

Dim UnitNumber As String
Dim UnitName As String
Dim Reg1 As String
Dim Reg2 As String
Dim Reg3 As String
Dim Reg5 As String
Dim TID As String
Dim Version As String
Dim RunDate As String
Dim RunTime As String
Dim ServerDate As Date

NumberConverted = 0

Open "s:\IRIS\VersionCheck\VersionCheck.csv" For Output As #4
Print #4, "####,Name,Version,File Date,File Time,Scheduled"

Open "s:\IRIS\VersionCheck\RolloutSchedule.csv" For Input As #1
    Line Input #1, IndividualLine
    Do While Not EOF(1)

    Line Input #1, IndividualLine
    FileInput = Split(IndividualLine, ",")
    UnitNumber = FileInput(0)
    UnitName = FileInput(1)
    Server = FileInput(2)
    TempName = Format(Mid$(UnitName, 1, 20), "@@@@@@@@@@@@@@@@@@@@!")
    
    If Server <> "" Then
        ServerDate = Server
        If ServerDate <= Now() - 1 Then
            Open "v:\Poll\HeartSIP\" + UnitNumber + "-S.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    Version = FileInput(5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If Version <> "11/12/2021" Then
                List1.AddItem UnitNumber + "  " + TempName + "  " + Version + "  " + RunDate + " " + RunTime + "  " + Server
                Print #4, UnitNumber + "," + UnitName + "," + Version + "," + RunDate + "," + RunTime + "," + Server
    
            Else
                NumberConverted = NumberConverted + 1
            End If
        End If
    End If
    

Loop
Close #1
Label3.Caption = Str(NumberConverted) + " Converted"

Close 4

Exit Sub

ErrorHandler:
   List1.AddItem UnitNumber + "  " + TempName + " Version file missing"
   
Resume Next

End Sub

