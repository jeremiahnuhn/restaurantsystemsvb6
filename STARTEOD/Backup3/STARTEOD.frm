VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Start EOD"
   ClientHeight    =   13125
   ClientLeft      =   -585
   ClientTop       =   -390
   ClientWidth     =   19080
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   13125
   ScaleWidth      =   19080
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   6000
      Left            =   7200
      Top             =   7320
   End
   Begin VB.Label Label3 
      Caption         =   "Version 7"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Starting End of Day"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   1440
      TabIndex        =   1
      Top             =   1920
      Width           =   10455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Please wait..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   1440
      TabIndex        =   0
      Top             =   3720
      Width           =   10455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
On Error Resume Next


msg = "Do you want to start the End of Day process?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "IRIS End of Day"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.
           
    Y = Shell("c:\iris\bin\termuse.bat", vbMinimizedFocus)
   
   If Dir("c:\BNEAPPS\PING.TXT") <> "" Then
        Kill ("c:\BNEAPPS\PING.TXT")
   End If
   'Check for internet connection
    Open "c:\BNEAPPS\PINGGOOGLE.BAT" For Output As #1
        Print #1, "ping 8.8.8.8 >c:\bneapps\ping.txt"
    Close #1
        X = Shell("c:\BNEAPPS\PINGGOOGLE.BAT", vbHide)
    DoEvents
    For Y = 1 To 500000
        DoEvents
    Next Y
    DoEvents

    Open "c:\BNEAPPS\PING.TXT" For Input As #1
       Line Input #1, LineData
       Line Input #1, LineData
       Line Input #1, LineData
       Line Input #1, LineData
       Line Input #1, LineData
       Line Input #1, LineData
       Line Input #1, LineData
    Close #1
    If Mid$(LineData, 1, 4) = "Repl" Then
           'Internet working
           X = Shell("c:\iris\setup\runvbscript.exe wscript.exe c:\iris\scripts\eod.vbs", vbNormalFocus)
    Else
            'Internet not working

       Open "c:\IRIS\INI\EOD.NEW" For Input As #1
       Open "C:\IRIS\INI\EOD.INI" For Output As #2
            Do While Not EOF(1)
                Line Input #1, Inline
                If Mid$(Inline, 1, 21) = "Cmd4=c:\iris\bin\EODR" Then
                                     
                        Print #2, "Cmd4=c:\iris\bin\Drawer.exe /DATE=%businessdate%"
                        Print #2, "Cmd5=c:\iris\bin\CloseDrawer.exe /date=%businessdate% /q /EnableDrawerClose /DisablePrompt /chkdrawersales /PlugIn"
                        Print #2, "Cmd6=c:\iris\bin\CountDrawer.exe /date=%businessdate% /PlugIn /EnableDrawerCount /DisablePrompt /CheckSales /Q"
                        Print #2, "Cmd7=c:\iris\bin\CashManagement.exe -Editor2013 -BDate%businessdate% /plugin /Always"
                        Print #2, "Cmd8=c:\iris\bin\Payroll.exe /Editor5 /StartBDate%businessdate%"
                        Print #2, "Cmd9=c:\iris\bin\printRDL.exe /r=" + Chr(34) + "RPY_Time Corrections Log Report.rdl" + Chr(34) + " /rp=" + Chr(34) + "c:\iris\reports" + Chr(34) + " /landscape"
                        Print #2, "Cmd10=c:\iris\bin\CashManagement.exe /Date=%BusinessDate% /DepositVariance /Always"
                        Print #2, "Cmd11=c:\iris\bin\InvTransEditor.exe -BDate%businessdate% -Tran1"
                        Print #2, "Cmd12=c:\windows\system32\WScript.exe c:\iris\scripts\DepleteInventory.vbs"
                        Line Input #1, Inline
                        Line Input #1, Inline
                        Line Input #1, Inline
                        Line Input #1, Inline
                        Line Input #1, Inline
                        Line Input #1, Inline
                        Line Input #1, Inline
                        Line Input #1, Inline
                        Line Input #1, Inline
                Else
                        Print #2, Inline
            End If
            Loop
            Close 1, 2
       
   
   
   
   ' Log
   Dim uname As String
   Dim unumber As String
   
       MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
       LogText = MyTime + " S  -  EOD Register Status Removed"
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + ",EOD Register Status Removed"
            Close 4
        X = Shell("c:\iris\setup\runvbscript.exe wscript.exe c:\iris\scripts\eod.vbs", vbNormalFocus)
   
End If
   
Else
    End
End If


X = 0

End Sub


Private Sub Timer1_Timer()


    X = X + 1
    If X = 51 Then
        End
    End If
    DoEvents
    Timer1.Enabled = True

End Sub
