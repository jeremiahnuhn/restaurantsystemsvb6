VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   0  'None
   Caption         =   "Start EOD"
   ClientHeight    =   13125
   ClientLeft      =   -585
   ClientTop       =   -390
   ClientWidth     =   19080
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   13125
   ScaleWidth      =   19080
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   6000
      Left            =   7200
      Top             =   7320
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Version 11"
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Starting End of Day"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   1440
      TabIndex        =   1
      Top             =   1920
      Width           =   10455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Please wait..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   1440
      TabIndex        =   0
      Top             =   3720
      Width           =   10455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
On Error Resume Next

msg = Chr(10) + Chr(10) + Chr(10) + "Did you remember to enter your IRIS Inventory Count?  " + Chr(10) + Chr(10) + Chr(10) + "If not, please answer No.  Enter your Inventory Count and then start End of Day again." + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10) + Chr(10) ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "* IRIS INVENTORY COUNT * IRIS INVENTORY COUNT *"  ' Define title.
Response = MsgBox(msg, Style, Title)

If Response = vbYes Then    ' User chose Yes.
           msg = "Do you want to start the End of Day process?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "IRIS End of Day"  ' Define title.
Response = MsgBox(msg, Style, Title)
    
    If Response = vbYes Then    ' User chose Yes.
               X = Shell("c:\iris\setup\runvbscript.exe wscript.exe c:\iris\scripts\eod.vbs", vbNormalFocus)
    Else
   
     End
    End If
Else
   
    End

End If






X = 0

End Sub


Private Sub Label3_Click()
    End
End Sub

Private Sub Timer1_Timer()


    X = X + 1
    If X = 35 Then
        End
    End If
    DoEvents
    Timer1.Enabled = True

End Sub
