VERSION 5.00
Begin VB.Form Employee 
   Caption         =   "Select the Employee"
   ClientHeight    =   10125
   ClientLeft      =   990
   ClientTop       =   885
   ClientWidth     =   9165
   LinkTopic       =   "Form2"
   ScaleHeight     =   10125
   ScaleWidth      =   9165
   Begin VB.CommandButton Add 
      Caption         =   "Add to IRIS"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   4440
      TabIndex        =   19
      Top             =   4920
      Width           =   4335
   End
   Begin VB.TextBox Password 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   7440
      TabIndex        =   2
      Top             =   3840
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   6960
      TabIndex        =   4
      Top             =   8400
      Width           =   1815
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8250
      Left            =   720
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   1440
      Width           =   3375
   End
   Begin VB.Label Label5 
      Caption         =   "Enter the password for this employee to use on IRIS.  Numbers only."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   4680
      TabIndex        =   18
      Top             =   3720
      Width           =   2655
   End
   Begin VB.Label JobCodeLabel 
      Caption         =   "JobCodeLabel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4680
      TabIndex        =   17
      Top             =   3240
      Width           =   4455
   End
   Begin VB.Label EmpID 
      Caption         =   "EmpID"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6720
      TabIndex        =   16
      Top             =   7080
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label EmpIdLabel 
      Caption         =   "EmpIDLabel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4680
      TabIndex        =   15
      Top             =   2760
      Width           =   4455
   End
   Begin VB.Label PayRate 
      Caption         =   "PayRate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4200
      TabIndex        =   14
      Top             =   7800
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label JobCode 
      Caption         =   "JobCode"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6720
      TabIndex        =   13
      Top             =   7800
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label BirthDate 
      Caption         =   "BirthDate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4320
      TabIndex        =   12
      Top             =   7080
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label HireDate 
      Caption         =   "HireDate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6840
      TabIndex        =   11
      Top             =   6360
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label Phone 
      Caption         =   "Phone"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4200
      TabIndex        =   10
      Top             =   6360
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label LastName 
      Caption         =   "LastName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6840
      TabIndex        =   9
      Top             =   5640
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label FirstName 
      Caption         =   "FirstName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4320
      TabIndex        =   8
      Top             =   5640
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label FullName 
      Caption         =   "FullName"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4680
      TabIndex        =   7
      Top             =   2160
      Width           =   4215
   End
   Begin VB.Label Label4 
      Caption         =   "You have selected:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4680
      TabIndex        =   6
      Top             =   1680
      Width           =   4215
   End
   Begin VB.Label Label3 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   5
      Top             =   120
      Width           =   7095
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   480
      TabIndex        =   3
      Top             =   600
      Width           =   6855
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   495
      Left            =   9000
      TabIndex        =   1
      Top             =   360
      Width           =   15
   End
End
Attribute VB_Name = "Employee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Add_Click()
response = MsgBox("This process will add " + RTrim(FullName) + " to the IRIS system.  Do you want to continue?", vbYesNo, "Confirm correct Employee to Add to IRIS")
If response = vbYes Then
    If Dir("c:\BNEAPPS\IBOI\*.*") = "" Then
        MkDir ("c:\BNEAPPS\IBOI")
        Open "c:\BNEAPPS\IBOI\DO NOT REMOVE.TXT" For Append As #5
             Print #5, "Do not remove this file."
        Close #5
    End If

    
    If Password.Text = "" Then
        PasswordResponse = MsgBox("Please enter a password for the Employee to use on IRIS.  Numbers only.", vbExclamation, "Enter Password")
        Exit Sub
    End If
    
    If IsNumeric(Password.Text) = False Then
        PasswordResponse = MsgBox("Please enter a password for the Employee to use on IRIS.  Numbers only.", vbExclamation, "Password must be only numbers")
        Password.Text = ""
        Exit Sub
    End If
    MyTime = Format(Now(), "mmddyy") + "-" + Format(Now(), "hhmmss")
    IBOI = "c:\BNEAPPS\IBOI\T" + unumber + "-" + RTrim(EmpId.Caption) + "-" + MyTime + ".xml"
    IBOIP = "c:\IRIS\IBOI\IN\T" + unumber + "-" + RTrim(EmpId.Caption) + "-" + MyTime + ".xml"
    Open IBOI For Append As #5
        '<?xml version="1.0" encoding="utf-8"?>
        Print #5, "<?xml version=" + Chr(34) + "1.0" + Chr(34) + " encoding=" + Chr(34) + " utf - 8" + Chr(34) + "?>"
        Print #5, "<import>"
        Print #5, "   <employees>"
        Print #5, "      <employee>"
        Print #5, "         <id>" + RTrim(EmpId.Caption) + "</id>"
        Print #5, "         <ssn>" + RTrim(EmpId.Caption) + "</ssn>"
        Print #5, "         <firstname>" + RTrim(FirstName.Caption) + "</firstname>"
        Print #5, "         <lastname>" + RTrim(LastName.Caption) + "</lastname>"
        Print #5, "         <statusid>4</statusid>"
        Print #5, "         <homephone>" + RTrim(Phone.Caption) + "</homephone>"
        Print #5, "         <allowrehire>1</allowrehire>"
        Print #5, "         <isborrowed>0</isborrowed>"
        Print #5, "         <hiredate>" + RTrim(HireDate.Caption) + "</hiredate>"
        Print #5, "         <birthdate>" + RTrim(BirthDate.Caption) + "</birthdate>"
        Print #5, "         <terminationdate/>"
        Print #5, "         <terminationnote/>"
        ' Set Security Case
        Select Case RTrim(JobCode.Caption)
        Case "230824"
            Print #5, "         <securitylevel>14</securitylevel>"
        Case "230805"
            Print #5, "         <securitylevel>14</securitylevel>"
        Case "230820"
            Print #5, "         <securitylevel>15</securitylevel>"
        Case "230822"
            Print #5, "         <securitylevel>15</securitylevel>"
        Case "230832"
            Print #5, "         <securitylevel>18</securitylevel>"
        Case "230855"
            Print #5, "         <securitylevel>18</securitylevel>"
        Case "230845"
            Print #5, "         <securitylevel>20</securitylevel>"
        Case "230810"
            Print #5, "         <securitylevel>20</securitylevel>"
        Case "230840"
            Print #5, "         <securitylevel>30</securitylevel>"
        Case "230825"
            Print #5, "         <securitylevel>30</securitylevel>"
        Case Else
            Print #5, "         <securitylevel>30</securitylevel>"
        End Select

        'Print #5, "         <securitylevel>30</securitylevel>"
        ' Pay Type
        Select Case RTrim(JobCode.Caption)
        Case "230824"
        Print #5, "         <paytypeid>2</paytypeid>"
        Print #5, "         <salary>0</salary>"
        Case "230805"
        Print #5, "         <paytypeid>2</paytypeid>"
        Print #5, "         <salary>0</salary>"
        Case "230820"
        Print #5, "         <paytypeid>2</paytypeid>"
        Print #5, "         <salary>0</salary>"
        Case "230822"
        Print #5, "         <paytypeid>2</paytypeid>"
        Print #5, "         <salary>0</salary>"
        Case Else
        Print #5, "         <paytypeid>1</paytypeid>"
        Print #5, "         <salary/>"
        End Select

        'Print #5, "         <paytypeid>1</paytypeid>"
        'Print #5, "         <salary/>"
        Print #5, "         <password>" + RTrim(Password.Text) + "</password>"
        Print #5, "         <isrehired>1</isrehired>"
        Print #5, "         <jobcodes>"
        Print #5, "            <jobcode>"
        Print #5, "               <jobcodeid>" + RTrim(JobCode.Caption) + "</jobcodeid>"
        Print #5, "               <isprimaryjobcode>1</isprimaryjobcode>"
        
        Select Case RTrim(JobCode.Caption)
        Case "230824"
        Print #5, "               <payrate>0</payrate>"
        Case "230805"
        Print #5, "               <payrate>0</payrate>"
        Case "230820"
        Print #5, "               <payrate>0</payrate>"
        Case "230822"
        Print #5, "               <payrate>0</payrate>"
        Case Else
        Print #5, "               <payrate>" + RTrim(PayRate.Caption) + "</payrate>"
        End Select
        'Print #5, "               <payrate>" + RTrim(PayRate.Caption) + "</payrate>"
        'Format(Now(), "mmddyy")  Format(Now(), "hhmmss")
        '               <effectivedate>2022-03-23T03:45:51-07:00</effectivedate>
        Print #5, "               <effectivedate>" + Format(Now(), "yyyy-mm-dd") + "T" + Format(Now(), "hh:mm:ss") + "-07:00</effectivedate>"
        ' Set Security Case
        Select Case RTrim(JobCode.Caption)
        Case "230824"
        Print #5, "               <securitylevel>14</securitylevel>"
        Case "230805"
        Print #5, "               <securitylevel>14</securitylevel>"
        Case "230820"
        Print #5, "               <securitylevel>15</securitylevel>"
        Case "230822"
        Print #5, "               <securitylevel>15</securitylevel>"
        Case "230832"
        Print #5, "               <securitylevel>18</securitylevel>"
        Case "230855"
        Print #5, "               <securitylevel>18</securitylevel>"
        Case "230845"
        Print #5, "               <securitylevel>20</securitylevel>"
        Case "230810"
        Print #5, "               <securitylevel>20</securitylevel>"
        Case "230840"
        Print #5, "               <securitylevel>30</securitylevel>"
        Case "230825"
        Print #5, "               <securitylevel>30</securitylevel>"
        Case Else
        Print #5, "               <securitylevel>30</securitylevel>"
        End Select
        
        'Print #5, "               <securitylevel>30</securitylevel>"
        Print #5, "               <isactive>1</isactive>"
        Print #5, "            </jobcode>"
        Print #5, "         </jobcodes>"
        Print #5, "         <securitygroups>"
        Print #5, "            <securitygroup>"
        ' Set Security Group
        Select Case RTrim(JobCode.Caption)
        Case "230824"
        Print #5, "               <securitygroupid>1</securitygroupid>"
        Case "230805"
        Print #5, "               <securitygroupid>1</securitygroupid>"
        Case "230820"
        Print #5, "               <securitygroupid>1</securitygroupid>"
        Case "230822"
        Print #5, "               <securitygroupid>1</securitygroupid>"
        Case "230832"
        Print #5, "               <securitygroupid>3</securitygroupid>"
        Case "230855"
        Print #5, "               <securitygroupid>3</securitygroupid>"
        Case "230845"
        Print #5, "               <securitygroupid>4</securitygroupid>"
        Case "230810"
        Print #5, "               <securitygroupid>4</securitygroupid>"
        Case "230840"
        Print #5, "               <securitygroupid>5</securitygroupid>"
        Case "230825"
        Print #5, "               <securitygroupid>5</securitygroupid>"
        Case Else
        Print #5, "               <securitygroupid>5</securitygroupid>"
        End Select
        
        'Print #5, "               <securitygroupid>5</securitygroupid>"
        Print #5, "            </securitygroup>"
        Print #5, "         </securitygroups>"
        Print #5, "      </employee>"
        Print #5, "   </employees>"
        Print #5, "</import>"
      Close #5
      FileCopy IBOI, IBOIP
    
    x = Shell("c:\iris\bin\workday.bat", vbHide)
    
    
    
    
    
    'Y = Shell("C:\ftpdb.bat", vbMinimizedFocus)
    'MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    'LogText = MyTime + " S  - FTP IRIS SQL DB"
    'Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
    '    Print #1, LogText
    'Close #1
    '
    '   End
    '  Exit Sub
Else
    Add.Enabled = False
    FullName.Caption = ""
    JobCode.Caption = ""
    EmpIdLabel.Caption = ""
    JobCodeLabel.Caption = ""
    Password.Text = ""
    Exit Sub
End If




End
End Sub

Private Sub Command1_Click()
End
End Sub

Private Sub Form_Load()
    FullName.Caption = ""
    JobCode.Caption = ""
    EmpIdLabel.Caption = ""
    JobCodeLabel.Caption = ""
    Password.Text = ""
Label2.Caption = "Click on the employee you want to add to your IRIS system from the list below."
Label3.Caption = "You selected: " + SelectedRestaurant


Open "c:\support\workdayempinfo.csv" For Input As #1
    Line Input #1, IndividualLine
    Do While Not EOF(1)

    Line Input #1, IndividualLine
    FileInput = Split(IndividualLine, ",")
    UnitNumber = FileInput(0)
    EmpNumber = FileInput(1)
    EmployeeID = FileInput(1)
    FName = FileInput(2)
    LName = FileInput(3)
    EmpName = FileInput(4)
    Phone = FileInput(5)
    Hire = FileInput(6)
    BDate = FileInput(7)
    JobCode = FileInput(8)
    PayRate = FileInput(9)
            
    TempName = Format(Mid$(EmpName, 1, 25), "@@@@@@@@@@@@@@@@@@@@!")
    If UnitNumber = Mid$(SelectedRestaurant, 31, 4) Then
        List1.AddItem TempName + "," + EmpNumber + "," + FName + "," + LName + "," + Phone + "," + Hire + "," + BDate + "," + JobCode + "," + PayRate
    End If
    Loop
    Close 1
End Sub

Private Sub List1_Click()
Add.Enabled = True
SelectedEmployee = List1.Text
Selected = Split(List1.Text, ",")
FullName.Caption = Selected(0)
EmpIdLabel.Caption = "Employee ID " + Selected(1)
EmpId.Caption = Selected(1)
FirstName.Caption = Selected(3)
LastName.Caption = Selected(2)
Phone.Caption = Selected(4)
HireDate.Caption = Selected(5)
BirthDate.Caption = Selected(6)
JobCode.Caption = Selected(7)
PayRate.Caption = Selected(8)
'JobCodeLabel
    Select Case Selected(7)
        Case "230824"
            JobCodeLabel.Caption = "District Manager in Training"
        Case "230805"
            JobCodeLabel.Caption = "Senior General Manager"
        Case "230820"
            JobCodeLabel.Caption = "General Manager"
        Case "230822"
            JobCodeLabel.Caption = "General Manager in Training"
        Case "230832"
            JobCodeLabel.Caption = "Manager Hourly"
        Case "230855"
            JobCodeLabel.Caption = "Manager In Training"
        Case "230845"
            JobCodeLabel.Caption = "Shift Leader"
        Case "230810"
            JobCodeLabel.Caption = "Breakfast Shift Leader"
        Case "230840"
            JobCodeLabel.Caption = "Crew Trainer"
        Case "230825"
            JobCodeLabel.Caption = "Crew"
        Case Else
            JobCodeLabel.Caption = "Not Found"
        End Select

'EmployeeInfo.Show
'Employee.Hide
End Sub
