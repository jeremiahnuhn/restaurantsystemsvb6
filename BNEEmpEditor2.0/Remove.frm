VERSION 5.00
Begin VB.Form Remove 
   Caption         =   "Remove an Employee form IRIS and LaborPro"
   ClientHeight    =   8235
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10500
   LinkTopic       =   "Form2"
   ScaleHeight     =   8235
   ScaleWidth      =   10500
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7305
      Left            =   240
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   600
      Width           =   3855
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   7320
      TabIndex        =   1
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Remove Employee from IRIS and LaborPro"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5040
      TabIndex        =   0
      Top             =   2760
      Width           =   3615
   End
   Begin VB.Label Label4 
      Caption         =   $"Remove.frx":0000
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   4320
      TabIndex        =   7
      Top             =   4080
      Width           =   5895
   End
   Begin VB.Label Label1 
      Caption         =   "Select the Employee's name from the list below:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   6255
   End
   Begin VB.Label FullName 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4320
      TabIndex        =   5
      Top             =   1440
      Width           =   5415
   End
   Begin VB.Label Label3 
      Caption         =   "Selected Employee:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4320
      TabIndex        =   4
      Top             =   840
      Width           =   4215
   End
   Begin VB.Label Label2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4440
      TabIndex        =   3
      Top             =   2040
      Width           =   5415
   End
End
Attribute VB_Name = "Remove"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()


    Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
    Input #1, unumber, uname, email, junk
    Input #1, unumber, uname, email, junk
    Close #1


Open "c:\support\workdayempinfo.csv" For Input As #1
    Line Input #1, IndividualLine
    Do While Not EOF(1)

    Line Input #1, IndividualLine
    FileInput = Split(IndividualLine, ",")
    UnitNumber = FileInput(0)
    EmpNumber = FileInput(1)
    EmployeeID = FileInput(1)
    FName = FileInput(2)
    LName = FileInput(3)
    EmpName = FileInput(4)
    Phone = FileInput(5)
    Hire = FileInput(6)
    BDate = FileInput(7)
    JobCode = FileInput(8)
    PayRate = FileInput(9)
            
    'TempName = Format(Mid$(EmpName, 1, 25), "@@@@@@@@@@@@@@@@@@@@!")
    If EmployeeID = RemoveID Then
        If UnitNumber = unumber Then
            y = MsgBox("The employee you are trying to remove is assigned to your restaurant in Workday.  To remove this employee form IRIS and LaborPro, you need to complete the termination process on Workday for this employee.  If this employee was transferred in Workday to another restaurant and you are trying to remove them, please try again tomorrow.  If you have trouble removing an employee, please contact the BNE Help Desk for assistance.", vbExclamation, "This employee can't be removed by this process.")
            End
        End If
    End If
    Loop
    Close 1




















response = MsgBox("This process will remove " + LTrim(RTrim(FullName)) + " from IRIS and LaborPro.  Do you want to continue?", vbYesNo, "Confirm correct Employee to remove")
If response = vbYes Then
  
 
 Open "c:\support\EMPPASSCHANGE.BAT" For Output As #1
        Print #1, "REM Created " + Str(Now())
        'If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
            Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\support\EMPPASSCHANGE.SQL"
            Print #1, "Exit"
        'Else
         '   Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\support\EMPPASSCHANGE.SQL"
         '   Print #1, "Exit"
        'End If
      Close #1
       Open "c:\support\EMPPASSCHANGE.SQL" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblemployees] Set StatusID = 3" + " Where EmployeeID = " + RemoveID
        Print #1, "UPDATE [IRIS].[dbo].[tblemployees] Set BadgeNumber = Null" + " Where EmployeeID = " + RemoveID
      
      Close #1
             Open "c:\BNEApps\IRISPassword.Log" For Append As #1
                Print #1, Now(), " Employee ID " + RemoveID + " Removed from IRIS"
             Close #1

      y = Shell("c:\support\EMPPASSCHANGE.BAT", vbHidden)
      
      y = MsgBox("The employee was successfully removed from IRIS and LaborPro.  If you have trouble removing an employee, please contact the BNE Help Desk for assistance.", vbInformation, "Employee Removed")

End If
      End
'End If

End Sub

Private Sub Form_Load()
    If Dir("c:\support\EmpPassword.TXT") <> "" Then
        Open "c:\support\EmpPassword.TXT" For Input As #1
        Line Input #1, LineData
        Line Input #1, LineData
        
        Do While Not EOF(1)
        Line Input #1, LineData
        
        If IsNumeric(Mid$(LineData, 1, 11)) = True Then
            EmpIDPass = Mid$(LineData, 1, 11)
            LNamePass = Mid$(LineData, 13, 20)
            FNamePass = Mid$(LineData, 33, 20)
            FullNamePass = RTrim(FNamePass) + " " + RTrim(LNamePass)
            
    Select Case LTrim(Mid$(LineData, 1, 11))
        Case "8"
            ' Do Nothing
        Case "9"
            ' Do Nothing
        Case "10"
            ' Do Nothing
        Case Else
            List1.AddItem FullNamePass + "                    ," + LTrim(EmpIDPass)
        End Select
        End If
        Loop
        Close 1
    End If
End Sub

Private Sub List1_Click()
Selected = Split(List1.Text, ",")
FullName.Caption = Selected(0)
Label2.Caption = "Employee ID " + Selected(1)
RemoveID = Selected(1)
Command2.Enabled = True
End Sub
