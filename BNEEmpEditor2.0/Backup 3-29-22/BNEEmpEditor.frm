VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "BNE IRIS Employee Editor"
   ClientHeight    =   7725
   ClientLeft      =   795
   ClientTop       =   885
   ClientWidth     =   12210
   LinkTopic       =   "Form1"
   ScaleHeight     =   7725
   ScaleWidth      =   12210
   Begin VB.CommandButton Command3 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1800
      TabIndex        =   0
      Top             =   5040
      Width           =   4815
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Add Employee from another Restaurant (Temporary Transfer)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1800
      TabIndex        =   2
      Top             =   3720
      Width           =   4815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Change an Employee's Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1800
      TabIndex        =   1
      Top             =   2400
      Width           =   4815
   End
   Begin VB.Label Label2 
      Caption         =   $"BNEEmpEditor.frx":0000
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   720
      TabIndex        =   4
      Top             =   6720
      Width           =   10935
   End
   Begin VB.Label Label1 
      Caption         =   $"BNEEmpEditor.frx":0093
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Width           =   11655
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Password.Show
Form1.Hide
End Sub

Private Sub Command2_Click()
Transfer.Show
Form1.Hide
End Sub

Private Sub Command3_Click()
    End
End Sub

