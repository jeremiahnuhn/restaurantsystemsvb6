VERSION 5.00
Begin VB.Form Concept 
   Caption         =   "Drive-Thru Terminal Configuration (T3 and T4)"
   ClientHeight    =   7020
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8640
   LinkTopic       =   "Form2"
   ScaleHeight     =   7020
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command6 
      Caption         =   "Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   7
      Top             =   4440
      Width           =   2295
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   4
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   720
      TabIndex        =   3
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Order Taker (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   2
      Top             =   1800
      Width           =   2295
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Cashier (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   720
      TabIndex        =   1
      Top             =   1800
      Width           =   2295
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Close without making any changes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1560
      TabIndex        =   0
      Top             =   5760
      Width           =   5415
   End
   Begin VB.Label Label3 
      Caption         =   "Terminal 3                                              Terminal 4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1200
      TabIndex        =   6
      Top             =   600
      Width           =   6495
   End
   Begin VB.Label Label2 
      Caption         =   "  Cashier                                               Order Taker"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1200
      TabIndex        =   5
      Top             =   1080
      Width           =   6615
   End
End
Attribute VB_Name = "Concept"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
'T 3 Normal
      X = MsgBox("This process will configure the Cashier Terminal to work as normal.", vbInformation, "Change Terminal 3")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 3 where RegID = 3 and PeriodID = 11"
        Print #1, "Update tblRegTimeConcepts set ConceptID = 9 where RegID = 3 and PeriodID = 12"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T3 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command3_Click()
'T 4 Normal
      X = MsgBox("This process will configure the Order Taker Terminal to work as normal.", vbInformation, "Change Terminal 4")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 2 where RegID = 4 and PeriodID = 11"
        Print #1, "Update tblRegTimeConcepts set ConceptID = 8 where RegID = 4 and PeriodID = 12"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
    Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
    End If
      'HEARTSIP
         If Dir("c:\iris\reginfo\reg3\ini\irisauthsrvr.ini") <> "" Then
            If Dir("c:\iris\reginfo\reg4\ini\irisauthsrvr.ini") <> "" Then
                Kill ("c:\iris\reginfo\reg4\ini\irisauthsrvr.ini")
            End If
            X = MsgBox("Your restaurant is configured with Payment Terminals.  You will need to disconnect the USB Cable Labeled 4 and connect the USB Cable Labeled 3 from the V-Box connected to the Drive-Thru Payment Terminal.  Then you will need to Turn Off and then On Terminal 3.", vbInformation, "Move Cable and Reboot Terminal 3")
            Print #1, "NET USE \\BNE_REG4\C$ /USER:LOCALADMIN 926**629"
            Print #1, "del \\BNE_REG4\C$\iris\ini\irisauthsrvr.ini"
         End If
            Print #1, "Exit"
      Close #1

      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T4 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command4_Click()
'T 3 Dual
      X = MsgBox("This process will configure the Cashier Terminal to work as Order Taker and Cashier.", vbInformation, "Change Terminal 3")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 5 where RegID = 3"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T3 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)

    End If
    X = Shell(exe, vbMaximizedFocus)

      End
      Exit Sub
End Sub

Private Sub Command5_Click()
'T 4 Dual
      X = MsgBox("This process will configure the Order Taker Terminal to work as Order Taker and Cashier.", vbInformation, "Change Terminal 4")
       
            If Hour(Now) < 15 Then
                CheckDate = Date - 1
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date
            End If
       
      Open "c:\dt.vbs" For Output As #1
        Print #1, "ConnectionString = " + Chr(34) + "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\iris\data\poslive.mdb" + Chr(34)
        Print #1, "SQL = " + Chr(34) + "Delete From tblOrder Where State = 4 And BusinessDate <= cdate('" + Str(CheckDate) + "')" + Chr(34)
        Print #1, "Set cn = CreateObject(" + Chr(34) + "ADODB.Connection" + Chr(34) + ")"
        Print #1, "Set cmd = CreateObject(" + Chr(34) + "ADODB.Command" + Chr(34) + ")"
        Print #1, "cn.Open ConnectionString"
        Print #1, "cmd.ActiveConnection = cn"
        Print #1, "cmd.CommandText = SQL"
        Print #1, "cmd.execute"
        Print #1, "cn.Close"
      Close #1
       
       
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 5 where RegID = 4"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
           Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
           Print #1, "C:\windows\system32\wscript.exe c:\DT.vbs"
              
              
      'HEARTSIP
         If Dir("c:\iris\reginfo\reg3\ini\irisauthsrvr.ini") <> "" Then
            FileCopy "c:\iris\reginfo\reg1\ini\irisauthsrvr.ini", "c:\iris\reginfo\reg4\ini\irisauthsrvr.ini"
            X = MsgBox("Your restaurant is configured with Payment Terminals.  You will need to disconnect the USB Cable Labeled 3 and connect the USB Cable Labeled 4 from the V-Box connected to the Drive-Thru Payment Terminal.  Then you will need to Turn Off and then On Terminal 4.", vbInformation, "Move Cable and Reboot Terminal 4")
         End If
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T4 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)

    End If
    X = Shell(exe, vbMaximizedFocus)


      End
    Exit Sub
End Sub

Private Sub Command6_Click()
'T 4 Cashier
      X = MsgBox("This process will configure the Order Taker Terminal to work as Drive-Thru Cashier.", vbInformation, "Change Terminal 4")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 3 where RegID = 4 and PeriodID = 11"
        Print #1, "Update tblRegTimeConcepts set ConceptID = 9 where RegID = 4 and PeriodID = 12"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
    Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
    End If
      'HEARTSIP
         If Dir("c:\iris\reginfo\reg3\ini\irisauthsrvr.ini") <> "" Then
            If Dir("c:\iris\reginfo\reg4\ini\irisauthsrvr.ini") <> "" Then
                Kill ("c:\iris\reginfo\reg4\ini\irisauthsrvr.ini")
            End If
            X = MsgBox("Your restaurant is configured with Payment Terminals.  You will need to disconnect the USB Cable Labeled 4 and connect the USB Cable Labeled 3 from the V-Box connected to the Drive-Thru Payment Terminal.  Then you will need to Turn Off and then On Terminal 3.", vbInformation, "Move Cable and Reboot Terminal 3")
            Print #1, "NET USE \\BNE_REG4\C$ /USER:LOCALADMIN 926**629"
            Print #1, "del \\BNE_REG4\C$\iris\ini\irisauthsrvr.ini"
         End If
            Print #1, "Exit"
      Close #1

      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T4 Cashier"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub
