sc config "OloXpientService" start= disabled
sc config "OloUpdateService" start= disabled
taskkill /F /IM "OloXpientService.exe"
taskkill /F /IM "Updater.exe"
pause
sc config "OloXpientService" start= auto
sc config "OloUpdateService" start= auto
sc start "OloXpientService"
sc start "OloUpdateService"
pause
Exit