VERSION 5.00
Begin VB.Form Concept12 
   Caption         =   "Drive-Thru Terminal Configuration (T1 and T2)"
   ClientHeight    =   5985
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8640
   LinkTopic       =   "Form2"
   ScaleHeight     =   5985
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Close without making any changes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1440
      TabIndex        =   0
      Top             =   4800
      Width           =   5415
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Order Taker"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   840
      TabIndex        =   8
      Top             =   4680
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   7
      Top             =   3360
      Width           =   2295
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   840
      TabIndex        =   6
      Top             =   3360
      Width           =   2295
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   5
      Top             =   2040
      Width           =   2295
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   840
      TabIndex        =   4
      Top             =   2040
      Width           =   2295
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Counter (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   3
      Top             =   720
      Width           =   2295
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Counter (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   840
      TabIndex        =   2
      Top             =   720
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "Terminal 1                                              Terminal 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   6495
   End
End
Attribute VB_Name = "Concept12"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
'T 1 Normal
      X = MsgBox("This process will configure Terminal 1 to work as normal.", vbInformation, "Change Terminal 1")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 1 Where PeriodID = 11 and RegID = 1"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 6 Where PeriodID = 12 and RegID = 1"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      'FileCopy "c:\support\Reg1-appini.ini", "c:\iris\reginfo\reg1\ini\appini.ini"
      'FileCopy "c:\support\Reg1-orderent.ini", "c:\iris\reginfo\reg1\ini\orderent.ini"
      
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command3_Click()
'T 4 Normal
      X = MsgBox("This process will configure Terminal 2 to work as normal.", vbInformation, "Change Terminal 2")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 1 Where PeriodID = 11 and RegID = 2"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 6 Where PeriodID = 12 and RegID = 2"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command4_Click()
'T 1 Dual
      X = MsgBox("This process will configure Terminal 1 to work as Order Taker and Cashier.", vbInformation, "Change Terminal 1")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 5 Where PeriodID = 11 And RegID = 1"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 7 Where PeriodID = 12 And RegID = 1"
      Close #1
            If Hour(Now) < 15 Then
                CheckDate = Date - 1
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date
            End If
      
      Open "c:\dt.vbs" For Output As #1
        Print #1, "ConnectionString = " + Chr(34) + "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\iris\data\poslive.mdb" + Chr(34)
        Print #1, "SQL = " + Chr(34) + "Delete From tblOrder Where State = 4 And BusinessDate <= cdate('" + Str(CheckDate) + "')" + Chr(34)
        Print #1, "Set cn = CreateObject(" + Chr(34) + "ADODB.Connection" + Chr(34) + ")"
        Print #1, "Set cmd = CreateObject(" + Chr(34) + "ADODB.Command" + Chr(34) + ")"
        Print #1, "cn.Open ConnectionString"
        Print #1, "cmd.ActiveConnection = cn"
        Print #1, "cmd.CommandText = SQL"
        Print #1, "cmd.execute"
        Print #1, "cn.Close"
      Close #1
      
 
      
      
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
            Print #1, "C:\windows\system32\wscript.exe c:\DT.vbs"
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)

    End If
    X = Shell(exe, vbMaximizedFocus)

      End
      Exit Sub
End Sub

Private Sub Command5_Click()
'T 2 Dual
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 5 Where PeriodID = 11 and RegID = 2"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 7 Where PeriodID = 12 and RegID = 2"
      Close #1
      
            If Hour(Now) < 15 Then
                CheckDate = Date - 1
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date
            End If
      
      Open "c:\dt.vbs" For Output As #1
        Print #1, "ConnectionString = " + Chr(34) + "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\iris\data\poslive.mdb" + Chr(34)
        Print #1, "SQL = " + Chr(34) + "Delete From tblOrder Where State = 4 And BusinessDate <= cdate('" + Str(CheckDate) + "')" + Chr(34)
        Print #1, "Set cn = CreateObject(" + Chr(34) + "ADODB.Connection" + Chr(34) + ")"
        Print #1, "Set cmd = CreateObject(" + Chr(34) + "ADODB.Command" + Chr(34) + ")"
        Print #1, "cn.Open ConnectionString"
        Print #1, "cmd.ActiveConnection = cn"
        Print #1, "cmd.CommandText = SQL"
        Print #1, "cmd.execute"
        Print #1, "cn.Close"
      Close #1
      
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
           Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
           Print #1, "C:\windows\system32\wscript.exe c:\DT.vbs"
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T2 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)

    End If
    X = Shell(exe, vbMaximizedFocus)


      End
    Exit Sub
End Sub

Private Sub Command6_Click()
'T 1 Cashier
      X = MsgBox("This process will configure Terminal 1 to work as a Drive-Thru Cashier.", vbInformation, "Change Terminal 1")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 3 Where PeriodID = 11 and RegID = 1"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 9 Where PeriodID = 12 and RegID = 1"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Cashier"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command7_Click()
'T 2 Cashier
      X = MsgBox("This process will configure Terminal 2 to work as a Drive-Thru Cashier.", vbInformation, "Change Terminal 1")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 3 Where PeriodID = 11 and RegID = 2"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 9 Where PeriodID = 12 and RegID = 2"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T2 Cashier"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub
End Sub

Private Sub Command8_Click()
'T 1 Order Taker
      X = MsgBox("This process will configure Terminal 1 to work as a Drive-Thru Order Taker.  This is not a normal practice, but can be used if your restaurant is only operating the Drive-Thru.", vbInformation, "Change Terminal 1")
       Open "c:\bneapps\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 2 Where PeriodID = 11 and RegID = 1"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 8 Where PeriodID = 12 and RegID = 1"
      Close #1
      Open "c:\bneapps\swipe.bat" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
        'Win10 Added the next line
            Print #1, "cd " + Chr(32) + "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\bneapps\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      FileCopy "c:\iris\reginfo\reg1\ini\appini.ini", "c:\support\Reg1-appini.ini"
      FileCopy "c:\iris\reginfo\reg1\ini\orderent.ini", "c:\support\Reg1-orderent.ini"
      Open "c:\support\Reg1-appini.ini" For Input As #2
      Open "c:\iris\reginfo\reg1\ini\appini.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 5)
                    Case "[POS]"
                        Print #3, Inline; P
                        Print #3, "DEFAULTID=99999"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
      Open "c:\support\Reg1-orderent.ini" For Input As #2
      Open "c:\iris\reginfo\reg1\ini\orderent.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 10)
                    Case "[Category]"
                        Print #3, Inline
                        Print #3, "Default_Component1=90013"
                    Case "SCREEN_TIM"
                        Print #3, "SCREEN_TIME_OUT=1200"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
      
      
      Y = Shell("c:\bneapps\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Order Taker"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub
End Sub
