VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   Caption         =   "Period End Inventory Count for Food Cost Calculations"
   ClientHeight    =   6795
   ClientLeft      =   4140
   ClientTop       =   2685
   ClientWidth     =   7545
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Fixedsys"
      Size            =   9
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   -1  'True
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "MECOUNT.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6795
   ScaleWidth      =   7545
   Begin Threed.SSCommand PrintVerify 
      Height          =   975
      Left            =   1680
      TabIndex        =   2
      Top             =   2880
      Width           =   4095
      _Version        =   65536
      _ExtentX        =   7223
      _ExtentY        =   1720
      _StockProps     =   78
      Caption         =   "Print &Verification Sheet"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Rounded MT Bold"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   4
      AutoSize        =   2
      Picture         =   "MECOUNT.frx":030A
   End
   Begin Threed.SSCommand EXITbutton 
      Height          =   975
      Left            =   1680
      TabIndex        =   3
      Top             =   4200
      Width           =   4095
      _Version        =   65536
      _ExtentX        =   7223
      _ExtentY        =   1720
      _StockProps     =   78
      Caption         =   "&EXIT"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Rounded MT Bold"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   4
      AutoSize        =   2
      Picture         =   "MECOUNT.frx":0624
   End
   Begin Threed.SSCommand ViewEdit 
      Height          =   975
      Left            =   1680
      TabIndex        =   1
      Top             =   1440
      Width           =   4095
      _Version        =   65536
      _ExtentX        =   7223
      _ExtentY        =   1720
      _StockProps     =   78
      Caption         =   "Enter/Edit &Count"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Rounded MT Bold"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   4
      AutoSize        =   2
      Picture         =   "MECOUNT.frx":093E
   End
   Begin Threed.SSCommand PRINTBLANK 
      Height          =   975
      Left            =   1680
      TabIndex        =   0
      Top             =   120
      Width           =   4095
      _Version        =   65536
      _ExtentX        =   7223
      _ExtentY        =   1720
      _StockProps     =   78
      Caption         =   "Print &Blank Count Sheet"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Rounded MT Bold"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   4
      AutoSize        =   2
      Picture         =   "MECOUNT.frx":0C58
   End
   Begin VB.Label Label2 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Please make sure your Inventory Count is entered into the system before completing End of Day on the first day of the Period."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   1080
      TabIndex        =   6
      Top             =   5640
      Width           =   5535
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   960
      TabIndex        =   4
      Top             =   2520
      Width           =   5415
   End
   Begin VB.Label FILEDATE 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1080
      TabIndex        =   5
      Top             =   5280
      Width           =   5535
   End
   Begin VB.Menu About 
      Caption         =   "&About"
   End
   Begin VB.Menu Exit 
      Caption         =   "E&xit"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub About_Click()
                                                                                         
TEMP = MsgBox("Written by Jody Smith" + Chr(10) + "        July 2007" + Chr(10) + "       Version 3.2", 64, "About")
End Sub

Private Sub Exit_Click()
End
End Sub

Private Sub EXITbutton_Click()
End
End Sub

Private Sub Form_Load()
    TimeStamp = Format$(FileDateTime("C:\MONTHEND\ICM00CJ.D00"), "MM")
    FILEDATE.Caption = "Inventory Data File For Period " + TimeStamp
    Open "C:\MONTHEND\ICM00CJ.D00" For Input As #1
    For X = 1 To MAXNUM
        STOCK(X) = ""
        COST(X) = 0
        ITEM(X) = ""
        MEASURE(X) = ""
        CASECNT(X) = ""
        CAT(X) = ""
        CATNUM(X) = ""
        SEQ(X) = 0
        UCOUNT(X) = 0
    Next X

    
    X = 0
    
    Do While Not EOF(1)
        X = X + 1
        Line Input #1, FILEDATA  ' Read line of data.
        STOCK(X) = Mid$(FILEDATA, 1, 6)
        COST(X) = Val(Mid$(FILEDATA, 8, 7))
        ITEM(X) = Mid$(FILEDATA, 16, 25)
        MEASURE(X) = Mid$(FILEDATA, 42, 8)
        CASECNT(X) = Mid$(FILEDATA, 58, 20)
        CAT(X) = Mid$(FILEDATA, 79, 15)
        CATNUM(X) = Mid$(FILEDATA, 95, 2)
        SEQ(X) = Val(Mid$(FILEDATA, 98, 4))
    Loop
    MAXITEMS = X
    Close #1    ' Close file.

    'Open "C:\NODESYS\UNITID.DAT" For Input As #1
    Open "C:\BNEAPPS\ID\UNIT.TXT" For Input As #1
    Line Input #1, FILEDATA
    Input #1, UNITNUM, UNITNAME
    Close 1
    'UNITNUM = Mid$(FILEDATA, 1, 4)
    'UNITNAME = Mid$(FILEDATA, 5, 19)
    
    On Error GoTo NOCOUNT:

    FIRSTCOUNT = 1
    X = 0
    Open "C:\MONTHEND\COUNT.OUT" For Input As #1
    For X = 1 To MAXITEMS
        Line Input #1, FILEDATA
        UCOUNT(X) = Val(Mid$(FILEDATA, 19, 6))
    Next X
    Close #1
    TimeStamp = FileDateTime("C:\MONTHEND\COUNT.OUT")
    Label1.Caption = "Last Edited: " + Str(TimeStamp)
    Exit Sub
    
NOCOUNT:
    TEMP = "Your Inventory Count has been sent to the home office."
    TEMP = TEMP + "  If you need to make changes to your count, please contact your Accounting Specialist.  Please PRESS CANCEL TO EXIT!" + Chr(10) + Chr(10)
    TEMP = TEMP + "If you are entering a new Inventory Count, please PRESS OK!"
    RESPONSE = MsgBox(TEMP, 17, "Period End Inventory")
        If RESPONSE = 2 Then
            End
        End If

    FIRSTCOUNT = 1
    Label1.Caption = "Last Edited:  New Count"
    PrintVerify.Enabled = False
    Open "C:\MONTHEND\COUNT.OUT" For Output As 1
    DDMMYY = Format$(Now, "MMDDYYYY")
    For X = 1 To MAXITEMS
        Print #1, UNITNUM; DDMMYY; STOCK(X); Format$(UCOUNT(X), "000000")
        UCOUNT(X) = 0
    Next X
    Close #1
    Exit Sub
Resume Next

End Sub

Private Sub PRINTBLANK_Click()

RESPONSE = MsgBox("Are you sure?", 36, "Print Count Sheet")
If RESPONSE = 7 Then
    Exit Sub
Else

PRINTFRM.Caption = "Printing Page # " + P
Load PRINTFRM
PRINTFRM.Show
Form1.MousePointer = 11
P = 1
LINES = 0
PRINTHEADER (P)
TEST = "  "
For X = 1 To MAXITEMS
If CATNUM(X) <> TEST Or LINES = 0 Then
        Printer.FontSize = 12
        Printer.FontBold = True
        Printer.Print "**** GROUP "; CAT(X); " ****"
        Printer.FontBold = False
        Printer.Print
    End If

    TEST = CATNUM(X)
    Printer.FontSize = 12
    BRENDA = 0
    If BRENDA = 1 Then
       Printer.Print ITEM(X); " "; " "; MEASURE(X); "       "; STOCK(X); "        "; CATNUM(X)
       Else
       Printer.Print ITEM(X); " "; " "; MEASURE(X); " ___________   ___________   ___________ "; X
    End If
    Printer.FontSize = 10
    Printer.Print " "; STOCK(X); "  "; CASECNT(X)
    LINES = LINES + 1
    If LINES = 25 Then
        P = P + 1
        Printer.NewPage
        PRINTHEADER (P)
        LINES = 0
    End If
Next X
    TimeStamp = Format$(FileDateTime("C:\MONTHEND\ICM00CJ.D00"), "MM")
    Printer.FontSize = 14
    Printer.Print
    Printer.Print "                   Inventory Data File For Period " + TimeStamp
    Printer.FontSize = 10
Printer.EndDoc
Unload PRINTFRM
Form1.MousePointer = 0
End If
End Sub

Private Sub PRINTHEADER(P)
PRINTFRM.Caption = "Printing Page # " + Str(P)
Printer.FontName = "COURIER NEW"
Printer.FontSize = 10
TODAY = Format$(Now, "MM-DD-YY TTTTT")
Printer.Print TODAY, "      #"; UNITNUM; " "; UNITNAME, "                     PAGE"
Printer.FontBold = True
Printer.FontSize = 14
Printer.Print , "      PERIOD END INVENTORY COUNT FORM              "; P
Printer.Print
Printer.FontBold = False
Printer.FontSize = 10
End Sub

Private Sub PrintVerify_Click()
RESPONSE = MsgBox("Are you sure?", 36, "Print Verify Report")
If RESPONSE = 7 Then
    Exit Sub
Else
PRINTFRM.Caption = "Printing Page # " + P
Load PRINTFRM
PRINTFRM.Show
Form1.MousePointer = 11
P = 1
LINES = 0
PRINTHEADER (P)
TOTALCOST = 0

For X = 1 To MAXITEMS
    Printer.FontSize = 12
    Printer.Print ITEM(X); " "; MEASURE(X); " "; CASECNT(X); "  "; STOCK(X); "    "; Format$(UCOUNT(X), "@@@@@@"); "  ";
    Printer.FontSize = 8
    Printer.Print X;
    Printer.FontSize = 12
    Printer.Print
    LINES = LINES + 1
    If LINES = 25 Then
        P = P + 1
        PRINTFRM.Caption = "Printing Page # " + Str(P)
        Printer.Print " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~"
        Printer.FontBold = True
        Printer.FontSize = 14
        Printer.Print , "      PERIOD END INVENTORY COUNT FORM              "; P
        Printer.Print
        Printer.FontBold = False
        Printer.FontSize = 10
    End If
    If LINES = 50 Then
        Printer.NewPage
        P = P + 1
        PRINTHEADER (P)
        LINES = 0
    End If
    ITEMCOST = UCOUNT(X) * (COST(X) * 0.0001)
    TOTALCOST = TOTALCOST + ITEMCOST
Next X
Printer.Print
Printer.FontSize = 18
Printer.Print , "COST OF INVENTORY "; Format$(TOTALCOST, "$###,###.##")
    TimeStamp = Format$(FileDateTime("C:\MONTHEND\ICM00CJ.D00"), "MM")
    Printer.FontSize = 10
    Printer.Print "If you have items to report to your Accounting Specialist that were not included on this count,"
    Printer.Print "please complete the Outlook Form - Month End Inventory - Extra Items."
    Printer.FontSize = 14
    Printer.Print
    Printer.Print "                   Inventory Data File For Period " + TimeStamp
Printer.FontSize = 10
Printer.EndDoc
Unload PRINTFRM
Form1.MousePointer = 0
End If
End Sub

Private Sub ViewEdit_Click()
    TEMP = "All Invoices received on the last day of the period are to be counted in your Period End Inventory, regardless of the time received."
    RESPONSE = MsgBox(TEMP, 0, "Reminder")
      
    Form1.Hide
    Load CountForm
End Sub

