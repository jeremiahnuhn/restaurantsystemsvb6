Attribute VB_Name = "MECOUNT1"
Global Const MAXNUM = 400


Global Const YELLOW = &HFFFF&
Global Const WHITE = &H80000005
Global Const GRAY = &HC0C0C0
Global UNITNUM As String * 4
Global UNITNAME As String * 20
Global STOCK(1 To MAXNUM) As String * 6
Global COST(1 To MAXNUM)  As Double
Global ITEM(1 To MAXNUM)  As String * 25
Global MEASURE(1 To MAXNUM) As String * 8
Global CASECNT(1 To MAXNUM) As String * 20
Global CAT(1 To MAXNUM) As String * 15
Global CATNUM(1 To MAXNUM) As String * 2
Global SEQ(1 To MAXNUM) As Integer
Global UCOUNT(1 To MAXNUM) As Double

Global MAXITEMS As Integer

Global PAGEHOLDER As Integer
Global ITEMHOLDER As Integer
Global FIRSTCOUNT As Integer

