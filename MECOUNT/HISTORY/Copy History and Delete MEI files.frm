VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Period End Inventory File Utility"
   ClientHeight    =   4980
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9450
   Icon            =   "Copy History and Delete MEI files.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4980
   ScaleWidth      =   9450
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command4 
      Caption         =   "Check for Zero Counts"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6600
      TabIndex        =   1
      Top             =   1440
      Width           =   2655
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Update Missing"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6600
      TabIndex        =   0
      Top             =   240
      Width           =   2655
   End
   Begin VB.ListBox List2 
      Height          =   2595
      Left            =   4800
      TabIndex        =   11
      Top             =   840
      Width           =   1455
   End
   Begin VB.ListBox List1 
      Height          =   2595
      Left            =   240
      TabIndex        =   8
      Top             =   840
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6600
      TabIndex        =   3
      Top             =   3840
      Width           =   2655
   End
   Begin VB.FileListBox File1 
      Height          =   2625
      Left            =   2520
      Pattern         =   "*.mei"
      TabIndex        =   4
      Top             =   840
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Create History and Delete Current Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6600
      TabIndex        =   2
      Top             =   2640
      Width           =   2655
   End
   Begin VB.Label Label7 
      Caption         =   "Label7"
      Height          =   375
      Left            =   4800
      TabIndex        =   13
      Top             =   3480
      Width           =   1455
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "Missing"
      Height          =   255
      Left            =   4800
      TabIndex        =   12
      Top             =   480
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   3480
      Width           =   1455
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Restaurants"
      Height          =   255
      Left            =   360
      TabIndex        =   9
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label Label3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   7
      Top             =   3960
      Width           =   5175
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   375
      Left            =   2520
      TabIndex        =   6
      Top             =   3480
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "\\bnewest\share1\Polling\Poll\PEInv"
      Height          =   255
      Left            =   1800
      TabIndex        =   5
      Top             =   480
      Width           =   2775
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
For x = 0 To File1.ListCount - 1
    Label3.Caption = "Processing file copy " + File1.List(x)
    Form1.Refresh
    FileCopy "\\bnewest\share1\polling\Poll\PEInv\" + File1.List(x), "\\bnewest\share1\polling\Poll\PEInv\History\" + File1.List(x)
Next x
Label3.Caption = " "
Kill "\\bnewest\share1\polling\Poll\PEInv\*.MEI"
File1.Refresh
Label2.Caption = Str(File1.ListCount) + " files received"
End Sub

Private Sub Command2_Click()
End
End Sub

Private Sub Command3_Click()
List2.Clear
File1.Refresh
Label2.Caption = Str(File1.ListCount) + " files received"
For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x
Label7.Caption = Str(List2.ListCount) + " missing"
End Sub

Private Sub Command4_Click()
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Checking file " + File1.List(Y)
    Form1.Refresh
    Open "\\bnewest\share1\polling\poll\PEINV\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, LINEDATA
        If Mid$(LINEDATA, 19, 6) = "000000" Or Mid$(LINEDATA, 19, 6) = IsBlank Then
            zerocount = zerocount + 1
            DoEvents
        End If
    Loop
    Close (1)
    If zerocount > 75 Then
        ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
    End If
            Label3.Caption = File1.List(Y) + " zero count = " + Str(zerocount)
            Form1.Refresh
Next Y
    Label3.Caption = "Zero check complete!"

End Sub

Private Sub Form_Load()

  Open "\\bnewest\share1\polling\Poll\PEInv\Restaurant Listing.txt" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
    If Mid$(unt, 1, 1) = "H" Then
        List1.AddItem Mid$(unt, 4, 4)
    End If
  Loop
  Close (1)

Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bnewest\share1\polling\Poll\PEInv"
Label2.Caption = Str(File1.ListCount) + " files received"

For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x
Label7.Caption = Str(List2.ListCount) + " missing"
End Sub

