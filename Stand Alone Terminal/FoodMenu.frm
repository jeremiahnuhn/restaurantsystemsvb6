VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FF0000&
   BorderStyle     =   0  'None
   ClientHeight    =   15360
   ClientLeft      =   -600
   ClientTop       =   -615
   ClientWidth     =   19200
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   15360
   ScaleWidth      =   19200
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   360
      Top             =   10560
   End
   Begin VB.CommandButton Command3 
      Caption         =   "END"
      Enabled         =   0   'False
      Height          =   735
      Left            =   5040
      TabIndex        =   3
      Top             =   7080
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Start IRIS in Stand Alone Mode (Not Connected to Server)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   1920
      TabIndex        =   1
      Top             =   1200
      Width           =   8055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Start IRIS is Normal Mode (Connected to Server)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   1920
      TabIndex        =   0
      Top             =   3960
      Width           =   8055
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FF0000&
      Caption         =   "Version 3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1920
      TabIndex        =   2
      Top             =   6360
      Width           =   8055
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
' NORMAL MODE
            FileCopy "C:\IRIS\INI\IRISAuthSrvr.ini", "C:\IRIS\INI\IRISAuthSrvr.bak"
            Open "C:\IRIS\INI\IRISAuthSrvr.bak" For Input As #2
            Open "C:\IRIS\INI\IRISAuthSrvr.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 8)
                    Case "ServerPa"
                        Print #3, "ServerPath=\\100.100.100.100\C\Program Files\xpient Solutions\Credit Card Application"
                    Case "Timeout="
                        Print #3, "Timeout=20"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
        G = MsgBox("Terminal configured for Normal Mode.", vbInformation, "Normal Mode")
         X = Shell("C:\IRIS\Setup\RunVBScript.exe wscript.exe c:\iris\scripts\logon.vbs /startiris", vbMinimizedFocus)
        
        End
End Sub

Private Sub Command2_Click()
'  STAND ALONE MODE
            FileCopy "C:\IRIS\INI\IRISAuthSrvr.ini", "C:\IRIS\INI\IRISAuthSrvr.bak"
            Open "C:\IRIS\INI\IRISAuthSrvr.bak" For Input As #2
            Open "C:\IRIS\INI\IRISAuthSrvr.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 8)
                    Case "ServerPa"
                        Print #3, "ServerPath=C:\IRIS\DATA"
                    Case "Timeout="
                        Print #3, "Timeout=10"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3

        G = MsgBox("Terminal configured for Stand Alone Mode.", vbInformation, "Stand Alone Mode")
        X = Shell("C:\IRIS\Setup\RunVBScript.exe wscript.exe c:\iris\scripts\logon.vbs /startiris", vbMinimizedFocus)
        
        End
End Sub

Private Sub Command3_Click()
End
End Sub

Private Sub Form_Load()
'        X = Shell("C:\IRIS\Setup\RunVBScript.exe wscript.exe c:\iris\scripts\logon.vbs /startiris", vbMinimizedFocus)
End Sub

Private Sub Timer1_Timer()
        X = Shell("C:\IRIS\Setup\RunVBScript.exe wscript.exe c:\iris\scripts\logon.vbs /startiris", vbMinimizedFocus)
End

End Sub
