VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H000080FF&
   Caption         =   "Secure Reports"
   ClientHeight    =   6420
   ClientLeft      =   1620
   ClientTop       =   1515
   ClientWidth     =   11505
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "secure.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6420
   ScaleWidth      =   11505
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox Check1 
      BackColor       =   &H000080FF&
      Caption         =   "Filter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9720
      TabIndex        =   8
      Top             =   4440
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton Command2 
      Caption         =   "View"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   240
      TabIndex        =   2
      Top             =   4920
      Width           =   9615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   9960
      TabIndex        =   0
      Top             =   4920
      Width           =   1215
   End
   Begin VB.FileListBox File1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Left            =   240
      Pattern         =   "Payroll_Check_List_UNT_??_??_13.bnr"
      TabIndex        =   1
      Top             =   1440
      Width           =   10935
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H000080FF&
      Caption         =   "Filtering by current year.  Uncheck filter to view all reports."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   7
      Top             =   4440
      Width           =   8895
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      BackColor       =   &H000080FF&
      Caption         =   "Version 3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10320
      TabIndex        =   6
      Top             =   6120
      Width           =   855
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      BackColor       =   &H000080FF&
      Caption         =   "Security is based on the person Signed On to the IRIS system at the time this program is started."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   5760
      Width           =   11055
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H000080FF&
      Caption         =   "Secure Reports only available to the General Manager"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   4
      Top             =   360
      Width           =   10815
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H000080FF&
      Caption         =   "Click on the report you want to view and then click on the View button."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   3
      Top             =   960
      Width           =   10935
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()
If Check1.Value = 0 Then
    File1.FileName = "C:\BNEAPPS\SECURE"
    Label5.Caption = "Showing all Reports."
    File1.Pattern = "*.bnr"
    Form1.Refresh
Else
    XYY = Mid$(Year(Now), 3, 2)
    File1.Pattern = "Payroll_Check_List_UNT_??_??_" + XYY + ".bnr"
    Label5.Caption = "Filtering by current year.  Uncheck filter to view all reports."
End If
End Sub

Private Sub Command1_Click()
If Dir("C:\Program Files\RPTVIEW32\RVRPTSTS\RVROSVFL.LST") <> "" Then
    Kill ("C:\Program Files\RPTVIEW32\RVRPTSTS\RVROSVFL.LST")
End If
    End
End Sub

Private Sub Command2_Click()
    FileName = Trim(File1.List(File1.ListIndex))
    
    If Dir("C:\program files\RptView32\RptVw32.exe") <> "" Then
     
        x = Shell("c:\Program Files\RptView32\RptVw32.exe" + " " + "c:\bneapps\secure\" + FileName, vbMaximizedFocus)
    Else
    
        x = Shell("c:\DREC Software\RptView32 for Windows\RptVw32.exe" + " " + "c:\bneapps\secure\" + FileName, vbMaximizedFocus)
    End If
End Sub

Private Sub File1_Click()
    Command2.Enabled = True
End Sub

Private Sub Form_Load()

    File1.FileName = "C:\BNEAPPS\SECURE"
    XYY = Mid$(Year(Now), 3, 2)
    File1.Pattern = "Payroll_Check_List_UNT_??_??_" + XYY + ".bnr"
    Counter = 0

End Sub

Private Sub Timer1_Timer()
    If Counter = 3 Then
        If Dir("C:\Program Files\RPTVIEW32\RVRPTSTS\RVROSVFL.LST") <> "" Then
            Kill ("C:\Program Files\RPTVIEW32\RVRPTSTS\RVROSVFL.LST")
        End If
        End
    End If
    Counter = Counter + 1
Timer1.Enabled = False
Timer1.Enabled = True

End Sub
