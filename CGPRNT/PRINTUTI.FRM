VERSION 4.00
Begin VB.Form PrintUtility 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "LaserJet Print Utility"
   ClientHeight    =   4935
   ClientLeft      =   1095
   ClientTop       =   810
   ClientWidth     =   6720
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Height          =   5340
   Icon            =   "PRINTUTI.frx":0000
   Left            =   1035
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   6720
   ShowInTaskbar   =   0   'False
   Top             =   465
   Width           =   6840
   Begin VB.CommandButton TonerInstructions 
      Caption         =   "&Toner Instructions"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   0
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   14
      Top             =   3360
      Width           =   1695
   End
   Begin VB.CommandButton View 
      Caption         =   "&View"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   13
      Top             =   2400
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Caption         =   " How this Utility works. "
      Height          =   975
      Left            =   120
      TabIndex        =   11
      Top             =   3840
      Width           =   6495
      Begin VB.Label Label2 
         Caption         =   $"PRINTUTI.frx":0442
         Height          =   615
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   6255
      End
   End
   Begin VB.CommandButton Print14 
      Caption         =   "14"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   7
      Top             =   3240
      Width           =   1695
   End
   Begin VB.CommandButton Print7 
      Caption         =   "&7"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   6
      Top             =   3240
      Width           =   1695
   End
   Begin VB.CommandButton Print6 
      Caption         =   "&6"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   5
      Top             =   2520
      Width           =   1695
   End
   Begin VB.CommandButton Print5 
      Caption         =   "&5"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   4
      Top             =   2520
      Width           =   1695
   End
   Begin VB.CommandButton Print4 
      Caption         =   "&4"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   3
      Top             =   1800
      Width           =   1695
   End
   Begin VB.CommandButton Print3 
      Caption         =   "&3"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   1800
      Width           =   1695
   End
   Begin VB.CommandButton Print2 
      Caption         =   "&2"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   1
      Top             =   1080
      Width           =   1695
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2175
      Left            =   4320
      Picture         =   "PRINTUTI.frx":054B
      ScaleHeight     =   2169.93
      ScaleMode       =   0  'User
      ScaleWidth      =   2055
      TabIndex        =   9
      Top             =   120
      Width           =   2055
   End
   Begin VB.CommandButton Print1 
      Caption         =   "&1"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   15.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   1080
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Cancel"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   8
      Top             =   2880
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   $"PRINTUTI.frx":5F9D
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   10
      Top             =   240
      Width           =   3495
   End
End
Attribute VB_Name = "PrintUtility"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub Command1_Click()
    End
End Sub


Private Sub Command2_Click()

End Sub

Private Sub Form_Load()
    TempName = Command
    BackSlash = 0
    For X = 1 To Len(TempName)
        If Mid$(TempName, X, 1) = "\" Then
            BackSlash = BackSlash + 1
        End If
        If BackSlash = 5 Then
            PrintName = ReportName + Mid$(TempName, X, (Len(TempName) - X + 1))
            X = Len(TempName)
        End If
    Next X
    
    
    Landscape = False
    Open "C:\WINDOWS\CGPRINT.LST" For Input As #1
    Do While Not EOF(1)
        Input #1, ReportName
        If UCase(ReportName) = UCase(PrintName) Then
            Landscape = True
            Close 1
            Exit Sub
        End If
    Loop
    Close 1
End Sub

Private Sub Print1_Click()
    PrintCommands (1)
End Sub



Public Sub PrintCommands(NumCopies)
    MousePointer = 11
    ProgName = "C:\WINDOWS\CGMINIVW.EXE " + Command
    X = Shell(ProgName, 3)
    'AppActivate x '"Common Ground Mini Viewer - 00004000.DP"
    If Landscape = False Then
        SendKeys "%FP{TAB 4}" + Str(NumCopies) + "{TAB 3}{ENTER}", True
    Else
        SendKeys "%FP{TAB 4}" + Str(NumCopies) + "{TAB 5}{ENTER}{TAB}{DOWN}{TAB 3}{ENTER 2}", True
    End If
    For X = 1 To 5000
        DoEvents
    Next X
    SendKeys "%FX", True
    'DoEvents
    'SendKeys "%{F4}", True  ' Send ALT+F4 to close Calculator.
    End

End Sub

Private Sub Print14_Click()
    PrintCommands (14)
End Sub

Private Sub Print2_Click()
    PrintCommands (2)
End Sub


Private Sub Print3_Click()
        PrintCommands (3)
End Sub


Private Sub Print4_Click()
    PrintCommands (4)
End Sub


Private Sub Print5_Click()
    PrintCommands (5)
End Sub


Private Sub Print6_Click()
    PrintCommands (6)
End Sub


Private Sub Print7_Click()
    PrintCommands (7)
End Sub


Private Sub TonerInstructions_Click()
ProgName = "C:\WINDOWS\CGMINIVW.EXE \TRAINING\TONERINS.DP"
X = Shell(ProgName, 3)
End
End Sub

Private Sub View_Click()
    ProgName = "C:\WINDOWS\CGMINIVW.EXE " + Command
    X = Shell(ProgName, 3)
    If Landscape = True Then
        SendKeys "%FR{TAB}{DOWN}{TAB 3}{ENTER}", True
    End If
    End
End Sub


