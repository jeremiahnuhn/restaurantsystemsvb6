VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
msg = "Did you enter your Deposit?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Deposit"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.
   End
Else

   Settle = MsgBox(Chr(10) + Chr(10) + "When you click on the OK button the Server will restart." + Chr(10) + Chr(10) + "After the Server restarts, click on the End of Day button again." + Chr(10) + Chr(10) + "If you are not asked the enter your deposit on the second End of Day, please contact the Help Desk before entering your Inventory." + Chr(10) + Chr(10) + Chr(10), vbCritical, " I M P O R T A N T :  Deposit not entered!")
   x = Shell("c:\iris\setup\runvbscript.exe wscript.exe c:\iris\scripts\shutdown.vbs /RESTART /delay=2 /Force", vbNormalFocus)
   End
End If
End Sub
