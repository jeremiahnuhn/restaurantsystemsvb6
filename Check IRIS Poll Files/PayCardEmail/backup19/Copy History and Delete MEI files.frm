VERSION 5.00
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form Form1 
   Caption         =   "Pay Card File and Email"
   ClientHeight    =   9045
   ClientLeft      =   420
   ClientTop       =   825
   ClientWidth     =   13890
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   12
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Copy History and Delete MEI files.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9045
   ScaleWidth      =   13890
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   3840
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   2880
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.CommandButton Command16 
      Caption         =   "Check QTimer Data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      TabIndex        =   65
      Top             =   7080
      Width           =   2295
   End
   Begin VB.CommandButton Command15 
      Caption         =   "Create Pay Card Data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      TabIndex        =   64
      Top             =   6120
      Width           =   2295
   End
   Begin VB.ListBox List10 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   1920
      TabIndex        =   62
      Top             =   2160
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CheckBox Check6 
      Caption         =   "6 pm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   7200
      TabIndex        =   60
      Top             =   4200
      Width           =   735
   End
   Begin VB.CheckBox Check5 
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   6600
      TabIndex        =   59
      Top             =   4200
      Width           =   375
   End
   Begin VB.CheckBox Check4 
      Caption         =   "4 "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   6000
      TabIndex        =   58
      Top             =   4200
      Width           =   375
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Display Name"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   12360
      TabIndex        =   56
      Top             =   3225
      Value           =   1  'Checked
      Width           =   1335
   End
   Begin VB.ListBox RestaurantListing 
      Height          =   360
      Left            =   120
      TabIndex        =   55
      Top             =   7800
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.TextBox CASHVAR 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   9360
      TabIndex        =   53
      Text            =   "2000"
      Top             =   4200
      Width           =   495
   End
   Begin VB.CommandButton ButtonEditS103 
      Caption         =   "Edit Tender Records (S103)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   10080
      TabIndex        =   52
      Top             =   4200
      Width           =   3615
   End
   Begin VB.CommandButton CmdTime 
      Caption         =   "Create Time Adjustment Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      TabIndex        =   50
      Top             =   5640
      Width           =   2295
   End
   Begin VB.CheckBox Texas 
      Caption         =   "Texas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4800
      TabIndex        =   49
      Top             =   6840
      Width           =   855
   End
   Begin VB.CheckBox VersionCheckBox 
      Caption         =   "Version 3.7.8.2 151"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3000
      TabIndex        =   48
      Top             =   7920
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton Command14 
      Caption         =   "Check S100 and S101"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      TabIndex        =   47
      Top             =   6600
      Width           =   2295
   End
   Begin VB.ListBox List9 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2580
      Left            =   10320
      TabIndex        =   44
      Top             =   600
      Width           =   3375
   End
   Begin VB.CommandButton Command13 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10320
      TabIndex        =   43
      Top             =   3480
      Width           =   3375
   End
   Begin VB.ListBox List8 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   6960
      TabIndex        =   40
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command12 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6960
      TabIndex        =   39
      Top             =   3480
      Width           =   1455
   End
   Begin VB.TextBox JobCodeTextBox 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   8880
      TabIndex        =   38
      Text            =   "Job Code"
      Top             =   7560
      Width           =   975
   End
   Begin VB.CommandButton Command11 
      Caption         =   "Search for   Job Code"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7560
      TabIndex        =   37
      Top             =   7560
      Width           =   1215
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Create Tax File"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      TabIndex        =   36
      Top             =   5160
      Width           =   2295
   End
   Begin VB.ListBox List7 
      Height          =   660
      Left            =   2040
      TabIndex        =   34
      Top             =   7200
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Check EDM Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   10080
      TabIndex        =   33
      Top             =   4920
      Width           =   3615
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Create Payroll Verification Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7560
      TabIndex        =   32
      Top             =   4680
      Width           =   2295
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Only Hardee's"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   5880
      TabIndex        =   31
      Top             =   6840
      Width           =   1335
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Check All"
      Height          =   615
      Left            =   10080
      TabIndex        =   0
      Top             =   5640
      Width           =   3615
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8640
      TabIndex        =   4
      Top             =   3480
      Width           =   1455
   End
   Begin VB.ListBox List6 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   8640
      TabIndex        =   27
      Top             =   600
      Width           =   1455
   End
   Begin VB.ListBox List5 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      Left            =   240
      TabIndex        =   22
      Top             =   4560
      Width           =   1455
   End
   Begin VB.ListBox List4 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      Left            =   1920
      TabIndex        =   20
      Top             =   4560
      Width           =   5415
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   7200
      Width           =   1455
   End
   Begin VB.ListBox List3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   5280
      TabIndex        =   17
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5280
      TabIndex        =   3
      Top             =   3480
      Width           =   1455
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3600
      TabIndex        =   2
      Top             =   3480
      Width           =   1455
   End
   Begin VB.ListBox List2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   3600
      TabIndex        =   14
      Top             =   600
      Width           =   1455
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   240
      TabIndex        =   11
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   10080
      TabIndex        =   1
      Top             =   6360
      Width           =   3615
   End
   Begin VB.FileListBox File1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Left            =   1920
      Pattern         =   "*.irs"
      TabIndex        =   7
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Create History and Delete Current Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   0
      TabIndex        =   6
      Top             =   8160
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Label Label27 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1920
      TabIndex        =   63
      Top             =   3240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label25 
      Caption         =   "Time Filter After"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4440
      TabIndex        =   57
      Top             =   4200
      Width           =   1455
   End
   Begin VB.Label Label24 
      Alignment       =   2  'Center
      Caption         =   "Cash O/S Filter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8160
      TabIndex        =   54
      Top             =   4320
      Width           =   1095
   End
   Begin VB.Label Label23 
      Caption         =   "             Difference   Tender      Settled  "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10320
      TabIndex        =   51
      Top             =   360
      Width           =   3135
   End
   Begin VB.Label Label22 
      Alignment       =   2  'Center
      Caption         =   "Credit Card Balance"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10320
      TabIndex        =   46
      Top             =   120
      Width           =   3255
   End
   Begin VB.Label Label21 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10320
      TabIndex        =   45
      Top             =   3240
      Width           =   1935
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Caption         =   "Missing X100"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6960
      TabIndex        =   42
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label19 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6960
      TabIndex        =   41
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Label Label18 
      Caption         =   "Label18"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1920
      TabIndex        =   35
      Top             =   6960
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   240
      TabIndex        =   30
      Top             =   3600
      Width           =   3255
   End
   Begin VB.Line Line1 
      X1              =   240
      X2              =   13680
      Y1              =   4080
      Y2              =   4080
   End
   Begin VB.Label Label16 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8640
      TabIndex        =   29
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      Caption         =   "Missing P100"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8640
      TabIndex        =   28
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label14 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   26
      Top             =   7200
      Width           =   2415
   End
   Begin VB.Label Label13 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   25
      Top             =   6960
      Width           =   2295
   End
   Begin VB.Label Label12 
      Caption         =   "Label9"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   24
      Top             =   6960
      Width           =   1335
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Caption         =   "Missing G100 "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   23
      Top             =   4200
      Width           =   1455
   End
   Begin VB.Label Label10 
      Caption         =   "Gift Card Transactions Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2040
      TabIndex        =   21
      Top             =   4200
      Width           =   2175
   End
   Begin VB.Label Label9 
      Caption         =   "Label9"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5280
      TabIndex        =   19
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Caption         =   "Missing R102"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5280
      TabIndex        =   18
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "Label7"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3600
      TabIndex        =   16
      Top             =   3240
      Width           =   1455
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "Missing Poll Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3600
      TabIndex        =   15
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   13
      Top             =   3240
      Width           =   1455
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Restaurants"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   12
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   5880
      TabIndex        =   10
      Top             =   8160
      Width           =   7815
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1920
      TabIndex        =   9
      Top             =   3240
      Width           =   1575
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "\\BNEWest\Polling\Poll"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1680
      TabIndex        =   8
      Top             =   240
      Width           =   1935
   End
   Begin VB.Label Label26 
      Alignment       =   2  'Center
      Caption         =   "Filtered by Time"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   1800
      TabIndex        =   61
      Top             =   1920
      Visible         =   0   'False
      Width           =   1695
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ButtonEditS103_Click()
EditS103.Show

End Sub

Private Sub Check1_Click()
List1.Clear
Call Form_Load
Call Command3_Click
End Sub

Private Sub Check4_Click()
If Check4 = 1 Then

Check6.Value = 0
Check5.Value = 0
File1.Height = 1065
Label2.Top = 1680
Label26.Visible = True
Label27.Visible = True
List10.Visible = True

' Check only pollfiles created after 4:00 pm

List10.Clear
For Y = 0 To File1.ListCount - 1
    
    PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
    'If PollHour >= 16 Or PollHour < 15 Then
        List10.AddItem File1.List(Y)
    'End If
Next Y


Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bns1\share1\polling\poll\Processed\"
Label2.Caption = Str(File1.ListCount) + " files received"

Label7.Caption = Str(List2.ListCount) + " missing"
Label9.Caption = Str(List3.ListCount) + " missing"
Label19.Caption = Str(List8.ListCount) + " missing"
Label12.Caption = Str(List5.ListCount) + " missing"
Label16.Caption = Str(List6.ListCount) + " missing"
Label21.Caption = Str(List9.ListCount) + " Out of Balance"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
Label27.Caption = Str(List10.ListCount) + " after 4 pm"
Form1.Refresh
End If
If Check4 = 0 Then
File1.Height = 2625
Label2.Top = 3240
Label26.Visible = False
Label27.Visible = False
List10.Visible = False
Form1.Refresh
End If
End Sub

Private Sub Check5_Click()
If Check5 = 1 Then

Check4.Value = 0
Check6.Value = 0
File1.Height = 1065
Label2.Top = 1680
Label26.Visible = True
Label27.Visible = True
List10.Visible = True

' Check only pollfiles created after 5:00 pm

List10.Clear
For Y = 0 To File1.ListCount - 1
    
    PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
    If PollHour >= 17 Or PollHour < 15 Then
        List10.AddItem File1.List(Y)
    End If
Next Y


Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bns1\share1\polling\poll\Processed\"
Label2.Caption = Str(File1.ListCount) + " files received"

Label7.Caption = Str(List2.ListCount) + " missing"
Label9.Caption = Str(List3.ListCount) + " missing"
Label19.Caption = Str(List8.ListCount) + " missing"
Label12.Caption = Str(List5.ListCount) + " missing"
Label16.Caption = Str(List6.ListCount) + " missing"
Label21.Caption = Str(List9.ListCount) + " Out of Balance"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
Label27.Caption = Str(List10.ListCount) + " after 5 pm"
Form1.Refresh
End If
If Check5 = 0 Then
File1.Height = 2625
Label2.Top = 3240
Label26.Visible = False
Label27.Visible = False
List10.Visible = False
Form1.Refresh
End If
End Sub

Private Sub Check6_Click()
If Check6 = 1 Then

Check4.Value = 0
Check5.Value = 0
File1.Height = 1065
Label2.Top = 1680
Label26.Visible = True
Label27.Visible = True
List10.Visible = True

' Check only pollfiles created after 6:00 pm

List10.Clear
For Y = 0 To File1.ListCount - 1
    
    PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
    If PollHour >= 18 Or PollHour < 15 Then
        List10.AddItem File1.List(Y)
    End If
Next Y


Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bns1\share1\polling\poll\Processed\"
Label2.Caption = Str(File1.ListCount) + " files received"

Label7.Caption = Str(List2.ListCount) + " missing"
Label9.Caption = Str(List3.ListCount) + " missing"
Label19.Caption = Str(List8.ListCount) + " missing"
Label12.Caption = Str(List5.ListCount) + " missing"
Label16.Caption = Str(List6.ListCount) + " missing"
Label21.Caption = Str(List9.ListCount) + " Out of Balance"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
Label27.Caption = Str(List10.ListCount) + " after 6 pm"
Form1.Refresh
End If
If Check6 = 0 Then
File1.Height = 2625
Label2.Top = 3240
Label26.Visible = False
Label27.Visible = False
List10.Visible = False
Form1.Refresh
End If
End Sub

Private Sub CmdTime_Click()
'Add Cafe and Moe's
List1.Clear
Call Form_Load
Check1.Value = 0
Call Command3_Click

'Add Texas and Diner
  Open "\\bns1\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
    File1.Pattern = "*.EPL;*.IRS"
        If Mid$(unt, 1, 1) = "T" Or Mid$(unt, 1, 1) = "F" Then
            List1.AddItem Mid$(unt, 4, 4)
        End If
  Loop
  Close (1)

Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bns1\share1\polling\poll\Processed\"
Label2.Caption = Str(File1.ListCount) + " files received"

Dim RestNum As Integer
Dim RestCount As Integer

Open "\\bns1\share1\polling\DATA\TimeAdjustDetailI.CSV" For Output As #2
Open "\\bns1\share1\polling\DATA\TimeAdjustDetailM.CSV" For Output As #3
'Print #2, "BneRefNum,StoreNum,BusinessDate,SSN,EmployeeNumber,FirstName,LastName,WageOriginal,JobCodeOriginal,DescriptionOriginal,WageAdjusted,JobCodeAdjusted,DescriptionAdjusted,DelFlag,ClockInDateOriginal,ClockInTimeOriginal,ClockInDateAdjusted,ClockInTimeAdjusted,ClockOutDateOriginal,ClockOutTimeOriginal,ClockOutDateAdjusted,ClockOutTimeAdjusted,TipsOriginal,TipsAdjusted,AdjustedBySSN,AdjustedByFirstName,AdjustedByLastName,AdjustmentReason,AdjustedDate,AdjustedTime"
For Y = 0 To File1.ListCount - 1
    RestNum = Val(Mid$(File1.List(Y), 1, 4))
    RestCount = 0
    zerocount = 0
    Label3.Caption = "Creating Time Adjustment File for " + File1.List(Y)
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "P107" Then
            Print #2, linedata
        End If
        If Mid$(linedata, 2, 4) = "P105" Then
            Print #3, linedata
        End If
    Loop
    Close (1)
            Form1.Refresh
Next Y
Close #2
Close #3


Open "\\bns1\share1\polling\DATA\TimeAdjustDetailI.CSV" For Input As #2
Open "\\bns1\share1\polling\DATA\TimeAdjustSummary.CSV" For Output As #1
    RestNum = "0000"
    Do Until EOF(2)
        Input #2, BneRefNum, StoreNum, BusinessDate, SSN, EmployeeNumber, FirstName, LastName, WageOriginal, JobCodeOriginal, DescriptionOriginal, WageAdjusted, JobCodeAdjusted, DescriptionAdjusted, DelFlag, ClockInDateOriginal, ClockInTimeOriginal, ClockInDateAdjusted, ClockInTimeAdjusted, ClockOutDateOriginal, ClockOutTimeOriginal, ClockOutDateAdjusted, ClockOutTimeAdjusted, TipsOriginal, TipsAdjusted, AdjustedBySSN, AdjustedByFirstName, AdjustedByLastName, AdjustmentReason, AdjustedDate, AdjustedTime
        If StoreNum <> RestNum Then
            If RestNum <> "0000" Then
            Print #1, Str(RestNum) + "," + Str(RestTotalCount) + "," + Str(RestDelCount) + "," + Str(RestSAMCount)
            End If
            RestTotalCount = 0
            RestDelCount = 0
            RestSAMCount = 0
            RestNum = StoreNum
        End If
        If AdjustmentReason = "STAND ALONE MODE" Then
            RestSAMCount = RestSAMCount + 1
        End If
        If DelFlag = "1" Then
            RestDelCount = RestDelCount + 1
        End If
        RestTotalCount = RestTotalCount + 1
    Loop
    'Print #1, Str(RestNum) + "," + Str(RestTotalCount) + "," + Str(RestDelCount) + "," + Str(RestSAMCount)

    Close (2)
    
'MICROS
    RestNum = "0000"
    Open "\\bns1\share1\polling\DATA\TimeAdjustDetailM.CSV" For Input As #2
    Do Until EOF(2)
        Input #2, BneRefNum, StoreNum, BusinessDate, Junk1, Junk2, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Junk9, Junk10, Junk11, Junk12
        RestTotalCount = RestTotalCount + 1
        If StoreNum <> RestNum Then
            If RestNum <> "0000" Then
                Print #1, Str(RestNum) + "," + Str(RestTotalCount) + "," + Str(RestDelCount) + "," + Str(RestSAMCount)
            End If
            RestTotalCount = 0
            RestDelCount = 0
            RestSAMCount = 0
            RestNum = StoreNum
        End If
    Loop
    Print #1, Str(RestNum) + "," + Str(RestTotalCount) + "," + Str(RestDelCount) + "," + Str(RestSAMCount)
    Close (2)
    Close (1)

Label3.Caption = "Time Adjustment File Complete! "
List1.Clear
Check1.Value = 1
Texas.Value = 0
'Call Form_Load
Call Command3_Click
End Sub

Private Sub Command1_Click()
For x = 0 To File1.ListCount - 1
    Label3.Caption = "Processing file copy " + File1.List(x)
    Form1.Refresh
    FileCopy "\\bns1\share1\polling\Poll\PEInv\" + File1.List(x), "\\bns1\share1\polling\Poll\PEInv\History\" + File1.List(x)
Next x
Label3.Caption = " "
Kill "\\bns1\share1\polling\Poll\PEInv\*.MEI"
File1.Refresh
Label2.Caption = Str(File1.ListCount) + " files received"
End Sub

Private Sub Command10_Click()
Open "\\bns1\share1\polling\DATA\SalesTax.CSV" For Output As #2
Print #2, "BneRefNum,StoreNum,BusinessDate,NetSales,Tax1,Tax2,Tax3,Tax4,Tax5,TaxedSales1,TaxedSales2,TaxedSales3,TaxedSales4,TaxedSales5,QtyOverringsVoids,TotalDollarAmtOfOverringsVoids,QtyItemRefunds,TotalDollarAmtOfItemRefunds,QtyClearOrder,TotalDollarAmtOfClearOrder,QtyItemCancels,TotalDollarAmtOfItemCancels,QtyDeleteAfterTotal,TotalDollarAmtOfDeleteAfterTotal,QtyTaxExempt,TotalDollarAmtOfTaxExempt,QtyNoSales,GiftCardSales"
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating Sales Tax File for " + File1.List(Y)
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "S100" Then
            Print #2, linedata
        End If
    Loop
    Close (1)
            Form1.Refresh
Next Y
    Label3.Caption = "Sales Tax File Complete! \\bns1\share1\polling\Data\SalesTax.CSV"
Close #2

End Sub

Private Sub Command11_Click()
List6.Clear
If Dir("\\bns1\share1\polling\poll\Payroll Verify\*.*") <> "" Then
    Kill ("\\bns1\share1\polling\poll\Payroll Verify\*.*")
End If
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Form1.Refresh
    Label3.Caption = "Creating CSV file " + File1.List(Y)
    Open "\\bns1\share1\polling\poll\Payroll Verify\" + Mid$(File1.List(Y), 1, 4) + ".CSV" For Output As #2
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "P100" Then
            zerocount = zerocount + 1
            Print #2, linedata
            'DoEvents
        End If
    Loop
    Close (1)
    Close (2)
    'If zerocount < 5 Then
    '    'ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
    '     z = MsgBox((File1.List(Y)), vbCritical, "Error")
    'End If
    DoEvents
Next Y

            Open "\\bns1\share1\polling\Data\Job Code.CSV" For Output As #2
            Print #2, "####,SS,First Name,Last Name,Job Code,Employee #"
            Close (2)


For Y = 0 To File1.ListCount - 1
    RestTotal = 0
    Label3.Caption = "Searching for " + JobCodeTextBox.Text + " on " + File1.List(Y)
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Payroll Verify\" + Mid$(File1.List(Y), 1, 4) + ".CSV" For Input As #1
     Do Until EOF(1)
        Input #1, Ref, StoreNum, BDate, SS, Emp#, FName, LName, Wage, JobCode, TimeType, Day1, Day2, Day3, Day4, Day5, Day6, Day7, J1, J2, J3, J4, J5
        If JobCode = JobCodeTextBox.Text And TimeType = 0 Then
            List6.AddItem Mid$(File1.List(Y), 1, 4)
            Open "\\bns1\share1\polling\Data\Job Code.CSV" For Append As #2
            Print #2, Str(StoreNum) + "," + Str(SS) + "," + FName + "," + LName + "," + Str(JobCode) + "," + Str(Emp#)
            Close (2)
            Label16.Caption = Str(List6.ListCount) + " found"
            Form1.Refresh
        End If
    Loop
    Close (1)
Next Y
    Label3.Caption = "Find Job Code Complete! \\bns1\share1\polling\Data\Job Code.CSV"
Label16.Caption = Str(List6.ListCount) + " found"

End Sub

Private Sub Command12_Click()
List8.Clear
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Checking X100 " + File1.List(Y)
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "X100" Then
            zerocount = zerocount + 1
            DoEvents
        End If
    Loop
    Close (1)
    If zerocount = 0 Then
        'ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List8.AddItem Mid$(File1.List(Y), 1, 4)
    End If
            Label3.Caption = File1.List(Y) + " zero count = " + Str(zerocount)
            Label19.Caption = Str(List8.ListCount) + " missing"
            Form1.Refresh
Next Y
    Label3.Caption = "X100 Check Complete!"
Label19.Caption = Str(List8.ListCount) + " missing"

End Sub

Private Sub Command13_Click()
  

  Open "\\bns1\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
           RestaurantListing.AddItem unt
  Loop
  Close (1)
R = 0
If Dir("c:\IRISCHECK\CHECK.TXT") = "" Then
    MkDir ("c:\IRISCHECK\")
    Open ("c:\IRISCHECK\CHECK.TXT") For Output As #6
            Print #6, "DO NOT DELETE THIS FILE"
    Close #6
End If
List2.Clear
List3.Clear
List10.Clear
File1.Refresh
Label9 = "Working ..."
Label8 = "Cash O/S"
Label2.Caption = Str(File1.ListCount) + " files received"
For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
            If Check4 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 16 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check5 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 17 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check6 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 18 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check6 = 0 And Check4 = 0 And Check5 = 0 Then
                    List10.AddItem File1.List(Y)
            End If
        
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x
Label7.Caption = Str(List2.ListCount) + " missing"
ButtonEditS103.Enabled = True
If Dir("c:\IRISCHECK\*.csv") <> "" Then
    Kill ("c:\IRISCHECK\*.csv")
End If
E# = 0
C# = 0
List9.Clear
    R = R + 1
    If R = 25 Then
        Form1.Refresh
        R = 0
    End If
Label2.Caption = Str(File1.ListCount) + " files received"
Label21.Caption = "Working ..."


    For Y = 0 To List10.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating S103 work file " + Mid$(List10.List(Y), 1, 4)
    
'PHASE 1 Create Work Files S103
    Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "S.csv") For Output As #6
    Open "\\bns1\share1\polling\poll\Processed\" + List10.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "S103" Then
            Print #6, linedata
        End If
    Loop
    Close #1, #6
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
Next Y
R = 0
DoEvents
Form1.Refresh
For Y = 0 To List10.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating S101 work file " + Mid$(List10.List(Y), 1, 4)
'PHASE 1 Create Work Files S100
    Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "S1.csv") For Output As #6
    Open "\\bns1\share1\polling\poll\Processed\" + List10.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "S100" Then
            Print #6, linedata
        End If
    Loop
    Close #1, #6
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
Next Y
R = 0
DoEvents
Form1.Refresh
For Y = 0 To List10.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating R102 work file " + Mid$(List10.List(Y), 1, 4)
'PHASE 1 Create Work Files R102
    Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "R.csv") For Output As #6
    Open "\\bns1\share1\polling\poll\Processed\" + List10.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "R102" Then
            Print #6, linedata
        End If
    Loop
    Close #1, #6
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
Next Y
R = 0
DoEvents
Form1.Refresh
For Y = 0 To List10.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating X100 work file " + Mid$(List10.List(Y), 1, 4)
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
'PHASE 1 Create Work Files X100
    Open "\\bns1\share1\polling\poll\Processed\" + List10.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "X100" Then
            Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "X.csv") For Append As #6
            Print #6, linedata
            Close #6
        End If
    Loop
       'Print #6, "X100, 1161, 20101006, TransactionTime=10/6/2010 03:22:26 PM, FDMS, BatchNumber=965,TranSuccess=YES, TranStatusCode=0, HostResponseCode=65, Remark=Settled successfully., BatchTotal=485.16, BatchTransCount=75"
    
    Close #1
    R = R + 1
    If R = 25 Then
        Form1.Refresh
        R = 0
    End If
Next Y
R = 0
DoEvents
Form1.Refresh
For Y = 0 To List10.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating R101 work file " + Mid$(List10.List(Y), 1, 4)
'PHASE 1 Create Work Files R101

    Open "\\bns1\share1\polling\poll\Processed\" + List10.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "R101" Then
            Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "R1.csv") For Append As #6
            Print #6, linedata
            Close #6
        End If
    Loop
       'Print #6, "X100, 1161, 20101006, TransactionTime=10/6/2010 03:22:26 PM, FDMS, BatchNumber=965,TranSuccess=YES, TranStatusCode=0, HostResponseCode=65, Remark=Settled successfully., BatchTotal=485.16, BatchTransCount=75"
    
    Close #1
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If

Next Y
R = 0
DoEvents
Form1.Refresh







TenderTotal = 0
TotalCash = 0
Open ("\\bns1\share1\polling\Data\Credit Card Balance.csv") For Output As #9
Print #9, "####,Difference,Tender,Settled,Comment"

For Y = 0 To List10.ListCount - 1
Label3.Caption = "Checking Credit Card Balance for " + Mid$(List10.List(Y), 1, 4)
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
TenderTotal = 0
CCTotal = 0
Cash = 0
SettleTotal = 0
Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "S.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, RestNum, SDATE, Tender, TenderAmount, TenderCount
        If Mid$(Tender, 1, 1) = "2" Then
            TenderTotal = TenderTotal + TenderAmount
            CCTotal = CCTotal + TenderAmount
        End If
       If Mid$(Tender, 1, 1) = "8" Then
            CCTotal = CCTotal + TenderAmount
       End If
       If Mid$(Tender, 1, 1) = "1" Then
            Cash = Cash + TenderAmount
       End If
    Loop
Close (6)
If Dir("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "X.csv") <> "" Then
Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "X.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, XDATE, Junk4, SettleType, Junk6, Junk7, Jun8, Junk9, Junk10, STotal, Jun12
        If SettleType = "FDMS" Then
            SettleTotal = SettleTotal + Val(Mid$(STotal, 12, 10))
        End If
    Loop
    R = R + 1
    If R = 25 Then
        Form1.Refresh
        R = 0
    End If
Close (6)
If TenderTotal <> SettleTotal Then
    BalTotal = TenderTotal - SettleTotal
    List9.AddItem Mid$(List10.List(Y), 1, 4) + " " + Str(Format(BalTotal, "0000.00")) + " " + Str(Format(TenderTotal, "0000.00")) + " " + Str(Format(SettleTotal, "0000.00"))
    Print #9, Mid$(List10.List(Y), 1, 4) + "," + Str(Format(BalTotal, "0000.00")) + "," + Str(Format(TenderTotal, "0000.00")) + "," + Str(Format(SettleTotal, "0000.00")) + ","
    If Check2.Value = 1 Then
    For x = 0 To RestaurantListing.ListCount - 1
        If Mid$(RestaurantListing.List(x), 4, 4) = Mid$(List10.List(Y), 1, 4) Then
            List9.AddItem "    " + Mid$(RestaurantListing.List(x), 9, 21)
            E# = E# + 1
            x = RestaurantListing.ListCount - 1
        End If
    Next x
    End If
    Form1.Refresh
End If
End If


If SDATE <> XDATE Then
   List9.AddItem Mid$(List10.List(Y), 1, 4) + " X " + Str(XDATE) + " S " + Str(SDATE)
   Print #9, Mid$(List10.List(Y), 1, 4) + ",X100DATE," + Str(XDATE) + ", ,"
   E# = E# + 1
   Form1.Refresh
End If

'S100 NetSales
If Dir("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "S1.csv") <> "" Then
Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "S1.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, Junk3, NetSales, Tax1, Tax2, Tax3, Tax4, Tax5, Junk10, Junk11, Jun12, Junk13, Junk14, Junk15, Junk16, Junk17, Junk18, Junk19, Junk20, Junk21, Junk22, Junk23, Junk24, Junk25, Junk26, Junk27, GiftCardSales
    Loop
Close (6)
End If

'R100 Deposit
Deposit = 0
If Dir("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "R.csv") <> "" Then
Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "R.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, Junk3, Junk4, SDeposit, Junk6
        Deposit = Deposit + SDeposit
    Loop
Close (6)
End If
If FileLen("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "R.csv") = 0 Then
    List3.AddItem Mid$(List10.List(Y), 1, 4) + " No Deposit"
    Form1.Refresh
End If

'R101 Deposit
TPaidIn = 0
TPaidOut = 0
If Dir("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "R1.csv") <> "" Then
Open ("c:\IRISCHECK\" + Mid$(List10.List(Y), 1, 4) + "R1.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, Junk3, Junk4, PaidIn, PaidOut
        TPaidIn = TPaidIn + PaidIn
        TPaidOut = TPaidOut + PaidOut
    Loop
Form1.Refresh
Close (6)
End If


TotalOS = (NetSales + Tax1 + Tax2 + Tax3 + Tax4 + Tax5 + GiftCardSales + TPaidIn - TPaidOut - Deposit - CCTotal) * -1

If TotalOS >= Val(CASHVAR.Text) Then
    List3.AddItem Mid$(List10.List(Y), 1, 4) + "     " + Str(Format(TotalOS, "0000.00"))
    TotalCash = TotalCash + 1
    If Check2.Value = 1 Then
    For x = 0 To RestaurantListing.ListCount - 1
        If Mid$(RestaurantListing.List(x), 4, 4) = Mid$(List10.List(Y), 1, 4) Then
            List3.AddItem " " + Mid$(RestaurantListing.List(x), 9, 21)
            List3.AddItem "  Speed # " + Mid$(RestaurantListing.List(x), 40, 4)
            C# = C# + 2
            x = RestaurantListing.ListCount - 1
        End If
    Next x
    End If
    Form1.Refresh
End If
'If TotalOS <= (Val(CASHVAR.Text) * -1) Then
'    List3.AddItem Mid$(List10.List(Y), 1, 4) + "     " + Str(Format(TotalOS, "0000.00"))
'    TotalCash = TotalCash + 1
'    If Mid$(List10.List(Y), 1, 4) = 2993 Then
'        List3.AddItem Mid$(List10.List(Y), 1, 4) + " School Meal"
'    End If
'    If Check2.Value = 1 Then
'    For X = 0 To RestaurantListing.ListCount - 1
'        If Mid$(RestaurantListing.List(X), 4, 4) = Mid$(List10.List(Y), 1, 4) Then
'            List3.AddItem " " + Mid$(RestaurantListing.List(X), 9, 21)
'            List3.AddItem "  Speed # " + Mid$(RestaurantListing.List(X), 40, 4)
'            C# = C# + 2
'            X = RestaurantListing.ListCount - 1
'        End If
'    Next X
'    End If
'    Form1.Refresh
'End If

TotalOS = 0
NetSales = 0
Deposit = 0
Tax1 = 0
Tax2 = 0
Tax3 = 0
Tax4 = 0
Tax5 = 0
GiftCardSales = 0
PaidIn = 0
PaidOut = 0
TPaidIn = 0
TPaidOut = 0
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If

Next Y

Label9 = Str(List3.ListCount - C#) + " To Research"
Label21.Caption = Str(List9.ListCount - E#) + " Out of Balance"
Label3.Caption = "Check Complete!"
If Check4 = 1 Then
    Label27.Caption = Str(List10.ListCount) + " after 4 pm"
End If
If Check5 = 1 Then
    Label27.Caption = Str(List10.ListCount) + " after 5 pm"
End If
If Check6 = 1 Then
    Label27.Caption = Str(List10.ListCount) + " after 6 pm"
End If

Form1.Refresh
Close #9

End Sub

Private Sub Command14_Click()
    S100S103.Show
End Sub

Private Sub Command15_Click()
'Msg = "Do you want to create email messages?"   ' Define message.
'Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
'Title = "Create Email Messages"  ' Define title.
'Help = "DEMO.HLP"   ' Define Help file.
'Ctxt = 1000 ' Define topic
        ' context.
        ' Display message.
'Response = MsgBox(Msg, Style, Title, Help, Ctxt)
'If Response = vbYes Then    ' User chose Yes.
    CreateMail = True
'Else    ' User chose No.
'    CreateMail = False
'End If




RestCount = 0
ErrorCount = 0

Form1.Check1.Value = 0
List1.Clear
Call Form_Load
Call Command3_Click


If Dir("c:\IRISCHECK\*.csv") <> "" Then
    Kill ("c:\IRISCHECK\*.csv")
End If

' Begin Pay Card
R = 0
        Open ("c:\IRISCHECK\AllE100.csv") For Output As #8
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating E103, E110, and E100 work files " + Mid$(File1.List(Y), 1, 4)
'PHASE 1 Create Work Files E103
    Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E.csv") For Output As #6
    Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E1.csv") For Output As #7

    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "E103" Then
            Print #6, linedata
        End If
        If Mid$(linedata, 2, 4) = "E110" Then
            Print #7, linedata
        End If
        If Mid$(linedata, 2, 4) = "E100" Then
            Print #8, linedata
        End If
    Loop
    Close #1, #6, #7
    
    
    
    
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
Next Y
Close #8
R = 0
DoEvents
Form1.Refresh



Dim BneRefNum As String
Dim StoreNum As String
Dim BusinessDate As String
Dim SocNum As String
Dim BneRefNum1 As String
Dim StoreNum1 As String
Dim BusinessDate1 As String
Dim SocNum1 As String
Dim IRISNum As String
Dim Account As String
Dim Bank As String
Dim AccountType As String
Dim Transcode As String
Dim ABANum As String
Open ("J:\Payroll\Pay Card\Pay Card Numbers.xxx") For Output As #6
Open ("J:\Payroll\Pay Card\Pay Card Errors.xxx") For Output As #7
Print #6, "Rest #" + "," + "BusinessDate" + "," + "SocNum" + "," + "PayCard #" + "," + "Verify PayCard #" + "," + "Employee Name" + "," + "IRIS #"
MAPISession1.SignOn
For Y = 0 To File1.ListCount - 1
    MailText = ""
    ErrorFound = False
    zerocount = 0
    Label3.Caption = "Creating Pay Card file " + Mid$(File1.List(Y), 1, 4)
    
    'PHASE 1 Create Work Files E100

    Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E.csv") For Input As #1
    Do Until EOF(1)
        Input #1, BneRefNum, StoreNum, BusinessDate, SocNum, Account, Bank, AccountType, Transcode, ABANum
    If Account <> "" Then
        Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E1.csv") For Input As #2
            FullName = "TERMINATED"
            IRISNum1 = "0"
        Do Until EOF(2)
            Input #2, BneRefNum1, StoreNum1, BusinessDate1, SocNum1, IRISNum, Junk1, FName, MI, LName, Junk2, Junk3, Junk4, Junk5, Junk6
            If SocNum = SocNum1 Then
                FullName = Trim(FName) + " " + Trim(LName)
                IRISNum1 = IRISNum
            End If
        Loop
        Close #2
        If FullName <> "TERMINATED" Then
            Print #6, StoreNum + "," + BusinessDate + "," + Chr(39) + SocNum + Chr(39) + "," + Chr(39) + Account + Chr(39) + "," + Chr(39) + Bank + Chr(39) + "," + FullName + "," + IRISNum1
    
        If Account <> Bank Then
            If ErrorFound = False Then
                        Print #7, "Restaurant Number: " + StoreNum
                        Print #7,
            End If
            ErrorFound = True
            ErrorCount = ErrorCount + 1
            If Bank = "" Then
                Bank = "Blank - Not Entered"
            End If
            Print #7, "Employee: " + FullName
            MailText = MailText + "Employee: " + FullName & Chr(10) & Chr(13)
            Print #7, "IRIS Employee #: " + IRISNum1
            MailText = MailText + "IRIS Employee #: " + IRISNum1 & Chr(10) & Chr(13)
            Print #7, "First PayAcct# entered into IRIS:  " + Account
            MailText = MailText + "First PayAcct# entered into IRIS:  " + Account & Chr(10) & Chr(13)
            Print #7, "Second PayAcct# entered into IRIS: " + Bank
            MailText = MailText + "Second PayAcct# entered into IRIS: " + Bank & Chr(10) & Chr(13) & Chr(10) & Chr(13)
            Print #7,
        End If
        End If
    End If
    Loop
    If ErrorFound = True Then
        RestCount = RestCount + 1
        MailText = MailText & "This is an automated email - please do not reply." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Both PayAcct# entries must be the same number (This number is located on the bottom right side of the Authorization Form that is attached with the actual Pay Card).  The number is entered twice for verification purposes.  Keying errors will delay your employee's ability to activate their card and access their pay!" & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Please correct the incorrect PayAcct#, so the Employee's Pay Card can be setup properly." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If this employee was transferred into your restaurant, please enter the number 9 in both fields." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If you have any questions, please refer to the Payroll - Pay Card Job Aide document found on LiveReporter in the IRIS Manual folder.  If you still have questions, please contact the BNE Help Desk at 1-800-773-8983 ext. 1437." & Chr(10) & Chr(13)
        Print #7, "----------------------------------------------"

If CreateMail = True Then

If Hour(Now()) > 15 And Val(StoreNum) < 6000 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
'MAPIMessages1.RecipDisplayName = "Jody smith"
MAPIMessages1.RecipDisplayName = "GM" & StoreNum
'MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Pay Card Information Entered Incorrectly"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
'Other Concepts
If CreateMail = True Then

If Hour(Now()) < 15 And Val(StoreNum) > 6000 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
MAPIMessages1.RecipDisplayName = "Jody smith"
'MAPIMessages1.RecipDisplayName = "GM" & StoreNum
MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
'MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Pay Card Information Entered Incorrectly"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
    End If
    Close #1
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
Next Y
ErrorFound = False
ErrorCount2 = 0
R = 0
' Check for Missing PayCard for New Employees
Dim CheckDate As Date
    Open ("c:\IRISCHECK\ALLE100.csv") For Input As #9
    Do Until EOF(9)
        Input #9, BneRefNum, StoreNum, BusinessDate, SSN, EmployeeNum, JobCode, Salary, FirstName, MiddleInitial, LastName, Address1, Address2, City, StateCode, ZipCode, HomePhone, PayRate, EffectiveDay, Gender, Race, BirthDate, StateNumOfDependents, StateAddAmtOfWithholdings, StateExempt, StateMaritalStatus, FederalNumOfDependents, FederalAddAmtOfWithholdings, FederalExempt, FederalMaritalStatus, ModDate, StatusID, ReasonID, ModTime
        CheckDate = Mid$(EffectiveDay, 5, 2) + "/" + Mid$(EffectiveDay, 7, 2) + "/" + Mid$(EffectiveDay, 1, 4)
If CheckDate >= (Now() - 10) And StatusID = "1" Then
    Open ("c:\IRISCHECK\" + Mid$(StoreNum, 1, 4) + "E.csv") For Input As #1
    Do Until EOF(1)
        Input #1, BneRefNum, StoreNum, BusinessDate, SocNum, Account, Bank, AccountType, Transcode, ABANum
        If SSN = SocNum Then
            If Account = "" Then
            If ErrorFound = False Then
                        Print #7, "Restaurant Number: " + StoreNum
                        Print #7,
            End If
            ErrorFound = True
            ErrorCount2 = ErrorCount2 + 1
            Print #7, "Employee: " + Trim(FirstName) + " " + LastName
            MailText = MailText + "Employee: " + Trim(FirstName) + " " + LastName & Chr(10) & Chr(13)
            Print #7, "IRIS Employee #: " + Str(EmployeeNum)
            MailText = MailText + "IRIS Employee #: " + Str(EmployeeNum) & Chr(10) & Chr(13)
            Print #7, "Hire Date: " + Str(CheckDate)
            MailText = MailText + "Hire Date: " + Str(CheckDate) & Chr(10) & Chr(13)
            Print #7, "New Hire missing Pay Card information."
            Print #7,
            End If
        End If
    Loop
    If ErrorFound = True Then
        RestCount = RestCount + 1
        MailText = MailText & "This is an automated email - please do not reply." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "It appears this new hire does not have the necessary Pay Card information." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Please enter the Pay Card Account Number found on the Pay Card Authorization Form into the IRIS Employee Editor.   Do not use the Credit Card Number found on the Pay Card.  The Account Number needs to be entered into both PayAcct# fields on the Payroll tab.  Verify the Account Number is typed in correctly for both PayAcct# fields.  The employee's Pay Card cannot be setup until you have completed this step." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Even if the employee is going to use their bank account rather than the Pay Card, a Pay Card needs to be issued to use until the Direct Deposit for the other account is setup." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If this employee was transferred to your restaurant, please make sure in the future to use the Temporary or Permanent Status rather than New.  To avoid receiving these messages on a daily basis, please enter 9 on both the PayAcct# fields on the Payroll tab for this employee." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If you have any questions, please refer to the Payroll - Pay Card Job Aide document found on LiveReporter in the IRIS Manual folder.  If you still have questions, please contact the BNE Help Desk at 1-800-773-8983 ext. 1437." & Chr(10) & Chr(13)
        Print #7, "----------------------------------------------"
    End If
    
If CreateMail = True And ErrorFound = True Then

If Hour(Now()) > 15 And Val(StoreNum) < 6000 Then
'If Hour(Now()) > 10 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
'MAPIMessages1.RecipDisplayName = "Jody smith"
MAPIMessages1.RecipDisplayName = "GM" & StoreNum
'MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Missing Pay Card Information for New Hire"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
'Other Concepts
If CreateMail = True And ErrorFound = True Then

If Hour(Now()) < 15 And Val(StoreNum) > 6000 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
'MAPIMessages1.RecipDisplayName = "Jody smith"
MAPIMessages1.RecipDisplayName = "GM" & StoreNum
'MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Missing Pay Card Information for New Hire"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
    End If
    Close #1
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
    MailText = ""
    ErrorFound = False
    Loop
    Close #9


    Print #7, "=============================="
    Print #7, "Restaurants with errors " + Str(RestCount)
    Print #7, "Total errors " + Str(ErrorCount)
    Print #7, "New Hire Errors " + Str(ErrorCount2)
    Print #7, "=============================="
Close #6, #7
R = 0
DoEvents
Form1.Refresh

FileName2 = "J:\Payroll\Pay Card\Errors " + Format$(Now(), "ddd") + ".TXT"
BackupFile = "J:\Payroll\Pay Card\Backup " + Format$(Now(), "ddd") + ".csv"
FileCopy "J:\Payroll\Pay Card\Pay Card Numbers.xxx", "J:\Payroll\Pay Card\Pay Card Numbers.CSV"
FileCopy "J:\Payroll\Pay Card\Pay Card Numbers.xxx", BackupFile
Kill ("J:\Payroll\Pay Card\Pay Card Numbers.xxx")
FileCopy "J:\Payroll\Pay Card\Pay Card Errors.xxx", FileName2
Kill ("J:\Payroll\Pay Card\Pay Card Errors.xxx")
    Label3.Caption = "Pay Card Data Created"

' End of Pay Card

'MAPISession1.SignOff
End
End Sub

Private Sub Command16_Click()

' Check Weekly QTimer file for missing data

FileD = Format$(Now() - 1, "d")
FileM = Format$(Now() - 1, "m")
FileY = Format$(Now() - 1, "yyyy")

tempDate = FileM + "-" + FileD + "-" + FileY
FileName = "S:\Hyperactive\Weekly Reports\BoddieNoell_QTimerWeeklyExport_" + tempDate + ".csv"
If Dir(FileName) = "" Then
    x = MsgBox(FileName + " was not found.  This program is setup to look for the file name to be the previous day.  If necessary copy the file and rename to match what this program is expecting.", vbCritical, "File Not Found")
    End
End If

List7.Clear
Open FileName For Input As #5
Do While Not EOF(5)
    Input #5, Junk1, UnitNum, Junk2, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8
    List7.AddItem (UnitNum)
Loop

Close #5
List2.Clear
For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To List7.ListCount - 1
        If List1.List(x) = List7.List(Y) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x


Label6.Caption = "Missing QTimer"
Label7.Caption = Str(List2.ListCount) + " missing"
    Label3.Caption = "QTIMER Data Check Complete"
Form1.Refresh
End Sub

Private Sub Command2_Click()
End
End Sub

Private Sub Command3_Click()
List2.Clear
List10.Clear
File1.Refresh
Label2.Caption = Str(File1.ListCount) + " files received"
For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
            If Check4 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 16 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check5 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 17 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check6 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 18 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check6 = 0 And Check4 = 0 And Check5 = 0 Then
                    List10.AddItem File1.List(Y)
            End If

        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x
Label7.Caption = Str(List2.ListCount) + " missing"
If Check4 = 1 Then
    Label27.Caption = Str(List10.ListCount) + " after 4 pm"
End If
If Check5 = 1 Then
    Label27.Caption = Str(List10.ListCount) + " after 5 pm"
End If
If Check6 = 1 Then
    Label27.Caption = Str(List10.ListCount) + " after 6 pm"
End If

End Sub

Private Sub Command4_Click()
List3.Clear
Label8 = "R102"
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Checking R102 " + File1.List(Y)
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "R102" Then ' Was R102
            zerocount = zerocount + 1
            DoEvents
        End If
    Loop
    Close (1)
    If zerocount = 0 Then
        'ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List3.AddItem Mid$(File1.List(Y), 1, 4)
    End If
            Label3.Caption = File1.List(Y) + " zero count = " + Str(zerocount)
            Label9.Caption = Str(List3.ListCount) + " missing"
            Form1.Refresh
Next Y
    Label3.Caption = "R102 Check Complete!"
Label9.Caption = Str(List3.ListCount) + " missing"
End Sub

Private Sub Command5_Click()
Label18.Visible = False
Label10.Caption = "Gift Card Transaction Details"
GCSales = 0
GCTender = 0
Label13.Caption = "Sales   = " + Str(GCSales)
Label14.Caption = "Tenders = " + Str(GCTender)
List5.Clear
List4.Clear

For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Checking G100 " + File1.List(Y)
    Sales = False
    Tender = False
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If (Val(File1.List(Y)) < 6000) Then
            If Mid$(linedata, 2, 4) = "S103" And Mid$(linedata, 22, 3) = "809" Then
                List4.AddItem Mid$(File1.List(Y), 1, 4) + " Tender"
                Tender = True
                GCTender = GCTender + 1
            End If
            If Mid$(linedata, 2, 4) = "M100" And Mid$(linedata, 28, 4) = "2329" Then
                If Sales = False Then
                    List4.AddItem Mid$(File1.List(Y), 1, 4) + " Sale"
                    Sales = True
                    GCSales = GCSales + 1
                End If
            End If
            If Mid$(linedata, 2, 4) = "G100" Then
                zerocount = zerocount + 1
                List4.AddItem linedata
            End If
        End If
            If (Val(File1.List(Y)) > 7000) And (Val(File1.List(Y)) < 8000) Then
            If Mid$(linedata, 2, 4) = "S103" And Mid$(linedata, 22, 3) = "809" Then
                List4.AddItem Mid$(File1.List(Y), 1, 4) + " Tender"
                Tender = True
                GCTender = GCTender + 1
            End If
            If Mid$(linedata, 2, 4) = "M100" And Mid$(linedata, 24, 3) = "215" Then
                If Sales = False Then
                    List4.AddItem Mid$(File1.List(Y), 1, 4) + " Sale"
                    Sales = True
                    GCSales = GCSales + 1
                End If
            End If
            If Mid$(linedata, 2, 4) = "G100" Then
                zerocount = zerocount + 1
                List4.AddItem linedata
            End If
        End If

    Loop
    Close (1)
    If (Tender = True Or Sales = True) And zerocount = 0 Then
        List5.AddItem Mid$(File1.List(Y), 1, 4)
    End If
    Label12.Caption = Str(List5.ListCount) + " missing"
    Label13.Caption = "Sales   = " + Str(GCSales)
    Label14.Caption = "Tenders = " + Str(GCTender)
    Form1.Refresh
Next Y
    Label3.Caption = "G100 Check Complete!"
    Label12.Caption = Str(List5.ListCount) + " missing"
    Label13.Caption = "Sales   = " + Str(GCSales)
    Label14.Caption = "Tenders = " + Str(GCTender)
    Form1.Refresh
End Sub

Private Sub Command6_Click()
List6.Clear
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Checking P100 " + File1.List(Y)
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "P100" Then
            zerocount = zerocount + 1
            DoEvents
        End If
    Loop
    Close (1)
    If zerocount < 5 Then
        'ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List6.AddItem Mid$(File1.List(Y), 1, 4) + " -" + Str(zerocount) + " Record(s)"
    End If
            Label16.Caption = Str(List6.ListCount) + " missing"
            Form1.Refresh
Next Y
    Label3.Caption = "P100 Check Complete!"
Label16.Caption = Str(List6.ListCount) + " missing"

End Sub

Private Sub Command7_Click()
R = 0
ButtonEditS103.Enabled = True
' Update Missing Files
Label18.Visible = False
Label10.Caption = "Gift Card Transaction Details"
GCSales = 0
GCTender = 0
List2.Clear
List3.Clear
List5.Clear
List4.Clear
List6.Clear
List8.Clear
List9.Clear
List10.Clear
File1.Refresh
Label2.Caption = Str(File1.ListCount) + " files received"
Label12.Caption = Str(List5.ListCount) + " missing"
Label21.Caption = "Working ..."

For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
            If Check4 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 16 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check5 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 17 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check6 = 1 Then
                PollHour = Format$(FileDateTime("\\bns1\share1\polling\poll\Processed\" + File1.List(Y)), "HH")
                If PollHour >= 18 Or PollHour < 15 Then
                    List10.AddItem File1.List(Y)
                End If
            End If
            If Check6 = 0 And Check4 = 0 And Check5 = 0 Then
                    List10.AddItem File1.List(Y)
            End If

        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x
Label7.Caption = Str(List2.ListCount) + " missing"
' End Update Missing Files
' READ POLL FILE BEGINS
For Y = 0 To List10.ListCount - 1
    zerocount = 0
    R102 = 0
    P100 = 0
    G100 = 0
    X100 = 0
    Sales = False
    Tender = False
    Label3.Caption = "Checking " + List10.List(Y)
    'Form1.Refresh
    Open "\\bns1\share1\polling\poll\Processed\" + List10.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "R102" Then
            R102 = R102 + 1
            'DoEvents
        End If
        If Mid$(linedata, 2, 4) = "X100" Then
            X100 = X100 + 1
            'DoEvents
        End If
        If Mid$(linedata, 2, 4) = "P100" Then
            P100 = P100 + 1
            'DoEvents
        End If
        If (Val(List10.List(Y)) < 6000) Then
            If Mid$(linedata, 2, 4) = "S103" And Mid$(linedata, 22, 3) = "809" Then
                List4.AddItem Mid$(List10.List(Y), 1, 4) + " Tender"
                Tender = True
                GCTender = GCTender + 1
            End If
            If Mid$(linedata, 2, 4) = "M100" And Mid$(linedata, 28, 4) = "2329" Then
                If Sales = False Then
                    List4.AddItem Mid$(List10.List(Y), 1, 4) + " Sale"
                    Sales = True
                    GCSales = GCSales + 1
                End If
            End If
            If Mid$(linedata, 2, 4) = "G100" Then
                G100 = G100 + 1
                List4.AddItem linedata
            End If
        End If
            If (Val(List10.List(Y)) > 7000) And (Val(List10.List(Y)) < 8000) Then
            If Mid$(linedata, 2, 4) = "S103" And Mid$(linedata, 22, 3) = "809" Then
                List4.AddItem Mid$(List10.List(Y), 1, 4) + " Tender"
                Tender = True
                GCTender = GCTender + 1
            End If
            If Mid$(linedata, 2, 4) = "M100" And Mid$(linedata, 24, 3) = "215" Then
                If Sales = False Then
                    List4.AddItem Mid$(List10.List(Y), 1, 4) + " Sale"
                    Sales = True
                    GCSales = GCSales + 1
                End If
            End If
            If Mid$(linedata, 2, 4) = "G100" Then
                G100 = G100 + 1
                List4.AddItem linedata
            End If
        End If
  Loop
    Close (1)
    If R102 = 0 Then
        'ZeroMsg = MsgBox("Restaurant " + List10.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List3.AddItem Mid$(List10.List(Y), 1, 4)
    End If
            Label9.Caption = Str(List3.ListCount) + " missing"
    
    If X100 = 0 Then
        'ZeroMsg = MsgBox("Restaurant " + List10.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List8.AddItem Mid$(List10.List(Y), 1, 4)
    End If
            Label19.Caption = Str(List8.ListCount) + " missing"
    
    If P100 < 5 Then
        'ZeroMsg = MsgBox("Restaurant " + List10.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List6.AddItem Mid$(List10.List(Y), 1, 4) + " -" + Str(P100) + " Record(s)"
    End If
            Label16.Caption = Str(List6.ListCount) + " missing"
            'Form1.Refresh
    If (Tender = True Or Sales = True) And G100 = 0 Then
        List5.AddItem Mid$(List10.List(Y), 1, 4)
    End If
    Label13.Caption = "Sales   = " + Str(GCSales)
    Label14.Caption = "Tenders = " + Str(GCTender)
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If

Next Y
    Label12.Caption = Str(List5.ListCount) + " missing"
    Label19.Caption = Str(List8.ListCount) + " missing"
    Label13.Caption = "Sales   = " + Str(GCSales)
    Label14.Caption = "Tenders = " + Str(GCTender)
    Label9.Caption = Str(List3.ListCount) + " missing"
    Label3.Caption = "All Checks Complete!"
    Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
    Call Command13_Click
    Form1.Refresh
End Sub

Private Sub Command8_Click()
If Dir("\\bns1\share1\polling\poll\Payroll Verify\*.*") <> "" Then
    Kill ("\\bns1\share1\polling\poll\Payroll Verify\*.*")
End If
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Form1.Refresh
    Label3.Caption = "Creating CSV file " + File1.List(Y)
    Open "\\bns1\share1\polling\poll\Payroll Verify\" + Mid$(File1.List(Y), 1, 4) + ".CSV" For Output As #2
    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "P100" Then
            zerocount = zerocount + 1
            Print #2, linedata
            'DoEvents
        End If
    Loop
    Close (1)
    Close (2)
    If zerocount < 5 Then
        'ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         z = MsgBox((File1.List(Y)), vbCritical, "Error")
    End If
    DoEvents
Next Y


For Y = 0 To File1.ListCount - 1
    RestTotal = 0
    Label3.Caption = "Creating Report file " + File1.List(Y)
    Form1.Refresh
    Open "\\bns1\share1\polling\poll\Payroll Verify\" + Mid$(File1.List(Y), 1, 4) + ".BNR" For Output As #2
    Open "\\bns1\share1\polling\poll\Payroll Verify\" + Mid$(File1.List(Y), 1, 4) + ".CSV" For Input As #1
     Print #2,
     Print #2,
     Print #2, "     Restaurant Number: " + Mid$(File1.List(Y), 1, 4)
     Print #2, "     Date and Time:     " + Str(Now())
     Print #2,
     Do Until EOF(1)
        Input #1, Ref, StoreNum, BDate, SS, Emp#, FName, LName, Wage, JobCode, TimeType, Day1, Day2, Day3, Day4, Day5, Day6, Day7, J1, J2, J3, J4, J5
            
        THours = (Day1 + Day2 + Day3 + Day4 + Day5 + Day6 + Day7) / 60
        RestTotal = RestTotal + THours
        'FTHours = Format(THours, "00.00")
        Print #2, "     " + Format(Emp#, "000") + "  " + Format$(FName, "!@@@@@@@@@@") + " " + Format$(LName, "!@@@@@@@@@@") + "  " + Format(THours, "00.00")
    Loop
        'FRestTotal = Format(RestTotal, "000.00")
        Print #2, "                          TOTAL " + Format(RestTotal, "000.00")
    Close (2)
    Close (1)
Next Y

    Label3.Caption = "Verify Files Complete!"
    Response = MsgBox("Do you want to send out the Payroll Verification Report?", vbYesNo, "Send")
    If Response = vbYes Then
        For Y = 0 To File1.ListCount - 1
            Sfile = "\\bns1\share1\polling\poll\Payroll Verify\" + Mid$(File1.List(Y), 1, 4) + ".BNR"
            TFile = "\\bns1\share1\fieldreports\" + Mid$(File1.List(Y), 1, 4) + "\Special Payroll Verification.BNR"
            FileCopy Sfile, TFile
            Label3.Caption = "Copying file for " + File1.List(Y)
            DoEvents
        Next Y
    Label3.Caption = "Copy to LiveReporter complete!"
    End If
End Sub

Private Sub Command9_Click()
Label10.Caption = "Restaurants with EDM Files"
List7.Clear
List4.Clear
  R = 0
  Open "\\bns1\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
        If Mid$(unt, 1, 1) = "H" Then
            List7.AddItem Mid$(unt, 4, 35)
        End If
  Loop
  Close (1)

For Y = 0 To List7.ListCount - 1
    zerocount = 0
    Label3.Caption = "Searching for EDM files for " + Mid$(List7.List(Y), 1, 4)
    If Dir("\\bns1\share1\edm\Outgoing\" + Mid$(List7.List(Y), 1, 4) + "\*.xml") <> "" Then
      List4.AddItem (Mid$(List7.List(Y), 1, 4) + " - " + Mid$(List7.List(Y), 6, 30))
      'z = MsgBox(Mid$(List1.List(Y), 1, 4) + " has EDM files.", vbCritical, "EDM File Check")
    End If
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If

Next Y
    Label3.Caption = "EDM File Check Complete!"
    Label18.Caption = Str(List4.ListCount) + " restaurants found"
    Label18.Visible = True
    Label14.Caption = ""
Exit Sub
End Sub

Private Sub EditS103_Click()
    EditS103.Show
End Sub

Private Sub Form_Load()

  Open "\\bns1\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
    If Check1.Value = False Then
    File1.Pattern = "*.irs"
        If Mid$(unt, 1, 1) = "H" Or Mid$(unt, 1, 1) = "C" Or Mid$(unt, 1, 1) = "M" Or Mid$(unt, 1, 1) = "D" Or Mid$(unt, 1, 1) = "W" Or Mid$(unt, 1, 1) = "F" Then
            List1.AddItem Mid$(unt, 4, 4)
        End If
    Else
    File1.Pattern = "10??.irs;11??.irs;12??.irs;130?.irs;131?.irs;132?.irs;133?.irs;134?.irs;135?.irs;136?.irs;137?.irs;138?.irs;14??.irs;15??.irs;16??.irs;18??.irs;19??.irs;2???.irs;3???.irs;4???.irs;5???.irs"
    If Mid$(unt, 1, 1) = "H" Then
            List1.AddItem Mid$(unt, 4, 4)
    End If
    End If
  Loop
  Close (1)

Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bns1\share1\polling\poll\Processed\"
Label2.Caption = Str(File1.ListCount) + " files received"

For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
            List10.AddItem File1.List(Y)
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x

Label7.Caption = Str(List2.ListCount) + " missing"
Label9.Caption = Str(List3.ListCount) + " missing"
Label19.Caption = Str(List8.ListCount) + " missing"
Label12.Caption = Str(List5.ListCount) + " missing"
Label16.Caption = Str(List6.ListCount) + " missing"
Label21.Caption = Str(List9.ListCount) + " Out of Balance"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))

    CreateMail = True
'Else    ' User chose No.
'    CreateMail = False
'End If




RestCount = 0
ErrorCount = 0

'Form1.Check1.Value = 0
'List1.Clear
'Call Form_Load
'Call Command3_Click



If Dir("c:\IRISCHECK\*.csv") <> "" Then
    Kill ("c:\IRISCHECK\*.csv")
End If

' Begin Pay Card
R = 0
        Open ("c:\IRISCHECK\AllE100.csv") For Output As #8
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating E103, E110, and E100 work files " + Mid$(File1.List(Y), 1, 4)
'PHASE 1 Create Work Files E103
    Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E.csv") For Output As #6
    Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E1.csv") For Output As #7

    Open "\\bns1\share1\polling\poll\Processed\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "E103" Then
            Print #6, linedata
        End If
        If Mid$(linedata, 2, 4) = "E110" Then
            Print #7, linedata
        End If
        If Mid$(linedata, 2, 4) = "E100" Then
            Print #8, linedata
        End If
    Loop
    Close #1, #6, #7
    
    
    
    
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
Next Y
Close #8
R = 0
DoEvents
Form1.Refresh



Dim BneRefNum As String
Dim StoreNum As String
Dim BusinessDate As String
Dim SocNum As String
Dim BneRefNum1 As String
Dim StoreNum1 As String
Dim BusinessDate1 As String
Dim SocNum1 As String
Dim IRISNum As String
Dim Account As String
Dim Bank As String
Dim AccountType As String
Dim Transcode As String
Dim ABANum As String
Open ("J:\Payroll\Pay Card\Pay Card Numbers.xxx") For Output As #6
Open ("J:\Payroll\Pay Card\Pay Card Errors.xxx") For Output As #7
Print #6, "Rest #" + "," + "BusinessDate" + "," + "SocNum" + "," + "PayCard #" + "," + "Verify PayCard #" + "," + "Employee Name" + "," + "IRIS #"
MAPISession1.SignOn
For Y = 0 To File1.ListCount - 1
    MailText = ""
    ErrorFound = False
    zerocount = 0
    Label3.Caption = "Creating Pay Card file " + Mid$(File1.List(Y), 1, 4)
    
    'PHASE 1 Create Work Files E100

    Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E.csv") For Input As #1
    Do Until EOF(1)
        Input #1, BneRefNum, StoreNum, BusinessDate, SocNum, Account, Bank, AccountType, Transcode, ABANum
    If Account <> "" Then
        Open ("c:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "E1.csv") For Input As #2
            FullName = "TERMINATED"
            IRISNum1 = "0"
        Do Until EOF(2)
            Input #2, BneRefNum1, StoreNum1, BusinessDate1, SocNum1, IRISNum, Junk1, FName, MI, LName, Junk2, Junk3, Junk4, Junk5, Junk6
            If SocNum = SocNum1 Then
                FullName = Trim(FName) + " " + Trim(LName)
                IRISNum1 = IRISNum
            End If
        Loop
        Close #2
        If FullName <> "TERMINATED" Then
            Print #6, StoreNum + "," + BusinessDate + "," + Chr(39) + SocNum + Chr(39) + "," + Chr(39) + Account + Chr(39) + "," + Chr(39) + Bank + Chr(39) + "," + FullName + "," + IRISNum1
    
        If Account <> Bank Then
            If ErrorFound = False Then
                        Print #7, "Restaurant Number: " + StoreNum
                        Print #7,
            End If
            ErrorFound = True
            ErrorCount = ErrorCount + 1
            If Bank = "" Then
                Bank = "Blank - Not Entered"
            End If
            Print #7, "Employee: " + FullName
            MailText = MailText + "Employee: " + FullName & Chr(10) & Chr(13)
            Print #7, "IRIS Employee #: " + IRISNum1
            MailText = MailText + "IRIS Employee #: " + IRISNum1 & Chr(10) & Chr(13)
            Print #7, "First PayAcct# entered into IRIS:  " + Account
            MailText = MailText + "First PayAcct# entered into IRIS:  " + Account & Chr(10) & Chr(13) & Chr(13)
            Print #7, "Second PayAcct# entered into IRIS: " + Bank
            MailText = MailText + "Second PayAcct# entered into IRIS: " + Bank & Chr(10) & Chr(13) & Chr(10) & Chr(13)
            Print #7,
        End If
        End If
    End If
    Loop
    If ErrorFound = True Then
        RestCount = RestCount + 1
        MailText = MailText & "This is an automated email - please do not reply." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Both PayAcct# entries must be the same number (This number is located on the bottom right side of the Authorization Form that is attached with the actual Pay Card).  The number is entered twice for verification purposes.  Keying errors will delay your employee's ability to activate their card and access their pay!" & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Please correct the incorrect PayAcct#, so the Employee's Pay Card can be setup properly." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If this employee was transferred into your restaurant, please enter the number 9 in both fields." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If you have any questions, please refer to the Payroll - Pay Card Job Aide document found on LiveReporter in the IRIS Manual folder.  If you still have questions, please contact the BNE Help Desk at 1-800-773-8983 ext. 1437." & Chr(10) & Chr(13)
        Print #7, "----------------------------------------------"

If CreateMail = True Then


If Hour(Now()) > 15 And Val(StoreNum) < 6000 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
'MAPIMessages1.RecipDisplayName = "Jody smith"
MAPIMessages1.RecipDisplayName = "GM" & StoreNum
'MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Pay Card Information Entered Incorrectly"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
'Other Concepts
If CreateMail = True Then

If Hour(Now()) < 15 And Val(StoreNum) > 6000 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
'MAPIMessages1.RecipDisplayName = "Jody smith"
MAPIMessages1.RecipDisplayName = "GM" & StoreNum
'MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Pay Card Information Entered Incorrectly"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
    End If
    Close #1
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
Next Y
ErrorFound = False
ErrorCount2 = 0
R = 0
' Check for Missing PayCard for New Employees
Dim CheckDate As Date
    Open ("c:\IRISCHECK\ALLE100.csv") For Input As #9
    Do Until EOF(9)
        Input #9, BneRefNum, StoreNum, BusinessDate, SSN, EmployeeNum, JobCode, Salary, FirstName, MiddleInitial, LastName, Address1, Address2, City, StateCode, ZipCode, HomePhone, PayRate, EffectiveDay, Gender, Race, BirthDate, StateNumOfDependents, StateAddAmtOfWithholdings, StateExempt, StateMaritalStatus, FederalNumOfDependents, FederalAddAmtOfWithholdings, FederalExempt, FederalMaritalStatus, ModDate, StatusID, ReasonID, ModTime
        CheckDate = Mid$(EffectiveDay, 5, 2) + "/" + Mid$(EffectiveDay, 7, 2) + "/" + Mid$(EffectiveDay, 1, 4)
If CheckDate >= (Now() - 10) And StatusID = "1" Then
    Open ("c:\IRISCHECK\" + Mid$(StoreNum, 1, 4) + "E.csv") For Input As #1
    Do Until EOF(1)
        Input #1, BneRefNum, StoreNum, BusinessDate, SocNum, Account, Bank, AccountType, Transcode, ABANum
        If SSN = SocNum Then
            If Account = "" Then
            If ErrorFound = False Then
                        Print #7, "Restaurant Number: " + StoreNum
                        Print #7,
            End If
            ErrorFound = True
            ErrorCount2 = ErrorCount2 + 1
            Print #7, "Employee: " + Trim(FirstName) + " " + LastName
            MailText = MailText + "Employee: " + Trim(FirstName) + " " + LastName & Chr(10) & Chr(13)
            Print #7, "IRIS Employee #: " + Str(EmployeeNum)
            MailText = MailText + "IRIS Employee #: " + Str(EmployeeNum) & Chr(10) & Chr(13)
            Print #7, "Hire Date: " + Str(CheckDate)
            MailText = MailText + "Hire Date: " + Str(CheckDate) & Chr(10) & Chr(13)
            Print #7, "New Hire missing Pay Card information."
            Print #7,
            End If
        End If
    Loop
    If ErrorFound = True Then
        RestCount = RestCount + 1
        MailText = MailText & "This is an automated email - please do not reply." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "It appears this new hire does not have the necessary Pay Card information." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Please enter the Pay Card Account Number found on the Pay Card Authorization Form into the IRIS Employee Editor.   Do not use the Credit Card Number found on the Pay Card.  The Account Number needs to be entered into both PayAcct# fields on the Payroll tab.  Verify the Account Number is typed in correctly for both PayAcct# fields.  The employee's Pay Card cannot be setup until you have completed this step." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "Even if the employee is going to use their bank account rather than the Pay Card, a Pay Card needs to be issued to use until the Direct Deposit for the other account is setup." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If this employee was transferred to your restaurant, please make sure in the future to use the Temporary or Permanent Status rather than New.  To avoid receiving these messages on a daily basis, please enter 9 on both the PayAcct# fields on the Payroll tab for this employee." & Chr(10) & Chr(13) & Chr(10) & Chr(13)
        MailText = MailText & "If you have any questions, please refer to the Payroll - Pay Card Job Aide document found on LiveReporter in the IRIS Manual folder.  If you still have questions, please contact the BNE Help Desk at 1-800-773-8983 ext. 1437." & Chr(10) & Chr(13)
        Print #7, "----------------------------------------------"
    End If
    
If CreateMail = True And ErrorFound = True Then

If Hour(Now()) > 15 And Val(StoreNum) < 6000 Then
'If Hour(Now()) > 10 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
'MAPIMessages1.RecipDisplayName = "Jody smith"
MAPIMessages1.RecipDisplayName = "GM" & StoreNum
'MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Missing Pay Card Information for New Hire"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
'Other Concepts
If CreateMail = True And ErrorFound = True Then

If Hour(Now()) < 15 And Val(StoreNum) > 6000 Then

MAPIMessages1.SessionID = MAPISession1.SessionID
'Compose new message
MAPIMessages1.Compose
'Address message
'MAPIMessages1.RecipDisplayName = "Jody smith"
MAPIMessages1.RecipDisplayName = "GM" & StoreNum
'MAPIMessages1.RecipAddress = "jodysmith@boddienoell.com"
MAPIMessages1.RecipAddress = "GM" & StoreNum + "@boddienoell.com"
' Resolve recipient name
MAPIMessages1.AddressResolveUI = True
MAPIMessages1.ResolveName
'Create the message
MAPIMessages1.MsgSubject = "Missing Pay Card Information for New Hire"
MAPIMessages1.MsgNoteText = MailText
'Add attachment
'MAPIMessages1.AttachmentPathName = "c:\zxcvzxcv.zip"
'Send the message
MAPIMessages1.Send False
End If
End If
    
    End If
    Close #1
    R = R + 1
    If R = 25 Then
        DoEvents
        Form1.Refresh
        R = 0
    End If
    MailText = ""
    ErrorFound = False
    Loop
    Close #9


    Print #7, "=============================="
    Print #7, "Restaurants with errors " + Str(RestCount)
    Print #7, "Total errors " + Str(ErrorCount)
    Print #7, "New Hire Errors " + Str(ErrorCount2)
    Print #7, "=============================="
Close #6, #7
R = 0
DoEvents
Form1.Refresh

FileName2 = "J:\Payroll\Pay Card\Errors " + Format$(Now(), "ddd") + ".TXT"
BackupFile = "J:\Payroll\Pay Card\Backup " + Format$(Now(), "ddd") + ".csv"
FileCopy "J:\Payroll\Pay Card\Pay Card Numbers.xxx", "J:\Payroll\Pay Card\Pay Card Numbers.CSV"
FileCopy "J:\Payroll\Pay Card\Pay Card Numbers.xxx", BackupFile
Kill ("J:\Payroll\Pay Card\Pay Card Numbers.xxx")
FileCopy "J:\Payroll\Pay Card\Pay Card Errors.xxx", FileName2
Kill ("J:\Payroll\Pay Card\Pay Card Errors.xxx")
    Label3.Caption = "Pay Card Data Created"

' End of Pay Card
'Form1.Check1.Value = 1
'List1.Clear
MAPISession1.SignOff
End


End Sub

Private Sub Texas_Click()
If Texas.Value = 1 Then
  JobCodeTextBox.Enabled = False
  VersionCheckBox.Enabled = False
  Command12.Enabled = False
  Command6.Enabled = False
  Command13.Enabled = False
  Command9.Enabled = False
  Command7.Enabled = False
  Command14.Enabled = False
  Command11.Enabled = False
  Command10.Enabled = False
  Command8.Enabled = False
  Check1.Enabled = False
  List1.Clear
  List2.Clear
  List3.Clear
  Texas.Value = 1
  Open "\\bns1\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
    File1.Pattern = "*.EPL"
        If Mid$(unt, 1, 1) = "T" Then
            List1.AddItem Mid$(unt, 4, 4)
        End If
  Loop
  Close (1)

Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bns1\share1\polling\poll\Processed\"
Label2.Caption = Str(File1.ListCount) + " files received"

For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x
List3.Refresh
List1.Refresh
List2.Refresh
Label7.Caption = Str(List2.ListCount) + " missing"
Label9.Caption = Str(List3.ListCount) + " missing"
Label19.Caption = Str(List8.ListCount) + " missing"
Label12.Caption = Str(List5.ListCount) + " missing"
Label16.Caption = Str(List6.ListCount) + " missing"
Label21.Caption = Str(List9.ListCount) + " converted"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
Call Command4_Click
    
Else
  JobCodeTextBox.Enabled = True
  VersionCheckBox.Enabled = True
  Command12.Enabled = True
  Command6.Enabled = True
  Command13.Enabled = True
  Command9.Enabled = True
  Command7.Enabled = True
  Command14.Enabled = True
  Command11.Enabled = True
  Command10.Enabled = True
  Command8.Enabled = True
  Check1.Enabled = True
  List1.Clear
  List2.Clear
  List3.Clear

  
  Open "\\bns1\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
    If Check1.Value = False Then
    File1.Pattern = "*.irs"
        If Mid$(unt, 1, 1) = "H" Or Mid$(unt, 1, 1) = "C" Or Mid$(unt, 1, 1) = "M" Or Mid$(unt, 1, 1) = "D" Or Mid$(unt, 1, 1) = "W" Then
            List1.AddItem Mid$(unt, 4, 4)
        End If
    Else
    File1.Pattern = "10??.irs;11??.irs;12??.irs;130?.irs;131?.irs;132?.irs;133?.irs;134?.irs;135?.irs;136?.irs;137?.irs;138?.irs;14??.irs;15??.irs;16??.irs;18??.irs;19??.irs;2???.irs;3???.irs;4???.irs;5???.irs"
    If Mid$(unt, 1, 1) = "H" Then
            List1.AddItem Mid$(unt, 4, 4)
    End If
    End If
  Loop
  Close (1)

Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\bns1\share1\polling\poll\Processed\"
Label2.Caption = Str(File1.ListCount) + " files received"

For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(x) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(x)
    End If
Next x

Label7.Caption = Str(List2.ListCount) + " missing"
Label9.Caption = Str(List3.ListCount) + " missing"
Label19.Caption = Str(List8.ListCount) + " missing"
Label12.Caption = Str(List5.ListCount) + " missing"
Label16.Caption = Str(List6.ListCount) + " missing"
Label21.Caption = Str(List9.ListCount) + " converted"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
  


End If

End Sub

Private Sub VersionCheckBox_Click()
If VersionCheckBox.Value = 1 Then
    Label22.Caption = "Version 3.7.8.2 151"
Else
    Label22.Caption = "Version 3.7.7"
End If
Call Command13_Click
End Sub
