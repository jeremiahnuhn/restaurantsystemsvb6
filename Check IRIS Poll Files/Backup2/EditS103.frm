VERSION 5.00
Begin VB.Form EditS103 
   Caption         =   "Update Tender Records S103"
   ClientHeight    =   5835
   ClientLeft      =   720
   ClientTop       =   2790
   ClientWidth     =   9720
   LinkTopic       =   "Form2"
   ScaleHeight     =   5835
   ScaleWidth      =   9720
   Begin VB.CommandButton Command2 
      Caption         =   "Log: Partial Offline Declined"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3240
      TabIndex        =   42
      Top             =   4440
      Width           =   2535
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      ItemData        =   "EditS103.frx":0000
      Left            =   1680
      List            =   "EditS103.frx":0002
      TabIndex        =   23
      Top             =   3360
      Width           =   3855
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Read Original File"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1680
      TabIndex        =   21
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox AE 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5400
      TabIndex        =   4
      Text            =   "0.00"
      Top             =   360
      Width           =   1455
   End
   Begin VB.TextBox Discover 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5400
      TabIndex        =   3
      Text            =   "0.00"
      Top             =   960
      Width           =   1455
   End
   Begin VB.TextBox MC 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5400
      TabIndex        =   2
      Text            =   "0.00"
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox Visa 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5400
      TabIndex        =   1
      Text            =   "0.00"
      Top             =   2160
      Width           =   1455
   End
   Begin VB.TextBox Comments 
      Height          =   735
      Left            =   480
      TabIndex        =   5
      Top             =   4800
      Width           =   6495
   End
   Begin VB.TextBox RestNum 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   1680
      TabIndex        =   0
      Top             =   600
      Width           =   1335
   End
   Begin VB.CommandButton CreateFiles 
      Caption         =   "Create Files"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   7440
      TabIndex        =   6
      Top             =   4200
      Width           =   1935
   End
   Begin VB.CommandButton Close 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   7440
      TabIndex        =   7
      Top             =   4920
      Width           =   1935
   End
   Begin VB.OptionButton Default 
      Caption         =   "Default"
      Height          =   375
      Left            =   360
      TabIndex        =   13
      Top             =   2400
      Value           =   -1  'True
      Width           =   1095
   End
   Begin VB.OptionButton Sheila 
      Caption         =   "Sheila"
      Height          =   375
      Left            =   360
      TabIndex        =   12
      Top             =   2040
      Width           =   1095
   End
   Begin VB.OptionButton Jody 
      Caption         =   "Jody"
      Height          =   375
      Left            =   360
      TabIndex        =   11
      Top             =   1680
      Width           =   1095
   End
   Begin VB.OptionButton Dannie 
      Caption         =   "Dannie"
      Height          =   375
      Left            =   360
      TabIndex        =   10
      Top             =   1320
      Width           =   1095
   End
   Begin VB.OptionButton Brad 
      Caption         =   "Brad"
      Height          =   375
      Left            =   360
      TabIndex        =   9
      Top             =   960
      Width           =   1095
   End
   Begin VB.OptionButton Bob 
      Caption         =   "Bob"
      Height          =   375
      Left            =   360
      TabIndex        =   8
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label22 
      Caption         =   "Don't Create for Offline Declined"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   5880
      TabIndex        =   43
      Top             =   4200
      Width           =   1455
   End
   Begin VB.Label Label21 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   5760
      TabIndex        =   41
      Top             =   3480
      Width           =   3615
   End
   Begin VB.Line Line1 
      X1              =   5280
      X2              =   9480
      Y1              =   2760
      Y2              =   2760
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8400
      TabIndex        =   40
      Top             =   2760
      Width           =   855
   End
   Begin VB.Label Label14 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6840
      TabIndex        =   39
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Label Label13 
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5520
      TabIndex        =   38
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   37
      Top             =   2760
      Width           =   855
   End
   Begin VB.Label Label27 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   36
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label26 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   35
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label25 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   34
      Top             =   1680
      Width           =   1095
   End
   Begin VB.Label Label24 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   33
      Top             =   2280
      Width           =   1095
   End
   Begin VB.Label Label23 
      Caption         =   "Original"
      Height          =   255
      Left            =   7320
      TabIndex        =   32
      Top             =   120
      Width           =   735
   End
   Begin VB.Label Label20 
      Caption         =   "Offline Declined"
      Height          =   255
      Left            =   8280
      TabIndex        =   31
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8400
      TabIndex        =   30
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8400
      TabIndex        =   29
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label Label17 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8400
      TabIndex        =   28
      Top             =   1680
      Width           =   855
   End
   Begin VB.Label Label16 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   27
      Top             =   2280
      Width           =   975
   End
   Begin VB.Label Label11 
      Caption         =   "Adjusted"
      Height          =   375
      Left            =   5760
      TabIndex        =   26
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label10 
      Caption         =   "X101 Offline Declined Transactions"
      Height          =   615
      Left            =   600
      TabIndex        =   25
      Top             =   3480
      Width           =   975
   End
   Begin VB.Label Label9 
      Caption         =   "Order #              Card Type                 Amount"
      Height          =   375
      Left            =   1680
      TabIndex        =   24
      Top             =   3120
      Width           =   3855
   End
   Begin VB.Label Label8 
      Caption         =   "(No commas)"
      Height          =   255
      Left            =   1920
      TabIndex        =   22
      Top             =   4560
      Width           =   1215
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "American Express"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3120
      TabIndex        =   20
      Top             =   480
      Width           =   2055
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Discover"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      TabIndex        =   19
      Top             =   1080
      Width           =   1335
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "MasterCard"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      TabIndex        =   18
      Top             =   1680
      Width           =   1335
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Visa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      TabIndex        =   17
      Top             =   2280
      Width           =   1335
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Comments for Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   16
      Top             =   4560
      Width           =   1455
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Restaurant Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1680
      TabIndex        =   15
      Top             =   360
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Completd By"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   360
      Width           =   1455
   End
End
Attribute VB_Name = "EditS103"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub AE_Change()
NetAdj = Val(Visa.Text) + Val(MC.Text) + Val(Discover.Text) + Val(AE.Text)
OrgTotal = Label14.Caption
Label13.Caption = Format$(Str(NetAdj), "#,##0.00")
Label21.Caption = "Adjustment of " + Format$(Str(NetAdj - OrgTotal), "##0.00")
EditS103.Refresh
End Sub

Private Sub Bob_Click()
Bob.Value = True
Dannie.Value = False
Sheila.Value = False
Brad.Value = False
Jody.Value = False
CBY = "Bob"
End Sub

Private Sub Brad_Click()
Bob.Value = False
Dannie.Value = False
Sheila.Value = False
Brad.Value = True
Jody.Value = False
End Sub

Private Sub Close_Click()
RestNum.Text = ""
Visa = ""
MC = ""
Discover = ""
AE = ""
List1.Clear
Comments.Text = ""
VDeclined = 0
MDeclined = 0
DDeclined = 0
ADeclined = 0
Label16.Caption = Format$(Str(VDeclined), "##0.00")
Label17.Caption = Format$(Str(MDeclined), "##0.00")
Label18.Caption = Format$(Str(DDeclined), "##0.00")
Label19.Caption = Format$(Str(ADeclined), "##0.00")
Label24.Caption = Format$(Str(VDeclined), "##0.00")
Label25.Caption = Format$(Str(MDeclined), "##0.00")
Label26.Caption = Format$(Str(DDeclined), "##0.00")
Label27.Caption = Format$(Str(ADeclined), "##0.00")
Label15 = Format$(Str(ADeclined + MDeclined + DDeclined + VDeclined), "##0.00")
OrgTotal = 0
Label14.Caption = Format$(Str(OrgTotal), "#,##0.00")
Label21.Caption = " "
EditS103.Refresh
EditS103.Hide
End Sub

Private Sub Command1_Click()
Comments.Text = ""
CreateFiles.Enabled = True
If Dir("N:\IRISCHECK\" + RestNum.Text + "S1.csv") <> "" Then
Open ("N:\IRISCHECK\" + RestNum.Text + "S1.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, RestDate, NetSales, Tax1, Tax2, Tax3, Tax4, Tax5, Junk10, Junk11, Jun12, Junk13, Junk14, Junk15, Junk16, Junk17, Junk18, Junk19, Junk20, Junk21, Junk22, Junk23, Junk24, Junk25, Junk26, Junk27, GiftCardSales
    Loop
Close (6)
End If
'20110509
CheckDate = Now()
If Hour(Now()) < 15 Then
    CheckDate = Now() - 1
End If

ForDate = Format(CheckDate, "YYYY") + Format(CheckDate, "mm") + Format(CheckDate, "dd")

If Int(ForDate) <> RestDate Then
    Response = MsgBox("It appears this restaurant's poll file contains more than one day of information.  Please contact Bob W or Jody for assistance.", vbCritical, "Data Error")
    EditS103.Hide
    Exit Sub
End If


Visa = 0
MC = 0
Discover = 0
AE = 0
VisaOrg = 0
MCOrg = 0
AEorg = 0
DiscoverOrg = 0
Open ("N:\IRISCHECK\" + RestNum.Text + "S.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, RestNumber, SDATE, Tender, TenderAmount, TenderCount
        RecordType = Junk1
        
        If Tender = "201" Then
            VisaOrg = TenderAmount
            VisaOrgC = TenderCount
        End If
        If Tender = "202" Then
            MCOrg = TenderAmount
            MCOrgC = TenderCount
        End If
        If Tender = "203" Then
            AEorg = TenderAmount
            AEOrgC = TenderCount
            AE.Text = Format$(Str(TenderAmount), "###.00")
        End If
        If Tender = "204" Then
            DiscoverOrg = TenderAmount
            DiscoverOrgC = TenderCount
            Discover.Text = Format$(Str(TenderAmount), "###.00")
        End If
        If Tender = "100" Then
            CashOrgC = TenderCount
            CashOrg = TenderAmount
        End If
        If Tender = "809" Then
            GCOrg = TenderAmount
            GCOrgC = TenderCount
        End If
    Loop
Close (6)
Visa.Text = Format$(Str(VisaOrg), "##0.00")
MC.Text = Format$(Str(MCOrg), "##0.00")
AE.Text = Format$(Str(AEorg), "##0.00")
Discover.Text = Format$(Str(DiscoverOrg), "##0.00")
OrgTotal = VisaOrg + MCOrg + AEorg + DiscoverOrg
Label14.Caption = Format$(Str(OrgTotal), "#,##0.00")
NetAdj = Val(Visa.Text) + Val(MC.Text) + Val(Discover.Text) + Val(AE.Text)
Label13.Caption = Format$(Str(NetAdj), "#,##0.00")
Label24.Caption = Format$(Str(VisaOrg), "##0.00")
Label25.Caption = Format$(Str(MCOrg), "##0.00")
Label27.Caption = Format$(Str(AEorg), "##0.00")
Label26.Caption = Format$(Str(DiscoverOrg), "##0.00")
Label21.Caption = "Adjustment of " + Format$(Str(OrgTotal - NetAdj), "##0.00")
'X101

    Open ("N:\IRISCHECK\" + RestNum.Text + "X1.csv") For Output As #6
    Open "\\bnewest\share1\polling\poll\" + RestNum.Text + ".IRS" For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "X101" Then
            Print #6, linedata
        End If
    Loop
    Close #1, #6
If FileLen("N:\IRISCHECK\" + RestNum.Text + "X1.csv") = 0 Then
    Kill ("N:\IRISCHECK\" + RestNum.Text + "X1.csv")
End If
List1.Clear
Dim OrderNum As String
Dim EmployeeID As String
Dim ApprovalCode As String
Dim TrnasType As String
VDeclined = 0
MDeclined = 0
DDeclined = 0
ADeclined = 0
If Dir("N:\IRISCHECK\" + RestNum.Text + "X1.csv") <> "" Then
Open ("N:\IRISCHECK\" + RestNum.Text + "X1.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, RestDate, Junk3, OrderNum, TotalAmt, TransType, Junk4, CardType, EmployeeID, ApprovalCode
        List1.AddItem OrderNum + "    " + Mid$(CardType, 1, 1) + "      " + Format$(Str(TotalAmt), "$###.00")
        If TotalAmt > 0 Then
            If Mid$(CardType, 1, 1) = "V" Then
                VDeclined = VDeclined + TotalAmt
            End If
            If Mid$(CardType, 1, 1) = "M" Then
                MDeclined = MDeclined + TotalAmt
            End If
            If Mid$(CardType, 1, 1) = "D" Then
                DDeclined = DDeclined + TotalAmt
            End If
            If Mid$(CardType, 1, 1) = "A" Then
                ADeclined = ADeclined + TotalAmt
            End If
        End If
    Loop
Close (6)
Label16.Caption = Format$(Str(VDeclined), "##0.00")
Label17.Caption = Format$(Str(MDeclined), "##0.00")
Label18.Caption = Format$(Str(DDeclined), "##0.00")
Label19.Caption = Format$(Str(ADeclined), "##0.00")
Label15 = Format$(Str(ADeclined + MDeclined + DDeclined + VDeclined), "##0.00")
Comments.Text = "Offline Declined"
Visa.Text = Format$(Str(VisaOrg - VDeclined), "##0.00")
MC.Text = Format$(Str(MCOrg - MDeclined), "##0.00")
Discover.Text = Format$(Str(DiscoverOrg - DDeclined), "##0.00")
AE.Text = Format$(Str(AEorg - ADeclined), "##0.00")
Else
    List1.AddItem "  No offline declined"
    List1.AddItem "  transactions."
End If



EditS103.Refresh
End Sub

Private Sub Command2_Click()
    Comments.Text = "Partial Offline Declined"
End Sub

Private Sub CreateFiles_Click()
    If Default.Value = True Then
            Response = MsgBox("Please identify yourself.", vbInformation, "Error")
            Exit Sub
    End If
    If RestNum.Text = "" Then
            Response = MsgBox("Restaurant Number Missing", vbInformation, "Error")
            Exit Sub
    End If
    If Visa.Text = "" Then
            Response = MsgBox("Visa Information Missing", vbInformation, "Error")
            Exit Sub
    End If

    If Bob.Value = True Then CBY = "Bob"
    If Brad.Value = True Then CBY = "Brad"
    If Dannie.Value = True Then CBY = "Dannie"
    If Sheila.Value = True Then CBY = "Sheila"
    If Jody.Value = True Then CBY = "Jody"


    Open ("N:\IRISCHECK\" + RestNum.Text + "S.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, RestNumber, SDATE, Tender, TenderAmount, TenderCount
        RecordType = Junk1
        
        If Tender = "201" Then
            VisaOrg = TenderAmount
            VisaOrgC = TenderCount
        End If
        If Tender = "202" Then
            MCOrg = TenderAmount
            MCOrgC = TenderCount
        End If
        If Tender = "203" Then
            AEorg = TenderAmount
            AEOrgC = TenderCount
        End If
        If Tender = "204" Then
            DiscoverOrg = TenderAmount
            DiscoverOrgC = TenderCount
        End If
        If Tender = "100" Then
            CashOrgC = TenderCount
            CashOrg = TenderAmount
        End If
        If Tender = "809" Then
            GCOrg = TenderAmount
            GCOrgC = TenderCount
        End If
    Loop

    Close #6
NetOrg = VisaOrg + MCOrg + AEorg + DiscoverOrg
NetAdj = Val(Visa.Text) + Val(MC.Text) + Val(Discover.Text) + Val(AE.Text)
NetBal = NetAdj - NetOrg

If NetBal > 25 Then
    Response = MsgBox("The Net Adjustment Amount of" + Str(NetBal) + " is greater than $25.00.", vbInformation, "Adjustment Greater Than $10.00")
Else

 
    
    FileCopy ("\\bnewest\share1\polling\poll\" + RestNum.Text + ".IRS"), ("\\bnewest\share1\polling\poll\" + RestNum.Text + ".ORG")
    Open ("\\bnewest\share1\polling\AS400\" + RestNum.Text + ".IRS") For Output As #6
    Open "\\bnewest\share1\polling\poll\" + RestNum.Text + ".IRS" For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) <> "S103" Then
            Print #6, linedata
        End If
    Loop
        
        
        Print #6, Chr(34) + "BneRefNum" + Chr(34) + "," + Chr(34) + "StoreNum" + Chr(34) + "," + Chr(34) + "BusinessDate" + Chr(34) + "," + Chr(34) + "TenderType" + Chr(34) + "," + Chr(34) + "TotalAmt" + Chr(34) + "," + Chr(34) + "Qty"
        Print #6, Chr(34) + RecordType + Chr(34) + "," + LTrim(Str(RestNumber)) + "," + LTrim(Str(SDATE)) + ",100," + Format$(LTrim(Str(CashOrg)), "######.00") + "," + LTrim(Str(CashOrgC))
        If Visa.Text <> 0 Then
            Print #6, Chr(34) + RecordType + Chr(34) + "," + LTrim(Str(RestNumber)) + "," + LTrim(Str(SDATE)) + ",201," + Format$(LTrim(Str(Visa.Text)), "######.00") + "," + LTrim(Str(VisaOrgC))
        End If
        If MC.Text <> 0 Then
                If MCOrgC = 0 Then MCOrgC = 1
                Print #6, Chr(34) + RecordType + Chr(34) + "," + LTrim(Str(RestNumber)) + "," + LTrim(Str(SDATE)) + ",202," + Format$(LTrim(Str(MC.Text)), "######.00") + "," + LTrim(Str(MCOrgC))
        End If
        If AE.Text <> 0 Then
            If AEOrgC = 0 Then AEOrgC = 1
            Print #6, Chr(34) + RecordType + Chr(34) + "," + LTrim(Str(RestNumber)) + "," + LTrim(Str(SDATE)) + ",203," + Format$(LTrim(Str(AE.Text)), "######.00") + "," + LTrim(Str(AEOrgC))
        End If
        If Discover.Text <> 0 Then
            If DiscoverOrgC = 0 Then DiscoverOrgC = 1
            Print #6, Chr(34) + RecordType + Chr(34) + "," + LTrim(Str(RestNumber)) + "," + LTrim(Str(SDATE)) + ",204," + Format$(LTrim(Str(Discover.Text)), "######.00") + "," + LTrim(Str(DiscoverOrgC))
        End If
        If GCOrg <> 0 Then
            Print #6, Chr(34) + RecordType + Chr(34) + "," + LTrim(Str(RestNumber)) + "," + LTrim(Str(SDATE)) + ",809," + Format$(LTrim(Str(GCOrg)), "######.00") + "," + LTrim(Str(GCOrgC))
        End If
    Close #1, #6

    FileCopy ("\\bnewest\share1\polling\AS400\" + RestNum.Text + ".IRS"), ("\\bnewest\share1\polling\poll\" + RestNum.Text + ".IRS")

If Dir("\\bnewest\share1\polling\Data\Credit Card Adjustments.csv") = "" Then
    Open ("\\bnewest\share1\polling\Data\Credit Card Adjustments.csv") For Output As #9
        Print #9, "####,DateTime,Org Visa,Adj Visa,Org MC,Adj MC,Org AE,Adj AE,Org D,Adj D,Net Adj,User,Comment"
    Close #9
CreateFiles.Enabled = False
End If


NetOrg = VisaOrg + MCOrg + AEorg + DiscoverOrg
NetAdj = Val(Visa.Text) + Val(MC.Text) + Val(Discover.Text) + Val(AE.Text)
NetBal = NetAdj - NetOrg
Open ("\\bnewest\share1\polling\Data\Credit Card Adjustments.csv") For Append As #9
Print #9, Str(RestNumber) + "," + Str(Now()) + "," + Str(VisaOrg) + "," + Str(Visa.Text) + ",";
Print #9, Str(MCOrg) + "," + Str(MC.Text) + ",";
Print #9, Str(AEorg) + "," + Str(AE.Text) + ",";
Print #9, Str(DiscoverOrg) + "," + Str(Discover.Text) + ",";
Print #9, Str(NetBal) + "," + CBY + ","; Comments.Text

Close #9
RestNum.Text = ""
Visa = ""
MC = ""
Discover = ""
AE = ""

    Response = MsgBox("Net Adjustment Amount is " + Str(NetBal) + ".", vbInformation, "Adjustment Saved")

End If
List1.Clear
Comments.Text = ""
VDeclined = 0
MDeclined = 0
DDeclined = 0
ADeclined = 0
Label16.Caption = Format$(Str(VDeclined), "##0.00")
Label17.Caption = Format$(Str(MDeclined), "##0.00")
Label18.Caption = Format$(Str(DDeclined), "##0.00")
Label19.Caption = Format$(Str(ADeclined), "##0.00")
Label24.Caption = Format$(Str(VDeclined), "##0.00")
Label25.Caption = Format$(Str(MDeclined), "##0.00")
Label26.Caption = Format$(Str(DDeclined), "##0.00")
Label27.Caption = Format$(Str(ADeclined), "##0.00")
Label15 = Format$(Str(ADeclined + MDeclined + DDeclined + VDeclined), "##0.00")
EditS103.Refresh
EditS103.Hide

End Sub

Private Sub Dannie_Click()
Bob.Value = False
Dannie.Value = True
Sheila.Value = False
Brad.Value = False
Jody.Value = False
End Sub

Private Sub Default_Click()
Bob.Value = False
Dannie.Value = False
Sheila.Value = False
Brad.Value = False
Jody.Value = False
End Sub

Private Sub Discover_Change()
NetAdj = Val(Visa.Text) + Val(MC.Text) + Val(Discover.Text) + Val(AE.Text)
OrgTotal = Label14.Caption
Label13.Caption = Format$(Str(NetAdj), "#,##0.00")
Label21.Caption = "Adjustment of " + Format$(Str(NetAdj - OrgTotal), "##0.00")
EditS103.Refresh
End Sub

Private Sub Form_Load()
List1.Clear
Comments.Text = ""
VDeclined = 0
MDeclined = 0
DDeclined = 0
ADeclined = 0
Label16.Caption = Format$(Str(VDeclined), "##0.00")
Label17.Caption = Format$(Str(MDeclined), "##0.00")
Label18.Caption = Format$(Str(DDeclined), "##0.00")
Label19.Caption = Format$(Str(ADeclined), "##0.00")
Label24.Caption = Format$(Str(VDeclined), "##0.00")
Label25.Caption = Format$(Str(MDeclined), "##0.00")
Label26.Caption = Format$(Str(DDeclined), "##0.00")
Label27.Caption = Format$(Str(ADeclined), "##0.00")
Label15 = Format$(Str(ADeclined + MDeclined + DDeclined + VDeclined), "##0.00")
EditS103.Refresh
End Sub

Private Sub Jody_Click()
Bob.Value = False
Dannie.Value = False
Sheila.Value = False
Brad.Value = False
Jody.Value = True
End Sub

Private Sub MC_Change()
NetAdj = Val(Visa.Text) + Val(MC.Text) + Val(Discover.Text) + Val(AE.Text)
OrgTotal = Label14.Caption
Label13.Caption = Format$(Str(NetAdj), "#,##0.00")
Label21.Caption = "Adjustment of " + Format$(Str(NetAdj - OrgTotal), "##0.00")
EditS103.Refresh
End Sub

Private Sub Sheila_Click()
Bob.Value = False
Dannie.Value = False
Sheila.Value = True
Brad.Value = False
Jody.Value = False
End Sub

Private Sub Visa_Change()
NetAdj = Val(Visa.Text) + Val(MC.Text) + Val(Discover.Text) + Val(AE.Text)
OrgTotal = Label14.Caption
Label13.Caption = Format$(Str(NetAdj), "#,##0.00")
Label21.Caption = "Adjustment of " + Format$(Str(NetAdj - OrgTotal), "##0.00")
EditS103.Refresh
End Sub
