VERSION 5.00
Begin VB.Form S100S103 
   Caption         =   "Check S100 and S101"
   ClientHeight    =   4395
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9975
   LinkTopic       =   "Form2"
   ScaleHeight     =   4395
   ScaleWidth      =   9975
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Copy file(s) to the AS400 and FIX\Backup folders"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1440
      TabIndex        =   7
      Top             =   3600
      Width           =   5775
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2460
      Left            =   240
      TabIndex        =   4
      Top             =   960
      Width           =   8175
   End
   Begin VB.FileListBox File1 
      Height          =   2430
      Left            =   8520
      Pattern         =   "*.irs"
      TabIndex        =   2
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8520
      TabIndex        =   0
      Top             =   3600
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "Filename                        Rest #                   S100 (Net Sales)                   S101 (Hourly Sales)        Difference"
      Height          =   375
      Left            =   240
      TabIndex        =   6
      Top             =   600
      Width           =   7815
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   4080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Directory of Files"
      Height          =   375
      Left            =   8520
      TabIndex        =   3
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "This program will check all *.IRS files in the \\BNS1\SHARE1\POLLING\FIX folder."
      Height          =   375
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Width           =   7455
   End
End
Attribute VB_Name = "S100S103"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
For Y = 0 To File1.ListCount - 1
    FromFile = "\\bns1\share1\polling\poll\FIX\" + File1.List(Y)
    ToFile = "\\bns1\share1\polling\AS400\" + File1.List(Y)
    'FileCopy FromFile, ToFile
    ToFile = "\\bns1\share1\polling\poll\FIX\Backup\" + File1.List(Y)
    FileCopy FromFile, ToFile
    ToFile = "\\bns1\share1\polling\poll\" + File1.List(Y)
    FileCopy FromFile, ToFile
    Kill FromFile
Next Y
End
End Sub

Private Sub Form_Load()
    File1.Pattern = "10??.irs;11??.irs;12??.irs;130?.irs;131?.irs;132?.irs;133?.irs;134?.irs;135?.irs;136?.irs;137?.irs;138?.irs;14??.irs;15??.irs;16??.irs;18??.irs;19??.irs;2???.irs;3???.irs;4???.irs;5???.irs;9???.irs"
    File1.FileName = "\\bns1\share1\polling\Poll\Fix"
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating S101 work file " + Mid$(File1.List(Y), 1, 4)
'PHASE 1 Create Work Files S101
    Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "S.csv") For Output As #6
    Open "\\bns1\share1\polling\poll\FIX\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "S101" Then
            Print #6, linedata
        End If
    Loop
    Close #1, #6
    R = R + 1
    If R = 25 Then
        DoEvents
        S100S103.Refresh
        R = 0
    End If
Next Y
R = 0
DoEvents
Form1.Refresh
For Y = 0 To File1.ListCount - 1
    zerocount = 0
    Label3.Caption = "Creating S100 work file " + Mid$(File1.List(Y), 1, 4)
'PHASE 1 Create Work Files S100
    Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "S1.csv") For Output As #6
    Open "\\bns1\share1\polling\poll\FIX\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "S100" Then
            Print #6, linedata
        End If
    Loop
    Close #1, #6
    R = R + 1
    If R = 25 Then
        DoEvents
        S100S103.Refresh
        R = 0
    End If
Next Y
R = 0
DoEvents

For Y = 0 To File1.ListCount - 1
'S100 NetSales
If Dir("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "S1.csv") <> "" Then
Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "S1.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, Junk3, NetSales, Tax1, Tax2, Tax3, Tax4, Tax5, Junk10, Junk11, Jun12, Junk13, Junk14, Junk15, Junk16, Junk17, Junk18, Junk19, Junk20, Junk21, Junk22, Junk23, Junk24, Junk25, Junk26, Junk27, GiftCardSales
    Loop
Close (6)
End If

'S101 Hourly Sales
Deposit = 0
If Dir("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "S.csv") <> "" Then
Open ("N:\IRISCHECK\" + Mid$(File1.List(Y), 1, 4) + "S.csv") For Input As #6
    Do Until EOF(6)
        Input #6, Junk1, Junk2, Junk3, Junk4, SDeposit, Junk6, HSales, Junk7
        Deposit = Deposit + HSales
    Loop
Close (6)
End If
    List1.AddItem " " + Mid$(File1.List(Y), 1, 4) + "                 " + Str(Junk2) + "                 " + Str(NetSales) + "                 " + Str(Deposit) + "                 " + Str(Deposit - NetSales)


Next Y






 Label3.Caption = "Processing complete!"
S100S103.Refresh

End Sub
