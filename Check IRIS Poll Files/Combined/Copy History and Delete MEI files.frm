VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "IRIS Poll File Check Utility"
   ClientHeight    =   7965
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8700
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   12
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Copy History and Delete MEI files.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7965
   ScaleWidth      =   8700
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox Check1 
      Caption         =   "Only Hardee's"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   7080
      TabIndex        =   31
      Top             =   4800
      Width           =   1335
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Check All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6960
      TabIndex        =   0
      Top             =   5400
      Width           =   1455
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6960
      TabIndex        =   4
      Top             =   3480
      Width           =   1455
   End
   Begin VB.ListBox List6 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   6960
      TabIndex        =   27
      Top             =   600
      Width           =   1455
   End
   Begin VB.ListBox List5 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      Left            =   240
      TabIndex        =   22
      Top             =   4680
      Width           =   1455
   End
   Begin VB.ListBox List4 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      Left            =   1920
      TabIndex        =   20
      Top             =   4680
      Width           =   4935
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   7200
      Width           =   1455
   End
   Begin VB.ListBox List3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   5280
      TabIndex        =   17
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5280
      TabIndex        =   3
      Top             =   3480
      Width           =   1455
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3600
      TabIndex        =   2
      Top             =   3480
      Width           =   1455
   End
   Begin VB.ListBox List2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   3600
      TabIndex        =   14
      Top             =   600
      Width           =   1455
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   240
      TabIndex        =   11
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6960
      TabIndex        =   1
      Top             =   6240
      Width           =   1455
   End
   Begin VB.FileListBox File1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Left            =   1920
      Pattern         =   "*.irs"
      TabIndex        =   7
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Create History and Delete Current Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   8400
      TabIndex        =   6
      Top             =   7920
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   375
      Left            =   240
      TabIndex        =   30
      Top             =   3720
      Width           =   3255
   End
   Begin VB.Line Line1 
      X1              =   240
      X2              =   8520
      Y1              =   4200
      Y2              =   4200
   End
   Begin VB.Label Label16 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6960
      TabIndex        =   29
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Label Label15 
      Alignment       =   2  'Center
      Caption         =   "Missing P100"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6960
      TabIndex        =   28
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label14 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   26
      Top             =   7200
      Width           =   2415
   End
   Begin VB.Label Label13 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   25
      Top             =   6960
      Width           =   2295
   End
   Begin VB.Label Label12 
      Caption         =   "Label9"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   24
      Top             =   6960
      Width           =   1335
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      Caption         =   "Missing G100 "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   23
      Top             =   4320
      Width           =   1455
   End
   Begin VB.Label Label10 
      Caption         =   "Gift Card Transactions Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2040
      TabIndex        =   21
      Top             =   4320
      Width           =   3015
   End
   Begin VB.Label Label9 
      Caption         =   "Label9"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5280
      TabIndex        =   19
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Caption         =   "Missing R102 "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5280
      TabIndex        =   18
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "Label7"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3600
      TabIndex        =   16
      Top             =   3240
      Width           =   1455
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "Missing Poll Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3600
      TabIndex        =   15
      Top             =   240
      Width           =   1455
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   13
      Top             =   3240
      Width           =   1455
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Restaurants"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   12
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label Label3 
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   4680
      TabIndex        =   10
      Top             =   7200
      Width           =   3375
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1920
      TabIndex        =   9
      Top             =   3240
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "\\Snap_is\Polling\Poll"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   8
      Top             =   240
      Width           =   1815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()
List1.Clear
Call Form_Load
Call Command3_Click
End Sub

Private Sub Command1_Click()
For X = 0 To File1.ListCount - 1
    Label3.Caption = "Processing file copy " + File1.List(X)
    Form1.Refresh
    FileCopy "\\Snap_is\polling\Poll\PEInv\" + File1.List(X), "\\Snap_is\polling\Poll\PEInv\History\" + File1.List(X)
Next X
Label3.Caption = " "
Kill "\\Snap_is\polling\Poll\PEInv\*.MEI"
File1.Refresh
Label2.Caption = Str(File1.ListCount) + " files received"
End Sub

Private Sub Command2_Click()
End
End Sub

Private Sub Command3_Click()
List2.Clear
File1.Refresh
Label2.Caption = Str(File1.ListCount) + " files received"
For X = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(X) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(X)
    End If
Next X
Label7.Caption = Str(List2.ListCount) + " missing"
End Sub

Private Sub Command4_Click()
List3.Clear
For Y = 0 To File1.ListCount - 1
    ZeroCount = 0
    Label3.Caption = "Checking R102 " + File1.List(Y)
    Form1.Refresh
    Open "\\Snap_is\polling\poll\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "R102" Then
            ZeroCount = ZeroCount + 1
            DoEvents
        End If
    Loop
    Close (1)
    If ZeroCount = 0 Then
        'ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List3.AddItem Mid$(File1.List(Y), 1, 4)
    End If
            Label3.Caption = File1.List(Y) + " zero count = " + Str(ZeroCount)
            Label9.Caption = Str(List3.ListCount) + " missing"
            Form1.Refresh
Next Y
    Label3.Caption = "R102 Check Complete!"
Label9.Caption = Str(List3.ListCount) + " missing"
End Sub

Private Sub Command5_Click()
GCSales = 0
GCTender = 0
List5.Clear
List4.Clear
For Y = 0 To File1.ListCount - 1
    ZeroCount = 0
    Label3.Caption = "Checking G100 " + File1.List(Y)
    Sales = False
    Tender = False
    Form1.Refresh
    Open "\\Snap_is\polling\poll\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If (Val(File1.List(Y)) < 6000) Then
            If Mid$(linedata, 2, 4) = "S103" And Mid$(linedata, 22, 3) = "809" Then
                List4.AddItem Mid$(File1.List(Y), 1, 4) + " Tender"
                Tender = True
                GCTender = GCTender + 1
            End If
            If Mid$(linedata, 2, 4) = "M100" And Mid$(linedata, 28, 4) = "2329" Then
                If Sales = False Then
                    List4.AddItem Mid$(File1.List(Y), 1, 4) + " Sale"
                    Sales = True
                    GCSales = GCSales + 1
                End If
            End If
            If Mid$(linedata, 2, 4) = "G100" Then
                ZeroCount = ZeroCount + 1
                List4.AddItem linedata
            End If
        End If
            If (Val(File1.List(Y)) > 7000) And (Val(File1.List(Y)) < 8000) Then
            If Mid$(linedata, 2, 4) = "S103" And Mid$(linedata, 22, 3) = "809" Then
                List4.AddItem Mid$(File1.List(Y), 1, 4) + " Tender"
                Tender = True
                GCTender = GCTender + 1
            End If
            If Mid$(linedata, 2, 4) = "M100" And Mid$(linedata, 24, 3) = "215" Then
                If Sales = False Then
                    List4.AddItem Mid$(File1.List(Y), 1, 4) + " Sale"
                    Sales = True
                    GCSales = GCSales + 1
                End If
            End If
            If Mid$(linedata, 2, 4) = "G100" Then
                ZeroCount = ZeroCount + 1
                List4.AddItem linedata
            End If
        End If

    Loop
    Close (1)
    If (Tender = True Or Sales = True) And ZeroCount = 0 Then
        List5.AddItem Mid$(File1.List(Y), 1, 4)
    End If
    Label12.Caption = Str(List5.ListCount) + " missing"
    Form1.Refresh
Next Y
    Label3.Caption = "G100 Check Complete!"
    Label12.Caption = Str(List5.ListCount) + " missing"
    Label13.Caption = "Sales   = " + Str(GCSales)
    Label14.Caption = "Tenders = " + Str(GCTender)
    Form1.Refresh
End Sub

Private Sub Command6_Click()
List6.Clear
For Y = 0 To File1.ListCount - 1
    ZeroCount = 0
    Label3.Caption = "Checking P100 " + File1.List(Y)
    Form1.Refresh
    Open "\\Snap_is\polling\poll\" + File1.List(Y) For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) = "P100" Then
            ZeroCount = ZeroCount + 1
            DoEvents
        End If
    Loop
    Close (1)
    If ZeroCount < 5 Then
        'ZeroMsg = MsgBox("Restaurant " + File1.List(Y) + " has " + LTrim(Str(zerocount)) + " lines with a zero count.  Please check.", vbCritical, "Problem File")
         List6.AddItem Mid$(File1.List(Y), 1, 4) + " -" + Str(ZeroCount) + " Record(s)"
    End If
            Label16.Caption = Str(List6.ListCount) + " missing"
            Form1.Refresh
Next Y
    Label3.Caption = "P100 Check Complete!"
Label16.Caption = Str(List6.ListCount) + " missing"

End Sub

Private Sub Command7_Click()
Call Command3_Click
Call Command4_Click
Call Command6_Click
Call Command5_Click
Label3.Caption = "All Checks Complete!"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
End Sub

Private Sub Form_Load()

  Open "\\Snap_is\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unt
    If Check1.Value = False Then
        If Mid$(unt, 1, 1) = "H" Or Mid$(unt, 1, 1) = "C" Or Mid$(unt, 1, 1) = "M" Then
            List1.AddItem Mid$(unt, 4, 4)
        End If
    Else
    If Mid$(unt, 1, 1) = "H" Then
            List1.AddItem Mid$(unt, 4, 4)
    End If
    End If
  Loop
  Close (1)

Label5.Caption = Str(List1.ListCount) + " restaurants"
File1.FileName = "\\Snap_is\polling\Poll\"
Label2.Caption = Str(File1.ListCount) + " files received"

For X = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To File1.ListCount - 1
        If List1.List(X) = Mid$(File1.List(Y), 1, 4) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        List2.AddItem List1.List(X)
    End If
Next X
Label7.Caption = Str(List2.ListCount) + " missing"
Label9.Caption = Str(List3.ListCount) + " missing"
Label12.Caption = Str(List5.ListCount) + " missing"
Label16.Caption = Str(List6.ListCount) + " missing"
Label17.Caption = LCase(Format$(Now(), "MM/DD/YY HH:MM ampm"))
End Sub

