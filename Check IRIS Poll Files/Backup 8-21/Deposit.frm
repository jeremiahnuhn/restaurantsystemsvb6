VERSION 5.00
Begin VB.Form DepositFrm 
   Caption         =   "Correct Deposit"
   ClientHeight    =   4260
   ClientLeft      =   5685
   ClientTop       =   4590
   ClientWidth     =   7200
   LinkTopic       =   "Form2"
   ScaleHeight     =   4260
   ScaleWidth      =   7200
   Begin VB.TextBox CorrectedDeposit 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4800
      TabIndex        =   6
      Top             =   1200
      Width           =   1455
   End
   Begin VB.TextBox RestNum 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   600
      TabIndex        =   0
      Top             =   480
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Read Original File"
      Height          =   1095
      Left            =   600
      TabIndex        =   3
      Top             =   1080
      Width           =   1335
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Correct Deposit"
      Enabled         =   0   'False
      Height          =   615
      Left            =   2400
      TabIndex        =   2
      Top             =   2760
      Width           =   3975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Close"
      Height          =   615
      Left            =   2400
      TabIndex        =   1
      Top             =   3480
      Width           =   3975
   End
   Begin VB.Label Label5 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2400
      TabIndex        =   9
      Top             =   2160
      Width           =   4695
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "No commas"
      Height          =   255
      Left            =   4920
      TabIndex        =   8
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Corrected Deposit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2400
      TabIndex        =   7
      Top             =   1320
      Width           =   2295
   End
   Begin VB.Label Label1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2400
      TabIndex        =   5
      Top             =   600
      Width           =   3735
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Restaurant Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   600
      TabIndex        =   4
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "DepositFrm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

RestNum = ""
CorrectedDeposit.Text = ""
Label1.Caption = " "
Label5.Caption = " "

DepositFrm.Hide
End Sub

Private Sub Command2_Click()
Dim BagNumber As String
Dim RestNumber As String
Dim RecordType As String
Dim DepNumber As String
Dim SDATE As String

If Dir("N:\IRISCHECK\" + RestNum.Text + "R.csv") <> "" Then
Open ("N:\IRISCHECK\" + RestNum.Text + "R.csv") For Input As #6
    Do Until EOF(6)
    '"BneRefNum","StoreNum","BusinessDate","DepositNumber","Amt","BagNumber"
        Input #6, RecordType, RestNumber, SDATE, DepNumber, DepAmt, BagNumber
    Loop
Close (6)
End If

    FileCopy ("\\bns1\share1\polling\poll\Processed\" + RestNum.Text + ".IRS"), ("\\bns1\share1\polling\poll\Processed\" + RestNum.Text + ".ORG")
    Open ("\\bnewest\share1\polling\Poll\FIX\" + RestNum.Text + ".IRS") For Output As #6
    Open "\\bns1\share1\polling\poll\Processed\" + RestNum.Text + ".IRS" For Input As #1
    Do Until EOF(1)
        Line Input #1, linedata
        If Mid$(linedata, 2, 4) <> "R102" Then
            If Mid$(linedata, 40, 7) <> "Deposit" Then
                Print #6, linedata
            End If
        End If
    Loop
        
            '"BneRefNum","StoreNum","BusinessDate","DepositNumber","Amt","BagNumber"
        Print #6, Chr(34) + "BneRefNum" + Chr(34) + "," + Chr(34) + "StoreNum" + Chr(34) + "," + Chr(34) + "BusinessDate" + Chr(34) + "," + Chr(34) + "DepositNumber" + Chr(34) + "," + Chr(34) + "Amt" + Chr(34) + "," + Chr(34) + "BagNumber"
        Print #6, Chr(34) + RecordType + Chr(34) + "," + LTrim(RestNumber) + "," + LTrim(Str(SDATE)) + "," + LTrim(Str(DepNumber)) + "," + Format$(LTrim(Str(CorrectedDeposit)), "######.00") + "," + Chr(34) + LTrim(BagNumber) + Chr(34)
    Close #1, #6

    FileCopy ("\\bns1\share1\polling\poll\FIX\" + RestNum.Text + ".IRS"), ("\\bns1\share1\polling\as400\" + RestNum.Text + ".IRS")

    Kill ("\\bns1\share1\polling\poll\FIX\" + RestNum.Text + ".IRS")
    
If Dir("\\bns1\share1\polling\Data\Deposit Corrections.csv") = "" Then
    Open ("\\bns1\share1\polling\Data\Deposit Corrections.csv") For Output As #9
        Print #9, "####,DateTime,Polled Deposit,Corrected Deposit"
    Close #9
End If

Open ("\\bnewest\share1\polling\Data\Deposit Corrections.csv") For Append As #9
    Print #9, Str(RestNumber) + "," + Str(Now()) + "," + Str(DepAmt) + "," + Str(CorrectedDeposit)
Close #9

Open ("N:\CorrectDeposit.SQL") For Output As #9
    Print #9, "/* Correct Deposit for " + Str(RestNumber) + "  DateTime " + Str(Now) + " */"
    Print #9, "Update tblEODDepositTrans Set Amount = " + Str(CorrectedDeposit) + " where DepositNum = " + Str(DepNumber)
    Print #9, "Update tblEODDepositTrans Set FaceValue = " + Str(CorrectedDeposit) + " where DepositNum = " + Str(DepNumber)
Close #9

RestNum = ""
CorrectedDeposited = ""
    
Response = MsgBox("Deposit correction made.  Please transfer N:\CorrectDeposit.SQL to the Restaurant's C:\Support folder, then use the FE Utility Password dep to update the Restaurant's database.", vbInformation, "Correction Complete")

DepositFrm.Hide

'Update tblEODDepositTrans Set Amount = 1.00 where DepositNumber =11
'Update tblEODDepositTrans Set FaceValue = 1.00 where DepositNumber =11

End Sub

Private Sub Command3_Click()
Dim BagNumber As String
Dim RestNumber As String
Dim RecordType As String
Dim DepNumber As String
Dim SDATE As String

If Dir("N:\IRISCHECK\" + RestNum.Text + "R.csv") <> "" Then
Open ("N:\IRISCHECK\" + RestNum.Text + "R.csv") For Input As #6
    Do Until EOF(6)
    '"BneRefNum","StoreNum","BusinessDate","DepositNumber","Amt","BagNumber"
        Input #6, RecordType, RestNumber, SDATE, DepNumber, DepAmt, BagNumber
    Loop
Close (6)
Command2.Enabled = True
CorrectedDeposit.Enabled = True
Label1.Caption = "Polled Deposit = $" + Format$(Str(DepAmt), "#,###.00")
DepositFrm.Refresh
End If
End Sub

Private Sub CorrectedDeposit_Change()

Label5.Caption = "Polled - Corrected = $" + Format$(Str(Val(DepAmt) - Val(CorrectedDeposit.Text)), "#,###.00")
End Sub
