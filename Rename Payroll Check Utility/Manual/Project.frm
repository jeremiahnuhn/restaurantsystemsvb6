VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Rename Payroll Check List Utility"
   ClientHeight    =   4140
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11205
   LinkTopic       =   "Form1"
   ScaleHeight     =   4140
   ScaleWidth      =   11205
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Rename"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3480
      TabIndex        =   6
      Top             =   2640
      Width           =   5175
   End
   Begin VB.TextBox Text2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2520
      TabIndex        =   3
      Text            =   "Text1"
      Top             =   1800
      Width           =   7455
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2520
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   840
      Width           =   7455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3480
      TabIndex        =   0
      Top             =   3240
      Width           =   5175
   End
   Begin VB.Label Label3 
      Caption         =   "New"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   960
      TabIndex        =   5
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "Current"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   960
      TabIndex        =   4
      Top             =   960
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Restaurant List ""S:\Find\Directory.TXT"""
      Height          =   375
      Left            =   3000
      TabIndex        =   1
      Top             =   120
      Width           =   3375
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
On Error Resume Next

Open "S:\Find\Directory.TXT" For Input As #1
Dim OfficeNumb As String
Dim Unit As String
Dim RestName As String
Dim CKE As String

CurrentFile2 = Text1.Text
NewFile2 = Text2.Text


Do While Not EOF(1)
    Input #1, Unit, RestName, SPEEDDIAL, OfficeNumb, VP, Director, DM, CKE, Payroll, Accounting, FE, TID1, TID2, TID3, TID4, ID

    CurrentFile = "\\bns1\share1\fieldreports\" + Unit + "\Secure\" + CurrentFile2
    
    NewFile = "\\bns1\share1\fieldreports\" + Unit + "\Secure\" + NewFile2

    FileCopy CurrentFile, NewFile

    Kill (CurrentFile)

Loop
Close #1
x = MsgBox("Rename complete!", vbInformation, "Pay Check Report Rename")

End


End Sub

Private Sub Form_Load()

On Error Resume Next

FileDay = Format(Now - 3, "dd")
NewFileDate = Format((Now - 3), "mm_dd_yy")
    CurrentFile = "Payroll_Check_List_UNT_" + FileDay + ".bnr"
    
    NewFile = "Payroll_Check_List_UNT_" + NewFileDate + ".bnr"


Text1.Text = CurrentFile
Text2.Text = NewFile
End Sub
