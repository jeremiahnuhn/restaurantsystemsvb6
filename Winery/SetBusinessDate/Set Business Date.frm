VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Form1 
   Caption         =   "Winery: Set Business Date on Festival Terminal"
   ClientHeight    =   3870
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9015
   LinkTopic       =   "Form1"
   ScaleHeight     =   3870
   ScaleWidth      =   9015
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Exit Utility"
      Height          =   735
      Left            =   6960
      TabIndex        =   4
      Top             =   1920
      Width           =   1695
   End
   Begin MSComCtl2.MonthView CalEntry 
      Bindings        =   "Set Business Date.frx":0000
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "M/d/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   3
      EndProperty
      Height          =   2370
      Left            =   1080
      TabIndex        =   2
      Top             =   480
      Width           =   2700
      _ExtentX        =   4763
      _ExtentY        =   4180
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      StartOfWeek     =   16056321
      CurrentDate     =   40490
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Save Business Date Change"
      Height          =   735
      Left            =   6960
      TabIndex        =   0
      Top             =   720
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000000&
      BorderStyle     =   0  'None
      DataField       =   "BusinessDate"
      DataSource      =   "Data1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4200
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   1680
      Width           =   2175
   End
   Begin VB.Data Data1 
      Caption         =   "POSLIVE"
      Connect         =   "Access"
      DatabaseName    =   "\\BNE_REG2\C\IRIS\DATA\POSLIVE.MDB"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4320
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "tblBusinessDate"
      Top             =   3360
      Visible         =   0   'False
      Width           =   2100
   End
   Begin VB.Label Label2 
      Caption         =   "Read error.  Please make sure Terminal 2 is powered on and the network cable is connected."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   1080
      TabIndex        =   5
      Top             =   2880
      Visible         =   0   'False
      Width           =   7215
   End
   Begin VB.Line Line4 
      X1              =   6600
      X2              =   6600
      Y1              =   960
      Y2              =   2280
   End
   Begin VB.Line Line3 
      X1              =   4080
      X2              =   4080
      Y1              =   960
      Y2              =   2280
   End
   Begin VB.Line Line2 
      X1              =   6600
      X2              =   4080
      Y1              =   960
      Y2              =   960
   End
   Begin VB.Line Line1 
      X1              =   6600
      X2              =   4080
      Y1              =   2280
      Y2              =   2280
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Current Business Date on Terminal 2"
      Height          =   495
      Left            =   4320
      TabIndex        =   3
      Top             =   1200
      Width           =   1935
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CalEntry_DateClick(ByVal DateClicked As Date)
    If Text1.Text = "" Then
        Label2.Visible = True
        Command1.Enabled = False
    Else
        Text1.Text = CalEntry.Value
        Label1.Caption = "Business Date will be set to this date"
    End If
End Sub

Private Sub Command1_Click()
    Data1.UpdateRecord
    Label1.Caption = "Current Business Date on Terminal 2"
End Sub

Private Sub Command2_Click()
End
End Sub

Private Sub Form_Load()
    CalEntry.Value = Date
End Sub
