VERSION 5.00
Begin VB.Form Ping 
   Caption         =   "Ping Menu"
   ClientHeight    =   7245
   ClientLeft      =   4125
   ClientTop       =   1065
   ClientWidth     =   7875
   LinkTopic       =   "Form2"
   ScaleHeight     =   7245
   ScaleWidth      =   7875
   Begin VB.CommandButton Command21 
      Caption         =   "QTimer / HyperView .15"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   20
      Top             =   5640
      Width           =   3375
   End
   Begin VB.CommandButton Command20 
      Caption         =   "Payment Terminal 4"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   19
      Top             =   5640
      Width           =   3375
   End
   Begin VB.CommandButton Command13 
      Caption         =   "QTimer / HyperView .140"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   17
      Top             =   5040
      Width           =   3375
   End
   Begin VB.CommandButton Command19 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   18
      Top             =   6360
      Width           =   7095
   End
   Begin VB.CommandButton Command18 
      Caption         =   "DMB TVs"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   16
      Top             =   4440
      Width           =   3375
   End
   Begin VB.CommandButton Command17 
      Caption         =   "DMB Controllers"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   15
      Top             =   3840
      Width           =   3375
   End
   Begin VB.CommandButton Command16 
      Caption         =   "Valley Protein"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   14
      Top             =   3240
      Width           =   3375
   End
   Begin VB.CommandButton Command15 
      Caption         =   "ADM/Energy Mgmt"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   13
      Top             =   2640
      Width           =   3375
   End
   Begin VB.CommandButton Command14 
      Caption         =   "Alarm Panel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   12
      Top             =   2040
      Width           =   3375
   End
   Begin VB.CommandButton Command12 
      Caption         =   "CloudBox"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   11
      Top             =   1440
      Width           =   3375
   End
   Begin VB.CommandButton Command11 
      Caption         =   "Envysion"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   10
      Top             =   840
      Width           =   3375
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Smart Safe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4080
      TabIndex        =   9
      Top             =   240
      Width           =   3375
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Payment Terminal 3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   8
      Top             =   5040
      Width           =   3375
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Payment Terminal 2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   7
      Top             =   4440
      Width           =   3375
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Payment Terminal 1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   6
      Top             =   3840
      Width           =   3375
   End
   Begin VB.CommandButton Command6 
      Caption         =   "IRIS Terminal 6"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   5
      Top             =   3240
      Width           =   3375
   End
   Begin VB.CommandButton Command5 
      Caption         =   "IRIS Terminal 5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   4
      Top             =   2640
      Width           =   3375
   End
   Begin VB.CommandButton Command4 
      Caption         =   "IRIS Terminal 4"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   3
      Top             =   2040
      Width           =   3375
   End
   Begin VB.CommandButton Command3 
      Caption         =   "IRIS Terminal 3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   2
      Top             =   1440
      Width           =   3375
   End
   Begin VB.CommandButton Command2 
      Caption         =   "IRIS Terminal 2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   1
      Top             =   840
      Width           =   3375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "IRIS Terminal 1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   3375
   End
End
Attribute VB_Name = "Ping"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Terminal  1"
                Print #1, "cls"
                Print #1, "PING 100.100.100.101"
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command10_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Smart Safe"
                Print #1, "cls"
                Print #1, "PING 192.168.2.10"
                Print #1, "Pause"
      Close #1
          Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command11_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Envysion"
                Print #1, "cls"
                Print #1, "PING 192.168.2.2"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command12_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  CloudBox"
                Print #1, "cls"
                Print #1, "PING 192.168.2.3"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command13_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  QTimer / HyperView"
                Print #1, "cls"
                Print #1, "PING 100.100.100.140"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command14_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Alarm Panel"
                Print #1, "cls"
                Print #1, "PING 192.168.2.199"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command15_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  ADM / Energy Management"
                Print #1, "cls"
                Print #1, "PING 192.168.2.4"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command16_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Valley Protein"
                Print #1, "cls"
                Print #1, "PING 192.168.2.5"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command17_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Digital Menu Board Controllers"
                Print #1, "Echo off"
                Print #1, "cls"
                Print #1, "Echo Controller 1"
                Print #1, "PING 192.168.2.71"
                Print #1, "Echo."
                Print #1, "Echo Controller 2"
                Print #1, "PING 192.168.2.72"
                Print #1, "Echo."
                Print #1, "Echo Controller 3"
                Print #1, "PING 192.168.2.73"
                Print #1, "Echo."
                Print #1, "Echo Controller 4"
                Print #1, "PING 192.168.2.74"
                
                Print #1, "Pause"
      Close #1
          Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command18_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Digital Menu Board TVs"
                Print #1, "Echo off"
                Print #1, "cls"
                Print #1, "Echo TV 1"
                Print #1, "PING 192.168.2.81"
                Print #1, "Echo."
                Print #1, "Echo TV 2"
                Print #1, "PING 192.168.2.82"
                Print #1, "Echo."
                Print #1, "Echo TV 3"
                Print #1, "PING 192.168.2.83"
                Print #1, "Echo."
                Print #1, "Echo TV 4"
                Print #1, "PING 192.168.2.84"
                
                Print #1, "Pause"
      Close #1
          Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command19_Click()
    End
End Sub

Private Sub Command2_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Terminal  2"
                Print #1, "cls"
                Print #1, "PING 100.100.100.102"
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command20_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Payment Terminal 4"
                Print #1, "cls"
                Print #1, "PING 192.168.2.224"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)

End Sub

Private Sub Command21_Click()
     
    If Mid$(unumber, 3, 1) = "0" Then
        Ping3 = Mid$(unumber, 4, 1)
    Else
        Ping3 = Mid$(unumber, 3, 2)
    End If
     
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  QTimer / HyperView"
                Print #1, "cls"
                Print #1, "PING 10." + Mid$(unumber, 1, 2) + "." + Ping3 + ".15"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)

End Sub

Private Sub Command3_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Terminal  3"
                Print #1, "cls"
                Print #1, "PING 100.100.100.103"
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command4_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Terminal  4"
                Print #1, "cls"
                Print #1, "PING 100.100.100.104"
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command5_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Terminal  5"
                Print #1, "cls"
                Print #1, "PING 100.100.100.105"
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command6_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Terminal  6"
                Print #1, "cls"
                Print #1, "PING 100.100.100.106"
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command7_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Payment Terminal  1"
                Print #1, "cls"
                Print #1, "PING 192.168.2.221"
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command8_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Payment Terminal  2"
                Print #1, "cls"
                Print #1, "PING 192.168.2.222"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Command9_Click()
     Open "c:\ih.BAT" For Output As #1
                Print #1, "Title Ping  Payment Terminal  3"
                Print #1, "cls"
                Print #1, "PING 192.168.2.223"
                Print #1, "Pause"
      Close #1
                Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub
