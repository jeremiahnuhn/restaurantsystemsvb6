VERSION 5.00
Begin VB.Form Concept12 
   Caption         =   "Drive-Thru Terminal Configuration (T1 and T2)"
   ClientHeight    =   5970
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8640
   LinkTopic       =   "Form2"
   ScaleHeight     =   5970
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command5 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   5
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   720
      TabIndex        =   4
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Counter (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   3
      Top             =   1800
      Width           =   2295
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Counter (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   720
      TabIndex        =   2
      Top             =   1800
      Width           =   2295
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Close without making any changes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1440
      TabIndex        =   0
      Top             =   4680
      Width           =   5415
   End
   Begin VB.Label Label1 
      Caption         =   "Terminal 1                                              Terminal 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1080
      TabIndex        =   1
      Top             =   960
      Width           =   6495
   End
End
Attribute VB_Name = "Concept12"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
'T 1 Normal
      X = MsgBox("This process will configure Terminal 1 to work as normal.", vbInformation, "Change Terminal 1")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 1 Where PeriodID = 11 and RegID = 1"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 6 Where PeriodID = 12 and RegID = 1"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command3_Click()
'T 4 Normal
      X = MsgBox("This process will configure Terminal 2 to work as normal.", vbInformation, "Change Terminal 2")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 1 Where PeriodID = 11 and RegID = 2"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 6 Where PeriodID = 12 and RegID = 2"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command4_Click()
'T 1 Dual
      X = MsgBox("This process will configure Terminal 1 to work as Order Taker and Cashier.", vbInformation, "Change Terminal 1")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 5 Where PeriodID = 11 And RegID = 1"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 7 Where PeriodID = 12 And RegID = 1"
      Close #1
            If Hour(Now) < 15 Then
                CheckDate = Date - 1
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date
            End If
      
      Open "c:\dt.vbs" For Output As #1
        Print #1, "ConnectionString = " + Chr(34) + "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\iris\data\poslive.mdb" + Chr(34)
        Print #1, "SQL = " + Chr(34) + "Delete From tblOrder Where State = 4 And BusinessDate <= cdate('" + Str(CheckDate) + "')" + Chr(34)
        Print #1, "Set cn = CreateObject(" + Chr(34) + "ADODB.Connection" + Chr(34) + ")"
        Print #1, "Set cmd = CreateObject(" + Chr(34) + "ADODB.Command" + Chr(34) + ")"
        Print #1, "cn.Open ConnectionString"
        Print #1, "cmd.ActiveConnection = cn"
        Print #1, "cmd.CommandText = SQL"
        Print #1, "cmd.execute"
        Print #1, "cn.Close"
      Close #1
      
 
      
      
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
            Print #1, "C:\windows\system32\wscript.exe c:\DT.vbs"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T1 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)

    End If
    X = Shell(exe, vbMaximizedFocus)

      End
      Exit Sub
End Sub

Private Sub Command5_Click()
'T 2 Dual
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 5 Where PeriodID = 11 and RegID = 2"
        Print #1, "Update tblRegTimeConcepts Set ConceptID = 7 Where PeriodID = 12 and RegID = 2"
      Close #1
      
            If Hour(Now) < 15 Then
                CheckDate = Date - 1
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date
            End If
      
      Open "c:\dt.vbs" For Output As #1
        Print #1, "ConnectionString = " + Chr(34) + "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\iris\data\poslive.mdb" + Chr(34)
        Print #1, "SQL = " + Chr(34) + "Delete From tblOrder Where State = 4 And BusinessDate <= cdate('" + Str(CheckDate) + "')" + Chr(34)
        Print #1, "Set cn = CreateObject(" + Chr(34) + "ADODB.Connection" + Chr(34) + ")"
        Print #1, "Set cmd = CreateObject(" + Chr(34) + "ADODB.Command" + Chr(34) + ")"
        Print #1, "cn.Open ConnectionString"
        Print #1, "cmd.ActiveConnection = cn"
        Print #1, "cmd.CommandText = SQL"
        Print #1, "cmd.execute"
        Print #1, "cn.Close"
      Close #1
      
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
           Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
           Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
           Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
           Print #1, "C:\windows\system32\wscript.exe c:\DT.vbs"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T2 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)

    End If
    X = Shell(exe, vbMaximizedFocus)


      End
    Exit Sub
End Sub
