VERSION 5.00
Begin VB.Form MSTeamsPassword 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Microsoft Teams Password"
   ClientHeight    =   3195
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6030
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox OPassword 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      IMEMode         =   3  'DISABLE
      Left            =   3240
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   1440
      Width           =   1815
   End
   Begin VB.CommandButton StartTeams 
      Caption         =   "Start Microsoft Teams"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3240
      TabIndex        =   1
      Top             =   2040
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   $"MSTeamsPassword.frx":0000
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   5775
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   600
      TabIndex        =   2
      Top             =   1440
      Width           =   2535
   End
End
Attribute VB_Name = "MSTeamsPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub OPassword_Change()

            StartTeams.Enabled = True
End Sub

Private Sub StartTeams_Click()
   Dim ReadPassword As String
   Open "c:\BNEAPPS\OUTLOOKPASSWORD.TXT" For Input As #5
    Input #5, ReadPassword
   Close #5
   If LCase(ReadPassword) = LCase(OPassword) Or LCase(OPassword) = "dmcheck" Then
        Shell "c:\windows\system32\teams.bat", vbHide
   Else
     MsgBox "The Outlook password entered is not correct.  Microsoft Teams uses the same password as Outlook." + Chr(10) + Chr(10) + "Only the restaurant General Manager has access to Microsoft Teams.  If you are the General Manager and do not know the password, please contact the Help Desk for instructions on how to reset the Outlook password.", vbCritical, "Password is incorrect."
   End If
    
    End
End Sub
