VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form Form1 
   Caption         =   "BNE Desktop"
   ClientHeight    =   10110
   ClientLeft      =   180
   ClientTop       =   465
   ClientWidth     =   13440
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   -1  'True
      Strikethrough   =   -1  'True
   EndProperty
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   10110
   ScaleWidth      =   13440
   Begin VB.CommandButton Command11 
      Caption         =   "Future Use"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   4800
      Picture         =   "Form1.frx":0442
      TabIndex        =   59
      ToolTipText     =   " Print or view reports, paper forms, and manuals. "
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton Command9 
      Caption         =   "&Quick Help"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   6720
      Picture         =   "Form1.frx":24FE
      TabIndex        =   3
      ToolTipText     =   " Print or view reports, paper forms, and manuals. "
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton Command2 
      Appearance      =   0  'Flat
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      Picture         =   "Form1.frx":45BA
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Support (FE Utilities)"
      Top             =   9000
      Width           =   735
   End
   Begin VB.CommandButton Exit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   12480
      Picture         =   "Form1.frx":49FC
      TabIndex        =   6
      Top             =   9000
      Width           =   855
   End
   Begin VB.CommandButton LoadPrintO 
      Caption         =   "&Print Orientation Guides"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   10560
      Picture         =   "Form1.frx":6AB8
      TabIndex        =   5
      ToolTipText     =   " Print new employee Orientation Forms. "
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton LoadLiveConnect 
      Caption         =   "Live&Connect"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   8640
      Picture         =   "Form1.frx":8B74
      TabIndex        =   4
      ToolTipText     =   " Update Outlook (Send and receive mail) and Update Live Reporter. "
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton LoadLiveReporter 
      Caption         =   "Live&Reporter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   2880
      Picture         =   "Form1.frx":AC30
      TabIndex        =   2
      ToolTipText     =   " Print or view reports, paper forms, and manuals. "
      Top             =   9000
      Width           =   1815
   End
   Begin VB.CommandButton LoadOutlook 
      Caption         =   "&Outlook"
      DisabledPicture =   "Form1.frx":CCEC
      DownPicture     =   "Form1.frx":D12E
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   960
      Picture         =   "Form1.frx":D570
      TabIndex        =   1
      ToolTipText     =   " Electronic mail, electronic forms, calendar, task list, update with Live Connect. "
      Top             =   9000
      Width           =   1815
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   8655
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   15266
      _Version        =   393216
      Tabs            =   5
      Tab             =   2
      TabsPerRow      =   5
      TabHeight       =   1147
      TabMaxWidth     =   25
      BackColor       =   13160660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Message"
      TabPicture(0)   =   "Form1.frx":F62C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "PrintSystem"
      Tab(0).Control(1)=   "PrintAudit"
      Tab(0).Control(2)=   "AUDITMESSAGE"
      Tab(0).Control(3)=   "SystemMessage"
      Tab(0).Control(4)=   "Label2"
      Tab(0).Control(5)=   "Label3"
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Programs"
      TabPicture(1)   =   "Form1.frx":F648
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "MonthView1"
      Tab(1).Control(1)=   "Command10"
      Tab(1).Control(2)=   "QTimer"
      Tab(1).Control(3)=   "LoadCameraSystem"
      Tab(1).Control(4)=   "LoadCalculator"
      Tab(1).Control(5)=   "LoadYeahWrite"
      Tab(1).Control(6)=   "PeriodEndInventory"
      Tab(1).Control(7)=   "LoadXcelleNetMail"
      Tab(1).ControlCount=   8
      TabCaption(2)   =   "PM Check List"
      TabPicture(2)   =   "Form1.frx":F664
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Label1"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "LabelCompA"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "Label7"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "Label6"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "LabelDescA"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "Label5"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "Label8"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "Label9"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "Label10"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "Label11"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).Control(10)=   "LabelCompB"
      Tab(2).Control(10).Enabled=   0   'False
      Tab(2).Control(11)=   "LabelCompC"
      Tab(2).Control(11).Enabled=   0   'False
      Tab(2).Control(12)=   "LabelCompD"
      Tab(2).Control(12).Enabled=   0   'False
      Tab(2).Control(13)=   "LabelCompE"
      Tab(2).Control(13).Enabled=   0   'False
      Tab(2).Control(14)=   "LabelCompF"
      Tab(2).Control(14).Enabled=   0   'False
      Tab(2).Control(15)=   "Label12"
      Tab(2).Control(15).Enabled=   0   'False
      Tab(2).Control(16)=   "LabelCompG"
      Tab(2).Control(16).Enabled=   0   'False
      Tab(2).Control(17)=   "Label13"
      Tab(2).Control(17).Enabled=   0   'False
      Tab(2).Control(18)=   "LabelCompH"
      Tab(2).Control(18).Enabled=   0   'False
      Tab(2).Control(19)=   "Label14"
      Tab(2).Control(19).Enabled=   0   'False
      Tab(2).Control(20)=   "Label15"
      Tab(2).Control(20).Enabled=   0   'False
      Tab(2).Control(21)=   "LabelCompI"
      Tab(2).Control(21).Enabled=   0   'False
      Tab(2).Control(22)=   "CBB"
      Tab(2).Control(22).Enabled=   0   'False
      Tab(2).Control(23)=   "CBA"
      Tab(2).Control(23).Enabled=   0   'False
      Tab(2).Control(24)=   "CBC"
      Tab(2).Control(24).Enabled=   0   'False
      Tab(2).Control(25)=   "CBD"
      Tab(2).Control(25).Enabled=   0   'False
      Tab(2).Control(26)=   "CBE"
      Tab(2).Control(26).Enabled=   0   'False
      Tab(2).Control(27)=   "CBF"
      Tab(2).Control(27).Enabled=   0   'False
      Tab(2).Control(28)=   "CBG"
      Tab(2).Control(28).Enabled=   0   'False
      Tab(2).Control(29)=   "CBH"
      Tab(2).Control(29).Enabled=   0   'False
      Tab(2).Control(30)=   "CBI"
      Tab(2).Control(30).Enabled=   0   'False
      Tab(2).ControlCount=   31
      TabCaption(3)   =   "Computer Based Training"
      TabPicture(3)   =   "Form1.frx":F680
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "LoadLaborProCBT"
      Tab(3).Control(1)=   "LoadLiveConnectCBT"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Support"
      TabPicture(4)   =   "Form1.frx":F69C
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Label4"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Command8"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Command7"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "Command5"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "Command4"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "Command3"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).Control(6)=   "CheckVersionButton"
      Tab(4).Control(6).Enabled=   0   'False
      Tab(4).Control(7)=   "CreditCard"
      Tab(4).Control(7).Enabled=   0   'False
      Tab(4).Control(8)=   "Command1"
      Tab(4).Control(8).Enabled=   0   'False
      Tab(4).Control(9)=   "LoadPCAnywhere"
      Tab(4).Control(9).Enabled=   0   'False
      Tab(4).Control(10)=   "LoadUpdateLinks"
      Tab(4).Control(10).Enabled=   0   'False
      Tab(4).Control(11)=   "LoadAutopol"
      Tab(4).Control(11).Enabled=   0   'False
      Tab(4).Control(12)=   "Command6"
      Tab(4).Control(12).Enabled=   0   'False
      Tab(4).ControlCount=   13
      Begin VB.CommandButton CBI 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   8640
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   4680
         Width           =   735
      End
      Begin VB.CommandButton CBH 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   4800
         Width           =   735
      End
      Begin VB.CommandButton CBG 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   4080
         Width           =   735
      End
      Begin VB.CommandButton CBF 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   7800
         Width           =   735
      End
      Begin VB.CommandButton CBE 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   6840
         Width           =   735
      End
      Begin VB.CommandButton CBD 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   6000
         Width           =   735
      End
      Begin VB.CommandButton CBC 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   2880
         Width           =   735
      End
      Begin VB.CommandButton Command6 
         BackColor       =   &H00FF0000&
         Caption         =   "Chicken Tender Menu Change"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -66120
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   3480
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.CommandButton LoadXcelleNetMail 
         Caption         =   "Line Buster Usage"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   -74640
         TabIndex        =   35
         Top             =   3000
         Visible         =   0   'False
         Width           =   4935
      End
      Begin VB.CommandButton PeriodEndInventory 
         Caption         =   "IRIS Drive-Thru Order Times"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   -74640
         TabIndex        =   34
         Top             =   2040
         Width           =   4935
      End
      Begin VB.CommandButton LoadYeahWrite 
         Caption         =   "Yeah Write"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   -74640
         TabIndex        =   33
         Top             =   1080
         Width           =   4935
      End
      Begin VB.CommandButton LoadAutopol 
         Caption         =   "Load Energy Management System (ST Only!)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -70440
         TabIndex        =   32
         Top             =   3480
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.CommandButton LoadUpdateLinks 
         Caption         =   "Update Links for IRIS Databases"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -66120
         TabIndex        =   31
         Top             =   4560
         Width           =   4215
      End
      Begin VB.CommandButton LoadPCAnywhere 
         Caption         =   "Use the Phone Line to access Internet Explorer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -70440
         TabIndex        =   30
         Top             =   4560
         Width           =   4215
      End
      Begin VB.CommandButton LoadLiveConnectCBT 
         Caption         =   "Outlook / LiveConnect"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -72120
         TabIndex        =   29
         Top             =   1560
         Width           =   6735
      End
      Begin VB.CommandButton LoadLaborProCBT 
         Caption         =   "LaborPro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Left            =   -72120
         TabIndex        =   28
         Top             =   3720
         Width           =   6735
      End
      Begin VB.CommandButton LoadCalculator 
         Caption         =   "Calculator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   -67920
         TabIndex        =   24
         Top             =   5040
         Width           =   3735
      End
      Begin VB.CommandButton PrintSystem 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -74880
         TabIndex        =   23
         Top             =   7920
         Width           =   12975
      End
      Begin VB.CommandButton PrintAudit 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -68400
         TabIndex        =   22
         Top             =   7920
         Visible         =   0   'False
         Width           =   6495
      End
      Begin VB.CommandButton LoadCameraSystem 
         Caption         =   "Camera System"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   -74640
         TabIndex        =   21
         Top             =   3960
         Visible         =   0   'False
         Width           =   4935
      End
      Begin VB.CommandButton Command1 
         Caption         =   "FE Utilities"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -70440
         TabIndex        =   20
         Top             =   6240
         Width           =   4215
      End
      Begin VB.CommandButton CreditCard 
         Caption         =   "Test Gift Card Modem and Telephone Connection"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -66120
         TabIndex        =   19
         Top             =   6240
         Width           =   4215
      End
      Begin VB.CommandButton CheckVersionButton 
         BackColor       =   &H0000C000&
         Caption         =   "Update IRIS Menu, Prices, and Coupons"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -74760
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   6240
         Width           =   4215
      End
      Begin VB.CommandButton Command3 
         BackColor       =   &H000000FF&
         Caption         =   "Emergency Priority Equipment Guidelines (Facilities Management)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -74760
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   840
         Width           =   6375
      End
      Begin VB.CommandButton Command4 
         BackColor       =   &H00C000C0&
         Caption         =   "Credit Card Authorization Instructions for when broadband (internet) is not working."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -74760
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2640
         Width           =   6375
      End
      Begin VB.CommandButton QTimer 
         Caption         =   "Configure QTimer Payment Window"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   -74640
         TabIndex        =   15
         Top             =   4920
         Visible         =   0   'False
         Width           =   4935
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Stop Printing"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -74760
         TabIndex        =   14
         Top             =   4560
         Width           =   4215
      End
      Begin VB.CommandButton Command7 
         BackColor       =   &H00FF0000&
         Caption         =   "Troubleshooting - Drive Thru Terminals"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -68280
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   840
         Width           =   6375
      End
      Begin VB.CommandButton Command8 
         BackColor       =   &H0000FFFF&
         Caption         =   "This button will open a document containing the known issues currently in the IRIS system."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -68280
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   2640
         Width           =   6375
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Print Food Prep Tags"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   -74640
         TabIndex        =   11
         Top             =   5880
         Visible         =   0   'False
         Width           =   4935
      End
      Begin VB.CommandButton CBA 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1440
         Width           =   735
      End
      Begin VB.CommandButton CBB 
         BackColor       =   &H000000FF&
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   27.75
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2160
         Width           =   735
      End
      Begin MSComCtl2.MonthView MonthView1 
         Height          =   3420
         Left            =   -67920
         TabIndex        =   25
         Top             =   1320
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   6033
         _Version        =   393216
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowToday       =   0   'False
         StartOfWeek     =   79036417
         CurrentDate     =   39057
      End
      Begin RichTextLib.RichTextBox AUDITMESSAGE 
         Height          =   6735
         Left            =   -68400
         TabIndex        =   26
         Top             =   1080
         Visible         =   0   'False
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   11880
         _Version        =   393217
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         MousePointer    =   1
         TextRTF         =   $"Form1.frx":F6B8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin RichTextLib.RichTextBox SystemMessage 
         Height          =   6735
         Left            =   -74880
         TabIndex        =   27
         Top             =   1080
         Width           =   12975
         _ExtentX        =   22886
         _ExtentY        =   11880
         _Version        =   393217
         Enabled         =   -1  'True
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         MousePointer    =   1
         TextRTF         =   $"Form1.frx":F738
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label LabelCompI 
         Caption         =   "Last Completed I"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   9480
         TabIndex        =   69
         Top             =   5040
         Width           =   3375
      End
      Begin VB.Label Label15 
         Caption         =   "All Fryers - Boil Out"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9480
         TabIndex        =   68
         Top             =   4680
         Width           =   2415
      End
      Begin VB.Label Label14 
         Caption         =   "Quarterly"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8400
         TabIndex        =   66
         Top             =   4200
         Width           =   1575
      End
      Begin VB.Label LabelCompH 
         Caption         =   "Last Completed H"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   65
         Top             =   5160
         Width           =   6615
      End
      Begin VB.Label Label13 
         Caption         =   "Safety - Deck Brushing Dining Room and Restrooms"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   64
         Top             =   4800
         Width           =   11175
      End
      Begin VB.Label LabelCompG 
         Caption         =   "Last Completed G"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   62
         Top             =   4440
         Width           =   6495
      End
      Begin VB.Label Label12 
         Caption         =   "Safety - Deck Brushing Frontline and Backline"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   61
         Top             =   4080
         Width           =   11175
      End
      Begin VB.Label LabelCompF 
         Caption         =   "Last Completed F"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   58
         Top             =   8160
         Width           =   9735
      End
      Begin VB.Label LabelCompE 
         Caption         =   "Last Completed E"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   57
         Top             =   7440
         Width           =   9735
      End
      Begin VB.Label LabelCompD 
         Caption         =   "Last Completed D"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   56
         Top             =   6360
         Width           =   9735
      End
      Begin VB.Label LabelCompC 
         Caption         =   "Last Completed C"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   55
         Top             =   3720
         Width           =   9735
      End
      Begin VB.Label LabelCompB 
         Caption         =   "Last Completed B"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   54
         Top             =   2520
         Width           =   9735
      End
      Begin VB.Label Label11 
         Caption         =   "Exhaust Fan Grease Container / Bucket - empty into grease barrel (located at dumpster corral)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   53
         Top             =   7800
         Width           =   11415
      End
      Begin VB.Label Label10 
         Caption         =   "HVAC and Exhaust Fan Belts - Check and replace as required.  Replacement belts can be ordered using the Equipment Order eForm."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   1680
         TabIndex        =   52
         Top             =   6840
         Width           =   10575
      End
      Begin VB.Label Label9 
         Caption         =   "HVAC Air Filters - replace filters on ALL systems."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   51
         Top             =   6000
         Width           =   10575
      End
      Begin VB.Label Label8 
         Caption         =   $"Form1.frx":F7B8
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   1680
         TabIndex        =   50
         Top             =   2880
         Width           =   11415
      End
      Begin VB.Label Label5 
         Caption         =   "Drink Systems - check pressure on water filters.  If less than 25 pounds...change water filter ASAP."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   49
         Top             =   2160
         Width           =   11415
      End
      Begin VB.Label Label2 
         Caption         =   "System Message"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   44
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label Label3 
         Caption         =   "EcoSure Audits"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -68280
         TabIndex        =   43
         Top             =   840
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Version 2.26 September 2016"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   -70320
         TabIndex        =   42
         Top             =   8280
         Width           =   3975
      End
      Begin VB.Label LabelDescA 
         Caption         =   "All Fryers - Clean "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   40
         Top             =   1440
         Width           =   10575
      End
      Begin VB.Label Label6 
         Caption         =   "Weekly"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   720
         TabIndex        =   39
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "Monthly"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   600
         TabIndex        =   38
         Top             =   5520
         Width           =   1215
      End
      Begin VB.Label LabelCompA 
         Caption         =   "Last Completed A"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   37
         Top             =   1800
         Width           =   9735
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Preventive Maintenance Check List"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   20.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   495
         Left            =   600
         TabIndex        =   41
         Top             =   840
         Width           =   12255
      End
   End
   Begin RichTextLib.RichTextBox LCHelp 
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   8520
      Visible         =   0   'False
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   450
      _Version        =   393217
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      MousePointer    =   1
      TextRTF         =   $"Form1.frx":F88D
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CBA_Click()
BoxComp = InputBox("Enter the name of the person who completed the Preventive Maintenance.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBA.Caption = "P"
CBA.BackColor = &HC000&
LabelCompA = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",A," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"
End Sub

Private Sub CBB_Click()
BoxComp = InputBox("Enter the name of the person who completed the Preventive Maintenance.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBB.Caption = "P"
CBB.BackColor = &HC000&
LabelCompB = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",B," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"
End Sub

Private Sub CBC_Click()
BoxComp = InputBox("Enter the name of the person who completed the Preventive Maintenance.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBC.Caption = "P"
CBC.BackColor = &HC000&
LabelCompC = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",C," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"
End Sub

Private Sub CBD_Click()
BoxComp = InputBox("Enter the name of the person who completed the Preventive Maintenance.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBD.Caption = "P"
CBD.BackColor = &HC000&
LabelCompD = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",D," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"
End Sub

Private Sub CBE_Click()
BoxComp = InputBox("Enter the name of the person who completed the Preventive Maintenance.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBE.Caption = "P"
CBE.BackColor = &HC000&
LabelCompE = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",E," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"
End Sub

Private Sub CBF_Click()
BoxComp = InputBox("Enter the name of the person who completed the Preventive Maintenance.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBF.Caption = "P"
CBF.BackColor = &HC000&
LabelCompF = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",F," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"
End Sub

Private Sub CBG_Click()
BoxComp = InputBox("Enter the name of the person who completed the Deck Brushing.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBG.Caption = "P"
CBG.BackColor = &HC000&
LabelCompG = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",G," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"

End Sub

Private Sub CBH_Click()
BoxComp = InputBox("Enter the name of the person who completed the Deck Brushing.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBH.Caption = "P"
CBH.BackColor = &HC000&
LabelCompH = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",G," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3

Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"


End Sub

Private Sub CBI_Click()
BoxComp = InputBox("Enter the name of the person who completed the Fryers Boil Out.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBI.Caption = "P"
CBI.BackColor = &HC000&
LabelCompI = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Print #1, Junk2 + ",I" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",I," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"


End Sub

Private Sub CheckVersionButton_Click()
'      ChDir ("C:\Program Files\Religent\LiveConnect 6.0 Client")
      X = MsgBox("This button will connect to the Home Office to retrieve updated IRIS POS Menu configuration files and then update your IRIS system with the changes.  The terminals will automatically receive these updates.  Your cashiers might be asked to Perform or Postpone, please ask them to touch Perform.  If this process does not resolve the issue with the IRIS POS Menu, please contact the BNE Help Desk for additional assistance.", vbInformation, "Update IRIS POS Menu")
'      Y = Shell("C:\program files\religent\liveconnect 6.0 Client\lcclient.exe -p " + Chr(34) + "Hardees - Update IRIS POS Menu" + Chr(34), vbNormalFocus)
    ChDir "C:\EdmWeb"
    Y = Shell("C:\EdmWeb\transfer_web_update_pos.bat", 1)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  Menu Updated Button"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
      End
End Sub

Private Sub Command1_Click()
FrmPassword.Show
End Sub

Private Sub Command10_Click()
    ChDir ("c:\program files\Lucas")
    X = Shell("C:\program files\Lucas\FoodPrepTags", vbNormalFocus)
End Sub

Private Sub Command11_Click()
Y = Shell("C:\Program Files\Internet Explorer\iexplore.exe", vbNormalFocus)
End
End Sub

Private Sub Command12_Click()
BoxComp = InputBox("Enter the name of the person who completed the Boil Out of the Fryers.", "Completed By")
If BoxComp = "" Then
    X = MsgBox("Completed By was blank, please try again.", vbCritical, "Entry Error")
    Exit Sub
End If
CBG.Caption = "P"
CBG.BackColor = &HC000&
LabelCompG = "Last Completed " + Format$(Now(), "MM/DD/YY") + " By " + BoxComp
Form1.Refresh
Open "c:\bneapps\PM.NEW" For Output As 1
Open "c:\bneapps\PM.txt" For Input As 2
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",A" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",B" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",C" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",D" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",E" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",F" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",G" + "," + Format$(Now(), "MM/DD/YY") + "," + Chr(34) + BoxComp + Chr(34)
Open "c:\bneapps\PM.LOG" For Append As 3
    Print #3, Junk2 + "," + RTrim(Now()), ",G," + Format$(Now(), "MM/DD/YY") + "," + BoxComp
Close #3
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",H" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Input #2, Junk2, Item, CompDate, CompBy
Print #1, Junk2 + ",I" + "," + CompDate + "," + Chr(34) + CompBy + Chr(34)
Close #1
Close #2
Kill ("c:\bneapps\pm.txt")
FileCopy "c:\bneapps\pm.new", "c:\bneapps\pm.txt"


End Sub

Private Sub Command2_Click()
FrmPassword.Show
End Sub

Private Sub Command3_Click()
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\Paper Forms\Emergency Priority Equipment.pdf" + Chr(34)
    Else
        exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\Paper Forms\Emergency Priority Equipment.pdf" + Chr(34)
    End If
    X = Shell(exe, vbMaximizedFocus)
 End
End Sub

Private Sub Command4_Click()
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Credit Card Authorization without broadband.pdf" + Chr(34)
    Else
        exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Credit Card Authorization without broadband.pdf" + Chr(34)
    End If
    X = Shell(exe, vbMaximizedFocus)
 End
End Sub

Private Sub Command5_Click()
      X = MsgBox("This process will delete all items in the print spooler.", vbInformation, "Stop Printing")
       Open "c:\swipe.bat" For Output As #1
        Print #1, "NET STOP spooler"
        Print #1, "DEL %SYSTEMROOT%\System32\spool\PRINTERS\* /Q"
        Print #1, "NET START spooler"
      Close #1
      Y = Shell("C:\swipe.bat", vbMinimizedFocus)
      End
      Exit Sub
      
      'Brians commands
      'net stop spooler
      'del %systemroot%\system32\spool\printers\*.shd
      'del %systemroot%\system32\spool\printers\*.spl
      'net start spooler

End Sub

Private Sub Command6_Click()
frmtblAvailableItem.Show
End Sub

Private Sub Command7_Click()
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Troubleshooting - Drive Thru Terminals.pdf" + Chr(34)

    End If
    X = Shell(exe, vbMaximizedFocus)
    End
End Sub

Private Sub Command8_Click()
   If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Known IRIS Issues.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\Reports\IRIS Manual\Known IRIS Issues.pdf" + Chr(34)
    End If
    X = Shell(exe, vbMaximizedFocus)
    End
End Sub

Private Sub Command9_Click()
QuickHelp.Show
End Sub

Private Sub CreditCard_Click()

'response = MsgBox("It is not longer necessary to use this button as of September 1, 2011.  The system should automatically use the phone line to process Gift Cards if your broadband (internet) connection is not working.  Please click on the purple button from the Support Tab on the BNE Desktop for complete instructions.  Please make sure the restaurant phone is not in use when trying to process a Gift Card when the broadband (internet) connection is not working.  If your system can not accept Gift Cards using the phone line, please contact the BNE Help Desk at 1-877-773-8983 ext 1437 for assistance.", vbInformation, "Updated Message!                 Please Read!")
'  End
' The statements below to edit the INI files are not longer needed as of 9/1/2011 due to the ability for IRIS to switch to dial gift cards.
' 11/1/2011 Changed button to allow easy testing of the Gift Card.

Open "c:\program files\xpient solutions\credit card application\SVS\tpe_svs.ini" For Input As #1
Do While Not EOF(1)
    Dim uname As String
    Line Input #1, Inline
    If Mid$(Inline, 1, 14) = "USE_SSL_AUTH=Y" Then
        response = MsgBox("Gift Card processing is setup for normal processing via the Internet (broadband) with automatic backup using the Gift Card modem and restaurant's voice phone line." + Chr(10) + Chr(10) + "Do you want to test the Gift Card modem and phone line?", vbYesNo, "Test Gift Card Processing")
        If response = vbYes Then
            Close 1
            
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Dial Test Setup"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Dial Test Setup"
            Close 2, 3, 4
            FileCopy "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini", "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak"
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "USE_SSL_AUTH=Y"
                        Print #3, "USE_SSL_AUTH=N"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=N"
                    'Case "MODEM_AUTH_COM"
                    '    Print #3, "MODEM_AUTH_COMMUNICATION=Y"
                    'Case "MODEM_SETTLE_C"
                    '    Print #3, "MODEM_SETTLE_COMMUNICATION=Y"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
        Open "c:\fixkvs.bat" For Output As #6
        Print #6, "REM Created " + Str(Now())
        Print #6, "REM Gift Card Modem Backup Test Setup"
        Print #6, "cd \iris\bin"
        Print #6, "POSTEVENT.EXE /STOPSERVICE TPE_SVS.EXE /Q"
        Print #6, "c:\iris\setup\sleep.exe 600"
        Print #6, "POSTEVENT.EXE /STARTSERVICE TPE_SVS.EXE /Q"
        Print #6, "exit"
        Close #6
        Y = Shell("C:\fixkvs.bat", vbMinimizedFocus)

            response = MsgBox("Configuration files changed.  To test the Gift Card system using the Gift Card Modem and the restaurant's voice phone line, please complete a balance inquiry on a new Gift Card.  Please make sure the restaurant's phone line is not being used during your test.  If the balance of 0.00 is received then the system is working correctly.  If not, please contact the Help Desk for assistance.  If the test worked correctly, please use the same button from the BNE Desktop to undo the test to configure the system back to normal operation.", vbInformation, "Test Gift Card Backup System")
            End
        Else
            Close 1
            End
        End If
    End If
    If Mid$(Inline, 1, 14) = "USE_SSL_AUTH=N" Then
        response = MsgBox("Gift Card processing is setup to test Gift Cards using the Gift Card modem and the restaurant's phone line." + Chr(10) + Chr(10) + "Do you want to change Gift Card processing back to normal operations using the Internet (broadband) with automatic backup using the Gift Card modem and restaurant's voice phone line?", vbYesNo, "Stop testing the Gift Card Processing")
        If response = vbYes Then
            Close 1
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Gift Card Normal"
            Close 4
            Open "c:\bneapps\Authorization.Log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Gift Card Normal"

            Close 2, 3, 4
            FileCopy "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini", "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak"
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "USE_SSL_AUTH=N"
                        Print #3, "USE_SSL_AUTH=Y"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=Y"
                    'Case "MODEM_AUTH_COM"
                    '    Print #3, "MODEM_AUTH_COMMUNICATION=N"
                    'Case "MODEM_SETTLE_C"
                    '    Print #3, "MODEM_SETTLE_COMMUNICATION=N"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
        Open "c:\fixkvs.bat" For Output As #6
        Print #6, "REM Created " + Str(Now())
        Print #6, "REM Gift Card Modem Backup Test Return to Normal"
        Print #6, "cd \iris\bin"
        Print #6, "POSTEVENT.EXE /STOPSERVICE TPE_SVS.EXE /Q"
        Print #6, "c:\iris\setup\sleep.exe 600"
        Print #6, "POSTEVENT.EXE /STARTSERVICE TPE_SVS.EXE /Q"
        Print #6, "exit"
        Close #6
        Y = Shell("C:\fixkvs.bat", vbMinimizedFocus)
            response = MsgBox("Configuration files changed. Gift Card processing should now be using the Internet (broadband) with automatic backup using the Gift Card modem and restaurant's voice phone line.", vbInformation, "Gift Card Test Complete")
            End
        Else
            Close 1
            End
        End If
    End If
Loop
Close 1
End

End Sub

Private Sub ESchvAct_Click()
ChDir ("C:\LABOR")
Y = Shell("C:\labor\veritime.exe", vbNormalFocus)
End
End Sub

Private Sub Exit_Click()
    End
End Sub


Private Sub Form_Load()
On Error Resume Next

'LiveConnect 6.0
 Y = Shell("c:\iris\bin\killstartup.bat", 0)

'SSTab1.TabsPerRow = 4
'SSTab1.TabVisible(2) = False


'Check for Line Buster Reg 8
If Dir("c:\iris\reginfo\reg8\ini\iris.ini") <> "" Then
    LoadXcelleNetMail.Visible = True
End If


'DELETE "c:\ih.BAT"
If Dir("c:\ih.bat") <> "" Then
    Kill ("c:\ih.BAT")
End If

If Dir("c:\win7.txt") <> "" Then
   Command11.Enabled = True
   Command11.Caption = "&Internet Explorer"
End If

'If Dir("C:\Hyper\Express Window\OPEN Payment Window.BAT") <> "" Then
'    QTimer.Visible = True
'End If


'RDU No BNE Labor Scheduling Tools
If Dir("c:\nodesys\nolabor.txt") <> "" Then
    ScheduleProjections.Enabled = False
    ScheduleCalculator.Enabled = False
    SchvAct.Enabled = False
    SchvActHistory.Enabled = False
    ESchvAct.Enabled = False
End If

'Check for Camera System Software
If Dir("c:\program files\IVPlayback\ivplay8.exe") <> "" Then
    LoadCameraSystem.Visible = True
End If

'Check for Food Prep Tags
If Dir("C:\program files\Lucas\FoodPrepTags.exe") <> "" Then
    Command10.Visible = True
End If

MonthView1.Value = Now()

' MESSAGE BOXES -

' Terry Lewis Author
If Dir("C:\BNEAPPS\SYSTEM.TXT") <> "" Then
    SystemMessage.FileName = "C:\BNEAPPS\SYSTEM.TXT"
End If
' Angela Maxwell Author
If Dir("C:\BNEAPPS\AUDIT.TXT") <> "" Then
    AUDITMESSAGE.FileName = "C:\BNEAPPS\AUDIT.TXT"
End If
' Restaurant Systems Author
If Dir("C:\BNEAPPS\LCHELP.RTF") <> "" Then
    LCHelp.FileName = "C:\BNEAPPS\LCHELP.RTF"
    'LCHelp.SelRTF = True
End If
ServerVer = "Unavailable"
If Dir("c:\iris\bin\pos.exe") <> "" Then
    ServerVer = Format$(FileDateTime("c:\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case ServerVer
        Case "04042011"
            ServerVersion = "3.7.14 874"
        Case "02162009"
            ServerVersion = "3.7.8.2 151"
        Case "05052010"
            ServerVersion = "3.7.14 848"
        Case "08102010"
            ServerVersion = "3.7.14 856"
        Case "01252011"
            ServerVersion = "3.7.14 872"
        Case "07052012"
            ServerVersion = "3.8 1174"
        Case "08282012"
            ServerVersion = "3.8 1176"
        Case "11012012"
            ServerVersion = "3.8 1406"
        Case "02112013"
            ServerVersion = "3.8 1407"
        Case "05062013"
            ServerVersion = "3.8 1413"
        Case "09232013"
            ServerVersion = "3.8 1416"
        Case "09212015"
            ServerVersion = "4.0 535.600"
        Case Else
            ServerVersion = ServerVer
        End Select
End If

CCAVer = "Unavailable"
If Dir("c:\program files\xpient solutions\credit card application\CCAManager\CCAManager.exe") <> "" Then
    CCAVer = Format$(FileDateTime("c:\program files\xpient solutions\credit card application\CCAManager\CCAManager.exe"), "MMDDYYYY")
'If Dir("c:\Program Files\Xpient Solutions\Credit Card Application\SVS\TPE_SVS.exe") <> "" Then
'    CCAVer = Format$(FileDateTime("c:\Program Files\Xpient Solutions\Credit Card Application\SVS\TPE_SVS.exe"), "MMDDYYYY")
    
    
    Select Case CCAVer
        Case "08202010"
            CCAVersion = "4.5.5.6"
        Case "08192013"
            CCAVersion = "4.5.5.18"
        Case "07222015"
            CCAVersion = "4.6.291"
        Case "02162016"
            CCAVersion = "4.6.345"
        Case Else
            CCAVersion = CCAVer
        End Select
End If


'TOTERM Date
ToTermVer = "01012000"
If Dir("c:\IRIS\BIN\TOTERM.BAT") <> "" Then
    ToTermVer = Format$(FileDateTime("c:\IRIS\BIN\TOTERM.BAT"), "MMDDYYYY")
End If




If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
    Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
    Input #1, unumber, uname, email, junk
    Input #1, unumber, uname, email, junk
    Close #1
    Form1.Caption = "BNE Desktop:   " + uname + "  #" + unumber + "  - IRIS " + ServerVersion + "  - CCA " + CCAVersion
End If


Open "c:\bneapps\IRISVersion.txt" For Output As #4
DATENOW = Format(Now(), "MMDDYYYY")
TIMENOW = Format(Now(), "HH:MM")
Print #4, Str(unumber) + "," + uname + "," + DATENOW + ",POS," + ServerVersion
Print #4, Str(unumber) + "," + uname + "," + DATENOW + ",CCA," + CCAVersion
Print #4, Str(unumber) + "," + uname + "," + DATENOW + ",ToTerm," + ToTermVer
Close 4



If Dir("c:\BNEAPPS\OUTLOOKPASSWORD.TXT") = "" Then
    Open "c:\BNEAPPS\OUTLOOKPASSWORD.TXT" For Output As #5    ' Open file for output.
        QUOTE = "email"
        Print #5, QUOTE
    Close #5
End If

If Command <> "" Then
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - " + Command
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + Command
            Close 4
End If

'Create PM File
If Dir("c:\bneapps\PM.txt") = "" Then
                MyDate = Format(Now(), "mm/dd/yy")
                MyUnit = Mid$(Str(unumber), 2, 4)
                Open "c:\bneapps\PM.txt" For Output As 1
                    Print #1, MyUnit + ",A" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",B" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",C" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",D" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",E" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",F" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",G" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",H" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                    Print #1, MyUnit + ",I" + "," + "01/01/14" + "," + Chr(34) + "Initial Setup" + Chr(34)
                Close #1
End If

'Update PM File


Open "c:\bneapps\PM.txt" For Input As 1

'A
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompA = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff < 7 Then
    CBA.Caption = "P"
    CBA.BackColor = &HC000&
End If
'B
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompB = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff < 7 Then
    CBB.Caption = "P"
    CBB.BackColor = &HC000&
End If
'C
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompC = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff < 7 Then
    CBC.Caption = "P"
    CBC.BackColor = &HC000&
End If
'D
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompD = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff <= 30 Then
    CBD.Caption = "P"
    CBD.BackColor = &HC000&
End If
'E
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompE = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff <= 30 Then
    CBE.Caption = "P"
    CBE.BackColor = &HC000&
End If
'F
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompF = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff <= 30 Then
    CBF.Caption = "P"
    CBF.BackColor = &HC000&
End If
'G
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompG = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff < 7 Then
    CBG.Caption = "P"
    CBG.BackColor = &HC000&
End If

'H
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompH = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff < 7 Then
    CBH.Caption = "P"
    CBH.BackColor = &HC000&
End If

'I
Input #1, Junk1, Item, CompDate, CompBy
DateComp = CDate(CompDate)
Diff = DateDiff("d", DateComp, Now())
LabelCompI = "Last Completed " + Format$(DateComp, "MM/DD/YY") + " By " + CompBy
If Diff < 90 Then
    CBI.Caption = "P"
    CBI.BackColor = &HC000&
End If
Close #1
End Sub

Private Sub LoadAutopol_Click()
    On Error GoTo ERR3
    
    Open "C:\AUTOPOL\FACILITY.DBF" For Binary As 1
    WORKLIST = InputB(1388, #1)
    Close 1
    NEWLIST = Mid$(WORKLIST, 1, 1080) + "DNNNNNNN" + Mid$(WORKLIST, 1089, 299)
    Open "C:\AUTOPOL\FACILITY.DBF" For Output As 1
    Print #1, NEWLIST
    Close 1
    Y = Shell("C:\AUTOPOL\STARTAP.BAT", 1)
    End
    Exit Sub

ERR3:
    If Err = 53 Or Err = 76 Then
        typebox = 0 + 16
        VERSIONT = "Facility File Missing." + Chr(10) + "Please contact support at" + Chr(10) + "252-937-2800 ext 1294" + Chr(10) + "Johnny Thompson"
        response = MsgBox(VERSIONT, typebox, "Required File(s) Missing")
        End
        
    End If
    End
Resume Next

End Sub

Private Sub LoadCalculator_Click()

If Dir("C:\Program Files\Moffsoft FreeCalc\MoffFreeCalc.exe") = "" Then

    msg = "The Calculator is not installed on your computer." + Chr(10) + Chr(10) + "Please contact the Help Desk 1-800-773-8983 ext 1437 and ask them to open a call for a Systems Analyst to install the Calcualtor." ' Define message.
    Style = vbInformation
    Title = "Please contact the Help Desk"  ' Define title.
    response = MsgBox(msg, Style, Title)
Else
    Y = Shell("C:\Program Files\Moffsoft FreeCalc\MoffFreeCalc.exe", vbNormalFocus)
End If
End

End Sub

Private Sub LoadCameraSystem_Click()
    ChDir ("c:\program files\IVPlayback")
    X = Shell("c:\program files\IVPlayback\ivplay8.exe", vbNormalFocus)
End Sub

Private Sub LoadLaborProCBT_Click()
    Y = Shell("c:\bneapps\tutorial\Labor Pro\bne-laborpro-tutorial.exe", vbNormalFocus)
End
End Sub

Private Sub LoadLiveConnect_Click()
' E-Verify Scanner Delete Temporary Files Version 1.55 March 6, 2013 per a request from Brian Milburn
If Dir("C:\telescreen\tsScanner\temp\*.bmp") <> "" Then
    Kill ("C:\telescreen\tsScanner\temp\*.bmp")
End If

If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
    Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
    Input #1, unumber, uname, email, junk
    Input #1, unumber, uname, email, junk
    Close #1
End If


On Error GoTo BypassApp2
    AppActivate "LiveConnect Client"
    End
    Exit Sub
BypassApp2:


CheckName = "C:\Program Files\Religent\LiveConnect 6.0 Client\Profiles\GM" + Trim(unumber) + "\header.cfg"

If Dir(CheckName) <> "" Then
    ChDir "C:\Program Files\Religent\LiveConnect 6.0 Client\"
    X = Shell("C:\Program Files\Religent\LiveConnect 6.0 Client\lcclient.exe", vbNormalFocus)
    End
End If
    
On Error GoTo BypassApp
    AppActivate "LiveConnect"
    End
    Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
   Y = Shell("c:\iris\bin\killstartup.bat", 0)
    pleasewait = MsgBox("It takes about 10 seconds for LiveConnect to start, please wait." + Chr(10) + Chr(10) + "To send and receive Outlook email, send Outlook forms, and update LiveReporter reports, choose the Hardees - Update Outlook and LiveReporter profile.  This process occurs automatically each day during the IRIS End of Day process." + Chr(10) + Chr(10) + "If asked to complete a LiveConnect session by the Help Desk, please choose the Hardees - Daily Communication." + Chr(10) + Chr(10) + "Do not close LiveConnect until you receive the message box labeled Profile Execution Complete.  When you receive this message box, click on OK and then close LiveConnect with the X in the top right corner of the LiveConnect window.", vbInformation, "LiveConnect")
   For Z = 1 To 20000
    DoEvents
   Next Z
    ChDir "c:\Program Files\Religent\LiveConnect 5.2 BNE"
    progname = "\Program Files\Religent\LiveConnect 5.2 BNE\startup.exe -sp -skin LiveConnect"
    ' -sp Profile Execution Complete
    X = Shell(progname, vbNormalFocus)
    Else
   Err.Clear
End If
    End
End Sub

Private Sub LoadLiveConnectCBT_Click()
    Y = Shell("c:\bneapps\tutorial\bne-tutorial.exe", vbNormalFocus)
    End
End Sub

Private Sub LoadLiveReporter_Click()
If Dir("c:\bneapps\BNEViewer.exe") <> "" Then
  ChDir ("C:\Reports")
  LOADPCAW = Shell("C:\BNEApps\BNEViewer", vbMaximizedFocus)
Else
  ChDir ("C:\Program Files\Religent\LiveReporter Client")
  LOADPCAW = Shell("C:\Program Files\Religent\LiveReporter Client\LiveReporter Client.exe", vbMaximizedFocus)
End If

  End
End Sub

Private Sub LoadOutlook_Click()
    OutlookPassword.Show
End Sub

Private Sub LoadPCAnywhere_Click()
'      ChDir ("C:\program files\symantec\pcanywhere")
'      Y = Shell("c:\Program Files\Symantec\pcAnywhere\STOPHOST.EXE", vbNormalFocus)
'      X = MsgBox("Thank You!", vbInformation, "Load PC Anywhere")
'      For G = 1 To 10000000
'        DoEvents
'      Next G
'        Open "C:\PCA.BAT" For Output As #5    ' Open file for output.
'        QUOTE = """"
'       Print #5, QUOTE;
'        Print #5, "C:\Program Files\Symantec\pcAnywhere\awhost32.exe";
'        Print #5, QUOTE;
'        Print #5, " ";
'        Print #5, QUOTE;
'        Print #5, "C:\Documents and Settings\All Users\Application Data\Symantec\pcAnywhere\support.bhf";
'        Print #5, QUOTE
'        Print #5, "EXIT"
'        Close 5
'        W = Shell("C:\pca.bat", nofocus)
'End

X = MsgBox("Do you want to use the Phone Line to access websites using Internet Explorer?  (This should only be used with your broadband method of communication (cable or DSL) is not working.)", vbYesNo, "Access the Internet using the Phone Line")
If X = vbYes Then
        m = MsgBox("To make sure you are not disconnected by someone trying to use the phone in the restaurant, please put a note on each phone.  If someone picks up the phone and leaves it off hook for 10 seconds it will disconnect the computer.", vbInformation, "Don't use phone")
        B = MsgBox("When you connect using this method you will need to enter hardeesweb for both the username and password on the Barracuda Web Filter screen.  Please write down hardeesweb down on paper.  Enter this for both the username and password.", vbInformation, "Barracuda Screen")

        Open "C:\PCA.BAT" For Output As #5    ' Open file for output.
        Print #5, "rasdial bneras"
        Print #5, "EXIT"
        Close 5
        W = Shell("C:\pca.bat", vbMinimizedFocus)
        For G = 1 To 10000000
            DoEvents
        Next G
        Y = Shell("C:\Program Files\Internet Explorer\iexplore.exe", vbMaximizedFocus)

        m = MsgBox("Please click on OK to be disconnected the phone line.", vbInformation, "Disconnect")
        Open "C:\PCA.BAT" For Output As #5    ' Open file for output.
        Print #5, "rasdial bneras /disconnect"
        Print #5, "EXIT"
        Close 5
        W = Shell("C:\pca.bat", vbMinimizedFocus)
End If
End
End Sub

Private Sub LoadPrintO_Click()
Y = Shell("C:\windows\printo.exe", vbNormalFocus)
End
End Sub

Private Sub LoadUpdateLinks_Click()
    Open "C:\ul.bat" For Output As 1
    Print #1, "cd \iris\setup"
    Print #1, "updatelinks.vbs"
    Close 1
Y = Shell("C:\ul.bat", vbNormalFocus)
End
End Sub

Private Sub LoadXcelleNetMail_Click()
LineBuster.Show
End Sub

Private Sub LoadYeahWrite_Click()
Y = Shell("C:\yw\yw.exe", vbNormalFocus)
End
End Sub

Private Sub PeriodEndInventory_Click()
DTTimes.Show
End Sub

Private Sub PrintAudit_Click()
exe = "C:\Program Files\Microsoft Office\OFFICE11\wordview.exe" + " c:\bneapps\audit.txt"
 X = Shell(exe, 1)
End Sub

Private Sub PrintSystem_Click()
exe = "C:\Program Files\Microsoft Office\OFFICE11\wordview.exe" + " c:\bneapps\system.txt"
 X = Shell(exe, 1)

End Sub

Private Sub QTimer_Click()
ChDir ("C:\Hyper\Express Window")
msg = "Click Yes to indicate you are using the payment windows.  Open the payment windows." + Chr(10) + Chr(10) + "Click No to indicate you are not using the payment windows.  Close the payment window." ' Define message.
Style = vbYesNoCancel + vbQuestion + vbDefaultButton1 ' Define buttons.
Title = "Do you want to configure QTimer to use the payment window?"  ' Define title.
response = MsgBox(msg, Style, Title)
If response = vbYes Then    ' User chose Yes.
        exe = "C:\Hyper\Express Window\OPEN Payment Window.BAT"
        X = Shell(exe, 1)
End If
If response = vbNo Then    ' User chose Yes.
        exe = "C:\Hyper\Express Window\CLOSE Payment Window.BAT"
        X = Shell(exe, 1)
End If
End Sub

Private Sub ScheduleCalculator_Click()
    Open "C:\ls.bat" For Output As 1
    Print #1, "cd \labor"
    Print #1, "schcalc.exe"
    Close 1
Y = Shell("C:\ls.bat", vbNormalFocus)
End
End Sub

Private Sub ScheduleProjections_Click()
ChDir ("C:\labor")
Y = Shell("C:\labor\slsproj.exe", vbNormalFocus)
End
End Sub

Private Sub SchvAct_Click()
ChDir ("C:\labor")
Y = Shell("C:\labor\schvact.exe", vbNormalFocus)
End
End Sub

Private Sub SchvActHistory_Click()
'Check for XcelleNet
If Dir("c:\NODESYS\NEVER.XNET") <> "" Then
        exe = "\NODESYS\NSBSCRBR.EXE FCH00048 30 SC-1821" + NODE + " SC"
        X = Shell(exe, 1)
        Exit Sub
End If
    Open "C:\NODESYS\UNITID.DAT" For Input As 1
        NODE = Input(4, #1)
    Close 1
    If NODE = "1647" Or NODE = "1648" Or NODE = "1649" Or NODE = "1650" Or NODE = "1350" Or NODE = "3096" Or NODE = "2997" Or NODE = "2998" Then
        exe = "\NODESYS\NSBSCRBR.EXE FCH00048 30 SC" + NODE + " SC"
        X = Shell(exe, 1)
    Else
        exe = "\NODESYS\NSBSCRBR.EXE FCH00048 30 SC-" + NODE + " SC"
        X = Shell(exe, 1)
    End If
End Sub



