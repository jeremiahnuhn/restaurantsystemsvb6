VERSION 5.00
Begin VB.Form CheckVersionForm 
   Caption         =   "Check Version"
   ClientHeight    =   4875
   ClientLeft      =   3900
   ClientTop       =   1680
   ClientWidth     =   4140
   LinkTopic       =   "Form2"
   ScaleHeight     =   4875
   ScaleWidth      =   4140
   Begin VB.CommandButton Command3 
      Caption         =   "6"
      Height          =   375
      Left            =   240
      TabIndex        =   16
      Top             =   4320
      Width           =   615
   End
   Begin VB.CommandButton Command2 
      Caption         =   "5"
      Height          =   375
      Left            =   240
      TabIndex        =   15
      Top             =   3840
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1080
      TabIndex        =   0
      Top             =   3840
      Width           =   2415
   End
   Begin VB.Label T6Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   14
      Top             =   3120
      Width           =   2895
   End
   Begin VB.Label T5Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   13
      Top             =   2640
      Width           =   2895
   End
   Begin VB.Label T4Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   12
      Top             =   2160
      Width           =   2895
   End
   Begin VB.Label T3Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   11
      Top             =   1680
      Width           =   2895
   End
   Begin VB.Label T2Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   10
      Top             =   1200
      Width           =   2895
   End
   Begin VB.Label Label12 
      Caption         =   "Terminal 6"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   3120
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "Terminal 5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   2640
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "Terminal 4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Top             =   2160
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Terminal 3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   6
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "Terminal 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label T1Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   4
      Top             =   720
      Width           =   2895
   End
   Begin VB.Label Label2 
      Caption         =   "Terminal 1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Server"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label ServerVersionLabel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   1
      Top             =   240
      Width           =   2895
   End
End
Attribute VB_Name = "CheckVersionForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
On Error GoTo ErrorHandler
Screen.MousePointer = 11
Label10.Visible = True
'Terminal 5
    T5Ver = "Unavailable"
    T5Ver = FileDateTime("\\bne_reg5\c\iris\bin\pos.exe")
    Select Case Mid$(T5Ver, 1, 10)
        Case "8/15/2006 "
            T5Label.Caption = "377"
        Case "2/16/2009 "
            T5Label.Caption = "378.2 151"
        Case Else
            T5Label.Caption = Mid$(T5Ver, 1, 11)
        End Select
Screen.MousePointer = 0
Exit Sub

ErrorHandler:

If Err.Number = 53 Then
    Resume Next
End If

End Sub

Private Sub Command3_Click()
On Error GoTo ErrorHandler
Screen.MousePointer = 11
Label12.Visible = True
'Terminal 6
    T6Ver = "Unavailable"
    T6Ver = FileDateTime("\\bne_reg6\c\iris\bin\pos.exe")
    Select Case Mid$(T6Ver, 1, 10)
        Case "8/15/2006 "
            T6Label.Caption = "377"
        Case "2/16/2009 "
            T6Label.Caption = "378.2 151"
        Case Else
            T6Label.Caption = Mid$(T6Ver, 1, 11)
        End Select
Screen.MousePointer = 0
Exit Sub

ErrorHandler:

If Err.Number = 53 Then
    Resume Next
End If

End Sub

Private Sub Form_Load()
On Error GoTo ErrorHandler
For x = 1 To 500000
    DoEvents
Next x
'SERVER
    ServerVer = "Unavailable"
    ServerVer = FileDateTime("c:\iris\bin\pos.exe")
    Select Case Mid$(ServerVer, 1, 10)
        Case "8/15/2006 "
            ServerVersionLabel.Caption = "377"
        Case "2/16/2009 "
            ServerVersionLabel.Caption = "378.2 151"
        Case Else
            ServerVersionLabel.Caption = Mid$(ServerVer, 1, 11)
        End Select
'Terminal 1
    T1Ver = "Unavailable"
    T1Ver = FileDateTime("\\bne_reg1\c\iris\bin\pos.exe")
    Select Case Mid$(T1Ver, 1, 10)
        Case "8/15/2006 "
            T1Label.Caption = "377"
        Case "2/16/2009 "
            T1Label.Caption = "378.2 151"
        Case Else
            T1Label.Caption = Mid$(T1Ver, 1, 11)
        End Select

'Terminal 2
    T2Ver = "Unavailable"
    T2Ver = FileDateTime("\\bne_reg2\c\iris\bin\pos.exe")
    Select Case Mid$(T2Ver, 1, 10)
        Case "8/15/2006 "
            T2Label.Caption = "377"
        Case "2/16/2009 "
            T2Label.Caption = "378.2 151"
        Case Else
            T2Label.Caption = Mid$(T2Ver, 1, 11)
        End Select

'Terminal 3
    T3Ver = "Unavailable"
    T3Ver = FileDateTime("\\bne_reg3\c\iris\bin\pos.exe")
    Select Case Mid$(T3Ver, 1, 10)
        Case "8/15/2006 "
            T3Label.Caption = "377"
        Case "2/16/2009 "
            T3Label.Caption = "378.2 151"
        Case Else
            T3Label.Caption = Mid$(T3Ver, 1, 11)
        End Select

'Terminal 4
    T4Ver = "Unavailable"
    T4Ver = FileDateTime("\\bne_reg4\c\iris\bin\pos.exe")
    Select Case Mid$(T4Ver, 1, 10)
        Case "8/15/2006 "
            T4Label.Caption = "377"
        Case "2/16/2009 "
            T4Label.Caption = "378.2 151"
        Case Else
            T4Label.Caption = Mid$(T4Ver, 1, 11)
        End Select
        
Screen.MousePointer = 0
Exit Sub

ErrorHandler:

If Err.Number = 53 Then
    Resume Next
End If

End Sub

