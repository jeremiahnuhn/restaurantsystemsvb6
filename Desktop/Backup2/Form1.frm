VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form Form1 
   Caption         =   "BNE Desktop"
   ClientHeight    =   10605
   ClientLeft      =   180
   ClientTop       =   465
   ClientWidth     =   13440
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   -1  'True
      Strikethrough   =   -1  'True
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   10605
   ScaleWidth      =   13440
   Begin VB.CommandButton Command2 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      Picture         =   "Form1.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   32
      ToolTipText     =   "Support (FE Utilities)"
      Top             =   9600
      Width           =   735
   End
   Begin RichTextLib.RichTextBox LCHelp 
      Height          =   2175
      Left            =   120
      TabIndex        =   27
      Top             =   7320
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   3836
      _Version        =   393217
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      MousePointer    =   1
      TextRTF         =   $"Form1.frx":0442
   End
   Begin VB.CommandButton Exit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11040
      Picture         =   "Form1.frx":04CD
      TabIndex        =   6
      Top             =   9600
      Width           =   2295
   End
   Begin VB.CommandButton LoadPrintO 
      Caption         =   "&Print Orientation Guides"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   8520
      Picture         =   "Form1.frx":2589
      TabIndex        =   4
      ToolTipText     =   " Print new employee Orientation Forms. "
      Top             =   9600
      Width           =   2295
   End
   Begin VB.CommandButton LoadLiveConnect 
      Caption         =   "Live&Connect"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6000
      Picture         =   "Form1.frx":4645
      TabIndex        =   3
      ToolTipText     =   " Update Outlook (Send and receive mail) and Update Live Reporter. "
      Top             =   9600
      Width           =   2295
   End
   Begin VB.CommandButton LoadLiveReporter 
      Caption         =   "Live&Reporter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3480
      Picture         =   "Form1.frx":6701
      TabIndex        =   2
      ToolTipText     =   " Print or view reports, paper forms, and manuals. "
      Top             =   9600
      Width           =   2295
   End
   Begin VB.CommandButton LoadOutlook 
      Caption         =   "&Outlook"
      DisabledPicture =   "Form1.frx":87BD
      DownPicture     =   "Form1.frx":8BFF
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   960
      Picture         =   "Form1.frx":9041
      TabIndex        =   1
      ToolTipText     =   " Electronic mail, electronic forms, calendar, task list, update with Live Connect. "
      Top             =   9600
      Width           =   2295
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   12515
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   1147
      BackColor       =   13160660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Messages"
      TabPicture(0)   =   "Form1.frx":B0FD
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "SystemMessage"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "AUDITMESSAGE"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "PrintSystem"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "PrintAudit"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Programs"
      TabPicture(1)   =   "Form1.frx":B119
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "LoadXcelleNetMail"
      Tab(1).Control(1)=   "PeriodEndInventory"
      Tab(1).Control(2)=   "LoadYeahWrite"
      Tab(1).Control(3)=   "MonthView1"
      Tab(1).Control(4)=   "LoadCalculator"
      Tab(1).Control(5)=   "LoadCameraSystem"
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "Labor Scheduling"
      TabPicture(2)   =   "Form1.frx":B135
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "ESchvAct"
      Tab(2).Control(1)=   "SchvActHistory"
      Tab(2).Control(2)=   "SchvAct"
      Tab(2).Control(3)=   "ScheduleCalculator"
      Tab(2).Control(4)=   "ScheduleProjections"
      Tab(2).ControlCount=   5
      TabCaption(3)   =   "Computer Based Training"
      TabPicture(3)   =   "Form1.frx":B151
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "LoadLiveConnectCBT"
      Tab(3).Control(1)=   "LoadLaborProCBT"
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Support"
      TabPicture(4)   =   "Form1.frx":B16D
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Command3"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "CheckVersionButton"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "CreditCard"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "Command1"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "LoadPCAnywhere"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "LoadUpdateLinks"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).Control(6)=   "LoadAutopol"
      Tab(4).Control(6).Enabled=   0   'False
      Tab(4).Control(7)=   "Label4"
      Tab(4).Control(7).Enabled=   0   'False
      Tab(4).ControlCount=   8
      Begin VB.CommandButton Command3 
         BackColor       =   &H000000FF&
         Caption         =   "Emergency Priority Equipment Guidelines (Facilities Management)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   -71640
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   1800
         Width           =   6375
      End
      Begin VB.CommandButton CheckVersionButton 
         Caption         =   "Check Version"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -74760
         TabIndex        =   33
         Top             =   5640
         Width           =   4215
      End
      Begin VB.CommandButton CreditCard 
         Caption         =   "Change Credit Card Authorization Method"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -66120
         TabIndex        =   31
         Top             =   5640
         Width           =   4215
      End
      Begin VB.CommandButton Command1 
         Caption         =   "FE Utilities"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -70440
         TabIndex        =   30
         Top             =   5640
         Width           =   4215
      End
      Begin VB.CommandButton LoadCameraSystem 
         Caption         =   "Camera System"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   -74640
         TabIndex        =   29
         Top             =   3840
         Visible         =   0   'False
         Width           =   4935
      End
      Begin VB.CommandButton PrintAudit 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6600
         TabIndex        =   26
         Top             =   6600
         Width           =   6495
      End
      Begin VB.CommandButton PrintSystem 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   25
         Top             =   6600
         Width           =   6375
      End
      Begin VB.CommandButton LoadCalculator 
         Caption         =   "Calculator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   -67920
         TabIndex        =   24
         Top             =   5040
         Width           =   3735
      End
      Begin MSComCtl2.MonthView MonthView1 
         Height          =   3420
         Left            =   -67920
         TabIndex        =   23
         Top             =   1320
         Width           =   3780
         _ExtentX        =   6668
         _ExtentY        =   6033
         _Version        =   393216
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowToday       =   0   'False
         StartOfWeek     =   16187393
         CurrentDate     =   39057
      End
      Begin RichTextLib.RichTextBox AUDITMESSAGE 
         Height          =   5415
         Left            =   6600
         TabIndex        =   20
         Top             =   1080
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   9551
         _Version        =   393217
         Enabled         =   -1  'True
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         MousePointer    =   1
         TextRTF         =   $"Form1.frx":B189
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin RichTextLib.RichTextBox SystemMessage 
         Height          =   5415
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   9551
         _Version        =   393217
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         MousePointer    =   1
         TextRTF         =   $"Form1.frx":B209
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton LoadLaborProCBT 
         Caption         =   "LaborPro"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Left            =   -72120
         TabIndex        =   18
         Top             =   3720
         Width           =   6735
      End
      Begin VB.CommandButton LoadLiveConnectCBT 
         Caption         =   "Outlook / LiveConnect"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   -72120
         TabIndex        =   17
         Top             =   1560
         Width           =   6735
      End
      Begin VB.CommandButton LoadPCAnywhere 
         Caption         =   "Load PC Anywhere"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -70440
         TabIndex        =   16
         Top             =   4560
         Width           =   4215
      End
      Begin VB.CommandButton LoadUpdateLinks 
         Caption         =   "Update Links for IRIS Databases"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -66120
         TabIndex        =   15
         Top             =   4560
         Width           =   4215
      End
      Begin VB.CommandButton LoadAutopol 
         Caption         =   "Load Energy Management System (ST Only!)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -74760
         TabIndex        =   14
         Top             =   4560
         Width           =   4215
      End
      Begin VB.CommandButton LoadYeahWrite 
         Caption         =   "Yeah Write"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   -74640
         TabIndex        =   13
         Top             =   1200
         Width           =   4935
      End
      Begin VB.CommandButton PeriodEndInventory 
         Caption         =   "Period End Inventory"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   -74640
         TabIndex        =   12
         Top             =   2520
         Width           =   4935
      End
      Begin VB.CommandButton ESchvAct 
         Caption         =   "Employees Schedule vs Actual"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -71400
         TabIndex        =   11
         Top             =   5880
         Width           =   5175
      End
      Begin VB.CommandButton SchvActHistory 
         Caption         =   "Schedule vs Actual History"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -71400
         TabIndex        =   10
         Top             =   4680
         Width           =   5175
      End
      Begin VB.CommandButton SchvAct 
         Caption         =   "Schedule vs Actual"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -71400
         TabIndex        =   9
         Top             =   3480
         Width           =   5175
      End
      Begin VB.CommandButton ScheduleCalculator 
         Caption         =   "Schedule Calculator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -71400
         TabIndex        =   8
         Top             =   2280
         Width           =   5175
      End
      Begin VB.CommandButton ScheduleProjections 
         Caption         =   "Schedule Projections"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -71400
         TabIndex        =   7
         Top             =   1080
         Width           =   5175
      End
      Begin VB.CommandButton LoadXcelleNetMail 
         Caption         =   "Update the eRestaurant System"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   -74640
         TabIndex        =   5
         Top             =   5160
         Visible         =   0   'False
         Width           =   4935
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Version 1.12 April 2009"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   -69720
         TabIndex        =   28
         Top             =   6720
         Width           =   2775
      End
      Begin VB.Label Label3 
         Caption         =   "Quality Audits"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6720
         TabIndex        =   22
         Top             =   840
         Width           =   2415
      End
      Begin VB.Label Label2 
         Caption         =   "System Message"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   840
         Width           =   1935
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CheckVersionButton_Click()
    Screen.MousePointer = 11
    x = Shell("C:\nodesys\termuse.bat", vbMinimizedFocus)
    CheckVersionForm.Show
End Sub

Private Sub Command1_Click()
FrmPassword.Show
End Sub

Private Sub Command2_Click()
FrmPassword.Show
End Sub

Private Sub Command3_Click()
    exe = "C:\Program Files\Adobe\Acrobat 7.0\Reader\AcroRd32.exe" + " c:\Reports\Paper Forms\Emergency Priority Equipment.pdf"
    x = Shell(exe, vbMaximizedFocus)
 End
End Sub

Private Sub CreditCard_Click()
Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini" For Input As #1
Do While Not EOF(1)
    Line Input #1, Inline
    If Mid$(Inline, 1, 23) = "IP_AUTH_COMMUNICATION=Y" Then
        response = MsgBox("Credit Card Authorizations are setup for processing via the Internet (broadband)." + Chr(10) + Chr(10) + "Do you want to change Credit Card Authorization to use the phone line?", vbYesNo, "Credit Card Authorization")
        If response = vbYes Then
            Close 1
            FileCopy "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini", "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak"
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "IP_AUTH_COMMUN"
                        Print #3, "IP_AUTH_COMMUNICATION=N"
                    Case "IP_SETTLE_COMM"
                        Print #3, "IP_SETTLE_COMMUNICATION=N"
                    Case "USE_SSL_AUTH=Y"
                        Print #3, "USE_SSL_AUTH=N"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=N"
                    Case Else
                        If Mid$(Inline, 1, 9) = "AUTHLIMIT" Then
                            Print #3, "AUTHLIMIT=15"
                        Else
                            Print #3, Inline
                        End If
                End Select
            Loop
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Dial"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Dial"
            Close 2, 3, 4
            FileCopy "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini", "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak"
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "USE_SSL_AUTH=Y"
                        Print #3, "USE_SSL_AUTH=N"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=N"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
            response = MsgBox("Configuration files changed.  Please complete the following steps:" + Chr(10) + Chr(10) + "1) Click on System Status from the IRIS Shell." + Chr(10) + "2) Click on the Stop All Services button." + Chr(10) + "3) Click OK on the message boxes that appear." + Chr(10) + "4) Click on the Done button." + Chr(10) + Chr(10) + "Credit Card Authorization should now be using the phone line and take 15 seconds to authorize.", vbCritical, "Stop and Start Services")
            End
        Else
            Close 1
            End
        End If
    End If
    If Mid$(Inline, 1, 23) = "IP_AUTH_COMMUNICATION=N" Then
        response = MsgBox("Credit Card Authorizations are setup for processing via the phone line." + Chr(10) + Chr(10) + "Do you want to change Credit Card Authorization to use the Internet (broadband)?", vbYesNo, "Credit Card Authorization")
        If response = vbYes Then
            Close 1
            FileCopy "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini", "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak"
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "IP_AUTH_COMMUN"
                        Print #3, "IP_AUTH_COMMUNICATION=Y"
                    Case "IP_SETTLE_COMM"
                        Print #3, "IP_SETTLE_COMMUNICATION=Y"
                    Case "USE_SSL_AUTH=N"
                        Print #3, "USE_SSL_AUTH=Y"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=Y"
                    Case Else
                        If Mid$(Inline, 1, 9) = "AUTHLIMIT" Then
                            Print #3, "AUTHLIMIT=0"
                        Else
                            Print #3, Inline
                        End If
                End Select
            Loop
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "IP"
            Close 4
            Open "c:\bneapps\Authorization.Log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
                    Input #1, unumber, uname, email, junk
                    Input #1, unumber, uname, email, junk
                Close #1
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "IP"

            Close 2, 3, 4
            FileCopy "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini", "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak"
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\svs\tpe_svs.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "USE_SSL_AUTH=N"
                        Print #3, "USE_SSL_AUTH=Y"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=Y"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
            response = MsgBox("Configuration files changed.  Please complete the following steps:" + Chr(10) + Chr(10) + "1) Click on System Status from the IRIS Shell." + Chr(10) + "2) Click on the Stop All Services button." + Chr(10) + "3) Click OK on the message boxes that appear." + Chr(10) + "4) Click on the Done button." + Chr(10) + Chr(10) + "Credit Card Authorization should now be using the Internet (broadband) and take 4 seconds to authorize.", vbCritical, "Stop and Start Services")
            End
        Else
            Close 1
            End
        End If
    End If
Loop
Close 1
End

End Sub

Private Sub ESchvAct_Click()
ChDir ("C:\LABOR")
Y = Shell("C:\labor\veritime.exe", vbNormalFocus)
End
End Sub

Private Sub Exit_Click()
    End
End Sub

Private Sub Form_Load()
On Error Resume Next

'Check for eRestaurant
If Dir("c:\IRIS\BIN\ERS.BAT") <> "" Then
    LoadXcelleNetMail.Visible = True
End If
'RDU No BNE Labor Scheduling Tools
If Dir("c:\nodesys\nolabor.txt") <> "" Then
    ScheduleProjections.Enabled = False
    ScheduleCalculator.Enabled = False
    SchvAct.Enabled = False
    SchvActHistory.Enabled = False
    ESchvAct.Enabled = False
End If

'Check for Camera System Software
If Dir("c:\program files\IVPlayback\ivplay8.exe") <> "" Then
    LoadCameraSystem.Visible = True
End If

MonthView1.Value = Now()

' MESSAGE BOXES -

' Terry Lewis Author
If Dir("C:\BNEAPPS\SYSTEM.TXT") <> "" Then
    SystemMessage.FileName = "C:\BNEAPPS\SYSTEM.TXT"
End If
' Angela Maxwell Author
If Dir("C:\BNEAPPS\AUDIT.TXT") <> "" Then
    AUDITMESSAGE.FileName = "C:\BNEAPPS\AUDIT.TXT"
End If
' Restaurant Systems Author
If Dir("C:\BNEAPPS\LCHELP.RTF") <> "" Then
    LCHelp.FileName = "C:\BNEAPPS\LCHELP.RTF"
    'LCHelp.SelRTF = True
End If
Dim unumber As String
Dim uname As String
If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
    Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
    Input #1, unumber, uname, email, junk
    Input #1, unumber, uname, email, junk
    Close #1
    Form1.Caption = "BNE Desktop:   " + uname + "  #" + unumber
End If

' INI SETUP


End Sub

Private Sub LoadAutopol_Click()
    On Error GoTo ERR3
    
    Open "C:\AUTOPOL\FACILITY.DBF" For Binary As 1
    WORKLIST = InputB(1388, #1)
    Close 1
    NEWLIST = Mid$(WORKLIST, 1, 1080) + "DNNNNNNN" + Mid$(WORKLIST, 1089, 299)
    Open "C:\AUTOPOL\FACILITY.DBF" For Output As 1
    Print #1, NEWLIST
    Close 1
    Y = Shell("C:\AUTOPOL\STARTAP.BAT", 1)
    End
    Exit Sub

ERR3:
    If Err = 53 Or Err = 76 Then
        typebox = 0 + 16
        VERSIONT = "Facility File Missing." + Chr(10) + "Please contact support at" + Chr(10) + "252-937-2800 ext 1294" + Chr(10) + "Johnny Thompson"
        response = MsgBox(VERSIONT, typebox, "Required File(s) Missing")
        End
        
    End If
    End
Resume Next

End Sub

Private Sub LoadCalculator_Click()
    Y = Shell("C:\Program Files\Moffsoft FreeCalc\MoffFreeCalc.exe", vbNormalFocus)
End
End Sub

Private Sub LoadCameraSystem_Click()
    ChDir ("c:\program files\IVPlayback")
    x = Shell("c:\program files\IVPlayback\ivplay8.exe", vbNormalFocus)
End Sub

Private Sub LoadLaborProCBT_Click()
    Y = Shell("c:\bneapps\tutorial\Labor Pro\bne-laborpro-tutorial.exe", vbNormalFocus)
End
End Sub

Private Sub LoadLiveConnect_Click()
    
On Error GoTo BypassApp
AppActivate "LiveConnect"
End
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
   Y = Shell("c:\iris\bin\killstartup.bat", 0)
   'pleasewait = MsgBox("It takes about 10 seconds for LiveConnect to start, please wait." + Chr(10) + Chr(10) + "To access approved Internet sites, choose the Texas Internet profile." + Chr(10) + Chr(10) + "To send and receive Outlook email, send Outlook forms, and update LiveReporter reports, choose the Update Outlook and LiveReporter - Texas profile.  This process occurs automatically each morning at 4:00 AM.  You should only use this profile to send Employment Services Forms, update your Employment Services Responses or update your Weekly Payroll Verification Report." + Chr(10) + Chr(10) + "If asked to complete a LiveConnect session by the Help Desk, please choose the Daily Communication - Texas profile." + Chr(10) + Chr(10) + "Do not close LiveConnect until you receive the message box labeled Profile Execution Complete.  When you receive this message box, click on OK and then close LiveConnect with the X in the top right corner of the LiveConnect window.", vbInformation, "LiveConnect")
    'pleasewait = MsgBox("It takes about 10 seconds for LiveConnect to start, please wait." + Chr(10) + Chr(10) + "To send and receive Outlook email, send Outlook forms, and update LiveReporter reports, choose the Hardees - Update Outlook and LiveReporter profile.  This process occurs automatically each day during the IRIS End of Day process." + Chr(10) + Chr(10) + "If asked to complete a LiveConnect session by the Help Desk, please choose the Hardees - Daily Communication." + Chr(10) + Chr(10) + "Do not close LiveConnect until you receive the message box labeled Profile Execution Complete.  When you receive this message box, click on OK and then close LiveConnect with the X in the top right corner of the LiveConnect window.", vbInformation, "LiveConnect")
   For z = 1 To 20000
    DoEvents
   Next z
    ChDir "c:\Program Files\Religent\LiveConnect 5.2 BNE"
    progname = "\Program Files\Religent\LiveConnect 5.2 BNE\startup.exe -sp -skin LiveConnect"
    ' -sp Profile Execution Complete
    x = Shell(progname, vbNormalFocus)
    Else
   Err.Clear
End If
    End
End Sub

Private Sub LoadLiveConnectCBT_Click()
    Y = Shell("c:\bneapps\tutorial\bne-tutorial.exe", vbNormalFocus)
    End
End Sub

Private Sub LoadLiveReporter_Click()
  ChDir ("C:\Program Files\Religent\LiveReporter Client")
  LOADPCAW = Shell("C:\Program Files\Religent\LiveReporter Client\LiveReporter Client.exe", vbMaximizedFocus)
  End
End Sub

Private Sub LoadOutlook_Click()
If Dir("c:\BNEAPPS\XNET.YES") <> "" Then
       LOADPCAW = Shell("c:\nodesys\ndesktop.exe", vbNormalFocus)
       End
Else
    If Dir("C:\Program Files\Microsoft Office\Office11\OUTLOOK.EXE") <> "" Then
        LOADPCAW = Shell("C:\Program Files\Microsoft Office\Office11\Outlook.exe /recycle", vbMaximizedFocus)
    Else
        LOADPCAW = Shell("c:\Program Files\Microsoft Office\Office10\outlook.exe  /recycle", vbMaximizedFocus)
    End If
    End
End If
End Sub

Private Sub LoadPCAnywhere_Click()
      ChDir ("C:\program files\symantec\pcanywhere")
      Y = Shell("c:\Program Files\Symantec\pcAnywhere\STOPHOST.EXE", vbNormalFocus)
      x = MsgBox("Thank You!", vbInformation, "Load PC Anywhere")
      For G = 1 To 10000000
        DoEvents
      Next G
        Open "C:\PCA.BAT" For Output As #5    ' Open file for output.
        QUOTE = """"
        Print #5, QUOTE;
        Print #5, "C:\Program Files\Symantec\pcAnywhere\awhost32.exe";
        Print #5, QUOTE;
        Print #5, " ";
        Print #5, QUOTE;
        Print #5, "C:\Documents and Settings\All Users\Application Data\Symantec\pcAnywhere\support.bhf";
        Print #5, QUOTE
        Print #5, "EXIT"
        Close 5
        W = Shell("C:\pca.bat", nofocus)
End
End Sub

Private Sub LoadPrintO_Click()
Y = Shell("C:\windows\printo.exe", vbNormalFocus)
End
End Sub

Private Sub LoadUpdateLinks_Click()
    Open "C:\ul.bat" For Output As 1
    Print #1, "cd \iris\setup"
    Print #1, "updatelinks.vbs"
    Close 1
Y = Shell("C:\ul.bat", vbNormalFocus)
End
End Sub

Private Sub LoadXcelleNetMail_Click()
SDMSG = MsgBox("This process will collect the missing information from IRIS and update the eRestaurant System.  The update process takes about one hour.  If the sales information is not updated after one hour, please contact the BNE Help Desk.", 64, "Update the eRestaurant System")
Y = Shell("c:\IRIS\BIN\ERS.BAT", vbNormalFocus)
End
End Sub

Private Sub LoadYeahWrite_Click()
Y = Shell("C:\yw\yw.exe", vbNormalFocus)
End
End Sub

Private Sub PeriodEndInventory_Click()
Y = Shell("C:\monthend\mecount.exe", vbNormalFocus)
End
End Sub

Private Sub PrintAudit_Click()
exe = "C:\Program Files\Microsoft Office\OFFICE11\wordview.exe" + " c:\bneapps\audit.txt"
 x = Shell(exe, 1)
End Sub

Private Sub PrintSystem_Click()
exe = "C:\Program Files\Microsoft Office\OFFICE11\wordview.exe" + " c:\bneapps\system.txt"
 x = Shell(exe, 1)

End Sub

Private Sub ScheduleCalculator_Click()
    Open "C:\ls.bat" For Output As 1
    Print #1, "cd \labor"
    Print #1, "schcalc.exe"
    Close 1
Y = Shell("C:\ls.bat", vbNormalFocus)
End
End Sub

Private Sub ScheduleProjections_Click()
ChDir ("C:\labor")
Y = Shell("C:\labor\slsproj.exe", vbNormalFocus)
End
End Sub

Private Sub SchvAct_Click()
ChDir ("C:\labor")
Y = Shell("C:\labor\schvact.exe", vbNormalFocus)
End
End Sub

Private Sub SchvActHistory_Click()
'Check for XcelleNet
If Dir("c:\NODESYS\NEVER.XNET") <> "" Then
        exe = "\NODESYS\NSBSCRBR.EXE FCH00048 30 SC-1821" + NODE + " SC"
        x = Shell(exe, 1)
        Exit Sub
End If
    Open "C:\NODESYS\UNITID.DAT" For Input As 1
        NODE = Input(4, #1)
    Close 1
    If NODE = "1647" Or NODE = "1648" Or NODE = "1649" Or NODE = "1650" Or NODE = "1350" Or NODE = "3096" Or NODE = "2997" Or NODE = "2998" Then
        exe = "\NODESYS\NSBSCRBR.EXE FCH00048 30 SC" + NODE + " SC"
        x = Shell(exe, 1)
    Else
        exe = "\NODESYS\NSBSCRBR.EXE FCH00048 30 SC-" + NODE + " SC"
        x = Shell(exe, 1)
    End If
End Sub

