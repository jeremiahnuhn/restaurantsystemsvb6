@Echo off
@TITLE **  COPY DATABASE FROM THE TEMPLATE FOLDER TO THE DATA FOLDER
REM - This is going to replace the 4xDbRepaire .exe 

copy "C:\IRIS\Data\Template\BackOffice.mdb"  "C:\IRIS\Data\BackOffice.mdb" /Y
copy "C:\IRIS\Data\Template\FlexPoll.Mdb" "C:\IRIS\Data\FlexPoll.Mdb" /Y
copy "C:\IRIS\Data\Template\Inventory.mdb" "C:\IRIS\Data\Inventory.mdb" /Y
copy "C:\IRIS\Data\Template\Payroll.mdb" "C:\IRIS\Data\Payroll.mdb" /Y
copy "C:\IRIS\Data\Template\POSTrans.mdb" "C:\IRIS\Data\POSTrans.mdb" /Y
copy "C:\IRIS\Data\Template\Report.Mdb" "C:\IRIS\Data\Report.Mdb" /Y
copy "C:\IRIS\Data\Template\Security.mdb" "C:\IRIS\Data\Security.mdb" /Y
copy "C:\IRIS\Data\Template\Shell.mdb" "C:\IRIS\Data\Shell.mdb" /Y
copy "C:\IRIS\Data\Template\WSR.Mdb" "C:\IRIS\Data\WSR.Mdb" /Y
copy "C:\IRIS\Data\Template\xception.mdb" "C:\IRIS\Data\xception.mdb" /Y
copy "C:\IRIS\Data\Template\forecast.mdb" "C:\IRIS\Data\forecast.mdb" /Y
copy "C:\IRIS\Data\Template\authdata.mdb" "C:\IRIS\Data\authdata.mdb" /Y
copy "C:\IRIS\Data\Template\DSR.mdb" "C:\IRIS\Data\DSR.mdb" /Y

call "c:\iris\setup\UpdateLinks.VBS"
pause
