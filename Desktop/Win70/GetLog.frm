VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form GetLog 
   Caption         =   "Get IRIS Log Files"
   ClientHeight    =   5550
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8625
   LinkTopic       =   "Form2"
   ScaleHeight     =   5550
   ScaleWidth      =   8625
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Get Log Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   5400
      TabIndex        =   0
      Top             =   2640
      Width           =   2535
   End
   Begin MSComCtl2.MonthView LogCal 
      Height          =   3420
      Left            =   1080
      TabIndex        =   1
      Top             =   1440
      Width           =   3750
      _ExtentX        =   6615
      _ExtentY        =   6033
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      StartOfWeek     =   76283905
      CurrentDate     =   43187
   End
   Begin VB.Label Label1 
      Caption         =   $"GetLog.frx":0000
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   8175
   End
End
Attribute VB_Name = "GetLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
        FrmPassword.Hide
        If Dir("c:\ftplogs.bat") <> "" Then
            Kill "c:\ftplogs.bat"
        End If
       
       GetDate1 = Format$(LogCal.Value + 1, "YYYY-MM-DD")
       GetDate2 = Format$(LogCal.Value + 2, "YYYY-MM-DD")
       Open "c:\ftplogs.bat" For Output As #1
            Print #1, "@echo off"
            Print #1, "REM Created " + Str(Now())
            Print #1, "c:"
            Print #1, "CD C:\Iris\Log\Backup"
            Print #1, "If not exist c:\support\transfer md c:\Support\Transfer"
            Print #1, "del c:\support\ftp.bat /q"
            Print #1, "del c:\support\ftp.txt /q"
            Print #1, "copy " + GetDate1 + "*.zip c:\Support\Transfer\%ComputerName%-" + GetDate1 + "*.zip"
            Print #1, "copy " + GetDate2 + "*.zip c:\Support\Transfer\%ComputerName%-" + GetDate2 + "*.zip"
            Print #1, "cd c:\Support"
            Print #1, Chr(34) + "c:\program files\winscp\winscp.com" + Chr(34) + " /ini=nul /script=c:\ftpLogs.txt"
            Print #1, ":End"
            Print #1, "exit"
      Close #1

      Y = Shell("c:\ftplogs.BAT", vbMinimizedNoFocus)
  
     End

End Sub

Private Sub Form_Load()
    LogCal.ShowToday = False

    LogCal.Value = Now()


End Sub

Private Sub LogCal_DateClick(ByVal DateClicked As Date)
    Command1.SetFocus
    
End Sub
