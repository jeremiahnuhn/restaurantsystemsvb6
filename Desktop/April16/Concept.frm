VERSION 5.00
Begin VB.Form Concept 
   Caption         =   "Drive-Thru Terminal Configuration"
   ClientHeight    =   5970
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8640
   LinkTopic       =   "Form2"
   ScaleHeight     =   5970
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command5 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   6
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Order Taker and Cashier"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   720
      TabIndex        =   5
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Order Taker (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   5400
      TabIndex        =   4
      Top             =   1800
      Width           =   2295
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Cashier (Standard)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   720
      TabIndex        =   3
      Top             =   1800
      Width           =   2295
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Close without making any changes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1440
      TabIndex        =   0
      Top             =   4680
      Width           =   5415
   End
   Begin VB.Label Label2 
      Caption         =   "  Cashier                                               Order Taker"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1080
      TabIndex        =   2
      Top             =   960
      Width           =   6615
   End
   Begin VB.Label Label1 
      Caption         =   "Terminal 3                                              Terminal 4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1080
      TabIndex        =   1
      Top             =   480
      Width           =   6495
   End
End
Attribute VB_Name = "Concept"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
'T 3 Normal
      X = MsgBox("This process will configure the Cashier Terminal to work as normal.", vbInformation, "Change Terminal 3")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 3 where RegID = 3 and PeriodID = 11"
        Print #1, "Update tblRegTimeConcepts set ConceptID = 9 where RegID = 3 and PeriodID = 12"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T3 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command3_Click()
'T 4 Normal
      X = MsgBox("This process will configure the Order Taker Terminal to work as normal.", vbInformation, "Change Terminal 4")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 2 where RegID = 4 and PeriodID = 11"
        Print #1, "Update tblRegTimeConcepts set ConceptID = 8 where RegID = 4 and PeriodID = 12"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T4 Normal"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub

End Sub

Private Sub Command4_Click()
'T 3 Dual
      X = MsgBox("This process will configure the Cashier Terminal to work as Order Taker and Cashier.", vbInformation, "Change Terminal 3")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 5 where RegID = 3"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T3 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
      Exit Sub
End Sub

Private Sub Command5_Click()
'T 4 Dual
      X = MsgBox("This process will configure the Order Taker Terminal to work as Order Taker and Cashier.", vbInformation, "Change Terminal 4")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblRegTimeConcepts set ConceptID = 5 where RegID = 4"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
      If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
           Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
           Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\swipe.sql"
           Print #1, "cls"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\swipe.sql"
            Print #1, "C:\windows\system32\wscript.exe c:\iris\update\initiateregisterupdate.vbs"
        End If
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Concept T4 Dual"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1

      End
    Exit Sub
End Sub
