VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form DTTimes 
   Caption         =   "IRIS Drive-Thru Order Times (Used to compare to the QTimer Order Times)"
   ClientHeight    =   7140
   ClientLeft      =   2370
   ClientTop       =   2145
   ClientWidth     =   8235
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   8235
   Begin VB.CommandButton Command4 
      Caption         =   "Select Another Date"
      Height          =   375
      Left            =   6120
      TabIndex        =   10
      Top             =   6240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Copy to Clipboard"
      Height          =   375
      Left            =   4320
      TabIndex        =   2
      Top             =   6720
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Print"
      Height          =   375
      Left            =   4320
      TabIndex        =   1
      Top             =   6240
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4920
      Left            =   360
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   480
      Visible         =   0   'False
      Width           =   7455
   End
   Begin VB.CommandButton ViewTimes 
      Caption         =   "View Data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2040
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   4560
      Width           =   3735
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      Height          =   375
      Left            =   6120
      TabIndex        =   0
      Top             =   6720
      Width           =   1815
   End
   Begin MSComCtl2.MonthView DT 
      Height          =   3420
      Left            =   2040
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   960
      Width           =   3780
      _ExtentX        =   6668
      _ExtentY        =   6033
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShowToday       =   0   'False
      StartOfWeek     =   16252929
      CurrentDate     =   39057
      MaxDate         =   40760
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Caption         =   "Average Adjusted Time"
      Height          =   255
      Left            =   2280
      TabIndex        =   13
      Top             =   6240
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label Label6 
      Height          =   255
      Left            =   2400
      TabIndex        =   12
      Top             =   6480
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label Label5 
      Height          =   255
      Left            =   2400
      TabIndex        =   11
      Top             =   6720
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Label Label4 
      Caption         =   $"DTTimes.frx":0000
      Height          =   495
      Left            =   480
      TabIndex        =   9
      Top             =   5520
      Visible         =   0   'False
      Width           =   7095
   End
   Begin VB.Label Label3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   6120
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   6720
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label1 
      Caption         =   "Order Number             Start Time                    Close Time              Actual Time         Adjusted Time"
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   7215
   End
End
Attribute VB_Name = "DTTimes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
Dim today As Date
Dim PageNum As Integer
Dim V As Integer
Dim W As Integer
Dim X As Integer
Dim Y As Integer
Dim Z As Integer
W = 1
Z = 5
V = 0
PageNum = 1
Y = 2200
   Printer.FontName = "COURIER NEW"
   Printer.FontBold = True
   Printer.FontSize = 14
   today = Format(Now, "ddddd")
   Printer.CurrentX = 3400
   Printer.CurrentY = 500
   Printer.Print "IRIS Drive-Thru Order Times"
   Printer.FontSize = 10
   Printer.Print "                                   " & Label2.Caption
   Printer.FontSize = 12
   Printer.CurrentX = 2400
   Printer.CurrentY = 1400
   Printer.FontSize = 10: Printer.FontBold = False
   Printer.CurrentX = 1215
   Printer.CurrentY = 1800
   Printer.FontUnderline = True
   Printer.Print " Order #   Start      Close    Actual  Adjusted "
   Printer.FontUnderline = False
   Printer.Print " "
   Z = 0
For I = 0 To List1.ListCount - 1
   Z = Z + 1
   Printer.FontSize = 10
   Printer.FontBold = False
   Printer.Print "           " + List1.List(I)
    If Z = 55 Then
       Printer.CurrentX = 10000
       Printer.CurrentY = 14500
       Printer.Print "Page " & PageNum
       PageNum = PageNum + 1
       Printer.NewPage
   Printer.FontSize = 10: Printer.FontBold = False
   Printer.CurrentX = 1215
   Printer.CurrentY = 1800
   Printer.FontUnderline = True
   Printer.Print " Order #   Start      Close    Actual  Adjusted "
   Printer.FontUnderline = False
   Printer.Print " "
   Z = 0
   Y = 2200
    End If
Next I
Printer.CurrentX = 10000
Printer.CurrentY = 14500
Printer.Print "Page " & PageNum
Printer.EndDoc
  m = MsgBox("Printing complete.", vbInformation, "Print Status")

End Sub

Private Sub Command3_Click()
  Clipboard.Clear
  Copytoclip = ""
Copytoclip = Label2.Caption & Chr$(13) & Chr$(10)
Copytoclip = Copytoclip & "Order #  Start      End        Actual  Adjusted" & Chr$(13) & Chr$(10)
For I = 0 To List1.ListCount - 1
Copytoclip = Copytoclip & List1.List(I) & Chr$(13) & Chr$(10)
Next I
Clipboard.SetText Copytoclip
m = MsgBox("Copy to Clipboard complete.", vbInformation, "Clipboard Status")
End Sub

Private Sub Command4_Click()
    List1.Clear
    List1.Visible = False
    Label1.Visible = False
     DT.Visible = True
     ViewTimes.Visible = True
    Label4.Visible = False
    Label5.Visible = False
    Label6.Visible = False
    Label7.Visible = False
    Label3.Caption = " "
    Label2.Visible = False
    Command2.Visible = False
    Command3.Visible = False
    Command4.Visible = False
    DTTimes.Refresh

End Sub

Private Sub DT_DateClick(ByVal DateClicked As Date)
    ViewTimes.Caption = "View Times for " + Str(DT.Value)
    DTTimes.Refresh
    ViewTimes.SetFocus
End Sub

Private Sub Form_Load()
    DT.MaxDate = Date + 1
    DT.MinDate = Date - 89
    DT.Value = Date
    ViewTimes.Caption = "Click on the date you want to view the IRIS Drive-Thru Order Times"
End Sub

Private Sub ViewTimes_Click()
        If Dir("c:\support\DTTIME.TXT") <> "" Then
            Kill "c:\support\DTTIME.TXT"
        End If
     
       Open "c:\support\DTTIME.BAT" For Output As #1
        Print #1, "REM Created " + Str(Now())
        If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
            Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\support\DTTime.sql -o " + Chr(34) + "c:\support\DTTIME.TXT" + Chr(34)
            Print #1, "Exit"
        Else
            Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\support\DTTime.sql -o " + Chr(34) + "c:\support\DTTIME.TXT" + Chr(34)
            Print #1, "Exit"
        End If
      Close #1
       Open "c:\support\DTTIME.SQL" For Output As #1
        Print #1, "SELECT tblOrder.OrderNum,tblOrder.FirstTime,tblOrder.CloseTime, (DateDiff(s,tblOrder.FirstTime,tblOrder.CloseTime)) AS 'Total Time'"
        Print #1, "FROM tblOrder Where  BusinessDate = '" + Str(DT.Value) + "' AND (tblOrder.State=1 Or tblOrder.State=128) AND (tblOrder.Destination = 4)"
        Print #1, "Order by FirstTime"
      Close #1

      Y = Shell("c:\support\DTTIME.BAT", vbMinimizedNoFocus)
  
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  -  DT Times " + Str(DT.Value)
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
     DT.Visible = False
     ViewTimes.Visible = False
     
     DoEvents
     List1.Visible = True
     List1.AddItem " "
     List1.AddItem " "
     List1.AddItem " "
     List1.AddItem " "
     List1.AddItem "         Collecting Order Times from IRIS"
     List1.AddItem " "
     List1.AddItem " "
     List1.AddItem "                  Please wait ..."
     DT.Refresh
     For X = 1 To 500000
        DoEvents
     Next X
     Label1.Visible = True
     List1.Clear

     Num8 = 0
        BrkCount = 0
        BrkTotal = 0
        RegCount = 0
        RegTotal = 0
    If Dir("c:\support\DTTIME.TXT") <> "" Then
        Open "c:\support\DTTIME.TXT" For Input As #1
        Line Input #1, LineData
        Line Input #1, LineData
        
        Do While Not EOF(1)
        Num8Str = " "
        Line Input #1, LineData
       ' If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
'2005

        If Mid$(LineData, 17, 1) = "-" Then
            OrderNum = Mid$(LineData, 6, 6)
            OrderStart = Mid$(LineData, 24, 8)
            OrderClose = Mid$(LineData, 48, 8)
            OrderTime = Val(Mid$(LineData, 68, 4))
            OrderDate = Mid$(LineData, 13, 10)
            StartHour = Val(Mid$(LineData, 24, 2))
            If StartHour > 4 And StartHour <= 11 Then
                OrderAdjTime = OrderTime + 70
                BrkCount = BrkCount + 1
                BrkTotal = BrkTotal + OrderAdjTime
            Else
                OrderAdjTime = OrderTime + 115
                RegCount = RegCount + 1
                RegTotal = RegTotal + OrderAdjTime
            End If
            OrderTimeStr = Format(Int(OrderTime / 60) Mod 60, "0#") & ":" & Format(OrderTime Mod 60, "0#")
            OrderAdjTimeStr = Format(Int(OrderAdjTime / 60) Mod 60, "0#") & ":" & Format(OrderAdjTime Mod 60, "0#")
            If Int(OrderAdjTime / 60) Mod 60 > 7 Then
                Num8 = Num8 + 1
                Num8Str = " *"
            End If
            OrderDetail = OrderNum + "   " + OrderStart + "   " + OrderClose + "   " + OrderTimeStr + "   " + OrderAdjTimeStr + Num8Str
            List1.AddItem (OrderDetail)
        Else
'2000
            If Mid$(LineData, 18, 1) = "-" Then
            OrderNum = Mid$(LineData, 7, 6)
            OrderStart = Mid$(LineData, 25, 8)
            OrderClose = Mid$(LineData, 49, 8)
            OrderTime = Val(Mid$(LineData, 69, 4))
            OrderDate = Mid$(LineData, 14, 10)
            StartHour = Val(Mid$(LineData, 25, 2))
            If StartHour > 4 And StartHour <= 11 Then
                OrderAdjTime = OrderTime + 70
                BrkCount = BrkCount + 1
                BrkTotal = BrkTotal + OrderAdjTime
            Else
                OrderAdjTime = OrderTime + 115
                RegCount = RegCount + 1
                RegTotal = RegTotal + OrderAdjTime
            End If
            OrderTimeStr = Format(Int(OrderTime / 60) Mod 60, "0#") & ":" & Format(OrderTime Mod 60, "0#")
            OrderAdjTimeStr = Format(Int(OrderAdjTime / 60) Mod 60, "0#") & ":" & Format(OrderAdjTime Mod 60, "0#")
            If Int(OrderAdjTime / 60) Mod 60 > 7 Then
                Num8 = Num8 + 1
                Num8Str = " *"
            End If
            OrderDetail = OrderNum + "   " + OrderStart + "   " + OrderClose + "   " + OrderTimeStr + "   " + OrderAdjTimeStr + Num8Str
            List1.AddItem (OrderDetail)
        End If
        'End If
        End If
        Loop
        Label2.Caption = "Business Date = " + Mid$(OrderDate, 6, 2) + "/" + Mid$(OrderDate, 9, 2) + "/" + Mid$(OrderDate, 1, 4)
        Label2.Visible = True
        Label3.Caption = ">= 8 Minutes " + Str(Num8)
        
        Label5.Caption = "  Lunch/Dinner  " + Format(Int((RegTotal / RegCount) / 60) Mod 60, "0#") & ":" & Format((RegTotal / RegCount) Mod 60, "0#")
        Label6.Caption = "  Breakfast         " + Format(Int((BrkTotal / BrkCount) / 60) Mod 60, "0#") & ":" & Format((BrkTotal / BrkCount) Mod 60, "0#")
        
        Command2.Visible = True
        Command3.Visible = True
        Label7.Visible = True
        Label5.Visible = True
        Label6.Visible = True
        Label4.Visible = True
        Command4.Visible = True
        Close #1
    Else
     X = MsgBox("DTTIME.TXT file not found." + Chr(10) + "Please contact the BNE Help Desk for assistance.", vbInformation, "File Missing.")
     End
    End If

End Sub
