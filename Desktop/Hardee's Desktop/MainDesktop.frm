VERSION 5.00
Begin VB.Form MainDesktop 
   Caption         =   "Hardee's Desktop"
   ClientHeight    =   8250
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10155
   Icon            =   "MainDesktop.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8250
   ScaleWidth      =   10155
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton LiveConnect 
      Caption         =   "Live Connect"
      Height          =   615
      Left            =   600
      TabIndex        =   6
      Top             =   5640
      Width           =   2655
   End
   Begin VB.CommandButton LiveReporter 
      Caption         =   "Live Reporter"
      Height          =   615
      Left            =   600
      TabIndex        =   5
      Top             =   4920
      Width           =   2655
   End
   Begin VB.Frame Frame1 
      Caption         =   " Communication Tools "
      Height          =   2655
      Left            =   240
      TabIndex        =   3
      Top             =   3840
      Width           =   3375
      Begin VB.CommandButton Outlook 
         Caption         =   "Outlook"
         Height          =   615
         Left            =   360
         TabIndex        =   4
         Top             =   360
         Width           =   2655
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1680
      TabIndex        =   0
      Top             =   7080
      Width           =   6975
   End
   Begin VB.Label Version 
      Caption         =   "Version 1.0"
      Height          =   255
      Left            =   9240
      TabIndex        =   7
      Top             =   7920
      Width           =   855
   End
   Begin VB.Label DateTime 
      Caption         =   "now()"
      Height          =   255
      Left            =   8280
      TabIndex        =   2
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label SystemMessage 
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "System Message ..."
      Height          =   3015
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   9855
   End
End
Attribute VB_Name = "MainDesktop"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub LiveConnect_Click()
    X = Shell("C:\bneapps\lc.bat")
    End
End Sub

Private Sub LiveReporter_Click()
    X = Shell("C:\Program Files\Religent\LiveReporter Client\LiveReporter Client.exe", 1)
    End
End Sub

Private Sub Outlook_Click()
    X = Shell("C:\PROGRAM FILES\MICROSOFT OFFICE 10\OFFICE10\OUTLOOK.EXE", 1)
    End
End Sub

Private Sub Form_Load()
 ' Create Batch File to Start LiveConnect
 ' Can't pass paramaters
    Open "C:\BNEAPPS\LC.BAT" For Output As 1
    Print #1, "CD C:\Program Files\Religent\LiveConnect 5.2"
    'Print #1, "startup.exe -skin liveconnect"
    Print #1, "startup.exe -skin compactclient"
    Close 1

    DateTime.Caption = Now()
End Sub
