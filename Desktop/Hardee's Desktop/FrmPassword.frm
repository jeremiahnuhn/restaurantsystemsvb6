VERSION 5.00
Begin VB.Form FrmPassword 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FE Utilites"
   ClientHeight    =   4710
   ClientLeft      =   4170
   ClientTop       =   3375
   ClientWidth     =   5580
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   5580
   Begin VB.Data Data1 
      Caption         =   "POSLIVE"
      Connect         =   "Access"
      DatabaseName    =   "C:\IRIS\DATA\POSLIVE.MDB"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   1800
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "tblBusinessDate"
      Top             =   4320
      Visible         =   0   'False
      Width           =   2100
   End
   Begin VB.TextBox TxtPassword 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      IMEMode         =   3  'DISABLE
      Left            =   1200
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   2040
      Width           =   3135
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "E&xit"
      Height          =   375
      Left            =   2880
      TabIndex        =   2
      Top             =   3000
      Width           =   1695
   End
   Begin VB.CommandButton PasswordEnter 
      Caption         =   "&Enter"
      Height          =   375
      Left            =   840
      TabIndex        =   1
      Top             =   3000
      Width           =   1695
   End
   Begin VB.Label BusinessDate 
      Alignment       =   2  'Center
      DataField       =   "BusinessDate"
      DataSource      =   "Data1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1200
      TabIndex        =   6
      Top             =   600
      Width           =   2775
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Current Business Date "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      TabIndex        =   5
      Top             =   240
      Width           =   3015
   End
   Begin VB.Label Label2 
      Caption         =   "You have to click on Enter, you can not press the [Enter] key on the keyboard."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1200
      TabIndex        =   4
      Top             =   3600
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Please enter Password"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1200
      TabIndex        =   3
      Top             =   1560
      Width           =   3135
   End
End
Attribute VB_Name = "FrmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdEnter_Click()

End Sub

Private Sub CmdExit_Click()
TxtPassword.Text = ""
FrmPassword.Hide
End
End Sub

Private Sub PasswordEnter_Click()
If Trim(LCase(TxtPassword.Text)) = "dt" Then
      x = MsgBox("This process will check all Drive-Thru orders to make sure no orders have a Drive-Thru time over 30 minutes.  If any orders are found the Drive-Thru time for these orders will be adjusted to 180 seconds.  After this process view a Daily Store Report to see if the Drive-Thru times were adjusted.", vbInformation, "Check and Adjust Drive-Thru Times")
       Open "c:\swipe.sql" For Output As #1
        Print #1, "Update tblOrder Set Closetime = DATEADD(second,180,firsttime) where (DATEDIFF(second,firsttime,closetime) >1800) AND (STATE=1)"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "enf" Then
      x = MsgBox("The password entered will enable the Schedule Enforcement feature of the IRIS system.  In order for this feature to work, you must publish your schedule weekly in LaborPro.  This process will also disable the BNE Labor Scheduling Tools.", vbInformation, "Enable Schedule Enforcement")
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 1 Where settingid = 542"
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgSetting] Set settingval = 1 Where settingid = 542"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
        Print #1, "cd\EdmWeb"
        Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      FileCopy "c:\swipe.sql", "c:\nodesys\nolabor.txt"
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "swipeoff" Then
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 0 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
        Print #1, "cd\EdmWeb"
        Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "swipeon" Then
      Open "c:\swipe.sql" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 1 Where settingid = 556"
      Close #1
      Open "c:\swipe.BAT" For Output As #1
        Print #1, "osql -S (local) -U sa -P -n -d Iris -i c:\swipe.sql"
        Print #1, "cd\EdmWeb"
        Print #1, "C:\EdmWeb\transfer_web_update_pos.bat"
      Close #1
      Y = Shell("C:\swipe.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "lsp" Then
      If Dir("C:\LABOR\SALESPRO.TXT") <> "" Then
        Kill ("C:\LABOR\SALESPRO.TXT")
      End If
      Y = Shell("C:\LABOR\SLSPROJ.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "hyper" Then
      Y = Shell("C:\hyper\Update_IRIS_POS_Config.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "stophyper" Then
      Y = Shell("C:\hyper\Stop Hyper.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "vnc" Then
      Y = Shell("C:\Documents and Settings\Administrator\Desktop\vnc-4.0-x86_win32_viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "pca" Then
      Y = Shell("C:\Program Files\Symantec\pcAnywhere\winaw32.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "mmc" Then
      Y = Shell("C:\WINdows\system32\mmc.exe c:\iris\bin\console1.msc", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "sql" Then
      Y = Shell("C:\Program Files\Microsoft SQL Server\80\Tools\Binn\isqlw.exe -S (local) -U sa -P -d Iris", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "log" Then
      If Dir("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe") = "" Then
        FileCopy "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.zip-_DisplayLogFileContents.UI.exe", "C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe"
      End If
      ChDir ("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer")
      Y = Shell("C:\Program Files\Religent\LiveConnect 5.2 BNE\logfileviewer\LiveConnect Client Log Viewer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "iex" Then
      Y = Shell("C:\Program Files\Internet Explorer\iexplore.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "vpn" Then
      Y = Shell("C:\bnevpn.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "delphi" Then
      ChDir ("C:\program files\OCS CS-Series Configuration Tool")
      Y = Shell("C:\program files\OCS CS-Series Configuration Tool\OCSConfigTool.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If


If Trim(LCase(TxtPassword.Text)) = "dbe" Then
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "sa" Then
      Y = Shell("cmd.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "exp" Then
      Y = Shell("c:\WINDOWS\explorer.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End
End If

If Trim(LCase(TxtPassword.Text)) = "oft" Then
      Y = Shell("c:\bneapps\oftload.exe", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "runlc" Then
      Y = Shell("c:\IRIS\BIN\RUNLC.BAT", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "dbr" Then
      
        Open "C:\DBR.BAT" For Output As #5    ' Open file for output.
        QUOTE = """"
        Print #5, "CD c:\X_WIN95\X_LANSA\X_DEV\SOURCE"
        Print #5, "C:\X_WIN95\X_LANSA\EXECUTE\x_start.EXE =c:\X_WIN95\X_LANSA\X_DEV\SOURCE\STRTFILE.1"
        Print #5, "EXIT"
        Close 5

      Y = Shell("c:\dbr.bat", vbNormalFocus)
      TxtPassword.Text = ""
      FrmPassword.Hide
      End
      Exit Sub
End If

If Trim(LCase(TxtPassword.Text)) = "file" Then
      If Dir("c:\program files\progressive software\psiexporter\out\iris.irs") <> "" Then
            x = MsgBox("IRIS poll file exist!  A LiveConnect session will be started.", vbInformation, "File Exists")
            Y = Shell("c:\IRIS\BIN\REPOLL.BAT", vbNormalFocus)
            End
      Else
              
            If Hour(Now) < 15 Then
                CheckDate = Date
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date + 1
            End If
            
            If BusinessDate <> CheckDate Then
                 x = MsgBox("Business Date is incorrect, please complete End of Day.", vbInformation, "Business Date Incorrect")
                 End
            End If
              
            Open "c:\iris\bin\createpoll.BAT" For Output As #1
            Print #1, "rem Created: " + Str(Now())
            Print #1, "Title DO NOT CLOSE THIS WINDOW"
            Print #1, "cd c:\iris\bin"
            If Weekday(Now) = 2 Then
                If Hour(Now) >= 15 Then
                    Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) = 3 Then
                If Hour(Now) < 15 Then
                    Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) <> 2 And Weekday(Now) <> 3 Then
                Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
            End If
            Print #1, "c:\iris\setup\sleep.exe 10000"
            Print #1, "cd c:\Program Files\Progressive Software\psiExporter\bin"
            Print #1, "PSIExporter.exe /yesterday /q"
            Print #1, "cd C:\Program Files\Progressive Software\psiExporter\OUT"
            Print #1, "IF EXIST IRIS.IRS copy IRIS.IRS + *.txt IRIS.IRS"
            Print #1, "IF NOT EXIST IRIS.IRS COPY *.TXT IRIS.IRS"
            Print #1, "del *.txt"
            Print #1, "cd \iris\bin"
            Print #1, "REPOLL.BAT"
            Close #1
            Y = Shell("c:\iris\bin\createpoll.BAT", vbNormalFocus)
            TxtPassword.Text = ""
            FrmPassword.Hide
        End
    Exit Sub
    End If
End If

If Trim(LCase(TxtPassword.Text)) = "eodcash" Then

            FileCopy "c:\iris\ini\eod.ini", "c:\iris\ini\eod.bak"
            Open "c:\iris\ini\eod.bak" For Input As #2
            Open "c:\iris\ini\eod.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 9)
                    Case "LastStep="
                        CurDate = Format$(Month(Now), "00") + "/" + Format$(Day(Now), "00") + "/" + Format$(Year(Now), "0000")
                        Print #3, "LastStep=PreEOD,Cmd9," + CurDate
                    Case Else
                        Print #3, Inline
                End Select
            Loop
            Close 2, 3
        response = MsgBox("End of Day has been configured to skip over the cash management error." + Chr(10) + Chr(10) + "Please start your End of Day again.", vbInformation, "Cash Drawer Error")
    End
End If

If Trim(LCase(TxtPassword.Text)) = "eodstand" Then

            FileCopy "c:\iris\ini\eod.ini", "c:\iris\ini\eod.bak"
            Open "c:\iris\ini\eod.bak" For Input As #2
            Open "c:\iris\ini\eod.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 38)
                    Case "Cmd5=c:\iris\bin\EODRegisterStatus.exe"
                        Print #3, "Cmd5=c:\iris\bin\EODRegisterStatus.exe /PlugIn /AllowContinue /q /Always"
                    Case Else
                        Print #3, Inline
                End Select
            Loop
            Close 2, 3
        response = MsgBox("End of Day has been configured allow continue on the Stand Alone Mode check." + Chr(10) + Chr(10) + "Please start your End of Day again.", vbInformation, "EOD Stand Alone Mode Check")
    End
End If


TxtPassword.Text = ""
FrmPassword.Hide
End
End Sub
