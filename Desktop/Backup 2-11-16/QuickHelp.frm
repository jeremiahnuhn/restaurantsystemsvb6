VERSION 5.00
Begin VB.Form QuickHelp 
   Caption         =   "Quick Help"
   ClientHeight    =   5715
   ClientLeft      =   1050
   ClientTop       =   1980
   ClientWidth     =   11580
   LinkTopic       =   "Form2"
   ScaleHeight     =   5715
   ScaleWidth      =   11580
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2460
      Left            =   2640
      Sorted          =   -1  'True
      TabIndex        =   15
      Top             =   2280
      Width           =   8655
   End
   Begin VB.CommandButton Command2 
      Caption         =   "View"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   13
      Top             =   3960
      Width           =   615
   End
   Begin VB.CommandButton ButtonClear 
      Caption         =   "Clear"
      Height          =   495
      Left            =   240
      TabIndex        =   12
      Top             =   3960
      Width           =   615
   End
   Begin VB.CommandButton Button0 
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   11
      Top             =   3960
      Width           =   615
   End
   Begin VB.CommandButton Button9 
      Caption         =   "9"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   10
      Top             =   3360
      Width           =   615
   End
   Begin VB.CommandButton Button8 
      Caption         =   "8"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   9
      Top             =   3360
      Width           =   615
   End
   Begin VB.CommandButton Button7 
      Caption         =   "7"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   8
      Top             =   3360
      Width           =   615
   End
   Begin VB.CommandButton Button6 
      Caption         =   "6"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   7
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton Button5 
      Caption         =   "5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   6
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton Button4 
      Caption         =   "4"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton Button3 
      Caption         =   "3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   4
      Top             =   2160
      Width           =   615
   End
   Begin VB.CommandButton Button2 
      Caption         =   "2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   3
      Top             =   2160
      Width           =   615
   End
   Begin VB.CommandButton Button1 
      Caption         =   "1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   2160
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9960
      TabIndex        =   1
      Top             =   5040
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Current Quick Help Documents and their Quick Help Numbers:"
      Height          =   255
      Left            =   2640
      TabIndex        =   16
      Top             =   2040
      Width           =   4575
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   14
      Top             =   4800
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   $"QuickHelp.frx":0000
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   11295
   End
End
Attribute VB_Name = "QuickHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Button0_Click()
QHNum = QHNum + "0"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button1_Click()
QHNum = QHNum + "1"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button2_Click()
QHNum = QHNum + "2"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button3_Click()
QHNum = QHNum + "3"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button4_Click()
QHNum = QHNum + "4"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button5_Click()
QHNum = QHNum + "5"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button6_Click()
QHNum = QHNum + "6"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button7_Click()
QHNum = QHNum + "7"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button8_Click()
QHNum = QHNum + "8"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub Button9_Click()
QHNum = QHNum + "9"
Label2 = "Quick Help # " & QHNum
QuickHelp.Refresh
Command2.Enabled = True
End Sub

Private Sub ButtonClear_Click()
QHNum = ""
Label2 = "Quick Help # is clear."
Command2.Enabled = False
QuickHelp.Refresh
End Sub

Private Sub Command1_Click()
QuickHelp.Hide
End Sub

Private Sub Command2_Click()
FoundNum = False
Open ("c:\Reports\IRIS Manual\Quick Help.BNR") For Input As #6
    Do Until EOF(6)
        Input #6, QHFileNum, QHFolder, QHDESC
        If QHNum = QHFileNum Then
            If Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then
                FileCopy QHFolder + QHDESC, "c:\PrintFile.PDF"
                exe = "C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /p " + Chr(34) + "c:\PrintFile.PDF" + Chr(34)
                
                FoundNum = True
                X = Shell(exe, vbMaximizedFocus)
                End
            Else
                FileCopy QHFolder + QHDESC, "c:\PrintFile.PDF"
                exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /n" + "c:\PrintFile.PDF"
                FoundNum = True
                X = Shell(exe, vbMaximizedFocus)
                End
            End If
        End If
    Loop
    If FoundNum = False Then
    msg = MsgBox("The Quick Help Number is not setup, please check your number.", vbCritical, "Quick Help")
    End If
Close (6)
End Sub

Private Sub Form_Load()
QHNum = ""
Label2 = "Enter Quick Help #."

Open ("c:\Reports\IRIS Manual\Quick Help.BNR") For Input As #6
    Do Until EOF(6)
        Input #6, QHFileNum, QHFolder, QHDESC
        List1.AddItem QHDESC + "   Quick Help # " + Str(QHFileNum)
    Loop
Close (6)


End Sub
