VERSION 5.00
Begin VB.Form CheckVersionForm 
   Caption         =   "Check Version"
   ClientHeight    =   4875
   ClientLeft      =   4380
   ClientTop       =   1185
   ClientWidth     =   4380
   LinkTopic       =   "Form2"
   ScaleHeight     =   4875
   ScaleWidth      =   4380
   Begin VB.CommandButton Command3 
      Caption         =   "6"
      Height          =   375
      Left            =   240
      TabIndex        =   16
      Top             =   4320
      Width           =   615
   End
   Begin VB.CommandButton Command2 
      Caption         =   "5"
      Height          =   375
      Left            =   240
      TabIndex        =   15
      Top             =   3840
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1080
      TabIndex        =   0
      Top             =   3840
      Width           =   3135
   End
   Begin VB.Label T6Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   14
      Top             =   3120
      Width           =   2055
   End
   Begin VB.Label T5Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   13
      Top             =   2640
      Width           =   2055
   End
   Begin VB.Label T4Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   12
      Top             =   2160
      Width           =   2055
   End
   Begin VB.Label T3Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   11
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label T2Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   10
      Top             =   1200
      Width           =   2055
   End
   Begin VB.Label Label12 
      Caption         =   "Terminal 6"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   3120
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label10 
      Caption         =   "Terminal 5"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   2640
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "Terminal 4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Top             =   2160
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Terminal 3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   6
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "Terminal 2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label T1Label 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   4
      Top             =   720
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Terminal 1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Server"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label ServerVersionLabel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      TabIndex        =   1
      Top             =   240
      Width           =   2055
   End
End
Attribute VB_Name = "CheckVersionForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
On Error GoTo ErrorHandler
Screen.MousePointer = 11
Label10.Visible = True
'Terminal 5
    T5Ver = "Unavailable"
    T5Ver = Format$(FileDateTime("\\bne_reg5\c\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case T5Ver
        Case "04042011"
            T5Label.Caption = "3.7.14 874"
        Case "2/16/2009 "
            T5Label.Caption = "3.7.8.2 151"
        Case "05052010 "
            T5Label.Caption = "3.7.14 848"
        Case "08102010"
            T5Label.Caption = "3.7.14 856"
        Case "01252011"
            T5Label.Caption = "3.7.14 872"
        Case "07052012"
            T5Label.Caption = "3.8 1174"
        Case "08282012"
            T5Label.Caption = "3.8 1176"
        Case "11012012"
            T5Label.Caption = "3.8 1406"
        Case "02112013"
            T5Label.Caption = "3.8 1407"
        Case "05062013"
            T5Label.Caption = "3.8 1413"
        Case "09232013"
            T5Label.Caption = "3.8 1416"
        Case Else
            T5Label.Caption = T5Ver
        End Select
Screen.MousePointer = 0
Exit Sub

ErrorHandler:

If Err.Number = 53 Then
    Resume Next
End If

End Sub

Private Sub Command3_Click()
On Error GoTo ErrorHandler
Screen.MousePointer = 11
Label12.Visible = True
'Terminal 6
    T6Ver = "Unavailable"
    T6Ver = Format$(FileDateTime("\\bne_reg6\c\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case Mid$(T6Ver, 1, 10)
        Case "04042011"
            T6Label.Caption = "3.7.14 874"
        Case "2/16/2009 "
            T6Label.Caption = "3.7.8.2 151"
        Case "05052010 "
            T6Label.Caption = "3.7.14 848"
        Case "08102010"
            T6Label.Caption = "3.7.14 856"
        Case "08102010"
            T6Label.Caption = "3.7.14 856"
        Case "01252011"
            T6Label.Caption = "3.7.14 872"
        Case "07052012"
            T6Label.Caption = "3.8 1174"
        Case "08282012"
            T6Label.Caption = "3.8 1176"
        Case "11012012"
            T6Label.Caption = "3.8 1406"
        Case "02112013"
            T6Label.Caption = "3.8 1407"
        Case "05062013"
            T6Label.Caption = "3.8 1413"
        Case "09232013"
            T6Label.Caption = "3.8 1416"
        Case Else
            T6Label.Caption = T6Ver
        End Select
Screen.MousePointer = 0
Exit Sub

ErrorHandler:

If Err.Number = 53 Then
    Resume Next
End If

End Sub

Private Sub Form_Load()
On Error GoTo ErrorHandler
For X = 1 To 500000
    DoEvents
Next X
'SERVER
    ServerVer = "Unavailable"
    ServerVer = Format$(FileDateTime("c:\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case ServerVer
        Case "04042011"
            ServerVersionLabel.Caption = "3.7.14 874"
        Case "02162009"
            ServerVersionLabel.Caption = "3.7.8.2 151"
        Case "05052010"
            ServerVersionLabel.Caption = "3.7.14 848"
        Case "08102010"
            ServerVersionLabel.Caption = "3.7.14 856"
        Case "01252011"
            ServerVersionLabel.Caption = "3.7.14 872"
        Case "07052012"
            ServerVersionLabel.Caption = "3.8 1174"
        Case "08282012"
            ServerVersionLabel.Caption = "3.8 1176"
        Case "11012012"
            ServerVersionLabel.Caption = "3.8 1406"
        Case "02112013"
            ServerVersionLabel.Caption = "3.8 1407"
        Case "05062013"
            ServerVersionLabel.Caption = "3.8 1413"
        Case "09232013"
            ServerVersionLabel.Caption = "3.8 1416"
        Case Else
            ServerVersionLabel.Caption = ServerVer
        End Select
'Terminal 1
    T1Ver = "Unavailable"
    T1Ver = Format$(FileDateTime("\\bne_reg1\c\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case T1Ver
        Case "04042011"
            T1Label.Caption = "3.7.14 874"
        Case "2/16/2009 "
            T1Label.Caption = "3.7.8.2 151"
        Case "05052010"
            T1Label.Caption = "3.7.14 848"
        Case "08102010"
            T1Label.Caption = "3.7.14 856"
        Case "01252011"
            T1Label.Caption = "3.7.14 872"
        Case "07052012"
            T1Label.Caption = "3.8 1174"
        Case "08282012"
            T1Label.Caption = "3.8 1176"
        Case "11012012"
            T1Label.Caption = "3.8 1406"
        Case "02112013"
            T1Label.Caption = "3.8 1407"
        Case "05062013"
            T1Label.Caption = "3.8 1413"
        Case "09232013"
            T1Label.Caption = "3.8 1416"
        Case Else
            T1Label.Caption = T1Ver
        End Select

'Terminal 2
    T2Ver = "Unavailable"
    T2Ver = Format$(FileDateTime("\\bne_reg2\c\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case T2Ver
        Case "04042011"
            T2Label.Caption = "3.7.14 874"
        Case "2/16/2009 "
            T2Label.Caption = "3.7.8.2 151"
        Case "05052010"
            T2Label.Caption = "3.7.14 848"
        Case "08102010"
            T2Label.Caption = "3.7.14 856"
        Case "01252011"
            T2Label.Caption = "3.7.14 872"
        Case "07052012"
            T2Label.Caption = "3.8 1174"
        Case "08282012"
            T2Label.Caption = "3.8 1176"
        Case "11012012"
            T2Label.Caption = "3.8 1406"
        Case "02112013"
            T2Label.Caption = "3.8 1407"
        Case "05062013"
            T2Label.Caption = "3.8 1413"
        Case "09232013"
            T2Label.Caption = "3.8 1416"
        Case Else
            T2Label.Caption = T2Ver
        End Select

'Terminal 3
    T3Ver = "Unavailable"
    T3Ver = Format$(FileDateTime("\\bne_reg3\c\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case T3Ver
        Case "04042011"
            T3Label.Caption = "3.7.14 874"
        Case "2/16/2009 "
            T3Label.Caption = "3.7.8.2 151"
        Case "05052010"
            T3Label.Caption = "3.7.14 848"
        Case "08102010"
            T3Label.Caption = "3.7.14 856"
        Case "01252011"
            T3Label.Caption = "3.7.14 872"
        Case "07052012"
            T3Label.Caption = "3.8 1174"
        Case "08282012"
            T3Label.Caption = "3.8 1176"
        Case "11012012"
            T3Label.Caption = "3.8 1406"
        Case "02112013"
            T3Label.Caption = "3.8 1407"
        Case "05062013"
            T3Label.Caption = "3.8 1413"
        Case "09232013"
            T3Label.Caption = "3.8 1416"
        Case Else
            T3Label.Caption = T3Ver
        End Select

'Terminal 4
    T4Ver = "Unavailable"
    T4Ver = Format$(FileDateTime("\\bne_reg4\c\iris\bin\pos.exe"), "MMDDYYYY")
    Select Case T4Ver
        Case "04042011"
            T4Label.Caption = "3.7.14 874"
        Case "02162009"
            T4Label.Caption = "3.7.8.2 151"
        Case "05052010"
            T4Label.Caption = "3.7.14 848"
        Case "08102010"
            T4Label.Caption = "3.7.14 856"
        Case "01252011"
            T4Label.Caption = "3.7.14 872"
        Case "07052012"
            T4Label.Caption = "3.8 1174"
        Case "08282012"
            T4Label.Caption = "3.8 1176"
        Case "11012012"
            T4Label.Caption = "3.8 1406"
        Case "02112013"
            T4Label.Caption = "3.8 1407"
        Case "05062013"
            T4Label.Caption = "3.8 1413"
        Case "09232013"
            T4Label.Caption = "3.8 1416"
        Case Else
            T4Label.Caption = T4Ver
        End Select
        
Screen.MousePointer = 0
Exit Sub

ErrorHandler:

If Err.Number = 53 Then
    Resume Next
End If

End Sub

