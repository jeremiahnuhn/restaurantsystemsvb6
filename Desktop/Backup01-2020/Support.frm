VERSION 5.00
Begin VB.Form Support 
   Caption         =   "IRIS Support Functions"
   ClientHeight    =   9405
   ClientLeft      =   255
   ClientTop       =   345
   ClientWidth     =   11580
   Icon            =   "Support.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   9405
   ScaleWidth      =   11580
   Begin VB.CommandButton Command42 
      Caption         =   "Set PW IRIS FTP"
      Height          =   375
      Left            =   8160
      TabIndex        =   63
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CommandButton Command28 
      Caption         =   "Replace Server MDBs"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   62
      Top             =   8280
      Width           =   3015
   End
   Begin VB.CommandButton Command41 
      Caption         =   "Activation"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      TabIndex        =   61
      Top             =   8280
      Width           =   1455
   End
   Begin VB.CommandButton Command40 
      Caption         =   "Sockets"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   60
      Top             =   8280
      Width           =   1455
   End
   Begin VB.CommandButton Command39 
      Caption         =   "Display"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   59
      Top             =   8280
      Width           =   1455
   End
   Begin VB.CommandButton Command38 
      Caption         =   "QSR Client"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9720
      TabIndex        =   58
      Top             =   7800
      Width           =   1455
   End
   Begin VB.CommandButton Command37 
      Caption         =   "Acronis Backup"
      Height          =   495
      Left            =   8160
      TabIndex        =   57
      Top             =   8760
      Width           =   1455
   End
   Begin VB.CommandButton Command36 
      Caption         =   "Scanner"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      TabIndex        =   56
      Top             =   8760
      Width           =   1455
   End
   Begin VB.CommandButton Command35 
      Caption         =   "Shutdown"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2880
      TabIndex        =   55
      Top             =   5160
      Width           =   1095
   End
   Begin VB.CommandButton Command34 
      Caption         =   "Network Connections"
      Height          =   495
      Left            =   5040
      TabIndex        =   54
      Top             =   8760
      Width           =   1455
   End
   Begin VB.CommandButton Command33 
      Caption         =   "SQLite Studio"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3480
      TabIndex        =   53
      Top             =   8760
      Width           =   1455
   End
   Begin VB.CommandButton Command32 
      Caption         =   "Secure Desktop on Terminals"
      Height          =   495
      Left            =   1920
      TabIndex        =   52
      Top             =   8760
      Width           =   1455
   End
   Begin VB.CommandButton Command31 
      Caption         =   "Void Setup"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   51
      Top             =   8760
      Width           =   1455
   End
   Begin VB.CommandButton Command30 
      Caption         =   "Event Viewer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9720
      TabIndex        =   50
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CommandButton Command29 
      Caption         =   "ODBC Admin"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9720
      TabIndex        =   49
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Command27 
      Caption         =   "Settle CC"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   48
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CommandButton Command26 
      Caption         =   "Stop Archiver"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6600
      TabIndex        =   47
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CommandButton Command25 
      Caption         =   "Repoll"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      TabIndex        =   46
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CommandButton Command24 
      Caption         =   "Log File"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   45
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CommandButton Command23 
      Caption         =   "RegMerge"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   44
      Top             =   7320
      Width           =   1455
   End
   Begin VB.CommandButton Command22 
      Caption         =   "PM Ping"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8160
      TabIndex        =   43
      Top             =   7800
      Width           =   1455
   End
   Begin VB.CommandButton Command21 
      Caption         =   "RAID Mgr"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6600
      TabIndex        =   42
      Top             =   7800
      Width           =   1455
   End
   Begin VB.CommandButton Command20 
      Caption         =   "Modems"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   41
      Top             =   7800
      Width           =   1455
   End
   Begin VB.CommandButton Serv 
      Caption         =   "Services"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      TabIndex        =   40
      Top             =   7800
      Width           =   1455
   End
   Begin VB.CommandButton Tasks 
      Caption         =   "Tasks"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   39
      Top             =   7800
      Width           =   1455
   End
   Begin VB.CommandButton Printers 
      Caption         =   "Printers"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   38
      Top             =   7800
      Width           =   1455
   End
   Begin VB.CommandButton CopyBIN 
      Caption         =   "Copy  Bin Folder"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   37
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton PING 
      Caption         =   "PING"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   36
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton Command19 
      Caption         =   "LaborPro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8160
      TabIndex        =   35
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Command18 
      Caption         =   "POSCFG Link"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      TabIndex        =   34
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Command17 
      Caption         =   "POSLive"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3480
      TabIndex        =   33
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Command16 
      Caption         =   "POSTrans"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5040
      TabIndex        =   32
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Payroll 
      Caption         =   "Payroll"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1920
      TabIndex        =   31
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Command15 
      Caption         =   "View Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8160
      TabIndex        =   30
      Top             =   6120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.FileListBox EDMIncoming 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3390
      Left            =   5520
      TabIndex        =   28
      Top             =   2280
      Visible         =   0   'False
      Width           =   5655
   End
   Begin VB.CommandButton Command14 
      Caption         =   "EDM DIR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8160
      TabIndex        =   27
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton Command13 
      Caption         =   "EDM Admin"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6600
      TabIndex        =   26
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton Command12 
      Caption         =   "Manager Message"
      Height          =   375
      Left            =   8280
      TabIndex        =   25
      Top             =   120
      Width           =   2895
   End
   Begin VB.CheckBox Day30 
      Caption         =   "Last 30 Days"
      Height          =   255
      Left            =   6960
      TabIndex        =   24
      Top             =   5760
      Value           =   1  'Checked
      Width           =   1575
   End
   Begin VB.CommandButton Command11 
      Caption         =   "Log Entry"
      Height          =   375
      Left            =   6720
      TabIndex        =   23
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   360
      TabIndex        =   22
      Top             =   120
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Manual Log Entry"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1560
      TabIndex        =   21
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton Command6 
      Caption         =   "POSLIVE and POSPEND"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1560
      TabIndex        =   13
      Top             =   2280
      Width           =   3735
   End
   Begin VB.CheckBox Filter 
      Caption         =   "Filtered"
      Height          =   255
      Left            =   5760
      TabIndex        =   19
      Top             =   5760
      Width           =   855
   End
   Begin VB.ListBox List1 
      Height          =   3375
      Left            =   5520
      TabIndex        =   18
      Top             =   2280
      Width           =   5655
   End
   Begin VB.OptionButton Option8 
      Caption         =   "Default"
      Height          =   375
      Left            =   360
      TabIndex        =   17
      Top             =   2280
      Value           =   -1  'True
      Width           =   1335
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Setup CHKDSK /F"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4080
      TabIndex        =   16
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton Command8 
      Caption         =   "4.0 Ghost (Reimage) Setup"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1560
      TabIndex        =   15
      Top             =   3240
      Width           =   3735
   End
   Begin VB.CommandButton Command7 
      Caption         =   "INIs and POSCFG"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1560
      TabIndex        =   14
      Top             =   4200
      Width           =   3735
   End
   Begin VB.CommandButton Command5 
      Caption         =   "DBE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9720
      TabIndex        =   12
      Top             =   6120
      Width           =   1455
   End
   Begin VB.OptionButton Option7 
      Caption         =   "Server"
      Height          =   375
      Left            =   360
      TabIndex        =   11
      Top             =   5640
      Width           =   1335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "SQL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5040
      TabIndex        =   10
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton Command3 
      Caption         =   "PCA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3480
      TabIndex        =   9
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "VNC"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1920
      TabIndex        =   8
      ToolTipText     =   "If keyboard is not working remotely: Hold down the Alt key and press Enter on the keyboard."
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   9720
      TabIndex        =   7
      Top             =   8760
      Width           =   1455
   End
   Begin VB.OptionButton Option6 
      Caption         =   "Terminal 6"
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Top             =   5160
      Width           =   1335
   End
   Begin VB.OptionButton Option5 
      Caption         =   "Terminal 5"
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   4680
      Width           =   1335
   End
   Begin VB.OptionButton Option4 
      Caption         =   "Terminal 4"
      Height          =   375
      Left            =   360
      TabIndex        =   4
      Top             =   4200
      Width           =   1335
   End
   Begin VB.OptionButton Option3 
      Caption         =   "Terminal 3"
      Height          =   375
      Left            =   360
      TabIndex        =   3
      Top             =   3720
      Width           =   1335
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Terminal 2"
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   3240
      Width           =   1335
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Terminal 1"
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   2760
      Width           =   1335
   End
   Begin VB.Label Label3 
      Caption         =   "Directory of c:\edmweb\incoming\0"
      Height          =   255
      Left            =   5520
      TabIndex        =   29
      Top             =   2040
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label Label2 
      Caption         =   "c:\bneapps\support.log"
      Height          =   255
      Left            =   9480
      TabIndex        =   20
      Top             =   5760
      Width           =   1695
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   $"Support.frx":0442
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1335
      Left            =   360
      TabIndex        =   0
      Top             =   600
      Width           =   10815
   End
End
Attribute VB_Name = "Support"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Update_List1()
On Error GoTo ErrorHandler
List1.Clear
If Filter.Value = Checked Then
    If Option1.Value = True Then
        CheckData = "*T1*"
    End If
    If Option2.Value = True Then
        CheckData = "*T2*"
    End If
    If Option3.Value = True Then
        CheckData = "*T3*"
    End If
    If Option4.Value = True Then
        CheckData = "*T4*"
    End If
    If Option5.Value = True Then
        CheckData = "*T5*"
    End If
    If Option6.Value = True Then
        CheckData = "*T6*"
    End If
    If Option7.Value = True Then
        CheckData = "*S *"
    End If
    If Option8.Value = True Then
        CheckData = "**"
    End If
Else
        CheckData = "**"
End If
Open ("c:\BNEAPPS\SUPPORT.LOG") For Input As #1
    If Day30.Value = 1 Then
        MaxDate = Format(Now() - 30, "mm/dd/yyyy")
    Else
        MaxDate = Format(Now() - 365, "mm/dd/yyyy")
    End If
    Do While Not EOF(1)
        Line Input #1, LineData
    If CDate(Mid$(LineData, 1, 10)) > CDate(MaxDate) Then
            MyResult = LineData Like CheckData Or LineData Like "*C *"
            If MyResult = True Then
                List1.AddItem LineData
            End If
    End If
    Loop
Close #1
Exit Sub
ErrorHandler:
            Close #1
            FileCopy "c:\bneapps\support.log", "c:\bneapps\support.bbb"
            Kill ("c:\bneapps\support.log")
            X = MsgBox("Error in Support Log.  The log will be restarted.  A backup file will be written to support.bbb.", vbInformation, "Log Read Error")
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
If Dir("c:\BNEAPPS\SUPPORT.LOG") = "" Then
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, MyTime + " C - Log File Created"
       Close #1
End If

'Read Log
Update_List1
End Sub


Private Sub Command1_Click()
    End
End Sub

Private Sub Command10_Click()
    Command11.Visible = True
    MyTime = Format(Now(), "hh:mm AMPM")
    MyDate = Format(Now(), "mm/dd/yyyy")
    Text1.Text = MyDate + " " + MyTime + " C  -  "
    Text1.Visible = True
    

End Sub

Private Sub Command11_Click()
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, Text1.Text
       Close #1
       Command11.Visible = False
       Text1.Visible = False
       Update_List1
End Sub

Private Sub Command12_Click()
    MgrMsg.Show

      Form1.Hide
      Support.Hide
End Sub

Private Sub Command13_Click()
        MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, MyTime + " S - EDM Admin Accessed"
       Close #1
        ChDir ("c:\EDMWEB")
      Y = Shell("c:\EDMWEB\GO.BAT", vbNormalFocus)
      Update_List1

End Sub

Private Sub Command14_Click()
List1.Visible = False
Command14.Visible = False
EDMIncoming.FileName = "c:\edmweb\incoming\0"
EDMIncoming.Visible = True
Label3.Visible = True
Command15.Visible = True
Label2.Visible = False
Filter.Visible = False
Day30.Visible = False
EDMIncoming.Refresh
End Sub

Private Sub Command15_Click()
List1.Visible = True
Command14.Visible = True
Command15.Visible = False
EDMIncoming.Visible = False
Label3.Visible = False
Label2.Visible = True
Filter.Visible = True
Day30.Visible = True
End Sub

Private Sub Command16_Click()
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE C:\IRIS\DATA\POSTRANS.MDB", vbNormalFocus)
End Sub

Private Sub Command17_Click()
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE C:\IRIS\DATA\POSLIVE.MDB", vbNormalFocus)
End Sub

Private Sub Command18_Click()
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE C:\IRIS\DATA\POSCFGLINK.MDB", vbNormalFocus)
End Sub

Private Sub Command19_Click()
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE C:\SUPPORT\LABORPROLINK.MDB", vbNormalFocus)
End Sub

Private Sub Command2_Click()
    If Dir("c:\program files\ultravnc\vncviewer.exe") = "" Then
      Y = Shell("C:\Documents and Settings\Administrator\Desktop\vnc-4.0-x86_win32_viewer.exe", vbNormalFocus)
    Else
      Y = Shell("c:\program files\ultravnc\vncviewer.exe", vbNormalFocus)
    End If
End Sub

Private Sub Command20_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "devmgmt.msc"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Modems "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command21_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    ChDir ("\Program Files\IBM\ServeRAID Manager")
    Open "C:\ul.bat" For Output As 1
    Print #1, Chr(34) + "\Program Files\IBM\ServeRAID Manager\RAIDMAN.BAT" + Chr(34)
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed RAID "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command22_Click()
    If Dir("C:\support\pmping.bat") = "" Then
        Command22.Enabled = False
        X = MsgBox("This process is not in production.  C:\support\pmping.bat missing.", vbInformation, "File Missing.")
        Exit Sub
    End If
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Y = Shell("C:\support\pmping.bat", vbHide)
    LogText = MyTime + " S  - PM Ping "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command23_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "c:\iris\scripts\ArchiverTerminate.vbs"
    Print #1, "cd\iris\bin"
    Print #1, "RegMerge.EXE"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - RegMerge"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command24_Click()
    
    FName = Format(Now() + 1, "yyyy-mm-dd")
    Open "C:\ul.bat" For Output As 1
        Print #1, Chr(34) + "\program files\winzip\winzip32.exe" + Chr(34) + " c:\iris\log\backup\" + FName + ".zip"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
        MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Log File Accessed "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command25_Click()
            If Hour(Now) < 15 Then
                CheckDate = Date
            End If
            If Hour(Now) >= 15 Then
                CheckDate = Date + 1
            End If
            
              
            Open "c:\iris\bin\createpoll.BAT" For Output As #1
            Print #1, "rem Created: " + Str(Now())
            Print #1, "Title DO NOT CLOSE THIS WINDOW"
            Print #1, "cd c:\iris\bin"
            If Weekday(Now) = 2 Then
                If Hour(Now) >= 15 Then
                    'Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) = 3 Then
                If Hour(Now) < 15 Then
                    'Print #1, "eodpayrollcalc.exe /payperiod=previouspayperiod"
                Else
                    'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
                End If
            End If
            If Weekday(Now) <> 2 And Weekday(Now) <> 3 Then
                'Print #1, "eodpayrollcalc.exe /payperiod=currentpayperiod"
            End If
            Print #1, "cd \iris\bin"
            'Print #1, "Settle~1.exe"
            'Print #1, "c:\iris\setup\sleep.exe 10000"
            Print #1, "cd c:\Program Files\xpient solutions\psiExporter\bin"
            Print #1, "PSIExporter.exe /yesterday /q"
            Print #1, "cd C:\Program Files\xpient solutions\psiExporter\OUT"
            Print #1, "CALL XNETCOPY.BAT"
'            Print #1, "IF EXIST IRIS.IRS copy IRIS.IRS + *.txt IRIS.IRS"
'            Print #1, "IF NOT EXIST IRIS.IRS COPY *.TXT IRIS.IRS"
'            Print #1, "del *.txt"
            Print #1, "cd \iris\bin"
            Print #1, "REPOLL.BAT"
            Close #1
            Y = Shell("c:\iris\bin\createpoll.BAT", vbMinimizedNoFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Repolled "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
End Sub

Private Sub Command26_Click()
    Open "C:\ul.bat" For Output As 1
    Print #1, "cd \iris\scripts"
    Print #1, "ArchiverTerminate.vbs"
    Close 1
    Y = Shell("C:\ul.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Stopped Archiver "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
End Sub

Private Sub Command27_Click()
    Open "C:\ul.bat" For Output As 1
    Print #1, "cd \iris\scripts"
    Print #1, "SettleSTAGEONLY.vbs"
    Close 1
    Y = Shell("C:\ul.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Settled Credit Cards"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
    Close #1
Update_List1
End Sub

Private Sub Command28_Click()
'Replace Server MDBs
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
Print #1, "@TITLE **  COPY DATABASE FROM THE TEMPLATE FOLDER TO THE DATA FOLDER"
Print #1, "copy C:\IRIS\Data\Template\BackOffice.mdb  C:\IRIS\Data\BackOffice.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\FlexPoll.Mdb C:\IRIS\Data\FlexPoll.Mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\Inventory.mdb C:\IRIS\Data\Inventory.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\Payroll.mdb C:\IRIS\Data\Payroll.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\POSTrans.mdb C:\IRIS\Data\POSTrans.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\Report.Mdb C:\IRIS\Data\Report.Mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\Security.mdb C:\IRIS\Data\Security.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\Shell.mdb C:\IRIS\Data\Shell.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\WSR.Mdb C:\IRIS\Data\WSR.Mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\xception.mdb C:\IRIS\Data\xception.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\forecast.mdb C:\IRIS\Data\forecast.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\authdata.mdb C:\IRIS\Data\authdata.mdb /Y"
Print #1, "copy C:\IRIS\Data\Template\DSR.mdb C:\IRIS\Data\DSR.mdb /Y"
Print #1, "call c:\iris\setup\UpdateLinks.VBS"
Print #1, "pause"
    Close 1
    Y = Shell("C:\ul.bat", vbMaximizedFocus)
    
    LogText = MyTime + " S  - Server MDBs Replaced "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1


End Sub

Private Sub Command29_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "odbcad32.exe"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed ODBC "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub

End Sub

Private Sub Command3_Click()
      Y = Shell("C:\Program Files\Symantec\pcAnywhere\winaw32.exe", vbNormalFocus)
End Sub

Private Sub Command30_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "eventvwr.msc"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Event Viewer "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command31_Click()
'Void Setup 12/2011
Open "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.ini" For Input As #8
Do While Not EOF(8)
    Line Input #8, Inline
    If Mid$(Inline, 1, 6) = "Void=1" Then
        response = MsgBox("Credit Card voids are disabled.  Do you want to enable this function?", vbYesNo, "Enable Voids")
        If response = vbYes Then
            Close 8
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Enable CC Void"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Enable CC Void"
            Close 2, 3, 4
            FileCopy "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.ini", "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.bak"
            Open "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.bak" For Input As #2
            Open "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 6)
                    Case "Void=1"
                        Print #3, "Void=0"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
        MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Enabled CC Void "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #7
        Print #7, LogText
       Close 7

            response = MsgBox("Configuration file changed.  The ability to void a Credit Card has been enabled.", vbInformation, "Enable Void")
            End
        Else
            Close 1

        End If
    End If
    If Mid$(Inline, 1, 6) = "Void=0" Then
        response = MsgBox("Credit Card voids are enabled.  Do you want to disable this function?", vbYesNo, "Disable Voids")
        If response = vbYes Then
            Close 8
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close #6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Disable CC Void"
            Close 4
            Open "c:\bneapps\Authorization.Log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Disable CC Void"

            Close 2, 3, 4
            FileCopy "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.ini", "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.bak"
            Open "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.bak" For Input As #2
            Open "C:\Program Files\Xpient Solutions\Credit Card Application\CCAManager\CCAManager.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 6)
                    Case "Void=0"
                        Print #3, "Void=1"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
                    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Disabled CC Void"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #7
        Print #7, LogText
       Close 7

            response = MsgBox("Configuration file changed.  The ability to void a Credit Card has been disabled.", vbInformation, "Disable Void")
            End
        Else
            Close 1

        End If
    End If
Loop
Close 8

Update_List1
End Sub

Private Sub Command32_Click()

'Disable/Enable Secure Desktop on Terminal 2/21/2012


If Dir("c:\iris\reginfo\common\support\DisableStart.trm") = "" Then

    response = MsgBox("Terminals are setup in secure mode.  Do you want to disable this function?", vbYesNo, "Secure Terminal")
        If response = vbYes Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Secure Terminal Disabled"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Secure Terminal Disabled"
            Close 4
            FileCopy "c:\iris\reginfo\common\support\EnableStart.reg", "c:\iris\reginfo\common\support\DisableStart.reg"
            FileCopy "c:\iris\reginfo\common\support\DisableStart.reg", "c:\iris\reginfo\common\support\DisableStart.trm"
        MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Secure Terminal Disabled "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #7
        Print #7, LogText
       Close 7

            response = MsgBox("Configuration file changed.  You will need to reboot the terminal(s) TWICE to unsecure the Desktop.  Once work has been completed, click on this button again to secure the terminal.", vbInformation, "Secure Terminal Disabled")
        
        End If
        End
   Else
    response = MsgBox("Terminals are setup in unsecure mode.  Do you want to enable this function?", vbYesNo, "Secure Terminal")
        If response = vbYes Then
            Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Secure Terminal Enabled"
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + "Secure Terminal Enabled"
            Close 4
            FileCopy "c:\iris\reginfo\common\support\DisableStart.trm", "c:\iris\reginfo\common\support\DisableStart.reg"
            Kill ("c:\iris\reginfo\common\support\Disablestart.trm ")
        MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " S  - Secure Terminal Enabled "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #7
        Print #7, LogText
       Close 7

            response = MsgBox("Configuration file changed.  You will need to reboot the terminal(s) TWICE to secure the Desktop.", vbInformation, "Secure Terminal Enabled")
        End If
        End

  
    End If
    
Update_List1




End Sub

Private Sub Command33_Click()
      Y = Shell("c:\IRIS\Tools\SQLitestudio.EXE", vbNormalFocus)
End Sub

Private Sub Command34_Click()
      Y = Shell("control netconnections", vbNormalFocus)
End Sub

Private Sub Command35_Click()
'SHUTDOWN TERMINAL
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
If Option7.Value = True Then
    Open "C:\ul.bat" For Output As 1
    Print #1, "cd \iris\setup"
    Print #1, "updatelinks.vbs"
    Close 1
    Y = Shell("C:\ul.bat", vbNormalFocus)
    LogText = MyTime + " S  - Update Links "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
Exit Sub
End If


' Shutdown FILES

If Option1.Value = True Then
    LogText = MyTime + " T1 - Shutdown "
    CmdText1a = "net use \\bne_reg1\c$ /user:iris_admin Bn3_Ir1$"
    CmdText1 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg1"
End If
If Option2.Value = True Then
    LogText = MyTime + " T2 - Shutdown "
    CmdText1a = "net use \\bne_reg2\c$ /user:iris_admin Bn3_Ir1$"
    CmdText1 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg2"
End If
If Option3.Value = True Then
    LogText = MyTime + " T3 - Shutdown "
    CmdText1a = "net use \\bne_reg3\c$ /user:iris_admin Bn3_Ir1$"
    CmdText1 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg3"
End If
If Option4.Value = True Then
    LogText = MyTime + " T4 - Shutdown "
    CmdText1a = "net use \\bne_reg4\c$ /user:iris_admin Bn3_Ir1$"
    CmdText1 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg4"
End If
If Option5.Value = True Then
    LogText = MyTime + " T5 - Shutdown "
    CmdText1a = "net use \\bne_reg5\c$ /user:iris_admin Bn3_Ir1$"
    CmdText1 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg5"
End If
If Option6.Value = True Then
    LogText = MyTime + " T6 - Shutdown "
    CmdText1a = "net use \\bne_reg6\c$ /user:iris_admin Bn3_Ir1$"
    CmdText1 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg6"
End If
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Shutdown Terminal"
            Print #1, CmdText1a
            Print #1, CmdText1
            Print #1, "Exit"
      Close #1
      Y = Shell("c:\ih.BAT", vbNormalFocus)
      Update_List1
End Sub

Private Sub Command36_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "control sticpl.cpl"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Printers "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command37_Click()
Y = Shell("C:\Program Files\Acronis\BackupAndRecoveryConsole\ManagementConsole.exe", vbNormalFocus)

End Sub

Private Sub Command38_Click()
qsr = MsgBox("The QSR Control Point Client does not have a close button or menu to exit.  Press the ALT and F4 keys at the same time to close the client.", vbInformational, "QSR Control Point Client")
Y = Shell("c:\Program Files\Qsr Automations\ControlPoint\ControlPointClient\Bin\controlpointclient.exe", vbMaximizedFocus)

End Sub

Private Sub Command39_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "desk.cpl"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Display Properties "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub

End Sub

Private Sub Command4_Click()
If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
      Y = Shell("c:\Program Files\Microsoft SQL Server\90\Tools\Binn\VSShell\Common7\IDE\SSMSEE.EXE", vbNormalFocus)
Else
      Y = Shell("C:\WINdows\system32\mmc.exe c:\iris\bin\console1.msc", vbNormalFocus)
End If
End Sub

Private Sub Command40_Click()
            response = MsgBox("This process will copy the backup Socket INI files for the terminals and server.  Reminder:  Each restaurant has unique socket.ini files due to Heartland credit card processing.", vbInformation, "Socket.ini Files")
    
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "c:\support\win7\sockets.bat"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Display Properties "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command41_Click()
qsr = MsgBox("This button will correct the Windows License.", vbInformational, "Activation")
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "slmgr.vbs /ato"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Windows Activation "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub

End Sub

Private Sub Command42_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "CD C:\IRIS\BIN\"
    Print #1, "XSAUTH -user=IRIS_Admin -password=Bn3_Ir1$"
    Print #1, "XSAUTH -user=IRIS_user -password=Bn3_U$3r"
    Print #1, "pause"
    Close 1
    Y = Shell("C:\ul.bat", vbNormalFocus)
    LogText = MyTime + " S  - Set Password for IRIS FTP"
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Command5_Click()
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE", vbNormalFocus)
End Sub

Private Sub Command6_Click()

'   POSLIVE AND POSPEND
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")

If Option1.Value = True Then
    LogText = MyTime + " T1 - Replace POSLIVE and POSPEND "
    CmdText1 = "Copy \\BNE_REG1\C\IRIS\DATA\POSLIVE.MDB \\BNE_REG1\C\IRIS\LOG\POSLIVE.BAD"
    CmdText2 = "Copy C:\FIX\POSLIVE.MDB \\BNE_REG1\C\IRIS\DATA\POSLIVE.MDB"
    CmdText3 = "Copy C:\FIX\POSPEND.MDB \\BNE_REG1\C\IRIS\DATA\POSPEND.MDB"
    CmdText4 = "Copy C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG1\C\REGEMERGE.FLG"
End If
If Option2.Value = True Then
    LogText = MyTime + " T2 - Replace POSLIVE and POSPEND "
    CmdText1 = "Copy \\BNE_REG2\C\IRIS\DATA\POSLIVE.MDB \\BNE_REG2\C\IRIS\LOG\POSLIVE.BAD"
    CmdText2 = "Copy C:\FIX\POSLIVE.MDB \\BNE_REG2\C\IRIS\DATA\POSLIVE.MDB"
    CmdText3 = "Copy C:\FIX\POSPEND.MDB \\BNE_REG2\C\IRIS\DATA\POSPEND.MDB"
    CmdText4 = "Copy C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG2\C\REGEMERGE.FLG"
End If
If Option3.Value = True Then
    LogText = MyTime + " T3 - Replace POSLIVE and POSPEND "
    CmdText1 = "Copy \\BNE_REG3\C\IRIS\DATA\POSLIVE.MDB \\BNE_REG3\C\IRIS\LOG\POSLIVE.BAD"
    CmdText2 = "Copy C:\FIX\POSLIVE.MDB \\BNE_REG3\C\IRIS\DATA\POSLIVE.MDB"
    CmdText3 = "Copy C:\FIX\POSPEND.MDB \\BNE_REG3\C\IRIS\DATA\POSPEND.MDB"
    CmdText4 = "Copy C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG3\C\REGEMERGE.FLG"

End If
If Option4.Value = True Then
    LogText = MyTime + " T4 - Replace POSLIVE and POSPEND "
    CmdText1 = "Copy \\BNE_REG4\C\IRIS\DATA\POSLIVE.MDB \\BNE_REG4\C\IRIS\LOG\POSLIVE.BAD"
    CmdText2 = "Copy C:\FIX\POSLIVE.MDB \\BNE_REG4\C\IRIS\DATA\POSLIVE.MDB"
    CmdText3 = "Copy C:\FIX\POSPEND.MDB \\BNE_REG4\C\IRIS\DATA\POSPEND.MDB"
    CmdText4 = "Copy C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG4\C\REGEMERGE.FLG"
End If
If Option5.Value = True Then
    LogText = MyTime + " T5 - Replace POSLIVE and POSPEND "
    CmdText1 = "Copy \\BNE_REG5\C\IRIS\DATA\POSLIVE.MDB \\BNE_REG5\C\IRIS\LOG\POSLIVE.BAD"
    CmdText2 = "Copy C:\FIX\POSLIVE.MDB \\BNE_REG5\C\IRIS\DATA\POSLIVE.MDB"
    CmdText3 = "Copy C:\FIX\POSPEND.MDB \\BNE_REG5\C\IRIS\DATA\POSPEND.MDB"
    CmdText4 = "Copy C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG5\C\REGEMERGE.FLG"

End If
If Option6.Value = True Then
    LogText = MyTime + " T6 - Replace POSLIVE and POSPEND "
    CmdText1 = "Copy \\BNE_REG6\C\IRIS\DATA\POSLIVE.MDB \\BNE_REG6\C\IRIS\LOG\POSLIVE.BAD"
    CmdText2 = "Copy C:\FIX\POSLIVE.MDB \\BNE_REG6\C\IRIS\DATA\POSLIVE.MDB"
    CmdText3 = "Copy C:\FIX\POSPEND.MDB \\BNE_REG6\C\IRIS\DATA\POSPEND.MDB"
    CmdText4 = "Copy C:\BNEAPPS\IRISVERSION.TXT \\BNE_REG6\C\REGEMERGE.FLG"

End If
If Option7.Value = True Then
    LogText = MyTime + " S   -  Replace POSLIVE "
    CmdText1 = "Copy C:\IRIS\DATA\TEMPLATE\POSLIVE.MDB C:\IRIS\DATA\POSLIVE.MDB"
    CmdText2 = "c:\iris\bin\EODIncBusDate.exe  /Database=POSLive.MDB /ServerOnly /SyncWith=c:\iris\data\postrans.mdb /q /PlugIn /NoUI"
    CmdText3 = "Echo."
    CmdText4 = "Echo."
End If

       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
        Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + Mid$(LogText, 21, 32)
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + Mid$(LogText, 21, 32)
            Close 4
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Copy Replace POSLIVE and POSPEND on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
            Print #1, CmdText3
            Print #1, CmdText4
            If Option7.Value = True Then
                Print #1, "Title Copy Replace POSLIVE on Server"
                Print #1, "@Echo Off"
                Print #1, "Echo."
                Print #1, "Echo."
                Print #1, "Echo  ** If the Red Cable is unplugged, plug it in now. **"
                Print #1, "Echo."
                Print #1, "Echo."
                Print #1, "PAUSE"
                Print #1, "CD\IRIS\BIN"
                Print #1, "REGMERGE.EXE"
                Print #1, "cd \iris\setup"
                Print #1, "updatelinks.vbs"
            Else
                Print #1, "Pause"
            End If
      Close #1
      Y = Shell("c:\ih.BAT", vbNormalFocus)
      Update_List1
End Sub

Private Sub Command7_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
If Option7.Value = True Then
    Open "C:\ul.bat" For Output As 1
    Print #1, "cd \iris\setup"
    Print #1, "updatelinks.vbs"
    Close 1
    Y = Shell("C:\ul.bat", vbNormalFocus)
    LogText = MyTime + " S  - Update Links "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
Exit Sub
End If


' INI FILES

If Option1.Value = True Then
    LogText = MyTime + " T1 - Copy INI Files and POSCFG & Shutdown"
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG1\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG1\INI\*.INI \\BNE_REG1\C\IRIS\INI"
    CmdText3 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg1"
    Y = Shell("c:\support\term1_replace.bat", vbNormalFocus)
End If
If Option2.Value = True Then
    LogText = MyTime + " T2 - Copy INI Files and POSCFG & Shutdown"
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG2\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG2\INI\*.INI \\BNE_REG2\C\IRIS\INI"
    CmdText3 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg2"
    Y = Shell("c:\support\term2_replace.bat", vbNormalFocus)
End If
If Option3.Value = True Then
    LogText = MyTime + " T3 - Copy INI Files and POSCFG & Shutdown"
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG3\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG3\INI\*.INI \\BNE_REG3\C\IRIS\INI"
    CmdText3 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg3"
    Y = Shell("c:\support\term3_replace.bat", vbNormalFocus)
End If
If Option4.Value = True Then
    LogText = MyTime + " T4 - Copy INI Files and POSCFG & Shutdown"
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG4\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG4\INI\*.INI \\BNE_REG4\C\IRIS\INI"
    CmdText3 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg4"
    Y = Shell("c:\support\term4_replace.bat", vbNormalFocus)
End If
If Option5.Value = True Then
    LogText = MyTime + " T5 - Copy INI Files and POSCFG & Shutdown"
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG5\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG5\INI\*.INI \\BNE_REG5\C\IRIS\INI"
    CmdText3 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg5"
    Y = Shell("c:\support\term5_replace.bat", vbNormalFocus)
End If
If Option6.Value = True Then
    LogText = MyTime + " T6 - Copy INI Files and POSCFG & Shutdown"
    CmdText1 = "Copy /Y C:\IRIS\REGINFO\COMMON\INI\*.INI \\BNE_REG6\C\IRIS\INI"
    CmdText2 = "Copy /Y C:\IRIS\REGINFO\REG6\INI\*.INI \\BNE_REG6\C\IRIS\INI"
    CmdText3 = "C:\windows\system32\shutdown.exe -r -f -m \\bne_Reg6"
    Y = Shell("c:\support\term6_replace.bat", vbNormalFocus)
End If
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Replace INI Files on Terminal"
            Print #1, CmdText1
            Print #1, CmdText2
            Print #1, CmdText3
            Print #1, "Pause"
      Close #1
      'Y = Shell("c:\ih.BAT", vbNormalFocus)
      Update_List1
End Sub

Private Sub Command8_Click()

' 4.0 Image Setup

    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
If Option1.Value = True Then
    LogText = MyTime + " T1 - 4.0 Reimage Setup "
    CmdText1 = "CALL C:\SUPPORT\GHOST\GHOST1.BAT"
End If
If Option2.Value = True Then
    LogText = MyTime + " T2 - 4.0 Reimage Setup "
    CmdText1 = "CALL C:\SUPPORT\GHOST\GHOST2.BAT"
End If
If Option3.Value = True Then
    LogText = MyTime + " T3 - 4.0 Reimage Setup "
    CmdText1 = "CALL C:\SUPPORT\GHOST\GHOST3.BAT"
End If
If Option4.Value = True Then
    LogText = MyTime + " T4 - 4.0 Reimage Setup "
    CmdText1 = "CALL C:\SUPPORT\GHOST\GHOST4.BAT"
End If
If Option5.Value = True Then
    LogText = MyTime + " T5 - 4.0 Reimage Setup "
    CmdText1 = "CALL C:\SUPPORT\GHOST\GHOST5.BAT"
End If
If Option6.Value = True Then
    LogText = MyTime + " T6 - 4.0 Reimage Setup "
    CmdText1 = "CALL C:\SUPPORT\GHOST\GHOST6.BAT"
End If
If Option7.Value = True Then
    LogText = MyTime + " S   -  40DB.EXE "
    CmdText1 = "C:\FIX\40DB.EXE"
End If
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
      

      Open "c:\ih.BAT" For Output As #1
            Print #1, "Title 4.0 Ghost Reimage Setup"
            Print #1, CmdText1
            'If Option7.Value = False Then
            '    Print #1, "Pause"
            'End If
      Close #1
      

              Open "c:\bneapps\Authorization.dly" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + Mid$(LogText, 21, 32)
            Close 4
            Open "c:\bneapps\Authorization.log" For Append As #4
            If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
                Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 6
                    Input #6, unumber, uname, email, junk
                    Input #6, unumber, uname, email, junk
                Close 6
            End If
            DATENOW = Format(Now(), "MMDDYYYY")
            TIMENOW = Format(Now(), "HH:MM")
            Print #4, Str(unumber) + "," + uname + "," + DATENOW + "," + TIMENOW + "," + Mid$(LogText, 21, 32)
            Close 4
      
      
      
      Y = Shell("c:\ih.BAT", vbNormalFocus)
      Update_List1
End Sub

Private Sub Command9_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
' LOG CHECKDSK
If Option1.Value = True Then
    LogText = MyTime + " T1 - Check Disk "
    CmdText1 = "Copy C:\FIX\KITCHEN.MDB \\BNE_REG1\C\CHKDSK.FLG"
End If
If Option2.Value = True Then
    LogText = MyTime + " T2 - Check Disk  "
    CmdText1 = "Copy C:\FIX\KITCHEN.MDB \\BNE_REG2\C\CHKDSK.FLG"
End If
If Option3.Value = True Then
    LogText = MyTime + " T3 - Check Disk  "
    CmdText1 = "Copy C:\FIX\KITCHEN.MDB \\BNE_REG3\C\CHKDSK.FLG"
End If
If Option4.Value = True Then
    LogText = MyTime + " T4 - Check Disk  "
    CmdText1 = "Copy C:\FIX\KITCHEN.MDB \\BNE_REG4\C\CHKDSK.FLG"
End If
If Option5.Value = True Then
    LogText = MyTime + " T5 - Check Disk  "
    CmdText1 = "Copy C:\FIX\KITCHEN.MDB \\BNE_REG5\C\CHKDSK.FLG"
End If
If Option6.Value = True Then
    LogText = MyTime + " T6 - Check Disk  "
    CmdText1 = "Copy C:\FIX\KITCHEN.MDB \\BNE_REG6\C\CHKDSK.FLG"
End If
       
       Open "c:\ih.BAT" For Output As #1
            Print #1, "Title Setup CHKDSK"
            Print #1, "Echo OFF"
            Print #1, "CLS"
            Print #1, CmdText1
            Print #1, "Echo."
            Print #1, "Echo To start Check Disk verify the copy statement above."
            Print #1, "Echo Then restart the terminal."
            Print #1, "Echo."
            Print #1, "Pause"
      Close #1
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
        Y = Shell("c:\ih.BAT", vbNormalFocus)
      Update_List1
End Sub

Private Sub CopyBIN_Click()
MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
If Option1.Value = True Then
    Y = Shell("c:\fix\bin-1.bat", vbNormalFocus)
    LogText = MyTime + " T1 - BIN Copy "
End If
If Option2.Value = True Then
    Y = Shell("c:\fix\bin-2.bat", vbNormalFocus)
    LogText = MyTime + " T2 - BIN Copy "
End If
If Option3.Value = True Then
    Y = Shell("c:\fix\bin-3.bat", vbNormalFocus)
    LogText = MyTime + " T3 - BIN Copy "
End If
If Option4.Value = True Then
    Y = Shell("c:\fix\bin-4.bat", vbNormalFocus)
    LogText = MyTime + " T4 - BIN Copy "
End If
If Option5.Value = True Then
    Y = Shell("c:\fix\bin-5.bat", vbNormalFocus)
    LogText = MyTime + " T5 - BIN Copy "
End If
If Option6.Value = True Then
    Y = Shell("c:\fix\bin-6.bat", vbNormalFocus)
    LogText = MyTime + " T6 - BIN Copy "
End If

    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
End Sub

Private Sub Day30_Click()
    Update_List1
End Sub

Private Sub Filter_Click()
      Update_List1
End Sub

Private Sub Form_Load()

Command14.Caption = "EDM Dir"
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
If Dir("c:\BNEAPPS\SUPPORT.LOG") = "" Then
       Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, MyTime + " C - Log File Created"
       Close #1
End If

'Read Log
Update_List1
End Sub

Private Sub Option1_Click()
Command6.Enabled = False
Command7.Enabled = True
If Mid$(ServerVersion, 1, 3) = "4.0" Then
    Command8.Enabled = True
End If
Command9.Enabled = False
Command35.Enabled = True
PING.Enabled = True
CopyBIN.Enabled = True
Command6.Caption = "POSLIVE and POSPEND"
Command8.Caption = "4.0 Ghost Reimage Setup"
Command7.Caption = "INIs and POSCFG"
Filter.Value = 1
Update_List1
End Sub

Private Sub Option2_Click()
Command6.Enabled = False
Command7.Enabled = True
If Mid$(ServerVersion, 1, 3) = "4.0" Then
    Command8.Enabled = True
End If
Command9.Enabled = False
Command35.Enabled = True
PING.Enabled = True
CopyBIN.Enabled = True
Command6.Caption = "POSLIVE and POSPEND"
Command8.Caption = "4.0 Ghost Reimage Setup"
Command7.Caption = "INIs and POSCFG"
Filter.Value = 1
Update_List1
End Sub

Private Sub Option3_Click()
Command6.Enabled = False
Command7.Enabled = True
If Mid$(ServerVersion, 1, 3) = "4.0" Then
    Command8.Enabled = True
End If
Command9.Enabled = False
Command35.Enabled = True
PING.Enabled = True
CopyBIN.Enabled = True
Command6.Caption = "POSLIVE and POSPEND"
Command8.Caption = "4.0 Ghost Reimage Setup"
Command7.Caption = "INIs and POSCFG"
Filter.Value = 1
Update_List1
End Sub

Private Sub Option4_Click()
Command6.Enabled = False
Command7.Enabled = True
If Mid$(ServerVersion, 1, 3) = "4.0" Then
    Command8.Enabled = True
End If
Command9.Enabled = False
Command35.Enabled = True
PING.Enabled = True
CopyBIN.Enabled = True
Command6.Caption = "POSLIVE and POSPEND"
Command8.Caption = "4.0 Ghost Reimage Setup"
Command7.Caption = "INIs and POSCFG"
Filter.Value = 1
Update_List1
End Sub

Private Sub Option5_Click()
Command6.Enabled = False
Command7.Enabled = True
If Mid$(ServerVersion, 1, 3) = "4.0" Then
    Command8.Enabled = True
End If
Command9.Enabled = False
Command35.Enabled = True
PING.Enabled = True
CopyBIN.Enabled = True
Command6.Caption = "POSLIVE and POSPEND"
Command8.Caption = "4.0 Ghost Reimage Setup"
Command7.Caption = "INIs and POSCFG"
Filter.Value = 1
Update_List1
End Sub

Private Sub Option6_Click()
Command6.Enabled = False
Command7.Enabled = True
If Mid$(ServerVersion, 1, 3) = "4.0" Then
    Command8.Enabled = True
End If
Command9.Enabled = False
Command35.Enabled = True
PING.Enabled = True
CopyBIN.Enabled = True
Command6.Caption = "POSLIVE and POSPEND"
Command8.Caption = "4.0 Ghost Reimage Setup"
Command7.Caption = "INIs and POSCFG"
Filter.Value = 1
Update_List1
End Sub

Private Sub Option7_Click()
Command6.Enabled = True
Command7.Enabled = True
Command8.Enabled = False
Command9.Enabled = False
PING.Enabled = False
CopyBIN.Enabled = False
Command6.Caption = "POSLIVE"
Command8.Caption = "Future"
Command7.Caption = "Update SQL Links"
Filter.Value = 1
Update_List1
End Sub

Private Sub Option8_Click()
Command6.Enabled = False
Command7.Enabled = False
Command8.Enabled = False
Command9.Enabled = False
Command35.Enabled = False
PING.Enabled = False
CopyBIN.Enabled = False
Command6.Caption = "POSLIVE and POSPEND"
Command8.Caption = "4.0 Ghost Reimage Setup"
Command7.Caption = "INI files (Common and Reg#)"
Filter.Value = 0
Update_List1
End Sub

Private Sub Payroll_Click()
      Y = Shell("c:\IRIS\BIN\DBEDITOR.EXE C:\IRIS\DATA\PAYROLL.MDB", vbNormalFocus)
End Sub

Private Sub PING_Click()
If Option1.Value = True Then
    CmdText1 = "PING 100.100.100.101"
End If
If Option2.Value = True Then
    CmdText1 = "PING 100.100.100.102"
End If
If Option3.Value = True Then
    CmdText1 = "PING 100.100.100.103"
End If
If Option4.Value = True Then
    CmdText1 = "PING 100.100.100.104"
End If
If Option5.Value = True Then
    CmdText1 = "PING 100.100.100.105"
End If
If Option6.Value = True Then
    CmdText1 = "PING 100.100.100.106"
End If
      Open "c:\ih.BAT" For Output As #1
                Print #1, "Title PING"
                Print #1, CmdText1
                Print #1, "Pause"
      Close #1
    Y = Shell("c:\ih.bat", vbNormalFocus)
End Sub

Private Sub Printers_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "control printers"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Printers "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Serv_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "services.msc"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Services "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub

Private Sub Tasks_Click()
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    Open "C:\ul.bat" For Output As 1
    Print #1, "control schedtasks"
    Close 1
    Y = Shell("C:\ul.bat", vbHide)
    LogText = MyTime + " S  - Accessed Tasks "
    Open "c:\BNEAPPS\SUPPORT.LOG" For Append As #1
        Print #1, LogText
       Close #1
    Update_List1
Exit Sub
End Sub
