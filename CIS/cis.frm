VERSION 5.00
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFF00&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   1020
   ClientLeft      =   4605
   ClientTop       =   2745
   ClientWidth     =   4710
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   FillColor       =   &H00FF0000&
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00400000&
   Icon            =   "cis.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1020
   ScaleWidth      =   4710
   Visible         =   0   'False
   WindowState     =   1  'Minimized
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFF00&
      BorderStyle     =   0  'None
      FillColor       =   &H00FFFF00&
      ForeColor       =   &H00FFFF00&
      Height          =   480
      Left            =   240
      Picture         =   "cis.frx":000C
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   2
      Top             =   240
      Width           =   480
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   240
      Top             =   3120
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFF00&
      Caption         =   " "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   4200
      TabIndex        =   1
      Top             =   360
      Width           =   375
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   $"cis.frx":0316
      ForeColor       =   &H00FF0000&
      Height          =   735
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   4215
   End
   Begin VB.Menu exitprog 
      Caption         =   "&Exit"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

      Private Const NULLPTR = 0&
      ' Constants for DEVMODE
      Private Const CCHDEVICENAME = 32
      Private Const CCHFORMNAME = 32
      ' Constants for DocumentProperties
      Private Const DM_MODIFY = 8
      Private Const DM_COPY = 2
      Private Const DM_IN_BUFFER = DM_MODIFY
      Private Const DM_OUT_BUFFER = DM_COPY
      ' Constants for dmOrientation
      Private Const DMORIENT_PORTRAIT = 1
      Private Const DMORIENT_LANDSCAPE = 2
      ' Constants for dmPrintQuality
      Private Const DMRES_DRAFT = (-1)
      Private Const DMRES_HIGH = (-4)
      Private Const DMRES_LOW = (-2)
      Private Const DMRES_MEDIUM = (-3)
      ' Constants for dmTTOption
      Private Const DMTT_BITMAP = 1
      Private Const DMTT_DOWNLOAD = 2
      Private Const DMTT_DOWNLOAD_OUTLINE = 4
      Private Const DMTT_SUBDEV = 3
      ' Constants for dmColor
      Private Const DMCOLOR_COLOR = 2
      Private Const DMCOLOR_MONOCHROME = 1
      ' Constants for dmCollate
      Private Const DMCOLLATE_FALSE = 0
      Private Const DMCOLLATE_TRUE = 1
      Private Const DM_COLLATE As Long = &H8000
      ' Constants for dmDuplex
      Private Const DM_DUPLEX = &H1000&
      Private Const DMDUP_HORIZONTAL = 3
      Private Const DMDUP_SIMPLEX = 1
      Private Const DMDUP_VERTICAL = 2

      Private Type DEVMODE
          dmDeviceName(1 To CCHDEVICENAME) As Byte
          dmSpecVersion As Integer
          dmDriverVersion As Integer
          dmSize As Integer
          dmDriverExtra As Integer
          dmFields As Long
          dmOrientation As Integer
          dmPaperSize As Integer
          dmPaperLength As Integer
          dmPaperWidth As Integer
          dmScale As Integer
          dmCopies As Integer
          dmDefaultSource As Integer
          dmPrintQuality As Integer
          dmColor As Integer
          dmDuplex As Integer
          dmYResolution As Integer
          dmTTOption As Integer
          dmCollate As Integer
          dmFormName(1 To CCHFORMNAME) As Byte
          dmUnusedPadding As Integer
          dmBitsPerPel As Integer
          dmPelsWidth As Long
          dmPelsHeight As Long
          dmDisplayFlags As Long
          dmDisplayFrequency As Long
                
      End Type

      Private Declare Function OpenPrinter Lib "winspool.drv" Alias _
      "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, _
      ByVal pDefault As Long) As Long

      Private Declare Function DocumentProperties Lib "winspool.drv" _
      Alias "DocumentPropertiesA" (ByVal hwnd As Long, _
      ByVal hPrinter As Long, ByVal pDeviceName As String, _
      pDevModeOutput As Any, pDevModeInput As Any, ByVal fMode As Long) _
      As Long

      Private Declare Function ClosePrinter Lib "winspool.drv" _
      (ByVal hPrinter As Long) As Long

      Private Declare Sub CopyMemory Lib "KERNEL32" Alias "RtlMoveMemory" _
      (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)

Function GetPrinterSettings(szPrinterName As String, hdc As Long) _
      As Boolean
      Dim hPrinter As Long
      Dim nSize As Long
      Dim pDevMode As DEVMODE
      Dim aDevMode() As Byte
      Dim TempStr As String
      GetPrinterSettings = True
        If OpenPrinter(szPrinterName, hPrinter, NULLPTR) <> 0 Then
           nSize = DocumentProperties(NULLPTR, hPrinter, szPrinterName, _
           NULLPTR, NULLPTR, 0)
          If nSize < 1 Then
            GetPrinterSettings = False
            Exit Function
          End If
          ReDim aDevMode(1 To nSize)
          nSize = DocumentProperties(NULLPTR, hPrinter, szPrinterName, _
          aDevMode(1), NULLPTR, DM_OUT_BUFFER)
          If nSize < 0 Then
            GetPrinterSettings = False
            Exit Function
          End If
        Else
         GetPrinterSettings = False
      End If
End Function


Private Sub about_Click()
MsgBox "                 Written by Johnny Mitchell                       Contact Paula at extension 1355 for assistance.", 64, "About"

End Sub

Private Sub exitprog_Click()
End
End Sub

Private Sub Form_Load()
Call Start_Form
End Sub
Private Sub Start_Form()
On Error GoTo ErrorMessage
J = 0
M = 0
I = 15
'Form1.Icon = LoadPicture("C:\WINDOWS\MAIL16A.ICO")
Form1.WindowState = 1
'FORM1.Show
STARTPROG:
    Call OPENFILE
    If GOODFILE = 9 Then
        Call PROCESSFILE
        Call ProcessExport
    End If
    Timer1.Enabled = True
Exit Sub
ErrorMessage:
MsgBox "An Error has occured. Please call HelpDesk, Ext 1767. Please advise her that CIS program has an error of " & ErrorMess
End Sub

Private Sub OPENFILE()
If Dir("T:\CIS_FORM\") <> "" Then
   ErrorMess = "Problem with opening T:\CIS_FORM\CIS_FORM.V02"
Else
   MsgBox "ERROR...Network is down... T:\CIS_FORM\  Call HelpDesk at Ext. 1767"
   End
End If
If Dir("S:\Access\") <> "" Then
   ErrorMess = "Problem with opening T:\CIS_FORM\CIS_FORM.V02"
Else
   MsgBox "ERROR...Network is down... S:\Access\  Call HelpDesk at Ext. 1767"
   End
End If
If Dir("U:\") <> "" Then
   ErrorMess = "Problem with opening T:\CIS_FORM\CIS_FORM.V02"
Else
   MsgBox "ERROR...Network is down...U:\  Call HelpDesk at Ext. 1767"
   End
End If

If GetPrinterSettings(Printer.DeviceName, Printer.hdc) = False Then
   MsgBox "ERROR...Printer not Available   Call HelpDesk at Ext. 1767"
   End
End If

If Dir("T:\CIS_FORM\CIS_FORM.V02") <> "" Then
   Open "T:\CIS_FORM\CIS_FORM.V02" For Input As 1
   Close #1
      GOODFILE = 9
Else
   GOODFILE = 8
End If
End Sub

Private Sub PROCESSFILE()
Dim DATASET
Dim L As Integer
'************ TO SETUP TEST T: & U: RUN OS/2 CMD FILE
'************ C:\MAPTU.CMD

Dim CurrentApp As Recordset
Dim RePrintApp As Recordset
ErrorMess = "Problem with opening S:\Access\es_be.mdb"
Dim ESBEdb As Database
Dim tblCurrentApp As Recordset
Dim TblInterview As Recordset
Dim TblPrimaryInterview As Recordset
Dim TblJody As Recordset
Dim LastInterviewer As String
Dim FirstInterviewer As String
Dim LastRecord As String
Dim J As Integer
Dim K As Integer
Dim tblUnit As Recordset
Dim LastUnit As String
Dim DistrictNum As String
Dim LineData As String
Dim CRLF As String
Dim CUR As Integer
J = 0
Set ESBEdb = DBEngine.Workspaces(0).OpenDatabase("S:\Access\ES_BE.mdb")
Set tblCurrentApp = ESBEdb.OpenRecordset("Current Applications", dbOpenTable)
Set TblPrimaryInterview = ESBEdb.OpenRecordset("Primary Interviewer", dbOpenTable)
Set TblInterview = ESBEdb.OpenRecordset("Interviewer", dbOpenTable)
Set tblUnit = ESBEdb.OpenRecordset("UnitMaster", dbOpenTable)
Set TblJody = ESBEdb.OpenRecordset("Rest Import PI", dbOpenTable)
K = 1

TblInterview.MoveFirst
Do Until TblInterview.EOF
    TempInterviewer(K) = TblInterview.Fields("ESInterviewer").Value
    TblInterview.MoveNext
    MaxRecords = K
    K = K + 1
Loop

Timer1.Enabled = False
'FORM1.Icon = LoadPicture("C:\WINDOWS\MAIL16B.ICO")
'FORM1.WindowState = 1


ErrorMess = "Problem with opening C:\CIS_FORM\CIS_FORM.V02 or C:\CIS_FORM\CIS_FORM.PRE"
    FileCopy "T:\CIS_FORM\CIS_FORM.V02", "T:\CIS_FORM\CIS_FORM.PRE"
    Kill "T:\CIS_FORM\CIS_FORM.V02"
LineData = "Junk"
    'Open "U:\CIS_FORM.V02" For Append Access Write As 6
    'Open "T:\CIS_FORM\CIS_FORM.PRE" For Input Access Read As 5
    Open "U:\CIS_FORM.V02" For Append Access Write As 6
    Open "T:\CIS_FORM\CIS_FORM.PRE" For Input Access Read As 5
    Do While Not EOF(5)
    If LineData <> "" Then
        LineData = Input(1498, 5)
        CRLF = Input(2, 5)
        Print #6, LineData
        LineData = " "
    Else
       Exit Do
    End If
    Loop
    Close 5, 6


'   PROCESS FILE
ErrorMess = "Problem with data in T:\CIS_FORM\CIS_FORM.PRE"
    
    'Open "T:\CIS_FORM\CIS_FORM.PRE" For Input As 1
    Open "T:\CIS_FORM\CIS_FORM.PRE" For Input As 1
    CUR = 0
    Do While Not EOF(1)
    CurrentInterviewer = ""
    PrimaryInterviewer = ""

'    FORM2.Show
    CREATETIME = Input$(32, 1)
    CREATEUSERID = Input$(6, 1)
    posttime = Input$(32, 1)
    node = Input$(9, 1)
    User = Input$(9, 1)
    DESTUSER = Input$(9, 1)
    DATASETTYPE = Input$(5, 1)
    unitnumber = Input$(5, 1)
    unitname = Input$(21, 1)
    DateCreated = Input$(9, 1)
    completedby = Input$(21, 1)
    interdate = Input$(9, 1)
    intertime = Input$(7, 1)
    APPLICATION = Input$(2, 1)
    Release = Input$(2, 1)
    SALESREP = Input$(2, 1)
    biscmaker = Input$(2, 1)
    storeclerk = Input$(2, 1)
    OUTSIDE = Input$(2, 1)
    HOSTESS = Input$(2, 1)
    BACKLINE = Input$(2, 1)
    IntPromo = Input$(2, 1)
    NewHire = Input$(2, 1)
    Company(1) = Input$(26, 1)
    Contact(1) = Input$(26, 1)
    Phone(1) = Input$(11, 1)
    Position(1) = Input$(26, 1)
    Reason(1) = Input$(31, 1)
    Fromdate(1) = Input$(9, 1)
    Todate(1) = Input$(9, 1)
    Rate(1) = Input$(17, 1)
    Company(2) = Input$(26, 1)
    Contact(2) = Input$(26, 1)
    Phone(2) = Input$(11, 1)
    Position(2) = Input$(26, 1)
    Reason(2) = Input$(31, 1)
    Fromdate(2) = Input$(9, 1)
    Todate(2) = Input$(9, 1)
    Rate(2) = Input$(17, 1)
    Company(3) = Input$(26, 1)
    Contact(3) = Input$(26, 1)
    Phone(3) = Input$(11, 1)
    Position(3) = Input$(26, 1)
    Reason(3) = Input$(31, 1)
    Fromdate(3) = Input$(9, 1)
    Todate(3) = Input$(9, 1)
    Rate(3) = Input$(17, 1)
    Company(4) = Input$(26, 1)
    Contact(4) = Input$(26, 1)
    Phone(4) = Input$(11, 1)
    Position(4) = Input$(26, 1)
    Reason(4) = Input$(31, 1)
    Fromdate(4) = Input$(9, 1)
    Todate(4) = Input$(9, 1)
    Rate(4) = Input$(17, 1)
    Persname(1) = Input$(26, 1)
    Persphone(1) = Input$(11, 1)
    Persworkphone(1) = Input$(11, 1)
    Persbesttime(1) = Input$(26, 1)
    Persname(2) = Input$(26, 1)
    Persphone(2) = Input$(11, 1)
    Persworkphone(2) = Input$(11, 1)
    Persbesttime(2) = Input$(26, 1)
    Persname(3) = Input$(26, 1)
    Persphone(3) = Input$(11, 1)
    Persworkphone(3) = Input$(11, 1)
    Persbesttime(3) = Input$(26, 1)
    firstname = Input$(31, 1)
    initial = Input$(2, 1)
    lastname = Input$(31, 1)
    address1 = Input$(31, 1)
    ADDRESS2 = Input$(31, 1)
    City = Input$(31, 1)
    State = Input$(3, 1)
    zip = Input$(10, 1)
    applphone = Input$(11, 1)
    applssn = Input$(10, 1)
    COMMENT0 = Input$(51, 1)
    COMMENT1 = Input$(51, 1)
    COMMENT2 = Input$(51, 1)
    COMMENT3 = Input$(51, 1)
    SchoolName = Input$(26, 1)
    SchoolContact = Input$(21, 1)
    Schoolphone = Input$(11, 1)
    lastdate = Input$(9, 1)
    CRLF = Input$(2, 1)
ErrorMess = "Problem with opening S:\Access\ES_BE.mdb(Current Applications)"
'Add to Databases
If Dir("U:\Intervi.int") <> "" Then
   Open "U:\Intervi.int" For Input As #12
      Input #12, CurrentInterviewer
   Close #12
   CurrentInterviewer = Trim(CurrentInterviewer)
   For K = 1 To MaxRecords
      If K = MaxRecords Then
         CurrentInterviewer = TempInterviewer(1)
         Exit For
      Else
         If CurrentInterviewer = Trim(TempInterviewer(K)) Then
            CurrentInterviewer = TempInterviewer(K + 1)
            Exit For
         End If
      End If
   Next K
Else
   Open "U:\Intervi.int" For Output As #12
      Print #12, TempInterviewer(1)
   Close #12
End If


Open "U:\Intervi.int" For Output As #12
   Print #12, CurrentInterviewer
Close #12

Open "U:\Inter.tst" For Output As #12
      Print #12, CurrentInterviewer
Close #12

ErrorMess = "Problem with opening S:\Access\ES_BE.mdb(Current Applicatios)"
tblCurrentApp.AddNew
If Val(Trim(unitnumber)) > 0 Then
   tblCurrentApp.Fields("Unit").Value = Val(Trim(unitnumber))
Else
   If Val(Trim(node)) > 0 Then
      tblCurrentApp.Fields("Unit").Value = Val(Trim(node))
   End If
End If
If Val(Trim(applssn)) > 0 Then
   tblCurrentApp.Fields("SSN").Value = Format(applssn, "000000000")
Else
  tblCurrentApp.Fields("SSN").Value = "000000000"
End If
PrimaryInterviewer = "N/A"
DistrictNum = 0

TblJody.MoveFirst
Do Until tblUnit.EOF
   If Trim(TblJody.Fields("Unit").Value) = Trim(node) Then
      PrimaryInterviewer = Trim(TblJody.Fields("Primary Interviewer").Value)
      Exit Do
   Else
      If Trim(TblJody.Fields("Unit").Value) = "9999" Then
        Exit Do
      End If
   End If
   TblJody.MoveNext
Loop
If PrimaryInterviewer = "N/A" Then
    PrimaryInterviewer = "NC"
    CurrentInterviewer = "NC"
End If

'tblCurrentApp.Fields("SSN").Value = Trim(Str(Format(applssn, "000000000")))
tblCurrentApp.Fields("Last Name").Value = Trim(lastname)
tblCurrentApp.Fields("First Name").Value = Trim(firstname)
tblCurrentApp.Fields("initial").Value = Trim(initial)
tblCurrentApp.Fields("ES Interviewer").Value = CurrentInterviewer
tblCurrentApp.Fields("XMail").Value = Trim(User)
tblCurrentApp.Update

ErrorMess = "Problem with opening S:\Access\ES_BE.mdb(UnitMaster)"

'TblPrimaryInterview.MoveFirst
'Do Until TblPrimaryInterview.EOF
'    If Val(TblPrimaryInterview.Fields("District").Value) = Val(Trim(DistrictNum)) Then
'        PrimaryInterviewer = TblPrimaryInterview.Fields("Interviewer Initials").Value
'        Exit Do
'    End If
'TblPrimaryInterview.MoveNext
'Loop

ErrorMess = "Problem with PrintOut(FirstPage)"
PrintFirstPage
ErrorMess = "Problem with PrintOut(LastPage)"
PrintLastPage
'PrintPersonalReference
GOODFILE = 8
J = 0
M = 0
Loop
Close #1
End Sub

Private Sub TIMER1_TIMER()
   Static I As Integer
   I = I + 1
 '  LABEL2.Caption = Str$(I)
   If I = 5 Then
        I = 0
        Timer1.Enabled = False
        Call Start_Form
   End If

End Sub

Public Sub PrintFirstPage()
Dim K As Integer
Dim L As Single
L = 2900
Dim TempHour As String
Dim TempMinute As String
Dim AMPM As String
If Val(Left(intertime, 2)) > 11 Then
   AMPM = "PM"
Else
   AMPM = "AM"
End If
TempHour = "00"
TempMinute = "00"
If Val(Left(intertime, 2)) < 13 Then
   TempHour = Val(Left(intertime, 2))
ElseIf Val(Left(intertime, 2)) < 20 Then
   TempHour = Val(Left(intertime, 2)) - 12
End If
'TempMinute = Val(Mid(intertime, 3, 3))
L = 2900
'Line 1
   Printer.FontBold = False
   Printer.FontSize = 8
   Printer.CurrentX = 100
   Printer.CurrentY = 100
   Printer.Print Format(Now, "mm/dd/yyyy") & " " & Format(Now, "hh:mm AM/PM")
   Printer.FontSize = 10
   Printer.CurrentX = 3500
   Printer.CurrentY = 100
   Printer.Print "BNE - ES APPLICANT FORM"
   Printer.FontSize = 8
   Printer.CurrentX = 9200
   Printer.CurrentY = 100
   Printer.Print Format(interdate, "##/##/####") & " " & TempHour & ":" & TempMinute & " " & AMPM
   
'Line 2
   Printer.FontBold = False
   Printer.FontSize = 12
   Printer.CurrentX = 100
   Printer.CurrentY = 500
   Printer.Print "E.S. Int: "
   Printer.FontBold = True
   Printer.CurrentX = 1500
   Printer.CurrentY = 500
   Printer.Print Trim(CurrentInterviewer)
   Printer.CurrentX = 3000
   Printer.CurrentY = 500
   If IsNumeric(Right(Trim(User), 4)) Then
      Printer.Print Left(unitname, 25)
   Else
      Printer.Print User
   End If
   Printer.CurrentX = 6000
   Printer.CurrentY = 500
   Printer.Print Left(unitnumber, 25)
   Printer.CurrentX = 8000
   Printer.CurrentY = 500
   Printer.FontBold = False
   Printer.Print "Pr:"
   Printer.CurrentX = 8500
   Printer.CurrentY = 500
   Printer.FontBold = True
   Printer.Print PrimaryInterviewer
   Printer.CurrentX = 10200
   Printer.CurrentY = 500
   Printer.FontBold = False
   Printer.FontSize = 10
   Printer.Print "Page 1"
   
'Line 3
   Printer.FontBold = False
   Printer.CurrentX = 100
   Printer.CurrentY = 900
   Printer.Print "App:"
   Printer.FontBold = True
   Printer.FontSize = 12
   Printer.CurrentX = 800
   Printer.CurrentY = 900
   Printer.Print Trim(firstname) & " " & Trim(initial) & " " & Trim(lastname)
   Printer.CurrentX = 5000
   Printer.CurrentY = 900
   Printer.Print Format(applssn, "###-##-####")
   Printer.CurrentX = 7200
   Printer.CurrentY = 1000
   Printer.FontSize = 10
   Printer.FontBold = False
   Printer.Print "CI:___________________________"
   Printer.CurrentX = 7200
   Printer.CurrentY = 1300
   Printer.Print "Grade   -  OK  +"
   Printer.DrawWidth = 6
   Printer.Line (7000, 900)-(11000, 900)
   Printer.Line (7000, 900)-(7000, 2400)
   Printer.Line (11000, 900)-(11000, 2400)
   Printer.Line (7000, 2400)-(11000, 2400)
   
   'Line 4
   Printer.FontBold = False
   Printer.CurrentX = 100
   Printer.CurrentY = 1300
   Printer.Print "Phone/Addr.:"
   Printer.CurrentX = 1500
   Printer.CurrentY = 1300
   Printer.Print Trim(Format(applphone, "(###)###-####"))
   Printer.CurrentX = 4000
   Printer.CurrentY = 1300
   Printer.Print Trim(City) & " " & Trim(State)
   
   'Line 5
   Printer.CurrentX = 100
   Printer.CurrentY = 1700
   Printer.Print "ES/App:___________________________________________________"
   Printer.CurrentX = 7200
   Printer.CurrentY = 1600
   Printer.Print "Decision:______________________"
   Printer.CurrentX = 7200
   Printer.CurrentY = 1900
   Printer.Print "Comment:_____________________"
    
   'Line 6
   Printer.CurrentX = 100
   Printer.CurrentY = 2100
   Printer.Print "_________________________________________________________"
    
   'Line 7
   Printer.CurrentX = 100
   Printer.CurrentY = 2500
   Printer.Print "_________________________________________________________"
   Printer.CurrentX = 7000
   Printer.CurrentY = 2500
   Printer.FontBold = True
   Printer.Print "H. S. Reference:   Att:"
   Printer.FontBold = False
   Printer.CurrentX = 9200
   Printer.CurrentY = 2500
   Printer.Print "E G F P"
    
   'Line 8
   Printer.CurrentX = 100
   Printer.CurrentY = 2900
   Printer.Print "_________________________________________________________"
   Printer.FontBold = True
   Printer.CurrentX = 7000
   Printer.CurrentY = 2900
   Printer.Print "Dis:"
   Printer.FontBold = False
   Printer.CurrentX = 7500
   Printer.CurrentY = 2900
   Printer.Print "E G F P"
   Printer.FontBold = True
   Printer.CurrentX = 8400
   Printer.CurrentY = 2900
   Printer.Print "Rec:"
   Printer.FontBold = False
   Printer.CurrentX = 9000
   Printer.CurrentY = 2900
   Printer.Print "Y N (?)"
    
    
   'Line 9
   Printer.CurrentX = 100
   Printer.CurrentY = 3400
   Printer.Print "GM Comments:"
   Printer.CurrentX = 1650
   Printer.CurrentY = 3400
   Printer.Print Left(Trim(COMMENT0), 45)
   Printer.CurrentX = 7000
   Printer.CurrentY = 3400
   Printer.Print "____________________________________"
    
   'Line 10
   Printer.CurrentX = 100
   Printer.CurrentY = 3800
   Printer.Print Left(Trim(COMMENT1), 50)
   Printer.CurrentX = 7000
   Printer.CurrentY = 3900
   Printer.Print "____________________________________"
   
   'Line 11
   Printer.CurrentX = 100
   Printer.CurrentY = 4175
   Printer.Print Left(Trim(COMMENT2), 50)
   Printer.CurrentX = 7000
   Printer.CurrentY = 4400
   Printer.Print "____________________________________"
   
   'Line 11.5
   Printer.CurrentX = 100
   Printer.CurrentY = 4450
   Printer.Print Left(Trim(COMMENT3), 50)
    
   'Line 12
   Printer.CurrentX = 100
   Printer.CurrentY = 4900
   Printer.FontBold = True
   Printer.FontUnderline = True
   Printer.FontSize = 12
   Printer.Print "SCHOOL"
   Printer.FontSize = 10
   Printer.CurrentX = 1600
   Printer.CurrentY = 4900
   Printer.FontUnderline = False
   Printer.FontBold = False
   Printer.Print "3yr+ Y N"
   'Printer.Print Format(lastdate, "##/##/####")
   Printer.CurrentX = 3000
   Printer.CurrentY = 4900
   Printer.Print Trim(SchoolName)
   Printer.CurrentX = 7000
   Printer.CurrentY = 4900
   Printer.Print "____________________________________"
   
   'Line 13
   Printer.CurrentX = 100
   Printer.CurrentY = 5400
   Printer.FontBold = True
   Printer.Print "Grade:_______"
   Printer.FontBold = False
   Printer.CurrentX = 2500
   Printer.CurrentY = 5400
   Printer.Print "Phone: "
   Printer.CurrentX = 3600
   Printer.CurrentY = 5400
   Printer.FontBold = True
   Printer.Print Format(Schoolphone, "(###) ###-####")
   Printer.FontBold = False
   Printer.CurrentX = 7000
   Printer.CurrentY = 5400
   Printer.Print "____________________________________"
    
   'Line 14
   Printer.CurrentX = 100
   Printer.CurrentY = 5900
   Printer.FontBold = True
   Printer.Print "Release?                                 Counselor:_______________________"
   Printer.CurrentX = 1000
   Printer.CurrentY = 5900
   Printer.FontBold = False
   Printer.Print "Y  N (?)__________"
   Printer.CurrentX = 7000
   Printer.CurrentY = 5900
   Printer.Print "____________________________________"
   
   
   'Line 15
   Printer.CurrentX = 100
   Printer.CurrentY = 6400
   Printer.FontBold = True
   Printer.FontSize = 12
   Printer.FontUnderline = True
   Printer.Print "PREINTERVIEW QUESTIONS"

   'Line 16
   Printer.CurrentX = 100
   Printer.CurrentY = 6900
   Printer.FontBold = False
   Printer.FontSize = 10
   Printer.FontUnderline = False
   Printer.Print "Theft:  Report?  Y N  Why?__________________________________________________________________________"

   'Line 17
   Printer.CurrentX = 100
   Printer.CurrentY = 7300
   Printer.Print "Drugs:  Report?  Y N  Why?_________________________________________________________________________"

   'Line 18
   Printer.CurrentX = 100
   Printer.CurrentY = 7700
   Printer.Print "Exp.Drugs: Y N  (Mo/Yr)/What:_______________________________________________________________________"

   'Line 19
   Printer.CurrentX = 100
   Printer.CurrentY = 8100
   Printer.Print "Felony: N Y  (When/What/Jail/Probation?)______________________________________________________________"

   'Line 20
   Printer.CurrentX = 100
   Printer.CurrentY = 8500
   Printer.Print "________________________________________________________________________________________________"

   'Line21
   Printer.CurrentX = 100
   Printer.CurrentY = 8900
   Printer.Print "Int?  Y N (1-800 #?)________________________________________________________________________________"
   
   'Line 22
   Printer.CurrentX = 100
   Printer.CurrentY = 9400
   Printer.FontBold = True
   Printer.FontUnderline = True
   Printer.FontSize = 12
   Printer.Print "WORK REFERENCES:"
   Printer.CurrentX = 2800
   Printer.CurrentY = 9400
   Printer.FontBold = False
   Printer.FontUnderline = False
   Printer.FontSize = 10
   Printer.Print "(Attach additional sheet if necessary)"
   Printer.CurrentX = 6700
   Printer.CurrentY = 9400
   'Printer.FontBold = True
   Printer.Print "H's  Y N  Tx  Y N  BBQ  Y N  BNE  Y N"
   'Printer.FontBold = False
   'Printer.CurrentX = 7100
   'Printer.CurrentY = 9400
   'Printer.Print "Y N"
   'Printer.CurrentX = 8000
   'Printer.CurrentY = 9400
   'Printer.FontBold = True
   'Printer.Print "BNE"
   'Printer.FontBold = False
   'Printer.CurrentX = 8600
   'Printer.CurrentY = 9400
   'Printer.Print "Y N"
   
    
L = 7400
For K = 1 To 2
   L = L + 2600
   If Trim(Company(K)) <> "" Then
   Printer.CurrentX = 100
   Printer.CurrentY = L
   Printer.FontBold = True
   Printer.Print Company(K)
   Printer.CurrentX = 3100
   Printer.CurrentY = L
   Printer.FontBold = False
   Printer.Print "Phone:"
   Printer.FontBold = True
   Printer.CurrentX = 3900
   Printer.CurrentY = L
   Printer.Print Format(Trim(Phone(K)), "(###) ###-####")
   Printer.CurrentX = 6100
   Printer.CurrentY = L
   Printer.FontUnderline = False
   Printer.Print "Dates correct?"
   Printer.CurrentX = 8000
   Printer.CurrentY = L
   Printer.FontBold = False
   Printer.Print "Y  N (?)____________________"
   Printer.CurrentX = 100
   Printer.CurrentY = L + 350
   Printer.Print "From:"
   Printer.CurrentX = 800
   Printer.CurrentY = L + 350
   Printer.Print Format(Fromdate(K), "00/00/0000")
   Printer.CurrentX = 2250
   Printer.CurrentY = L + 350
   Printer.Print "To:"
   Printer.CurrentX = 2750
   Printer.CurrentY = L + 350
   Printer.Print Format(Todate(K), "00/00/0000")
   Printer.CurrentX = 4200
   Printer.CurrentY = L + 350
   Printer.Print "(Call? Y N)"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 350
   Printer.FontBold = True
   Printer.Print "Att:"
   Printer.FontBold = False
   Printer.CurrentX = 6700
   Printer.CurrentY = L + 350
   Printer.Print "E G F P"
   Printer.CurrentX = 7600
   Printer.CurrentY = L + 350
   Printer.FontBold = True
   Printer.Print "Per:"
   Printer.FontBold = False
   Printer.CurrentX = 8100
   Printer.CurrentY = L + 350
   Printer.Print "E G F P"
   Printer.CurrentX = 9000
   Printer.CurrentY = L + 350
   Printer.FontBold = True
   Printer.Print "Atd:"
   Printer.FontBold = False
   Printer.CurrentX = 9500
   Printer.CurrentY = L + 350
   Printer.Print "E G F P"
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 700
   Printer.Print "Supervisor:"
   Printer.CurrentX = 1500
   Printer.CurrentY = L + 700
   Printer.Print Trim(Contact(K))
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 700
   Printer.FontBold = True
   Printer.Print "Notice:"
   Printer.CurrentX = 7000
   Printer.CurrentY = L + 700
   Printer.FontBold = False
   Printer.Print "Y  N"
   Printer.CurrentX = 7600
   Printer.CurrentY = L + 700
   Printer.FontBold = True
   Printer.Print "Rehire:"
   Printer.CurrentX = 8500
   Printer.CurrentY = L + 700
   Printer.FontBold = False
   Printer.Print "Y  N"
   
   
   'New Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1050
   Printer.Print "Positon:"
   Printer.CurrentX = 1600
   Printer.CurrentY = L + 1050
   Printer.Print Trim(Position(K))
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1050
   Printer.Print "____________________________________________"
   
   
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1400
   Printer.Print "Reason Left:"
   Printer.CurrentX = 1600
   Printer.CurrentY = L + 1400
   Printer.Print Trim(Reason(K))
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1400
   Printer.Print "____________________________________________"
   
   'Next Line
   Printer.FontBold = True
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1750
   Printer.Print "Notice"
   Printer.FontBold = False
   Printer.CurrentX = 1000
   Printer.CurrentY = L + 1750
   Printer.Print "Y N"
   Printer.FontBold = True
   Printer.CurrentX = 1500
   Printer.CurrentY = L + 1750
   Printer.Print "Why?"
   Printer.FontBold = False
   Printer.CurrentX = 2100
   Printer.CurrentY = L + 1750
   Printer.Print "_________________________________"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1750
   Printer.Print "____________________________________________"
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 2100
   Printer.Print "___________________________________________________"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 2100
   Printer.Print "____________________________________________"
End If
Next K
Printer.EndDoc
End Sub

Public Sub PrintLastPage()
Dim K As Integer
Dim L As Single
Dim N As Single
N = 0
Printer.FontSize = 10
Printer.CurrentX = 10000
Printer.CurrentY = 100
Printer.FontBold = False
Printer.Print "Page 2"
Printer.CurrentX = 4800
Printer.CurrentY = 100
Printer.FontBold = True
Printer.Print Left(unitnumber, 4) & "  " & Left(unitname, 25)
Printer.CurrentX = 4300
Printer.CurrentY = 300
Printer.Print Trim(Left(firstname, 12)) & "  " & Trim(Left(lastname, 18)) & "   " & Trim(Left(applssn, 9))

If Trim(Company(3)) <> "" Then
   Printer.FontUnderline = True
   Printer.CurrentX = 100
   Printer.CurrentY = 700
   Printer.FontSize = 12
   Printer.Print "WORK REFERENCES"
   Printer.FontUnderline = False
   Printer.FontBold = False
   Printer.FontSize = 10
   Printer.CurrentX = 2600
   Printer.CurrentY = 700
   Printer.Print "(Continued)"
End If

L = 1200
For K = 3 To 4
   If Trim(Company(K)) <> "" Then
   Printer.CurrentX = 100
   Printer.CurrentY = L
   Printer.FontBold = True
   Printer.Print Company(K)
   Printer.CurrentX = 3000
   Printer.CurrentY = L
   Printer.FontBold = False
   Printer.Print "Phone:"
   Printer.FontBold = True
   Printer.CurrentX = 3800
   Printer.CurrentY = L
   Printer.Print Format(Trim(Phone(K)), "(###) ###-####")
   Printer.CurrentX = 6100
   Printer.CurrentY = L
   Printer.FontUnderline = False
   Printer.Print "Dates correct?"
   Printer.CurrentX = 8000
   Printer.CurrentY = L
   Printer.FontBold = False
   Printer.Print "Y  N (?)____________________"
   Printer.CurrentX = 100
   Printer.CurrentY = L + 350
   Printer.Print "From:"
   Printer.CurrentX = 800
   Printer.CurrentY = L + 350
   Printer.Print Format(Fromdate(K), "00/00/0000")
   Printer.CurrentX = 2250
   Printer.CurrentY = L + 350
   Printer.Print "To:"
   Printer.CurrentX = 2750
   Printer.CurrentY = L + 350
   Printer.Print Format(Todate(K), "00/00/0000")
   Printer.CurrentX = 4200
   Printer.CurrentY = L + 350
   Printer.Print "(Call? Y N)"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 350
   Printer.FontBold = True
   Printer.Print "Att:"
   Printer.FontBold = False
   Printer.CurrentX = 6700
   Printer.CurrentY = L + 350
   Printer.Print "E G F P"
   Printer.CurrentX = 7600
   Printer.CurrentY = L + 350
   Printer.FontBold = True
   Printer.Print "Per:"
   Printer.FontBold = False
   Printer.CurrentX = 8100
   Printer.CurrentY = L + 350
   Printer.Print "E G F P"
   Printer.CurrentX = 9000
   Printer.CurrentY = L + 350
   Printer.FontBold = True
   Printer.Print "Atd:"
   Printer.FontBold = False
   Printer.CurrentX = 9500
   Printer.CurrentY = L + 350
   Printer.Print "E G F P"
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 700
   Printer.Print "Supervisor:"
   Printer.CurrentX = 1500
   Printer.CurrentY = L + 700
   Printer.Print Trim(Contact(K))
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 700
   Printer.FontBold = True
   Printer.Print "Notice:"
   Printer.CurrentX = 7000
   Printer.CurrentY = L + 700
   Printer.FontBold = False
   Printer.Print "Y  N"
   Printer.CurrentX = 7600
   Printer.CurrentY = L + 700
   Printer.FontBold = True
   Printer.Print "Rehire:"
   Printer.CurrentX = 8500
   Printer.CurrentY = L + 700
   Printer.FontBold = False
   Printer.Print "Y  N"
   
   
   'New Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1050
   Printer.Print "Positon:"
   Printer.CurrentX = 1600
   Printer.CurrentY = L + 1050
   Printer.Print Trim(Position(K))
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1050
   Printer.Print "____________________________________________"
   
   
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1400
   Printer.Print "Reason Left:"
   Printer.CurrentX = 1600
   Printer.CurrentY = L + 1400
   Printer.Print Trim(Reason(K))
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1400
   Printer.Print "____________________________________________"
   
   'Next Line
   Printer.FontBold = True
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1750
   Printer.Print "Notice"
   Printer.FontBold = False
   Printer.CurrentX = 1000
   Printer.CurrentY = L + 1750
   Printer.Print "Y N"
   Printer.FontBold = True
   Printer.CurrentX = 1500
   Printer.CurrentY = L + 1750
   Printer.Print "Why?"
   Printer.FontBold = False
   Printer.CurrentX = 2100
   Printer.CurrentY = L + 1750
   Printer.Print "_________________________________"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1750
   Printer.Print "____________________________________________"
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 2100
   Printer.Print "___________________________________________________"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 2100
   Printer.Print "____________________________________________"
End If
L = L + 2600
Next K


L = 600
If Trim(Company(3)) <> "" Then
   L = 4000
End If
If Trim(Company(4)) <> "" Then
   L = 7500
End If


If Trim(Persname(1)) <> "" Then
   Printer.FontUnderline = True
   Printer.FontBold = True
   Printer.CurrentX = 100
   Printer.CurrentY = L
   Printer.FontSize = 12
   Printer.Print "PERSONAL REFERENCES"
   Printer.FontUnderline = False
   Printer.FontBold = False
   Printer.FontSize = 10
End If
L = L + 400
For K = 1 To 4
If Trim(Persname(K)) <> "" Then
   N = N + 1
   Printer.CurrentX = 100
   Printer.CurrentY = L
   Printer.FontBold = False
   Printer.Print Trim(Persname(K))
   Printer.CurrentX = 2500
   Printer.CurrentY = L
   Printer.Print "Phone:"
   Printer.CurrentX = 3300
   Printer.CurrentY = L
   Printer.Print Format(Trim(Persphone(K)), "(###) ###-####")
   Printer.CurrentX = 6100
   Printer.CurrentY = L
   Printer.Print "Know____________________How Long?_________"
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 400
   Printer.FontBold = True
   Printer.Print "Rel?"
   Printer.CurrentX = 600
   Printer.CurrentY = L + 400
   Printer.FontBold = False
   Printer.Print "Y N"
   Printer.CurrentX = 2500
   Printer.CurrentY = L + 400
   Printer.Print "Other #"
   Printer.CurrentX = 3300
   Printer.CurrentY = L + 400
   Printer.Print Trim(Persworkphone(K))
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 400
   Printer.Print "Pers? _____________________________________"
   
   'Next Line
   Printer.FontBold = True
   Printer.CurrentX = 100
   Printer.CurrentY = L + 800
   Printer.Print "18+"
   Printer.FontBold = False
   Printer.CurrentX = 500
   Printer.CurrentY = L + 800
   Printer.Print "Y N"
   Printer.CurrentX = 2000
   Printer.CurrentY = L + 800
   Printer.Print "Attempts: _____________________"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 800
   Printer.Print "Work?_____________________________________"
   
   'Next Line
   Printer.FontBold = True
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1200
   Printer.Print "1+Yr:"
   Printer.FontBold = False
   Printer.CurrentX = 1000
   Printer.CurrentY = L + 1200
   Printer.Print "Y N  __________________________________"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1200
   Printer.Print "T/T: Y N(?)_________________________________"
   
   'Next Line
   Printer.CurrentX = 100
   Printer.CurrentY = L + 1600
   Printer.Print "______________________________________________"
   Printer.CurrentX = 6100
   Printer.CurrentY = L + 1600
   Printer.Print "Rec? Y/N__________________________________"
End If
L = L + 2000
Next K
Printer.EndDoc
End Sub

Public Sub ProcessExport()
Dim I As Integer
Dim Filearray(1 To 300)
Dim FromDate1Data(1 To 100)
Dim Entryfile As String
Dim db As Database
Dim tblUnit As Recordset
Dim TblJody As Recordset
Dim MaxI As Integer
Dim SSN As String
ErrorMess = "Problem with S:\Access\es.mdb(Reprint)"
Set db = DBEngine.Workspaces(0).OpenDatabase("S:\Access\es_be.mdb")
Set tblUnit = db.OpenRecordset("Reprint", dbOpenTable)
Set TblJody = db.OpenRecordset("Reprint", dbOpenTable)
'Entryfile = "T:\CIS_FORM\cis_form.pre"
ErrorMess = "Problem with T:\CIS_FORM\cis_form.pre(Corrupt)"
Entryfile = "T:\CIS_FORM\cis_form.pre"
I = 1
Open Entryfile For Input As #1
 '  Line Input #1, Filearray(i)
 '  Line Input #1, Filearray(2)
 '  i = i + 1
   Do While Not EOF(1)
   '  Line Input #1, Filearray(i)
     Filearray(I) = Input(1498, 1)
     CRLF = Input(2, 1)
     I = I + 1
   Loop

Close #1
ErrorMess = "Problem with S:\Access\es_be.mdb(Reprint..Corrupt Data from pre file going into Reprint)"

MaxI = I
For I = 1 To MaxI - 1
tblUnit.AddNew
tblUnit.Fields("DATECREATED").Value = Mid(Filearray(I), 1, 20)
tblUnit.Fields("XMAIL").Value = Mid(Filearray(I), 81, 8)

If Val(Trim(Mid(Filearray(I), 103, 4))) > 0 Then
   tblUnit.Fields("UNIT").Value = Mid(Filearray(I), 103, 4)
Else
   If Val(Trim(Mid(Filearray(I), 71, 4))) > 0 Then
      tblUnit.Fields("UNIT").Value = Mid(Filearray(I), 71, 4)
   End If
End If

tblUnit.Fields("COMPLETEDBY").Value = Mid(Filearray(I), 138, 20)
If Trim(Mid(Filearray(I), 159, 8)) <> "" Then
   tblUnit.Fields("INTERDATE").Value = Format(Mid(Filearray(I), 159, 8), "&&/&&/&&")
End If
tblUnit.Fields("INTERTIME").Value = Mid(Filearray(I), 168, 6)
tblUnit.Fields("APPLICATION").Value = Mid(Filearray(I), 175, 1)
tblUnit.Fields("RELEASE").Value = Mid(Filearray(I), 177, 1)
tblUnit.Fields("SALESREP").Value = Mid(Filearray(I), 179, 1)
tblUnit.Fields("BISMAKER").Value = Mid(Filearray(I), 181, 1)
tblUnit.Fields("STORECLERK").Value = Mid(Filearray(I), 183, 1)
tblUnit.Fields("BACKLINE").Value = Mid(Filearray(I), 185, 1)
tblUnit.Fields("OUTSIDE").Value = Mid(Filearray(I), 187, 1)
tblUnit.Fields("HOSTESS").Value = Mid(Filearray(I), 189, 1)
tblUnit.Fields("INTPROMO").Value = Mid(Filearray(I), 191, 1)
tblUnit.Fields("EXTNEWHIRE").Value = Mid(Filearray(I), 193, 1)
tblUnit.Fields("COMPANY1").Value = Mid(Filearray(I), 195, 25)
tblUnit.Fields("CONTACT1").Value = Mid(Filearray(I), 221, 25)
tblUnit.Fields("PHONE1").Value = Mid(Filearray(I), 247, 10)
tblUnit.Fields("POSITION1").Value = Mid(Filearray(I), 258, 25)
tblUnit.Fields("REASON1").Value = Mid(Filearray(I), 284, 30)
If Trim(Mid(Filearray(I), 315, 8)) <> "" Then
   tblUnit.Fields("FROMDATE1").Value = Format(Mid(Filearray(I), 315, 8), "&&/&&/&&")
End If
If Trim(Mid(Filearray(I), 324, 8)) <> "" Then
   tblUnit.Fields("TODATE1").Value = Format(Mid(Filearray(I), 324, 8), "&&/&&/&&")
End If
tblUnit.Fields("RATE1").Value = Mid(Filearray(I), 343, 6)
tblUnit.Fields("COMPANY2").Value = Mid(Filearray(I), 350, 25)
tblUnit.Fields("CONTACT2").Value = Mid(Filearray(I), 376, 25)
tblUnit.Fields("PHONE2").Value = Mid(Filearray(I), 402, 10)
tblUnit.Fields("POSITION2").Value = Mid(Filearray(I), 413, 25)
tblUnit.Fields("REASON2").Value = Mid(Filearray(I), 439, 30)
If Trim(Mid(Filearray(I), 470, 8)) <> "" Then
   tblUnit.Fields("FROMDATE2").Value = Format(Mid(Filearray(I), 470, 8), "&&/&&/&&")
End If
If Trim(Mid(Filearray(I), 479, 8)) <> "" Then
   tblUnit.Fields("TODATE2").Value = Format(Mid(Filearray(I), 479, 8), "&&/&&/&&")
End If
tblUnit.Fields("RATE2").Value = Mid(Filearray(I), 498, 6)
tblUnit.Fields("COMPANY3").Value = Mid(Filearray(I), 505, 25)
tblUnit.Fields("CONTACT3").Value = Mid(Filearray(I), 531, 25)
tblUnit.Fields("PHONE3").Value = Mid(Filearray(I), 557, 10)
tblUnit.Fields("POSITION3").Value = Mid(Filearray(I), 568, 25)
tblUnit.Fields("REASON3").Value = Mid(Filearray(I), 594, 30)
If Trim(Mid(Filearray(I), 625, 8)) <> "" Then
   tblUnit.Fields("FROMDATE3").Value = Format(Mid(Filearray(I), 625, 8), "&&/&&/&&")
End If
If Trim(Mid(Filearray(I), 634, 8)) <> "" Then
   tblUnit.Fields("TODATE3").Value = Format(Mid(Filearray(I), 634, 8), "&&/&&/&&")
End If
tblUnit.Fields("RATE3").Value = Mid(Filearray(I), 653, 6)
tblUnit.Fields("COMPANY4").Value = Mid(Filearray(I), 660, 25)
tblUnit.Fields("CONTACT4").Value = Mid(Filearray(I), 686, 25)
tblUnit.Fields("PHONE4").Value = Mid(Filearray(I), 712, 10)
tblUnit.Fields("POSITION4").Value = Mid(Filearray(I), 723, 25)
tblUnit.Fields("REASON4").Value = Mid(Filearray(I), 749, 30)
If Trim(Mid(Filearray(I), 780, 8)) <> "" Then
   tblUnit.Fields("FROMDATE4").Value = Format(Mid(Filearray(I), 780, 8), "&&/&&/&&")
End If
If Trim(Mid(Filearray(I), 789, 8)) <> "" Then
   tblUnit.Fields("TODATE4").Value = Format(Mid(Filearray(I), 789, 8), "&&/&&/&&")
End If
tblUnit.Fields("RATE4").Value = Mid(Filearray(I), 808, 6)
tblUnit.Fields("PERSNAME1").Value = Mid(Filearray(I), 815, 25)
tblUnit.Fields("PERSPHONE1").Value = Mid(Filearray(I), 841, 10)
tblUnit.Fields("WORKPHONE1").Value = Mid(Filearray(I), 852, 10)
tblUnit.Fields("BESTTIME1").Value = Mid(Filearray(I), 863, 25)
tblUnit.Fields("PERSNAME2").Value = Mid(Filearray(I), 889, 25)
tblUnit.Fields("PERSPHONE2").Value = Mid(Filearray(I), 915, 10)
tblUnit.Fields("WORKPHONE2").Value = Mid(Filearray(I), 926, 10)
tblUnit.Fields("BESTTIME2").Value = Mid(Filearray(I), 937, 25)
tblUnit.Fields("PERSNAME3").Value = Mid(Filearray(I), 963, 25)
tblUnit.Fields("PERSPHONE3").Value = Mid(Filearray(I), 989, 10)
tblUnit.Fields("WORKPHONE3").Value = Mid(Filearray(I), 1000, 10)
tblUnit.Fields("BESTTIME3").Value = Mid(Filearray(I), 1011, 25)
tblUnit.Fields("FIRSTNAME").Value = Mid(Filearray(I), 1037, 15)
tblUnit.Fields("INITIAL").Value = Mid(Filearray(I), 1068, 1)
tblUnit.Fields("LASTNAME").Value = Mid(Filearray(I), 1070, 30)
tblUnit.Fields("ADDRESS1").Value = Mid(Filearray(I), 1101, 30)
tblUnit.Fields("ADDRESS2").Value = Mid(Filearray(I), 1132, 30)
tblUnit.Fields("CITY").Value = Mid(Filearray(I), 1163, 30)
tblUnit.Fields("STATE").Value = Mid(Filearray(I), 1194, 2)
tblUnit.Fields("ZIP").Value = Mid(Filearray(I), 1197, 5)
tblUnit.Fields("APPLPHONE").Value = Mid(Filearray(I), 1207, 10)
SSN = Mid(Filearray(I), 1218, 9)
If SSN = "         " Then
     SSN = "000000000"
  Else
     tblUnit.Fields("APPLSSN").Value = Format(SSN, "000000000")
End If
tblUnit.Fields("COMMENTS0").Value = Mid(Filearray(I), 1228, 50)
tblUnit.Fields("COMMENTS1").Value = Mid(Filearray(I), 1279, 50)
tblUnit.Fields("COMMENTS2").Value = Mid(Filearray(I), 1330, 50)
tblUnit.Fields("COMMENTS3").Value = Mid(Filearray(I), 1381, 50)
tblUnit.Fields("SCHOOLNAME").Value = Mid(Filearray(I), 1432, 25)
tblUnit.Fields("CONTACT").Value = Mid(Filearray(I), 1458, 20)
tblUnit.Fields("SCHOOLPHONE").Value = Mid(Filearray(I), 1479, 10)
'Removed from form on 10-13-2002
'tblUnit.Fields("LASTDATE").Value = Format(Mid(Filearray(I), 1490, 8), "&&/&&/&&")
tblUnit.Update
Next I
End Sub

