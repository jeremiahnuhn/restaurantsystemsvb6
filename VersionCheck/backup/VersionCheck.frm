VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "HeartSIP Version Check for 3.3.7"
   ClientHeight    =   6975
   ClientLeft      =   4125
   ClientTop       =   1980
   ClientWidth     =   12420
   Icon            =   "VersionCheck.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6975
   ScaleWidth      =   12420
   Begin VB.CommandButton Command3 
      Caption         =   "Print"
      Height          =   495
      Left            =   8520
      TabIndex        =   4
      Top             =   6240
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Copy to Clipboard"
      Height          =   495
      Left            =   6600
      TabIndex        =   2
      Top             =   6240
      Width           =   1695
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5730
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   11895
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      Height          =   495
      Left            =   10440
      TabIndex        =   0
      Top             =   6240
      Width           =   1695
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Label3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   6
      Top             =   6240
      Width           =   4575
   End
   Begin VB.Label Label2 
      Caption         =   "Version 2"
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   6240
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   13815
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
  Clipboard.Clear
  Copytoclip = ""
For I = 0 To List1.ListCount - 1
Copytoclip = Copytoclip & List1.List(I) & Chr$(13) & Chr$(10)
Next I
Clipboard.SetText Copytoclip
m = MsgBox("Copy to Clipboard complete.", vbInformation, "Clipboard Status")
End Sub

Private Sub Command3_Click()
Printer.Print
Printer.Print
   Printer.FontName = "COURIER NEW"
   Printer.FontSize = 12
      Printer.CurrentX = 600
   Printer.Print "####   Restaurant Name          TID    Version  File Date Time   Scheduled"
      Printer.CurrentX = 600
   Printer.Print "--------------------------------------------------------------------------"
For I = 0 To List1.ListCount - 1
Printer.CurrentX = 600
Printer.Print List1.List(I)
Printer.Print
Printer.Print
Next I
Printer.Print
Printer.FontSize = 10
Printer.CurrentX = 2600
Printer.Print Str(Now()) + " - " + Label3.Caption + " Converted"
Printer.EndDoc
m = MsgBox("Printed", vbInformation, "Printing Status")
End Sub

Private Sub Form_Load()
Label1 = "####   Restaurant Name          TID    Version  File Date Time   Scheduled"
On Error GoTo ErrorHandler

Dim UnitNumber As String
Dim UnitName As String
Dim Reg1 As String
Dim Reg2 As String
Dim Reg3 As String
Dim Reg5 As String
Dim TID As String
Dim Version As String
Dim RunDate As String
Dim RunTime As String
Dim Reg1Date As Date
Dim Reg2Date As Date
Dim Reg3Date As Date
Dim Reg5Date As Date

NumberConverted = 0

Open "s:\HeartSIP\VersionCheck\VersionCheck.csv" For Output As #4
Print #4, "####,Name,Terminal,TID,Version,File Date,File Time,Scheduled"

Open "s:\HeartSIP\VersionCheck\377RolloutSchedule.csv" For Input As #1
    Line Input #1, IndividualLine
    Do While Not EOF(1)

    Line Input #1, IndividualLine
    FileInput = Split(IndividualLine, ",")
    UnitNumber = FileInput(0)
    UnitName = FileInput(1)
    Reg1 = FileInput(2)
    Reg2 = FileInput(3)
    Reg3 = FileInput(4)
    Reg5 = FileInput(5)
    TempName = Format(Mid$(UnitName, 1, 20), "@@@@@@@@@@@@@@@@@@@@!")
    
    If Reg1 <> "" Then
        Temp = "T1"
        Reg1Date = Reg1
        If Reg1Date <= Now() - 1 Then
            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T1.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If Version <> "3.3.7" Then
                List1.AddItem UnitNumber + "  " + TempName + " T1 " + TID + "  " + Version + "  " + RunDate + " " + RunTime + "  " + Reg1
                Print #4, UnitNumber + "," + UnitName + ",T1," + TID + "," + Version + "," + RunDate + "," + RunTime + "," + Reg1
    
            Else
                NumberConverted = NumberConverted + 1
            End If
        End If
    End If
    
    If Reg2 <> "" Then
        Reg2Date = Reg2
        Temp = "T2"
        If Reg2Date <= Now() - 1 Then
            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T2.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If Version <> "3.3.7" Then
                List1.AddItem UnitNumber + "  " + TempName + " T2 " + TID + "  " + Version + "  " + RunDate + " " + RunTime + "  " + Reg2
                Print #4, UnitNumber + "," + UnitName + ",T2," + TID + "," + Version + "," + RunDate + "," + RunTime + "," + Reg2
            Else
                NumberConverted = NumberConverted + 1
            End If
         End If
    End If
        
    If Reg3 <> "" Then
        Temp = "T3"
        Reg3Date = Reg3
        If Reg3Date <= Now() - 1 Then
            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T3.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If Version <> "3.3.7" Then
                List1.AddItem UnitNumber + "  " + TempName + " T3 " + TID + "  " + Version + "  " + RunDate + " " + RunTime + "  " + Reg3
                Print #4, UnitNumber + "," + UnitName + ",T3," + TID + "," + Version + "," + RunDate + "," + RunTime + "," + Reg3
            Else
                NumberConverted = NumberConverted + 1
            End If
        End If
    End If
    
    If Reg5 <> "No" Then
    If Reg5 <> "" Then
        Temp = "T5"
        Reg5Date = Reg5
        If Reg5Date <= Now() - 1 Then
            Open "v:\Poll\HeartSIP\" + UnitNumber + "-T5.ver" For Input As #2
                Line Input #2, IndividualLine2
                    FileInput = Split(IndividualLine2, ",")
                    TID = FileInput(2)
                    Version = Mid$(FileInput(4), 1, 5)
                    RunDate = FileInput(6)
                    RunTime = FileInput(7)
            Close #2
            If Version <> "3.3.7" Then
                List1.AddItem UnitNumber + "  " + TempName + " T5 " + TID + "  " + Version + "  " + RunDate + " " + RunTime + "  " + Reg5
                Print #4, UnitNumber + "," + UnitName + ",T5," + TID + "," + Version + "," + RunDate + "," + RunTime + "," + Reg5
            Else
                NumberConverted = NumberConverted + 1
            End If
        End If
    End If
    End If






Loop
Close #1
Label3.Caption = Str(NumberConverted) + " Converted"

Close 4

Exit Sub

ErrorHandler:
   List1.AddItem UnitNumber + "  " + TempName + " " + Temp + " Version file missing."

Resume Next

End Sub

