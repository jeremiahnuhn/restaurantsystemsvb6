VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Main 
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000004&
   Caption         =   "Import"
   ClientHeight    =   4845
   ClientLeft      =   2280
   ClientTop       =   1965
   ClientWidth     =   7095
   FillStyle       =   0  'Solid
   ForeColor       =   &H00000000&
   Icon            =   "Main.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   Palette         =   "Main.frx":0442
   ScaleHeight     =   4845
   ScaleWidth      =   7095
   Begin VB.Frame Frame1 
      Height          =   2415
      Left            =   4680
      TabIndex        =   5
      Top             =   1200
      Width           =   2175
      Begin VB.CommandButton CmdExit 
         Caption         =   "E&xit"
         Height          =   375
         Left            =   240
         TabIndex        =   8
         Top             =   1800
         Width           =   1695
      End
      Begin VB.CommandButton CmdLoadData 
         Caption         =   "&Read File"
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   1080
         Width           =   1695
      End
      Begin VB.CommandButton CmdImport 
         Caption         =   "&Import to Database"
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.DirListBox Dir1 
      Height          =   1665
      Left            =   360
      TabIndex        =   3
      Top             =   1920
      Width           =   2175
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   360
      TabIndex        =   2
      Top             =   1320
      Width           =   2175
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   3840
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
      Max             =   1000
      Scrolling       =   1
   End
   Begin VB.FileListBox File1 
      Height          =   2235
      Left            =   2760
      Pattern         =   "*.d00"
      TabIndex        =   0
      Top             =   1320
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Texas Restaurants"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   2160
      TabIndex        =   11
      Top             =   4320
      Width           =   2895
   End
   Begin VB.Label Lblprocessing 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   10
      Top             =   360
      Width           =   2175
   End
   Begin VB.Label LblLastName 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   9
      Top             =   360
      Width           =   1695
   End
   Begin VB.Label LblFirstName 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   4
      Top             =   360
      Width           =   1695
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private SSAN(250) As String
Private FirstName1(250) As String
Private FirstName2(250) As String
Private MiddleInit(250) As String
Private LastName1(250) As String
Private LastName2(250) As String
Private Addr1(250) As String
Private Addr2(250) As String
Private City1(250) As String
Private City2(250) As String
Private State(250) As String
Private Zip(250) As String
Private JobCode(250) As String
Private PayRate(250) As String
Private PayType(250) As String
Private Sex(250) As String
Private BirthDate(250) As String
Private StateExempt(250) As String
Private StateAdd(250) As String
Private FedExempt(250) As String
Private FedAdd(250) As String
Private StateMarStatus(250) As String
Private FedMarStatus(250) As String
Private EmpStatus(250) As String
Private Race(250) As String
Private HireDate(250) As String
Private I As Integer
Private MaxI As Integer

Private Sub Adodc1_WillMove(ByVal adReason As ADODB.EventReasonEnum, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

End Sub

Private Sub CmdExit_Click()
End
End Sub

Private Sub CmdOpenDb_Click()

End Sub

Private Sub CmdImport_Click()
Dim Tempyear As String
Dim Tempmonth As String
Dim TempDay As String
Dim TestChar As String
Dim TestString As String
Dim FirstChar As String
Dim TestLen As Integer
Dim J As Integer
Dim K As Integer
ProgressBar1.Max = MaxI
I = 1
J = 1
Dim cn As ADODB.Connection
Dim rs As ADODB.Recordset
Dim ps As ADODB.Recordset
Dim ss As ADODB.Recordset
Set cn = New ADODB.Connection
'Open the ADO Recordset
cn.Open "Provider=sqloledb;user id=sa;pwd=;Initial Catalog=IRIS;Server=BNE_SERVER;"
Set rs = New ADODB.Recordset
Set ps = New ADODB.Recordset
Set ss = New ADODB.Recordset
With rs
   .CursorLocation = adUseClient
   .CursorType = adOpenDynamic
   .LockType = adLockOptimistic
   .Open "Select * FROM tblEmployees ORDER BY EmployeeID", cn
End With
With ps
   .CursorLocation = adUseClient
   .CursorType = adOpenDynamic
   .LockType = adLockOptimistic
   .Open "Select * FROM tblEmployeesJobs ORDER BY EmployeeID", cn
End With
With ss
   .CursorLocation = adUseClient
   .CursorType = adOpenDynamic
   .LockType = adLockOptimistic
   .Open "Select * FROM tblSecurityGroupMembers ORDER BY EmployeeID", cn
End With
rs.MoveLast
If rs!EmployeeID > 0 Then
      J = rs!EmployeeID + 1
Else
      J = 1
End If
ProgressBar1.Max = MaxI
rs.MoveFirst
ps.MoveFirst
ss.MoveFirst
Do While I < MaxI + 1
   rs.AddNew
   ps.AddNew
   ss.AddNew
   rs!EmployeeID = J
   ps!EmployeeID = J
   ss!EmployeeID = J
   If Trim(EmpStatus(I)) <> "" Then
      If Trim(EmpStatus(I)) = "A" Then
         rs!StatusID = 1
      Else
         rs!StatusID = 2
      End If
    Else
      rs!StatusID = 2
    End If
   rs!Rehire = 0
   rs!Borrowed = 0
   If Trim(LastName1(I)) <> "" Then
      rs!LastName = LastName1(I) & Format(Trim(LastName2(I)), "<")
   Else
      rs!LastName = "XXXXX"
   End If
   If Trim(FirstName1(I)) <> "" Then
      rs!FirstName = FirstName1(I) & Format(Trim(FirstName2(I)), "<")
   Else
      rs!FirstName = "XXXXX"
   End If
   If Trim(MiddleInit(I)) <> "" Then
      rs!MiddleInitial = Trim(MiddleInit(I))
   End If
   If Addr1(I) <> "" Then
      K = 1
      TestString = ""
      FirstChar = Mid(Addr1(I), 1, 1)
      Do
        TestChar = Mid(Addr1(I), K, 1)
            If TestChar = " " Then
                TestString = TestString + TestChar
                K = K + 1
                TestChar = Format(Mid(Addr1(I), K, 1), ">")
                TestString = TestString + TestChar
            Else
               K = K + 1
               TestChar = Format(Mid(Addr1(I), K, 1), "<")
               TestString = TestString + TestChar
            End If
      Loop Until K = 30
      TestString = FirstChar + TestString
      rs!Address1 = Left(Trim(TestString), 30)
   End If
   If Addr2(I) <> "" Then
      rs!Address2 = Format(Trim(Addr2(I)), "<")
   End If
   If Trim(City1(I)) <> "" Then
      rs!City = City1(I) & Format(Trim(City2(I)), "<")
   End If
   If Trim(State(I)) <> "" Then
      rs!statecode = Trim(State(I))
   End If
   If Trim(SSAN(I)) > 0 Then
      rs!SSN = Trim(SSAN(I))
   Else
      rs!SSN = "000000000"
      SSAN(I) = "000000000"
   End If
   rs!Password = Right(Trim(SSAN(I)), 4)
   rs!HomePhone = "0000000000"
   If Trim(Zip(I)) <> "" Then
      rs!Zip = Trim(Zip(I))
   Else
      rs!Zip = "00000"
   End If
   If Trim(Sex(I)) <> "" Then
      If Trim(Sex(I)) = "M" Then
         rs!Gender = 0
      Else
         rs!Gender = 1
      End If
   End If
   If Trim(Race(I)) <> "" Then
      Select Case Trim(Race(I))
         Case "WH"
            rs!RaceID = 6
         Case "B"
            rs!RaceID = 3
         Case "I"
            rs!RaceID = 1
         Case "AS"
            rs!RaceID = 2
         Case "HI"
            rs!RaceID = 4
         Case Else
            rs!RaceID = 9
       End Select
    Else
       rs!RaceID = 9
    End If
   If Trim(HireDate(I)) <> "" Then
      Tempyear = Left(Trim(HireDate(I)), 4)
      Tempmonth = Mid(Trim(HireDate(I)), 5, 2)
      TempDay = Mid(Trim(HireDate(I)), 7, 2)
      rs!HireDate = Tempmonth & "/" & TempDay & "/" & Tempyear
    Else
      rs!HireDate = "0:00:00"
    End If
   If Trim(PayType(I)) <> "" Then
      Select Case Trim(PayType(I))
         Case 230925
            ' General Manager
            rs!PayTypeID = 2
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(0.01, "0.00")
            rs!SecurityLevel = 15
            ps!SecurityLevel = 15
            ss!SecurityGroupID = 3
         Case 230860
            ' Manager
            rs!PayTypeID = 2
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(0.01, "0.00")
            rs!SecurityLevel = 18
            ps!SecurityLevel = 18
            ss!SecurityGroupID = 4
         Case 230951
            'Hourly Shift Supervisor
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 20
            ps!SecurityLevel = 20
            ss!SecurityGroupID = 5
        Case 230949
            'Management Shift Supervisor
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 20
            ps!SecurityLevel = 20
            ss!SecurityGroupID = 5
         Case 230875
            'Day Host/Hostess
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230890
            'Night Host/Hostess
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230896
            'Line Cook
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230897
            'Prep Cook
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230898
            'Dishwasher
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230915
            'Night Server
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230916
            'Day Server
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230917
            'Carry Out
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230920
            'Server Assistant
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230945
            'Training
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230945
            'Night Bartender
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230950
            'Day Bartender
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230895
            'Old Like Cook
            rs!PayTypeID = 1
            ps!JobCode = 238096
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
         Case 230952
            'Old Mgmt Shift Supervisor
            rs!PayTypeID = 1
            ps!JobCode = 230949
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 20
            ps!SecurityLevel = 20
            ss!SecurityGroupID = 5
         Case Else
            rs!PayTypeID = 1
            ps!JobCode = Trim(PayType(I))
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
            ss!SecurityGroupID = 6
      End Select
   Else
      rs!PayTypeID = 1
      ps!JobCode = Trim(PayType(I))
      ps!PayRate = Format(0.0525, "0.00")
      rs!SecurityLevel = 55
      ps!SecurityLevel = 55
      ss!SecurityGroupID = 6
   End If
   ss.Update
   If (PayRate(I) < 515) Then
        ps!PrimaryJob = -1
   Else
        ps!PrimaryJob = 0
   End If
   ps!PerformRating = 0
   ps!SkillLevel = 0
   ps!EffectiveDate = Format(Now, "mm/dd/yy") & " 00:00:00"
   ps!ModUserID = J
   ps!ModDate = Format(Now, "mm/dd/yy")
   ps!DelFlag = 0
   ps!ApprovalObtained = 0
   ps!Except_ = 0
   If Trim(FedExempt(I)) <> "" Then
      rs!FederalDependents = Trim(FedExempt(I))
   End If
   rs!FederalExempt = 0
   If Trim(FedAdd(I)) <> "" Then
      rs!WithholdFederalPercentage = Trim(FedAdd(I))
   End If
   If Trim(FedMarStatus(I)) <> "" Then
      rs!FedMaritalStatus = Trim(FedMarStatus(I))
   End If
   'rs!EIC = 0
   If Trim(StateExempt(I)) <> "" Then
      rs!StateDependents = Trim(StateExempt(I))
   End If
   rs!StateExempt = 0
   If Trim(StateAdd(I)) <> "" Then
      rs!WithholdStatePercentage = Trim(StateExempt(I))
   End If
   If Trim(StateMarStatus(I)) <> "" Then
      rs!StateMaritalStatus = Trim(StateMarStatus(I))
   End If
   rs!CountyExempt = 0
   rs!CityExempt = 0
   rs!INSStatusID = 1
   rs!AlienExpirationDate = "0:00:00"
   rs!DocumentADate = "0:00:00"
   rs!DocumentBDate = "0:00:00"
   rs!DocumentCDate = "0:00:00"
   If Trim(BirthDate(I)) <> "" Then
      Tempyear = Left(Trim(BirthDate(I)), 4)
      Tempmonth = Mid(Trim(BirthDate(I)), 5, 2)
      TempDay = Mid(Trim(BirthDate(I)), 7, 2)
      rs!BirthDate = Tempmonth & "/" & TempDay & "/" & Tempyear
   End If
   rs!Notes = "Imported"
   rs!Veteran = 0
   rs!Handicapped = 0
   rs!RotationStartDate = Format(Now, "mm/dd/yyyy")
   rs!LaborRuleSetID = 0
   rs!SchedulingCriteriaNumber = 1
   rs!Rehired = 0
   rs!CareerProgram = 0
   rs!TermPaid = 0
   rs!ForeignNational = 0
   rs!EUMember = 0
   rs!WHCheck = 0
   rs.Update
   ps.Update
   
   'Add Training Jobs
   If PayType(I) <> 230860 And PayType(I) <> 230925 Then
    ps.AddNew
    ps!EmployeeID = J
      Select Case Trim(PayType(I))
         Case 230951
            'Hourly Shift Supervisor
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 20
        Case 230949
            'Management Shift Supervisor
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 20
         Case 230875
            'Day Host/Hostess
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230890
            'Night Host/Hostess
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230896
            'Line Cook
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230897
            'Prep Cook
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230898
            'Dishwasher
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230915
            'Night Server
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230916
            'Day Server
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230917
            'Carry Out
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230920
            'Server Assistant
            ps!JobCode = Trim(PayType(I))
            rs!SecurityLevel = 55
            ps!SecurityLevel = 55
         Case 230945
            'Training
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
            ps!SecurityLevel = 55
         Case 230945
            'Night Bartender
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230950
            'Day Bartender
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
         Case 230895
            'Old Like Cook
            ps!JobCode = 238096
            ps!SecurityLevel = 55
         Case 230952
            'Old Mgmt Shift Supervisor
            ps!JobCode = 230949
            ps!SecurityLevel = 20
         Case Else
            ps!JobCode = Trim(PayType(I))
            ps!SecurityLevel = 55
      End Select
    ps!JobCode = 230935
    If (PayRate(I) < 515) Then
            ps!PayRate = 5.15
        Else
            ps!PayRate = Format(PayRate(I) * 0.01, "0.00")
    End If
    ps!PrimaryJob = 0
    ps!PerformRating = 0
    ps!SkillLevel = 0
    ps!EffectiveDate = Format(Now, "mm/dd/yy") & " 00:00:00"
    ps!ModUserID = J
    ps!ModDate = Format(Now, "mm/dd/yy")
    ps!DelFlag = 0
    ps!ApprovalObtained = 0
    ps!Except_ = 0
    ps.Update
   End If
   'End Add Training Jobs
   ProgressBar1.Value = I
   J = J + 1
   I = I + 1
   For K = 1 To 10000
      LblFirstName.Caption = Trim(LastName1(I)) & Trim(LastName2(I))
      LblLastName.Caption = Trim(FirstName1(I)) & Trim(FirstName2(I))
      Lblprocessing.Caption = "Processing Data"
      DoEvents
   Next K
Loop
ss.Close
ps.Close
rs.Close
cn.Close
Lblprocessing.Caption = "DataBase Upload"
LblFirstName.Caption = "Complete"
Set rs = Nothing
Set ps = Nothing
Set ss = Nothing
Set cn = Nothing
End Sub

Private Sub CmdLoadData_Click()
Dim TempFileName As String
I = 1
TempFileName = File1.Path & "\" & InputFile
If InputFile <> "" Then
   Open TempFileName For Input As #1
        Do Until EOF(1)
           Line Input #1, FileLine(I)
           MaxI = I
           I = I + 1
        Loop
    Close #1
Else
   MsgBox "Please Select File to Read"
End If
I = 1
Do Until I = MaxI + 1
   SSAN(I) = Mid(FileLine(I), 15, 9)
   FirstName1(I) = Mid(FileLine(I), 24, 1)
   FirstName2(I) = Mid(FileLine(I), 25, 14)
   MiddleInit(I) = Mid(FileLine(I), 39, 1)
   LastName1(I) = Mid(FileLine(I), 40, 1)
   LastName2(I) = Mid(FileLine(I), 41, 29)
   Addr1(I) = Mid(FileLine(I), 70, 30)
   Addr2(I) = Mid(FileLine(I), 100, 30)
   City1(I) = Mid(FileLine(I), 130, 1)
   City2(I) = Mid(FileLine(I), 131, 12)
   State(I) = Mid(FileLine(I), 148, 2)
   Zip(I) = Mid(FileLine(I), 150, 10)
   JobCode(I) = Mid(FileLine(I), 160, 6)
   PayType(I) = Mid(FileLine(I), 160, 6)
   PayRate(I) = Mid(FileLine(I), 176, 4)
   Sex(I) = Mid(FileLine(I), 182, 1)
   Race(I) = Mid(FileLine(I), 183, 2)
   BirthDate(I) = Mid(FileLine(I), 185, 8)
   StateExempt(I) = Mid(FileLine(I), 197, 1)
   StateAdd(I) = Mid(FileLine(I), 203, 9)
   FedExempt(I) = Mid(FileLine(I), 202, 1)
   FedAdd(I) = Mid(FileLine(I), 212, 9)
   StateMarStatus(I) = Mid(FileLine(I), 221, 2)
   FedMarStatus(I) = Mid(FileLine(I), 223, 2)
   EmpStatus(I) = Mid(FileLine(I), 226, 2)
   HireDate(I) = Mid(FileLine(I), 286, 8)
   I = I + 1
Loop
If FileLine(1) <> "" Then
   CmdImport.Enabled = True
   Lblprocessing.Caption = "Read input File:"
   LblFirstName.Caption = "Complete"
   Main.Caption = "                          Ready to Import file into IRIS Database"
End If
End Sub

Private Sub Dir1_Change()
File1.Path = Dir1.Path
End Sub


Private Sub Drive1_Change()
On Error Resume Next
Dir1.Path = Drive1.Drive
   If Err.Number <> 0 Then
      MsgBox "Disk drive not ready"
      Err.Clear
   End If
End Sub

Private Sub File1_Click()
InputFile = File1.FileName
End Sub

Private Sub Form_Load()
Main.Caption = "                                         Ready to read file"
ProgressBar1.Min = 0
ProgressBar1.Max = 1
CmdImport.Enabled = False
End Sub

