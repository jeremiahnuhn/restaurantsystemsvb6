REM   BATCH FILE TO CREATE WINDOWS SCHEDULED TASKS FOR REMINDER.EXE
REM   ALL RESTAURANTS
schtasks /Delete /tn Reminder-9am /F
schtasks /create /sc DAILY /tn Reminder-9am /tr "c:\support\reminder.exe" /st 09:00:00
schtasks /Delete /tn Reminder-2pm /F
schtasks /create /sc DAILY /tn Reminder-2pm /tr "c:\support\reminder.exe" /st 14:00:00
schtasks /Delete /tn Reminder-7pm /F
schtasks /create /sc DAILY /tn Reminder-7pm /tr "c:\support\reminder.exe" /st 19:00:00
EXIT
