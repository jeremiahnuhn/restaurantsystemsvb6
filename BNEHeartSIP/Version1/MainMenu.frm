VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H0080FFFF&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   12000
   ClientLeft      =   -180
   ClientTop       =   0
   ClientWidth     =   18615
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   12000
   ScaleWidth      =   18615
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command5 
      Caption         =   "Download Application (Full)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   7
      Top             =   6000
      Width           =   5175
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Download Configuration (Partial)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   6
      Top             =   4680
      Width           =   5175
   End
   Begin VB.FileListBox File1 
      Height          =   2625
      Left            =   -120
      Pattern         =   "*.txt"
      TabIndex        =   5
      Top             =   9360
      Visible         =   0   'False
      Width           =   3255
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Create Report Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   4
      Top             =   2040
      Width           =   5175
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Settle Batch"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   3
      Top             =   3360
      Width           =   5175
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   2
      Top             =   7320
      Width           =   5175
   End
   Begin VB.Label Label6 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Left            =   5880
      TabIndex        =   11
      Top             =   2760
      Width           =   6255
   End
   Begin VB.Label Label5 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   5880
      TabIndex        =   10
      Top             =   5880
      Width           =   6255
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   9
      Top             =   960
      Width           =   12135
   End
   Begin VB.Label Label3 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   8
      Top             =   2040
      Width           =   6255
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "Version 1 - January 2018"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   8400
      Width           =   5175
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "Boddie-Noell Payment Terminal Utility"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   13095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
msg = "Are you sure you want to settle the batch on the payment terminal?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Confirm Settlement"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.

    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Settling Payment Terminal Batch"
    Print #1, "cd \Iris\Bin"
    Print #1, "SCADeviceBatch.exe /NOUI /SETTLE /REPORT"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Batch Settled"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If
End Sub

Private Sub Command3_Click()
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Creating Report Files"
    Print #1, "cd \Iris\Bin"
    Print #1, "SCADeviceBatch.exe /NOUI /REPORT"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Report Files Created"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End Sub

Private Sub Command4_Click()
    
msg = "Are you sure you want to complete a partial (configuration) download to the payment terminal?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Confirm Partial Downlaod"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Performing Partial Download"
    Print #1, "cd \Iris\Bin"
    Print #1, "HEARTSIPUTIL.EXE /UPDATE=PARTIAL,NOW /NOUI"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Partial Download"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
Else
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Partial Download Answered No"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1

End If


End Sub

Private Sub Command5_Click()
    
msg = "Are you sure you want to complete a full (application and configuration) download to the payment terminal?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Confirm Full Downlaod"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Performing Full Download"
    Print #1, "cd \Iris\Bin"
    Print #1, "HEARTSIPUTIL.EXE /UPDATE=FULL,NOW /NOUI"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Full Download"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
Else
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Full Download Answered No"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If

End Sub

Private Sub Form_Load()
On Error Resume Next

If Dir("c:\iris\log\reports") = "" Then
  File1.FileName = "c:\iris\log\reports\App*"
   Open "c:\iris\log\reports\" + File1.List(File1.ListCount - 1) For Input As 2
        'General Information
        Line Input #2, linedata
        Label3.Caption = "HeartSIP Version: " + Mid$(linedata, 53, 5) + "." + Mid$(linedata, 89, 5)
        'Heartland Libraries
        Line Input #2, linedata
        Line Input #2, linedata
        
        'IP Information
        Line Input #2, linedata
        Line Input #2, linedata

        Label5.Caption = "IP Info: " + Mid$(linedata, 71, 600)
        Line Input #2, linedata
        'Memory Information
        Line Input #2, linedata

        Label6.Caption = "Memory Info: " + Mid$(linedata, 79, 600)
        Line Input #2, linedata
        'Terminal Information
        Line Input #2, linedata
        Label4.Caption = "Type: " + Mid$(linedata, 55, 6) + " Terminal ID (TID): " + Mid$(linedata, 141, 8) + " Serial Number: " + Mid$(linedata, 77, 8)

                
  
  Close #2
End If


  End Sub
