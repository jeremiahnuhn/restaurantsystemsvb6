VERSION 5.00
Begin VB.Form Form2 
   BackColor       =   &H0080FFFF&
   Caption         =   "View Report Files"
   ClientHeight    =   6015
   ClientLeft      =   990
   ClientTop       =   1980
   ClientWidth     =   3015
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6015
   ScaleWidth      =   3015
   Begin VB.CommandButton Command6 
      Caption         =   "Store and Forward"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   6
      Top             =   4080
      Width           =   2055
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   4
      Top             =   5040
      Width           =   2055
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Parameter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   3
      Top             =   3120
      Width           =   2055
   End
   Begin VB.CommandButton Command3 
      Caption         =   "EMV"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   2
      Top             =   2160
      Width           =   2055
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Batch"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   1
      Top             =   1200
      Width           =   2055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "App"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   2055
   End
   Begin VB.FileListBox File1 
      Height          =   1455
      Left            =   -1080
      Pattern         =   "*.txt"
      TabIndex        =   5
      Top             =   1320
      Visible         =   0   'False
      Width           =   3015
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
If Dir("c:\iris\log\reports\app*.*") <> "" Then
  File1.FileName = "c:\iris\log\reports\App*"
  
Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         View App Report"
    Print #1, "cd \Windows\System32\"
    Print #1, "Start /max Notepad.exe " + "c:\iris\log\reports\" + File1.List(File1.ListCount - 1)
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - View App Report"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If

        
End Sub

Private Sub Command2_Click()
If Dir("c:\iris\log\reports\app*.*") <> "" Then
  File1.FileName = "c:\iris\log\reports\Batch*"
  
Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         View App Report"
    Print #1, "cd \Windows\System32\"
    Print #1, "Start /max Notepad.exe " + "c:\iris\log\reports\" + File1.List(File1.ListCount - 1)
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - View Aapp"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If

End Sub

Private Sub Command3_Click()
If Dir("c:\iris\log\reports\app*.*") <> "" Then
  File1.FileName = "c:\iris\log\reports\EMV*"
  
Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         View EMV Report"
    Print #1, "cd \Windows\System32\"
    Print #1, "Start /max Notepad.exe " + "c:\iris\log\reports\" + File1.List(File1.ListCount - 1)
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - View EMV Report"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If
End Sub

Private Sub Command4_Click()
If Dir("c:\iris\log\reports\app*.*") <> "" Then
  File1.FileName = "c:\iris\log\reports\PARA*"
  
Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         View Parameter Report"
    Print #1, "cd \Windows\System32\"
    Print #1, "Start /max Notepad.exe " + "c:\iris\log\reports\" + File1.List(File1.ListCount - 1)
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - View Parameter Report"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If
End Sub

Private Sub Command5_Click()
    Form2.Hide
End Sub

Private Sub Command6_Click()
If Dir("c:\iris\log\reports\app*.*") <> "" Then
  File1.FileName = "c:\iris\log\reports\SAF*"
  

Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         View SAF Report"
    Print #1, "cd \Windows\System32\"
    Print #1, "Start /max Notepad.exe " + "c:\iris\log\reports\" + File1.List(File1.ListCount - 1)
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - View SAF Report"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If
End Sub
