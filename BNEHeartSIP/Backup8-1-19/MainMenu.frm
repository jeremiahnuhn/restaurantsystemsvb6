VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H0080FFFF&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   12000
   ClientLeft      =   -180
   ClientTop       =   0
   ClientWidth     =   18615
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   12000
   ScaleWidth      =   18615
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command10 
      Caption         =   "Update Required Files for IRIS"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3000
      TabIndex        =   24
      Top             =   3960
      Width           =   2535
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Reboot"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3000
      TabIndex        =   23
      Top             =   2880
      Width           =   2535
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Correct Missing Logo Image"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   22
      Top             =   2880
      Width           =   2535
   End
   Begin VB.CommandButton Command7 
      Caption         =   " View Report Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3000
      TabIndex        =   21
      Top             =   1800
      Width           =   2535
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Enable Buttons"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   20
      Top             =   7200
      Width           =   2535
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Download Application (Full)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   7
      Top             =   6120
      Width           =   5175
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Download Configuration (Partial)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   6
      Top             =   5040
      Width           =   5175
   End
   Begin VB.FileListBox File1 
      Height          =   2625
      Left            =   480
      Pattern         =   "*.txt"
      TabIndex        =   5
      Top             =   9000
      Visible         =   0   'False
      Width           =   3255
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Create Report Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   4
      Top             =   1800
      Width           =   2535
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Settle Batch"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      TabIndex        =   3
      Top             =   3960
      Width           =   2535
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3000
      TabIndex        =   2
      Top             =   7200
      Width           =   2535
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "Missing Report Files"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   19
      Top             =   8280
      Visible         =   0   'False
      Width           =   5175
   End
   Begin VB.Label Label13 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5880
      TabIndex        =   18
      Top             =   3960
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.Label Label12 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5880
      TabIndex        =   17
      Top             =   3360
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.Label Label11 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   16
      Top             =   7560
      Width           =   6255
   End
   Begin VB.Label Label10 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   15
      Top             =   6960
      Width           =   6255
   End
   Begin VB.Label Label9 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   14
      Top             =   6360
      Width           =   6255
   End
   Begin VB.Label Label8 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   13
      Top             =   5760
      Width           =   6255
   End
   Begin VB.Label Label7 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   12
      Top             =   5160
      Width           =   6255
   End
   Begin VB.Label Label6 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5880
      TabIndex        =   11
      Top             =   2760
      Visible         =   0   'False
      Width           =   6255
   End
   Begin VB.Label Label5 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   10
      Top             =   4560
      Width           =   6255
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "Missing AppInfo Report - Create Reports Files - Exit - Start Utility Again"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   9
      Top             =   960
      Width           =   12135
   End
   Begin VB.Label Label3 
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   8
      Top             =   2040
      Width           =   6255
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "Version 1.10 - January 2019"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   8280
      Width           =   5175
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "Boddie-Noell Payment Terminal Utility"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   13095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command10_Click()
    Y = Shell("C:\copyconfig.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - CopyConfig Executed"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End Sub

Private Sub Command2_Click()
'Settle Batch
msg = "Are you sure you want to settle the batch on the payment terminal?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Settle Batch"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Settling Payment Terminal Batch"
    Print #1, "cd \Iris\Bin"
    Print #1, "SCADeviceBatch.exe /NOUI /SETTLE /REPORT"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Batch Settled"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If
    
End Sub

Private Sub Command3_Click()
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Creating Report Files"
    Print #1, "cd \Iris\Bin"
    Print #1, "SCADeviceBatch.exe /NOUI /REPORT"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Report Files Created"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
    Command7.Enabled = True
End Sub

Private Sub Command4_Click()
    
msg = "Are you sure you want to complete a configuration (partial) download to the payment terminal?"   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Confirm Configuration Download"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Performing Partial Download"
    Print #1, "cd \Iris\Bin"
    Print #1, "HEARTSIPUTIL.EXE /UPDATE=PARTIAL,NOW /NOUI"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Configuration Download"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
Else
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Configuration Download Answered No"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1

End If


End Sub

Private Sub Command5_Click()
    
msg = "Are you sure you want to complete a application and configuration (full) download to the payment terminal?  Make sure you have confirmed the payment terminal does not have any Store and Forward transactions."   ' Define message.
Style = vbYesNo + vbQuestion + vbDefaultButton2 ' Define buttons.
Title = "Confirm Application Download"  ' Define title.
Response = MsgBox(msg, Style, Title)
If Response = vbYes Then    ' User chose Yes.
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Performing Full Download"
    Print #1, "cd \Iris\Bin"
    Print #1, "HEARTSIPUTIL.EXE /UPDATE=FULL,NOW /NOUI"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Application Download"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
Else
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Application Download Answered No"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
End If

End Sub

Private Sub Command6_Click()
    Dialog.Text1.Text = ""
    Dialog.Show
End Sub

Private Sub Command7_Click()

If Dir("c:\iris\log\reports\app*.*") = "" Then

    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo         Creating Report Files"
    Print #1, "cd \Iris\Bin"
    Print #1, "SCADeviceBatch.exe /NOUI /REPORT"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Report Files Created"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1


End If
        Form2.Show
End Sub

Private Sub Command8_Click()
x = MsgBox("The next time Order Entry loads the logos will be updated on the Payment Terminal.", vbOKOnly, "Correct Missing Logos")
If Dir("c:\iris\ini\heartsiplogo.checksum") <> "" Then
    Kill ("c:\iris\ini\heartsiplogo.checksum")
End If
End Sub

Private Sub Command9_Click()
    Open "C:\BNEHeartSIPCommand.bat" For Output As 1
    Print #1, "Echo Off"
    Print #1, "Cls"
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo."
    Print #1, "Echo   Rebooting Payment Terminal"
    Print #1, "cd\IRIS\BIN"
    Print #1, "HeartsipUtil.exe /Q /NOUI /Reboot"
    Print #1, "EXIT"
    Close 1
    Y = Shell("C:\BNEHeartSIPCommand.bat", vbNormalFocus)
    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
    LogText = MyTime + " - Reboot"
    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
        Print #1, LogText
    Close #1
    
End Sub

Private Sub Form_Load()
On Error Resume Next
Dim POSDATE As Date
If Dir("c:\iris\log\IrisAuthSrvr.log") <> "" Then
   Open ("c:\iris\log\IrisAuthSrvr.log") For Input As 2
   
End If
POSDATE = FileDateTime("C:\iris\bin\pos.exe")
If POSDATE > "11/21/2018" Then


'XML File Format
Do While Not EOF(2)
    Line Input #2, LineIn

    If Mid$(LineIn, 82, 18) = "<Key>SERIAL NUMBER" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        TSerial33 = "Serial Number " + Mid$(LineIn, 96, 8)
        TSerial32 = "Serial Number " + Mid$(LineIn, 89, 8)
    
    End If
    If Mid$(LineIn, 82, 10) = "<DeviceId>" Then
        TID = "Terminal ID (TID) " + Mid$(LineIn, 92, 8)
        End If
    If Mid$(LineIn, 82, 18) = "<Key>VERSION</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        Gversion = "Version " + RTrim(Mid$(LineIn, 89, 5))
    End If
    If Mid$(LineIn, 82, 14) = "<Key>REV</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        GRevision = "." + Mid$(LineIn, 89, 5)
    
    End If
'    If Mid$(LineIn, 78, 18) = "<Key>HARDWARE TYPE" Then
'        Line Input #2, LineIn
'        Line Input #2, LineIn
'        TType = Mid$(LineIn, 85, 6)
'    End If

    If Mid$(LineIn, 82, 20) = "<Key>USE DHCP?</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        IDHCPT = Mid$(LineIn, 89, 1)
        If IDHCPT = N Then
            IDHCPT2 = "No"
        Else
            IHDCPT2 = "Yes"
        End If
        IDHCP = "Use DHCP?       " + IHDCPT2
    End If
    If Mid$(LineIn, 82, 22) = "<Key>TERMINAL IP</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        IIP = "Terminal IP        " + Mid$(LineIn, 89, 15)
    End If
    If Mid$(LineIn, 82, 22) = "<Key>SUBNET MASK</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        ISUB = "SubNet Mask      " + Mid$(LineIn, 89, 15)
    End If
    If Mid$(LineIn, 82, 21) = "<Key>GATEWAY IP</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        IGateway = "Gateway IP        " + Mid$(LineIn, 89, 15)
    End If
    If Mid$(LineIn, 82, 22) = "<Key>PRIMARY DNS</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        IDNS = "Primary DNS      " + Mid$(LineIn, 89, 15)
    End If
    If Mid$(LineIn, 82, 24) = "<Key>SECONDARY DNS</Key>" Then
        Line Input #2, LineIn
        Line Input #2, LineIn
        ISECONDARY = "Secondary DNS " + Mid$(LineIn, 89, 15)

        Label3.Caption = Gversion + GRevision
        Label11.Caption = ISECONDARY
        Label5.Caption = IIP
        Label7.Caption = IDHCP
        Label8.Caption = ISUB
        Label9.Caption = IGateway
        Label10.Caption = IDNS
        If Mid$(Gversion, 11, 1) = "2" Then
            TSerial = TSerial32
        Else
            TSerial = TSerial33
        End If
        Label4.Caption = TID + "    " + TSerial
        Exit Sub
    End If

Loop


       
Else
Close #2

If Dir("c:\iris\log\reports\app*.*") <> "" Then
  File1.FileName = "c:\iris\log\reports\App*"
   Open "c:\iris\log\reports\" + File1.List(File1.ListCount - 1) For Input As 2
        Label14.Visible = True
        Label14.Caption = "AppInfoReport DateTime: " + Str(FileDateTime("c:\iris\log\reports\" + File1.List(File1.ListCount - 1)))
End If
        
        Input #2, Junk, Gversion, Junk2, GRevision, Junk3, Junk4, Junk5
        Input #2, Junk1, Junk2, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Junk9, Junk10, Junk11, Junk12, Junk13, Junk14, Junk15, Junk16, Junk17, Junk18, Junk19, Junk20, Junk21, Junk22, Junk23, Junk24
        Input #2, Junk1, IMac, Junk2, IDHCP, IIP, ISUB, IGateway, IDNS, ISECONDARY
        Input #2, Junk1, Junk2, Junk3, Junk7, Junk8, MBatchCount, MBatchMax, MPercentBatch, Junk4, Junk5, Junk6
        Input #2, Junk1, Junk4, Junk5, TType, TSerial, Junk2, Junk3, TID, Junk6, Junk7, Junk8
        
        If Mid$(TSerial, 1, 6) <> "SERIAL" Then
            TType = Junk2
            TSerial = Junk3
            TID = Junk7
        End If
        Label4.Caption = Mid$(TID, 1, 20) + " " + TSerial
        Label3.Caption = Gversion + " " + GRevision
        Label11.Caption = Mid$(ISECONDARY, 1, 29)


        
        
        Label5.Caption = IIP
        Label7.Caption = IDHCP
        Label8.Caption = ISUB
        Label9.Caption = IGateway
        Label10.Caption = IDNS

        Label6.Caption = MBatchCount
        Label12.Caption = MBatchMax
        Label13.Caption = MPercentBatch


                
  
  Close #2
End If


'Check to make sure terminal has the needed files


'        Open "c:\iris\ini\IrisAuthSrvr.ini" For Input As #2
'        Do While Not EOF(2)
'            Line Input #2, INIDATA
'            If Mid$(INIDATA, 1, 17) = "; 1/2018 HeartSIP" Then
'                x = MsgBox("Terminal has the correct files to support the Payment Terminal.", vbInformation, "File Check Passed")
'                Exit Sub
'            End If
'        Loop
'        Close #2

'                x = MsgBox("This terminal does not have the needed files to support the Payment Terminal.  A batch files will be executed to copy the files from the IRIS Server.", vbCritical, "File Check Failed")


'    Y = Shell("C:\copyconfig.bat", vbNormalFocus)
'    MyTime = Format(Now(), "mm/dd/yyyy") + " " + Format(Now(), "hh:mm AMPM")
'    LogText = MyTime + " - CopyConfig Executed Automatically"
'    Open "c:\iris\log\BNEHeartSIP.LOG" For Append As #1
'        Print #1, LogText
'    Close #1



  End Sub
