VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Vision 500 Count Down"
   ClientHeight    =   4140
   ClientLeft      =   210
   ClientTop       =   10515
   ClientWidth     =   8475
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   8475
   Begin VB.Timer Timer1 
      Interval        =   9999
      Left            =   7320
      Top             =   1320
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Week"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   4200
      TabIndex        =   5
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Weeks"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4200
      TabIndex        =   4
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Days"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5520
      TabIndex        =   3
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Months"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2640
      TabIndex        =   2
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Days"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   5520
      TabIndex        =   1
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      Caption         =   "Months"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   2640
      TabIndex        =   0
      Top             =   840
      Width           =   1095
   End
   Begin VB.Image Image1 
      Height          =   4785
      Left            =   -120
      Picture         =   "Vision500.frx":0000
      Top             =   -360
      Width           =   8655
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Form_Load()
VisionDate = 12 / 28 / 2008
Label1 = DateDiff("m", Now, "12/28/2008 00:00:00")
Label10 = DateDiff("ww", Now, "12/28/2008 00:00:00")
Label2 = DateDiff("d", Now, "12/28/2008 00:00:00")
Form1.Caption = "Vision 500 Count Down: " + Label1.Caption + " Months " + Label10.Caption + " Weeks " + Label2.Caption + " Days"
End Sub

Private Sub Label7_Click()

End Sub

Private Sub Timer1_Timer()
VisionDate = 12 / 28 / 2008
Label1 = DateDiff("m", Now, "12/28/2008 00:00:00")
Label10 = DateDiff("ww", Now, "12/28/2008 00:00:00")
Label2 = DateDiff("d", Now, "12/28/2008 00:00:00")
Form1.Caption = "Vision 500 Count Down: " + Label1.Caption + " Months " + Label10.Caption + " Weeks " + Label2.Caption + " Days"
Form1.Refresh
End Sub
