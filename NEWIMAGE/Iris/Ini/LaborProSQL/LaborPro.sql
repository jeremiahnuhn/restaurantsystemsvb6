--Fix L102 so that BNERefNum exists instead of HOSSRefNum

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_LaborScheduledByHoursPolling]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_LaborScheduledByHoursPolling]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO





CREATE    	PROCEDURE sp_LaborScheduledByHoursPolling 
(	@strDate varchar(25)
) AS
-- by KK on 12/08/2004 Labor Scheduled by Hours Polling for BNE

DECLARE @dtDate datetime
DECLARE @StoreNum int
SET @dtDate = CONVERT(datetime, @strDate)
SET @StoreNum = (SELECT TOP 1 Storenum FROM IRIS.dbo.tblStoreInfo)

-- Create Temp Table to fill and for recordset output
CREATE TABLE #LaborBy15Min (
	[BusinessDate] [DateTime] NOT NULL,
	JobCode int NOT NULL,
	[TimeFrame15] [DateTime] NOT NULL,
	[ScheduleID] [int] NOT NULL,
	[LaborCost] [decimal](10, 2) NULL,
	[LaborHrs] [decimal](10, 2) NULL
) ON [PRIMARY]

-- Create table for LaborCost & OT values
CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 2) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]

DECLARE @ScheduleID int 
DECLARE @DayIterator int
DECLARE	@ScheduleStartDateTime datetime
DECLARE @dtTimeIterator DateTime
DECLARE @WeekStartTimeID AS int

SELECT @ScheduleID = ScheduleID
FROM tbllsschedules  
WHERE @dtDate BETWEEN StartDate AND EndDate

-- Set Values
SET @WeekStartTimeID = dbo.fnLsGetWeekStartTimeID()
SET @ScheduleStartDateTime = (SELECT StartDate FROM tblLsSchedules WHERE ScheduleID = @ScheduleID)
SET @ScheduleStartDateTime = DATEADD(Minute, 15 * @WeekStartTimeID, @ScheduleStartDateTime)
SET @dtTimeIterator = @ScheduleStartDateTime

-- Fill temp table with payroll calculations
INSERT INTO #ScheduleTotals
EXECUTE spLsSelectPayrollCalculations @ScheduleID


-- Create #ScheduleTotalsDated from #ScheduleTotals with added datetimes
SELECT ST.*,
	DATEADD(Day, ST.DayNumber-1, Sch.StartDate) AS BusinessDate,
	DATEADD(Day, ST.DayNumber-1, Sch.StartDate) AS dtStartTime,
	DATEADD(Day, ST.DayNumber-1, Sch.StartDate) AS dtEndTime
	INTO #ScheduleTotalsDated
FROM  #ScheduleTotals AS ST
	INNER JOIN tblLsSchedules AS Sch ON ST.ScheduleID = Sch.ScheduleID
        INNER JOIN dbo.tblLsEmployees Emp ON ST.EmployeeID = Emp.EmployeeID


-- Adjust Start & End Date if based on WeekStartTime
IF (@WeekStartTimeID < 0)
BEGIN
	-- Day starts prior day
	UPDATE #ScheduleTotalsDated
	SET dtStartTime = DATEADD(Day, -1, dtStartTime),
	    dtEndTime = DATEADD(Day, -1, dtEndTime)
	WHERE StartTime >= @WeekStartTimeID + 96

	-- Adjust prior schedule dates
	UPDATE #ScheduleTotalsDated
	SET businessdate = DATEADD(Day, 7, businessdate),
	    dtStartTime = DATEADD(Day, 7, dtStartTime),
	    dtEndTime = DATEADD(Day, 7, dtEndTime)
	WHERE ScheduleID <> @ScheduleID
END
ELSE
BEGIN
	-- Day starts at midnight current day or during current day
	UPDATE #ScheduleTotalsDated
	SET dtStartTime = DATEADD(Day, 1, dtStartTime),
	    dtEndTime = DATEADD(Day, 1, dtEndTime)
	WHERE StartTime < @WeekStartTimeID

	-- Adjust week after schedule dates
	UPDATE #ScheduleTotalsDated
	SET businessdate = DATEADD(Day, -7, businessdate),
	    dtStartTime = DATEADD(Day, -7, dtStartTime),
	    dtEndTime = DATEADD(Day, -7, dtEndTime)
	WHERE ScheduleID <> @ScheduleID
END

-- Adjust Start & End DateTimes to add the time to the dates
UPDATE #ScheduleTotalsDated
SET dtStartTime = DATEADD(Minute, StartTime*15, dtStartTime),
    dtEndTime = DATEADD(Minute, EndTime*15, dtEndTime)


-- Iterate each 60 min interval
WHILE (@dtTimeIterator < DATEADD(Day, 7, @ScheduleStartDateTime))
BEGIN
	INSERT INTO #LaborBy15Min
	SELECT *
	FROM
	(
		SELECT 	Min(BusinessDate) AS BusinessDate,
			JobID AS JobCode,
			@dtTimeIterator AS dtTimeIterator,
		 	ScheduleID AS ScheduleID,
			CONVERT(decimal(10,2), ROUND(SUM(CASE WHEN LaborHrs = 0 THEN 0 ELSE ST.LaborCost/ST.LaborHrs END), 2)) AS LaborCost,
			CONVERT(decimal(10,2), SUM(1)) AS LaborHrs
		FROM 	#ScheduleTotalsDated ST
		WHERE   (StartTime <= EndTime AND @dtTimeIterator >= dtStartTime AND @dtTimeIterator < dtEndTime )
			OR (StartTime > EndTime AND @dtTimeIterator >= dtStartTime AND @dtTimeIterator < DATEADD(day,1,dtEndTime) )

		GROUP BY ScheduleID, ST.JobID
	) AS TempRS
	WHERE BusinessDate IS NOT NULL

	SET @dtTimeIterator = DATEADD(Minute, 60, @dtTimeIterator)
END

SELECT 'L102' AS BNERefNum,
	@StoreNum AS StoreNum, 
	(DATEPART(year, Businessdate)*10000) + (DATEPART(month, Businessdate)*100) + DATEPART(day, Businessdate) AS BusinessDate,
	JobCode, 
	DATEPART(HOUR, TimeFrame15) AS TimeHour, 
	LaborHrs AS LaborHoursScheduled, LaborCost AS LaborCostScheduled 
FROM #LaborBy15Min 
WHERE BusinessDate = @dtDate 
ORDER BY JobCode
, TimeHour


DROP TABLE #ScheduleTotals
DROP TABLE #ScheduleTotalsDated
DROP TABLE #LaborBy15Min





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

