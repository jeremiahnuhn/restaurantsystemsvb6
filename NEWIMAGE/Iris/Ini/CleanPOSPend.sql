[Database]
Database=..\data\pospend.mdb
Description=PurgePOSPend

[SQLStatement]
SQL1=DELETE * FROM tblBusinessDateUpdate
SQL2=DELETE * FROM tblPendingDrawer
SQL3=DELETE * FROM tblPendingEmployee
SQL4=DELETE * FROM tblPendingEOD
SQL5=DELETE * FROM tblPendingOrders
