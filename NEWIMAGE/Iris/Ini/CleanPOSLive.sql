[Database]
Database=..\data\poslive.mdb
Description=PurgePOSLive

[SQLStatement]
SQL1=DELETE * FROM tblOrder
SQL2=DELETE * FROM tblCashManagement
SQL3=DELETE * FROM tblDrawerNum
SQL4=DELETE * FROM tblGrandTotal
SQL5=DELETE * FROM tblDrawerServer
SQL6=DELETE * FROM tblDrawerTotal
SQL7=DELETE * FROM tblDrawerTrans
SQL8=DELETE * FROM tblnextnum