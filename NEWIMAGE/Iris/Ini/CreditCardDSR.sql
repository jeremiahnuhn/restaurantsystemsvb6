if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUST_CREDITCARDSTOTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CUST_CREDITCARDSTOTAL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUST_CREDITCARDSTOTAL1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CUST_CREDITCARDSTOTAL1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUST_DEPOSITED_PAYMENTS_AMT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CUST_DEPOSITED_PAYMENTS_AMT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUST_TOTAL_REVENUE_AMT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CUST_TOTAL_REVENUE_AMT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUST_TOT_REVENUE_AMT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CUST_TOT_REVENUE_AMT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUST_TOT_REVENUE_AMT_MINUS_PROMO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CUST_TOT_REVENUE_AMT_MINUS_PROMO]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_dkExecDataKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_dkExecDataKey]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE CUST_CREDITCARDSTOTAL
	 (@FromDateString varchar(15) , @ToDateString varchar(15) , @Amount money OUTPUT, @CreateDataKeyValues smallint) AS
Declare @FromDate datetime, @ToDate datetime
Set @FromDate = Convert(Datetime, @FromDateString, 101)
Set @ToDate = Convert(Datetime, @ToDateString, 101)
Declare @TotalAmt money
SELECT @TotalAmt = Sum(tblOrderPay.Amount)
FROM tblOrder 	INNER JOIN (tblOrderPay 
			INNER JOIN tbl_PayType 
			ON tblOrderPay.PayType = tbl_PayType.PayTypeID) 
		ON (tblOrder.OrderNum = tblOrderPay.OrderNum) 
		AND (tblOrder.RegisterNum = tblOrderPay.RegisterNum) 
		AND (tblOrder.BusinessDate = tblOrderPay.BusinessDate)
WHERE (((tblOrder.State)=1 Or (tblOrder.State)=128 Or (tblOrder.State)=32))
	AND tblOrder.BusinessDate >= @FromDate
	AND tblOrder.BusinessDate <= @ToDate
              AND tblOrderPay.PayType <> 100

set @TotalAmt = IsNull(@TotalAmt,0)
set @Amount =  @TotalAmt
/*
 Insert value into tbl_datakey_values
*/
if(@CreateDatakeyValues = 1)
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'CREDIT_CARDS_AMT',NULL,@Amount








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE CUST_CREDITCARDSTOTAL1(@FromDateString varchar(15) , @ToDateString varchar(15) , @Amount money OUTPUT, @CreateDataKeyValues smallint) AS
Declare @FromDate datetime, @ToDate datetime
Set @FromDate = Convert(Datetime, @FromDateString, 101)
Set @ToDate = Convert(Datetime, @ToDateString, 101)
Declare @TotalAmt money
SELECT @TotalAmt = Sum(tblOrderPay.Amount)
FROM tblOrder  INNER JOIN (tblOrderPay INNER JOIN tbl_PayType
   ON tblOrderPay.PayType = tbl_PayType.PayTypeID)
   ON (tblOrder.OrderNum = tblOrderPay.OrderNum)
   AND (tblOrder.RegisterNum = tblOrderPay.RegisterNum)
   AND (tblOrder.BusinessDate = tblOrderPay.BusinessDate)
WHERE   (((tblOrder.State) =1 Or (tblOrder.State)=128 Or (tblOrder.State)=32))
   AND tblOrder.BusinessDate >= @FromDate
   AND tblOrder.BusinessDate <= @ToDate
      AND tblOrderPay.PayType <> 100

Set @TotalAmt = IsNull(@TotalAmt,0)
Set @Amount = @TotalAmt
/*
Insert value into tbl_datakey_values
*/
if(@CreateDatakeyValues = 1)
EXEC sp_dkInsertDataKeyValue @FromDate,0, 'CREDIT_CARDS_AMT' ,NULL,@Amount
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE CUST_DEPOSITED_PAYMENTS_AMT
 (@FromDateString varchar(15) , @ToDateString varchar(15) , @Amount money OUTPUT, @CreateDataKeyValues smallint) AS
Declare @FromDate datetime, @ToDate datetime
Set @FromDate = Convert(Datetime, @FromDateString, 101)
Set @ToDate = Convert(Datetime, @ToDateString, 101)
Declare @TotalAmt money
Declare @Desc varchar (50)
Declare @Descc varchar(50)
Declare listCursor CURSOR FOR
SELECT tblEODDeposit.DepositTypeID AS [Desc],
               Sum(tblEODDepositTrans.Amount) AS Amount
FROM (tblEODDeposit
 LEFT JOIN (tblEODDepositTrans 
  LEFT JOIN tbl_PayType
              ON tblEODDepositTrans.PayTypeID = tbl_PayType.PayTypeID) 
 ON tblEODDeposit.DepositNum = tblEODDepositTrans.DepositNum) 
WHERE (((tblEODDeposit.State)<>2) AND ((tblEODDepositTrans.State)<>2))
  AND tblEODDeposit.BusinessDate >= @FromDate 
 AND tblEODDeposit.BusinessDate <= @ToDate
             AND tblEODDeposit.DepositTypeID = 1
GROUP BY tblEODDeposit.BusinessDate, tblEODDeposit.EmployeeID,tblEODDeposit.DepositTypeID
OPEN listCursor
Set @Amount = IsNull(@Amount,0)
Set @Desc = 0
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
While(@@FETCH_STATUS=0)
Begin
if(@CreateDatakeyValues = 1)
Begin
   if(@Desc = 1)
    Set @Desc = "Breakfast"
End
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'DEPOSITED_PAYMENTS_AMT',@Desc,@TotalAmt
Set @Amount = @Amount + 1
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
End
CLOSE listCursor
DEALLOCATE listCursor
Declare listCursor CURSOR FOR
SELECT tblEODDeposit.DepositTypeID AS [Desc],
               Sum(tblEODDepositTrans.Amount) AS Amount
FROM (tblEODDeposit
 LEFT JOIN (tblEODDepositTrans 
  LEFT JOIN tbl_PayType
              ON tblEODDepositTrans.PayTypeID = tbl_PayType.PayTypeID) 
 ON tblEODDeposit.DepositNum = tblEODDepositTrans.DepositNum) 
WHERE (((tblEODDeposit.State)<>2) AND ((tblEODDepositTrans.State)<>2))
  AND tblEODDeposit.BusinessDate >= @FromDate 
 AND tblEODDeposit.BusinessDate <= @ToDate
             AND tblEODDeposit.DepositTypeID = 2
GROUP BY tblEODDeposit.BusinessDate, tblEODDeposit.EmployeeID,tblEODDeposit.DepositTypeID
OPEN listCursor
Set @Amount = IsNull(@Amount,0)
Set @Desc = 0
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
While(@@FETCH_STATUS=0)
Begin
if(@CreateDatakeyValues = 1)
Begin
   if(@Desc = 2)
    Set @Desc = "End of Day"
End
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'DEPOSITED_PAYMENTS_AMT',@Desc,@TotalAmt
Set @Amount = @Amount + 1
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
End
CLOSE listCursor
DEALLOCATE listCursor
Declare listCursor CURSOR FOR
SELECT tblEODDeposit.DepositTypeID AS [Desc],
               Sum(tblEODDepositTrans.Amount) AS Amount
FROM (tblEODDeposit
 LEFT JOIN (tblEODDepositTrans 
  LEFT JOIN tbl_PayType
              ON tblEODDepositTrans.PayTypeID = tbl_PayType.PayTypeID) 
 ON tblEODDeposit.DepositNum = tblEODDepositTrans.DepositNum) 
WHERE (((tblEODDeposit.State)<>2) AND ((tblEODDepositTrans.State)<>2))
  AND tblEODDeposit.BusinessDate >= @FromDate 
 AND tblEODDeposit.BusinessDate <= @ToDate
             AND tblEODDeposit.DepositTypeID = 4
GROUP BY tblEODDeposit.BusinessDate, tblEODDeposit.EmployeeID,tblEODDeposit.DepositTypeID
OPEN listCursor
Set @Amount = IsNull(@Amount,0)
Set @Desc = 0
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
While(@@FETCH_STATUS=0)
Begin
if(@CreateDatakeyValues = 1)
Begin
   if(@Desc = 4)
    Set @Desc = "Midnight"
End
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'DEPOSITED_PAYMENTS_AMT',@Desc,@TotalAmt
Set @Amount = @Amount + 1
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
End
CLOSE listCursor
DEALLOCATE listCursor
Declare listCursor CURSOR FOR
SELECT tblEODDeposit.DepositTypeID AS [Desc],
               Sum(tblEODDepositTrans.Amount) AS Amount
FROM (tblEODDeposit
 LEFT JOIN (tblEODDepositTrans 
  LEFT JOIN tbl_PayType
              ON tblEODDepositTrans.PayTypeID = tbl_PayType.PayTypeID) 
 ON tblEODDeposit.DepositNum = tblEODDepositTrans.DepositNum) 
WHERE (((tblEODDeposit.State)<>2) AND ((tblEODDepositTrans.State)<>2))
  AND tblEODDeposit.BusinessDate >= @FromDate 
 AND tblEODDeposit.BusinessDate <= @ToDate
             AND tblEODDeposit.DepositTypeID = 5
GROUP BY tblEODDeposit.BusinessDate, tblEODDeposit.EmployeeID,tblEODDeposit.DepositTypeID
OPEN listCursor
Set @Amount = IsNull(@Amount,0)
Set @Desc = 0
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
While(@@FETCH_STATUS=0)
Begin
if(@CreateDatakeyValues = 1)
Begin
   if(@Desc = 5)
    Set @Desc = "Night"
End
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'DEPOSITED_PAYMENTS_AMT',@Desc,@TotalAmt
Set @Amount = @Amount + 1
FETCH NEXT FROM listCursor INTO @Desc,@TotalAmt
End
CLOSE listCursor
DEALLOCATE listCursor
if(@Amount = 0)
Begin
 Set @TotalAmt = 0
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'DEPOSITED_PAYMENTS_AMT',NULL,@TotalAmt
End




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





CREATE PROCEDURE CUST_TOTAL_REVENUE_AMT
 (@FromDateString varchar(15), @ToDateString varchar(15), @Amount money OUTPUT, @CreateDatakeyValues smallint)  AS
Declare @FromDate Datetime
Set @FromDate = CONVERT(Datetime, @FromDateString,101)
Declare @PAID_ORDERS_AMT money, @Amount1 money, @Amount2 money, @ITEM_REFUND money, @DISCOUNT_AMT money, @PAID_IN_AMT money,
 @PAID_OUT_AMT money, @GIFT_CERTIFICATE_REDEEM_AMT money
exec CUST_NET_SALES_AMT @FromDateString, @ToDateString, @PAID_ORDERS_AMT OUTPUT, 0
exec sp_dkPAID_IN_AMT @FromDateString, @ToDateString, @PAID_IN_AMT OUTPUT, 0
exec sp_dkPAID_OUT_AMT  @FromDateString, @ToDateString, @PAID_OUT_AMT OUTPUT, 0
exec sp_dkPAID_SALES_TAX_AMT @FromDateString, @ToDateString, @Amount1 OUTPUT, 0
exec CUST_CREDITCARDSTOTAL @FromDateString, @ToDateString, @Amount2 OUTPUT, 0
Set @Amount =  (@PAID_ORDERS_AMT+@PAID_IN_AMT-@PAID_OUT_AMT+@Amount1-@Amount2)*-1

/*
 Insert value into tbl_datakey_values
*/
if(@CreateDatakeyValues = 1)
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'TOTAL_REVENUE_AMT',NULL,@Amount






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





CREATE PROCEDURE CUST_TOT_REVENUE_AMT
 (@FromDateString varchar(15), @ToDateString varchar(15), @Amount money OUTPUT, @CreateDatakeyValues smallint)  AS
Declare @FromDate Datetime
Set @FromDate = CONVERT(Datetime, @FromDateString,101)
Declare @PAID_ORDERS_AMT money, @Amount1 money, @Amount2 money, @ITEM_REFUND money, @DISCOUNT_AMT money, @PAID_IN_AMT money,
 @PAID_OUT_AMT money, @GIFT_CERTIFICATE_REDEEM_AMT money
exec CUST_NET_SALES_AMT @FromDateString, @ToDateString, @PAID_ORDERS_AMT OUTPUT, 0
exec sp_dkPAID_IN_AMT @FromDateString, @ToDateString, @PAID_IN_AMT OUTPUT, 0
exec sp_dkPAID_OUT_AMT  @FromDateString, @ToDateString, @PAID_OUT_AMT OUTPUT, 0
exec sp_dkPAID_SALES_TAX_AMT @FromDateString, @ToDateString, @Amount1 OUTPUT, 0
exec CUST_CREDITCARDSTOTAL @FromDateString, @ToDateString, @Amount2 OUTPUT, 0
Set @Amount =  @PAID_ORDERS_AMT +@PAID_IN_AMT-@PAID_OUT_AMT+@Amount1-@Amount2
/*
 Insert value into tbl_datakey_values
*/
if(@CreateDatakeyValues = 1)
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'TOT_REVENUE_AMT',NULL,@Amount






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO






CREATE PROCEDURE CUST_TOT_REVENUE_AMT_MINUS_PROMO
  (@FromDateString varchar(15), @ToDateString varchar(15), @Amount money OUTPUT, @CreateDatakeyValues smallint)  AS
Declare @FromDate Datetime
Set @FromDate = CONVERT(Datetime, @FromDateString,101)
Declare @PAID_ORDERS_AMT money, @Amount1 money, @ITEM_REFUND money, @DISCOUNT_AMT money, @PAID_IN_AMT money,
 @PAID_OUT_AMT money, @GIFT_CERTIFICATE_REDEEM_AMT money, @PROMO_AMT money
exec CUST_NET_SALES_AMT @FromDateString, @ToDateString, @PAID_ORDERS_AMT OUTPUT, 0
exec CUST_PROMO_ITEMS_AMT @FromDateString, @ToDateString, @PROMO_AMT OUTPUT, 0
Set @Amount =  @PAID_ORDERS_AMT -@PROMO_AMT
/*
 Insert value into tbl_datakey_values
*/
if(@CreateDatakeyValues = 1)
 EXEC sp_dkInsertDataKeyValue @FromDate,0,'TOT_REVENUE_AMT_MINUS_PROMO',NULL,@Amount





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO







CREATE PROCEDURE sp_dkExecDataKey
 (@DataKeyName varchar(50), @FromDateString varchar(15) , @ToDateString varchar(15) , @Amount money OUTPUT, @CreateDataKeyValues smallint) AS
IF @DataKeyName = 'BEGINNING_GRAND_TOTAL_AMT'
Begin
 EXEC sp_dkBEGINNING_GRAND_TOTAL_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'credit_cards_amt'
Begin
 EXEC CUST_creditcardstotal @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'TIME_BREAKFAST'
Begin
 EXEC CUST_TIME_BREAKFAST @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'TIME_LATENIGHT'
Begin
 EXEC CUST_TIME_LATENIGHT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'TIME_DINNER'
Begin
 EXEC CUST_TIME_DINNER @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'TIME_AFTERNOON'
Begin
 EXEC CUST_TIME_AFTERNOON @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'TIME_LUNCH'
Begin
 EXEC CUST_TIME_LUNCH @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'TIME_MIDNIGHT'
Begin
 EXEC CUST_TIME_MIDNIGHT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'TIME_REGULAR'
Begin
 EXEC CUST_TIME_REGULAR @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'TIME_OVERALL'
Begin
 EXEC CUST_TIME_OVERALL @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'CANCELLED_ITEMS_AMT'
Begin
 EXEC sp_dkCANCELLED_ITEMS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'DISCOUNT_SUMMARY'
Begin
 EXEC sp_dkDISCOUNT_SUMMARY @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End

IF @DataKeyName = 'DISCOUNT_SUMMARY_CNT'
Begin
 EXEC sp_dkDISCOUNT_SUMMARY_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End



IF @DataKeyName = 'CASH_PULL_AMT'
Begin
 EXEC sp_dkCASH_PULL_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DISCOUNT_AMT'
Begin
 EXEC sp_dkDISCOUNT_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'EMPLOYEE_DISCOUNT_AMT'
Begin
 EXEC CUST_EMPLOYEE_DISCOUNT_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DISCOUNT_CNT'
Begin
 EXEC sp_dkDISCOUNT_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'EAT_IN_SALES_AMT'
Begin
 EXEC CUST_EAT_IN_SALES @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'EAT_IN_CNT'
Begin
 EXEC CUST_EAT_IN_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DRIVE_THRU_SALES_AMT'
Begin
 EXEC CUST_DRIVE_THRU_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DRIVE_THRU_CNT'
Begin
 EXEC CUST_DRIVE_THRU_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'CARRY_OUT_SALES_AMT'
Begin
 EXEC CUST_CARRY_OUT_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'CARRY_OUT_CNT'
Begin
 EXEC CUST_CARRY_OUT_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'BREAKFAST_SALES_AMT'
Begin
 EXEC CUST_BREAKFAST_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'MIDAFTERNOON_SALES_AMT'
Begin
 EXEC CUST_MIDAFTERNOON_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DINNER_SALES_AMT'
Begin
 EXEC CUST_DINNER_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'LATENIGHT_SALES_AMT'
Begin
 EXEC CUST_LATENIGHT_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'MIDNIGHT_SALES_AMT'
Begin
 EXEC CUST_MIDNIGHT_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'LUNCH_SALES_AMT'
Begin
 EXEC CUST_LUNCH_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'CLEARED_SALES_AMT'
Begin
 EXEC CUST_CLEARED_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'CENTS_OFF_AMT'
Begin
 EXEC CUST_CENTS_OFF_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'FREE_ITEM_AMT'
Begin
 EXEC CUST_FREE_ITEM_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = '10_OFF_AMT'
Begin
 EXEC CUST_10_OFF_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = '20_OFF_AMT'
Begin
 EXEC CUST_20_OFF_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = '30_OFF_AMT'
Begin
 EXEC CUST_30_OFF_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DONATION_DOLLARS_AMT'
Begin
 EXEC CUST_DONATION_DOLLARS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'CENTS_OFF_CNT'
Begin
 EXEC CUST_CENTS_OFF_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'FREE_ITEM_CNT'
Begin
 EXEC CUST_FREE_ITEM_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = '10_OFF_CNT'
Begin
 EXEC CUST_10_OFF_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = '20_OFF_CNT'
Begin
 EXEC CUST_20_OFF_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = '30_OFF_CNT'
Begin
 EXEC CUST_30_OFF_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DONATION_DOLLARS_CNT'
Begin
 EXEC CUST_DONATION_DOLLARS_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'EMPLOYEE_MEAL_CNT'
Begin
 EXEC CUST_EMPLOYEE_MEAL_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'REVENUE_CALC'
Begin
 EXEC CUST_REVENUE_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'MIDAFTERNOON_SALES_CNT'
Begin
 EXEC CUST_MIDAFTERNOON_SALES_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'DINNER_SALES_CNT'
Begin
 EXEC CUST_DINNER_SALES_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'LATENIGHT_SALES_CNT'
Begin
 EXEC CUST_LATENIGHT_SALES_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'MIDNIGHT_SALES_CNT'
Begin
 EXEC CUST_MIDNIGHT_SALES_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'BREAKFAST_SALES_CNT'
Begin
 EXEC CUST_BREAKFAST_SALES_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'LUNCH_SALES_CNT'
Begin
 EXEC CUST_LUNCH_SALES_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'PROMO_ITEMS'
Begin
 EXEC CUST_PROMO_ITEMS @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'ITEM_REFUND'
Begin
 EXEC CUST_ITEM_REFUND @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'ITEM_REFUND_CNT'
Begin
 EXEC CUST_ITEM_REFUND_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'NET_SALES_AMT'
Begin
 EXEC CUST_NET_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'TOTAL_REVENUE_AMT'
Begin
 EXEC CUST_TOTAL_REVENUE_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'WASTE_ITEM_AMT'
Begin
 EXEC CUST_WASTE_ITEM_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'WASTE_ITEM_CNT'
Begin
 EXEC CUST_WASTE_ITEM_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'TOT_REVENUE_AMT'
Begin
 EXEC CUST_TOT_REVENUE_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'TOT_REVENUE_AMT_MINUS_PROMO'
Begin
 EXEC CUST_TOT_REVENUE_AMT_MINUS_PROMO @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'ENDING_GRAND_TOTAL_AMT'
Begin
 EXEC sp_dkENDING_GRAND_TOTAL_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'GIFT_CERTIFICATE_REDEEM_AMT'
Begin
 EXEC sp_dkGIFT_CERTIFICATE_REDEEM_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'GIFT_CERTIFICATE_REDEEM_CNT'
Begin
 EXEC sp_dkGIFT_CERTIFICATE_REDEEM_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'GROSS_SALES_CALC'
Begin
 EXEC sp_dkGROSS_SALES_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'LIST_COLLECTED_PAYMENTS_AMT'
Begin
 EXEC sp_dkLIST_COLLECTED_PAYMENTS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'LIST_COUNTED_PAYMENTS_AMT'
Begin
 EXEC sp_dkLIST_COUNTED_PAYMENTS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'DEPOSITED_PAYMENTS_AMT'
Begin
 EXEC CUST_DEPOSITED_PAYMENTS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'OVERSHORT_AMT'
Begin
 EXEC CUST_OVERSHORT_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'NET_SALES_CALC'
Begin
 EXEC sp_dkNET_SALES_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'NO_SALE_CNT'
Begin
 EXEC sp_dkNO_SALE_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'OVERRING_ORDERS_AMT'
Begin
 EXEC sp_dkOVERRING_ORDERS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'OVERRING_ORDERS_CNT'
Begin
 EXEC sp_dkOVERRING_ORDERS_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'PAID_DISCOUNTS_AMT'
Begin
 EXEC sp_dkPAID_DISCOUNTS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF @DataKeyName = 'PAID_GUEST_CNT'
Begin
 EXEC sp_dkPAID_GUEST_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAIDIN_AMT'
Begin
 EXEC CUST_PAID_IN_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAID_IN_AMT'
Begin
 EXEC sp_dkPAID_IN_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAID_IN_CNT'
Begin
 EXEC sp_dkPAID_IN_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAID_ORDERS_AMT'
Begin
 EXEC sp_dkPAID_ORDERS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAID_ORDERS_CNT'
Begin
 EXEC sp_dkPAID_ORDERS_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAIDOUT_AMT'
Begin
 EXEC CUST_PAID_OUT_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAID_OUT_AMT'
Begin
 EXEC sp_dkPAID_OUT_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAID_OUT_CNT'
Begin
 EXEC sp_dkPAID_OUT_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'PAID_SALES_TAX_AMT'
Begin
 EXEC sp_dkPAID_SALES_TAX_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'REFUNDED_ITEMS_AMT'
Begin
 EXEC sp_dkREFUNDED_ITEMS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'REFUNDED_ORDERS_AMT'
Begin
 EXEC sp_dkREFUNDED_ORDERS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'REFUNDED_ORDERS_CNT'
Begin
 EXEC sp_dkREFUNDED_ORDERS_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'TAX_EXEMPT_SALES_AMT'
Begin
 EXEC sp_dkTAX_EXEMPT_SALES_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'TOTAL_REFUNDS_CALC'
Begin
 EXEC sp_dkTOTAL_REFUNDS_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'TOTAL_REVENUE_CALC'
Begin
 EXEC sp_dkTOTAL_REVENUE_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'TOTAL_SALES_CALC'
Begin
 EXEC sp_dkTOTAL_SALES_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'TOTAL_VOIDS_CALC'
Begin
 EXEC sp_dkTOTAL_VOIDS_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'LIST_DEPOSITED_PAYMENTS_AMT'
Begin
 EXEC sp_dkLIST_DEPOSITED_PAYMENTS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'TOTAL_REVENUE_CALC'
Begin
 EXEC sp_dkTOTAL_REVENUE_CALC @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'VOIDED_ITEMS_AMT'
Begin
 EXEC sp_dkVOIDED_ITEMS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'VOIDED_ORDERS_AMT'
Begin
 EXEC sp_dkVOIDED_ORDERS_AMT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
IF  @DataKeyName = 'VOIDED_ORDERS_CNT'
Begin
 EXEC sp_dkVOIDED_ORDERS_CNT @FromDateString,@ToDateString,@Amount OUTPUT, @CreateDataKeyValues
 RETURN 
End
Declare @FromDate datetime
IF @DataKeyName = 'BLANK_LINE'
Begin
 Set @Amount = 0
 Set @FromDate = CONVERT(datetime,@FromDateString,101)
 EXEC sp_dkInsertDataKeyValue @FromDate,0,@DataKeyName,NULL,@Amount
 RETURN
End
 
/* If data key was not found insert message */
Set @Amount = 0
Set @FromDate = CONVERT(datetime,@FromDateString,101)
EXEC sp_dkInsertDataKeyValue @FromDate,0,@DataKeyName,'Data Key Not Found',@Amount


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

