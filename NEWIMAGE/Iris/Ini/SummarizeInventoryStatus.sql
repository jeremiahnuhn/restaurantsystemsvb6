[Database]
Database=..\data\Inventory.mdb
Description=Summarizing Inventory Transactions
[SQLStatement]
SQL1=INSERT INTO tblInventoryStatusArchive (InvID, TranID, UpdDate, Data, InOut, EmployeeID, BusinessDate, 
Custom1, Custom2, Custom3, IsBegining) SELECT InvID, TranID, UpdDate, Data, InOut, EmployeeID, BusinessDate, 
Custom1, Custom2, Custom3, IsBegining FROM tblInventoryStatus WHERE BusinessDate <= (DateValue(now()) - 2) AND 
InOut<>0 AND Archived = 0 AND TranID<>4 AND TranID<>5 AND TranID<>7

SQL2=DELETE FROM tblInventoryStatus WHERE BusinessDate <= (DateValue(now()) - 2) AND InOut<>0 AND TranID<>4 AND 
TranID<>5 AND TranID<>7 AND Archived=0

SQL3=INSERT INTO tblInventoryStatus ( InvID, TranID, UpdDate, BusinessDate, Data, InOut, Archived )
SELECT sarch.InvID, sarch.TranID, Last(sarch.UpdDate) AS UpdDate, sarch.BusinessDate, SUM(sarch.Data) AS Data, sarch.InOut, Yes AS Archived
FROM tblInventoryStatusArchive AS sarch
WHERE sarch.Archived=0
GROUP BY sarch.InvID, sarch.TranID, sarch.BusinessDate, sarch.InOut

SQL4=UPDATE TBLINVENTORYSTATUSARCHIVE SET ARCHIVED = 1 WHERE ARCHIVED=0