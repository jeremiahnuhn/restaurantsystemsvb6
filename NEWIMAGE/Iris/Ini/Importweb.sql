-- Import transaction file numbers from EDM database to EdmWeb database

use Edm
select * from cdmlocation where cdmlocid=0;

declare @SENTTFN int
set @SENTTFN = (select SENTTFN from cdmlocation where CDMLOCID=0)

declare @RCVDTFN int
set @RCVDTFN = (select RCVDTFN from cdmlocation where CDMLOCID=0)

use EdmWeb

update cdmlocation set SENTTFN=@SENTTFN, RCVDTFN=@RCVDTFN
select * from cdmlocation where siteid=0;
