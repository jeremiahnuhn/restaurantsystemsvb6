' ------------------------------------------------------------------------
'               Copyright (C) 2002 Progressive Software, Inc.
'
'                          Perform End of Day Script
'
' Author:  Jim Davidson
' Date: 5/1/2002
'
' ********************************************************************************
'##### Start check for x64 System #####
'This will check for a x64 System and restart the script using the 32bit version if needed
'xsCheckArchitecture v1.0
Sub xsCheckArchitecture
   On Error Resume Next
   If Not IsObject(WScript) Then Exit Sub
   Dim oWshShell, oWshProcEnv, oExec, Argument, sCmd, iResult
   Set oWshShell =  CreateObject("WScript.Shell")
   Set oWshProcEnv = oWshShell.Environment("Process")
   If LCase(oWshProcEnv("PROCESSOR_ARCHITECTURE")) <> "x86" and Instr(1, WScript.FullName, "SysWOW64", 1) = 0 then
      sCmd = Chr(34) & oWshProcEnv("SystemRoot") & "\SysWOW64\" &  Mid(WScript.FullName, InstrRev(WScript.FullName, "\") + 1) & Chr(34)
      sCmd = sCmd & " //NoLogo " & Chr(34) & WScript.ScriptFullName & Chr(34)
      For Each Argument in WScript.Arguments
         Argument = Split(Argument,"=",2)
         sCmd = sCmd & " " & Chr(34) & Argument(0) & Chr(34)
         If UBound(Argument) = 1 Then sCmd = sCmd & "=" & Chr(34) & Argument(1) & Chr(34)
      Next
      Set oExec = oWshShell.Exec(sCmd)
      Do While oExec.Status = 0
         Do While Not oExec.StdOut.AtEndOfStream
            WScript.StdOut.Write oExec.StdOut.Read(1)
         Loop
      Loop
      Do While Not oExec.StdErr.AtEndOfStream
         WScript.StdErr.Write oExec.StdErr.Read(1)
      Loop
      If Not oExec.ExitCode = 0  then WScript.Quit oExec.ExitCode
      WScript.quit
   End If
End Sub
xsCheckArchitecture
'##### End check for x64 System #####

' COM Objects
Dim WSHShell
Dim fso 
Dim MessageScreen

'Global Variables
Dim irisPath
Dim LogDetail
Dim showProgress
Dim result
Dim PreScript
Dim PostScript
Dim MachineType
Dim IExplorer
Dim CloseMsgScreen

'Constants
Const Success = 0
Const Error = 1
Const Warning = 2
Const Info = 4

'*************************************************************
' THIS IS THE MAIN SCRIPT LOGIC. 
'
' We exit if any step fails, with a status of 1.  If everything
' succeeds, our exit status is 0.
'*************************************************************
   showProgress=0  'Make this 1 to display a messagebox before performing each step.
                   'Otherwise, 0.
   LogDetail = 0   'Make this 1 to output additional log information to application log

   On Error Resume Next

  'Create the COM objects we will use in this script
   If showProgress Then MsgBox "Creating COM Objects"
   result=CreateCOMObjects()
   If result<>0 Then GiveUp()

   If LogDetail = 1 Then     
     'Log beginning of script
      If showProgress Then MsgBox "Log start of script"
      OutputLog Info,"Executing End of Day script."
   End If

  'Get the IRIS directory path and machine type
   If showProgress Then MsgBox "Determining IRIS path and machine type"
   result=GetIRISDirectory()
   If result<>0 Then GiveUp()

  'Display html message with Preparing System message
   If showProgress Then MsgBox "Opening html page"
   result=OpenMessageScreen()

  'Run Pre-EOD script if it exists
   PreScript = irispath & "\Scripts\PreEOD.vbs"
   If fso.FileExists(PreScript) Then
      OutputLog Info,"Launching Pre-EOD script: " & PreScript
      WSHShell.Run PreScript,,true
   End If

  'Launch End of Day
   If showProgress Then MsgBox "Executing End of Day"
   If fso.FileExists(irispath & "\bin\EODProc.exe") Then
      WSHShell.Run irispath & "\bin\EODProc.exe /ExitIfNoError /HideShell /NoTopMost /NoUI",,true
   Else
      OutputLog Error, "Unable to locate EODProc at " & irispath & "\bin\EODProc.exe.  End of Day will not be performed."
      GiveUp()
   End If

  'Run Post-EOD script if it exists
   PostScript = irispath & "\Scripts\PostEOD.vbs"
   If fso.FileExists(PostScript) Then
      OutputLog Info,"Launching Post-EOD script: " & PostScript
      WSHShell.Run PostScript,,true
   End If
	
  'End the script with success
   ExitThisScript()

'*************************************************************
' THIS IS THE END OF THE MAIN SCRIPT LOGIC
'*************************************************************
'
'*************************************************************
' Create the COM Objects we will use in this script
'
Function CreateCOMObjects()
   CreateCOMObjects = 1	'Assume failure

   On Error Resume Next

   Set WSHShell = WScript.CreateObject("WScript.Shell")
   If Err.Number Then
      If ShowProgress = 1 Then MsgBox "CreateCOMObjects(): Error creating WScript.Shell object.",16,"ERROR"
      ExitThisScript()
   End If

   Set fso = CreateObject("Scripting.FileSystemObject")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating File System Object (fso)." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Function
   End If

   CreateCOMObjects = 0	'Success
End Function
'*************************************************************
'*************************************************************
'Get the IRIS directory and machine type
'
Function GetIRISDirectory()
   GetIRISDirectory=0 'Assume success
   On Error Resume Next
   irisPath=WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")
   If Err.Number Then
      OutputLog Error,"IRIS Path is missing from registry: HKEY_LOCAL_MACHINE\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR"
      GetIRISDirectory=1
   End If
   
   MachineType = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\BOOrPOS")
   If InStr(LCase(MachineType),"pos") Or InStr(LCase(MachineType),"register") Then
      MachineType = "Register"
   Else
      MachineType = "Server"
   End If

End Function
'*************************************************************
'*************************************************************
'Open html page with please wait message

Function OpenMessageScreen()

On Error Resume Next

CloseMsgScreen = 0

   If WScript.Version >= 5.6 Then

      IExplorer = WSHShell.RegRead ("HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\IEXPLORE.EXE\Path")
     IExplorer = Left(IExplorer,Len(IExplorer) - 1)
      If Right(IExplorer,1) <> "\" Then IExplorer = IExplorer & "\"
      If fso.FileExists(IExplorer & "IEXPLORE.exe") Then
         Set MessageScreen = WSHShell.Exec(IExplorer & "IEXPLORE.exe -k " & irispath & "\Scripts\EOD.htm")
         CloseMsgScreen = 1
      Else
         OutputLog Warning, "Unable to locate IExplore.exe at " & IExplorer & "IExplore.exe.  Html page will not be displayed during script."
      End If
   Else
      OutputLog Warning, "Message screen can only be displayed with Windows Scripting 5.6 or above."
   End If
End Function
'*************************************************************
'*************************************************************
'Output specified text to log file 
'
Sub OutputLog(EventID,OutputText)
   On Error Resume Next
   WshShell.LogEvent EventID, "IRIS(" & WScript.ScriptName & "):  " & OutputText
End Sub
'*************************************************************
'*************************************************************
' Exit the script with an error
'
Sub GiveUp()
   On Error Resume Next
   OutputLog Error,"Terminating script due to error."
   ExitThisScript()
End Sub
'*************************************************************
'*************************************************************
' Exit the Script
'
Sub ExitThisScript()
   On Error Resume Next
   If CloseMsgScreen = 1 Then
      If MessageScreen.status = 0 Then MessageScreen.Terminate
      If Err.number Then OutputLog Error, "Unable to close Message Screen (html page)"
      Set MessageScreen = Nothing
   End If
   Set WSHShell = Nothing
   Set fso = Nothing
   WScript.Quit  ' end of script
End Sub

'' SIG '' Begin signature block
'' SIG '' MIIQ7QYJKoZIhvcNAQcCoIIQ3jCCENoCAQExDjAMBggq
'' SIG '' hkiG9w0CBQUAMGYGCisGAQQBgjcCAQSgWDBWMDIGCisG
'' SIG '' AQQBgjcCAR4wJAIBAQQQTvApFpkntU2P5azhDxfrqwIB
'' SIG '' AAIBAAIBAAIBAAIBADAgMAwGCCqGSIb3DQIFBQAEEIU4
'' SIG '' aRWsAf3EGP18P64ZpzGgggyQMIIDejCCAmKgAwIBAgIQ
'' SIG '' OCXX+vhhr570kOcmtdZa1TANBgkqhkiG9w0BAQUFADBT
'' SIG '' MQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNpZ24s
'' SIG '' IEluYy4xKzApBgNVBAMTIlZlcmlTaWduIFRpbWUgU3Rh
'' SIG '' bXBpbmcgU2VydmljZXMgQ0EwHhcNMDcwNjE1MDAwMDAw
'' SIG '' WhcNMTIwNjE0MjM1OTU5WjBcMQswCQYDVQQGEwJVUzEX
'' SIG '' MBUGA1UEChMOVmVyaVNpZ24sIEluYy4xNDAyBgNVBAMT
'' SIG '' K1ZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMg
'' SIG '' U2lnbmVyIC0gRzIwgZ8wDQYJKoZIhvcNAQEBBQADgY0A
'' SIG '' MIGJAoGBAMS18lIVvIiGYCkWSlsvS5Frh5HzNVRYNerR
'' SIG '' Nl5iTVJRNHHCe2YdicjdKsRqCvY32Zh0kfaSrrC1dpbx
'' SIG '' qUpjRUcuawuSTksrjO5YSovUB+QaLPiCqljZzULzLcB1
'' SIG '' 3o2rx44dmmxMCJUe3tvvZ+FywknCnmA84eK+FqNjeGkU
'' SIG '' e60tAgMBAAGjgcQwgcEwNAYIKwYBBQUHAQEEKDAmMCQG
'' SIG '' CCsGAQUFBzABhhhodHRwOi8vb2NzcC52ZXJpc2lnbi5j
'' SIG '' b20wDAYDVR0TAQH/BAIwADAzBgNVHR8ELDAqMCigJqAk
'' SIG '' hiJodHRwOi8vY3JsLnZlcmlzaWduLmNvbS90c3MtY2Eu
'' SIG '' Y3JsMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMA4GA1Ud
'' SIG '' DwEB/wQEAwIGwDAeBgNVHREEFzAVpBMwETEPMA0GA1UE
'' SIG '' AxMGVFNBMS0yMA0GCSqGSIb3DQEBBQUAA4IBAQBQxUvI
'' SIG '' JIDf5A0kwt4asaECoaaCLQyDFYE3CoIOLLBaF2G12AX+
'' SIG '' iNvxkZGzVhpApuuSvjg5sHU2dDqYT+Q3upmJypVCHbC5
'' SIG '' x6CNV+D61WQEQjVOAdEzohfITaonx/LhhkwCOE2DeMb8
'' SIG '' U+Dr4AaH3aSWnl4MmOKlvr+ChcNg4d+tKNjHpUtk2scb
'' SIG '' W72sOQjVOCKhM4sviprrvAchP0RBCQe1ZRwkvEjTRIDr
'' SIG '' oc/JArQUz1THFqOAXPl5Pl1yfYgXnixDospTzn099io6
'' SIG '' uE+UAKVtCoNd+V5T9BizVw9ww/v1rZWgDhfexBaAYMkP
'' SIG '' K26GBPHr9Hgn0QXF7jRbXrlJMvIzMIIDxDCCAy2gAwIB
'' SIG '' AgIQR78Zld+NUkZD99ttSA0xpDANBgkqhkiG9w0BAQUF
'' SIG '' ADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rl
'' SIG '' cm4gQ2FwZTEUMBIGA1UEBxMLRHVyYmFudmlsbGUxDzAN
'' SIG '' BgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhhd3RlIENl
'' SIG '' cnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1l
'' SIG '' c3RhbXBpbmcgQ0EwHhcNMDMxMjA0MDAwMDAwWhcNMTMx
'' SIG '' MjAzMjM1OTU5WjBTMQswCQYDVQQGEwJVUzEXMBUGA1UE
'' SIG '' ChMOVmVyaVNpZ24sIEluYy4xKzApBgNVBAMTIlZlcmlT
'' SIG '' aWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EwggEi
'' SIG '' MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpyrKk
'' SIG '' zM0grwp9iayHdfC0TvHfwQ+/Z2G9o2Qc2rv5yjOrhDCJ
'' SIG '' WH6M22vdNp4Pv9HsePJ3pn5vPL+Trw26aPRslMq9Ui2r
'' SIG '' SD31ttVdXxsCn/ovax6k96OaphrIAuF/TFLjDmDsQBx+
'' SIG '' uQ3eP8e034e9X3pqMS4DmYETqEcgzjFzDVctzXg0M5US
'' SIG '' mRK53mgvqubjwoqMKsOLIYdmvYNYV291vzyqJoddyhAV
'' SIG '' PJ+E6lTBCm7E/sVK3bkHEZcifNs+J9EeeOyfMcnx5iIZ
'' SIG '' 28SzR0OaGl+gHpDkXvXufPF9q2IBj/VNC97QIlaolc2u
'' SIG '' iHau7roN8+RN2aD7aKCuFDuzh8G7AgMBAAGjgdswgdgw
'' SIG '' NAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhhodHRw
'' SIG '' Oi8vb2NzcC52ZXJpc2lnbi5jb20wEgYDVR0TAQH/BAgw
'' SIG '' BgEB/wIBADBBBgNVHR8EOjA4MDagNKAyhjBodHRwOi8v
'' SIG '' Y3JsLnZlcmlzaWduLmNvbS9UaGF3dGVUaW1lc3RhbXBp
'' SIG '' bmdDQS5jcmwwEwYDVR0lBAwwCgYIKwYBBQUHAwgwDgYD
'' SIG '' VR0PAQH/BAQDAgEGMCQGA1UdEQQdMBukGTAXMRUwEwYD
'' SIG '' VQQDEwxUU0EyMDQ4LTEtNTMwDQYJKoZIhvcNAQEFBQAD
'' SIG '' gYEASmv56ljCRBwxiXmZK5a/gqwB1hxMzbCKWG7fCCmj
'' SIG '' XsjKkxPnBFIN70cnLwA4sOTJk06a1CJiFfc/NyFPcDGA
'' SIG '' 8Ys4h7Po6JcA/s9Vlk4k0qknTnqut2FB8yrO58nZXt27
'' SIG '' K4U+tZ212eFX/760xX71zwye8Jf+K9M7UhsbOCf3P0ow
'' SIG '' ggVGMIIELqADAgECAhEAsHgJfsXSrQMA7fgpGia4sjAN
'' SIG '' BgkqhkiG9w0BAQUFADCBlTELMAkGA1UEBhMCVVMxCzAJ
'' SIG '' BgNVBAgTAlVUMRcwFQYDVQQHEw5TYWx0IExha2UgQ2l0
'' SIG '' eTEeMBwGA1UEChMVVGhlIFVTRVJUUlVTVCBOZXR3b3Jr
'' SIG '' MSEwHwYDVQQLExhodHRwOi8vd3d3LnVzZXJ0cnVzdC5j
'' SIG '' b20xHTAbBgNVBAMTFFVUTi1VU0VSRmlyc3QtT2JqZWN0
'' SIG '' MB4XDTA5MDUxNDAwMDAwMFoXDTEyMDUxMzIzNTk1OVow
'' SIG '' gboxCzAJBgNVBAYTAlVTMQ4wDAYDVQQRDAUyODIyNjEL
'' SIG '' MAkGA1UECAwCTkMxEjAQBgNVBAcMCUNoYXJsb3R0ZTEu
'' SIG '' MCwGA1UECQwlMTE1MjUgQ2FybWVsIENvbW1vbnMgQmx2
'' SIG '' ZC4sIFN1aXRlIDEwMDEZMBcGA1UECgwQWFBJRU5UIFNv
'' SIG '' bHV0aW9uczEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGTAX
'' SIG '' BgNVBAMMEFhQSUVOVCBTb2x1dGlvbnMwggEiMA0GCSqG
'' SIG '' SIb3DQEBAQUAA4IBDwAwggEKAoIBAQC35Pl7Op1x200/
'' SIG '' nJxncnnhKXmPQw8T3KOIYnZqZ4m3lb+cvnF+d7SDy9jD
'' SIG '' dxbdM39WLyXWOpskG+LIViFlBV1KHTEsHgGkqwQpx5UT
'' SIG '' 5HVLMtNm0NYCWznFtYfVIqJs+tqz5lSQ2MUV4rSnstxm
'' SIG '' 0WRw4X9dneXe+zIrJ1W96UCw4ZLifsPbeqVYGpklgUmF
'' SIG '' xmFfwuPX/4qX0K6CXoqMRBZxg48qny2rv2vq+s0jscch
'' SIG '' sAyeZxjmWVUpxmYrZtyXCSNfVWd3UZsyZyDdl5YurfZx
'' SIG '' 28G1vjHr8WUuys8OX/FMgaRos8IUfDgdyP2LftB42HTh
'' SIG '' F/snyMik7PaMdv/wEAMtAgMBAAGjggFoMIIBZDAfBgNV
'' SIG '' HSMEGDAWgBTa7WR0FJwUPKvdmam9WyhNizzJ2DAdBgNV
'' SIG '' HQ4EFgQUeH7WhOp5HeJU1lLMlQxJBkArhykwDgYDVR0P
'' SIG '' AQH/BAQDAgeAMAwGA1UdEwEB/wQCMAAwEwYDVR0lBAww
'' SIG '' CgYIKwYBBQUHAwMwEQYJYIZIAYb4QgEBBAQDAgQQMEYG
'' SIG '' A1UdIAQ/MD0wOwYMKwYBBAGyMQECAQMCMCswKQYIKwYB
'' SIG '' BQUHAgEWHWh0dHBzOi8vc2VjdXJlLmNvbW9kby5uZXQv
'' SIG '' Q1BTMEIGA1UdHwQ7MDkwN6A1oDOGMWh0dHA6Ly9jcmwu
'' SIG '' dXNlcnRydXN0LmNvbS9VVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dC5jcmwwNAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzAB
'' SIG '' hhhodHRwOi8vb2NzcC5jb21vZG9jYS5jb20wGgYDVR0R
'' SIG '' BBMwEYEPaW5mb0B4cGllbnQuY29tMA0GCSqGSIb3DQEB
'' SIG '' BQUAA4IBAQBvSaL3UXO313UuGSpKCtggrg83wHa4XmlM
'' SIG '' 4LaUpCekcEBSFvVnBoxuGGR12GLgBkkLOVgPUnTsoH8G
'' SIG '' qvwcsTa11kQAV8XZ23K/Bk+c6MV3VeHa03V5kdf+2qKY
'' SIG '' VVadxQdrry8Dw2nvbuNZvBA1nMhB+3BQfSo2nTqIhahp
'' SIG '' MyY6FPXL2bemF/JWVAflFwyrdaInUueimSfBPu8ubQX1
'' SIG '' vgBOANoE33nQ8kXS1IhRsuB4xAjE9N4ZLwdr4rGkgstO
'' SIG '' xAVseaEUePSt+yWGMsyD9C/2XX4aG5Yp6wemvOrMF59G
'' SIG '' oJ7Uij5WrvxUJ4JNMuX22ywuQ6zZC1sCbVo1kYAagm9q
'' SIG '' MYIDxzCCA8MCAQEwgaswgZUxCzAJBgNVBAYTAlVTMQsw
'' SIG '' CQYDVQQIEwJVVDEXMBUGA1UEBxMOU2FsdCBMYWtlIENp
'' SIG '' dHkxHjAcBgNVBAoTFVRoZSBVU0VSVFJVU1QgTmV0d29y
'' SIG '' azEhMB8GA1UECxMYaHR0cDovL3d3dy51c2VydHJ1c3Qu
'' SIG '' Y29tMR0wGwYDVQQDExRVVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dAIRALB4CX7F0q0DAO34KRomuLIwDAYIKoZIhvcNAgUF
'' SIG '' AKBsMBAGCisGAQQBgjcCAQwxAjAAMBkGCSqGSIb3DQEJ
'' SIG '' AzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAM
'' SIG '' BgorBgEEAYI3AgEVMB8GCSqGSIb3DQEJBDESBBCLHvaF
'' SIG '' 4lOIMOpwYwvbNcMxMA0GCSqGSIb3DQEBAQUABIIBAGsS
'' SIG '' Tt3FOorQv0XgN8gXxbkoe8bOg/lxqLiky0WTLJzdZcwJ
'' SIG '' w/TKCFQ+5tulN9TP6BGpCoyC+don3yE+/7ieE+AR9L0T
'' SIG '' GkV0LqGdmmeVC9Twow9LIRZG6rPOmPf1cHUZtpQgkqq9
'' SIG '' 0wN5cY+2lufwfvazQzZeHNPV/0ULaUhQh1PeqtN9jQAC
'' SIG '' wQq1Do5S9KDJ3ohM869/ESjz9pLKjZ2xnKJB2opEgv+W
'' SIG '' shlXmFDdU5zAHi225XsVV9y/N7yeiPGalhHYBU1jj3ID
'' SIG '' CRT282sdIt4A6u7hxUVtumv4FfPG9tjJ3DHiBOYw0CYT
'' SIG '' +Ks8GoQmeKilY36NAj+0NfU8skrZVtShggF/MIIBewYJ
'' SIG '' KoZIhvcNAQkGMYIBbDCCAWgCAQEwZzBTMQswCQYDVQQG
'' SIG '' EwJVUzEXMBUGA1UEChMOVmVyaVNpZ24sIEluYy4xKzAp
'' SIG '' BgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2Vy
'' SIG '' dmljZXMgQ0ECEDgl1/r4Ya+e9JDnJrXWWtUwCQYFKw4D
'' SIG '' AhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEw
'' SIG '' HAYJKoZIhvcNAQkFMQ8XDTEyMDMwNjE4NDA1OFowIwYJ
'' SIG '' KoZIhvcNAQkEMRYEFGCP+8CMsFaxVGca2RR8n81Fq5dU
'' SIG '' MA0GCSqGSIb3DQEBAQUABIGAlDvmpmHgC0uJI8Lg0xC3
'' SIG '' o+XnqjCx5jbq6voAf7gSWX5bjlcuacMwbHgKe98Ltc2d
'' SIG '' VEpb+0E55dP+FwKkfEDCbnfcz6VlGYSqMOaKKP4/2NOn
'' SIG '' B5Q+qAqHIsczlL31aQHSLoNdGijk9Ho3TjGf9EZ4VqX9
'' SIG '' LW/rBrtRgqub2s0chRk=
'' SIG '' End signature block
