UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET sku = null
where sku = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET itemDesc = null
where itemdesc = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET longdesc = null
where longdesc = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET receiptdesc = null
where receiptdesc = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET monitordesc = null
where monitordesc = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET printerdesc = null
where printerdesc = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET soundfile = null
where soundfile = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET imagefile = null
where imagefile = ''

UPDATE [IRIS].[dbo].[tbl_ItemMaster]
SET shortdesc = null
where shortdesc = ''

------------------------------------------------------


UPDATE [IRIS].[dbo].[tbl_ItemAttachments]
SET PromptSoundFile = null
where PromptSoundFile = ''

------------------------------------------------------


UPDATE [IRIS].[dbo].[tbl_ItemCategories]
SET Description = null
where Description = ''

------------------------------------------------------


UPDATE [IRIS].[dbo].[tbl_MenuBtns]
SET text = null
where text = ''

UPDATE [IRIS].[dbo].[tbl_MenuBtns]
SET imagefile = null
where imagefile = ''

------------------------------------------------------

UPDATE [IRIS].[dbo].[tbl_MenuCmds]
SET description = null
where description = ''

------------------------------------------------------

UPDATE [IRIS].[dbo].[tbl_MenuMaster]
SET title = null
where title = ''

------------------------------------------------------

UPDATE [IRIS].[dbo].[tblDiscounts]
SET description = null
where description = ''

UPDATE [IRIS].[dbo].[tblDiscounts]
SET userule = null
where userule = ''

-----------------------------------------------------


UPDATE [IRIS].[dbo].[tblEmployees]
SET borrowedfrom = null
where borrowedfrom = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET lastname = null
where lastname = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET maidenname = null
where maidenname = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET firstname = null
where firstname = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET middleinitial = null
where middleinitial = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET address1 = null
where address1 = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET address2 = null
where address2 = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET city = null
where city = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET statecode = null
where statecode = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET zip = null
where zip = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET homephone = null
where homephone = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET pager = null
where pager = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET internetadr = null
where internetadr = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET ssn = null
where ssn = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET aliennumber = null
where aliennumber = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET preparerinformation = null
where preparerinformation = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET DocumentAID = null
where DocumentAID = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET DocumentCID = null
where DocumentCID = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET password = null
where password = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET lastReviewComment = null
where lastReviewComment = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET NextReview1Comment = null
where NextReview1Comment = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET NextReview2Comment = null
where NextReview2Comment = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET NextReview3Comment = null
where NextReview3Comment = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET ABANum = null
where ABANum = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET accountnum = null
where accountnum = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET EM_FirstName = null
where EM_FirstName = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET EM_LastName = null
where EM_LastName = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET EM_MiddleInitial = null
where EM_MiddleInitial = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET EM_phone = null
where EM_phone = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET EM_address = null
where EM_address = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET bankname = null
where bankname = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET country = null
where country = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET mobilephone = null
where mobilephone = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET taxcode = null
where taxcode = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET picturefilename = null
where picturefilename = ''

UPDATE [IRIS].[dbo].[tblEmployees]
SET documentbid = null
where documentbid = ''


---------------------------------------------------


UPDATE [IRIS].[dbo].[tblEmployeesJobs]
SET reason = null
where reason = ''

---------------------------------------------------
update [IRIS].[dbo].[tblTimedRegisterEvents]
SET posteodscript = 'c:\iris\scripts\systemmaint.vbs'
Where posteodscript = ''

