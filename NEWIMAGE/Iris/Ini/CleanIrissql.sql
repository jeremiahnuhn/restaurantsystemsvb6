GO
DELETE FROM tblOrder
GO
DELETE FROM tblCashManagement
GO
DELETE FROM tblEodDeposit
GO
DELETE FROM tblDrawerNum
GO
DELETE FROM tblDrawerTrans
GO
DELETE FROM tblEodGrandTotal
GO
DELETE FROM tblEodDepositItem
GO
DELETE FROM tblDrawerTotal
GO
DELETE FROM tblCashMgtTrans
GO
DELETE FROM tblEodDepositTrans
GO
DELETE FROM tblEodProcessed
GO
DELETE FROM tblSafeMgtTotals
GO
DELETE FROM tblSafeMgtDenomination
GO
DELETE FROM tblSafeMgtTrans
GO
DELETE FROM tblInventoryStatus
GO
DELETE FROM tblCostHistory
GO
DELETE FROM tblRep_InventoryStatus
GO
DELETE FROM tblEmployeesHours
GO
DELETE FROM tblEmployeeClockStatus
GO
DELETE FROM tblEmployeesHoursAudit
GO