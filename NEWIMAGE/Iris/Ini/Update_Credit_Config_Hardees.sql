Delete [IRIS].[dbo].[tblCfgSetting]
where SettingID in (560,565,567,834)

Delete [IRIS].[dbo].[tblCfgTabSetting]
where SettingID in (560,565,567,834)

Delete [IRIS].[dbo].[tblCfgRegModeDefSetting]
where SettingID in (560,565,567,834)

--insert into tblCfgSetting
INSERT INTO [IRIS].[dbo].[tblCfgSetting]([SettingID], [SettingDesc], [SettingVal])
VALUES(560,'Allow Voice Approval on Credi',1)
go
INSERT INTO [IRIS].[dbo].[tblCfgSetting]([SettingID], [SettingDesc], [SettingVal])
VALUES(565,'Allow Voice Approval on Comm Error',1)
go
INSERT INTO [IRIS].[dbo].[tblCfgSetting]([SettingID], [SettingDesc], [SettingVal])
VALUES(567,'Req Manager Approval for Voice Auth',0)
go
INSERT INTO [IRIS].[dbo].[tblCfgSetting]([SettingID], [SettingDesc], [SettingVal])
VALUES(834,'Allow Retry on Credit Comm Error',1)
go


--insert into tblCfgTabSetting
INSERT INTO [IRIS].[dbo].[tblCfgTabSetting]([TabID], [SettingID], [Serial])
VALUES(6,560,560)
go
INSERT INTO [IRIS].[dbo].[tblCfgTabSetting]([TabID], [SettingID], [Serial])
VALUES(6,565,565)
go
INSERT INTO [IRIS].[dbo].[tblCfgTabSetting]([TabID], [SettingID], [Serial])
VALUES(6,567,567)
go
INSERT INTO [IRIS].[dbo].[tblCfgTabSetting]([TabID], [SettingID], [Serial])
VALUES(6,834,834)
go



--insert into tblCfgRegModeDefSetting
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(1, 560, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(2, 560, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(3, 560, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(4, 560, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(5, 560, 1)
go

INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(1, 565, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(2, 565, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(3, 565, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(4, 565, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(5, 565, 1)
go

INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(1, 567, 0)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(2, 567, 0)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(3, 567, 0)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(4, 567, 0)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(5, 567, 0)
go

INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(1, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(2, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(3, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(4, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(5, 834, 1)
go


--Disable CVV=1, Enable CVV=0 (setting 5033)
--Enable Voice Auth (settings 5027, 5028)
delete [IRIS].[dbo].[tbl_PayTypeConfig]
where ConfigID in (5027,5028,5033)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (200,5033,0)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (201,5033,0)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (202,5033,0)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (203,5033,0)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (204,5033,0)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (205,5033,0)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (261,5033,0)
go




INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (200,5027,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (201,5027,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (202,5027,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (203,5027,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (204,5027,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (205,5027,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (261,5027,1)
go

INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (200,5028,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (201,5028,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (202,5028,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (203,5028,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (204,5028,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (205,5028,1)
go
INSERT INTO [IRIS].[dbo].[tbl_PayTypeConfig]([PayTypeID], [ConfigID], [Amt])
VALUES (261,5028,1)
go

