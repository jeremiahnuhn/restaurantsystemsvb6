IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[tblLsVersionNumber]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
	IF NOT EXISTS (SELECT	* 
				   FROM		[dbo].[tblLsVersionNumber]
				   WHERE	[dbo].[tblLsVersionNumber].[Major] = 3 AND
							[dbo].[tblLsVersionNumber].[Minor] = 1 AND
							[dbo].[tblLsVersionNumber].[Build] = 5)
	BEGIN
		INSERT tblLsVersionNumber (Major, Minor, Build, InstallationDateTime) VALUES (3, 1, 5, GETDATE())
	END
END
GO

-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLsSelectManagersPayrateToExport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLsSelectManagersPayrateToExport]
GO

CREATE   PROCEDURE [dbo].[spLsSelectManagersPayrateToExport]
	@ScheduleID	int
AS

SET NOCOUNT ON

-- spLsSelectManagersPayrateToExport 3

-- Create table for LaborCost & OT values
CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 4) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]


CREATE TABLE #EmpData (
	[EmployeeID] [int] NOT NULL,
	[LaborHoursWeek] [decimal](10, 2) NOT NULL,
	[LaborCostWeek] [decimal](10, 2) NOT NULL,
	[Payrate] [money] NOT NULL,
) ON [PRIMARY]

-- Fill temp table
INSERT INTO #ScheduleTotals
EXECUTE spLsSelectPayrollCalculations @ScheduleID

-- Fill temp table
INSERT INTO #EmpData
SELECT EmployeeID, SUM(LaborHrs) AS LaborHoursWeek, CONVERT(decimal(10,2),SUM(LaborCost)) LaborCostWeek, CONVERT(decimal(10,2),CASE WHEN SUM(LaborHrs) = 0 THEN 0.00 ELSE SUM(LaborCost) / SUM(LaborHrs) END) AS Payrate 
FROM #ScheduleTotals GROUP BY EmployeeID


-- FOR XML EXPLICIT
SELECT 1 as Tag,
null as Parent,
'' AS [ManagersPayrate!1],
'' AS [ManagerPayrate!2!element],
null as [ManagerPayrate!2!ManagerID!element],
null as [ManagerPayrate!2!LaborHoursWeek!element],
null as [ManagerPayrate!2!LaborCostWeek!element],
null as [ManagerPayrate!2!Payrate!element],
null as [ManagerPayrate!2!SalaryID!element],
null as [ManagerPayrate!2!PublishedDateTime!element]

UNION ALL	

-- Select only Managers
SELECT 
	2,
	1,
	null,
	null,
	ed.EmployeeID, 
	ed.LaborHoursWeek,
	ed.LaborCostWeek,
	ed.Payrate,
	e.SalaryID,
--	CONVERT(varchar, (SELECT PublishDateTime FROM tblLsSchedules WHERE ScheduleID = @ScheduleID), 112) + ' ' + CONVERT(varchar, CONVERT(DATETIME, (SELECT PublishDateTime FROM tblLsSchedules WHERE ScheduleID = @ScheduleID)), 108) AS PublishDateTime
	(SELECT StartDate FROM tblLsSchedules WHERE ScheduleID = @ScheduleID) AS PublishDateTime
FROM #EmpData AS ed
LEFT JOIN tblLsEmployees AS e
	ON ed.EmployeeID = e.EmployeeID
WHERE e.SalaryID = 3 OR e.SalaryID = 4
ORDER BY [ManagerPayrate!2!ManagerID!element]
FOR XML EXPLICIT


DROP TABLE #ScheduleTotals
DROP TABLE #EmpData
GO

	
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


IF NOT EXISTS (SELECT * FROM tblLsOptionsGeneral WHERE OptionID = 101) INSERT INTO tblLsOptionsGeneral VALUES (101, 'Show Forecast By Business Date', '1', 'Shows the forecast screen by business date 1=true, 0=false which shows the forecast screen by calendar date.')
GO

	
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-----------------------------------------------------------------------------------------------

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLsSelectSalaryLaborInfoBySchedule]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLsSelectSalaryLaborInfoBySchedule]
GO

CREATE PROCEDURE [dbo].[spLsSelectSalaryLaborInfoBySchedule] 
	@ScheduleId int,
	@TotalSalaryLaborCost float OUTPUT,
	@TotalSalaryLaborHrs float OUTPUT,
	@TotalSalaryOTHrs float OUTPUT,
	@TotalSalaryPaidBreakHrs float OUTPUT,
	@TotalSalaryPaidBreaksCost float OUTPUT,
	@TotalSalaryUnPaidBreaksHrs float OUTPUT,
	@TotalSalaryUnPaidBreaksCost float OUTPUT

AS

SET NOCOUNT ON

-- T E S T I N G    P U R P O S E S    O N L Y
--DECLARE @ScheduleId as int,
--	@TotalSalaryLaborCost float,
--	@TotalSalaryLaborHrs float,
--	@TotalSalaryOTHrs float,
--	@TotalSalaryPaidBreakHrs float,
--	@TotalSalaryPaidBreaksCost float,
--	@TotalSalaryUnPaidBreaksHrs float,
--	@TotalSalaryUnPaidBreaksCost float

--SET @ScheduleId=7


-- Create table for LaborCost & OT values
CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 4) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]

CREATE TABLE #LaborTotals (
	[TotalLaborCost] [decimal](10, 4) NOT NULL,
	[TotalLaborHrs] [decimal](10, 2) NOT NULL,
	[TotalOTHrs] [decimal](10, 2) NOT NULL,
	[TotalLPaidBreakHrs] [decimal](10, 2) NOT NULL,
	[TotalPaidBreaksCost] [decimal](10, 4) NOT NULL,
	[TotalUnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[TotalUnPaidBreaksCost] [decimal](10, 4) NOT NULL
) ON [PRIMARY]


-- Fill temp table with payroll calculations
INSERT INTO #ScheduleTotals
EXECUTE spLsSelectPayrollCalculations @ScheduleID 


-- Get summary labor info for the selected schedule and salaryid
-- SELECT ST.*, Emp.LastName, Emp.FirstName
--SELECT IsNull(sum(LaborCost),0) as TotalLaborCost, IsNull(sum(LaborHrs),0) as TotalLaborHrs,
--	IsNull(sum(OTHrs),0) as TotalOTHrs, IsNull(Sum(PaidBreaksHrs),0) as TotalPaidBreaksHrs,
--	IsNull(sum(PaidBreaksCost),0) as TotalPaidBreaksCost,
--	IsNull(Sum(UnPaidBreaksHrs),0) as TotalUnPaidBreaksHrs, IsNull(sum(UnPaidBreaksCost),0) as TotalUnPaidBreaksCost 
--FROM  #ScheduleTotals AS ST 
--       INNER JOIN dbo.tblLsEmployees Emp ON Emp.EmployeeID = ST.EmployeeID
--	INNER JOIN dbo.tblLsSalaryTypes SalType ON Emp.SalaryID = SalType.SalaryID
--WHERE SalType.IsYearly=1
--WHERE Emp.SalaryId>1

INSERT INTO #LaborTotals   
SELECT IsNull(sum(LaborCost),0), IsNull(sum(LaborHrs),0),
	IsNull(sum(OTHrs),0), IsNull(Sum(PaidBreaksHrs),0),
	IsNull(sum(PaidBreaksCost),0),
	IsNull(Sum(UnPaidBreaksHrs),0), IsNull(sum(UnPaidBreaksCost),0) 
FROM  #ScheduleTotals AS ST 
     INNER JOIN dbo.tblLsEmployees Emp ON Emp.EmployeeID = ST.EmployeeID
	INNER JOIN dbo.tblLsSalaryTypes SalType ON Emp.SalaryID = SalType.SalaryID
WHERE SalType.IsYearly=1




-- Populate the output variables
SET @TotalSalaryLaborCost = (SELECT TotalLaborCost from #LaborTotals)
SET @TotalSalaryLaborHrs = (SELECT TotalLaborHrs from #LaborTotals)
SET @TotalSalaryOTHrs = (SELECT TotalOTHrs from #LaborTotals)
SET @TotalSalaryPaidBreakHrs = (SELECT TotalLPaidBreakHrs from #LaborTotals)
SET @TotalSalaryPaidBreaksCost = (SELECT TotalPaidBreaksCost from #LaborTotals)
SET @TotalSalaryUnPaidBreaksHrs = (SELECT TotalUnPaidBreaksHrs from #LaborTotals)
SET @TotalSalaryUnPaidBreaksCost = (SELECT TotalUnPaidBreaksCost from #LaborTotals)


-- Clean up - your mother didn't write this
Select * from #ScheduleTotals ST inner join tblLsEmployees Emp on Emp.EmployeeId= ST.EmployeeId INNER JOIN dbo.tblLsSalaryTypes SalType ON Emp.SalaryID = SalType.SalaryID WHERE SalType.IsYearly=1
select * from #LaborTotals


DROP TABLE #ScheduleTotals
DROP TABLE #LaborTotals
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

-----------------------------------------------------------------------------------------------

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLsUpdateLaborRequirementsPerPosGuide]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLsUpdateLaborRequirementsPerPosGuide]
GO

CREATE PROCEDURE [dbo].[spLsUpdateLaborRequirementsPerPosGuide]
	@ScheduleID int
AS
SET NOCOUNT ON

--CLEAR OUT Requirements table first
delete from tblLsLaborRequirements where scheduleid=@scheduleid

-- Calculate Forecasted Sales for Week
DECLARE @WeekForecastedSales float
exec spLsSelectWeekForecastedSales @ScheduleID, @WeekForecastedSales OUTPUT


-- Calculate Salary Info for Week
DECLARE @TotalSalaryLaborCost float
DECLARE @TotalSalaryLaborHrs float
DECLARE @TotalSalaryOTHrs float
DECLARE @TotalSalaryPaidBreakHrs float
DECLARE @TotalSalaryPaidBreaksCost float
DECLARE @TotalSalaryUnPaidBreaksHrs float
DECLARE @TotalSalaryUnPaidBreaksCost float
exec spLsSelectSalaryLaborInfoBySchedule @ScheduleID, @TotalSalaryLaborCost OUTPUT,@TotalSalaryLaborHrs OUTPUT,@TotalSalaryOTHrs OUTPUT,@TotalSalaryPaidBreakHrs OUTPUT,@TotalSalaryPaidBreaksCost OUTPUT, @TotalSalaryUnPaidBreaksHrs OUTPUT, @TotalSalaryUnPaidBreaksCost OUTPUT


-- Calculate Allowed Labor Cost
DECLARE @AllowedLaborCost float
exec spLsSelectAllowedLaborCostO2 @WeekForecastedSales, @AllowedLaborCost OUTPUT

-- Calculate the cost of Recurring Shifts
DECLARE @RecurringShiftsCost float
exec spLsSelectRecurringShiftsLaborCost @ScheduleID, @RecurringShiftsCost OUTPUT

-- Calculate Average PayRate
DECLARE @AvgCrewPayrate float
exec spLsSelectAverageCrewRate @AvgCrewPayrate OUTPUT
--SET @AvgCrewPayrate = @AvgCrewPayrate / 4

-- Calculate Total Crew Labor Percentage
DECLARE @TotalCrewLaborPercentage float
IF @WeekForecastedSales > 0
	SET @TotalCrewLaborPercentage = (@AllowedLaborCost - @RecurringShiftsCost - @TotalSalaryLaborCost) / @WeekForecastedSales * 100
ELSE 
	SET @TotalCrewLaborPercentage = 0


-- Loop through the day numbers for the specified schedule */

DECLARE
	@DayNumber int,
	@TimeID int,
	@ForecastValue float,
	@TotalEmployeesRequired float,
	@dayStart15id int,
	@dayEnd15id int,
	@prevDayStart15id int,
	@prevDayEnd15id int,
	@prevDayNumber int,
	@prevDayFlag bit

DELETE FROM tblLsLaborRequirements WHERE ScheduleID = @ScheduleID


SET @DayNumber = 1
WHILE @DayNumber <= 7
BEGIN
	SET @TimeID = 0
	WHILE @TimeID < 96
	BEGIN
		SET @ForecastValue = 0
		SET @TotalEmployeesRequired = 0
		SET @prevDayFlag = 0

-- changed by KK on 05/05/2004 for 2.6.2, I get the forecast value for the whole hour, to calculate
-- labor requirements for the whole hour, as per BNE formula
		SELECT @ForecastValue = SUM(ForecastValue)
		FROM tblLsForecast
		WHERE (ScheduleID = @ScheduleID) AND (DayNumber = @DayNumber) AND Time15ID >= @TimeID AND Time15ID < (@TimeID + 4)
		
		IF @AvgCrewPayrate > 0 and IsNull(@ForecastValue, 0) > 0
			SET @TotalEmployeesRequired = ( @ForecastValue * @TotalCrewLaborPercentage * .01 / @AvgCrewPayrate  )
		ELSE
			SET @TotalEmployeesRequired = 0
-- end by KK for v. 2.6.2
		

-- Added by KK on 10/10/2003 per BNE request, for v.2.4
		-- Find out shift start / end time for the current day
		SELECT @dayStart15id = EarliestShift, @dayEnd15id = LatestShift
		FROM tblLsOptionsStoreHours AS h
			JOIN tblLsDayOfWeek AS d ON d.DOWID = h.DOWID
		WHERE D.DayNumber = @DayNumber

		IF @dayStart15id > @dayEnd15id 
			SET @dayEnd15id = @dayEnd15id + 96
	
		-- Find out shift end time for the previous day
		IF @DayNumber > 1
		BEGIN
			SET @prevDayNumber = @DayNumber - 1
			SELECT @prevDayStart15id = EarliestShift, @prevDayEnd15id = LatestShift
			FROM tblLsOptionsStoreHours AS h
				JOIN tblLsDayOfWeek AS d ON d.DOWID = h.DOWID
			WHERE D.DayNumber = @prevDayNumber
		END
		IF @prevDayStart15id > @prevDayEnd15id SET @prevDayFlag = 1

		-- There should be always min three employees during store shift hours,
		-- even if the sales are low or there is no sales at all.
		if @TotalEmployeesRequired >= 0 and @TotalEmployeesRequired <= 3 and 
			 (@TimeID >= @dayStart15id and @TimeID < @dayEnd15id) -- OR ( @prevDayFlag = 1 and (@TimeID between 0 and @prevDayEnd15id) ) )
				SET @TotalEmployeesRequired = 3
		else if @prevDayFlag = 1 and @TotalEmployeesRequired >= 0 and @TotalEmployeesRequired <= 3 and (@TimeID >= 0 and @TimeID < @prevDayEnd15id)
			SET @TotalEmployeesRequired = 3
		else if @TotalEmployeesRequired > 3
			SET @TotalEmployeesRequired = ROUND(@TotalEmployeesRequired, 0)
-- End by KK for v.2.4

		EXEC dbo.spLsUpdateLaborRequirementsForDayNumberPosGuide @ScheduleID, @DayNumber, @TimeID, @TotalEmployeesRequired
--		print CONVERT(varchar(2), @DayNumber) + ',  ' + CONVERT(varchar(3), @TimeID)  + ',  ' + CONVERT(varchar(3), @dayStart15id)  + ',  ' + CONVERT(varchar(3), @dayEnd15id)  + ',  ' + CONVERT(varchar(3), IsNull(@prevDayStart15id,0))  + ',  ' + CONVERT(varchar(3), IsNull(@prevDayEnd15id,0)) + ',  ' + CONVERT(varchar(6), IsNull(@ForecastValue,0)) + ',  ' + CONVERT(varchar(3), IsNull(@TotalEmployeesRequired,0))

		SET @TimeID = @TimeID + 4
	END

	SET @DayNumber = @DayNumber + 1
END


-- DEBUG INFO
SELECT @WeekForecastedSales AS WeekForecastedSales
SELECT @AllowedLaborCost AS AllowedLaborCost
SELECT @RecurringShiftsCost AS RecurringShiftsCost
SELECT @AvgCrewPayrate AS AvgCrewPayrate
SELECT @TotalCrewLaborPercentage AS TotalCrewLaborPercentage
SELECT @TotalSalaryLaborCost as TotalSalaryLaborCost
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SelectForecastValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_SelectForecastValues]
GO


CREATE PROCEDURE [dbo].[sp_SelectForecastValues]
(
	@ScheduleId 	int,
	@DayNumbers 	nvarchar(2000), -- If null then find the forecast values for the entire week.
	@ActivityIds	nvarchar(2000),
	@DayPartNameIds nvarchar(2000)
)
AS

CREATE TABLE #tblDayNumber ([DayNumber] INT)
CREATE TABLE #tblActivity ([ActivityId] INT)
CREATE TABLE #tblDayPartName ([DayPartNameId] INT)
CREATE TABLE #tblTimes (ScheduleId INT, [Date] DATETIME, TimeId INT, DayNumber INT, DayPartNameId INT, DayPartName NVARCHAR(50), ActivityId INT, ActivityName NVARCHAR(50))
CREATE TABLE #tblTimeLink (TimeID int)

IF (@DayNumbers IS NULL)
BEGIN
	INSERT INTO #tblDayNumber ([DayNumber])
	VALUES (0)
END
ELSE
BEGIN
	INSERT INTO #tblDayNumber ([DayNumber])
	SELECT ids.Data
	FROM dbo.Split (@DayNumbers, ',') ids
END
IF (@ActivityIds IS NOT NULL)
BEGIN
	INSERT INTO #tblActivity ([ActivityId])
	SELECT ids.Data
	FROM dbo.Split (@ActivityIds, ',') ids
END

IF (@DayPartNameIds IS NULL)
BEGIN
	INSERT INTO #tblDayPartName ([DayPartNameId])
	VALUES (0)
END
ELSE
BEGIN
	INSERT INTO #tblDayPartName ([DayPartNameId])
	SELECT ids.Data
	FROM dbo.Split (@DayPartNameIds, ',') ids
END

-- Fill the time link table that will be used later on.
DECLARE @TimeInterval INT
EXEC [dbo].[spLsGetMinInterval] @TimeInterval OUTPUT
IF @TimeInterval = 60
	INSERT INTO #tblTimeLink SELECT DISTINCT Time60ID FROM tblLsTimeInterval
ELSE IF @TimeInterval = 30
	INSERT INTO #tblTimeLink SELECT DISTINCT Time30ID FROM tblLsTimeInterval
ELSE
	INSERT INTO #tblTimeLink SELECT DISTINCT Time15ID FROM tblLsTimeInterval

-- Gets the week start time which will be used to determine whether or not we need the previous
-- weeks schedule identification or the next weeks schedule identification.
DECLARE		@WeekStartTime INT
SELECT		@WeekStartTime = [dbo].[tblLsOptionsGeneral].[OptionValue]
FROM		[dbo].[tblLsOptionsGeneral]
WHERE		[dbo].[tblLsOptionsGeneral].[OptionId] = 5

-- Gets whether or not to show the forecast by the business date or by calendar date.
DECLARE		@ByBusinessDate INT
SELECT		@ByBusinessDate = [dbo].[tblLsOptionsGeneral].[OptionValue]
FROM		[dbo].[tblLsOptionsGeneral]
WHERE		[dbo].[tblLsOptionsGeneral].[OptionId] = 101

-- Gets whether or not to show the forecast by the business date or by calendar date.
DECLARE		@WeekStartDate DATETIME
SELECT		@WeekStartDate = [dbo].[tblLsSchedules].[StartDate]
FROM		[dbo].[tblLsSchedules]
WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId	

-- Get the other schedule id that is needed.
DECLARE		@OtherScheduleId INT
IF (@ByBusinessDate <> 1)
BEGIN
	SET @OtherScheduleId = @ScheduleId
END
ELSE
BEGIN
	IF (@WeekStartTime < 0)
	BEGIN
		SELECT		@OtherScheduleId = schedule.[ScheduleId]
		FROM		[dbo].[tblLsSchedules] schedule
		WHERE		schedule.EndDate = 
					(SELECT		DATEADD(DAY, -1, [dbo].[tblLsSchedules].[StartDate])
					 FROM		[dbo].[tblLsSchedules]
					 WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId)
		
		-- The previous schedule was not found.  This could cause a problem if forecast records from the 
		-- previous schedule needed to be returned.
		IF (@OtherScheduleId IS NULL)
		BEGIN
			-- Prepare to insert a new schedule id because it may be needed when returning forecast 
			-- records for the previous schedule
			SELECT		@OtherScheduleId = MAX([dbo].[tblLsSchedules].[ScheduleId]) + 1
			FROM		[dbo].[tblLsSchedules]
			
			-- Create a previous schedule so the forecast records that may need to be created will be valid.
			INSERT INTO	[dbo].[tblLsSchedules] ([ScheduleID],[StoreID],[StartDate],[EndDate],[Pattern],[Published],[Recurring],[PatternName],[PublishDateTime],[LastUpdateDateTime])
			SELECT		@OtherScheduleId,[StoreID],DATEADD(DAY, -7, [StartDate]),DATEADD(DAY, -7, [EndDate]),0,0,0,'','1900/01/01',GETDATE()
			FROM		[dbo].[tblLsSchedules]
			WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId
		END
	END
	ELSE
	BEGIN
		SELECT		@OtherScheduleId = schedule.[ScheduleId]
		FROM		[dbo].[tblLsSchedules] schedule
		WHERE		schedule.StartDate = 
					(SELECT		DATEADD(DAY, 1, [dbo].[tblLsSchedules].[EndDate])
					 FROM		[dbo].[tblLsSchedules]
					 WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId)
					 
		-- The next schedule was not found.  This could cause a problem if forecast records from the 
		-- next schedule needed to be returned.
		IF (@OtherScheduleId IS NULL)
		BEGIN
			-- Prepare to insert a new schedule id because it may be needed when returning forecast 
			-- records for the next schedule
			SELECT		@OtherScheduleId = MAX([dbo].[tblLsSchedules].[ScheduleId]) + 1
			FROM		[dbo].[tblLsSchedules]
			
			-- Create the next schedule so the forecast records that may need to be created will be valid.
			INSERT INTO	[dbo].[tblLsSchedules] ([ScheduleID],[StoreID],[StartDate],[EndDate],[Pattern],[Published],[Recurring],[PatternName],[PublishDateTime],[LastUpdateDateTime])
			SELECT		@OtherScheduleId,[StoreID],DATEADD(DAY, 7, [StartDate]),DATEADD(DAY, 7, [EndDate]),0,0,0,'','1900/01/01',GETDATE()
			FROM		[dbo].[tblLsSchedules]
			WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId
		END
	END
END

INSERT INTO #tblTimes (ScheduleId, [Date], TimeId, DayNumber, DayPartNameId, DayPartName, ActivityId, ActivityName)
SELECT		DISTINCT @ScheduleId,
			DATEADD(DAY, [dbo].[tblLsDayOfWeek].[DayNumber]-1, @WeekStartDate),
			[dbo].[tblLsTimeInterval].[Time15Id],
			[dbo].[tblLsDayOfWeek].[DayNumber],
			[dbo].[tblLsDayPartNames].[DayPartNameId],
			[dbo].[tblLsDayPartNames].[Name],
			#tblActivity.[ActivityId],
			[dbo].[tblLsActivities].[ActivityName]
FROM		[dbo].[tblLsTimeInterval] INNER JOIN
			#tblTimeLink ON [dbo].[tblLsTimeInterval].[Time15ID] = #tblTimeLink.[TimeID] INNER JOIN
			[dbo].[tblLsDayParts] ON (
				([dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsDayParts].[EndTime] AND 
				 [dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsTimeInterval].[Time15ID] AND
				 [dbo].[tblLsTimeInterval].[Time15ID] < [dbo].[tblLsDayParts].[EndTime]) OR
				([dbo].[tblLsDayParts].[StartTime] > [dbo].[tblLsDayParts].[EndTime] AND 
				 ([dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsTimeInterval].[Time15ID] OR
				  [dbo].[tblLsTimeInterval].[Time15ID] < [dbo].[tblLsDayParts].[EndTime]))) INNER JOIN
			[dbo].[tblLsDayPartNames] ON [dbo].[tblLsDayParts].[DayPartNameId] = [dbo].[tblLsDayPartNames].[DayPartNameId] INNER JOIN
			[dbo].[tblLsDayOfWeek] ON [dbo].[tblLsDayParts].[DOWID] = [dbo].[tblLsDayOfWeek].[DOWID] INNER JOIN
			#tblDayNumber ON [dbo].[tblLsDayOfWeek].[DayNumber] = #tblDayNumber.[DayNumber] OR #tblDayNumber.[DayNumber] = 0 INNER JOIN
			#tblDayPartName ON [dbo].[tblLsDayPartNames].[DayPartNameId] = #tblDayPartName.[DayPartNameId] OR #tblDayPartName.[DayPartNameId] = 0,
			#tblActivity INNER JOIN
			[dbo].[tblLsActivities] ON #tblActivity.[ActivityId] = [dbo].[tblLsActivities].[ActivityId]

IF (@ByBusinessDate = 1)
BEGIN
	IF (@WeekStartTime < 0)
	BEGIN
		-- If the week start time is less than zero then go through all the records and update the schedule id (that is if the
		-- day number is the first day of the week), the day number and day name.
		UPDATE		#tblTimes
		SET			#tblTimes.ScheduleId = CASE WHEN #tblTimes.DayNumber = 1 THEN @OtherScheduleId ELSE #tblTimes.ScheduleId END,
					#tblTimes.DayNumber = CASE WHEN #tblTimes.DayNumber = 1 THEN 7 ELSE #tblTimes.DayNumber - 1 END,
					#tblTimes.[Date] = DATEADD(DAY, -1, #tblTimes.[Date])
		WHERE		#tblTimes.TimeId >= 96 + @WeekStartTime
	END
	ELSE
	BEGIN
		IF (@WeekStartTime > 0)
		BEGIN
			-- If the week start time is greater than zero then go through all the records and update the schedule id (that is if the
			-- day number is the last day of the week), the day number and day name.
			UPDATE		#tblTimes
			SET			#tblTimes.ScheduleId = CASE WHEN #tblTimes.DayNumber = 7 THEN @OtherScheduleId ELSE #tblTimes.ScheduleId END,
						#tblTimes.DayNumber = CASE WHEN #tblTimes.DayNumber = 7 THEN 1 ELSE #tblTimes.DayNumber + 1 END,
						#tblTimes.[Date] = DATEADD(DAY, 1, #tblTimes.[Date])
			WHERE		#tblTimes.TimeId >= @WeekStartTime
		END
	END
END

SELECT		CASE WHEN @DayNumbers IS NULL THEN @ScheduleId ELSE #tblTimes.[ScheduleId] END AS ScheduleId, CASE WHEN @DayNumbers IS NULL THEN @WeekStartDate ELSE #tblTimes.[Date] END AS [Date], #tblTimes.[TimeId] AS StartTimeId, #tblTimes.[TimeId] + (@TimeInterval / 15) AS EndTimeId, CASE WHEN @DayNumbers IS NULL THEN 0 ELSE #tblTimes.[DayNumber] END AS DayNumber, #tblTimes.[DayPartNameId], #tblTimes.[DayPartName], #tblTimes.[ActivityId], #tblTimes.[ActivityName], ISNULL(SUM(Forecasts.[ForecastValue]),0) AS ForecastValue
FROM		#tblTimes LEFT OUTER JOIN
			(
				SELECT		[dbo].[tblLsForecast].[ForecastId], [dbo].[tblLsForecast].[ScheduleId], [dbo].[tblLsForecast].[ActivityId], [dbo].[tblLsForecast].[DayNumber], [dbo].[tblLsForecast].[ForecastValue], CASE WHEN @TimeInterval = 60 THEN [dbo].[tblLsTimeInterval].[Time60Id] WHEN @TimeInterval = 30 THEN [dbo].[tblLsTimeInterval].[Time30Id] ELSE [dbo].[tblLsTimeInterval].[Time15Id] END AS TimeId
				FROM		[dbo].[tblLsForecast] INNER JOIN 
							[dbo].[tblLsTimeInterval] ON [dbo].[tblLsForecast].[Time15Id] = [dbo].[tblLsTimeInterval].[Time15Id]
			) AS Forecasts ON 
				#tblTimes.[ScheduleId] = Forecasts.[ScheduleId] AND
				#tblTimes.[TimeId] = Forecasts.[TimeId] AND
				#tblTimes.[DayNumber] = Forecasts.[DayNumber] AND
				#tblTimes.[ActivityId] = Forecasts.[ActivityId]
GROUP BY	CASE WHEN @DayNumbers IS NULL THEN @ScheduleId ELSE #tblTimes.[ScheduleId] END, CASE WHEN @DayNumbers IS NULL THEN @WeekStartDate ELSE #tblTimes.[Date] END, #tblTimes.TimeId, #tblTimes.TimeId + (@TimeInterval / 15), CASE WHEN @DayNumbers IS NULL THEN 0 ELSE #tblTimes.[DayNumber] END, #tblTimes.DayPartNameId, #tblTimes.DayPartName, #tblTimes.ActivityId, #tblTimes.ActivityName
ORDER BY	CASE WHEN @DayNumbers IS NULL THEN @WeekStartDate ELSE #tblTimes.[Date] END, #tblTimes.[TimeId]

DROP TABLE #tblDayNumber
DROP TABLE #tblActivity
DROP TABLE #tblDayPartName
DROP TABLE #tblTimes
DROP TABLE #tblTimeLink
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_UpdateForecast]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_UpdateForecast]
GO


CREATE PROCEDURE [dbo].[sp_UpdateForecast]
(
	@ScheduleId INT,
	@ActivityId INT,
	@DayOfWeekId INT,
	@StartTime INT,
	@EndTime INT,
	@Value FLOAT
)
AS

SET NOCOUNT ON

DECLARE @DayNumber INT
DECLARE @NewValue FLOAT
DECLARE @Count INT
DECLARE @Id INT
DECLARE @TimeInteger INT

-- Given the day of week id find the day number id.
SELECT	@DayNumber = [dbo].[tblLsDayOfWeek].[DayNumber]
FROM	[dbo].[tblLsDayOfWeek]
WHERE	[dbo].[tblLsDayOfWeek].[DOWID] = @DayOfWeekId

-- Just in case the time integer is less than 0 then make it 95.
IF @EndTime < 0
BEGIN
	SET @EndTime = 95
END

IF @StartTime <= @EndTime
BEGIN
	-- @Count will hold the total number of time increments that should be found within the forecast table.
	SET @Count = @EndTime - @StartTime
	
	-- @NewValue will hold the average value that should be stored within each record within the forecast table.
	SET	@NewValue = @Value / @Count
	
	-- The reason why we are looping is because if the user sets a forecast value for an hour
	-- then that value should be spreadout through every possible interval for that hour.  We
	-- will need a while loop to accomplish this.
	
	SET @TimeInteger = @StartTime
	WHILE @TimeInteger < @EndTime
	BEGIN
		IF EXISTS (	SELECT		*
					FROM		[dbo].[tblLsForecast]
					WHERE		[dbo].[tblLsForecast].[ScheduleID] = @ScheduleId AND
								[dbo].[tblLsForecast].[ActivityID] = @ActivityId AND
								[dbo].[tblLsForecast].[DayNumber] = @DayNumber AND
								[dbo].[tblLsForecast].[Time15ID] = @TimeInteger)
		BEGIN
			UPDATE		[dbo].[tblLsForecast]
			SET			[dbo].[tblLsForecast].[ForecastValue] = @NewValue
			WHERE		[dbo].[tblLsForecast].[ScheduleID] = @ScheduleId AND
						[dbo].[tblLsForecast].[ActivityID] = @ActivityId AND
						[dbo].[tblLsForecast].[DayNumber] = @DayNumber AND
						[dbo].[tblLsForecast].[Time15ID] = @TimeInteger
		END
		ELSE
		BEGIN
			-- The then next possible id that can be used within the tblForecast table.
			SELECT	@Id = COALESCE(MAX([ForecastID]) + 1, 0)
			FROM	[dbo].[tblLsForecast]
		
			INSERT INTO [dbo].[tblLsForecast] ([dbo].[tblLsForecast].[ForecastID],[dbo].[tblLsForecast].[ScheduleID],[dbo].[tblLsForecast].[ActivityID],[dbo].[tblLsForecast].[DayNumber],[dbo].[tblLsForecast].[ForecastValue],[dbo].[tblLsForecast].[Time15ID])
			VALUES		(@Id,@ScheduleId,@ActivityId,@DayNumber,@NewValue,@TimeInteger)
		END
		SET @TimeInteger = @TimeInteger + 1
	END
END
ELSE
BEGIN
	-- @Count will hold the total number of time increments that should be found within the forecast table.
	SET @Count = @EndTime + (96 - @StartTime)
	
	-- @NewValue will hold the average value that should be stored within each record within the forecast table.
	SET	@NewValue = @Value / @Count
	
	-- The reason why we are looping is because if the user sets a forecast value for an hour
	-- then that value should be spreadout through every possible interval for that hour.  We
	-- will need a while loop to accomplish this.
	
	-- Get all the time numbers between the start time and the highest possible time integer which is 95. EX: If
	-- the start time was 2300 which is 92 then do 92, 93, 94, and 95
	SET @TimeInteger = @StartTime
	WHILE @TimeInteger <= 95
	BEGIN
		IF EXISTS (	SELECT		*
					FROM		[dbo].[tblLsForecast]
					WHERE		[dbo].[tblLsForecast].[ScheduleID] = @ScheduleId AND
								[dbo].[tblLsForecast].[ActivityID] = @ActivityId AND
								[dbo].[tblLsForecast].[DayNumber] = @DayNumber AND
								[dbo].[tblLsForecast].[Time15ID] = @TimeInteger)
		BEGIN
			UPDATE		[dbo].[tblLsForecast]
			SET			[dbo].[tblLsForecast].[ForecastValue] = @NewValue
			WHERE		[dbo].[tblLsForecast].[ScheduleID] = @ScheduleId AND
						[dbo].[tblLsForecast].[ActivityID] = @ActivityId AND
						[dbo].[tblLsForecast].[DayNumber] = @DayNumber AND
						[dbo].[tblLsForecast].[Time15ID] = @TimeInteger
		END
		ELSE
		BEGIN
			-- The then next possible id that can be used within the tblForecast table.
			SELECT	@Id = COALESCE(MAX([ForecastID]) + 1, 0)
			FROM	[dbo].[tblLsForecast]
		
			INSERT INTO [dbo].[tblLsForecast] ([dbo].[tblLsForecast].[ForecastID],[dbo].[tblLsForecast].[ScheduleID],[dbo].[tblLsForecast].[ActivityID],[dbo].[tblLsForecast].[DayNumber],[dbo].[tblLsForecast].[ForecastValue],[dbo].[tblLsForecast].[Time15ID])
			VALUES		(@Id,@ScheduleId,@ActivityId,@DayNumber,@NewValue,@TimeInteger)
		END
		SET @TimeInteger = @TimeInteger + 1
	END
	
	-- Get all the time numbers between 0 and the end time. EX: If the end time was 1:00 then the end time number
	-- would be 3 when it hits this point (Remember that 1:00 is actually 4 but it will be 3 when it hits this point)
	-- then do 0, 1, 2, and 3.
	SET @TimeInteger = 0
	WHILE @TimeInteger < @EndTime
	BEGIN
		IF EXISTS (	SELECT		*
					FROM		[dbo].[tblLsForecast]
					WHERE		[dbo].[tblLsForecast].[ScheduleID] = @ScheduleId AND
								[dbo].[tblLsForecast].[ActivityID] = @ActivityId AND
								[dbo].[tblLsForecast].[DayNumber] = @DayNumber AND
								[dbo].[tblLsForecast].[Time15ID] = @TimeInteger)
		BEGIN
			UPDATE		[dbo].[tblLsForecast]
			SET			[dbo].[tblLsForecast].[ForecastValue] = @NewValue
			WHERE		[dbo].[tblLsForecast].[ScheduleID] = @ScheduleId AND
						[dbo].[tblLsForecast].[ActivityID] = @ActivityId AND
						[dbo].[tblLsForecast].[DayNumber] = @DayNumber AND
						[dbo].[tblLsForecast].[Time15ID] = @TimeInteger
		END
		ELSE
		BEGIN
			-- The then next possible id that can be used within the tblForecast table.
			SELECT	@Id = COALESCE(MAX([ForecastID]) + 1, 0)
			FROM	[dbo].[tblLsForecast]
		
			INSERT INTO [dbo].[tblLsForecast] ([dbo].[tblLsForecast].[ForecastID],[dbo].[tblLsForecast].[ScheduleID],[dbo].[tblLsForecast].[ActivityID],[dbo].[tblLsForecast].[DayNumber],[dbo].[tblLsForecast].[ForecastValue],[dbo].[tblLsForecast].[Time15ID])
			VALUES		(@Id,@ScheduleId,@ActivityId,@DayNumber,@NewValue,@TimeInteger)
		END
		SET @TimeInteger = @TimeInteger + 1
	END
END
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spLsSelectScheduleLaborCostVariancePercent]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spLsSelectScheduleLaborCostVariancePercent]
GO


CREATE PROCEDURE [dbo].[spLsSelectScheduleLaborCostVariancePercent]
	@ScheduleID int
AS

SET NOCOUNT ON

DECLARE @LaborCost float,
 	@LaborCostAllowed float,
	@SalesActivityID int,
	@SalesForecast float

-- Create table for LaborCost & OT values
CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 4) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]

-- Create table for LaborCost & OT values
CREATE TABLE #Forecast (
	[ScheduleID] [int] NOT NULL,
	[Date] [DateTime] NOT NULL,
	[StartTimeId] [int] NOT NULL,
	[EndTimeId] [int] NOT NULL,
	[DayPartNameId] [int] NOT NULL,
	[DayPartName] [nvarchar] (50) NOT NULL,
	[ActivityId] [int] NOT NULL,
	[ActivityName] [nvarchar] (50) NOT NULL,
	[ForecastValue] [float] NOT NULL
)

-- Fill temp table
INSERT INTO #ScheduleTotals
EXECUTE spLsSelectPayrollCalculations @ScheduleID

-- Set Variables
SELECT @LaborCost = isNull(CONVERT(decimal(10,2),SUM(LaborCost)), 0) FROM #ScheduleTotals 

EXEC dbo.spLsGetOptionSalesActivityID @SalesActivityID OUTPUT


DECLARE @SalesActivityIdAsString nvarchar(20)
SET @SalesActivityIdAsString = CONVERT(nvarchar(20), @SalesActivityID)
INSERT INTO #Forecast
EXEC dbo.[sp_SelectForecastValues] @ScheduleID, NULL, @SalesActivityIdAsString, NULL

SELECT @SalesForecast = isNull(SUM(ForecastValue), 0)
FROM #Forecast

SELECT TOP 1 @LaborCostAllowed = isNull(AllowedCost, 0)
FROM tblLsAllowedLaborCost
WHERE @SalesForecast between RangeFrom AND RangeTo

DROP TABLE #ScheduleTotals
DROP TABLE #Forecast

if @LaborCostAllowed is null
	set @LaborCostAllowed = 1

SELECT (1-(@LaborCost/@LaborCostAllowed))*100 AS LaborCostVariance
GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SelectForecastValue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_SelectForecastValue]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SelectForecastValueWeekView]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_SelectForecastValueWeekView]
GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


-----------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[spLsSelectLaborRequirements]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spLsSelectLaborRequirements]
GO


CREATE PROCEDURE [dbo].[spLsSelectLaborRequirements] 
(
	@ScheduleId 	int,
	@DayNumbers 	nvarchar(2000), -- If null then find labor requirements for the entire week.
	@JobIds			nvarchar(2000),
	@DayPartNameIds nvarchar(2000)
)
AS

CREATE TABLE #tblDayNumber ([DayNumber] INT)
CREATE TABLE #tblJob ([JobId] INT)
CREATE TABLE #tblDayPartName ([DayPartNameId] INT)
CREATE TABLE #tblTimes (TimeId INT, TimeDesc NVARCHAR(50), DayNumber INT, DayName NVARCHAR(50), DayPartNameId INT, DayPartName NVARCHAR(50), TimeSeqNum INT, JobId INT, JobName NVARCHAR(50))
CREATE TABLE #tblTimeLink (TimeID int)

IF (@DayNumbers IS NULL)
BEGIN
	INSERT INTO #tblDayNumber ([DayNumber])
	VALUES (0)
END
ELSE
BEGIN
	INSERT INTO #tblDayNumber ([DayNumber])
	SELECT ids.Data
	FROM dbo.Split (@DayNumbers, ',') ids
	
	IF (EXISTS(SELECT * FROM #tblDayNumber WHERE DayNumber = 0))
	BEGIN
		SET @DayNumbers = NULL
	END
END
IF (@JobIds IS NOT NULL)
BEGIN
	INSERT INTO #tblJob ([JobId])
	SELECT ids.Data
	FROM dbo.Split (@JobIds, ',') ids
END

IF (@DayPartNameIds IS NOT NULL)
BEGIN
	INSERT INTO #tblDayPartName ([DayPartNameId])
	SELECT ids.Data
	FROM dbo.Split (@DayPartNameIds, ',') ids
END


-- Fill the time link table that will be used later on.
DECLARE @TimeInterval INT
EXEC [dbo].[spLsGetMinInterval] @TimeInterval OUTPUT
IF @TimeInterval = 60
	INSERT INTO #tblTimeLink SELECT DISTINCT Time60ID FROM tblLsTimeInterval
ELSE IF @TimeInterval = 30
	INSERT INTO #tblTimeLink SELECT DISTINCT Time30ID FROM tblLsTimeInterval
ELSE
	INSERT INTO #tblTimeLink SELECT DISTINCT Time15ID FROM tblLsTimeInterval

INSERT INTO #tblTimes (TimeId, TimeDesc, DayNumber, DayName, DayPartNameId, DayPartName, TimeSeqNum, JobId, JobName)
SELECT		DISTINCT [dbo].[tblLsTimeInterval].[Time15Id],
			[dbo].[tblLsTimeInterval].[TimeDesc],
			#tblDayNumber.[DayNumber],
			CASE WHEN #tblDayNumber.[DayNumber] = 0 THEN 'All' ELSE [dbo].[tblLsDayOfWeek].[Name] END,
			[dbo].[tblLsDayPartNames].[DayPartNameId],
			[dbo].[tblLsDayPartNames].[Name],
			CASE WHEN [dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsDayParts].[EndTime] THEN [dbo].[tblLsTimeInterval].[Time15ID] WHEN [dbo].[tblLsTimeInterval].[Time15Id] >= [dbo].[tblLsDayParts].[StartTime] THEN [dbo].[tblLsTimeInterval].[Time15ID] ELSE [dbo].[tblLsTimeInterval].[Time15ID] + 96 END,
			#tblJob.[JobId],
			[dbo].[tblLsJobs].[JobName]
FROM		[dbo].[tblLsTimeInterval] INNER JOIN
			#tblTimeLink ON [dbo].[tblLsTimeInterval].[Time15ID] = #tblTimeLink.[TimeID] INNER JOIN
			[dbo].[tblLsDayParts] ON (
				([dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsDayParts].[EndTime] AND 
				 [dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsTimeInterval].[Time15ID] AND
				 [dbo].[tblLsTimeInterval].[Time15ID] < [dbo].[tblLsDayParts].[EndTime]) OR
				([dbo].[tblLsDayParts].[StartTime] > [dbo].[tblLsDayParts].[EndTime] AND 
				 ([dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsTimeInterval].[Time15ID] OR
				  [dbo].[tblLsTimeInterval].[Time15ID] < [dbo].[tblLsDayParts].[EndTime]))) INNER JOIN
			[dbo].[tblLsDayPartNames] ON [dbo].[tblLsDayParts].[DayPartNameId] = [dbo].[tblLsDayPartNames].[DayPartNameId] INNER JOIN
			[dbo].[tblLsDayOfWeek] ON [dbo].[tblLsDayParts].[DOWID] = [dbo].[tblLsDayOfWeek].[DOWID] INNER JOIN
			#tblDayNumber ON [dbo].[tblLsDayOfWeek].[DayNumber] = #tblDayNumber.[DayNumber] OR #tblDayNumber.[DayNumber] = 0 INNER JOIN
			#tblDayPartName ON [dbo].[tblLsDayPartNames].[DayPartNameId] = #tblDayPartName.[DayPartNameId],
			#tblJob INNER JOIN
			[dbo].[tblLsJobs] ON #tblJob.[JobId] = [dbo].[tblLsJobs].[JobId]

SELECT		@ScheduleId AS ScheduleId, #tblTimes.DayNumber, #tblTimes.DayName, #tblTimes.JobID, #tblTimes.JobName, #tblTimes.DayPartName, #tblTimes.TimeDesc AS ReqTime, #tblTimes.TimeSeqNum, COUNT(DISTINCT Shifts.[ShiftId]) AS ScheduledValue, ISNULL(Required.[ReqValue], 0) AS ReqValue, COUNT(DISTINCT Shifts.[ShiftId]) - ISNULL(Required.[ReqValue], 0) AS VarValue
FROM		#tblTimes LEFT OUTER JOIN
			(
				SELECT		DISTINCT [dbo].[tblLsShifts].[ShiftId], [dbo].[tblLsShifts].[EndDateTime], [dbo].[tblLsShifts].[StartDateTime], [dbo].[tblLsShifts].[JobId], [dbo].[tblLsShifts].[DayNumber]
				FROM		[dbo].[tblLsShifts] INNER JOIN 
							#tblJob ON [dbo].[tblLsShifts].[JobId] = #tblJob.[JobId]
				WHERE		[dbo].[tblLsShifts].[ScheduleId] = @ScheduleId
			) AS Shifts ON 
				((Shifts.[StartDateTime] < Shifts.[EndDateTime] AND
				  Shifts.[StartDateTime] <= #tblTimes.[TimeId] AND #tblTimes.[TimeId] < Shifts.[EndDateTime]) OR
				 (Shifts.[StartDateTime] > Shifts.[EndDateTime] AND
				  (Shifts.[StartDateTime] <= #tblTimes.[TimeId] OR #tblTimes.[TimeId] < Shifts.[EndDateTime]))) AND
				(Shifts.DayNumber = #tblTimes.DayNumber OR #tblTimes.DayNumber = 0) AND
				Shifts.JobId = #tblTimes.JobId LEFT OUTER JOIN
			(
				SELECT		LaborRequired.ReqTime, LaborRequired.JobId, CASE WHEN @DayNumbers IS NULL THEN 0 ELSE LaborRequired.DayNumber END AS DayNumber, SUM(LaborRequired.ReqValue) AS ReqValue
				FROM
				(
					SELECT		CASE WHEN @TimeInterval = 60 THEN [dbo].[tblLsTimeInterval].[Time15Id] WHEN @TimeInterval = 30 THEN [dbo].[tblLsTimeInterval].[Time30Id] ELSE [dbo].[tblLsTimeInterval].[Time15Id] END AS ReqTime, [dbo].[tblLsLaborRequirements].[JobId], [dbo].[tblLsLaborRequirements].[DayNumber], MAX([dbo].[tblLsLaborRequirements].[ReqValue]) AS ReqValue
					FROM		[dbo].[tblLsLaborRequirements] INNER JOIN 
								#tblJob ON [dbo].[tblLsLaborRequirements].[JobId] = #tblJob.[JobId] INNER JOIN
								[dbo].[tblLsTimeInterval] ON [dbo].[tblLsLaborRequirements].[ReqTime] = [dbo].[tblLsTimeInterval].[Time15Id]
					WHERE		[dbo].[tblLsLaborRequirements].[ScheduleId] = @ScheduleId
					GROUP BY	CASE WHEN @TimeInterval = 60 THEN [dbo].[tblLsTimeInterval].[Time15Id] WHEN @TimeInterval = 30 THEN [dbo].[tblLsTimeInterval].[Time30Id] ELSE [dbo].[tblLsTimeInterval].[Time15Id] END, [dbo].[tblLsLaborRequirements].[JobId], [dbo].[tblLsLaborRequirements].[DayNumber]
				) LaborRequired
				GROUP BY	LaborRequired.ReqTime, LaborRequired.JobId, CASE WHEN @DayNumbers IS NULL THEN 0 ELSE LaborRequired.DayNumber END
			) AS Required ON #tblTimes.[TimeId] = Required.[ReqTime] AND
				(Required.DayNumber = #tblTimes.DayNumber OR #tblTimes.DayNumber = 0) AND
				Required.JobId = #tblTimes.JobId
GROUP BY	#tblTimes.DayNumber, #tblTimes.DayName, #tblTimes.JobID, #tblTimes.JobName, #tblTimes.DayPartName, #tblTimes.TimeDesc, #tblTimes.TimeSeqNum, Required.[ReqValue]
ORDER BY	#tblTimes.TimeSeqNum


DROP TABLE #tblDayNumber
DROP TABLE #tblJob
DROP TABLE #tblDayPartName
DROP TABLE #tblTimes
DROP TABLE #tblTimeLink
GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-----------------------------------------------------------------------------------------------


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[sp_SelectForecastValues]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_SelectForecastValues]
GO


CREATE PROCEDURE [dbo].[sp_SelectForecastValues]
(
	@ScheduleId 	int,
	@DayOfWeekIds 	nvarchar(2000), -- If null then find the forecast values for the entire week.
	@ActivityIds	nvarchar(2000),
	@DayPartNameIds nvarchar(2000)
)
AS

CREATE TABLE #tblDayNumber ([DayNumber] INT)
CREATE TABLE #tblActivity ([ActivityId] INT)
CREATE TABLE #tblDayPartName ([DayPartNameId] INT)
CREATE TABLE #tblTimes (ScheduleId INT, [Date] DATETIME, TimeId INT, DayNumber INT, DayPartNameId INT, DayPartName NVARCHAR(50), ActivityId INT, ActivityName NVARCHAR(50))
CREATE TABLE #tblTimeLink (TimeID int)

IF (@DayOfWeekIds IS NULL)
BEGIN
	INSERT INTO #tblDayNumber ([DayNumber])
	VALUES (0)
END
ELSE
BEGIN
	INSERT INTO #tblDayNumber ([DayNumber])
	SELECT		[dbo].[tblLsDayOfWeek].[DayNumber]
	FROM		dbo.Split (@DayOfWeekIds, ',') ids INNER JOIN
				[dbo].[tblLsDayOfWeek] ON ids.Data = [dbo].[tblLsDayOfWeek].[DOWID]
END
IF (@ActivityIds IS NOT NULL)
BEGIN
	INSERT INTO #tblActivity ([ActivityId])
	SELECT ids.Data
	FROM dbo.Split (@ActivityIds, ',') ids
END

IF (@DayPartNameIds IS NULL)
BEGIN
	INSERT INTO #tblDayPartName ([DayPartNameId])
	VALUES (0)
END
ELSE
BEGIN
	INSERT INTO #tblDayPartName ([DayPartNameId])
	SELECT ids.Data
	FROM dbo.Split (@DayPartNameIds, ',') ids
END

-- Fill the time link table that will be used later on.
DECLARE @TimeInterval INT
EXEC [dbo].[spLsGetMinInterval] @TimeInterval OUTPUT
IF @TimeInterval = 60
	INSERT INTO #tblTimeLink SELECT DISTINCT Time60ID FROM tblLsTimeInterval
ELSE IF @TimeInterval = 30
	INSERT INTO #tblTimeLink SELECT DISTINCT Time30ID FROM tblLsTimeInterval
ELSE
	INSERT INTO #tblTimeLink SELECT DISTINCT Time15ID FROM tblLsTimeInterval

-- Gets the week start time which will be used to determine whether or not we need the previous
-- weeks schedule identification or the next weeks schedule identification.
DECLARE		@WeekStartTime INT
SELECT		@WeekStartTime = [dbo].[tblLsOptionsGeneral].[OptionValue]
FROM		[dbo].[tblLsOptionsGeneral]
WHERE		[dbo].[tblLsOptionsGeneral].[OptionId] = 5

-- Gets whether or not to show the forecast by the business date or by calendar date.
DECLARE		@ByBusinessDate INT
SELECT		@ByBusinessDate = [dbo].[tblLsOptionsGeneral].[OptionValue]
FROM		[dbo].[tblLsOptionsGeneral]
WHERE		[dbo].[tblLsOptionsGeneral].[OptionId] = 101

-- Gets whether or not to show the forecast by the business date or by calendar date.
DECLARE		@WeekStartDate DATETIME
SELECT		@WeekStartDate = [dbo].[tblLsSchedules].[StartDate]
FROM		[dbo].[tblLsSchedules]
WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId	

-- Get the other schedule id that is needed.
DECLARE		@OtherScheduleId INT
IF (@ByBusinessDate <> 1)
BEGIN
	SET @OtherScheduleId = @ScheduleId
END
ELSE
BEGIN
	IF (@WeekStartTime < 0)
	BEGIN
		SELECT		@OtherScheduleId = schedule.[ScheduleId]
		FROM		[dbo].[tblLsSchedules] schedule
		WHERE		schedule.EndDate = 
					(SELECT		DATEADD(DAY, -1, [dbo].[tblLsSchedules].[StartDate])
					 FROM		[dbo].[tblLsSchedules]
					 WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId)
		
		-- The previous schedule was not found.  This could cause a problem if forecast records from the 
		-- previous schedule needed to be returned.
		IF (@OtherScheduleId IS NULL)
		BEGIN
			-- Prepare to insert a new schedule id because it may be needed when returning forecast 
			-- records for the previous schedule
			SELECT		@OtherScheduleId = MAX([dbo].[tblLsSchedules].[ScheduleId]) + 1
			FROM		[dbo].[tblLsSchedules]
			
			-- Create a previous schedule so the forecast records that may need to be created will be valid.
			INSERT INTO	[dbo].[tblLsSchedules] ([ScheduleID],[StoreID],[StartDate],[EndDate],[Pattern],[Published],[Recurring],[PatternName],[PublishDateTime],[LastUpdateDateTime])
			SELECT		@OtherScheduleId,[StoreID],DATEADD(DAY, -7, [StartDate]),DATEADD(DAY, -7, [EndDate]),0,0,0,'','1900/01/01',GETDATE()
			FROM		[dbo].[tblLsSchedules]
			WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId
		END
	END
	ELSE
	BEGIN
		SELECT		@OtherScheduleId = schedule.[ScheduleId]
		FROM		[dbo].[tblLsSchedules] schedule
		WHERE		schedule.StartDate = 
					(SELECT		DATEADD(DAY, 1, [dbo].[tblLsSchedules].[EndDate])
					 FROM		[dbo].[tblLsSchedules]
					 WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId)
					 
		-- The next schedule was not found.  This could cause a problem if forecast records from the 
		-- next schedule needed to be returned.
		IF (@OtherScheduleId IS NULL)
		BEGIN
			-- Prepare to insert a new schedule id because it may be needed when returning forecast 
			-- records for the next schedule
			SELECT		@OtherScheduleId = MAX([dbo].[tblLsSchedules].[ScheduleId]) + 1
			FROM		[dbo].[tblLsSchedules]
			
			-- Create the next schedule so the forecast records that may need to be created will be valid.
			INSERT INTO	[dbo].[tblLsSchedules] ([ScheduleID],[StoreID],[StartDate],[EndDate],[Pattern],[Published],[Recurring],[PatternName],[PublishDateTime],[LastUpdateDateTime])
			SELECT		@OtherScheduleId,[StoreID],DATEADD(DAY, 7, [StartDate]),DATEADD(DAY, 7, [EndDate]),0,0,0,'','1900/01/01',GETDATE()
			FROM		[dbo].[tblLsSchedules]
			WHERE		[dbo].[tblLsSchedules].[ScheduleId] = @ScheduleId
		END
	END
END

INSERT INTO #tblTimes (ScheduleId, [Date], TimeId, DayNumber, DayPartNameId, DayPartName, ActivityId, ActivityName)
SELECT		DISTINCT @ScheduleId,
			DATEADD(DAY, [dbo].[tblLsDayOfWeek].[DayNumber]-1, @WeekStartDate),
			[dbo].[tblLsTimeInterval].[Time15Id],
			[dbo].[tblLsDayOfWeek].[DayNumber],
			[dbo].[tblLsDayPartNames].[DayPartNameId],
			[dbo].[tblLsDayPartNames].[Name],
			#tblActivity.[ActivityId],
			[dbo].[tblLsActivities].[ActivityName]
FROM		[dbo].[tblLsTimeInterval] INNER JOIN
			#tblTimeLink ON [dbo].[tblLsTimeInterval].[Time15ID] = #tblTimeLink.[TimeID] INNER JOIN
			[dbo].[tblLsDayParts] ON (
				([dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsDayParts].[EndTime] AND 
				 [dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsTimeInterval].[Time15ID] AND
				 [dbo].[tblLsTimeInterval].[Time15ID] < [dbo].[tblLsDayParts].[EndTime]) OR
				([dbo].[tblLsDayParts].[StartTime] > [dbo].[tblLsDayParts].[EndTime] AND 
				 ([dbo].[tblLsDayParts].[StartTime] <= [dbo].[tblLsTimeInterval].[Time15ID] OR
				  [dbo].[tblLsTimeInterval].[Time15ID] < [dbo].[tblLsDayParts].[EndTime]))) INNER JOIN
			[dbo].[tblLsDayPartNames] ON [dbo].[tblLsDayParts].[DayPartNameId] = [dbo].[tblLsDayPartNames].[DayPartNameId] INNER JOIN
			[dbo].[tblLsDayOfWeek] ON [dbo].[tblLsDayParts].[DOWID] = [dbo].[tblLsDayOfWeek].[DOWID] INNER JOIN
			#tblDayNumber ON [dbo].[tblLsDayOfWeek].[DayNumber] = #tblDayNumber.[DayNumber] OR #tblDayNumber.[DayNumber] = 0 INNER JOIN
			#tblDayPartName ON [dbo].[tblLsDayPartNames].[DayPartNameId] = #tblDayPartName.[DayPartNameId] OR #tblDayPartName.[DayPartNameId] = 0,
			#tblActivity INNER JOIN
			[dbo].[tblLsActivities] ON #tblActivity.[ActivityId] = [dbo].[tblLsActivities].[ActivityId]

IF (@ByBusinessDate = 1)
BEGIN
	IF (@WeekStartTime < 0)
	BEGIN
		-- If the week start time is less than zero then go through all the records and update the schedule id (that is if the
		-- day number is the first day of the week), the day number and day name.
		UPDATE		#tblTimes
		SET			#tblTimes.ScheduleId = CASE WHEN #tblTimes.DayNumber = 1 THEN @OtherScheduleId ELSE #tblTimes.ScheduleId END,
					#tblTimes.DayNumber = CASE WHEN #tblTimes.DayNumber = 1 THEN 7 ELSE #tblTimes.DayNumber - 1 END,
					#tblTimes.[Date] = DATEADD(DAY, -1, #tblTimes.[Date])
		WHERE		#tblTimes.TimeId >= 96 + @WeekStartTime
	END
	ELSE
	BEGIN
		IF (@WeekStartTime > 0)
		BEGIN
			-- If the week start time is greater than zero then go through all the records and update the schedule id (that is if the
			-- day number is the last day of the week), the day number and day name.
			UPDATE		#tblTimes
			SET			#tblTimes.ScheduleId = CASE WHEN #tblTimes.DayNumber = 7 THEN @OtherScheduleId ELSE #tblTimes.ScheduleId END,
						#tblTimes.DayNumber = CASE WHEN #tblTimes.DayNumber = 7 THEN 1 ELSE #tblTimes.DayNumber + 1 END,
						#tblTimes.[Date] = DATEADD(DAY, 1, #tblTimes.[Date])
			WHERE		#tblTimes.TimeId >= @WeekStartTime
		END
	END
END

SELECT		CASE WHEN @DayOfWeekIds IS NULL THEN @ScheduleId ELSE #tblTimes.[ScheduleId] END AS ScheduleId, CASE WHEN @DayOfWeekIds IS NULL THEN @WeekStartDate ELSE #tblTimes.[Date] END AS [Date], #tblTimes.[TimeId] AS StartTimeId, #tblTimes.[TimeId] + (@TimeInterval / 15) AS EndTimeId, #tblTimes.[DayPartNameId], #tblTimes.[DayPartName], #tblTimes.[ActivityId], #tblTimes.[ActivityName], ISNULL(SUM(Forecasts.[ForecastValue]),0) AS ForecastValue
FROM		#tblTimes LEFT OUTER JOIN
			(
				SELECT		[dbo].[tblLsForecast].[ForecastId], [dbo].[tblLsForecast].[ScheduleId], [dbo].[tblLsForecast].[ActivityId], [dbo].[tblLsForecast].[DayNumber], [dbo].[tblLsForecast].[ForecastValue], CASE WHEN @TimeInterval = 60 THEN [dbo].[tblLsTimeInterval].[Time60Id] WHEN @TimeInterval = 30 THEN [dbo].[tblLsTimeInterval].[Time30Id] ELSE [dbo].[tblLsTimeInterval].[Time15Id] END AS TimeId
				FROM		[dbo].[tblLsForecast] INNER JOIN 
							[dbo].[tblLsTimeInterval] ON [dbo].[tblLsForecast].[Time15Id] = [dbo].[tblLsTimeInterval].[Time15Id]
			) AS Forecasts ON 
				#tblTimes.[ScheduleId] = Forecasts.[ScheduleId] AND
				#tblTimes.[TimeId] = Forecasts.[TimeId] AND
				#tblTimes.[DayNumber] = Forecasts.[DayNumber] AND
				#tblTimes.[ActivityId] = Forecasts.[ActivityId]
GROUP BY	CASE WHEN @DayOfWeekIds IS NULL THEN @ScheduleId ELSE #tblTimes.[ScheduleId] END, CASE WHEN @DayOfWeekIds IS NULL THEN @WeekStartDate ELSE #tblTimes.[Date] END, #tblTimes.TimeId, #tblTimes.TimeId + (@TimeInterval / 15), #tblTimes.DayPartNameId, #tblTimes.DayPartName, #tblTimes.ActivityId, #tblTimes.ActivityName
ORDER BY	CASE WHEN @DayOfWeekIds IS NULL THEN @WeekStartDate ELSE #tblTimes.[Date] END, #tblTimes.[TimeId]

DROP TABLE #tblDayNumber
DROP TABLE #tblActivity
DROP TABLE #tblDayPartName
DROP TABLE #tblTimes
DROP TABLE #tblTimeLink
GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-----------------------------------------------------------------------------------------------

ALTER  TRIGGER tblLsEmployees_DTrig ON dbo.tblLsEmployees INSTEAD OF DELETE AS

-- Added by KK on 04/21/2006 for 2.8.2
-- Cascade delete to tblLsEmployeeTimeoff
DELETE tblLsEmployeeTimeoff
FROM deleted, tblLsEmployeeTimeoff AS t 
WHERE deleted.EmployeeID = t.EmployeeID
-- end

-- Cascade delete to tblLsEmployeeAvailability
DELETE tblLsEmployeeAvailability
FROM deleted, tblLsEmployeeAvailability AS t 
WHERE deleted.EmployeeID = t.EmployeeID

-- Cascade delete to  tblLsEmployeeJobs
DELETE tblLsEmployeeJobs
FROM deleted, tblLsEmployeeJobs AS t 
WHERE deleted.EmployeeID = t.EmployeeID

-- Added by KK on 04/21/2006 for 2.8.2
-- Cascade delete to  tblLsUsers
DELETE tblLsUsers
FROM deleted, tblLsUsers AS t 
WHERE CAST(deleted.EmployeeID as nvarchar(20)) = t.UserName
-- end

-- Finally Delete record from tblLsEmployees
DELETE tblLsEmployees 
FROM deleted, tblLsEmployees AS t 
WHERE deleted.EmployeeID = t.EmployeeID

GO

-----------------------------------------------------------------------------------------------

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[spLsSelectPayrollCalculations]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spLsSelectPayrollCalculations]
GO

CREATE PROCEDURE [dbo].[spLsSelectPayrollCalculations]
	@ScheduleID int
AS

SET NOCOUNT ON

DECLARE @OtherScheduleID int,
	@WeekStartTimeID int,
	@AdjustedWeekStartTimeID int,
	@AdjustedEndTimeID int,
	@ActualDayNumber int,
	@WeekStartDateTime DateTime,
	@WeekEndDateTime DateTime,
	@DayStartDateTime DateTime,
	@DayEndDateTime DateTime,
	@ShiftStartDateTime DateTime,
	@ShiftEndDateTime DateTime,
	@BreakStartDateTime DateTime,
	@BreakEndDateTime DateTime,
	@LaborCost decimal(10,4),
	@OTHrs decimal(10,2),
	@PaidBreaksCost decimal(10,2),
	@UnPaidBreaksCost decimal(10,2),
	@EmpAccumulatedHrs decimal(10,2),
	@EmpTotalWeekHrs decimal(10,2),
	@LastEmpID int,
	@Payrate decimal(10,4)


DECLARE @cScheduleID int,
	@cShiftID int,
	@cDayNumber int,
	@cEmployeeID int,
	@cSalaryID int,
	@cJobID int,
	@cStartTime int,
	@cEndTime int,
	@cPayrate decimal(10,4),
	@cOTThreshold int,
	@cOTCoeff decimal(10,2),
	@cIncludeInLaborVariance bit,
	@cIncludeLaborCost bit,
	@cLaborHrs decimal(10,2),
	@cPaidBreaksHrs decimal(10,2),
	@cUnPaidBreaksHrs decimal(10,2),
	@cRecurringShift bit,
	@cIsPaidBreak bit


-- Create temp table to split shifts if they span the WeekStartTime 
CREATE TABLE #ActualShiftHours (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[OrigDayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]

--Create temp table to split breaks if they span the WeekStartTime
CREATE TABLE #ActualBreakHours (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL
) ON [PRIMARY]


-- Create temp table to for final returned resultset for schedule
CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 4) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]


-- Get Week Start Time
SELECT @WeekStartTimeID = dbo.fnLsGetWeekStartTimeID()


-- Get ScheduleID of schedule before or after current schedule
IF @WeekStartTimeID >= 0
BEGIN
	SET @AdjustedWeekStartTimeID = @WeekStartTimeID
	SELECT @OtherScheduleID = dbo.fnLsGetFollowingScheduleID(@ScheduleID)
END
ELSE
BEGIN
	SET @AdjustedWeekStartTimeID = @WeekStartTimeID + 96
	SELECT @OtherScheduleID = dbo.fnLsGetPreviousScheduleID(@ScheduleID)
END


-- Set variables 
-- Actual Week Start & End dates are NOT necessary so use default date span
SET @WeekStartDateTime = '2000-01-01' 
SET @WeekEndDateTime = '2000-01-07' 
SET @cPaidBreaksHrs = 0
-- Adjust for week start time
SET @WeekStartDateTime = DATEADD(minute, @WeekStartTimeID*15, @WeekStartDateTime)
SET @WeekEndDateTime = DATEADD(minute, @WeekStartTimeID*15, @WeekEndDateTime) 

-- Declare Shifts cursor
-- This will fill the table #ActualShiftHours which will change the 
--    DayNumber, StartTime, EndTime to reflect WeekStartTime.  If a shift 
--    spans the start time then it will split it into 2 records and assign the 
--    correct DayNumber.
DECLARE EmpHours_cursor CURSOR FAST_FORWARD FOR 
	SELECT  ScheduleID,
		ShiftID,
		DayNumber,
		EmployeeID, 
		JobID,
		StartDateTime, 
		EndDateTime,
		RecurringShift
	FROM dbo.tblLsShifts S 
	WHERE ((ScheduleID = @ScheduleID) 
		OR (@WeekStartTimeID > 0 AND ScheduleID = @OtherScheduleID AND DayNumber = 1)
		OR (@WeekStartTimeID < 0 AND ScheduleID = @OtherScheduleID AND DayNumber = 7))
		AND (EmployeeID<>0)
	ORDER BY ScheduleID, DayNumber, EmployeeID, JobID, StartDateTime

-- Open cursor
OPEN EmpHours_cursor
FETCH NEXT FROM EmpHours_cursor INTO
	@cScheduleID, @cShiftID, @cDayNumber, @cEmployeeID, @cJobID, @cStartTime, @cEndTime, @cRecurringShift


--Start loop through all records in cursor
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Add a day to endtime if less than starttime
	IF @cStartTime > @cEndTime
		SET @AdjustedEndTimeID = @cEndTime + 96
	ELSE
		SET @AdjustedEndTimeID = @cEndTime 

	-- Get Start and end dates for shifts
	IF @cScheduleID = @ScheduleID
	BEGIN
		SET @ShiftStartDateTime = DATEADD(day, @cDayNumber-1, DATEADD(minute, @cStartTime*15, '2000-01-01'))
		SET @ShiftEndDateTime = DATEADD(day, @cDayNumber-1, DATEADD(minute, @AdjustedEndTimeID*15, '2000-01-01'))
	END
	ELSE IF @WeekStartTimeID >= 0
	BEGIN
		SET @ShiftStartDateTime = DATEADD(day, @cDayNumber+6, DATEADD(minute, @cStartTime*15, '2000-01-01'))
		SET @ShiftEndDateTime = DATEADD(day, @cDayNumber+6, DATEADD(minute, @AdjustedEndTimeID*15, '2000-01-01'))
	END
	ELSE 
	BEGIN
		SET @ShiftStartDateTime = DATEADD(day, @cDayNumber-8, DATEADD(minute, @cStartTime*15, '2000-01-01'))
		SET @ShiftEndDateTime = DATEADD(day, @cDayNumber-8, DATEADD(minute, @AdjustedEndTimeID*15, '2000-01-01'))
	END

	-- Check for each Day of Week to find which day the shift falls
	SET @ActualDayNumber = 1
	WHILE @ActualDayNumber <= 7
	BEGIN
		-- Set Day Start & End DateTimes
		SET @DayStartDateTime = DATEADD(day, @ActualDayNumber-1, @WeekStartDateTime)
		SET @DayEndDateTime = DATEADD(minute, 1439, @DayStartDateTime)

		-- Check for whole shift
		IF @ShiftStartDateTime between @DayStartDateTime AND @DayEndDateTime
		   AND @ShiftEndDateTime between @DayStartDateTime AND @DayEndDateTime
		BEGIN
			INSERT INTO #ActualShiftHours
			VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @cDayNumber, @cEmployeeID, @cJobID, @cStartTime, @cEndTime, @cRecurringShift)
			--PRINT 'Whole ShiftID:' + CONVERT(nvarchar(20),@cShiftID) + '   @DayStartDateTime:' + CONVERT(nvarchar(20),@DayStartDateTime,120) + '  @DayEndDateTime:' + CONVERT(nvarchar(20),@DayEndDateTime,120) + '     @ShiftStartDateTime:' + CONVERT(nvarchar(20),@ShiftStartDateTime,120) + '  @ShiftEndDateTime:' + CONVERT(nvarchar(20),@ShiftEndDateTime,120) 
			BREAK
		END
		ELSE 
		BEGIN
			-- Check for split shift
			IF @ShiftStartDateTime between @DayStartDateTime AND @DayEndDateTime
			BEGIN
				-- Insert first half of shift
			   	INSERT INTO #ActualShiftHours
				VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @cDayNumber, @cEmployeeID, @cJobID, @cStartTime, @AdjustedWeekStartTimeID, @cRecurringShift)

				--PRINT 'Split1 ShiftID:' + CONVERT(nvarchar(20),@cShiftID) + '   @DayStartDateTime:' + CONVERT(nvarchar(20),@DayStartDateTime,120) + '  @DayEndDateTime:' + CONVERT(nvarchar(20),@DayEndDateTime,120) + '     @ShiftStartDateTime:' + CONVERT(nvarchar(20),@ShiftStartDateTime,120) + '  @ShiftEndDateTime:' + CONVERT(nvarchar(20),@ShiftEndDateTime,120) 
			END
			IF @ShiftEndDateTime > @DayStartDateTime AND @ShiftEndDateTime <= @DayEndDateTime
			BEGIN
				-- Insert 2nd half of shift
			   	INSERT INTO #ActualShiftHours
				VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @cDayNumber, @cEmployeeID, @cJobID, @AdjustedWeekStartTimeID, @cEndTime, @cRecurringShift)

				--PRINT 'Split2 ShiftID:' + CONVERT(nvarchar(20),@cShiftID) + '   @DayStartDateTime:' + CONVERT(nvarchar(20),@DayStartDateTime,120) + '  @DayEndDateTime:' + CONVERT(nvarchar(20),@DayEndDateTime,120) + '     @ShiftStartDateTime:' + CONVERT(nvarchar(20),@ShiftStartDateTime,120) + '  @ShiftEndDateTime:' + CONVERT(nvarchar(20),@ShiftEndDateTime,120) 
			END
		END	
		-- Increment day
		SET @ActualDayNumber = @ActualDayNumber + 1
	END  

	FETCH NEXT FROM EmpHours_cursor INTO
		@cScheduleID, @cShiftID, @cDayNumber, @cEmployeeID, @cJobID, @cStartTime, @cEndTime, @cRecurringShift
END

-- Clean up cursor
CLOSE EmpHours_cursor
DEALLOCATE EmpHours_cursor


-- Declare Breaks cursor
-- This will fill the table #ActualBreakHours which will change the 
--    DayNumber, StartTime, EndTime to reflect WeekStartTime.
DECLARE EmpBreaks_cursor CURSOR FAST_FORWARD FOR 
	SELECT  S.ScheduleID,
		S.ShiftID,
		S.DayNumber,
		B.StartTime, 
		B.EndTime,
		B.Paid
	FROM dbo.tblLsShifts S INNER JOIN 
		dbo.tblLsBreaks B ON S.ShiftID = B.ShiftID
	WHERE ((ScheduleID = @ScheduleID) 
		OR (@WeekStartTimeID > 0 AND ScheduleID = @OtherScheduleID AND DayNumber = 1)
		OR (@WeekStartTimeID < 0 AND ScheduleID = @OtherScheduleID AND DayNumber = 7))
		AND (S.EmployeeID<>0) 
	ORDER BY S.ScheduleID, S.ShiftID, S.DayNumber, B.StartTime

-- Open cursor
OPEN EmpBreaks_cursor
FETCH NEXT FROM EmpBreaks_cursor INTO
	@cScheduleID, @cShiftID, @cDayNumber, @cStartTime, @cEndTime, @cIsPaidBreak


--Start loop through all records in cursor
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Add a day to endtime if less than starttime
	IF @cStartTime > @cEndTime
		SET @AdjustedEndTimeID = @cEndTime + 96
	ELSE
		SET @AdjustedEndTimeID = @cEndTime 

	-- Get Start and end dates for breaks
	IF @cScheduleID = @ScheduleID
	BEGIN
		SET @BreakStartDateTime = DATEADD(day, @cDayNumber-1, DATEADD(minute, @cStartTime*15, '2000-01-01'))
		SET @BreakEndDateTime = DATEADD(day, @cDayNumber-1, DATEADD(minute, @AdjustedEndTimeID*15, '2000-01-01'))
	END
	ELSE IF @WeekStartTimeID >= 0
	BEGIN
		SET @BreakStartDateTime = DATEADD(day, @cDayNumber+6, DATEADD(minute, @cStartTime*15, '2000-01-01'))
		SET @BreakEndDateTime = DATEADD(day, @cDayNumber+6, DATEADD(minute, @AdjustedEndTimeID*15, '2000-01-01'))
	END
	ELSE 
	BEGIN
		SET @BreakStartDateTime = DATEADD(day, @cDayNumber-8, DATEADD(minute, @cStartTime*15, '2000-01-01'))
		SET @BreakEndDateTime = DATEADD(day, @cDayNumber-8, DATEADD(minute, @AdjustedEndTimeID*15, '2000-01-01'))
	END

	-- Check for each Day of Week to find which day the shift falls
	SET @ActualDayNumber = 1
	WHILE @ActualDayNumber <= 7
	BEGIN
		-- Set Day Start & End DateTimes
		SET @DayStartDateTime = DATEADD(day, @ActualDayNumber-1, @WeekStartDateTime)
		SET @DayEndDateTime = DATEADD(minute, 1439, @DayStartDateTime)

		-- Check for whole shift
		IF @BreakStartDateTime between @DayStartDateTime AND @DayEndDateTime
		   AND @BreakEndDateTime between @DayStartDateTime AND @DayEndDateTime
		BEGIN
			-- Check if Paid or Unpaid break
			IF @cIsPaidBreak = 1
				INSERT INTO #ActualBreakHours
				VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @cStartTime, @cEndTime, dbo.fnLsConvertTimeIDsToHours(@cStartTime, @cEndTime), 0)
			ELSE
				INSERT INTO #ActualBreakHours
				VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @cStartTime, @cEndTime, 0, dbo.fnLsConvertTimeIDsToHours(@cStartTime, @cEndTime))
		
			--PRINT 'Whole BreakID:' + CONVERT(nvarchar(20),@cShiftID) + '   @DayStartDateTime:' + CONVERT(nvarchar(20),@DayStartDateTime,120) + '  @DayEndDateTime:' + CONVERT(nvarchar(20),@DayEndDateTime,120) + '     @BreakStartDateTime:' + CONVERT(nvarchar(20),@BreakStartDateTime,120) + '  @BreakEndDateTime:' + CONVERT(nvarchar(20),@BreakEndDateTime,120) 
			BREAK
		END
		ELSE 
		BEGIN
			-- Check for split shift
			IF @BreakStartDateTime between @DayStartDateTime AND @DayEndDateTime
			BEGIN
				-- Insert first half of shift
				-- Check if Paid or Unpaid break
				IF @cIsPaidBreak = 1
					INSERT INTO #ActualBreakHours
					VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @cStartTime, @AdjustedWeekStartTimeID, dbo.fnLsConvertTimeIDsToHours(@cStartTime, @cEndTime), 0)
				ELSE
					INSERT INTO #ActualBreakHours
					VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @cStartTime, @AdjustedWeekStartTimeID, 0, dbo.fnLsConvertTimeIDsToHours(@cStartTime, @cEndTime))

				--PRINT 'Split1 BreakID:' + CONVERT(nvarchar(20),@cShiftID) + '   @DayStartDateTime:' + CONVERT(nvarchar(20),@DayStartDateTime,120) + '  @DayEndDateTime:' + CONVERT(nvarchar(20),@DayEndDateTime,120) + '     @BreakStartDateTime:' + CONVERT(nvarchar(20),@BreakStartDateTime,120) + '  @BreakEndDateTime:' + CONVERT(nvarchar(20),@BreakEndDateTime,120) 
			END
			IF @BreakEndDateTime > @DayStartDateTime AND @BreakEndDateTime <= @DayEndDateTime
			BEGIN
				-- Insert 2nd half of shift
				IF @cIsPaidBreak = 1
					INSERT INTO #ActualBreakHours
					VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @AdjustedWeekStartTimeID, @cEndTime, dbo.fnLsConvertTimeIDsToHours(@cStartTime, @cEndTime), 0)
				ELSE
					INSERT INTO #ActualBreakHours
					VALUES (@cScheduleID, @cShiftID, @ActualDayNumber, @AdjustedWeekStartTimeID, @cEndTime, 0, dbo.fnLsConvertTimeIDsToHours(@cStartTime, @cEndTime))

				--PRINT 'Split2 BreakID:' + CONVERT(nvarchar(20),@cShiftID) + '   @DayStartDateTime:' + CONVERT(nvarchar(20),@DayStartDateTime,120) + '  @DayEndDateTime:' + CONVERT(nvarchar(20),@DayEndDateTime,120) + '     @BreakStartDateTime:' + CONVERT(nvarchar(20),@BreakStartDateTime,120) + '  @BreakEndDateTime:' + CONVERT(nvarchar(20),@BreakEndDateTime,120) 
			END
		END	
		-- Increment day
		SET @ActualDayNumber = @ActualDayNumber + 1
	END  

	FETCH NEXT FROM EmpBreaks_cursor INTO
		@cScheduleID, @cShiftID, @cDayNumber, @cStartTime, @cEndTime, @cIsPaidBreak
END

-- Clean up cursor
CLOSE EmpBreaks_cursor
DEALLOCATE EmpBreaks_cursor





-- Declare Employee Hours & Breaks cursor
-- This will fill the table #ScheduleTotals which combines 
--    Actual Shift hours with Actual Break hours and the jobs table
DECLARE Totals_cursor CURSOR FAST_FORWARD FOR 
	SELECT 	Act.ScheduleID, 
		Act.ShiftID, 
		Act.DayNumber, 
		Act.JobID, 
		Act.EmployeeID, 
		Act.StartTime,
		Act.EndTime,
		EJ.Payrate, 
		J.OvertimeThreshold, 
		J.OvertimeCoeff, 
		J.IncludeInLaborVariance,
		J.IncludeLaborCost,
		dbo.fnLsConvertTimeIDsToHours(Act.StartTime, Act.EndTime) AS LaborHrs,
		isNull(B.PaidBreaksHrs, 0) AS PaidBreaksHrs,
		Act.RecurringShift,
		E.SalaryID,
		isNull(B.UnPaidBreaksHrs, 0) AS UnPaidBreaksHrs
	FROM #ActualShiftHours Act INNER JOIN 
		dbo.tblLsEmployees E ON Act.EmployeeID = E.EmployeeID INNER JOIN
		dbo.tblLsJobs J ON Act.JobID = J.JobID INNER JOIN
		dbo.tblLsEmployeeJobs EJ ON Act.EmployeeID = EJ.EmployeeID AND Act.JobID =  EJ.JobID
		 LEFT OUTER JOIN 
		(
		SELECT ScheduleID, ShiftID, DayNumber, SUM(PaidBreaksHrs) AS PaidBreaksHrs, SUM(UnPaidBreaksHrs) AS UnPaidBreaksHrs 
		FROM #ActualBreakHours 
		GROUP BY ScheduleID, ShiftID, DayNumber
		) AS B 
		ON Act.ScheduleID = B.ScheduleID AND Act.ShiftID = B.ShiftID AND Act.DayNumber = B.DayNumber
	ORDER BY Act.EmployeeID, Act.DayNumber, Act.StartTime

-- Open cursor
OPEN Totals_cursor
FETCH NEXT FROM Totals_cursor INTO
	@cScheduleID, @cShiftID, @cDayNumber, @cJobID, @cEmployeeID, @cStartTime, @cEndTime,
	@cPayrate, @cOTThreshold, @cOTCoeff, @cIncludeInLaborVariance, @cIncludeLaborCost, @cLaborHrs, @cPaidBreaksHrs, @cRecurringShift, @cSalaryID, @cUnPaidBreaksHrs

--Start loop through all records in cursor
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Reset values
	SET @LaborCost = 0
	SET @PaidBreaksCost = 0
	SET @UnPaidBreaksCost = 0
	SET @OTHrs = 0

	-- Get Employee Week Hours
	IF @LastEmpID = @cEmployeeID
		SET @EmpAccumulatedHrs = @EmpAccumulatedHrs + @cLaborHrs
	ELSE
		SET @EmpAccumulatedHrs = @cLaborHrs

	-- Get LaborCost and OT based on Salary Type
	-- Hourly Salary
	IF @cSalaryID = 1
	BEGIN
	
		-- Check if over overtime threshold
		IF @cOTThreshold <= @EmpAccumulatedHrs - @cLaborHrs
		BEGIN
			-- All OverTime Hours
			SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate * @cOTCoeff
			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate * @cOTCoeff
			SET @LaborCost = (@cLaborHrs * @cPayrate * @cOTCoeff) - @UnPaidBreaksCost
			SET @OTHrs = @cLaborHrs
		END
		ELSE IF @cOTThreshold < @EmpAccumulatedHrs 
		BEGIN
			-- Regular Hours & Overtime Hours
			-- Regular Hours 
			SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate	
			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate	
			SET @LaborCost = ((@cOTThreshold - (@EmpAccumulatedHrs - @cLaborHrs)) * @cPayrate) - @UnPaidBreaksCost
			-- OT Hours
			SET @LaborCost = @LaborCost + ((@EmpAccumulatedHrs - @cOTThreshold) * @cPayrate * @cOTCoeff)
			SET @OTHrs = @EmpAccumulatedHrs - @cOTThreshold 
		END
		ELSE 
		BEGIN
			-- No Overtime
			SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate
			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate
			SET @LaborCost = (@cLaborHrs * @cPayrate) - @UnPaidBreaksCost
		END
	END

	-- Yearly Fixed Salary (Always gets paid full amount each week)
	ELSE IF @cSalaryID = 2
	BEGIN
		-- Get Total Week Hours if new employee
		IF @LastEmpID <> @cEmployeeID
			SET @EmpTotalWeekHrs = (SELECT SUM(dbo.fnLsConvertTimeIDsToHours(StartTime, EndTime)) 
						FROM #ActualShiftHours
						WHERE EmployeeID = @cEmployeeID)

		-- REMOVE this line if UNPAID Breaks for Yearly Salary should be calculated and removed from LaborCost
		SET @cUnPaidBreaksHrs = 0

		SET @cPayrate = (@cPayrate/52)/@EmpTotalWeekHrs
		SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate
		SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate
		SET @LaborCost = @cLaborHrs * @cPayrate - @UnPaidBreaksCost
	END

	-- Fluctuating Salary
	ELSE IF @cSalaryID = 3
	BEGIN
		IF @LastEmpID <> @cEmployeeID
			SET @EmpTotalWeekHrs = (SELECT SUM(dbo.fnLsConvertTimeIDsToHours(StartTime, EndTime)) 
						FROM #ActualShiftHours
						WHERE EmployeeID = @cEmployeeID)
						
		IF @EmpTotalWeekHrs < @cOTThreshold
			SET @EmpTotalWeekHrs = @cOTThreshold

		-- Only gets paid full amount if employee work hours are >= OTThreshold
--		IF @EmpTotalWeekHrs > @cOTThreshold
--			SET @cPayrate = (@cPayrate/52)/@EmpTotalWeekHrs
--		ELSE 
--			SET @cPayrate = (@cPayrate/52)/@cOTThreshold

		SET @Payrate = @cPayrate

		-- REMOVE this line if UNPAID Breaks for Fluctuating Salary should be calculated and removed from LaborCost
		SET @cUnPaidBreaksHrs = 0

		-- changed by Konstantin K. on March 10, 2003
		IF @EmpTotalWeekHrs > @cOTThreshold
		BEGIN
			
			SET @cPayrate = @Payrate/52/@cOTThreshold	-- hourly payrate
			SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate
			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate
				
			SET @cPayrate = @Payrate/52/@EmpTotalWeekHrs/2	-- overtime payrate
			SET @LaborCost = @Payrate/52 + (@EmpTotalWeekHrs - 
				@cOTThreshold)*@cPayrate - @UnPaidBreaksCost -- weekly labor cost

			SET @cPayrate = @LaborCost / @EmpTotalWeekHrs	 -- adjusted hourly payrate
			SET @LaborCost = @cLaborHrs * @cPayrate		-- shift labor cost
						
			IF @cOTThreshold <= @EmpAccumulatedHrs - @cLaborHrs
				SET @OTHrs = @cLaborHrs
			ELSE IF @cOTThreshold < @EmpAccumulatedHrs
				SET @OTHrs = @EmpAccumulatedHrs - @cOTThreshold
			
		END
		ELSE 
		BEGIN
			-- No Overtime
			SET @cPayrate = @Payrate/52/@cOTThreshold	-- hourly payrate
			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate
			SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate
			SET @LaborCost = (@cLaborHrs * @cPayrate) - @UnPaidBreaksCost
		END

		-- Check if over overtime threshold
--		IF @cOTThreshold <= @EmpAccumulatedHrs - @cLaborHrs
--		BEGIN
			-- All OverTime Hours
--			SET @PaidBreaksCost = @cPaidBreaksHrs * (@cPayrate/2) 
--			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * (@cPayrate/2)
--			SET @LaborCost = (@cLaborHrs * @cPayrate) + (@cLaborHrs * (@cPayrate/2)) - @UnPaidBreaksCost
--			SET @OTHrs = @cLaborHrs
--		END
--		ELSE IF @cOTThreshold < @EmpAccumulatedHrs
--		BEGIN
			-- Regular Hours & Overtime Hours
			-- Regular Hours 		
--			SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate
--			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate
--			SET @LaborCost = ((@cOTThreshold - (@EmpAccumulatedHrs - @cLaborHrs)) * @cPayrate) - @UnPaidBreaksCost
			-- OT Hours
--			SET @LaborCost = @LaborCost + ((@EmpAccumulatedHrs - @cOTThreshold) * (@cPayrate/2))
	--				 + ((@EmpAccumulatedHrs - @cOTThreshold) * (@cPayrate))
--			SET @OTHrs = @EmpAccumulatedHrs - @cOTThreshold 
--		END
--		ELSE 
--		BEGIN
			-- No Overtime
--			SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate
--			SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate
--			SET @LaborCost = (@cLaborHrs * @cPayrate) - @UnPaidBreaksCost
--		END
	END

	-- Yearly Actual Hours (Only gets paid full amount if employee work hours are >= OTThreshold)
	ELSE IF @cSalaryID = 4
	BEGIN
		-- Get Total Week Hours if new employee
		IF @LastEmpID <> @cEmployeeID
			SET @EmpTotalWeekHrs = (SELECT SUM(dbo.fnLsConvertTimeIDsToHours(StartTime, EndTime)) 
						FROM #ActualShiftHours
						WHERE EmployeeID = @cEmployeeID)

		-- REMOVE this line if UNPAID Breaks for Yearly Salary should be calculated and removed from LaborCost
		SET @cUnPaidBreaksHrs = 0

		IF @EmpTotalWeekHrs >= @cOTThreshold
			SET @cPayrate = (@cPayrate/52)/@EmpTotalWeekHrs
		ELSE 
			SET @cPayrate = (@cPayrate/52)/@cOTThreshold

		SET @PaidBreaksCost = @cPaidBreaksHrs * @cPayrate
		SET @UnPaidBreaksCost = @cUnPaidBreaksHrs * @cPayrate
		SET @LaborCost = @cLaborHrs * @cPayrate - @UnPaidBreaksCost
	END
	
	-- Instead of messing with all the different calculations of the laborcost just check to see
	-- if it should be included or not and if it is not to be included then set the labor cost to 0.
	IF @cIncludeLaborCost = 0
	BEGIN
		SET @LaborCost = 0
	END
	
	-- Instead of messing with all the different calculations of the laborcost and labor hours just
	-- check to see if it should be included or not and if it is not to be included then set the labor
	-- hours to 0.
	IF @cIncludeInLaborVariance = 0
	BEGIN
		SET @cLaborHrs = 0
	END

	-- Insert Record
	INSERT INTO #ScheduleTotals
	VALUES (@cScheduleID, @cShiftID, @cDayNumber, @cEmployeeID, @cJobID, 
		@cStartTime, @cEndTime,	@cLaborHrs, @LaborCost, @OTHrs, 
		@cPaidBreaksHrs, @PaidBreaksCost, @cUnPaidBreaksHrs, @UnPaidBreaksCost, @cRecurringShift)

	SET @LastEmpID = @cEmployeeID

	FETCH NEXT FROM Totals_cursor INTO
		@cScheduleID, @cShiftID, @cDayNumber, @cJobID, @cEmployeeID, @cStartTime, @cEndTime, 
		@cPayrate, @cOTThreshold, @cOTCoeff, @cIncludeInLaborVariance, @cIncludeLaborCost, @cLaborHrs, @cPaidBreaksHrs, @cRecurringShift, @cSalaryID, @cUnPaidBreaksHrs
END

-- Clean up cursor
CLOSE Totals_cursor
DEALLOCATE Totals_cursor

SELECT * 
FROM #ScheduleTotals
ORDER BY EmployeeID, DayNumber, StartTime

-- Clean temp tables
DROP TABLE #ActualShiftHours
DROP TABLE #ActualBreakHours
DROP TABLE #ScheduleTotals
GO

	
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

-----------------------------------------------------------------------------------------------

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[sp_SelectLaborCostForDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_SelectLaborCostForDay]
GO


CREATE PROCEDURE [dbo].[sp_SelectLaborCostForDay]
	@ScheduleId AS INT,
	@EmployeeId AS INT,
	@DayNumber AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	CREATE TABLE #ScheduleTotals (
		[ScheduleID] [int] NOT NULL,
		[ShiftID] [int] NOT NULL,
		[DayNumber] [int] NOT NULL,
		[EmployeeID] [int] NOT NULL,
		[JobID] [int] NOT NULL,
		[StartTime] [int] NOT NULL,
		[EndTime] [int] NOT NULL,
		[LaborHrs] [decimal](10, 2) NOT NULL,
		[LaborCost] [decimal](10, 4) NOT NULL,
		[OTHrs] [decimal](10, 2) NOT NULL,
		[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
		[PaidBreaksCost] [decimal](10, 2) NOT NULL,
		[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
		[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
		[RecurringShift] [bit] NOT NULL
	) ON [PRIMARY]


	-- Fill temp table
	INSERT INTO #ScheduleTotals
	EXECUTE spLsSelectPayrollCalculations @ScheduleID
	
	DECLARE @LaborCost FLOAT
	DECLARE @IsYearly int

	SET @IsYearly = (SELECT ST.IsYearly FROM tblLsSalaryTypes ST, tblLsEmployees E WHERE @EmployeeId = E.EmployeeID AND E.SalaryID = ST.SalaryID)
	
	SELECT	@LaborCost = SUM(
				CASE
					WHEN	[#ScheduleTotals].[EndTime] <= [#ScheduleTotals].[StartTime] AND 
							[#ScheduleTotals].[ScheduleId] = @ScheduleId AND
							[#ScheduleTotals].[DayNumber] = @DayNumber
						THEN ([#ScheduleTotals].[StartTime] - 96) / 4.00
					WHEN	[#ScheduleTotals].[EndTime] >= [#ScheduleTotals].[StartTime] AND 
							[#ScheduleTotals].[ScheduleId] = @ScheduleId AND
							[#ScheduleTotals].[DayNumber] = @DayNumber
						THEN ([#ScheduleTotals].[EndTime] - [#ScheduleTotals].[StartTime]) / 4.00
					WHEN	[#ScheduleTotals].[EndTime] <= [#ScheduleTotals].[StartTime] AND 
							([#ScheduleTotals].[DayNumber] = @DayNumber - 1 OR
							 ([#ScheduleTotals].[DayNumber] = 7 AND @DayNumber = 1))
						THEN [#ScheduleTotals].[StartTime] / 4.00
					ELSE 0
				END * CASE WHEN @IsYearly = 1 THEN [dbo].[tblLsEmployeeJobs].[PayRate]/52/[dbo].[tblLsJobs].[OvertimeThreshold] ELSE [dbo].[tblLsEmployeeJobs].[PayRate] END
				)
	FROM	[#ScheduleTotals] INNER JOIN
			[dbo].[tblLsEmployeeJobs] ON [#ScheduleTotals].[JobID] = [dbo].[tblLsEmployeeJobs].[JobID] AND [#ScheduleTotals].[EmployeeID] = [dbo].[tblLsEmployeeJobs].[EmployeeID] INNER JOIN
			[dbo].[tblLsJobs] ON [#ScheduleTotals].[JobID] = [dbo].[tblLsJobs].[JobID]
	WHERE	[#ScheduleTotals].[EmployeeId] = @EmployeeId AND
			[dbo].[tblLsJobs].[IncludeLaborCost] = 1

	SELECT	COALESCE(@LaborCost, 0.0) AS LaborCost
END
GO


SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


IF NOT EXISTS (SELECT * FROM tblLsOptionsGeneral WHERE OptionID = 102) INSERT INTO tblLsOptionsGeneral VALUES (102, 'Update IRIS Forecast After Publish', '0', 'Updates the IRIS forecast data when Labor Pro forecast data is modified after a schedule is published.')
GO

	
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


-----------------------------------------------------------------------------------------------

UPDATE tblLsSalaryTypes SET IsYearly=1
UPDATE tblLsSalaryTypes SET IsYearly=0 WHERE SalaryID=1

GO

-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[spLsSelectScheduleWeeklyTotalsReport]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spLsSelectScheduleWeeklyTotalsReport]
GO

CREATE     PROCEDURE [dbo].[spLsSelectScheduleWeeklyTotalsReport]  @ScheduleID int 
AS

SET NOCOUNT ON

CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 4) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
        [UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]


-- Create temp table for final returned result set for schedule
CREATE TABLE #GrandTotals (
	[ScheduleID] [int] NOT NULL,
	[TotalCrewHours] [decimal](10, 2) NOT NULL,
	[TotalCrewCost] [nVarChar](20) NOT NULL,
	[ProjectedBreakSavings] [nVarChar](20) NOT NULL,
	[ScheduledOvertime] [decimal](10, 2) NOT NULL,
	[TotalLaborCost] [nVarChar](20) NOT NULL,
	[TotalCostAllowed] [nVarChar](20) NOT NULL,
    [PaidBreaksCost][nVarChar](20) NOT NULL,
	[ManagerSchedOvertime] [decimal] (10, 2) NOT NULL
) ON [PRIMARY]


INSERT INTO #ScheduleTotals
EXEC spLsSelectPayrollCalculations @ScheduleID

DECLARE @crewHours decimal(10,2)
DECLARE @crewCost decimal(10,2)
DECLARE @ManagerTotalOT decimal(10,2)

-- added by KK on 06/08/2005 for 2.8.0
SELECT *,
	IsManager =  (SELECT IsManager 
				FROM tblLsJobs 
				INNER JOIN tblLsEmployeeJobs ON tblLsEmployeeJobs.JobID = tblLsJobs.JobID
				WHERE tblLsEmployeeJobs.PrimaryJob = 1 AND
				               tblLsEmployeeJobs.EmployeeID = #ScheduleTotals.EmployeeID)
INTO #ScheduleTotalsTemp
FROM #ScheduleTotals

--Get total hours for hourly workers
SELECT @crewHours = SUM(LaborHrs) FROM #ScheduleTotalsTemp WHERE IsManager = 0	
SELECT @crewCost = CONVERT(decimal(10,2),SUM(LaborCost)) FROM #ScheduleTotalsTemp WHERE IsManager = 0
SELECT @ManagerTotalOT = SUM(OTHrs) FROM #ScheduleTotalsTemp WHERE IsManager = 1

--Get the total cost allowed for the week. This is based on the forecast.
DECLARE @TotalCostAllowed decimal(10,2)

DECLARE @fVal float
--DECLARE @Ret decimal(10,2)

/*
select @fVal=SUM(ForecastValue) from tbllsforecast where ScheduleID=@ScheduleID

SET @TotalCostAllowed = (SELECT AllowedCost AS Value
FROM tblLsAllowedLaborCost
WHERE @fVal >=  RangeFrom AND @fVal <  RangeTo)
*/

CREATE TABLE #Forecast (
	[DayNumber] [int] NOT NULL,
	[ForecastValue] [float] NOT NULL,
) ON [PRIMARY]

INSERT INTO #Forecast EXEC dbo.spLsSelectForecastForTotals @ScheduleID

SET @fval = (SELECT SUM(ForecastValue) FROM #Forecast)

SET @TotalCostAllowed = (SELECT AllowedCost AS Value
FROM tblLsAllowedLaborCost
WHERE @fVal >=  RangeFrom AND @fVal <  RangeTo)
IF @TotalCostAllowed = null
	SET @TotalCostAllowed = 0

-- Create grand total
INSERT INTO #GrandTotals (ScheduleID, TotalCrewHours, TotalCrewCost, ProjectedBreakSavings, ScheduledOvertime, TotalLaborCost, TotalCostAllowed, PaidBreaksCost, ManagerSchedOvertime)
(SELECT  @ScheduleID, isnull(@crewHours,0),'$' + convert(nvarchar(15),isnull(@crewCost,0)),'$' +convert(nvarchar(15), isnull(SUM(UnPaidBreaksCost),0)), isnull(SUM(OTHrs),0),'$' + convert(nvarchar(15),isnull(CONVERT(decimal(10,2),SUM(LaborCost)),0)),'$' + convert(nvarchar(15),isnull(@TotalCostAllowed,0)),'$' + convert(nvarchar(15),isnull(SUM(PaidBreaksCost),0)), isnull(@ManagerTotalOT,0)
FROM #ScheduleTotals)


SELECT * 
FROM #GrandTotals


-- Clean temp tables
DROP TABLE #ScheduleTotals
DROP TABLE #GrandTotals
DROP TABLE #ScheduleTotalsTemp


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

-----------------------------------------------------------------------------------------------


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[spLsSelectScheduleWeekO2]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spLsSelectScheduleWeekO2]
GO

CREATE               PROCEDURE [dbo].[spLsSelectScheduleWeekO2]
	@ScheduleID	int
AS
SET NOCOUNT ON

--Create temporary table that will hold data in final cross-tab format
CREATE TABLE #EmpData (
	[RowID] [int] IDENTITY (1, 1) NOT NULL ,
	[ScheduleID] [int] NULL,
	[EmployeeID] [int] NULL ,
	[PrimaryJob] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DepartmentName] [nvarchar] (50) NULL ,
	[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Payrate] [char] (12) NULL ,
	[LaborCostWeek] [char] (12) NULL,
	[LaborCostWeekShown] [char] (12) NULL,
	[HoursWeek] [float] (8) NULL,
	[OTWeek] [float] (8) NULL,
	[Day1Start] [datetime] NULL ,
	[Day1End] [datetime] NULL ,
	[Day1Job] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Day1ShiftID] [int] NULL,
	[Day2Start] [datetime] NULL ,
	[Day2End] [datetime] NULL ,
	[Day2Job] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Day2ShiftID] [int] NULL,
	[Day3Start] [datetime] NULL ,
	[Day3End] [datetime] NULL ,
	[Day3Job] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Day3ShiftID][int] NULL,
	[Day4Start] [datetime] NULL ,
	[Day4End] [datetime] NULL ,
	[Day4Job] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Day4ShiftID] [int] NULL,
	[Day5Start] [datetime] NULL ,
	[Day5End] [datetime] NULL ,
	[Day5Job] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Day5ShiftID] [int] NULL,
	[Day6Start] [datetime] NULL ,
	[Day6End] [datetime] NULL ,
	[Day6Job] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Day6ShiftID] [int] NULL,
	[Day7Start] [datetime] NULL ,
	[Day7End] [datetime] NULL, 
	[Day7Job] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Day7ShiftID] [int] NULL,
	[RowNumber] [int] NULL,
        [ShowPayRate][int]
) ON [PRIMARY]

-- Create table for LaborCost & OT values
CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 4) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]


-- Fill temp table
INSERT INTO #ScheduleTotals
EXECUTE spLsSelectPayrollCalculations @ScheduleID


--Setup a cursor to loop through all the shift records and use logic to load into temp CrossTab table
DECLARE employees_cursor CURSOR FAST_FORWARD FOR 
	SELECT	a.ScheduleID,
		a.EmployeeID, 
		PrimaryJob = (SELECT tblLsJobs.JobName 
				FROM tblLsEmployeeJobs 
				INNER JOIN tblLsJobs ON tblLsEmployeeJobs.JobID = tblLsJobs.JobID
				WHERE tblLsEmployeeJobs.PrimaryJob =1 AND
				               tblLsEmployeeJobs.EmployeeID = a.EmployeeID),
		DepartmentName = (SELECT dep.DepartmentName FROM tblLsEmployees emp 
			      INNER JOIN tblLsDepartments dep ON emp.DepartmentID = dep.DepartmentID
			      WHERE emp.EmployeeID = a.EmployeeID),
		FirstName = (SELECT FirstName FROM tblLsEmployees emp WHERE emp.EmployeeID = a.EmployeeID), 
		LastName = (SELECT LastName FROM tblLsEmployees emp WHERE emp.EmployeeID = a.EmployeeID),  
		CASE WHEN ShowPayRate = 1 THEN LTRIM(RTRIM(CONVERT( char(12), (SELECT Payrate FROM tblLsEmployeeJobs WHERE EmployeeID = a.EmployeeID AND PrimaryJob=1) ))) ELSE '****' END AS Payrate,
		--CASE WHEN ShowPayRate = 1 THEN LTRIM(RTRIM(CONVERT( char(12), (SELECT isNull(CONVERT(decimal(10,2),SUM(LaborCost)), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID) ))) ELSE '****' END AS LaborCostWeek,
		CASE WHEN ShowPayRate = 1 THEN LTRIM(RTRIM(CONVERT( char(12), CASE WHEN (SELECT SalaryID FROM tblLsEmployees WHERE EmployeeID=a.EmployeeID)=3 AND (SELECT isNull(SUM(OTHrs), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID) <= 0 THEN (SELECT payrate FROM tbllsemployeejobs WHERE JobID = b.JobID and EmployeeID=a.EmployeeID)/52 ELSE(SELECT isNull(CONVERT(decimal(10,2),SUM(LaborCost)), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID) END ))) ELSE '****' END AS LaborCostWeek,
		LTRIM(RTRIM(CONVERT( char(12), CASE WHEN (SELECT SalaryID FROM tblLsEmployees WHERE EmployeeID=a.EmployeeID)=3 AND (SELECT isNull(SUM(OTHrs), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID) <= 0 THEN (SELECT payrate FROM tbllsemployeejobs WHERE JobID = b.JobID and EmployeeID=a.EmployeeID)/52 ELSE(SELECT isNull(CONVERT(decimal(10,2),SUM(LaborCost)), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID) END ))) AS LaborCostWeekShown,
--		Payrate = (SELECT Payrate FROM tblLsEmployeeJobs WHERE EmployeeID = a.EmployeeID AND PrimaryJob=1), 
--		LaborCostWeek = (SELECT isNull(SUM(LaborCost), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID), 
		HoursWeek = (SELECT isNull(SUM(LaborHrs), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID), 
		OTWeek = (SELECT isNull(SUM(OTHrs), 0) FROM #ScheduleTotals WHERE EmployeeID = a.EmployeeID), 

		DATEADD(minute, a.StartDateTime*15, '1980-01-01') AS StartDateTime,
		DATEADD(minute, a.EndDateTime*15, '1980-01-01') AS EndDateTime,
		b.JobName,
		a.ShiftID,
		a.DayNumber,
		a.RowNumber,
                ShowPayRate = (SELECT tblLsJobs.ShowPayRate 
				FROM tblLsJobs 
				INNER JOIN tblLsEmployeeJobs ON tblLsEmployeeJobs.JobID = tblLsJobs.JobID 	
				WHERE tblLsEmployeeJobs.PrimaryJob =1
					AND tblLsEmployeeJobs.EmployeeID = a.EmployeeID)

	FROM tblLsShifts a
	LEFT OUTER JOIN tblLsJobs b ON a.JobID = b.JobID
	WHERE a.ScheduleID = @ScheduleID 
	ORDER BY a.EmployeeID


--Prepare temp variables for cursor
DECLARE	@_ScheduleID	int,
		@EmployeeID		int,
		@PrimaryJob		nvarchar(50),
		@DepartmentName	nvarchar(50),
		@FirstName		nvarchar(50),
		@LastName		nvarchar(50),
		@Payrate		char(12),
		@LaborCostWeek	char(12),
		@LaborCostWeekShown	char(12),
		@HoursWeek		float(8),
		@OTWeek			float(8),
		@StartTime		datetime,
		@EndTime		datetime,
		@JobName		nvarchar(50),
		@ShiftID		int,
		@DayNumber		int,
		@RowNumber		int,
		@ShowPayRate	int
		
--Prepare temp variables for looping
DECLARE 	@TempRowCount	int,
			@RowID			int

--Open cursor
OPEN employees_cursor
FETCH NEXT FROM employees_cursor INTO
	@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @DayNumber, @RowNumber, @ShowPayRate

--Add null row if no records are returned from cursor
IF @@FETCH_STATUS <> 0
	INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob, DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day1Start, Day1End, Day1Job, Day1ShiftID, RowNumber,ShowPayRate) 
                                                     	VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 

--Start loop through all records in cursor
WHILE @@FETCH_STATUS = 0
BEGIN
	--Is employee already in our cross tab table?
	SELECT @TempRowCount = COUNT(*) 
	FROM #EmpData WHERE RowNumber = @RowNumber

	IF (@TempRowCount = 0)
		-- RowNumber not in table yet, create new record
		BEGIN
			IF @DayNumber = 1
				INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob, DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day1Start, Day1End, Day1Job, Day1ShiftID, RowNumber,ShowPayRate) 
                                                     		VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 
			IF @DayNumber = 2
				INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob, DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day2Start, Day2End, Day2Job, Day2ShiftID, RowNumber,ShowPayRate) 
                                                     		VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 
			IF @DayNumber = 3
				INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob, DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day3Start, Day3End, Day3Job, Day3ShiftID, RowNumber,ShowPayRate) 
                                                     		VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 
			IF @DayNumber = 4
				INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob, DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day4Start, Day4End, Day4Job, Day4ShiftID, RowNumber,ShowPayRate) 
                                                     		VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 
			IF @DayNumber = 5
				INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob, DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day5Start, Day5End, Day5Job, Day5ShiftID, RowNumber,ShowPayRate) 
                                                     		VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 
			IF @DayNumber = 6
				INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob,  DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day6Start, Day6End, Day6Job, Day6ShiftID, RowNumber,ShowPayRate) 
                                                    		 VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 
			IF @DayNumber = 7
				INSERT INTO #EmpData (ScheduleID, EmployeeID, PrimaryJob, DepartmentName, FirstName, LastName, Payrate, LaborCostWeek, LaborCostWeekShown, HoursWeek, OTWeek, Day7Start, Day7End, Day7Job, Day7ShiftID, RowNumber,ShowPayRate) 
				        	VALUES (@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @RowNumber,@ShowPayRate) 
		END
	ELSE
		--RowNumber is currently in table.  Now get ID of first row where this day is null to decide 
		--if you can do an update on that row, or need to insert a new row for this RowNumber
		BEGIN
			SET @RowID = NULL
			IF @DayNumber = 1
				SELECT  @RowID = RowID FROM #EmpData WHERE RowNumber = @RowNumber AND
					  				   Day1Start IS NULL
			IF @DayNumber = 2
				SELECT  @RowID = RowID FROM #EmpData WHERE RowNumber = @RowNumber AND
					  				   Day2Start IS NULL
			IF @DayNumber = 3
				SELECT  @RowID = RowID FROM #EmpData WHERE RowNumber = @RowNumber AND
					  				   Day3Start IS NULL
			IF @DayNumber = 4
				SELECT  @RowID = RowID FROM #EmpData WHERE RowNumber = @RowNumber AND
					  				   Day4Start IS NULL
			IF @DayNumber = 5
				SELECT  @RowID = RowID FROM #EmpData WHERE RowNumber = @RowNumber AND
					  				   Day5Start IS NULL
			IF @DayNumber = 6
				SELECT  @RowID = RowID FROM #EmpData WHERE RowNumber = @RowNumber AND

					  				   Day6Start IS NULL
			IF @DayNumber = 7
				SELECT  @RowID = RowID FROM #EmpData WHERE RowNumber = @RowNumber AND
					  				   Day7Start IS NULL
			IF @RowID IS NOT NULL
				--Found a row - now just need to update
				BEGIN
					IF @DayNumber = 1
						UPDATE #EmpData SET Day1Start = @StartTime, Day1End = @EndTime, Day1Job = @JobName, Day1ShiftID = @ShiftID
								WHERE RowID = @RowID
					IF @DayNumber = 2
						UPDATE #EmpData SET Day2Start = @StartTime, Day2End = @EndTime, Day2Job = @JobName, Day2ShiftID = @ShiftID
								WHERE RowID = @RowID
					IF @DayNumber = 3
						UPDATE #EmpData SET Day3Start = @StartTime, Day3End = @EndTime, Day3Job = @JobName, Day3ShiftID = @ShiftID
								WHERE RowID = @RowID
					IF @DayNumber = 4
						UPDATE #EmpData SET Day4Start = @StartTime, Day4End = @EndTime, Day4Job = @JobName, Day4ShiftID = @ShiftID
								WHERE RowID = @RowID
					IF @DayNumber = 5
						UPDATE #EmpData SET Day5Start = @StartTime, Day5End = @EndTime, Day5Job = @JobName, Day5ShiftID = @ShiftID
								WHERE RowID = @RowID
					IF @DayNumber = 6
						UPDATE #EmpData SET Day6Start = @StartTime, Day6End = @EndTime, Day6Job = @JobName, Day6ShiftID = @ShiftID
								WHERE RowID = @RowID
					IF @DayNumber = 7
						UPDATE #EmpData SET Day7Start = @StartTime, Day7End = @EndTime, Day7Job = @JobName, Day7ShiftID = @ShiftID
								WHERE RowID = @RowID
				END -- IF @RowID IS NOT NULL

		END

	FETCH NEXT FROM employees_cursor INTO
		@_ScheduleID, @EmployeeID, @PrimaryJob, @DepartmentName, @FirstName, @LastName, @Payrate, @LaborCostWeek, @LaborCostWeekShown, @HoursWeek, @OTWeek, @StartTime, @EndTime, @JobName, @ShiftID, @DayNumber, @RowNumber,@ShowPayRate
END


--Clean up cursor
CLOSE employees_cursor
DEALLOCATE employees_cursor


--Return results from our temp cross tab table
SELECT	ScheduleID,
	EmployeeID,
	SUBSTRING(PrimaryJob, 1, 7) as 'PrimaryJob',
	DepartmentName,
	FirstName,
	LastName,
	Payrate,
	LaborCostWeek,
	LaborCostWeekShown,
	isNull((dbo.fnLsConvertFloatToHours(HoursWeek)), 0) 	as 'HoursWeek',
	isNull((dbo.fnLsConvertFloatToHours(OTWeek)), 0) 	as 'OTWeek',
	(dbo.fnLsExtractTime(Day1Start))		as 'Day1StartTime',
	(dbo.fnLsExtractTime(Day1End))			as 'Day1EndTime',
	dbo.fnLsConvertMinToHours(DateDiff(mi, Day1Start, Day1End)) 	as 'Day1LengthTime',
	Day1Job,
	Day1ShiftID,	
	(dbo.fnLsExtractTime(Day2Start)) 		as 'Day2StartTime',
	(dbo.fnLsExtractTime(Day2End)) 			as 'Day2EndTime',
	dbo.fnLsConvertMinToHours(DateDiff(mi, Day2Start, Day2End)) 	as 'Day2LengthTime',
	Day2Job,
	Day2ShiftID,	
	(dbo.fnLsExtractTime(Day3Start)) 		as 'Day3StartTime',
	(dbo.fnLsExtractTime(Day3End)) 			as 'Day3EndTime',
	dbo.fnLsConvertMinToHours(DateDiff(mi, Day3Start, Day3End)) 	as 'Day3LengthTime',
	Day3Job,
	Day3ShiftID,
	(dbo.fnLsExtractTime(Day4Start)) 		as 'Day4StartTime',
	(dbo.fnLsExtractTime(Day4End)) 			as 'Day4EndTime',
	dbo.fnLsConvertMinToHours(DateDiff(mi, Day4Start, Day4End)) 	as 'Day4LengthTime',
	Day4Job,
	Day4ShiftID,	
	(dbo.fnLsExtractTime(Day5Start)) 		as 'Day5StartTime',
	(dbo.fnLsExtractTime(Day5End)) 			as 'Day5EndTime',
	dbo.fnLsConvertMinToHours(DateDiff(mi, Day5Start, Day5End)) 	as 'Day5LengthTime',
	Day5Job,
	Day5ShiftID,
	(dbo.fnLsExtractTime(Day6Start)) 		as 'Day6StartTime',
	(dbo.fnLsExtractTime(Day6End)) 			as 'Day6EndTime',
	dbo.fnLsConvertMinToHours(DateDiff(mi, Day6Start, Day6End)) 	as 'Day6LengthTime',
	Day6Job,
	Day6ShiftID,
	(dbo.fnLsExtractTime(Day7Start)) 		as 'Day7StartTime',
	(dbo.fnLsExtractTime(Day7End)) 			as 'Day7EndTime',
	dbo.fnLsConvertMinToHours(DateDiff(mi, Day7Start, Day7End)) 	as 'Day7LengthTime',
	Day7Job,
	Day7ShiftID,
	RowNumber,
        ShowPayRate
FROM #EmpData
ORDER BY RowNumber

--Clean up temp tableID
DROP TABLE #EmpData
DROP TABLE #ScheduleTotals





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

