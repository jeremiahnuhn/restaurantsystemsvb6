[Database]
Database=..\data\xception.mdb
Description=PurgeXception

[SQLStatement]
SQL1=DELETE * FROM tblOrder
SQL2=DELETE * FROM tblCashManagement
SQL3=DELETE * FROM tblGrandTotal
SQL4=DELETE * FROM tblDrawerNum
SQL5=DELETE * FROM tblDrawerTrans
SQL6=DELETE * FROM tblEodGrandTotal
SQL7=DELETE * FROM tblNonCashItems
SQL8=DELETE * FROM tblCashMgtTrans
