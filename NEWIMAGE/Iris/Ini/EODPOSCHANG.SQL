-- BNE Fix Zero Mgr Salaries
update tblEmployees set salary = 0 where PayTypeID = 2 and Salary is Null

-- BNE Fix Zero Hourly Pay Rates
Update tblEmployeesJobs Set PayRate = 8.88 Where PayRate = 0

-- BNE Fix Payrates entered that are more than 50 dollars an hour
Update tblEmployeesJobs Set PayRate = 8.88 where Payrate > 50

-- BNE Fix Rehires
UPDATE tblEmployeesStatusHistory SET Rehire = -1 where Rehire = 0

-- BNE Fix Clock Out not completed in prior pay week
Update tblEmployeesHours Set ClockOut = DateAdd(Minute,1,ClockIn), ClockOutType = 2, StoreNum = '9999' Where ClockOut is null AND DATEDIFF(day, ClockIn, getdate()) > 5

-- BNE Set Card Swipes to 'On' at beginning of month 
If Day(GetDate()) = '01'
BEGIN
Update [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 1 where settingid = 556
END

-- BNE Turn Manual Auth OFF Nightly
Update [IRIS].[dbo].[tblCfgRegModeDefSetting] Set settingval = 0 where settingid = 544

GO
-- DO NOT REMOVE ABOVE SCRIPTS

-- VAC-SICK-Discontinue.sql
Update tblMenus 
set parameter = 'c:\bneapps\VSdisc.exe' where Description = 'Add Holiday Vacation Meeting Hours'
go
Update tblMenus set FormID = '0' where Description = 'Add Holiday Vacation Meeting Hours'
go
Update tblMenus set TypeID = '3' where Description = 'Add Holiday Vacation Meeting Hours'
go