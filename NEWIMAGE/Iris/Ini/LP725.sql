SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


ALTER    PROCEDURE [dbo].[spLsSelectManagersPayrateToExport]
	@ScheduleID	int
AS

SET NOCOUNT ON

-- spLsSelectManagersPayrateToExport 3

-- Create table for LaborCost & OT values
CREATE TABLE #ScheduleTotals (
	[ScheduleID] [int] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[StartTime] [int] NOT NULL,
	[EndTime] [int] NOT NULL,
	[LaborHrs] [decimal](10, 2) NOT NULL,
	[LaborCost] [decimal](10, 4) NOT NULL,
	[OTHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[PaidBreaksCost] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksHrs] [decimal](10, 2) NOT NULL,
	[UnPaidBreaksCost] [decimal](10, 2) NOT NULL,
	[RecurringShift] [bit] NOT NULL
) ON [PRIMARY]


CREATE TABLE #EmpData (
	[EmployeeID] [int] NOT NULL,
	[LaborHoursWeek] [decimal](10, 2) NOT NULL,
	[LaborCostWeek] [decimal](10, 2) NOT NULL,
	[Payrate] [money] NOT NULL,
) ON [PRIMARY]

-- Fill temp table
INSERT INTO #ScheduleTotals
EXECUTE spLsSelectPayrollCalculations @ScheduleID

-- Fill temp table
INSERT INTO #EmpData
SELECT EmployeeID, SUM(LaborHrs) AS LaborHoursWeek, CONVERT(decimal(10,2),SUM(LaborCost)) LaborCostWeek, CONVERT(decimal(10,2),CASE WHEN SUM(LaborHrs) = 0 THEN 0.00 ELSE SUM(LaborCost) / SUM(LaborHrs) END) AS Payrate 
FROM #ScheduleTotals GROUP BY EmployeeID


-- FOR XML EXPLICIT
SELECT 1 as Tag,
null as Parent,
'' AS [ManagersPayrate!1],
'' AS [ManagerPayrate!2!element],
null as [ManagerPayrate!2!ManagerID!element],
null as [ManagerPayrate!2!LaborHoursWeek!element],
null as [ManagerPayrate!2!LaborCostWeek!element],
null as [ManagerPayrate!2!Payrate!element],
null as [ManagerPayrate!2!SalaryID!element],
null as [ManagerPayrate!2!PublishedDateTime!element]

UNION ALL	

-- Select only Managers
SELECT 
	2,
	1,
	null,
	null,
	ed.EmployeeID, 
	ed.LaborHoursWeek,
	ed.LaborCostWeek,
	ed.Payrate,
	e.SalaryID,
--	CONVERT(varchar, (SELECT PublishDateTime FROM tblLsSchedules WHERE ScheduleID = @ScheduleID), 112) + ' ' + CONVERT(varchar, CONVERT(DATETIME, (SELECT PublishDateTime FROM tblLsSchedules WHERE ScheduleID = @ScheduleID)), 108) AS PublishDateTime
	(SELECT StartDate FROM tblLsSchedules WHERE ScheduleID = @ScheduleID) AS PublishDateTime
FROM #EmpData AS ed
LEFT JOIN tblLsEmployees AS e
	ON ed.EmployeeID = e.EmployeeID
WHERE e.SalaryID = 3 OR e.SalaryID = 4 OR e.SalaryID = 2
ORDER BY [ManagerPayrate!2!ManagerID!element]
FOR XML EXPLICIT


DROP TABLE #ScheduleTotals
DROP TABLE #EmpData

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


use PSILS

Go
UPDATE [tblLsRoleAccessPoints]
SET PermissionID=2
WHERE RoleID=2
and AccessPointID=25

