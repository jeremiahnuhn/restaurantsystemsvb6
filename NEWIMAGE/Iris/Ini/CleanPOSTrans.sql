[Database]
Database=c:\iris\data\postrans.mdb
Description=PurgePOSTrans
[SQLStatement]
SQL1=DELETE * FROM tblOrder
SQL2=DELETE * FROM tblCashManagement
SQL3=DELETE * FROM EodDeposit
SQL4=DELETE * FROM tblDrawerNum
SQL5=DELETE * FROM tblDrawerTrans
SQL6=DELETE * FROM tblEodGrandTotal
SQL7=DELETE * FROM tblEodDepositItem
SQL8=DELETE * FROM tblDrawerTotal
SQL9=DELETE * FROM tblCashMgtTrans
SQL10=DELETE * FROM tblEodDepositTrans
SQL11=DELETE * FROM tblEodProcessed
SQL12=DELETE * FROM tblSafeMgtTotals
SQL13=DELETE * FROM tblSafeMgtDenomination
SQL14=DELETE * FROM tblSafeMgtTrans