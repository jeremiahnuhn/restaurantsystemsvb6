go
Insert into tblmenuicons (IconID,
Iconfilename,
Description)
values ('35',
'winaw32.ico',
'winaw32.ico');
go
Insert into tblmenuicons (IconID,
Iconfilename,
Description)
values ('36',
'explorer.ico',
'explorer.ico');
go
Insert into tblmenuicons (IconID,
Iconfilename,
Description)
values ('37',
'mmc.ico',
'mmc.ico');
go
Insert into tblmenuicons (IconID,
Iconfilename,
Description)
values ('38',
'dbeditor.ico',
'dbeditor.ico');
go
Insert into tblMenus(ApplicationID,
MenuName,
Description,
MenuLevel,
ParentID,
SeqNum,
IconID,
TypeID,
FormID,
Parameter,
SecurityLevel,
MenuID) 
values ('5',
'Help Desk',
'Help Desk',
'1',
'190',
'24',
'20',
'1',
'0',
null,
'99',
'909');
go
Insert into tblMenus(ApplicationID,
MenuName,
Description,
MenuLevel,
ParentID,
SeqNum,
IconID,
TypeID,
FormID,
Parameter,
SecurityLevel,
MenuID) 
Values ('5',
'PC Anywhere',
'PC Anywhere',
'2',
'909',
'1',
'35',
'3',
'0',
'"C:\Program Files\Symantec\pcAnywhere\winaw32.exe"',
'99',
'910');
go
Insert into tblMenus(ApplicationID,
MenuName,
Description,
MenuLevel,
ParentID,
SeqNum,
IconID,
TypeID,
FormID,
Parameter,
SecurityLevel,
MenuID)
Values ('5',
'Explorer',
'Explorer',
'2',
'909',
'2',
'36',
'3',
'0',
'"c:\winnt\explorer.exe"',
'99',
'911');
go
Insert into tblMenus(ApplicationID,
MenuName,
Description,
MenuLevel,
ParentID,
SeqNum,
IconID,
TypeID,
FormID,
Parameter,
SecurityLevel,
MenuID)
Values ('5',
'Enterprise Manager',
'Enterprise Manager',
'2',
'909',
'3',
'37',
'3',
'0',
'"C:\WINNT\system32\mmc.exe"',
'99',
'912');
go
Insert into tblMenus(ApplicationID,
MenuName,
Description,
MenuLevel,
ParentID,
SeqNum,
IconID,
TypeID,
FormID,
Parameter,
SecurityLevel,
MenuID)
Values ('5',
'DB Editor',
'DB Editor',
'2',
'909',
'4',
'38',
'3',
'0',
'"C:\iris\bin\DBEditor.exe"',
'99',
'913');
go
Insert into tblMenus(ApplicationID,
MenuName,
Description,
MenuLevel,
ParentID,
SeqNum,
IconID,
TypeID,
FormID,
Parameter,
SecurityLevel,
MenuID)
Values ('5',
'Eventviewer',
'Eventviewer',
'2',
'909',
'5',
'17',
'3',
'0',
'"c:\winnt\system32\eventvwr.exe"',
'99',
'914');
go
Insert into tblMenus(ApplicationID,
MenuName,
Description,
MenuLevel,
ParentID,
SeqNum,
IconID,
TypeID,
FormID,
Parameter,
SecurityLevel,
MenuID)
Values ('5',
'Exit',
'Exit Application',
'2',
'909',
'1004',
'6',
'6',
'0',
null,
'99',
'915');
GO
INSERT INTO [IRIS].[dbo].[tbl_Application]([SeqNum], 
[Title], [CmdLine], [WorkingDirectory], [IconPath], 
[IconIndex], [AutoStart], [AutoRestart], [CloseOnSignOff], 
[RunMaximized], [RunMinimized], [RunHidden], [SystemService], 
[CriticalService], [StartupPriority], [ShutdownPriority], 
[SecurityLevel], [AppTimeoutWarning], [AppTimeoutFailure], 
[RequiredMemory])
VALUES('11',
'Electronic Journal', 
'EJPOS.exe', 
'C:\IRIS\Bin',
'C:\IRIS\images\POS.ico', 
'0', 
'0',
'0', 
'0', 
'0', 
'0', 
'0', 
'0',
'0', 
'0', 
'0',
'99',
'0', 
'0', 
'22');
UPDATE [Iris].[dbo].[tbl_Application]
SET [CmdLine]='c:\iris\setup\runvbscript.exe wscript.exe c:\iris\scripts\depleteinventory.vbs /launchinv'
WHERE [Title] like 'Inventory%'
Go
UPDATE [Iris].[dbo].[tbl_Application]
SET [WorkingDirectory]='c:\iris\setup'
WHERE [Title] like 'Inventory%'
go
UPDATE [IRIS].[dbo].[tbl_Application]
SET [CmdLine]='c:\iris\setup\runvbscript.exe wscript.exe c:\iris\update\initiateregisterupdate.vbs /EDMFile=c:\edm\system.properties /q'
WHERE [Title]= 'Update Terminals'
go
UPDATE [IRIS].[dbo].[tbl_Application]
SET [WorkingDirectory]='c:\iris\setup'
WHERE [Title]= 'Update Terminals'
go
UPDATE [IRIS].[dbo].[tbl_Application]
SET [CmdLine]='wscript.exe c:\iris\Scripts\runpayrollBNE.vbs'
WHERE [Title] like 'Payroll%'
go
UPDATE [IRIS].[dbo].[tbl_Application]
SET [WorkingDirectory]='C:\IRIS\Bin'
WHERE [Title] like 'Payroll%'
go
UPDATE [IRIS].[dbo].[tbl_Application]
SET [CmdLine]='c:\iris\setup\runvbscript.exe wscript.exe c:\iris\scripts\eod.vbs'
WHERE [Title]= 'End of Day'
go
UPDATE [IRIS].[dbo].[tbl_Application]
SET [WorkingDirectory]='C:\IRIS\Setup'
WHERE [Title]= 'End of Day'
go
Insert into tblPayrollSetupOptions(OptionID,
SubAppName,
OptionDescription,
OptionMask,
bOption)
Values ('27',
'Employee Master',
'Hide Availability Page',
'1',
'1');
go
go
Insert into tblPayrollSetupOptions(OptionID,
SubAppName,
OptionDescription,
OptionMask,
bOption)
Values ('28',
'Employee Master',
'Hide Schedule Page',
'1',
'1');
go
Use IRIS
UPDATE [IRIS].[dbo].[tblTimedRegisterEvents]
SET [PostEODScript]='c:\iris\scripts\systemmaint.vbs'
WHERE [EventType]='1'
go