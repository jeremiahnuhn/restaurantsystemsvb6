

-------------------------------------------------------------------------
ALTER Procedure sp_InvRpt_Transactions
@FromDateString varchar(15),
@ToDateString varchar(15),
@CostTypeString varchar(15)
As
DECLARE @FromDt datetime, @ToDt datetime
Set @FromDt = CONVERT(datetime,@FromDateString,101)
Set @ToDt = CONVERT(datetime,@ToDateString,101)
--Set @ToDt = DATEADD( Day, 1, @ToDt )



/* Modified by Boddie-Noell
   July 2004
   Break out Menu and Raw Waste
   on RIN0247.RPT
*/


CREATE TABLE #RptTrans (
   InvID       int PRIMARY KEY,
   InvDesc     nvarchar(50),
   MeasDesc    nvarchar(50),
   Cost        money,
   BeginInv    money,
   SalesDepl   money,
   TransferIn  money,
   TransferOut money,
   Purchase    money,
   PrepRecipe  money,
   Waste       money,
   WasteMenu money,
   PhysCount   money
   )

-- 1 step insert IDs:
INSERT INTO #RptTrans ( InvID, InvDesc, MeasDesc, Cost ) 
   SELECT im.InvID, im.InvDesc, mm.MeasDesc, 
   CASE @CostTypeString 
      WHEN 'std' THEN im.StdCost
      WHEN 'avg' THEN im.AvgCost
      WHEN 'last' THEN im.LastCost
      WHEN 'vend' THEN im.VendorCost
   END
   FROM tblInventoryMaster im INNER JOIN tblMeasureMaster mm ON
      im.MeasID = mm.MeasID
   WHERE im.Active <> 0   
-- step 2: Set BeginInv = Theor( @FromDt )
UPDATE #RptTrans SET BeginInv = res.Amt
FROM (
SELECT tr.InvID AS InvID, SUM(tr.Data) AS Amt
FROM (
   SELECT st.InvID AS InvID, 
      (CASE WHEN InOut = 1 OR InOut = 0 THEN 
         st.Data 
       WHEN InOut = 2 THEN 
         - st.Data 
       ELSE 0 END
      ) AS Data
   FROM 
   (
      SELECT si.InvID AS id, si.UpdDate AS upddate
      FROM tblInventoryStatus si JOIN
         (SELECT InvID AS id, MAX(UpdDate) AS upd_dt
          FROM tblInventoryStatus 
          WHERE (TranID = 1) AND (BusinessDate < @FromDt)
          GROUP BY InvID
         ) AS tt ON ((si.InvID = tt.id) AND (si.TranID = 1) AND (si.UpdDate = tt.upd_dt))
   ) AS pc INNER JOIN tblInventoryStatus AS st ON pc.id = st.InvID AND 
        pc.upddate <= st.UpdDate
   WHERE (st.BusinessDate < @FromDt)
   ) AS tr
GROUP BY tr.InvID
) AS res
WHERE #RptTrans.InvID = res.InvID

-- step 3: Set PhysCount = Theor( @ToDt )
UPDATE #RptTrans SET PhysCount = res.Amt
FROM (
SELECT tr.InvID AS InvID, SUM(tr.Data) AS Amt
FROM (
   SELECT st.InvID AS InvID, 
      (CASE WHEN InOut = 1 OR InOut = 0 THEN 
         st.Data 
       WHEN InOut = 2 THEN 
         - st.Data 
       ELSE 0 END
      ) AS Data
   FROM 
   (
      SELECT si.InvID AS id, si.UpdDate AS upddate
      FROM tblInventoryStatus si JOIN
         (SELECT InvID AS id, MAX(UpdDate) AS upd_dt
          FROM tblInventoryStatus 
          WHERE (TranID = 1) AND (BusinessDate <= @ToDt) AND (BusinessDate >= @FromDt)
          GROUP BY InvID
         ) AS tt ON ((si.InvID = tt.id) AND (si.TranID = 1) AND (si.UpdDate = tt.upd_dt))
   ) AS pc INNER JOIN tblInventoryStatus AS st ON pc.id = st.InvID AND 
        pc.upddate <= st.UpdDate
   WHERE (st.BusinessDate <= @ToDt)
   ) AS tr
GROUP BY tr.InvID
) AS res
WHERE #RptTrans.InvID = res.InvID

-- step 4:
UPDATE #RptTrans SET 
   SalesDepl=res.SalesDepl,
   TransferIn=res.TransferIn,
   TransferOut=res.TransferOut,
   Purchase=res.Purchase,
   PrepRecipe=res.PrepRecipe,
   Waste=res.Waste,
   WasteMenu=res.WasteMenu
FROM (
   SELECT st.InvID, 
      SUM(CASE st.TranID WHEN 3 THEN st.Data ELSE 0 END) AS SalesDepl, 
      SUM(CASE st.TranID WHEN 4 THEN st.Data ELSE 0 END) AS TransferIn, 
      SUM(CASE st.TranID WHEN 5 THEN st.Data ELSE 0 END) AS TransferOut, 
      SUM(CASE st.TranID WHEN 6 THEN st.Data ELSE 0 END) AS Purchase, 
      SUM(CASE st.TranID WHEN 12 THEN st.Data ELSE 0 END) AS Waste, 
      SUM(CASE st.TranID WHEN 2 THEN st.Data ELSE 0 END) AS WasteMenu, 
      SUM(CASE st.TranID WHEN 7 THEN st.Data ELSE 0 END) AS PrepRecipe 
   FROM tblInventoryStatus st
   WHERE st.BusinessDate >= @FromDt AND st.BusinessDate <= @ToDt
   GROUP BY st.InvID
) AS res
WHERE #RptTrans.InvID = res.InvID

SELECT 
   InvID,
   InvDesc,
   MeasDesc,
   Cost,
   ISNULL(BeginInv,0) AS BeginInv,
   ISNULL(SalesDepl,0) AS SalesDepl,
   ISNULL(TransferIn,0) AS TransferIn,
   ISNULL(TransferOut,0) AS TransferOut,
   ISNULL(Purchase,0) AS Purchase,
   ISNULL(PrepRecipe,0) AS PrepRecipe,
    ISNULL(WasteMenu,0) AS WasteMenu,
   ISNULL(Waste,0) AS Waste,
   PhysCount
FROM #RptTrans ORDER BY InvDesc

	return (0)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

