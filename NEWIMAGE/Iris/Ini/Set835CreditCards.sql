INSERT INTO [IRIS].[dbo].[tblCfgSetting]([SettingID], [SettingDesc], [SettingVal])
VALUES(835,'Hide Cancel button on Credit',1)
go

INSERT INTO [IRIS].[dbo].[tblCfgTabSetting]([TabID], [SettingID])
VALUES(2,835)
go

INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(1, 835, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(2, 835, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(3, 835, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(4, 835, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(5, 835, 1)
go

INSERT INTO [IRIS].[dbo].[tblCfgSetting]([SettingID], [SettingDesc], [SettingVal])
VALUES(834,'Add Retry button on Credit',1)
go

INSERT INTO [IRIS].[dbo].[tblCfgTabSetting]([TabID], [SettingID])
VALUES(2,834)
go

INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(1, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(2, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(3, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(4, 834, 1)
go
INSERT INTO [IRIS].[dbo].[tblCfgRegModeDefSetting]([RegModeID], [SettingID], [SettingVal])
VALUES(5, 834, 1)
go