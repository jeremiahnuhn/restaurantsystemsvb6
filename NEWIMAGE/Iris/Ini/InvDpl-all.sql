if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_InvDpl_DepleteForDateRange]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_InvDpl_DepleteForDateRange]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_InvDpl_InsertIngred]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_InvDpl_InsertIngred]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_InvDpl_MakeRecipeItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_InvDpl_MakeRecipeItems]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_InvDpl_Test]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_InvDpl_Test]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_InvDpl_WasteForDateRange]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_InvDpl_WasteForDateRange]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_DepletedItems]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_DepletedItems]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_RptMeasureDepleted]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_RptMeasureDepleted]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_WastedItems]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_WastedItems]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_ItemRecipeFactors]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_ItemRecipeFactors]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_LeastInvMeas]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_LeastInvMeas]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_RecipeFactor]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_RecipeFactor]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_RecpIngredFactors]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_RecpIngredFactors]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_InvDpl_RptMeasFactor]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_InvDpl_RptMeasFactor]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_InvDpl_Depleted]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_InvDpl_Depleted]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_InvDpl_RecipeItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_InvDpl_RecipeItems]
GO

CREATE TABLE [dbo].[tbl_InvDpl_Depleted] (
	[InvID] [int] NOT NULL ,
	[UpdDate] [datetime] NOT NULL ,
	[Amount] [money] NOT NULL ,
	[MeasFactor] [money] NOT NULL ,
	[EmployeeID] [int] NOT NULL ,
	[BusinessDate] [datetime] NOT NULL ,
	[OrderNum] [int] NOT NULL ,
	[RegisterNum] [smallint] NOT NULL ,
	[SequenceNum] [int] NOT NULL ,
	[MenuItemNum] [int] NULL ,
	[RecpID] [int] NULL ,
	[TranID] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tbl_InvDpl_RecipeItems] (
	[RecipeID] [int] NULL ,
	[InvID] [int] NULL ,
	[Qty] [money] NULL ,
	[MeasID] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tbl_InvDpl_Depleted] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_InvDpl_Depleted] PRIMARY KEY  CLUSTERED 
	(
		[InvID],
		[UpdDate],
		[MeasFactor],
		[OrderNum],
		[RegisterNum],
		[SequenceNum],
		[TranID]
	)  ON [PRIMARY] 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_InvDpl_ItemRecipeFactors
AS
SELECT im.ItemNum, im.RecipeID, 
    CASE WHEN (ISNULL(ilm.MeasFactor, 0) = 0) OR
    (ISNULL(im.RecipeServeQty, 0) = 0) 
    THEN (CASE WHEN (ISNULL(slm.MeasFactor, 0) = 0) OR
    (ISNULL(rm.ServeQty, 0) = 0) 
    THEN 1 ELSE rm.ServeQty / slm.MeasFactor END) 
    ELSE (im.RecipeServeQty / ilm.MeasFactor) 
    END AS ServeFactor, CASE WHEN (ISNULL(mlm.MeasFactor, 
    0) = 0) OR
    (ISNULL(rm.MakeQty, 0) = 0) 
    THEN 1 ELSE (rm.MakeQty / mlm.MeasFactor) 
    END AS MakeFactor
FROM dbo.tbl_ItemMaster im INNER JOIN
    dbo.tblRecipeMaster rm ON 
    im.RecipeID = rm.RecpID LEFT OUTER JOIN
    dbo.tblLinkMeas ilm ON 
    im.RecipeServeMeasID = ilm.MeasID AND 
    im.RecipeID = ilm.InvID LEFT OUTER JOIN
    dbo.tblLinkMeas mlm ON rm.MeasID = mlm.MeasID AND 
    rm.RecpID = mlm.InvID LEFT OUTER JOIN
    dbo.tblLinkMeas slm ON rm.ServeMeasID = slm.MeasID AND 
    rm.RecpID = slm.InvID
WHERE ISNULL(mlm.IsRecipe, - 1) <> 0 AND ISNULL(slm.IsRecipe, 
    - 1) <> 0 AND ISNULL(ilm.IsRecipe, - 1) <> 0

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE VIEW dbo.vw_InvDpl_LeastInvMeas
AS
SELECT InvID, MeasID, MeasFactor, IsRecipe
FROM dbo.tblLinkMeas lm
WHERE (MeasFactor =
        (SELECT MAX(MeasFactor)
      FROM dbo.tblLinkMeas
      WHERE InvID = lm.InvID AND IsRecipe = lm.IsRecipe))

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_InvDpl_RecipeFactor
AS
SELECT DISTINCT 
    rm.RecpID, CASE WHEN ISNULL(lmm.IsRecipe, 0) = 0 OR ISNULL(lmm.MeasFactor, 0) = 0 THEN 1 
    ELSE lmm.MeasFactor END AS MakeFactor, 
    CASE WHEN ISNULL(lms.IsRecipe, 0) = 0 OR ISNULL(lms.MeasFactor, 0) = 0 THEN 1 
    ELSE lms.MeasFactor END AS ServeFactor
FROM dbo.tblRecipeMaster rm LEFT OUTER JOIN
    dbo.tblLinkMeas lmm ON rm.MeasID = lmm.MeasID AND 
    rm.RecpID = lmm.InvID LEFT OUTER JOIN
    dbo.tblLinkMeas lms ON rm.ServeMeasID = lms.MeasID AND 
    rm.RecpID = lms.MeasFactor
WHERE ISNULL(lmm.IsRecipe, - 1) <> 0 AND ISNULL(lms.IsRecipe, - 1) <> 0

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_InvDpl_RecpIngredFactors
AS
SELECT ri.RecpID, ri.IngredID, 
   CASE WHEN ISNULL(lmm.IsRecipe, 0) = 0 OR ISNULL(lmm.MeasFactor, 0) = 0 OR
      ISNULL(rm.MakeQty, 0) = 0 THEN 10000 ELSE 
      (rm.MakeQty * 10000 / lmm.MeasFactor) END AS MakeFactor, 
   CASE WHEN ISNULL(lmi.IsRecipe, 0) = 0 OR ISNULL(lmi.MeasFactor, 0) = 0 OR
      ISNULL(ri.QtyUsed, 0) = 0 THEN 10000 ELSE 
      (ri.QtyUsed * 10000 / lmi.MeasFactor) END AS IngredFactor
FROM tblRecipeIngred ri INNER JOIN tblRecipeMaster rm ON ri.IngredID = rm.RecpID 
   LEFT OUTER JOIN tblLinkMeas lmi ON ri.IngredID = lmi.InvID AND ri.RecpMeasID = lmi.MeasID 
   LEFT OUTER JOIN tblLinkMeas lmm ON rm.RecpID = lmm.InvID AND rm.MeasID = lmm.MeasID
WHERE (ri.IsRecipe <> 0) AND (ISNULL(lmm.IsRecipe, - 1) <> 0) AND 
   (ISNULL(lmi.IsRecipe, - 1) <> 0)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_InvDpl_RptMeasFactor
AS
SELECT dbo.tblInventoryMaster.InvID, 
    dbo.tblInventoryMaster.MeasID, 
    dbo.tblLinkMeas.MeasFactor
FROM dbo.tblInventoryMaster INNER JOIN
    dbo.tblLinkMeas ON 
    dbo.tblInventoryMaster.InvID = dbo.tblLinkMeas.InvID AND 
    dbo.tblInventoryMaster.MeasID = dbo.tblLinkMeas.MeasID
WHERE (dbo.tblLinkMeas.IsRecipe = 0)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW dbo.vw_InvDpl_DepletedItems
AS
SELECT     oi.BusinessDate, oi.RegisterNum, oi.OrderNum, oi.SequenceNum, oi.ItemNum, oi.OrdTime, oi.Quantity, oi.Numerator, oi.Denominator, oi.Weight, 
                      ri.InvID, ri.Qty * irf.ServeFactor / irf.MakeFactor AS DepletedQty, ri.MeasID, oi.ServerNum, ri.RecipeID
FROM         dbo.tblOrderItem oi INNER JOIN
                      dbo.vw_InvDpl_ItemRecipeFactors irf ON oi.ItemNum = irf.ItemNum INNER JOIN
                      dbo.tbl_InvDpl_RecipeItems ri ON irf.RecipeID = ri.RecipeID
WHERE     (oi.State = 1) OR
                      (oi.State = 128)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_InvDpl_RptMeasureDepleted
AS
SELECT dbo.tbl_InvDpl_Depleted.InvID, 
    dbo.tbl_InvDpl_Depleted.UpdDate, 
    dbo.tbl_InvDpl_Depleted.EmployeeID, 
    dbo.tbl_InvDpl_Depleted.BusinessDate, 
    SUM(dbo.tbl_InvDpl_Depleted.Amount * dbo.vw_InvDpl_RptMeasFactor.MeasFactor
     / dbo.tbl_InvDpl_Depleted.MeasFactor) AS RptAmount, 
    TranID
FROM dbo.tbl_InvDpl_Depleted INNER JOIN
    dbo.vw_InvDpl_RptMeasFactor ON 
    dbo.tbl_InvDpl_Depleted.InvID = dbo.vw_InvDpl_RptMeasFactor.InvID
GROUP BY dbo.tbl_InvDpl_Depleted.InvID, 
    dbo.tbl_InvDpl_Depleted.UpdDate, 
    dbo.tbl_InvDpl_Depleted.EmployeeID, 
    dbo.tbl_InvDpl_Depleted.BusinessDate, 
    dbo.vw_InvDpl_RptMeasFactor.MeasFactor, 
    dbo.tbl_InvDpl_Depleted.MeasFactor, TranID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.vw_InvDpl_WastedItems
AS
SELECT oi.BusinessDate, oi.RegisterNum, oi.OrderNum, 
    oi.SequenceNum, oi.ItemNum, oi.OrdTime, oi.Quantity, 
    oi.Numerator, oi.Denominator, oi.Weight, ri.InvID, 
    ri.Qty * irf.ServeFactor / irf.MakeFactor AS DepletedQty, 
    ri.MeasID, oi.ServerNum, ri.RecipeID
FROM dbo.tblOrderItem oi INNER JOIN
    dbo.vw_InvDpl_ItemRecipeFactors irf ON 
    oi.ItemNum = irf.ItemNum INNER JOIN
    dbo.tbl_InvDpl_RecipeItems ri ON 
    irf.RecipeID = ri.RecipeID
WHERE (oi.State = 8) AND (oi.Destination = 0x10000000)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE Procedure sp_InvDpl_InsertIngred @RecipeId int, @MeasType int = 0
As
   CREATE TABLE #IngredRecipes
   (
	   id			Int,
	   qty		Money
   )	

   CREATE TABLE #TmpRecipes
   (
	   id			Int,
	   qty		Money
   )
	   
   CREATE TABLE #IngredItems
   (
	   id			Int,
	   qty		Money, 
      mid      int
   )
	
--DECLARE @RecCount Int
   DECLARE @InvId int, @Qty money, @MeasId int, @Amt money

   INSERT INTO #IngredRecipes( id, qty ) VALUES ( @RecipeId, 1 )

   WHILE (1=1)
   BEGIN
   --1) insert Inv items
	   INSERT INTO #IngredItems( id, qty, mid )
	      SELECT r.IngredID, (r.QtyUsed * i.qty), r.RecpMeasID
	      FROM tblRecipeIngred r
	      JOIN #IngredRecipes i ON r.RecpID = i.id
	      WHERE r.IsRecipe=0

   --2) select sub-recipes
	   INSERT INTO #TmpRecipes( id, qty )
	      SELECT rif.IngredID, 
            i.qty * rif.IngredFactor / rif.MakeFactor -- ! calc measure factor here
	      FROM vw_InvDpl_RecpIngredFactors rif
	      INNER JOIN #IngredRecipes i ON rif.RecpID = i.id 

   --3) if no more sub-recipes we are done
	   IF ( (SELECT COUNT(*) FROM #TmpRecipes) = 0 )
		   BREAK

   --4) init subrecipe list
	   DELETE FROM #IngredRecipes
	   INSERT INTO #IngredRecipes ( id, qty ) SELECT id, qty FROM #TmpRecipes
	   DELETE FROM #TmpRecipes
   END

   IF @MeasType = 0 -- use Report Measure
      INSERT INTO tbl_InvDpl_RecipeItems
         SELECT @RecipeId, ii.id, 
            SUM( ii.qty * lmd.MeasFactor / lmi.MeasFactor ),
            lmd.MeasID
         FROM #IngredItems ii INNER JOIN
             tblInventoryMaster im ON ii.id = im.InvID INNER JOIN 
             tblLinkMeas lmd ON im.MeasID = lmd.MeasID AND im.InvID = lmd.InvID INNER JOIN 
             tblLinkMeas lmi ON ii.id = lmi.InvID AND ii.mid = lmi.MeasID
         WHERE lmd.IsRecipe = 0 AND lmd.IsRecipe = 0
         GROUP BY ii.id, lmd.MeasID
   ELSE IF @MeasType = 1 -- use Least Measure
      INSERT INTO tbl_InvDpl_RecipeItems
         SELECT @RecipeId, ii.id, 
            SUM( ii.qty * lim.MeasFactor / lmi.MeasFactor ),
            lim.MeasID
         FROM #IngredItems ii INNER JOIN
             vw_InvDpl_LeastInvMeas lim ON ii.id = lim.InvID INNER JOIN 
             tblLinkMeas lmi ON ii.id = lmi.InvID AND ii.mid = lmi.MeasID
         WHERE lim.IsRecipe = 0
         GROUP BY ii.id, lim.MeasID
   ELSE IF @MeasType = 2 -- use Original Measure
      INSERT INTO tbl_InvDpl_RecipeItems
         SELECT @RecipeId, ii.id, SUM( ii.qty ), ii.mid
         FROM #IngredItems ii 
         GROUP BY ii.id, ii.mid
return (0)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE Procedure sp_InvDpl_MakeRecipeItems @MeasType int = 0
As
   DECLARE @RecipeId int
   DECLARE cursor_ CURSOR FOR
      SELECT RecpID FROM tblRecipeMaster

   DELETE FROM tbl_InvDpl_RecipeItems

   OPEN cursor_

   FETCH NEXT FROM cursor_ INTO @RecipeId

   WHILE @@FETCH_STATUS = 0 BEGIN
      EXEC sp_InvDpl_InsertIngred @RecipeId, @MeasType
      FETCH NEXT FROM cursor_ INTO @RecipeId
   END

   CLOSE cursor_
   DEALLOCATE cursor_
return (0)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE dbo.sp_InvDpl_WasteForDateRange
	(
		@FromStr nvarchar(20) = NULL,
		@ToStr nvarchar(20) = NULL,
      @UpdInvStatusFlag int = 0
	)

AS
	/* SET NOCOUNT ON */
	DECLARE @FromDt datetime, @ToDt datetime, @TranID int

   SET @TranID = 2 -- Waste

   IF @FromStr IS NULL BEGIN
      SELECT @FromDt = BusinessDate FROM tblBusinessDate
      SET @ToDt = @FromDt
   END
   ELSE BEGIN
      SET @FromDt = CONVERT( datetime, @FromStr, 101 )
      IF @ToStr IS NULL
         SET @ToDt = @FromDt
      ELSE
         SET @ToDt = CONVERT( datetime, @ToStr, 101 )
   END
	
   EXEC sp_InvDpl_MakeRecipeItems 1 -- 0-report measure, 1-least measure, 2-recipe measure

	DELETE FROM tbl_InvDpl_Depleted 
      WHERE BusinessDate>=@FromDt AND BusinessDate<=@ToDt AND TranID = @TranID

	INSERT INTO tbl_InvDpl_Depleted 
		SELECT wi.InvID, wi.OrdTime, SUM(wi.Quantity*wi.DepletedQty), 
         lm.MeasFactor, wi.ServerNum, wi.BusinessDate, 
			wi.OrderNum, wi.RegisterNum, wi.SequenceNum, wi.ItemNum, wi.RecipeID,
         @TranID
		FROM vw_InvDpl_WastedItems wi INNER JOIN tblLinkMeas lm ON 
         wi.InvID = lm.InvID AND wi.MeasID = lm.MeasID
		WHERE wi.BusinessDate>=@FromDt AND wi.BusinessDate<=@ToDt
         AND lm.IsRecipe = 0
      GROUP BY wi.InvID, wi.OrdTime, lm.MeasFactor, wi.ServerNum, wi.BusinessDate, 
			wi.OrderNum, wi.RegisterNum, wi.SequenceNum, wi.ItemNum, wi.RecipeID

   IF @UpdInvStatusFlag <> 0
   BEGIN
      DELETE FROM tblInventoryStatus WHERE BusinessDate>=@FromDt AND BusinessDate<=@ToDt
         AND TranID = @TranID
	   INSERT INTO tblInventoryStatus (InvID, TranID, UpdDate, Data, InOut, EmployeeID, BusinessDate) 
		   SELECT InvID, TranID, UpdDate, SUM(RptAmount), 2, MIN(EmployeeID), MIN(BusinessDate)
		   FROM vw_InvDpl_RptMeasureDepleted
		   WHERE BusinessDate>=@FromDt AND BusinessDate<=@ToDt AND TranID = @TranID
         GROUP BY InvID, UpdDate, TranID
   END

RETURN(0) 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE dbo.sp_InvDpl_DepleteForDateRange
	(
		@FromStr nvarchar(20) = NULL,
		@ToStr nvarchar(20) = NULL,
      @UpdInvStatusFlag int = 0
	)

AS
	/* SET NOCOUNT ON */
	DECLARE @FromDt datetime, @ToDt datetime, @TranID int

   SET @TranID = 3 -- depleteion

   IF @FromStr IS NULL BEGIN
      SELECT @FromDt = BusinessDate FROM tblBusinessDate
      SET @ToDt = @FromDt
   END
   ELSE BEGIN
      SET @FromDt = CONVERT( datetime, @FromStr, 101 )
      IF @ToStr IS NULL
         SET @ToDt = @FromDt
      ELSE
         SET @ToDt = CONVERT( datetime, @ToStr, 101 )
   END

	EXEC sp_InvDpl_MakeRecipeItems 1 -- 0-report measure, 1-least measure, 2-recipe measure
	DELETE FROM tbl_InvDpl_Depleted 
      WHERE BusinessDate>=@FromDt AND BusinessDate<=@ToDt AND TranID = @TranID
	INSERT INTO tbl_InvDpl_Depleted 
		SELECT di.InvID, di.OrdTime, SUM(di.Quantity*di.DepletedQty), 
         lm.MeasFactor, di.ServerNum, di.BusinessDate, 
			di.OrderNum, di.RegisterNum, di.SequenceNum, di.ItemNum, di.RecipeID,
         @TranID
		FROM vw_InvDpl_DepletedItems di INNER JOIN tblLinkMeas lm ON 
         di.InvID = lm.InvID AND di.MeasID = lm.MeasID
		WHERE di.BusinessDate>=@FromDt AND di.BusinessDate<=@ToDt
         AND lm.IsRecipe = 0
      GROUP BY di.InvID, di.OrdTime, lm.MeasFactor, di.ServerNum, di.BusinessDate, 
			di.OrderNum, di.RegisterNum, di.SequenceNum, di.ItemNum, di.RecipeID

   IF @UpdInvStatusFlag <> 0
   BEGIN
      DELETE FROM tblInventoryStatus WHERE BusinessDate>=@FromDt AND BusinessDate<=@ToDt
         AND TranID = @TranID
	   INSERT INTO tblInventoryStatus (InvID, TranID, UpdDate, Data, InOut, EmployeeID, BusinessDate) 
		   SELECT InvID, TranID, UpdDate, SUM(RptAmount), 2, MIN(EmployeeID), MIN(BusinessDate)
		   FROM vw_InvDpl_RptMeasureDepleted
		   WHERE BusinessDate>=@FromDt AND BusinessDate<=@ToDt AND TranID = @TranID
         GROUP BY InvID, UpdDate, TranID
   END

RETURN(0) 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE dbo.sp_InvDpl_Test
/*
	(
		@parameter1 datatype = default value,
		@parameter2 datatype OUTPUT
	)
*/
AS
--	EXEC sp_InvDpl_DepleteForDateRange '10/7/2002', '10/7/2002', 1 -- replace invstat
	EXEC sp_InvDpl_DepleteForDateRange '03/06/2003','03/09/2003',@UpdInvStatusFlag = 0 -- for current business date
	EXEC sp_InvDpl_WasteForDateRange '03/06/2003','03/09/2003',@UpdInvStatusFlag = 0 -- for current business date
	SELECT * FROM tbl_InvDpl_Depleted 
      WHERE BusinessDate >= '03/06/2003' AND BusinessDate <= '03/09/2003' 
         AND TranID = 2
      ORDER BY BusinessDate
	RETURN 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
