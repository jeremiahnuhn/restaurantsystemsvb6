[Database]
Database=..\Data\POSTrans.mdb
Description=Update Forever Totals

[SQLStatement]
SQL1=DELETE tblEODGrandTotal.* FROM tblEODGrandTotal WHERE tblEODGrandTotal.BusinessDate=%BusinessDate%;
SQL2=INSERT INTO tblEODGrandTotal (BusinessDate, RegisterNum, PrevEODTotal, EODTotal) SELECT %BusinessDate% AS BusinessDate, tblGrandTotal.RegisterNum, tblGrandTotal.PrevDayTotal, tblGrandTotal.CurrentTotal FROM tblGrandTotal;
SQL3=UPDATE tblGrandTotal SET tblGrandTotal.PrevDayTotal=tblGrandTotal.CurrentTotal, tblGrandTotal.CurrentTotal=0;
