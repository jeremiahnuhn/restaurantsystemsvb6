use Iris
go
Update tblmenus
Set ApplicationID = '2',
    MenuID = '7610',
    MenuName = 'Update Inventory',
    Description = 'Update Inventory',
    MenuLevel = '1',
    ParentID = '182',
    SeqNum = '10',
    IconID = '26',
    TypeID = '3',
    FormID = '0',
    Parameter = '"c:\iris\bin\InvTransEditor.exe"',
    SecurityLevel ='50'
Where Description = 'Update Inventory';
use Iris
go
Update tblmenus
Set ApplicationID = '2',
    MenuID = '903',
    MenuName = 'Inventory Count Sheet',
    Description = 'Inventory Count Sheet',
    MenuLevel = '1',
    ParentID = '182',
    SeqNum = '11',
    IconID = '4',
    TypeID = '4',
    FormID = '0',
    Parameter = 'rin0037.rpt',
    SecurityLevel ='50'
Where Description = 'Daily Count Sheet';
use Iris
go
Update tblmenus
Set ApplicationID = '4',
    MenuID = '904',
    MenuName = 'Weekly Count Sheet',
    Description = 'Weekly Count Sheet',
    MenuLevel = '1',
    ParentID = '182',
    SeqNum = '12',
    IconID = '4',
    TypeID = '4',
    FormID = '0',
    Parameter = 'rin0044.rpt',
    SecurityLevel = '50'
Where MenuID = '904';
go