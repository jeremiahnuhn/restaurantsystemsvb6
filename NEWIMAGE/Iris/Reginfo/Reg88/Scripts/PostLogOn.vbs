' ---------------------------------------------------------------------------------------------------------------------------
'
' FILENAME:   PostLogOn.vbs
'
' COPYRIGHT:  (C) 1995-2002 Progressive Software, Inc.  All rights reserved.
'
' AUTHOR:   Lenny Giambalvo
'
' EMAIL:    lgiambalvo@ProgressiveSoftware.com
'
' VERSION:    1.0 (10/01/2002) - Initial creation of the PreLogOn script
'
' PURPOSE:    To Upgrade a known version of IRIS
'
' NOTES:
'       This script was created using the Visual Studio Dot Net IDE. Some formatting
'       may not be available in other editors such as notepad.
'
'       DO NOT MODIFY OR CHANGE ANY PART OF THIS SCRIPT OR IRREVERSIBLE DAMAGE MAY OCCUR TO YOUR SYSTEMS.
'
'       All Copyright notices contained within this source code must remain in place and may not be removed or modified.
'
'       To be successfull this script must have **** ADMINISTRATOR **** privledges on the local machine and the server
'
' ----------------------------------------------------------------------------------------------------------------------------
'##### Start check for x64 System #####
'This will check for a x64 System and restart the script using the 32bit version if needed
'xsCheckArchitecture v1.0
Sub xsCheckArchitecture
   On Error Resume Next
   If Not IsObject(WScript) Then Exit Sub
   Dim oWshShell, oWshProcEnv, oExec, Argument, sCmd, iResult
   Set oWshShell =  CreateObject("WScript.Shell")
   Set oWshProcEnv = oWshShell.Environment("Process")
   If LCase(oWshProcEnv("PROCESSOR_ARCHITECTURE")) <> "x86" and Instr(1, WScript.FullName, "SysWOW64", 1) = 0 then
      sCmd = Chr(34) & oWshProcEnv("SystemRoot") & "\SysWOW64\" &  Mid(WScript.FullName, InstrRev(WScript.FullName, "\") + 1) & Chr(34)
      sCmd = sCmd & " //NoLogo " & Chr(34) & WScript.ScriptFullName & Chr(34)
      For Each Argument in WScript.Arguments
         Argument = Split(Argument,"=",2)
         sCmd = sCmd & " " & Chr(34) & Argument(0) & Chr(34)
         If UBound(Argument) = 1 Then sCmd = sCmd & "=" & Chr(34) & Argument(1) & Chr(34)
      Next
      Set oExec = oWshShell.Exec(sCmd)
      Do While oExec.Status = 0
         Do While Not oExec.StdOut.AtEndOfStream
            WScript.StdOut.Write oExec.StdOut.Read(1)
         Loop
      Loop
      Do While Not oExec.StdErr.AtEndOfStream
         WScript.StdErr.Write oExec.StdErr.Read(1)
      Loop
      If Not oExec.ExitCode = 0  then WScript.Quit oExec.ExitCode
      WScript.quit
   End If
End Sub
xsCheckArchitecture
'##### End check for x64 System #####

'ActiveX Objects
Dim oWSHShell
Dim oFSO
Dim oIniEditor
Dim oWSHNetwork
Dim oIrisNet
Dim oUpdateLinks
Dim oDbUtils
Dim Status
Dim PostInstallScript
Dim PreInstallScript

'Global variables
Dim g_IrisPath
Dim g_ServerName
Dim g_RegNum
Dim g_UpdateDir
Dim g_ServerIrisPath
Dim g_IsServer
Dim g_NewVersion
Dim g_StatusMonitoring
Dim g_SuppressReboot
Dim g_ServerHostName
Dim g_IsRdsBackup

Dim g_LogFileEntries()
Dim g_LogFileCounter

Dim bSupportBlobUpgrade
Dim bCurrentBlobUpgradeable

Dim g_IrisVersion
Dim g_IrisBuild


'--------------------  CONSTANTS  ------------------------------

'event log constants
Const SUCCESS = 0
Const ERRROR = 1
Const WARNING = 2
Const INFO = 4

'---------------------------------------------------------------
'         MAIN SCRIPT LOGIC
'---------------------------------------------------------------

  On Error Resume Next

  g_StatusMonitoring = False

  '-- This com object creation was moved from createCOMObjects since it is needed to get the local system configuration
  '-- and this way I can tell if I need to fail and exit better during the createCOMObjects routine some objects are only
  '-- needed on the server so if they fail to create on the register it is not necessary to exit the script.

  Set oWSHShell = WScript.CreateObject("WScript.Shell")
    If err.number then
      Wscript.Quit
    end If

  Set objExplorer = WScript.CreateObject("InternetExplorer.Application")
  objExplorer.Navigate "about:blank"
  objExplorer.ToolBar = 0
  objExplorer.StatusBar = 0
  objExplorer.Visible = 1
  objExplorer.FullScreen = 1
  objExplorer.Document.Body.Scroll = "No"
  objExplorer.Document.Body.BGcolor = "#FF0000"

  PrintMsg ""


  createCOMObjects

  'Clean up Reg to fix MSDE reinstall problem
  oWSHShell.RegDelete ("HKCR\CLSID\{44EC053A-400F-11D0-9DCD-00A0C90391D3}\InprocServer32\")
  oWSHShell.Run "regsvr32 /s /u Atl.dll",0,1
  oWSHShell.Run "regsvr32 /s Atl.dll",0,1
  If Err.Number <> 0 Then Err.Clear

  getLocalSystemInfo

  LogMsg "Started"

  getServerSystemInfo

  TimeOutMinutes = 10
  TimeOut = Now() + (1/24/60 * TimeOutMinutes)
  Do
    PrintMsg "Reading Delta Config..."
    If Not oFSO.FileExists( g_IrisPath & "\Delta\Config.ini") Then
      oFSO.CopyFile g_ServerIrisPath & "\Delta\Config.ini", g_IrisPath & "\Delta\Config.ini", True   
      If Err.Number <> 0 Then Err.Clear
    End If
    If GetConfig("Options","UpgradeFrom") <> "" Then Exit Do
    If Now() > TimeOut Then
      LogMsg "Error: Timed out trying to read config.ini on server: " & g_ServerIrisPath & "\Delta\Config.ini"
      ExitThisScript False, True
    End If
    wscript.Sleep(10000)
  Loop

   '-- Check for Admin
   If LCase(Trim(GetConfig("Admin","ServerOnly"))) = "false" Then
      sAdminName = Trim(GetConfig("Admin","Username"))
      sAdminPass = Trim(GetConfig("Admin","Password"))
      If sAdminName <> "" And sAdminPass <> "" Then
         RunasAdmin sAdminName, sAdminPass
      End If
   End If

  'Tell The server we are Ready
  SetConfig "RDS", "Reg" & CStr(g_RegNum), g_IsRdsBackup
  SetConfig "ReadyForDelta", "Reg" & CStr(g_RegNum), "1"
  SetConfig "DeltaComplete", "Reg" & CStr(g_RegNum), "0"

  ProcessMsgToServer "UpgradeStart", Date & " " & Time
  ProcessMsgToServer "CompleteStatus", "In_Progress"
  ProcessMsgToServer "Progress", 10

  If SameVersion() = False Then
    If CorrectVersion() = False Then
      LogMsg "Incorrect version for this upgrade."
      ProcessMsgToServer "Progress", 100
      ProcessMsgToServer "Message", "Incorrect version for this upgrade."
      ProcessMsgToServer "UpgradeEnd", Date & " " & Time
      ProcessMsgToServer "CompleteStatus", "Completed"
      ExitThisScript False, False
    End If
  Else
    LogMsg "Register already has  this upgrade, skipping upgrade"
    ProcessMsgToServer "Progress", 100
    ProcessMsgToServer "Message", "Upgrade complete"

    ProcessMsgToServer "UpgradeEnd", Date & " " & Time
    ProcessMsgToServer "CompleteStatus", "Completed"

    SetConfig "DeltaComplete", "Reg" & CStr(g_RegNum), "1"
   
    If g_IsRdsBackup = 1 Then CheckWaitStatus

    g_SuppressReboot = True
    ExitThisScript True, False
  End If

  'Wait On Server to upgrade
  TimeOutMinutes = 60
  TimeOut = Now() + (1/24/60 * TimeOutMinutes)
  Do
    PrintMsg "Waiting on Server to upgrade..."
    sConfigValue = GetConfig("DeltaComplete","Server")
    If sConfigValue = "1" Then Exit Do
    If sConfigValue = "-1" Then 
      SetConfig "DeltaComplete", "Reg" & CStr(g_RegNum), "-1"
      ExitThisScript False, False
   End If
    If Now() > TimeOut Then
      LogMsg "Error: Timed out waiting for server to upgrade, will try again next reboot"
      Wscript.Quit
    End If
    wscript.Sleep(10000)
  Loop
  If Err.Number Then Err.Clear

  'Make Backup
  if oFSO.FileExists (g_ServerIrisPath & "\Delta\DeltaBackup.exe") then
    PrintMsg "Creating Backup"
    oFSO.CopyFile g_ServerIrisPath & "\Delta\DeltaBackup.exe", g_IrisPath & "\DeltaBackup.exe",True
    oWSHShell.Run g_IrisPath & "\DeltaBackup.exe",,True
    If Not oFSO.FileExists (g_IrisPath & "\BackupSuccess.txt") Then
      LogMsg "Failed to create Backup"
      ExitThisScript False
    End If
  End if

  SetBlobOptions

    PreInstallScript = GetConfig("Register","PreInstall")

    If PreInstallScript <> "" Then

      ProcessMsgToServer "Message", "Launching PreInstallation script"

      If lcase(Right(PreInstallScript,3)) = "bat" Then
        ExecuteBatchFile g_ServerIrisPath & "\Delta\" & PreInstallScript
      End If

      If lcase(Right(PreInstallScript,3)) = "vbs" Then
        ExecuteScript g_ServerIrisPath & "\Delta\" & PreInstallScript
      End If

  End If

   If oFSO.FileExists(g_IrisPath & "\INI\__sdir.ini") Then
      oFSO.DeleteFile g_IrisPath & "\INI\__sdir.ini",True
   End If

   If oFSO.FileExists(g_IrisPath & "\Data\ItemCache.dat") Then
      oFSO.DeleteFile g_IrisPath & "\Data\ItemCache.dat",True
   End If

   If oFSO.FileExists(g_IrisPath & "\Data\MenuCache.dat") Then
      oFSO.DeleteFile g_IrisPath & "\Data\MenuCache.dat",True
   End If

   'Stop IRIS Service
   PrintMsg "Stopping IRIS services"
   If oFSO.FileExists (g_ServerIrisPath & "\Delta\StopServices.vbs") then
      oFSO.CopyFile g_ServerIrisPath & "\Delta\StopServices.vbs", g_IrisPath & "\Scripts\StopServices.vbs",True
      oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Scripts\StopServices.vbs",1,1
   End if

  'VC 2008 
  if oFSO.FileExists (g_ServerIrisPath & "\Delta\VC2008x86_9.0.30729.6161.exe") then
    PrintMsg "Installing VC 2008"
    oFSO.CopyFile g_ServerIrisPath & "\Delta\VC2008x86_9.0.30729.6161.exe", g_IrisPath & "\Delta\VC2008x86_9.0.30729.6161.exe",True
    oWSHShell.Run g_IrisPath & "\Delta\VC2008x86_9.0.30729.6161.exe /SILENT /NORESTART /RESTARTEXITCODE=3010",,True
    oFSO.DeleteFile g_IrisPath & "\Delta\VC2008x86_9.0.30729.6161.exe",True
  End if

  'ReportViewer 2005
  if oFSO.FileExists (g_ServerIrisPath & "\Delta\ReportViewer2005_SP1_8.0.50727.5677.exe") then
    PrintMsg "Installing ReportViewer 2005"
    oFSO.CopyFile g_ServerIrisPath & "\Delta\ReportViewer2005_SP1_8.0.50727.5677.exe", g_IrisPath & "\Delta\ReportViewer2005_SP1_8.0.50727.5677.exe",True
    oWSHShell.Run g_IrisPath & "\Delta\ReportViewer2005_SP1_8.0.50727.5677.exe /SILENT /NORESTART /RESTARTEXITCODE=3010",,True
    oFSO.DeleteFile g_IrisPath & "\Delta\ReportViewer2005_SP1_8.0.50727.5677.exe",True
  End if

  'Python 
  if oFSO.FileExists (g_ServerIrisPath & "\Delta\Python32x86_3.2.1.exe") then
    PrintMsg "Installing VC 2008"
    oFSO.CopyFile g_ServerIrisPath & "\Delta\Python32x86_3.2.1.exe", g_IrisPath & "\Delta\Python32x86_3.2.1.exe",True
    oWSHShell.Run g_IrisPath & "\Delta\Python32x86_3.2.1.exe /SILENT /NORESTART /RESTARTEXITCODE=3010",,True
    oFSO.DeleteFile g_IrisPath & "\Delta\Python32x86_3.2.1.exe",True
  End if

  'VB6 
  if oFSO.FileExists (g_ServerIrisPath & "\Delta\VB6Pack_1.0.0.2.exe") then
    PrintMsg "Installing VB6 Updates"
    oFSO.CopyFile g_ServerIrisPath & "\Delta\VB6Pack_1.0.0.2.exe", g_IrisPath & "\Delta\VB6Pack_1.0.0.2.exe",True
    oWSHShell.Run g_IrisPath & "\DeltaVB6Pack_1.0.0.2.exe /SILENT /NORESTART /RESTARTEXITCODE=3010",,True
    oFSO.DeleteFile g_IrisPath & "\Delta\VB6Pack_1.0.0.2.exe",True
  End if

  PrintMsg "Copying files."
  ProcessMsgToServer "Progress", 15
    ProcessMsgToServer "Message", "Copying files."
    
   Set oIniEditor = Nothing
    CopyFiles "Delta",True

   Set oIniEditor = CreateObject("IRISIniEditor.IniEditor.1")

  PrintMsg "Registering files."
    ProcessMsgToServer "Progress", 40
    ProcessMsgToServer "Message", "Registering files"
    RegisterFiles

   Set oIniEditor = CreateObject("IRISIniEditor.IniEditor.1")


  PrintMsg "Applying database changes."
    ProcessMsgToServer "Progress", 50
  ProcessMsgToServer "Message", "Applying database changes"
    ApplyDbChanges

  PrintMsg "Updating version information."
    ProcessMsgToServer "Progress", 80
    ProcessMsgToServer "Message", "Updating version information"
    UpdateVersionInfo

    PostInstallScript = GetConfig("Register","PostInstall")

    If PostInstallScript <> "" Then

  PrintMsg "Launching post-installation script."
      ProcessMsgToServer "Progress", 85
      ProcessMsgToServer "Message", "Launching post-installation script"

      If lcase(Right(PostInstallScript,3)) = "bat" Then
        ExecuteBatchFile g_ServerIrisPath & "\Delta\" & PostInstallScript
      End If

      If lcase(Right(PostInstallScript,3)) = "vbs" Then
        ExecuteScript g_ServerIrisPath & "\Delta\" & PostInstallScript
      End If

  End If

   'Restart IRIS Service
   PrintMsg "Starting Services"
   oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Scripts\StartServices.vbs",1,1


  '--- Check to see if this upgrade needs to wait for an RDS update from the server ---

  LogMsg "Update " & g_NewVersion & " applied successfully"


  PrintMsg "Upgrade complete."
  ProcessMsgToServer "Progress", 100
  ProcessMsgToServer "Message", "Upgrade complete"

  ProcessMsgToServer "UpgradeEnd", Date & " " & Time
  ProcessMsgToServer "CompleteStatus", "Completed"

  SetConfig "DeltaComplete", "Reg" & CStr(g_RegNum), "1"

  If g_IsRdsBackup = 1 Then CheckWaitStatus

  ExitThisScript True, False

'---------------------------------------------------------------
'         END MAIN SCRIPT LOGIC
'---------------------------------------------------------------


Sub CheckWaitStatus

  Dim CurrentStatus


  'Wait On Server to upgrade
  TimeOutMinutes = 30
  TimeOut = Now() + (1/24/60 * TimeOutMinutes)
  Do
    PrintMsg "Waiting on RDS to Replicate..."
    sConfigValue = GetConfig("RDS","Server")
    If sConfigValue = "1" Then Exit Do
    If sConfigValue = "-1" Then Exit Do
    If GetConfig("DeltaComplete","Server") = "-1" Then Exit Do
    If Now() > TimeOut Then Exit Do
    wscript.Sleep(10000)
  Loop

End Sub


Sub ExecuteScript(ScriptName)
  On Error Resume Next
  
  oWSHShell.Run "wscript.exe //B " & ScriptName ,1,1
  If Err.Number Then
    LogMsg "Script: " & ScriptName & " failed to execute"
  End if

End Sub

Sub ExecuteBatchFile(BatchFile)
  On Error Resume Next

  oWSHShell.Run BatchFile ,1,1
  If Err.Number Then
    LogMsg "Batch: " & BatchFile & " failed to execute"
  End if

End Sub


'------------------------------------------------------------------------------------------------------------
Function CorrectVersion()

  Dim InstFromVersion
  Dim InstalledVersion
  Dim EligibleVersions

  CorrectVersion = false

  On Error Resume Next


  InstFromVersion = GetConfig("Options","UpgradeFrom")

  EligibleVersions = Split(InstFromVersion,";",-1)

  InstalledVersion = oWSHShell.RegRead("HKLM\SOFTWARE\Progressive Software\IRIS\Common\VERSION")

  LogMsg "Installed Iris version: " & InstalledVersion
  LogMsg "Upgrade from Iris version: " & InstFromVersion

 If InstFromVersion = "" Then
  ExitThisScript False, True
 End If

  For i = 0 To UBound(EligibleVersions)
    If EligibleVersions(i) = InstalledVersion Then
      CorrectVersion = True
      Exit Function
    End if
  Next

   LogMsg "Warning: Version in the Registry is not valid or is missing, Checking version of _Security.dll"
   GetIrisFileVersion g_IrisPath & "\Bin\_security.dll"
   LogMsg "InstalledVersion=" & g_IrisVersion
   LogMsg "Build=" & g_IrisBuild
   oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Common\VERSION", g_IrisVersion
   oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Common\Build", g_IrisBuild
   For i = 0 To UBound(EligibleVersions)
      If EligibleVersions(i) = g_IrisVersion Then
         CorrectVersion = true
      End if
   Next

End Function

'------------------------------------------------------------------------------------------------------------
Sub RegisterFiles()
  On Error Resume Next
   oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Setup\SetupSystemFiles.vbs /source=" & Chr(34) & g_ServerIrisPath & "\Setup\System32" & Chr(34),1,1
   oWSHShell.Run g_IrisPath & "\Bin\ComReg.exe /" & g_IrisPath & "\Bin",1,1
   oWSHShell.Run g_IrisPath & "\Bin\ComReg.exe /" & g_IrisPath & "\Setup",1,1
   'oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Setup\RegisterSetupFiles.vbs",1,1
   oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Setup\SetupEventLog.vbs",1,1
   oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Setup\SetupSQLServer.vbs",1,1
   oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Setup\XpientExclusions.vbs",1,1
   oWSHShell.Run "cmd /c echo %date% %time%  2>&1 >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
   oWSHShell.Run "%systemroot%\Microsoft.NET\Framework\v2.0.50727\RegAsm.exe /u " & g_IrisPath & "\Bin\ReportLibrary.Win.Controls.Specialized.IrisBackOfficeRdl.dll >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
   oWSHShell.Run "cmd /c echo %date% %time%  2>&1 >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
   oWSHShell.Run "%systemroot%\Microsoft.NET\Framework\v2.0.50727\RegAsm.exe /codebase " & g_IrisPath & "\Bin\ReportLibrary.Win.Controls.Specialized.IrisBackOfficeRdl.dll >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
   oWSHShell.Run "cmd /c echo %date% %time%  2>&1 >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
   oWSHShell.Run "%systemroot%\Microsoft.NET\Framework\v2.0.50727\RegAsm.exe /u " & g_IrisPath & "\Bin\ScriptProgressDlg.dll >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
   oWSHShell.Run "cmd /c echo %date% %time%  2>&1 >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
   oWSHShell.Run "%systemroot%\Microsoft.NET\Framework\v2.0.50727\RegAsm.exe /codebase " & g_IrisPath & "\Bin\ScriptProgressDlg.dll >> " & g_IrisPath & "\Log\RegAsm.Log" ,0,1
End Sub

'----------------------------------------------------------------------------------------------------------------------------------
' This method will apply SQL and Access database changes to the system.
'----------------------------------------------------------------------------------------------------------------------------------

Sub ApplyDbChanges()

  Dim ApplyVM
  Dim SQLScript
  Dim ServerOnly
  Dim VersionMgtCmd

  On Error Resume Next

   If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Common\Data\POSCfg.mdb") Then
      oFSO.CopyFile g_ServerIrisPath & "\Reginfo\Common\Data\POSCfg.mdb", g_IrisPath & "\Data\Poscfg.mdb" , True
   End If

   If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Common\Data\Payroll.mdb") Then
      oFSO.CopyFile g_ServerIrisPath & "\Reginfo\Common\Data\Payroll.mdb", g_IrisPath & "\Data\Payroll.mdb" , True
   End If

   If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Common\Data\ItemCache.dat") Then
      oFSO.CopyFile g_ServerIrisPath & "\Reginfo\Common\Data\ItemCache.dat", g_IrisPath & "\Data\ItemCache.dat" , True
   End If

   If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Common\Data\MenuCache.dat") Then
      oFSO.CopyFile g_ServerIrisPath & "\Reginfo\Common\Data\MenuCache.dat", g_IrisPath & "\Data\MenuCache.dat" , True
   End If

   'SQLite

   If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Common\Data\POSCfg.sqlite") Then
      oFSO.CopyFile g_ServerIrisPath & "\Reginfo\Common\Data\POSCfg.sqlite", g_IrisPath & "\Data\Poscfg.sqlite" , True
   End If

   If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Common\Data\Payroll.sqlite") Then
      oFSO.CopyFile g_ServerIrisPath & "\Reginfo\Common\Data\Payroll.sqlite", g_IrisPath & "\Data\Payroll.sqlite" , True
   End If

   'Clean up BO only files'
   If oFSO.FileExists (g_IrisPath & "\data\BackOffice.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\BackOffice.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\BOC Message Translator.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\BOC Message Translator.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\BOC.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\BOC.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\CashControl.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\CashControl.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\delivery.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\delivery.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\DSR.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\DSR.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\Forecast.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\Forecast.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\GenericPolling.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\GenericPolling.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\Inventory.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\Inventory.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\InvExport.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\InvExport.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\IRISdbMgmt.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\IRISdbMgmt.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\IRISPol.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\IRISPol.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\JackPol.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\JackPol.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\POSCfgExport.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\POSCfgExport.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\POSTrans.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\POSTrans.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\Purchase.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\Purchase.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\Region.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\Region.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\Security.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\Security.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\Shell.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\Shell.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\SODB.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\SODB.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\Usda.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\Usda.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\WSR.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\WSR.mdb", True
   End If
   If oFSO.FileExists (g_IrisPath & "\data\xception.mdb") Then
     oFSO.DeleteFile g_IrisPath & "\data\xception.mdb", True
   End If

   UseSQLite = False
   oIniEditor.iniFilePath = g_IrisPath & "\ini\adoopt.ini"
   oIniEditor.section = "PROVIDER"
   oIniEditor.key = "POSCFG"
   If Instr(1, oIniEditor.value, "sqlite", 1) > 0 Then
     UseSQLite = True
   End If

  If not UseSQLite Then
    oIniEditor.IniFilePath = g_IrisPath & "\Ini\Iris.ini"
    oIniEditor.Section = "Database"
    oIniEditor.Key = "DBTYPE"
    oIniEditor.SetValue "ACCESS"

    oIniEditor.IniFilePath = g_IrisPath & "\Ini\Iris.ini"
    oIniEditor.Section = "Database"
    oIniEditor.Key = "poscfgDB"
    If LCase(oInieditor.Value) = LCase("C:\IRIS\data\POSCfgLink.mdb") Then
      oIniEditor.SetValue "C:\IRIS\data\POSCfg.mdb"
    End If

    ApplyVM = GetConfig("ScriptOptions","ApplySchemaChanges")
    ServerOnly = GetConfig("ScriptOptions","ServerOnly")
    VersionMgtCmd = GetConfig("ScriptOptions","VersionManagementCmd")

    If bCurrentBlobUpgradeable = True Then VersionMgtCmd = (VersionMgtCmd OR 65536) '-- Add PosPend
    If bCurrentBlobUpgradeable = True Then VersionMgtCmd = (VersionMgtCmd OR 2048)'-- Add PosLive

    If InStr(Lcase(ApplyVM),"yes") Then
      If InStr(Lcase(ServerOnly), "no") Then
        If LCASE(GetConfig("ScriptOptions","VersionManagement")) = "yes" or bCurrentBlobUpgradeable = True Then
          LogMsg "Applying Version Management to the POS databases"
          oWSHShell.CurrentDirectory = g_IrisPath & "\Bin"
          oWSHShell.Run g_IrisPath & "\Bin\Vermgmt.exe -Q -t" & VersionMgtCmd,1,1
        End If
      End If
    End If
  End If
End Sub


'----------------------------------------------------------------------------------------------------------------------------------
' Update the IRIS version information in the registry
'----------------------------------------------------------------------------------------------------------------------------------
Sub UpdateVersionInfo()
  Dim BuildID
  Dim strDate
  Dim strTime

  On Error Resume Next


  g_NewVersion = GetConfig("Options","UpgradeTo")
  BuildID = GetConfig("Options","Build")

  strDate = Date()
  strTime = Time()

  If g_NewVersion = "" Then
    ExitThisScript False, True
  Else
    oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Common\VERSION", g_NewVersion
    oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Common\Build", BuildID
    oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\VERSION", g_NewVersion
    oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\UPDATES\VERSIONS\" & g_NewVersion & "\", ""
    oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\UPDATES\VERSIONS\" & g_NewVersion & "\APPLIED", strDate & " " & strTime
    oWSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\UPDATES\VERSIONS\" & g_NewVersion & "\Build", BuildID
  End If
end Sub

'----------------------------------------------------------------------------------------------------------------------------------
' This method will roll back any file changes made to the system if a critical error occurs
'----------------------------------------------------------------------------------------------------------------------------------
Sub RollBackUpGrade(MethodName)

  Dim BackUpFolderSrc
  Dim BackUpFolders
  Dim BackUpFile
  Dim IrisFolder

  On Error Resume Next

  oWSHShell.Run "cmd /C Tasklist.exe /V >> " & g_IrisPath & "\Delta\LOG_Delta.log", 0, True

  Select Case MethodName

    Case "FileCopy"
        Set BackUpFolderSrc = oFSO.GetFolder(g_IrisPath)

        For each BackUpFolders in BackUpFolderSrc.SubFolders

          If Instr(BackUpFolders.Name,"_DeltaBackup") then

            For Each BackUpFile in BackUpFolders.Files

              IrisFolder = Left(BackUpFolders.Name,Instr(BackUpFolders.Name,"_")-1)
              oFSO.CopyFile BackUpFile,g_IrisPath & "\" & IrisFolder & "\" & BackUpFile.Name,TRUE

              If Err.number then
                If Err.Number <> 70 then
                  LogMsg "Catastrophic error restoring backed up file: " & BackUpFile.Name & " Error Description: " & Err.Description
                  Msgbox "CALL FOR HELP IMMEDIATELY"
                  ExitThisScript False, True
                End if
              end if

            Next

          end if

        Next

       oWSHShell.Run "Chkntfs /c " & Left(g_IrisPath,2),0,False

  End Select

  ExitThisScript False, True

end sub

'----------------------------------------------------------------------------------------------------------------------------------
' This function will copy and backup any files that are updated by this process
'----------------------------------------------------------------------------------------------------------------------------------
Function CopyFiles(DirName,BackUp)
   On Error Resume Next
   if g_IsServer = TRUE then
      FromDirName = g_IrisPath & "\" & DirName
   else
      FromDirName = g_ServerIrisPath & "\" & DirName
   End If
   LogMsg "FromDirName=" & FromDirName
   '-- If the staging folder does not exist locally (server-upgrade) or on the server (register-upgrade) then
   '-- if it is a register exit with failure since nothing has been done to the system. On the server upgrade you must
   '-- delete the staging folder or the register will attempt to perform the upgrade. It is not necessary to delete the
   '-- prelogon scripts since the absence of the staging folder on the server will cause the upgrade to exit here!

   If oFSO.FolderExists(FromDirName)=False then
      LogMsg "Delta directory not found. The script must exit"
      if g_IsServer = TRUE then
         RollBackUpGrade "BeforeCopyFiles"
      else
         ExitThisScript False, True
      End If
   End If

   Set FolderSource = oFSO.GetFolder(FromDirName)
   For each Folder in FolderSource.SubFolders
      CopyFiles = CopyFilesWorker(FromDirName,BackUp,Folder.Name)
   Next 
End Function

Function CopyFilesWorker(RootFolder,BackUp,CurrentPath)
   On Error Resume Next
   Dim FileCount
   Dim Count
   Dim IncAmt
   Dim FolderDest
   Dim FolderSource
   Dim Folder
   Dim File

   if CurrentPath = "" Then
      DestPath = g_IrisPath
      FromDirName = RootFolder
   Else
      DestPath = g_IrisPath & "\" & CurrentPath
      FromDirName = RootFolder & "\" & CurrentPath
   End If

   If oFSO.FolderExists(DestPath)= False then
      oFSO.CreateFolder DestPath
      LogMsg "Created Directory:" & DestPath
   End If

   Set FolderSource = oFSO.GetFolder(FromDirName)

   If BackUp = TRUE then
      If CurrentPath = "" Then
         BackupFolder = g_IrisPath & "\" & "_DeltaBackup"
      Else
         For each CP in Split(CurrentPath,"\")
          if CP <> "" Then
            if BackupFolder = "" Then
              BackupFolder = g_IrisPath & "\" & CP & "_DeltaBackup"
            Else
              BackupFolder = BackupFolder & "\" & CP
            End If
          End If
         Next
      End IF

      '-- If the folder exists then delete it to clear it of files, then create it.
      If oFSO.FolderExists(BackupFolder) Then
         DeleteFolderItems BackupFolder
      Else
         LogMsg "Created Directory:" & BackupFolder
         oFSO.CreateFolder BackupFolder
      End If
   End If

   For each SubFolder in FolderSource.SubFolders
      if CurrentPath = "" Then
         CopyFilesWorker RootFolder,BackUp,SubFolder.Name
      Else
         CopyFilesWorker RootFolder,BackUp,CurrentPath & "\" & SubFolder.Name
      End If
   Next
   For each File in FolderSource.Files
      If CurrentPath = "" Then
         FilePath = File.Name
      Else
         FilePath = CurrentPath & "\" & File.Name
      End If

      If BackUp = TRUE then
         '-- If the file about to be copied exists then back it up!
         if oFSO.FileExists(g_IrisPath & "\" & FilePath)= True then
            oFSO.CopyFile g_IrisPath & "\" & FilePath,BackupFolder & "\" & File.Name, True
            If Err.number then
               LogMsg "Error backing up file: " & FilePath & " Description: " & Err.Description
               Err.Clear
            End If
         End If
      End If

      If Right(LCase(FilePath),8) = ".default" Then
         DefaultFile = Left(FilePath,Len(FilePath)-8)
         If Not oFSO.FileExists(g_IrisPath & "\" & DefaultFile) Then
            LogMsg "CopyFiles: " & File & " --> " & g_IrisPath & "\" & DefaultFile
            oFSO.CopyFile File, g_IrisPath & "\" & DefaultFile, True
         End If
      Else
         LogMsg "CopyFiles: " & File & " --> " & g_IrisPath & "\" & FilePath
         oFSO.CopyFile File, g_IrisPath & "\" & FilePath, True
         if Err.number Then
            If Err.Number = 70 Then
               Err.Clear
               '-- Read only file found since it is backed up delete it
               oFSO.DeleteFile g_IrisPath & "\" & FilePath, True
               oFSO.CopyFile File, g_IrisPath & "\" & FilePath, True
               If Err.Number Then
                  LogMsg "Error deleting file: " & FilePath & " Error description: " & Err.Description
                  Err.Clear
                  RollBackUpGrade "FileCopy"
               End If
            Else
               LogMsg "Error copying file: " & FilePath & " Error description: " & Err.Description
               Err.Clear
               RollBackUpGrade "FileCopy"
            End If
         End If
      End If
   Next
End Function

Function DeleteFolderItems(Folder)
   On Error Resume Next
   If oFSO.FolderExists(Folder) Then
      Set FolderSource = oFSO.GetFolder(Folder)
      For each SubFolder in FolderSource.SubFolders
         DeleteFolderItems SubFolder
      Next
      For each File in FolderSource.Files
         oFSO.DeleteFile File,True
      Next
   End If
End Function

'----------------------------------------------------------------------------------------------------------------------------------
' This method will gather information about the local system.
'----------------------------------------------------------------------------------------------------------------------------------
Sub getLocalSystemInfo()

  Dim MachineType

  on error resume next

  g_IrisPath = oWSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")

  if err.number then
    LogMsg "Cannot locate the IRISDIR key in the registry."
  end if

  if g_IrisPath = "" then
    LogMsg "Cannot determine the IRIS installation directory from the registry. Using default c:\IRIS"
    g_IrisPath = "C:\IRIS"
  end if

  MachineType = oWSHShell.RegRead("HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\BOOrPOS")

  if InStr(Lcase(MachineType),"back") then
    LogMsg "Server upgrade"
    g_IsServer = TRUE
  else
    g_IsServer = FALSE
    LogMsg "Register upgrade"
  end if

  oIniEditor.IniFilePath = g_IrisPath & "\ini\Appini.ini"
  oIniEditor.Section = "Pos"
  oIniEditor.Key = "REGNUM"
  g_RegNum = oIniEditor.Value

  oIniEditor.IniFilePath = g_IrisPath & "\ini\dataserv.ini"
  oIniEditor.Section = "PARAMETERS"
  oIniEditor.Key = "Backup_Server"
  If Trim(oIniEditor.Value) = "1" Then
    g_IsRdsBackup = 1
  Else
    g_IsRdsBackup = 0
  End If
end sub


'----------------------------------------------------------------------------------------------------------------------------------
' This function will delete the prelogon script after it has executed. This prevents the update from executing multiple times
'----------------------------------------------------------------------------------------------------------------------------------

 Function DeletePreLogOnScript()

  ' --- Changed to delete Postlogon.vbs ---

  If oFSO.FileExists (g_IrisPath & "\Scripts\Postlogon.vbs") then
    oFSO.DeleteFile g_IrisPath & "\Scripts\Postlogon.vbs",TRUE
  end If

  If oFSO.FileExists (g_IrisPath & "\Scripts\Postlogon_SAV.vbs") then
    oFSO.MoveFile g_IrisPath & "\Scripts\PostLogOn_SAV.vbs", g_IrisPath & "\Scripts\PostLogOn.vbs"
  end If

  '--- Delete the servers copy of postlogon.vbs ---
  If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Reg" & CStr(g_RegNum) & "\Scripts\PostLogon.vbs") Then
    oFSO.DeleteFile g_ServerIrisPath & "\Reginfo\Reg" & CStr(g_RegNum) & "\Scripts\PostLogon.vbs",TRUE
  End if

  If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Reg" & CStr(g_RegNum) & "\Scripts\PostLogon_SAV.vbs") then
    oFSO.MoveFile g_ServerIrisPath & "\Reginfo\Reg" & CStr(g_RegNum) & "\Scripts\PostLogon_SAV", g_ServerIrisPath & "\Reginfo\Reg" & CStr(g_RegNum) & "\Scripts\PostLogon.vbs"
  end If

  If oFSO.FileExists (g_ServerIrisPath & "\Reginfo\Reg" & CStr(g_RegNum) & "\Scripts\StopServices.vbs") Then
    oFSO.DeleteFile g_ServerIrisPath & "\Reginfo\Reg" & CStr(g_RegNum) & "\Scripts\StopServices.vbs",TRUE
  End if


End Function

'----------------------------------------------------------------------------------------------------------------------------------
Function createCOMObjects()

  On Error Resume Next

  Set oFSO = CreateObject("Scripting.FileSystemObject")
    If err.number then
      LogMsg  "Error registering component Scripting.FileSystemObject! Error Message: " & err.Description
      ExitThisScript False, True
      end if

  Set oIniEditor = CreateObject("IRISIniEditor.IniEditor.1")
    If err.number then
      LogMsg  "Error registering component IRISIniEditor.IniEditor.1! Error Message: " & err.Description
      ExitThisScript False, True
      end if

    Set oWSHNetwork = WScript.CreateObject("WScript.Network")
      If err.number then
        LogMsg  "Error registering component WScript.Network! Error Message: " & err.Description
        ExitThisScript False, True
      end if

End Function


'----------------------------------------------------------------------------------------------------------------------------------
' This function will get the location of the IRIS directory on the server. Requires Regobj.dll on the local machine.
'----------------------------------------------------------------------------------------------------------------------------------
Function getServerSystemInfo()

  Dim TempStr
  Dim objRegistry
  Dim objReg
  Dim objRegKey

  On Error Resume Next

  oIniEditor.iniFilePath = g_IrisPath & "\ini\dataserv.ini"
  oIniEditor.section="PDS"
  oIniEditor.key="POSTRANS"

    TempStr = Mid(oIniEditor.Value, 3)
  TempStr = Mid(TempStr, 1, InStr(TempStr, "\") - 1)
  g_ServerName = TempStr
    
  Dim i
  i = InStr(lcase(oIniEditor.Value),"\data")
  g_ServerIrisPath = Mid(oIniEditor.Value ,1,i)

  Dim r
  r = InstrRev(g_ServerIrisPath, "\")
  g_ServerIrisPath = Mid(g_ServerIrisPath, 1, r-1)

  oIniEditor.key="PRIMARY_ADDRESS"
  g_ServerHostName = oIniEditor.Value


End Function

'----------------------------------------------------------------------------------------------------------------------------------
' SetBusinessDate
'
'----------------------------------------------------------------------------------------------------------------------------------
Function SetBusinessDate()

  Dim dbConn
  Dim rs
  Dim CurrentBusDate

  Set dbConn = CreateObject("ADODB.Connection")
  Set rs = CreateObject("ADODB.RecordSet")

  dbConn.Open IRIS_SQL_CONN_STRING
  rs.Open "SELECT BusinessDate FROM tblBusinessDate",dbConn

  If rs.EOF = False Then
    rs.MoveFirst
    CurrentBusDate = rs.Fields(0)
    rs.Close
  Else
    CurrentBusDate = ""
  End If

  dbConn.Close

  Set rs = Nothing

  dbConn.Open "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & g_IrisPath & "\Data\PosLive.mdb"
  If Err.Number Then
    LogMsg  Err.Description
    Exit function
  End If

  dbConn.Execute "DELETE * FROM tblBusinessDate"
  dbConn.Execute "INSERT INTO tblBusinessDate (BusinessDate) VALUES('" & CurrentBusDate & "')"
  dbConn.Close


  Set dbConn = nothing


End Function


Function ProcessMsgToServer(Key, Message)
  LogMsg Key & ":" & Message
End Function


'----------------------------------------------------------------------------------------------------------------------------------
' ExitThisScript
'
' INPUT:    bSuccess - TRUE the script completed successfully
'              FALSE the script did not complete
'----------------------------------------------------------------------------------------------------------------------------------
Sub ExitThisScript(bSuccess, bRetry)

   On Error Resume Next
   objExplorer.quit
   Set oIniEditor = CreateObject("IRISIniEditor.IniEditor.1")
   If bSuccess = True then
      DeletePreLogOnScript
   Else
      LogMsg "Update failed to apply."
      If bRetry = False Then 
         DeletePreLogOnScript
      Else
         sConfigValue = GetLocalConfig("Retry","Reg" & CStr(g_RegNum))
         If sConfigValue = "" Then
            Retry = 1
         Else
            Retry = CInt(sConfigValue) + 1
         End If
         If Retry = 3 Then 
            DeletePreLogOnScript
         Else
            SetLocalConfig "DeltaComplete", "Retry" & CStr(g_RegNum), CStr(Retry)
         End If
      End If
   End If

  LogMsg "End script"

  oFSO.CopyFile g_IrisPath & "\Delta\LOG_Delta.log", g_ServerIrisPath & "\Delta\LOG_Reg" & CStr(g_RegNum) & "_Delta.log", True

  If g_SuppressReboot = False Then
     If oWSHShell.Run(g_IrisPath & "\Delta\psShutdown.exe -f -o", 0, True) <> 0 Then
       oWSHShell.Run "wscript.exe //B " & g_IrisPath & "\Scripts\Shutdown.vbs /restart /force",0,True
     End If
    WScript.Sleep(30000)
   End If

  Wscript.Quit

End Sub

Sub PrintMsg (Message)
  On Error Resume Next
  LogMsg (Message)
  Dim sColor, sHTML
  sHTML = "<p align='center'><B><font face='Arial' color='yellow' size='10'>"
  sHTML = sHTML & "<BR><BR>Upgrading Software<BR>"
  sHTML = sHTML & "Please Wait!<BR>"
  sHTML = sHTML & "<p align='center'><font face='Arial' color='white' size='5'>" & Message & "</font></p>"
  objExplorer.Document.Body.InnerHTML = sHTML
End Sub

Sub LogMsg (Message)
  On Error Resume Next
  Dim oWSHShell, oFSO, sLogFile, oLog
  Set oWSHShell = CreateObject("WScript.Shell")
  Set oFSO=CreateObject("Scripting.FileSystemObject")
  If Not g_IrisPath = "" Then
    sLogFile = g_IrisPath & "\Delta\LOG_Delta.log"
    If Not oFSO.FolderExists(g_IrisPath & "\Delta") Then
      oFSO.CreateFolder g_IrisPath & "\Delta"
      If Err.Number Then
        sLogFile = ""
        Err.Clear
      End If
    End If
  End If
  If Not sLogFile = "" Then
    Set oLog = oFSO.OpenTextFile(sLogFile, 8, True)
    oLog.WriteLine CStr(Now()) & " " & WScript.ScriptName & ":" & Message
    oLog.Close
    If Err.Number Then
      sLogFile = ""
      Err.Clear
    End If
  End If
  If sLogFile = "" Then
    oWSHShell.LogEvent "Error", "IRIS(" & WScript.ScriptName & "):  " & Message
  End If
End Sub

Function SameVersion()
  Dim sVersion, sBuild,sInstalledVersion, sInstalledBuild
  SameVersion = false

  On Error Resume Next


  sVersion = GetConfig("Options","UpgradeTo")
  sBuild = GetConfig("Options","Build")

  sInstalledVersion = oWSHShell.RegRead("HKLM\SOFTWARE\Progressive Software\IRIS\Common\VERSION")
  sInstalledBuild = oWSHShell.RegRead("HKLM\SOFTWARE\Progressive Software\IRIS\Common\Build")

  If sVersion = sInstalledVersion And sBuild = sInstalledBuild Then
      SameVersion = true
  End if
End Function

Sub GetIrisFileVersion(sFile)
   Dim oErr: oErr = Err: Err.Clear
   If Not bDebugScript Then On Error Resume Next
   Dim fsName: fsName = "StripAlpha"
   Dim oFSO: Set oFSO=CreateObject("Scripting.FileSystemObject")
   Dim sVersion, sBuild, aVersion, iVersionIndex
   aVersion = Split(StripAlpha(oFSO.GetFileVersion(sFile)),".")
   For iVersionIndex = 0 To UBound(aVersion)
      Select Case iVersionIndex
         Case 0
            sVersion = aVersion(iVersionIndex)
         Case 1, 2
            sVersion = sVersion & "." & aVersion(iVersionIndex)
         Case 3
            If Len(aVersion(iVersionIndex)) > 3 Then 
               sVersion = sVersion & "." & Left(aVersion(iVersionIndex),Len(aVersion(iVersionIndex))-3)
               sBuild = Right(aVersion(iVersionIndex),3)
            Else
               sBuild = aVersion(iVersionIndex)
            End If
      End Select
   Next
   g_IrisVersion = sVersion
   g_IrisBuild = sBuild
 
   If Err.Number And IsObject(oLog) Then oLog.WriteLog "ERROR: " & fsName & ": (" & Err.Number & ") " & Err.Description
   Err = oErr
End Sub

Function StripAlpha(sString)
   Dim oErr: oErr = Err: Err.Clear
   If Not bDebugScript Then On Error Resume Next
   Dim fsName: fsName = "StripAlpha"
   Dim sNewString, iIndex
   For iIndex = 1 To Len(sString)
     Select Case Mid(sString,iIndex,1)
       Case "1","2","3","4","5","6","7","8","9","0","."
         sNewString = sNewString & Mid(sString,iIndex,1)
     End Select
   Next
   StripAlpha = sNewString
   If Err.Number And IsObject(oLog) Then oLog.WriteLog "ERROR: " & fsName & ": (" & Err.Number & ") " & Err.Description
   Err = oErr
End Function

Sub SetBlobOptions
   On Error Resume Next 
   Dim CurrentVersion, CurrentBuild, NewVersion, NewBuild
   bSupportBlobUpgrade = False
   bCurrentBlobUpgradeable = False
   'Read new version info from Config.ini
   NewVersion = GetConfig("Options", "UpgradeTo")
   NewBuild = GetConfig("Options", "Build")
   'Read current version info from Registry
   CurrentVersion = oWSHShell.RegRead("HKLM\SOFTWARE\Progressive Software\IRIS\Common\VERSION")
   CurrentBuild = oWSHShell.RegRead("HKLM\SOFTWARE\Progressive Software\IRIS\Common\Build")
   LogMsg  "CurrentVersion=" & CurrentVersion
   LogMsg  "CurrentBuild=" & CurrentBuild
   LogMsg  "NewVersion=" & NewVersion
   LogMsg  "NewBuild=" & NewBuild
   If (CompareVersion(NewVersion,3.8) = 1) or (CompareVersion(NewVersion,3.8) <> -1  and CompareVersion(NewBuild,695) <> -1) Then
      bSupportBlobUpgrade = True
      If (CompareVersion(CurrentVersion,3.8) = 1) or (CompareVersion(CurrentVersion,3.8) <> -1  and CompareVersion(CurrentBuild,695) <> -1) Then
         bCurrentBlobUpgradeable = True
      End If
   End If
   LogMsg  "bSupportBlobUpgrade=" & BoolToString(bSupportBlobUpgrade)
   LogMsg  "bCurrentBlobUpgradeable=" & BoolToString(bCurrentBlobUpgradeable)
End Sub

Function BoolToString(bValue)
   On Error Resume Next
   If bValue = True then 
      BoolToString = "True"
   Else
      BoolToString = "False"
   End If
End Function

'Function to compare version numbers
'This function will call itself for each part of the version number
'This function calls CompareSubVersion() to handle alphanumeric versions
Function CompareVersion(VersionA, VersionB)
	'VersionA = VersionB = 0, VersionA > VersionB = 1, VersionA < VersionB = -1
	Dim Result : Result = 0
	'Get the first part of the version and the remainder
	Dim VersionArrayA : VersionArrayA = Split(VersionA,".",2)
	Dim VersionArrayB : VersionArrayB = Split(VersionB,".",2)
  If UBound(VersionArrayA) = 0 Then ReDim Preserve VersionArrayA(1):VersionArrayA(1)="0"
  If UBound(VersionArrayB) = 0 Then ReDim Preserve VersionArrayB(1):VersionArrayB(1)="0"
	'Use CompareSubVersion to compare the first part
	Result = CompareSubVersion(VersionArrayA(0),VersionArrayB(0))
  If Result = 0 And  VersionArrayA(1) <> VersionArrayB(1) Then
   'If this part of the version is the same compare the next part
	 Result = CompareVersion(VersionArrayA(1), VersionArrayB(1))
	End If	
	'Return Result
	CompareVersion = Result
End Function

Function Decrypt(ByVal encryptedstring)
   On Error Resume Next
   Dim x, i, tmp
   encryptedstring = StrReverse( encryptedstring )
   For i = 1 To Len( encryptedstring )
      x = Mid( encryptedstring, i, 1 )
      tmp = tmp & Chr( Asc( x ) - 1 )
   Next
   Decrypt = tmp
End Function

'Function to compare part of a version number
'This function will call itself in oder to handle alphanumeric versions
Function CompareSubVersion(VersionA, VersionB)
  'VersionA = VersionB = 0, VersionA > VersionB = 1, VersionA < VersionB = -1
	Dim TypeA, TypeB, CompareA, CompareB
	'If the versions are the same return 0
	If VersionA = VersionB Then
		CompareSubVersion = 0
		Exit Function
	End If
	'Read the first alpha or numeric part of the version
	If IsNumeric(Left(VersionA,1)) Then NumerA = True
	If IsNumeric(Left(VersionB,1)) Then NumerB = True
	For IndexA = 1 To Len(VersionA)
	  If IsNumeric(Mid(VersionA,IndexA,1)) <> NumerA Then Exit For
	  CompareA = CompareA & Mid(VersionA,IndexA,1)
	Next
	For IndexB = 1 To Len(VersionB)
	  If IsNumeric(Mid(VersionB,IndexB,1)) <> NumerB Then Exit For
	  CompareB = CompareB & Mid(VersionB,IndexB,1)
	Next
	'If both parts are numeric do a numeric compare
	If NumerA = True And NumerB = True Then 
	  If CInt(CompareA) > CInt(CompareB) Then 
	  	Result = 1
	  ElseIf CInt(CompareA) < CInt(CompareB) Then
	    Result = -1
	  End If
	'Else we do a string compare
  Else
	  If CompareA > CompareB Then 
	  	Result = 1
	  ElseIf CompareA < CompareB Then
	  	Result = -1
	  End If
  End If
  'If this part is the same compare the next part
  If Result = 0 Then Result = CompareSubVersion(Mid(VersionA,IndexA), Mid(VersionB,IndexB))
  'Return Result
  CompareSubVersion = Result
End Function

Function RunasAdmin(sUser,sPass)
   On Error Resume Next
   Set oWSHShell = CreateObject("WScript.Shell")
   Set oWSHNetwork = WScript.CreateObject("WScript.Network")
   LogMsg "Current Domain: " & oWSHNetwork.UserDomain & " User: " & oWSHNetwork.UserName
   LogMsg "Required User: " & sUser
   If ((UCase(oWSHNetwork.UserName) <> UCase(sUser)) and (UCase(oWSHNetwork.UserDomain) & "\" & UCase(oWSHNetwork.UserName)  <> UCase(sUser))) Then
         sScriptCmd = Quote(Wscript.FullName) & " " & Quote(Wscript.ScriptFullName)
         For i = 0 to Wscript.Arguments.Count - 1
            If Trim(Wscript.Arguments.Item(i)) = "/psexec" then
               LogMsg "Error: Failed to detect user change."
               Exit Function
            End If
            sScriptCmd= sScriptCmd & " " & Wscript.Arguments.Item(i)
         Next
         objExplorer.quit
         LogMsg "Restarting Script as " & sUser
         If Not oFSO.FileExists( g_IrisPath & "\Delta\psexec.exe") Then
            oFSO.CopyFile g_ServerIrisPath & "\Delta\psexec.exe", g_IrisPath & "\Delta\psexec.exe",True
            If Err.Number <> 0 Then Err.Clear
         End If
         oWSHShell.Run g_IrisPath & "\Delta\psexec.exe -u " & sUser & " -p " & Decrypt(sPass) & " " & sScriptCmd & " /psexec", 0, True
         Wscript.Quit
   End If
End Function

Function Quote(sString)
   On Error Resume Next
   Quote = Chr(34) & sString & Chr(34)
End Function

Sub SetConfig(sSection, sKey, sValue)
   On Error Resume Next
   LogMsg "SetConfig(" & sSection & ", " & sKey & ", " & sValue & ")"
   If oFSO.FileExists (g_ServerIrisPath & "\Delta\Config.ini") then
     oIniEditor.IniFilePath = g_ServerIrisPath & "\Delta\Config.ini"
     oIniEditor.Section = sSection
     oIniEditor.Key = sKey
     oIniEditor.SetValue sValue
   Else
     LogMsg "Failed to Access " & g_ServerIrisPath & "\Delta\Config.ini"
   End If
End Sub

Function GetConfig(sSection, sKey)
   On Error Resume Next
   If oFSO.FileExists (g_ServerIrisPath & "\Delta\Config.ini") then
     oIniEditor.IniFilePath = g_ServerIrisPath & "\Delta\Config.ini"
     oIniEditor.Section = sSection
     oIniEditor.Key = sKey
     GetConfig = oIniEditor.Value
     LogMsg "GetConfig(" & sSection & ", " & sKey & ")=" & oIniEditor.Value
   Else
     LogMsg "Failed to Access " & g_ServerIrisPath & "\Delta\Config.ini"
     GetConfig = ""
   End If
End Function

Sub SetLocalConfig(sSection, sKey, sValue)
   On Error Resume Next
   LogMsg "SetConfig(" & sSection & ", " & sKey & ", " & sValue & ")"
   oIniEditor.IniFilePath = g_IrisPath & "\Delta\Config.ini"
   oIniEditor.Section = sSection
   oIniEditor.Key = sKey
   oIniEditor.SetValue sValue
End Sub

Function GetLocalConfig(sSection, sKey)
   On Error Resume Next
   oIniEditor.IniFilePath = g_IrisPath & "\Delta\Config.ini"
   oIniEditor.Section = sSection
   oIniEditor.Key = sKey
   GetConfig = oIniEditor.Value
   LogMsg "GetConfig(" & sSection & ", " & sKey & ")=" & oIniEditor.Value
End Function

Function ConfigValueExists(sSection, sKey)
   On Error Resume Next
   If oFSO.FileExists (g_ServerIrisPath & "\Delta\Config.ini") then
     oIniEditor.IniFilePath = g_ServerIrisPath & "\Delta\Config.ini"
     oIniEditor.Section = sSection
     oIniEditor.Key = sKey
     If oIniEditor.Value = "" Then
        ConfigValueExists = False
     Else
        ConfigValueExists = True
     End If
   Else
     LogMsg "Failed to Access " & g_ServerIrisPath & "\Delta\Config.ini"
     ConfigValueExists = False
   End If
End Function
