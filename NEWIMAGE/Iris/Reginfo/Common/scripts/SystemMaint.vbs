' ------------------------------------------------------------------------
'               Copyright (C) 2002 Progressive Software, Inc.
'
'                        System Maintenance Script
'
' Author:  Jim Davidson
' Date: 3/11/2002
'
' ********************************************************************************

' COM Objects
Dim WSHShell
Dim fso
Dim dbConn
Dim iniEditor

'Global Variables
Dim irisPath
Dim showProgress
Dim result
Dim PreScript
Dim PostScript
Dim MachineType
Dim IRISDB
Dim Purge
Dim Compact
Dim Logoff
Dim LogoffDelay
Dim PurgePath
Dim CompactPath
Dim PlugIn
Dim BusinessDate
Dim AppPath
Dim Addendum
Dim RunPolling
Dim PollingCmdLine
Dim PerformUpdate
Dim CmdLine
Dim PerformBackup
Dim EODBackupScript
Dim EODBackupFile
Dim RemoteBackup

'Constants
Const Success = 0
Const Error = 1
Const Warning = 2
Const Info = 4

'*************************************************************
' THIS IS THE MAIN SCRIPT LOGIC. 
'*************************************************************
   showProgress=0  'Make this 1 to display a messagebox before performing each step.
                   'Otherwise, 0.

   On Error Resume Next

  'Create the COM objects we will use in this script
   If showProgress Then MsgBox "Creating COM Objects"
   result=CreateCOMObjects()
   If result<>0 Then GiveUp()

  'Log beginning Of script
   If showProgress Then MsgBox "Log start of script"
   OutputLog Info,"Executing System Maintenance script."

  'Parse command line
   If showProgress Then MsgBox "Parsing command line"
   result=ParseCommandLine()
   If result<>0 Then GiveUp()
	
  'Load registry settings
   If showProgress Then MsgBox "Retrieving registry settings..."
   result=GetRegistrySettings()
   If result<>0 Then GiveUp()

  'Run Pre-SystemMaint script if it exists
   PreScript = irispath & "\Scripts\PreSystemMaint.vbs"
   If fso.FileExists(PreScript) Then
      OutputLog Info,"Launching Pre-SystemMaint script: " & PreScript
      WSHShell.Run PreScript,,true
   End If

  'Perform Polling if specified
   If showProgress Then MsgBox "Determining whether to execute polling."
   If MachineType = "Server" Then
      result=Polling()
      If result<>0 Then GiveUp()
   End If

  'Call purge script or set to run on next windows logon
   If showProgress Then MsgBox "Determining when to call purge"
   result=SetPurge()
   If result<>0 Then GiveUp()

  'Call Compact script or set to run on next windows logon
   If showProgress Then MsgBox "Determining when to call Compact.vbs"
   result=SetCompact()
   If result<>0 Then GiveUp()

  'Get Business Date
   If MachineType = "Server" Then
      If showProgress Then MsgBox "Determining Business Date"
      result=GetBusinessDate()
   End If

  'Run PayEOD plug-in on server only
   If MachineType = "Server" Then
      If showProgress Then MsgBox "Executing PayEOD plug-in"
      result=RunPayEOD()
      If result<>0 Then GiveUp()
   End If

  'Run EODFiscalYear plug-in on server only
   If MachineType = "Server" Then
      If showProgress Then MsgBox "Executing EODFiscalYear plug-in"
      result=RunEODFiscalYear()
      If result<>0 Then GiveUp()
   End If

  'Update item prices with effective future prices on server only
   If MachineType = "Server" Then
      If showProgress Then MsgBox "Updating item prices with effective future prices"
      result=UpdatePrice()
      If result<>0 Then GiveUp()
   End If       

  'Initiate Register Update if necessary
   If MachineType = "Server" Then
      If showProgress Then MsgBox "Determining whether to perform register update"
      result=UpdateRegisters()
      If result<>0 Then GiveUp()
   End If

  'Spawn EODBackup script if necessary
   If MachineType = "Server" Then
      If showProgress Then MsgBox "Determining whether to execute EODBackup script"
      result=EODBackup()
      If result<>0 Then GiveUp()
   End If

  'Set EODBackup flag in registry so logon script will initiate remote backup on next logon
   If MachineType = "Server" Then
      If showProgress Then MsgBox "Determining whether to set EODBackup flag"
      SetRemoteBackupFlag()
   End If

  'Run Post-SystemMaint script if it exists
   PostScript = irispath & "\Scripts\PostSystemMaint.vbs"
   If fso.FileExists(PostScript) Then
      OutputLog Info,"Launching Post-SystemMaint script: " & PostScript
      WSHShell.Run PostScript,,true
   End If

  'Set PostSystemMaint flag in registry so logon script will check for software upgrade on next logon
   WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\PostSystemMaint",1

  'Log Off of windows if specified
   If Logoff = 1 Then
      Addendum = ""
      If LogoffDelay <> 0 Then Addendum = " /Delay=" & LogoffDelay
      OutputLog Info,"Executing Logoff with Delay = " & LogoffDelay
      AppPath = irispath & "\Scripts\Shutdown.vbs /Logoff" & Addendum
      WSHShell.Run AppPath,,false
   End If
	
  'End the script with success
   OutputLog Success,"Finished executing System Maintenance script."
   ExitThisScript(0)

'*************************************************************
' THIS IS THE END OF THE MAIN SCRIPT LOGIC
'*************************************************************
'
'*************************************************************
' Create the COM Objects we will use in this script
'
Function CreateCOMObjects()
   CreateCOMObjects = 1	'Assume failure

   On Error Resume Next

   Set WSHShell = WScript.CreateObject("WScript.Shell")
   If Err.Number Then ExitThisScript(1)

   Set fso = CreateObject("Scripting.FileSystemObject")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating File System object (fso)." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Function
   End If

   Set dbConn = CreateObject("ADODB.Connection")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating ADO object (dbConn)." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Function
   End If

   Set iniEditor = CreateObject("IRISIniEditor.IniEditor.1")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating IRISIniEditor object.  Make sure that IRISIniEditor.ocx is registered." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Function
   End If

   CreateCOMObjects = 0	'Success
End Function
'*************************************************************
'*************************************************************
'Parse command line to determine if the script should stage the update to the register or apply the staged update
'
Function ParseCommandLine()
   ParseCommandLine=0    'Assume success

   On Error Resume Next

   Purge = 1  	      'Default to run purge script instead of waiting until next windows logon
   Compact = 0        'Default to not compact/repair database during script, but set to compact/repair on next windows logon
   Logoff = 0 	      'Default to not log off of windows at completion of script
   LogoffDelay = 0    'Default to no delay
   PlugIn = 0 	      'Default to not display message in plugin window
   RunPolling = 0     'Default to not execute polling package
   PerformUpdate = 0  'Default to not initiate register update process
   PerformBackup = 0  'Default to not spawn EODBackup process   
   RemoteBackup = 0   'Default to not set Remote Backup flag in registry

   Set objArgs = WScript.Arguments
   For I = 0 to objArgs.Count - 1
      If Instr(objArgs(I),"=") Then
         If LCase(Left(objArgs(I),Instr(1,objArgs(I),"=")-1)) = "/logoffdelay" Then LogoffDelay = Right(objArgs(I),Len(objArgs(I)) - Instr(1,objArgs(I),"="))
         If LCase(Left(objArgs(I),Instr(1,objArgs(I),"=")-1)) = "/pollingcmd" Then PollingCmdLine = Right(objArgs(I),Len(objArgs(I)) - Instr(1,objArgs(I),"="))
         If LCase(Left(objArgs(I),Instr(1,objArgs(I),"=")-1)) = "/backupfile" Then EODBackupFile = Right(objArgs(I),Len(objArgs(I)) - Instr(1,objArgs(I),"="))
      Else
         If LCase(objArgs(I)) = "/purgeonlogon" Then Purge = 0
         If LCase(objArgs(I)) = "/compact" Then Compact = 1
         If LCase(objArgs(I)) = "/logoff" Then Logoff = 1
         If LCase(objArgs(I)) = "/plugin" Then PlugIn = 1
         If LCase(objArgs(I)) = "/polling" Then RunPolling = 1
         If LCase(objArgs(I)) = "/updateregisters" Then PerformUpdate = 1
         If LCase(objArgs(I)) = "/eodbackup" Then PerformBackup = 1
         If LCase(objArgs(I)) = "/remotebackup" Then RemoteBackup = 1
      End If
   Next

   If LogOffDelay <> 0 Then Logoff = 1
   If PollingCmdLine <> "" Then RunPolling = 1
   If EODBackupFile <> "" Then PerformBackup = 1

End Function
'*************************************************************
'*************************************************************
'Retrieve registry settings
'
Function GetRegistrySettings()
   GetRegistrySettings=0 'Assume success
   On Error Resume Next
   irisPath=WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")
   If Err.Number Then
      OutputLog Error,"IRIS Path is missing from registry: HKEY_LOCAL_MACHINE\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR"
      GetRegistrySettings=1
      Err.Clear
   End If
   
   MachineType = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\BOOrPOS")
   If Instr(Lcase(MachineType),"pos") OR Instr(Lcase(MachineType),"register") Then
      MachineType = "Register"
   Else
      MachineType = "Server"
   End If

   IRISDB = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\DBConnectString")
   If IRISDB = "" Then
      OutputLog Error,"GetRegistrySettings(): DBConnectString is missing from registry: HKEY_LOCAL_MACHINE\SOFTWARE\Progressive Software\IRIS\Common\DBConnectString" & vbcrlf & "Using default connect string for IRIS database."
      IRISDB = "Provider=SQLOLEDB.1;Initial Catalog=IRIS;Data Source=(local);Integrated Security=sspi;"
   End If

End Function
'*************************************************************
'*************************************************************
'Get Business Date
'
Function GetBusinessDate()
   GetBusinessDate = 0   'Assume success
   
   Dim rsBusinessDate

   On Error Resume Next

   dbConn.Open IRISDB
   If Err.Number Then
      OutputLog Error,"GetBusinessDate(): Error opening IRIS database (" & IRISDB & "):" & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      BusinessDate = Date
      GetBusinessDate=1
      Exit Function
   End If

   Set rsBusinessDate = CreateObject ("ADODB.Recordset")
   If Err.Number Then
      OutputLog Error,"GetBusinessDate(): Error creating recordset object." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      BusinessDate = Date
      GetBusinessDate=1
      Exit Function
   End If

   rsBusinessDate.Open "SELECT * FROM tblBusinessDate",dbConn,1,2
   If Err.Number Then
      OutputLog Error,"GetBusinessDate(): Error retrieving business date from POSLive." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      BusinessDate = Date
      GetBusinessDate=1
      Exit Function
   End If

   If rsBusinessDate.EOF = 0 Then
      BusinessDate = rsBusinessDate.Fields("BusinessDate")
   Else
      BusinessDate = Date
   End If
   rsBusinessDate.close

   dbConn.close

End Function
'*************************************************************
'*************************************************************
'Launch Polling command if necessary
'
Function Polling()
   Polling = 0    'Assume success

   On Error Resume Next

   If fso.FileExists(irisPath & "\ini\SystemMaint.ini") Then
      iniEditor.iniFilePath = irisPath & "\ini\SystemMaint.ini"
      inieditor.section="Options"

      If RunPolling <> 1 Then
         inieditor.key="Polling"
         RunPolling=inieditor.value
      End If

      If PollingCmdLine = "" Then
         inieditor.key="PollingCmdLine"
         PollingCmdLine=inieditor.value
      End If

   End If

   If RunPolling <> 1 Then Exit Function

   If PollingCmdLine = "" Then PollingCmdLine = Chr(34) & "C:\Program Files\Progressive Software\psiExporter\bin\PSIExporter.exe" & Chr(34) & " /q"

   If Left(PollingCmdLine,1) = Chr(34) Then
      tmp=Right(PollingCmdLine,Len(PollingCmdLine)-1)
      tmp = Left(tmp,Instr(tmp,Chr(34))-1)
      WSHShell.CurrentDirectory = fso.GetParentFolderName(tmp)
   Else
      WSHShell.CurrentDirectory = fso.GetParentFolderName(PollingCmdLine)
   End If

   OutputLog Info,"Executing Polling Package: " & vbCrLf & PollingCmdLine
   Err.Clear
   WSHShell.Run PollingCmdLine,,true
   If Err.Number Then OutputLog Error,"Polling(): Error executing polling package (" & PollingCmdLine & ")." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description

End Function
'*************************************************************
'*************************************************************
'Launch Purge script or set to launch on next windows logon
'
Function SetPurge()

   SetPurge = 0   'Assume success

   On Error Resume Next

   PurgePath = irispath & "\Scripts\Purge.vbs"

   PlugInSwitch = ""
   If PlugIn = 1 Then PlugInSwitch = " /PlugIn"

   If Purge = 1 Then
      If Not fso.FileExists(PurgePath) Then
         OutputLog Error,"Unable to locate " & PurgePath & ".  Purge script cannot be executed."
         Exit Function
      Else
         Err.Clear
         WSHShell.Run PurgePath & PlugInSwitch,,true
         If Err.Number Then
            OutputLog Error,"SetPurge() - Error executing Purge command line: " & PurgePath & PlugInSwitch & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
            Err.Clear
         End If
      End If
   Else
      Err.Clear
      WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\Purge",1
      If Err.Number Then
         OutputLog Error,"SetPurge(): Error writing Purge flag to registry." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
      End If
   End If

End Function
'*************************************************************
'*************************************************************
'Launch Compact script or set to launch on next windows logon
'
Function SetCompact()

   SetCompact = 0   'Assume success

   On Error Resume Next

   CompactPath = irispath & "\Scripts\Compact.vbs"

   If Compact = 1 Then
      If Not fso.FileExists(CompactPath) Then
         OutputLog Error,"Unable to locate " & CompactPath & ".  Compact/Repair script cannot be executed."
         Exit Function
      Else
         Err.Clear
         WSHShell.Run CompactPath,,true
         If Err.Number Then
            OutputLog Error,"SetCompact() - Error executing Compact command line: " & CompactPath & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
            Err.Clear
         End If
      End If
   Else
      Err.Clear
      WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\Compact",1
      If Err.Number Then
         OutputLog Error,"SetCompact(): Error writing Compact flag to registry." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
      End If
   End If

End Function
'*************************************************************
'*************************************************************
'Launch PayEOD plug-in
'
Function RunPayEOD()

   RunPayEOD = 0   'Assume success

   On Error Resume Next

   AppPath = irispath & "\bin\PayEOD.exe"

   If Not fso.FileExists(AppPath) Then
      OutputLog Error,"Unable to locate " & AppPath & ".  PayEOD plug-in cannot be executed."
      Exit Function
   Else
      WSHShell.CurrentDirectory = irisPath & "\bin"
      WSHShell.Run AppPath,,true
      If Err.Number Then
         OutputLog Error,"Error executing " & AppPath & ":" & vbCrLf & Err.Source & ": " & Err.Description
         Err.Clear
      End If
   End If

End Function
'*************************************************************
'*************************************************************
'Launch EODFiscalYear plug-in
'
Function RunEODFiscalYear()

   RunEODFiscalYear = 0   'Assume success

   On Error Resume Next

   AppPath = irispath & "\bin\EODFiscalYear.exe"

   If Not fso.FileExists(AppPath) Then
      OutputLog Error,"Unable to locate " & AppPath & ".  EODFiscalYear plug-in cannot be executed."
      Exit Function
   Else
      WSHShell.CurrentDirectory = irisPath & "\bin"
      WSHShell.Run AppPath & " /date=" & BusinessDate & " /NoUI /PlugIn /q",,true
      If Err.Number Then
         OutputLog Error,"Error executing " & AppPath & ":" & vbCrLf & Err.Source & ": " & Err.Description
         Err.Clear
      End If
   End If

End Function
'*************************************************************
'*************************************************************
'Update item prices with effective future prices on server only
'
Function UpdatePrice()
   UpdatePrice = 0       'Assume success

   On Error Resume Next   

   dbConn.Open IRISDB
   If Err.Number Then
      OutputLog Error, "UpdatePrice(): Error opening IRIS Database.  Future price processing will not occur." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
   Else
      dbConn.sp_SetFuturePrice
      If Err.Number Then OutputLog Error, "Error executing sp_SetFuturePrice stored procedure." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
   End If

   dbConn.Close

End Function
'*************************************************************
'*************************************************************
'Launch Register Update process if necessary
'
Function UpdateRegisters()
   UpdateRegisters = 0    'Assume success

   On Error Resume Next

   If fso.FileExists(irisPath & "\ini\SystemMaint.ini") Then
      iniEditor.iniFilePath = irisPath & "\ini\SystemMaint.ini"
      inieditor.section="Options"

      If PerformUpdate <> 1 Then
         inieditor.key="UpdateRegisters"
         PerformUpdate=inieditor.value
      End If

      inieditor.key="UpdateRegistersCmdLine"
      CmdLine=inieditor.value   

   End If

   If PerformUpdate <> 1 Then Exit Function

   If CmdLine = "" Then CmdLine = irisPath & "\Update\InitiateRegisterUpdate.vbs"

   OutputLog Info,"Launching register update script (" & CmdLine & ")."
   WSHShell.Run CmdLine,,true
   If Err.Number Then OutputLog Error,"UpdateRegisters(): Error executing Register Update process (" & CmdLine & ")." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description

End Function
'*************************************************************
'*************************************************************
'Launch EODBackup
'
Function EODBackup()
   EODBackup = 0    'Assume success

   On Error Resume Next

   If fso.FileExists(irisPath & "\ini\SystemMaint.ini") Then
      iniEditor.iniFilePath = irisPath & "\ini\SystemMaint.ini"
      inieditor.section="LocalBackup"

      If PerformBackup <> 1 Then
         inieditor.key="LocalBackup"
         PerformBackup=inieditor.value
      End If

      If EODBackupFile = "" Then
         inieditor.key="LocalBackupFile"
         EODBackupFile=inieditor.value
      End If

   End If

   If PerformBackup <> 1 Then Exit Function

   EODBackupScript = irispath & "\Scripts\EODBackup.vbs"

   If EODBackupFile = "" Then EODBackupFile = irisPath & "\Ini\EODBackup.ini"

  'Get list of backup destinations
   If fso.FileExists(irisPath & "\ini\SystemMaint.ini") Then
      iniEditor.iniFilePath = irisPath & "\ini\SystemMaint.ini"
      inieditor.section="LocalBackup"

      s = "aaa"
      n = 1
      While s <> ""
         inieditor.key="Dest" & n
         s = inieditor.value
         If s <> "" Then
            If BackupList = "" Then
               BackupList = s
            Else
               BackupList = BackupList & "," & s
            End If
         End If
         n = n + 1
      wend        

   End If      

   Dim Params
   Params = " /file=" & EODBackupFile & " /NOUI /Q"
   If BackUpList <> "" Then Params = Params & " /backupdest=" & BackUpList

   OutputLog Info,"Launching EODBackup script (" & EODBackupScript & Params & ")."
   If Not fso.FileExists(EODBackupScript) Then
      OutputLog Error,"Unable to locate " & EODBackupScript & ".  EODBackup will not be performed."
   Else
       WSHShell.Run EODBackupScript & Params,,true
   End If

End Function
'*************************************************************
'*************************************************************
'Set EODBackup flag so logon script will initiate remote backup on next logon
'
Sub SetRemoteBackupFlag()
   On Error Resume Next

   If RemoteBackup <> 1 Then

      If fso.FileExists(irisPath & "\ini\SystemMaint.ini") Then
         iniEditor.iniFilePath = irisPath & "\ini\SystemMaint.ini"
         inieditor.section="RemoteBackup"

         inieditor.key="RemoteBackup"
         RemoteBackup=inieditor.value

      End If

   End If

   If RemoteBackup = 1 Then WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\EODBackup",1

End Sub
'*************************************************************
'*************************************************************
'Output specified text to log file 
'
Sub OutputLog(EventID,OutputText)
   On Error Resume Next
   WshShell.LogEvent EventID, "IRIS(" & WScript.ScriptName & "):  " & OutputText
End Sub
'*************************************************************
'*************************************************************
' Exit the script with an error
'
Sub GiveUp()
   On Error Resume Next
   OutputLog Error,"Terminating script due to error."
   ExitThisScript(1)
End Sub
'*************************************************************
'*************************************************************
' Exit the Script
'
Sub ExitThisScript(ExitCode)
   On Error Resume Next
   Set WSHShell = Nothing
   Set fso = Nothing
   Set dbConn = Nothing
   Set iniEditor = Nothing
   WScript.Quit(ExitCode)  ' end of script
End Sub
'' SIG '' Begin signature block
'' SIG '' MIIQ7QYJKoZIhvcNAQcCoIIQ3jCCENoCAQExDjAMBggq
'' SIG '' hkiG9w0CBQUAMGYGCisGAQQBgjcCAQSgWDBWMDIGCisG
'' SIG '' AQQBgjcCAR4wJAIBAQQQTvApFpkntU2P5azhDxfrqwIB
'' SIG '' AAIBAAIBAAIBAAIBADAgMAwGCCqGSIb3DQIFBQAEEMnF
'' SIG '' LhJze5/Z8fz7JR+JGiCgggyQMIIDejCCAmKgAwIBAgIQ
'' SIG '' OCXX+vhhr570kOcmtdZa1TANBgkqhkiG9w0BAQUFADBT
'' SIG '' MQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNpZ24s
'' SIG '' IEluYy4xKzApBgNVBAMTIlZlcmlTaWduIFRpbWUgU3Rh
'' SIG '' bXBpbmcgU2VydmljZXMgQ0EwHhcNMDcwNjE1MDAwMDAw
'' SIG '' WhcNMTIwNjE0MjM1OTU5WjBcMQswCQYDVQQGEwJVUzEX
'' SIG '' MBUGA1UEChMOVmVyaVNpZ24sIEluYy4xNDAyBgNVBAMT
'' SIG '' K1ZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMg
'' SIG '' U2lnbmVyIC0gRzIwgZ8wDQYJKoZIhvcNAQEBBQADgY0A
'' SIG '' MIGJAoGBAMS18lIVvIiGYCkWSlsvS5Frh5HzNVRYNerR
'' SIG '' Nl5iTVJRNHHCe2YdicjdKsRqCvY32Zh0kfaSrrC1dpbx
'' SIG '' qUpjRUcuawuSTksrjO5YSovUB+QaLPiCqljZzULzLcB1
'' SIG '' 3o2rx44dmmxMCJUe3tvvZ+FywknCnmA84eK+FqNjeGkU
'' SIG '' e60tAgMBAAGjgcQwgcEwNAYIKwYBBQUHAQEEKDAmMCQG
'' SIG '' CCsGAQUFBzABhhhodHRwOi8vb2NzcC52ZXJpc2lnbi5j
'' SIG '' b20wDAYDVR0TAQH/BAIwADAzBgNVHR8ELDAqMCigJqAk
'' SIG '' hiJodHRwOi8vY3JsLnZlcmlzaWduLmNvbS90c3MtY2Eu
'' SIG '' Y3JsMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMA4GA1Ud
'' SIG '' DwEB/wQEAwIGwDAeBgNVHREEFzAVpBMwETEPMA0GA1UE
'' SIG '' AxMGVFNBMS0yMA0GCSqGSIb3DQEBBQUAA4IBAQBQxUvI
'' SIG '' JIDf5A0kwt4asaECoaaCLQyDFYE3CoIOLLBaF2G12AX+
'' SIG '' iNvxkZGzVhpApuuSvjg5sHU2dDqYT+Q3upmJypVCHbC5
'' SIG '' x6CNV+D61WQEQjVOAdEzohfITaonx/LhhkwCOE2DeMb8
'' SIG '' U+Dr4AaH3aSWnl4MmOKlvr+ChcNg4d+tKNjHpUtk2scb
'' SIG '' W72sOQjVOCKhM4sviprrvAchP0RBCQe1ZRwkvEjTRIDr
'' SIG '' oc/JArQUz1THFqOAXPl5Pl1yfYgXnixDospTzn099io6
'' SIG '' uE+UAKVtCoNd+V5T9BizVw9ww/v1rZWgDhfexBaAYMkP
'' SIG '' K26GBPHr9Hgn0QXF7jRbXrlJMvIzMIIDxDCCAy2gAwIB
'' SIG '' AgIQR78Zld+NUkZD99ttSA0xpDANBgkqhkiG9w0BAQUF
'' SIG '' ADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rl
'' SIG '' cm4gQ2FwZTEUMBIGA1UEBxMLRHVyYmFudmlsbGUxDzAN
'' SIG '' BgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhhd3RlIENl
'' SIG '' cnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1l
'' SIG '' c3RhbXBpbmcgQ0EwHhcNMDMxMjA0MDAwMDAwWhcNMTMx
'' SIG '' MjAzMjM1OTU5WjBTMQswCQYDVQQGEwJVUzEXMBUGA1UE
'' SIG '' ChMOVmVyaVNpZ24sIEluYy4xKzApBgNVBAMTIlZlcmlT
'' SIG '' aWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EwggEi
'' SIG '' MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpyrKk
'' SIG '' zM0grwp9iayHdfC0TvHfwQ+/Z2G9o2Qc2rv5yjOrhDCJ
'' SIG '' WH6M22vdNp4Pv9HsePJ3pn5vPL+Trw26aPRslMq9Ui2r
'' SIG '' SD31ttVdXxsCn/ovax6k96OaphrIAuF/TFLjDmDsQBx+
'' SIG '' uQ3eP8e034e9X3pqMS4DmYETqEcgzjFzDVctzXg0M5US
'' SIG '' mRK53mgvqubjwoqMKsOLIYdmvYNYV291vzyqJoddyhAV
'' SIG '' PJ+E6lTBCm7E/sVK3bkHEZcifNs+J9EeeOyfMcnx5iIZ
'' SIG '' 28SzR0OaGl+gHpDkXvXufPF9q2IBj/VNC97QIlaolc2u
'' SIG '' iHau7roN8+RN2aD7aKCuFDuzh8G7AgMBAAGjgdswgdgw
'' SIG '' NAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhhodHRw
'' SIG '' Oi8vb2NzcC52ZXJpc2lnbi5jb20wEgYDVR0TAQH/BAgw
'' SIG '' BgEB/wIBADBBBgNVHR8EOjA4MDagNKAyhjBodHRwOi8v
'' SIG '' Y3JsLnZlcmlzaWduLmNvbS9UaGF3dGVUaW1lc3RhbXBp
'' SIG '' bmdDQS5jcmwwEwYDVR0lBAwwCgYIKwYBBQUHAwgwDgYD
'' SIG '' VR0PAQH/BAQDAgEGMCQGA1UdEQQdMBukGTAXMRUwEwYD
'' SIG '' VQQDEwxUU0EyMDQ4LTEtNTMwDQYJKoZIhvcNAQEFBQAD
'' SIG '' gYEASmv56ljCRBwxiXmZK5a/gqwB1hxMzbCKWG7fCCmj
'' SIG '' XsjKkxPnBFIN70cnLwA4sOTJk06a1CJiFfc/NyFPcDGA
'' SIG '' 8Ys4h7Po6JcA/s9Vlk4k0qknTnqut2FB8yrO58nZXt27
'' SIG '' K4U+tZ212eFX/760xX71zwye8Jf+K9M7UhsbOCf3P0ow
'' SIG '' ggVGMIIELqADAgECAhEAsHgJfsXSrQMA7fgpGia4sjAN
'' SIG '' BgkqhkiG9w0BAQUFADCBlTELMAkGA1UEBhMCVVMxCzAJ
'' SIG '' BgNVBAgTAlVUMRcwFQYDVQQHEw5TYWx0IExha2UgQ2l0
'' SIG '' eTEeMBwGA1UEChMVVGhlIFVTRVJUUlVTVCBOZXR3b3Jr
'' SIG '' MSEwHwYDVQQLExhodHRwOi8vd3d3LnVzZXJ0cnVzdC5j
'' SIG '' b20xHTAbBgNVBAMTFFVUTi1VU0VSRmlyc3QtT2JqZWN0
'' SIG '' MB4XDTA5MDUxNDAwMDAwMFoXDTEyMDUxMzIzNTk1OVow
'' SIG '' gboxCzAJBgNVBAYTAlVTMQ4wDAYDVQQRDAUyODIyNjEL
'' SIG '' MAkGA1UECAwCTkMxEjAQBgNVBAcMCUNoYXJsb3R0ZTEu
'' SIG '' MCwGA1UECQwlMTE1MjUgQ2FybWVsIENvbW1vbnMgQmx2
'' SIG '' ZC4sIFN1aXRlIDEwMDEZMBcGA1UECgwQWFBJRU5UIFNv
'' SIG '' bHV0aW9uczEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGTAX
'' SIG '' BgNVBAMMEFhQSUVOVCBTb2x1dGlvbnMwggEiMA0GCSqG
'' SIG '' SIb3DQEBAQUAA4IBDwAwggEKAoIBAQC35Pl7Op1x200/
'' SIG '' nJxncnnhKXmPQw8T3KOIYnZqZ4m3lb+cvnF+d7SDy9jD
'' SIG '' dxbdM39WLyXWOpskG+LIViFlBV1KHTEsHgGkqwQpx5UT
'' SIG '' 5HVLMtNm0NYCWznFtYfVIqJs+tqz5lSQ2MUV4rSnstxm
'' SIG '' 0WRw4X9dneXe+zIrJ1W96UCw4ZLifsPbeqVYGpklgUmF
'' SIG '' xmFfwuPX/4qX0K6CXoqMRBZxg48qny2rv2vq+s0jscch
'' SIG '' sAyeZxjmWVUpxmYrZtyXCSNfVWd3UZsyZyDdl5YurfZx
'' SIG '' 28G1vjHr8WUuys8OX/FMgaRos8IUfDgdyP2LftB42HTh
'' SIG '' F/snyMik7PaMdv/wEAMtAgMBAAGjggFoMIIBZDAfBgNV
'' SIG '' HSMEGDAWgBTa7WR0FJwUPKvdmam9WyhNizzJ2DAdBgNV
'' SIG '' HQ4EFgQUeH7WhOp5HeJU1lLMlQxJBkArhykwDgYDVR0P
'' SIG '' AQH/BAQDAgeAMAwGA1UdEwEB/wQCMAAwEwYDVR0lBAww
'' SIG '' CgYIKwYBBQUHAwMwEQYJYIZIAYb4QgEBBAQDAgQQMEYG
'' SIG '' A1UdIAQ/MD0wOwYMKwYBBAGyMQECAQMCMCswKQYIKwYB
'' SIG '' BQUHAgEWHWh0dHBzOi8vc2VjdXJlLmNvbW9kby5uZXQv
'' SIG '' Q1BTMEIGA1UdHwQ7MDkwN6A1oDOGMWh0dHA6Ly9jcmwu
'' SIG '' dXNlcnRydXN0LmNvbS9VVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dC5jcmwwNAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzAB
'' SIG '' hhhodHRwOi8vb2NzcC5jb21vZG9jYS5jb20wGgYDVR0R
'' SIG '' BBMwEYEPaW5mb0B4cGllbnQuY29tMA0GCSqGSIb3DQEB
'' SIG '' BQUAA4IBAQBvSaL3UXO313UuGSpKCtggrg83wHa4XmlM
'' SIG '' 4LaUpCekcEBSFvVnBoxuGGR12GLgBkkLOVgPUnTsoH8G
'' SIG '' qvwcsTa11kQAV8XZ23K/Bk+c6MV3VeHa03V5kdf+2qKY
'' SIG '' VVadxQdrry8Dw2nvbuNZvBA1nMhB+3BQfSo2nTqIhahp
'' SIG '' MyY6FPXL2bemF/JWVAflFwyrdaInUueimSfBPu8ubQX1
'' SIG '' vgBOANoE33nQ8kXS1IhRsuB4xAjE9N4ZLwdr4rGkgstO
'' SIG '' xAVseaEUePSt+yWGMsyD9C/2XX4aG5Yp6wemvOrMF59G
'' SIG '' oJ7Uij5WrvxUJ4JNMuX22ywuQ6zZC1sCbVo1kYAagm9q
'' SIG '' MYIDxzCCA8MCAQEwgaswgZUxCzAJBgNVBAYTAlVTMQsw
'' SIG '' CQYDVQQIEwJVVDEXMBUGA1UEBxMOU2FsdCBMYWtlIENp
'' SIG '' dHkxHjAcBgNVBAoTFVRoZSBVU0VSVFJVU1QgTmV0d29y
'' SIG '' azEhMB8GA1UECxMYaHR0cDovL3d3dy51c2VydHJ1c3Qu
'' SIG '' Y29tMR0wGwYDVQQDExRVVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dAIRALB4CX7F0q0DAO34KRomuLIwDAYIKoZIhvcNAgUF
'' SIG '' AKBsMBAGCisGAQQBgjcCAQwxAjAAMBkGCSqGSIb3DQEJ
'' SIG '' AzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAM
'' SIG '' BgorBgEEAYI3AgEVMB8GCSqGSIb3DQEJBDESBBDVckDL
'' SIG '' +NKSdvNKKK5BSftkMA0GCSqGSIb3DQEBAQUABIIBAJCO
'' SIG '' qPuyDVu0xBWWI4h6/lpggavw3xFQYw5m6/pwiw8oGTyQ
'' SIG '' y/oMRyGo/uoQVSvx4m6P9DauexS/LlL5Xzo7Qb5c9/dR
'' SIG '' 3GpiCirLmOhHvOMzvSf2OcDoBuX37F8ZV6v7+RDgzknV
'' SIG '' hh1PP2EppyJivM3clBIU9kSEmyAW+VtpBODrzjF2tTFB
'' SIG '' WuKTfQKMQbkKx66lt47HxjjQvmHOhcfkdenEnXayvzs9
'' SIG '' 4LcrgsgyUAMRGu5LWm0N3isLrrZ+PUKyyk2aStwHZxaW
'' SIG '' L/nAUrQLR5ezRquTmPp/8CP833MbH5KGQzOwqQc8aZ2F
'' SIG '' udsXxvRgJzkf8G1YcrBlEbcPCnYXHeahggF/MIIBewYJ
'' SIG '' KoZIhvcNAQkGMYIBbDCCAWgCAQEwZzBTMQswCQYDVQQG
'' SIG '' EwJVUzEXMBUGA1UEChMOVmVyaVNpZ24sIEluYy4xKzAp
'' SIG '' BgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2Vy
'' SIG '' dmljZXMgQ0ECEDgl1/r4Ya+e9JDnJrXWWtUwCQYFKw4D
'' SIG '' AhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEw
'' SIG '' HAYJKoZIhvcNAQkFMQ8XDTExMDQwNDIzNTE1M1owIwYJ
'' SIG '' KoZIhvcNAQkEMRYEFG7FQfCOIaM/ezj+tN9fNWUCuK54
'' SIG '' MA0GCSqGSIb3DQEBAQUABIGADS2GcpAQHYPT+U2gSTHJ
'' SIG '' RR52JBFy4Ll1OjAppr09Iq+evl7VgGpj1W1ft5Dr2pen
'' SIG '' 0WjYzErIbFZdl8QtZqHhFk7u4MMjwHS9CkQfD8tDBE2g
'' SIG '' XuUP2e1aa+WN93rwSsZzLcjFww+nQJjKyfo8aGquiJpP
'' SIG '' gC0gNx09eqMH0uiDUiA=
'' SIG '' End signature block
