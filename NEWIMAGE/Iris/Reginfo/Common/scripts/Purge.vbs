' ------------------------------------------------------------------------
'               Copyright (C) 2004-2009 xpient Solutions, LLC
'
'                            Purge Script
'
' Author:  Jim Davidson
' Date: 3/11/2002
' Last Updated: 11/12/2009
'
' ************************************************************************
'##### Start check for x64 System #####
'This will check for a x64 System and restart the script using the 32bit version if needed
'xsCheckArchitecture v1.0
Sub xsCheckArchitecture
   On Error Resume Next
   If Not IsObject(WScript) Then Exit Sub
   Dim oWshShell, oWshProcEnv, oExec, Argument, sCmd, iResult
   Set oWshShell =  CreateObject("WScript.Shell")
   Set oWshProcEnv = oWshShell.Environment("Process")
   If LCase(oWshProcEnv("PROCESSOR_ARCHITECTURE")) <> "x86" and Instr(1, WScript.FullName, "SysWOW64", 1) = 0 then
      sCmd = Chr(34) & oWshProcEnv("SystemRoot") & "\SysWOW64\" &  Mid(WScript.FullName, InstrRev(WScript.FullName, "\") + 1) & Chr(34)
      sCmd = sCmd & " //NoLogo " & Chr(34) & WScript.ScriptFullName & Chr(34)
      For Each Argument in WScript.Arguments
         Argument = Split(Argument,"=",2)
         sCmd = sCmd & " " & Chr(34) & Argument(0) & Chr(34)
         If UBound(Argument) = 1 Then sCmd = sCmd & "=" & Chr(34) & Argument(1) & Chr(34)
      Next
      Set oExec = oWshShell.Exec(sCmd)
      Do While oExec.Status = 0
         Do While Not oExec.StdOut.AtEndOfStream
            WScript.StdOut.Write oExec.StdOut.Read(1)
         Loop
      Loop
      Do While Not oExec.StdErr.AtEndOfStream
         WScript.StdErr.Write oExec.StdErr.Read(1)
      Loop
      If Not oExec.ExitCode = 0  then WScript.Quit oExec.ExitCode
      WScript.quit
   End If
End Sub
xsCheckArchitecture
'##### End check for x64 System #####

' COM Objects
Dim WSHShell
Dim iniEditor
Dim fso
Dim dbConn
Dim oPB

'Global Variables
Dim irisPath
Dim Debug
Dim result
Dim PlugIn
Dim MessageScreen
Dim CloseMsgScreen
Dim PreScript
Dim PostScript
Dim BusinessDate
Dim MachineType
Dim POSLiveDB
Dim KitchenDB
Dim POSPendDB
Dim CCADB
Dim IRISDB
Dim TransDays
Dim ArchivedTransDays
Dim KitchenDays
Dim ForecastDays
Dim ForecastHistDays
Dim ShiftDays
Dim PurchaseDays
Dim InvDetailDays
Dim InvLiveDays
Dim InvHistDays
Dim ClockRecordDays
Dim MsgQDeletedDays
Dim MsgQReadDays
Dim MsgQUnreadDays
Dim LogFileDays
Dim SystemDeletedItemDays
Dim EDMAuditDays
Dim NewZipFile
Dim MinDiskSpace
Dim MaxLogFileSize
Dim ClearData
Dim sCaption
Dim iPctComp, iMsg
Dim sEvent
Dim PercentComplete
Dim UseProgressBar
Dim CameraSaveLocationPath
Dim CameraSaveLocationFileCount
Dim BackupLogstoServer
Dim ClearEventLogs
Dim KeepUnpaidTips

'Constants
Const Success = 0
Const Error = 1
Const Warning = 2
Const Info = 4
Const sUnLoadEvent = "OnUnLoad"  ' event flag
Const sCancelEvent = "Cancel"
Const sInternalErr = "Internal"

'*************************************************************
' THIS IS THE MAIN SCRIPT LOGIC.
'
' We exit if any step fails, with a status of 1.  If everything
' succeeds, our exit status is 0.
'*************************************************************
   Debug=0
   UseProgressBar=1

       'Create the COM objects we will use in this script
        If Debug Then MsgBox "Creating COM Objects"
        result=createCOMObjects()
        If result<>0 Then ExitThisScript()

        'Log beginning Of script
        If Debug Then MsgBox "Log start of script"
        OutputLog Info,"Executing Purge script."

        'Parse command line
        If Debug Then MsgBox "Parsing command line"
        result=ParseCommandLine()
        If result<>0 Then GiveUp()

        'Retrieve registry settings
        If Debug Then MsgBox "Determining IRIS path and machine type"
        result=GetRegistrySettings()
        If result<>0 Then GiveUp()

        'Determine database connect strings
        If Debug Then MsgBox "Determining database connect strings"
        result=GetDBStrings()
        If result<>0 Then GiveUp()

        'Display purging UI if plug-in option specified
        If Debug Then MsgBox "Executing EODMessage to display Purge message."
        If PlugIn = 1 Then result=OpenMessageScreen()

        'Display Progress Bar
        If UseProgressBar = 1 Then
           If Debug Then MsgBox "Initializing user interface"
           result = InitializeUI()
           If result<>0 then GiveUp()
        End If

        'Run Pre-script if it exists
        PreScript = irispath & "\Scripts\Pre" & WScript.ScriptName
        If fso.FileExists(PreScript) Then
           OutputLog Info,"Launching Pre-script: " & PreScript
           WSHShell.Run PreScript,,true
        End If

        'Get Days To Keep values
        If Debug Then MsgBox "Determining number of days of data to retain"
        result=GetDaysToKeep()
        If result<>0 Then GiveUp()

        'Get Business Date
        If Debug Then MsgBox "Determining Business Date"
        result=GetBusinessDate()
        If result<>0 Then GiveUp()

   'Set BusinessDate in PosLive Template
        PosLiveTemplate = irispath & "\Data\Template\PosLive.mdb"
        If fso.FileExists(PosLiveTemplate) Then
           OutputLog Info,"Setting BusinessDate: " & PosLiveTemplate
           SetBusinessDate "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & PosLiveTemplate, BusinessDate
        End If

   'Purge Databases
        If Debug Then MsgBox "Purging Databases"
        If UseProgressBar = 1 Then
           oPB.PctComplete = 10
           oPB.SetLine2 = "Purging Databases"
        End If
        result=PurgeData()
        If result<>0 Then GiveUp()

   'Backup Log Files
        If Debug Then MsgBox "Backing up log files"
        If UseProgressBar = 1 Then
           oPB.PctComplete = 80
           oPB.SetLine2 = "Purging Log Files"
        End If
        result=LogFileMaint()
        If result<>0 Then GiveUp()

   'Purge Image Files
        If Debug Then MsgBox "Purging Image Files"
        If UseProgressBar = 1 Then
           oPB.PctComplete = 90
           oPB.SetLine2 = "Purging Image Files"
        End If
        result=PurgeImage()
        If result<>0 Then GiveUp()

   'Run Post-script if it exists
        PostScript = irispath & "\Scripts\Post" & WScript.ScriptName
        If fso.FileExists(PostScript) Then
           OutputLog Info,"Launching Post-script: " & PostScript
           WSHShell.Run PostScript,,true
        End If

   'End the script with success
        WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\Purge",0
        OutputLog Success, "Purge script finished executing."
        ExitThisScript()

'*************************************************************
' THIS IS THE END OF THE MAIN SCRIPT LOGIC
'*************************************************************
'
'*************************************************************
' Create the COM Objects we will use in this script
'
Function CreateCOMObjects()
   CreateCOMObjects=1   'Assume failure

   On Error Resume Next

   Set WSHShell = WScript.CreateObject("WScript.Shell")
   If Err.Number Then
      If Debug = 1 Then MsgBox "CreateCOMObjects(): Error creating WScript.Shell object.",16,"ERROR"
      Err.Clear
      Exit Function
   End If

   Set iniEditor = CreateObject("IRISIniEditor.IniEditor.1")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating IRISIniEditor object.  Make sure that IRISIniEditor.ocx is registered." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
      Exit Function
   End If

   Set fso = CreateObject("Scripting.FileSystemObject")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating File System Object (fso)." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Err.Clear
      Exit Function
   End If

   Set dbConn = CreateObject("ADODB.Connection")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating ADO object (dbConn)." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Err.Clear
      Exit Function
   End If

   Set oPB = WScript.CreateObject("ProgressBar.Scriptlet", "oPB_")
   If Err.Number Then
      OutputLog Warning,"CreateCOMObjects(): Error creating Progress Bar object (oPB).  Progress Bar will not be displayed." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      UseProgressBar = 0
      Err.Clear
   End If

   CreateCOMObjects = 0 'Success
End Function
'*************************************************************
'*************************************************************
'Parse command line
'
Function ParseCommandLine()
   ParseCommandLine=0    'Assume success

   On Error Resume Next

   PlugIn=0
   ClearData=0

   Set objArgs = WScript.Arguments
   For I = 0 to objArgs.Count - 1
      If Instr(objArgs(I),"=") Then
      Else
         If LCase(objArgs(I)) = "/plugin" Then PlugIn = 1
         If LCase(objArgs(I)) = "/clearall" Then ClearData = 1
      End If
   Next

   If PlugIn = 1 Then UseProgressBar = 0
   If ClearData <> 1 Then UseProgressBar = 0

End Function
'*************************************************************
'*************************************************************
'Get the IRIS directory and machine type
'
Function GetRegistrySettings()
   GetRegistrySettings=0 'Assume success
   On Error Resume Next
   irisPath=WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")
   If Err.Number Then
      OutputLog Error,"IRIS Path is missing from registry: HKEY_LOCAL_MACHINE\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR"
      GetRegistrySettings=1
      Err.Clear
   End If

   MachineType = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\BOOrPOS")
   If Instr(Lcase(MachineType),"pos") OR Instr(Lcase(MachineType),"register") Then
      MachineType = "Register"
   Else
      MachineType = "Server"
   End If

   If MachineType = "Register" Then UseProgressBar = 0

End Function
'*************************************************************
'*************************************************************
'Determine database connect strings
'
Function GetDBStrings()
   GetDBStrings = 0   'Assume success

   On Error Resume Next

  'IRIS database
   IRISDB = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\DBConnectString")
   If IRISDB = "" Then
      OutputLog Error,"Unable to retrieve connect string from registry.  Using default IRIS connect string."
      IRISDB = "Provider=SQLOLEDB.1;Initial Catalog=IRIS;Data Source=(local);Integrated Security=sspi;"
   End If

  'Get POS database connect strings
   iniEditor.iniFilePath = irisPath & "\ini\adoopt.ini"
   inieditor.section="Provider"

  'POSLive database
   POSLiveDB = ""
   inieditor.key = "POSLive"
   If inieditor.value <> "" Then POSLiveDB = inieditor.value

   If POSLiveDB = "" Then
      inieditor.key = "POS"
      If inieditor.value <> "" Then POSLiveDB = Replace(inieditor.value,"%s",irispath & "\data\POSLive.mdb")
   End If

   If POSLiveDB = "" Then
      OutputLog Warning,"GetDBStrings(): Unable to determine connect string for POSLive data.  Using default of iris\data\POSLive.mdb)"
      POSLiveDB = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & irispath & "\data\POSLive.mdb"
   End If

  'POSPend database
   POSPendDB = ""
   inieditor.key = "POSPend"
   If inieditor.value <> "" Then POSPendDB = inieditor.value

   If POSPendDB = "" Then
      inieditor.key = "POS"
      If inieditor.value <> "" Then POSPendDB = Replace(inieditor.value,"%s",irispath & "\data\POSPend.mdb")
   End If

   If POSPendDB = "" Then
      OutputLog Warning,"GetDBStrings(): Unable to determine connect string for POSPend data.  Using default of iris\data\POSPend.mdb)"
      POSPendDB = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & irispath & "\data\POSPend.mdb"
   End If

  'Kitchen database
   KitchenDB = ""
   iniEditor.iniFilePath = irisPath & "\ini\kitchen.ini"
   iniEditor.section = "Monitors"
   iniEditor.key = "ConnectString"
   If inieditor.value <> "" Then KitchenDB = inieditor.value
   If KitchenDB = "" Then KitchenDB = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & irispath & "\data\Kitchen.mdb"

   If Debug = 1 Then
      MsgBox IRISDB,,"IRISDB"
      MsgBox POSLiveDB,,"POSLiveDB"
      MsgBox POSPendDB,,"POSPendDB"
      MsgBox KitchenDB,,"KitchenDB"
   End If

End Function
'*************************************************************
'*************************************************************
'Execute EODMessage plug-in to display that purge is in progress
'
Function OpenMessageScreen()

On Error Resume Next

CloseMsgScreen = 0

   If WScript.Version >= 5.6 Then

      If fso.FileExists(irispath & "\bin\EODMessage.exe") Then
         Temp = Chr(34) & "Purging Data." & vbCrLf & vbCrLf & "Please Wait..." & Chr(34)
         Set MessageScreen = WSHShell.Exec(irispath & "\bin\EODMessage.exe /message=" & Temp & " /PlugIn /Timeout=2000000")
         CloseMsgScreen = 1
      Else
         OutputLog Warning, "Unable to locate EODMessage.exe at " & irispath & "\bin\EODMessage.exe.  Plug-in message will not be displayed during script."
      End If
   Else
      OutputLog Warning, "Message screen can only be displayed with Windows Scripting 5.6 or above."
   End If
End Function
'*************************************************************
'*************************************************************
'Initialize User Interface
'
Function InitializeUI()
   InitializeUI = 0    'Assume success

   On Error Resume Next

   sCaption = "IRIS Purge Script"
   oPB.Open sCaption, 40,100, 950,200
   If Err.Number Then
      OutputLog Error, "InitializeUI(): Error opening progress bar interface" & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      InitializeUI = 1
      Err.Clear
      Exit Function
   End If
   oPB.Show TRUE
   oPB.SetLine1 = "IRIS Purge Script"
   oPB.SetLine2 = ""
   sEvent = ""  ' initialize event flag (to sNull)...

End Function
'*************************************************************
'*************************************************************
'Get values for days of data to retain
'
Function GetDaysToKeep()
   GetDaysToKeep = 0   ' assume success

   On Error Resume Next

   If ClearData <> 1 Then
      'Set default number of days of data to keep for different types of data
      ArchivedTransDays = 366     'Archived/historical transactional data
      TransDays = 3               'POSLive transactional data
      KitchenDays = 2             'Kitchen data
      InvDetailDays = 3           'Unsummarized data in tblInventoryStatus
      InvLiveDays = 90            'Live (unarchived) Inventory Data to keep
      ForecastDays = ArchivedTransDays
      ForecastHistDays = ArchivedTransDays
      ShiftDays = ArchivedTransDays
      PurchaseDays = ArchivedTransDays
      InvHistDays = ArchivedTransDays
      ClockRecordDays = ArchivedTransDays
      MsgQDeletedDays = 3
      MsgQReadDays = 30
      MsgQUnreadDays = 60
      LogFileDays = 7             'Log Files
      EDMAuditDays = 30
      MinDiskSpace = 100000000
      MaxLogFileSize = 1500000
      CameraSaveLocationFileCount=50
      CameraSaveLocationPath="c:\Iris\Images\MiniChk1"

      'Flags Defaults
      KeepUnpaidTips = 0
      BackupLogsToServer=1
      ClearEventLogs=1

      If fso.FileExists(irispath & "\ini\SystemMaint.ini") Then
         iniEditor.iniFilePath = irisPath & "\ini\SystemMaint.ini"

         If MachineType = "Server" Then
            inieditor.section="Server"
         Else
            inieditor.section="Registers"
         End If

         inieditor.key = "TransDays"       'Keeping this for backwards compatibility, but changed key in ini file to LiveTransDays below
         If inieditor.value <> "" Then TransDays = inieditor.value

         inieditor.key = "LiveTransDays"
         If inieditor.value <> "" Then TransDays = inieditor.value

         inieditor.key = "ArchivedTransDays"
         If inieditor.value <> "" Then ArchivedTransDays = inieditor.value

         inieditor.key = "InvDetailDays"
         If inieditor.value <> "" Then InvDetailDays = inieditor.value

         inieditor.key = "KitchenDays"
         If inieditor.value <> "" Then KitchenDays = inieditor.value
         If KitchenDays < 2 Then KitchenDays = 2

         inieditor.key = "LogFileDays"
         If inieditor.value <> "" Then LogFileDays = inieditor.value

         ForecastDays = ArchivedTransDays
         ForecastHistDays = ArchivedTransDays
         ShiftDays = ArchivedTransDays
         PurchaseDays = ArchivedTransDays
         InvHistDays = ArchivedTransDays
         ClockRecordDays = ArchivedTransDays
         SystemDeletedItemDays = LogFileDays

         inieditor.key = "ForecastDays"
         If inieditor.value <> "" Then ForecastDays = inieditor.value

         inieditor.key = "ForecastHistDays"
         If inieditor.value <> "" Then ForecastHistDays = inieditor.value

         inieditor.key = "ShiftDays"
         If inieditor.value <> "" Then ShiftDays = inieditor.value

         inieditor.key = "PurchaseDays"
         If inieditor.value <> "" Then PurchaseDays = inieditor.value

         inieditor.key = "InvLiveDays"
         If inieditor.value <> "" Then InvLiveDays = inieditor.value

         inieditor.key = "InvHistDays"
         If inieditor.value <> "" Then InvHistDays = inieditor.value

         inieditor.key = "ClockRecordDays"
         If inieditor.value <> "" Then ClockRecordDays = inieditor.value

         inieditor.key = "SystemDeletedItemDays"
         If inieditor.value <> "" Then SystemDeletedItemDays = inieditor.value

         inieditor.key = "EDMAuditDays"
         If IsNumeric(inieditor.value) Then EDMAuditDays = inieditor.value

         inieditor.key = "MsgQueueDeletedMessages"
         If inieditor.value <> "" Then MsgQDeletedDays = inieditor.value

         inieditor.key = "MsgQueueReadMessages"
         If inieditor.value <> "" Then MsgQReadDays = inieditor.value

         inieditor.key = "MsgQueueUnreadMessages"
         If inieditor.value <> "" Then MsgQUnreadDays = inieditor.value

         inieditor.key = "MinDiskSpace"
         If inieditor.value <> "" Then MinDiskSpace = inieditor.value

         inieditor.key = "MaxLogFileSize"
         If inieditor.value <> "" Then MaxLogFileSize = inieditor.value

         inieditor.key = "KeepUnpaidTips"
         If inieditor.value <> "" Then KeepUnpaidTips = inieditor.value

         inieditor.key = "CameraSaveLocationFileCount"
         If inieditor.value <> "" Then CameraSaveLocationFileCount = cint(inieditor.value)

         inieditor.key = "CameraSaveLocationPath"
         If inieditor.value <> "" Then CameraSaveLocationPath = inieditor.value

         inieditor.key = "BackupLogsToServer"
         If inieditor.value <> "" Then BackupLogsToServer = CInt(inieditor.value)

         inieditor.key = "ClearEventLogs"
         If inieditor.value <> "" Then ClearEventLogs = CInt(inieditor.value)


      End If

   Else

      'Set values of days to keep to negative numbers to clear all data
      ArchivedTransDays = -99999     'Archived/historical transactional data
      TransDays = -99999             'POSLive transactional data
      KitchenDays = -99999           'Kitchen data
      InvDetailDays = -99999         'Unsummarized data in tblInventoryStatus
      InvLiveDays = -99999           'Live (unarchived) Inventory Data to keep
      ForecastDays = ArchivedTransDays
      ForecastHistDays = ArchivedTransDays
      ShiftDays = ArchivedTransDays
      PurchaseDays = ArchivedTransDays
      InvHistDays = ArchivedTransDays
      ClockRecordDays = ArchivedTransDays
      LogFileDays = -99999             'Log Files
      SystemDeletedItemDays = -99999
      EDMAuditDays = -99999
      MsgQDeletedDays = -99999
      MsgQReadDays = -99999
      MsgQUnreadDays = -99999
      CameraSaveLocationFileCount=0

   End If

End Function
'*************************************************************
'*************************************************************
'Get Business Date
'
Function GetBusinessDate()
   GetBusinessDate = 0   'Assume success

   On Error Resume Next

   Dim rsBusinessDate

   dbConn.Open POSLiveDB
   Set rsBusinessDate = CreateObject ("ADODB.Recordset")
   rsBusinessDate.Open "SELECT * FROM tblBusinessDate",dbConn,1,2
   If rsBusinessDate.EOF = 0 Then
      BusinessDate = rsBusinessDate.Fields("BusinessDate")
   Else
      BusinessDate = Date
   End If
   rsBusinessDate.close

   Set rsBusinessDate = Nothing

   dbConn.close
End Function

'*************************************************************
'*************************************************************
'Set Business Date
'
Function SetBusinessDate(sDB, CurrentBusDate)
   On Error Resume Next
   dbConn.Open sDB
   dbConn.Execute "DELETE * FROM tblBusinessDate"
   dbConn.Execute "INSERT INTO tblBusinessDate (BusinessDate) VALUES(#" & Cstr(CurrentBusDate) & "#)"
   dbConn.Close
End Function

'*************************************************************
'*************************************************************
'Purge databases
'
Function PurgeData()
   PurgeData = 0   'Assume success

   On Error Resume Next

   'Purge POSLive
   If UseProgressBar = 1 Then
      oPB.PctComplete = 10
      oPB.SetLine2 = "Purging POSLive..."
   End If
   result=PurgePOSLive()

   'Purge Kitchen
   If UseProgressBar = 1 Then
      oPB.PctComplete = 20
      oPB.SetLine2 = "Purging Kitchen..."
   End If
   result=PurgeKitchen()

  'Purge POSPend.mdb if cleaning data
   If ClearData = 1 Then
      If UseProgressBar = 1 Then
         oPB.PctComplete = 40
         oPB.SetLine2 = "Purging POSPend..."
      End If
      result=PurgePOSPend()
   End If

   'Purge Credit Card data if cleaning data
   If ClearData = 1 AND MachineType = "Server" Then
      If UseProgressBar = 1 Then
         oPB.PctComplete = 50
         oPB.SetLine2 = "Purging Credit Card Data..."
      End If
      result=PurgeCCA()
   End If

   'Purge IRIS SQL Server Database
   If MachineType = "Server" Then
      If UseProgressBar = 1 Then
         oPB.PctComplete = 60
         oPB.SetLine2 = "Purging IRIS Database..."
      End If
      PurgeIRIS()
   End If
End Function
'*************************************************************
'*************************************************************
'Purge POSLive database
'
Function PurgePOSLive()
   PurgePOSLive = 0   'Assume success

   On Error Resume Next

   dbConn.Open POSLiveDB
   If Err.Number Then
      OutputLog Error, "Unable to open " & POSLiveDB & ".  POSLive database cannot be purged." & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Err.Clear
   End If

  'If purging a non-MSAccess POSLive then set the timout value higher
   If Not Instr(1,LCASE(POSLiveDB),"microsoft.jet") Then dbConn.CommandTimeout = 300

   If KeepUnpaidTips = 0 Then
     strSQL = GetDeleteString(POSLiveDB) & " FROM tblOrder WHERE DateDiff(" & GetDateDiffInterval(POSLiveDB,"day") & ",(tblOrder.BusinessDate)," & GetDateString(POSLiveDB,BusinessDate) & ") > " & TransDays-1
   Else
     strSQL = GetDeleteString(POSLiveDB) & " FROM tblOrder WHERE DateDiff(" & GetDateDiffInterval(POSLiveDB,"day") & ",(tblOrder.BusinessDate)," & GetDateString(POSLiveDB,BusinessDate) & ") > " & TransDays-1 & " AND (tblOrder.CreditTips = tblOrder.TipsPaid) AND (tblOrder.State <> 4)"
   End If
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblOrder in " & POSLiveDB & "."

   strSQL = GetDeleteString(POSLiveDB) & " FROM tblOrder WHERE (tblOrder.State) > 131071"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge training records from tblOrder in " & POSLiveDB & "."

   strSQL = GetDeleteString(POSLiveDB) & " FROM tblCashManagement WHERE DateDiff(" & GetDateDiffInterval(POSLiveDB,"day") & ",(tblCashManagement.BusinessDate)," & GetDateString(POSLiveDB,BusinessDate) & ") > " & TransDays-1
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblCashManagement in " & POSLiveDB & "."

   strSQL = GetDeleteString(POSLiveDB) & " FROM tblDrawerNum WHERE DateDiff(" & GetDateDiffInterval(POSLiveDB,"day") & ",(tblDrawerNum.BusinessDate)," & GetDateString(POSLiveDB,BusinessDate) & ") > " & TransDays-1
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblDrawerNum in " & POSLiveDB & "."

   strSQL = GetDeleteString(POSLiveDB) & " FROM tblDrawerTrans WHERE DateDiff(" & GetDateDiffInterval(POSLiveDB,"day") & ",(tblDrawerTrans.BusinessDate)," & GetDateString(POSLiveDB,BusinessDate) & ") > " & TransDays-1
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblDrawerTrans in " & POSLiveDB & "."

   strSQL = GetDeleteString(POSLiveDB) & " FROM tblReprintDrawer WHERE DateDiff(" & GetDateDiffInterval(POSLiveDB,"day") & ",(tblReprintDrawer.BusinessDate)," & GetDateString(POSLiveDB,BusinessDate) & ") > " & TransDays-1
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblReprintDrawer in " & POSLiveDB & "."

   strSQL = GetDeleteString(POSLiveDB) & " FROM tblIRISEvents WHERE DateDiff(" & GetDateDiffInterval(POSLiveDB,"day") & ",(tblIRISEvents.BusinessDate)," & GetDateString(POSLiveDB,BusinessDate) & ") > " & TransDays-1
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblIRISEvents in " & POSLiveDB & "."

   If ClearData = 1 Then

      strSQL = GetDeleteString(POSLiveDB) & " FROM tblOrder"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblOrder in " & POSLiveDB & "."

      strSQL = GetDeleteString(POSLiveDB) & " FROM tblGrandTotal"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblGrandTotal in " & POSLiveDB & "."

      strSQL = GetDeleteString(POSLiveDB) & " FROM tblDrawerServer"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblDrawerServer in " & POSLiveDB & "."

      strSQL = GetDeleteString(POSLiveDB) & " FROM tblDrawerTotal"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblDrawerTotal in " & POSLiveDB & "."

      strSQL = GetDeleteString(POSLiveDB) & " FROM tblNextNum"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblNextNum in " & POSLiveDB & "."

      strSQL = GetDeleteString(POSLiveDB) & " FROM tblIRISEvents"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblIRISEvents in " & POSLiveDB & "."

   End If

   dbConn.Close

End Function
'*************************************************************
'*************************************************************
'Purge Kitchen database
'
Function PurgeKitchen()
   PurgeKitchen = 0   'Assume success

   On Error Resume Next

   dbConn.Open KitchenDB
   If Err.Number Then OutputLog Error, "Unable to open " & KitchenDB & ".  Kitchen database cannot be purged: " & Err.Description
   Err.Clear

   If ClearData <> 1 Then
      strSQL = GetDeleteString(KitchenDB) & " FROM tblOrder WHERE DateDiff(" & GetDateDiffInterval(KitchenDB,"day") & ",(tblOrder.BussDate)," & GetDateString(KitchenDB,BusinessDate) & ") > " & KitchenDays-1
   Else
      strSQL = GetDeleteString(KitchenDB) & " FROM tblOrder"
   End If
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblOrder in " & KitchenDB & "."

   dbConn.Close

End Function
'*************************************************************
'*************************************************************
'Purge Credit Card Data
'
Function PurgeCCA()
   PurgeCCA = 0   'Assume success

   On Error Resume Next

   Dim CCAPath

   CCAPath = WSHShell.RegRead ("HKLM\SOFTWARE\xpient solutions\Credit Card Application\WorkingDir")
   If CCAPath = "" Then CCAPath = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\Credit Card Application\WorkingDir")
   If CCAPath = "" Then
      OutputLog Error,"PurgeCCA(): Unable to locate CCA path in registry.  Unable to purge CCA data."
      PurgeCCA = 1
      Exit Function
   End If

   CCADB = CCAPath & "\Reports\CCATransactions.mdb"

   If Debug = 1 Then MsgBox CCAPath,,"CCAPath"
   If Debug = 1 Then MsgBox CCADB,,"CCADB"

   If Not fso.FileExists(CCADB) Then
      OutputLog Error,"Unable to locate " & CCADB & ".  Credit card database cannot be purged."
   Else

      dbConn.Open "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & CCADB
      If Err.Number Then OutputLog Error, "Unable to open " & CCADB & ".  Credit card database cannot be purged: " & Err.Description
      Err.Clear

      strSQL = "DELETE * FROM tblAuthData"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblAuthData in " & CCADB & "."

      strSQL = "DELETE * FROM tblAuthHistory"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblAuthHistory in " & CCADB & "."

      strSQL = "DELETE * FROM tblSettleHistory"
      dbConn.Execute strSQL
      If Err.Number Then SQLError "Unable to purge records from tblSettleHistory in " & CCADB & "."

      dbConn.Close
   End If

   PurgeFilesByExtension CCAPath,"dat",1

End Function
'*************************************************************
'*************************************************************
'Purge POSPend database
'
Function PurgePOSPend()
   PurgePOSPend = 0   'Assume success

   On Error Resume Next

   dbConn.Open POSPendDB
   If Err.Number Then OutputLog Error, "Unable to open " & POSPendDB & ".  POSPend database cannot be purged: " & Err.Description
   Err.Clear

   strSQL = GetDeleteString(POSPendDB) & " FROM tblAuditEvents"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblAuditEvents in " & POSPendDB & "."

   strSQL = GetDeleteString(POSPendDB) & " FROM tblBusinessDateUpdate"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblBusinessDateUpdate in " & POSPendDB & "."

   strSQL = GetDeleteString(POSPendDB) & " FROM tblPendingDrawer"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblPendingDrawer in " & POSPendDB & "."

   strSQL = GetDeleteString(POSPendDB) & " FROM tblPendingEmployee"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblPendingEmployee in " & POSPendDB & "."

   strSQL = GetDeleteString(POSPendDB) & " FROM tblPendingEOD"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblPendingEOD in " & POSPendDB & "."

   strSQL = GetDeleteString(POSPendDB) & " FROM tblPendingGrandTotal"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblPendingGrandTotal in " & POSPendDB & "."

   strSQL = GetDeleteString(POSPendDB) & " FROM tblPendingOrders"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblPendingOrders in " & POSPendDB & "."

   strSQL = GetDeleteString(POSPendDB) & " FROM tblIRISEvents"
   dbConn.Execute strSQL
   If Err.Number Then SQLError "Unable to purge records from tblIRISEvents in " & POSPendDB & "."

   dbConn.Close

End Function
'*************************************************************
'*************************************************************
'Purge IRIS database
'
Function PurgeIRIS()
   PurgeIRIS = 0   'Assume success

   On Error Resume Next

   dbConn.Open IRISDB
   If Err.Number Then
      OutputLog Error, "PurgeIRIS(): Error opening IRIS Database.  IRIS database cannot be purged: " & Err.Description
      Err.Clear
      Exit Function
   Else
      dbConn.CommandTimeout=1200

      If ClearData <> 1 Then

         dbConn.sp_PurgeTransData ArchivedTransDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgeTransData stored procedure."

         dbConn.sp_PurgeInventoryArchive InvHistDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgeInventoryArchive stored procedure."

         dbConn.sp_InventoryArchive InvDetailDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_InventoryArchive stored procedure."

         dbConn.sp_PurgeInventoryStatus InvLiveDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgeInventoryStatus stored procedure."

         dbConn.sp_PurgePurchaseInvoice PurchaseDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgePurchaseInvoice stored procedure."

         dbConn.sp_PurgeShiftData ShiftDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgeShiftData stored procedure."

         dbConn.sp_PurgeForecast ForecastDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgeForecast stored procedure."

         dbConn.sp_PurgeForecastHistory ForecastHistDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgeForecastHistory stored procedure."

         dbConn.sp_PurgeSystemDeletedOrderItems SystemDeletedItemDays
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_PurgeSystemDeletedOrderItems stored procedure."

         dbConn.sp_RpyPC_CleanUp
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_RpyPC_Cleanup stored procedure."

         strSQL = "DELETE FROM tblPOSMsgQ WHERE Deleted <> 0 AND DATEDIFF(day, SentTime,GetDate()) > " & MsgQDeletedDays
         dbConn.Execute strSQL
         If Err.Number Then SQLError "PurgeIRIS(): Unable to purge deleted messages from tblPOSMsgQ."

         strSQL = "DELETE FROM tblPOSMsgQ WHERE Acknowledged <> 0 AND DATEDIFF(day, SentTime,GetDate()) > " & MsgQReadDays
         dbConn.Execute strSQL
         If Err.Number Then SQLError "PurgeIRIS(): Unable to purge read/acknowledged messages from tblPOSMsgQ."

         strSQL = "DELETE FROM tblPOSMsgQ WHERE Acknowledged = 0 AND DATEDIFF(day, SentTime,GetDate()) > " & MsgQUnreadDays
         dbConn.Execute strSQL
         If Err.Number Then SQLError "PurgeIRIS(): Unable to purge unread messages from tblPOSMsgQ."

         strSQL = "DELETE FROM tblEDMTransRegStatus WHERE DATEDIFF(day, EDMProcessDateTime,GetDate()) > " & EDMAuditDays
         dbConn.Execute strSQL
         If Err.Number Then SQLError "PurgeIRIS(): Unable to purge EDM Transaction Audit records."

         'Purge tblOrderPay.EncryptedAccount data based on number of days to keep in CCA.ini
         CCAPath = WSHShell.RegRead ("HKLM\SOFTWARE\xpient solutions\Credit Card Application\WorkingDir")
         If CCAPath = "" Then CCAPath = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\Credit Card Application\WorkingDir")

         If CCAPath = "" Then
            OutputLog Error,"PurgeIRIS(): Unable to locate CCA path in registry.  Unable to purge tblOrderPay.EncryptedAccount"
         Else
            iniEditor.iniFilePath = CCAPath & "\cca.ini"
            inieditor.section="DATABASE"
            inieditor.key = "INFO_TO_KEEP_DAYS"
            CCA_KEEP_DAYS = iniEditor.Value
            If CCA_KEEP_DAYS = "" Then
               OutputLog Error,"PurgeIRIS(): CCA_KEEP_DAYS not set in cca.ini.  Unable to purge tblOrderPay.EncryptedAccount"
            Else
               strSQL = "UPDATE tblOrderPay SET EncryptedAccount = NULL WHERE EncryptedAccount is not NULL and BusinessDate < dateadd(day,-" & CCA_KEEP_DAYS & ",getdate())"
               dbConn.Execute strSQL
               If Err.Number Then SQLError "PurgeIRIS(): Unable to purge tblOrderPay.EncryptedAccount."
            End If
         End If
      Else
         dbConn.sp_ClearAllData
         If Err.Number Then SQLError "PurgeIRIS(): Error executing sp_ClearAllData stored procedure."
         Err.Clear
      End If
   End If

   dbConn.Close

End Function
'*************************************************************
'*************************************************************
'Log file maintenance
'
Function LogFileMaint()
   LogFileMaint = 0   'Assume success

   On Error Resume Next

  'Get Path to WkServer Log file if it exists
   Dim WKServerLog
   WKServerLog=WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\Wkserver\logfile")


   If ClearData = 1 Then
      'Delete all contents of Iris\Log folder
      PurgeFolder(irispath & "\log")

      'Delete the following files
      If fso.FileExists(irispath & "\data\poslog.dat") Then fso.DeleteFile irispath & "\data\poslog.dat"
      If fso.FileExists(irispath & "\data\OrdDAlog.dat") Then fso.DeleteFile irispath & "\data\OrdDAlog.dat"
      If fso.FileExists(irispath & "\data\DSClLog.dat") Then fso.DeleteFile irispath & "\data\DSClLog.dat"
      If fso.FileExists(irispath & "\data\DSClLog.1") Then fso.DeleteFile irispath & "\data\DSClLog.1"
      If fso.FileExists(irispath & "\data\DSClLog.2") Then fso.DeleteFile irispath & "\data\DSClLog.2"
      If fso.FileExists(irispath & "\data\DSClLog.3") Then fso.DeleteFile irispath & "\data\DSClLog.3"
      If fso.FileExists(irispath & "\data\DSSrvLog.dat") Then fso.DeleteFile irispath & "\data\DSSrvLog.dat"
      If fso.FileExists(irispath & "\data\SyncDwDp.dat") Then fso.DeleteFile irispath & "\data\SyncDwDp.dat"
      If fso.FileExists(irispath & "\data\Orderlog.txt") Then fso.DeleteFile irispath & "\data\Orderlog.txt"
      If fso.FileExists(WkServerLog) Then fso.DeleteFile WkServerLog

      'Clear Event Logs
      Err.Clear
      strComputer = "."
      Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate, (Backup, Security)}!\\" & strComputer & "\root\cimv2")
      Set colLogFiles = objWMIService.ExecQuery("Select * from Win32_NTEventLogFile")
      For each objLogfile in colLogFiles
         objLogFile.ClearEventLog()
      Next

   Else
      strComputer = "."
      Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate, (Backup, Security)}!\\" & strComputer & "\root\cimv2")

     'Backup Application Event Log
      Set colLogFiles = objWMIService.ExecQuery("Select * from Win32_NTEventLogFile WHERE LogFileName='Application'")
      For each objLogfile in colLogFiles
         Err.Clear
         objLogFile.BackupEventLog(irisPath & "\Log\Application.evt")
         If Err.Number Then
            OutputLog Error, "LogFileMaint(): Unable to back up the Application event log." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
            Err.Clear
         Else
            If ClearEventLogs = 1 Then objLogFile.ClearEventLog()
         End If
      Next

     'Backup System Event Log
      Set colLogFiles = objWMIService.ExecQuery("Select * from Win32_NTEventLogFile WHERE LogFileName='System'")
      For each objLogfile in colLogFiles
         Err.Clear
         objLogFile.BackupEventLog(irisPath & "\Log\System.evt")
         If Err.Number Then
            OutputLog Error, "LogFileMaint(): Unable to back up the System event log." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Else
            If ClearEventLogs = 1 Then objLogFile.ClearEventLog()
         End If
      Next

     'Backup Security Event Log
      Set colLogFiles = objWMIService.ExecQuery("Select * from Win32_NTEventLogFile WHERE LogFileName='Security'")
      For each objLogfile in colLogFiles
         Err.Clear
         objLogFile.BackupEventLog(irisPath & "\Log\Security.evt")
         If Err.Number Then
            OutputLog Error, "LogFileMaint(): Unable to back up the Security event log." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Else
            If ClearEventLogs = 1 Then objLogFile.ClearEventLog()
         End If
      Next

      If fso.FileExists(WkServerLog) Then fso.MoveFile WkServerLog, irispath & "\Log\"

      PurgeOversizedFiles(irisPath & "\Log")

      Dim FileDetermined, fil

     'Determine name of zip file to be created
      If MachineType = "Register" Then
        iniEditor.iniFilePath = irisPath & "\ini\AppIni.ini"
        inieditor.section="POS"
        inieditor.key = "REGNUM"
        NewZipFile = "Reg" & CStr(iniEditor.Value) & "-" & BDFile(BusinessDate)
      Else
        NewZipFile = BDFile(BusinessDate)
      End If

      i = 1
      FileDetermined = 0
      fil = irispath & "\Log\Backup\" & NewZipFile & ".zip"

      While FileDetermined = 0

         If Not fso.FileExists(fil) Then
            NewZipFile = fil
            FileDetermined = 1
         Else
            fil = irispath & "\Log\Backup\" & NewZipFile & "-" & i & ".zip"
            i = i + 1
         End If
      wend

     'Zip up all logs
      Dim strDataLogs, PSIZip
      PSIZip = Chr(34) & irisPath & "\bin\PSIZip.exe" & Chr(34)

     'Add all files except other ZIP files from iris\log folder & its subfolders
      CmdLine = PSIZip & " /ZipFile=" & NewZipFile & " /Action=Zip /FilePath=" & irisPath & "\Log /FileName=*.* /Exclude=" & irisPath & "\log\backup\*.* /SubFolders /q /DeleteOriginal /NoUI"
      Result = WSHShell.Run(CmdLine,,true)
      If Result <> 0 Then OutputLog Warning,"LogFileMaint(): (" & Result & ")Error zipping up " & irisPath & "\Log\*.* into " & NewZipFile & "."

     'Add log files in iris\data folder
      strDataLogs = "poslog.dat,OrdDAlog.dat,DSClLog.dat,DSClLog.1,DSClLog.2,DSClLog.3,DSSrvLog.dat,SyncDwDp.dat,Orderlog.txt"
      CmdLine = PSIZip & " /ZipFile=" & NewZipFile & " /Action=Zip /FilePath=" & irisPath & "\Data /FileName=" & strDataLogs & " /q /DeleteOriginal /NoUI"
      Result = WSHShell.Run(CmdLine,,true)
      If Result <> 0 Then OutputLog Warning,"LogFileMaint(): (" & Result & ")Error zipping up logs from iris\data folder into " & NewZipFile & "."

     'Add dump.txt
      If fso.FileExists(fso.GetDriveName(irispath) & "\dump.txt") Then
         CmdLine = PSIZip & " /ZipFile=" & NewZipFile & " /Action=Zip /FilePath=" & fso.GetDriveName(irispath) & " /FileName=dump.txt /q /DeleteOriginal /NoUI"
         Result = WSHShell.Run(CmdLine,,true)
         If Result <> 0 Then OutputLog Warning,"LogFileMaint(): (" & Result & ")Error zipping up " & fso.GetDriveName(irispath) & "\dump.txt into " & NewZipFile & "."
      End If

     'Add minidumps
      Dim MiniDumpDir, MiniDumpFiles, MiniDumpFile, CheckMiniDump
      CheckMiniDump = False
      Set MiniDumpDir = fso.GetFolder(WSHShell.ExpandEnvironmentStrings("%SystemRoot%") & "\minidump")
      Set MiniDumpFiles = MiniDumpDir.Files
      For Each MiniDumpFile in MiniDumpFiles
         If Left(LCase(MiniDumpFile.Name),4) = "mini" And Right(LCase(MiniDumpFile.Name),4) = ".dmp" Then
            CheckMiniDump = True
         End If
      Next
      If CheckMiniDump = True Then
         CmdLine = PSIZip & " /ZipFile=" & NewZipFile & " /Action=Zip /FilePath=" & WSHShell.ExpandEnvironmentStrings("%SystemRoot%") & "\minidump\ /FileName=mini*.dmp /q /DeleteOriginal /NoUI"
         Result = WSHShell.Run(CmdLine,,true)
         If Result <> 0 Then OutputLog Warning,"LogFileMaint(): (" & Result & ")Error zipping up " & WSHShell.ExpandEnvironmentStrings("%SystemRoot%") & "\minidump into " & NewZipFile & "."
      End If

     'Delete old archived log files
      Err.Clear
      Dim LogFolder, LogFiles
      Set LogFolder = fso.GetFolder(irispath & "\Log\Backup")
      If Err.Number Then
         OutputLog Error, "LogFileMaint(): Unable to Set LogFolder object for iris\log\backup folder." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
         Exit Function
      End If
      Set LogFiles = LogFolder.Files
      If Err.Number Then
         OutputLog Error, "LogFileMaint(): Unable to Set LogFiles object for iris\log\backup files." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
      End If

      For Each f in LogFiles
            If UCase(Left(F.Name,3)) = "REG" Then
              LogDate = Mid(F.Name,InStr(F.Name,"-")+1)
            Else
              LogDate = F.Name
            End If
            If DateValue(BusinessDate) - DateValue(Mid(LogDate,6,2) & "/" & Mid(LogDate,9,2) & "/" & Left(LogDate,4)) > LogFileDays - 1 Then fso.DeleteFile(f)
      Next

      'Copy files Logs to Server
      If MachineType = "Register" And BackupLogsToServer = 1 Then

         iniEditor.iniFilePath = irisPath & "\ini\dataserv.ini"
         iniEditor.section="PDS"
         iniEditor.key="POSTRANS"

         TempStr = Mid(IniEditor.Value, 3)
         TempStr = Mid(TempStr, 1, InStr(TempStr, "\") - 1)
         g_ServerName = TempStr

         i = InStr(lcase(IniEditor.Value),"\data")
         g_ServerIrisPath = Mid(IniEditor.Value ,1,i)

         r = InstrRev(g_ServerIrisPath, "\")
         g_ServerIrisPath = Mid(g_ServerIrisPath, 1, r-1)

         'Copy Newest log files
         Err.Clear
         Set LogFolder = fso.GetFolder(irispath & "\Log\Backup")
         If Err.Number Then
            OutputLog Error, "LogFileMaint(): Unable to Set LogFolder object for iris\log\backup folder." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
            Err.Clear
            Exit Function
         End If
         Set LogFiles = LogFolder.Files
         If Err.Number Then
            OutputLog Error, "LogFileMaint(): Unable to Set LogFiles object for iris\log\backup files." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
            Err.Clear
            Exit Function
         End If

         MaxDate = 0
         For Each f in LogFiles
            If UCase(Left(F.Name,3)) = "REG" Then
              LogDate = Mid(F.Name,InStr(F.Name,"-")+1)
            Else
              LogDate = F.Name
            End If
            If DateValue(Mid(LogDate,6,2) & "/" & Mid(LogDate,9,2) & "/" & Left(LogDate,4)) > MaxDate Then
               MaxDate = DateValue(Mid(LogDate,6,2) & "/" & Mid(LogDate,9,2) & "/" & Left(LogDate,4))
            End If
         Next

         For Each f in LogFiles
            If UCase(Left(F.Name,3)) = "REG" Then
              LogDate = Mid(F.Name,InStr(F.Name,"-")+1)
            Else
              LogDate = F.Name
            End If
            If DateValue(Mid(LogDate,6,2) & "/" & Mid(LogDate,9,2) & "/" & Left(LogDate,4)) = MaxDate Then
               fso.CopyFile f,g_ServerIrisPath & "\log\" & f.Name, True
            End If
         Next

      End If

      Set LogFiles = Nothing
      Set LogFolder = Nothing

   End If

End Function
'*************************************************************
'*************************************************************
'Image file maintenance
'
Function PurgeImage()
   PurgeImage = 0   'Assume success

   On Error Resume Next

   If CameraSaveLocationPath = "" Then Exit Function

   If fso.FolderExists(CameraSaveLocationPath) = False Then

     Exit Function
   End If

   If ClearData = 1 Then
      PurgeFolder(CameraSaveLocationPath)
   Else
     'Delete old image files
      Err.Clear
      Dim ImageFolder, ImageFiles
      Set ImageFolder = fso.GetFolder(CameraSaveLocationPath)
      If Err.Number Then
         OutputLog Info, "LogFileMaint(): ImageFolder not found, skipping: " & CameraSaveLocationPath
         Err.Clear
         Exit Function
      End If
      Set ImageFiles = ImageFolder.Files
      If Err.Number Then
         OutputLog Error, "LogFileMaint(): Unable to Set ImageFiles object for " & CameraSaveLocationPath & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
      End If

      Dim ImageFileList()
      ReDim ImageFileList(1,0)
      Dim ImageCount
      For Each ImageFile in ImageFiles
         ImageCount = UBound(ImageFileList,2)+1
         ReDim Preserve ImageFileList(1,ImageCount)
         ImageFileList(0,ImageCount) = ImageFile.Name
         ImageFileList(1,ImageCount) = ImageFile.DateLastModified
      Next

      'Sort by Date
      Dim bSorted
      bSorted = False
      Do While bSorted = False
         bSorted = True
         For ImageCount = 1 To UBound(ImageFileList,2) - 1
            If ImageFileList(1,ImageCount) < ImageFileList(1,ImageCount + 1) Then
               bSorted = False
               ImageFileList(0,0) = ImageFileList(0,ImageCount)
               ImageFileList(1,0) = ImageFileList(1,ImageCount)
               ImageFileList(0,ImageCount) = ImageFileList(0,ImageCount + 1)
               ImageFileList(1,ImageCount) = ImageFileList(1,ImageCount + 1)
               ImageFileList(0,ImageCount + 1) = ImageFileList(0,0)
               ImageFileList(1,ImageCount + 1) = ImageFileList(1,0)
            End If
         Next
      Loop

      'Delete files that exceed CameraSaveLocationFileCount
      For ImageCount = 1 To UBound(ImageFileList,2)
        If ImageCount > CameraSaveLocationFileCount Then
            fso.DeleteFile CameraSaveLocationPath & "\" & ImageFileList(0,ImageCount)
        End If
      Next

      Set ImageFiles = Nothing
      Set ImageFolder = Nothing
   End If
End Function
'*************************************************************
'*************************************************************
'Get file name for specified business date
'
Function BDFile(BusDate)

   Dim BDMonth
   Dim BDDay

   BDMonth = Month(BusDate)
   If BDMonth < 10 Then BDMonth = "0" & BDMonth

   BDDay = Day(BusDate)
   If BDDay < 10 Then BDDay = "0" & BDDay

   BDFile = Year(BusinessDate) & "-" & BDMonth & "-" & BDDay

End Function
'*************************************************************
'*************************************************************
'Purge OverSized Files log archive folder
'
Sub PurgeOverSizedFiles(strPath)

   Dim LogFile
   Dim IrisDrive
   Dim SubFolders
   Dim SubFolder
   Dim LogFolder
   Dim LogFiles
   Dim LowDiskSpace

   'On Error Resume Next
   'Get Drive Object
   Set IrisDrive = fso.GetDrive(fso.GetDriveName(irispath))
   If Err.Number Then
      OutputLog Error, "PurgeOversizedFiles(): Error setting IrisDrive object.  Cannot validate file sizes against remaining disk space."
      Err.Clear
   Else
      'If low on disk space delete file instead of backing up
      If (IrisDrive.FreeSpace / 1000) < (MinDiskSpace / 1000) Then
         LowDiskSpace = True
      End If
   End If

   'Get Log Folder Object
   Set LogFolder = fso.GetFolder(strPath)
   If Err.Number Then
      OutputLog Error, "PurgeOversizedFiles(): Unable to Set LogFolder object for " & strPath & " folder." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Err.Clear
      Exit Sub
   End If

   'Get Log FilesObject
   Set LogFiles = LogFolder.Files
   If Err.Number Then
      OutputLog Error, "PurgeOversizedFiles(): Unable to Set LogFiles object for " & strPath & " files." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Err.Clear
      Exit Sub
   End If

   For Each LogFile in LogFiles
     'Make sure file name is valid.
      If Left(LogFile.Name,1) = "." Then LogFile.Name = "-No Name-" & LogFile.Name

     'If low on disk space delete file instead of backing up
      If LowDiskSpace = True Then
         If LogFile.Size > CLng(MaxLogFileSize) Then
            OutputLog Warning, "PurgeOversizedFiles(): Low disk space (" & IrisDrive.FreeSpace & " bytes is less than MinDiskSpace of " & MinDiskSpace & " bytes).  " & LogFile.Path & " is " & LogFile.Size & " bytes and will be deleted, not archived."
            LogFile.Delete
         End If
      ElseIf LCase(Right(LogFile.Path,4)) = ".log" Then
         If LogFile.Size > 500000000 Then
            OutputLog Warning,"PurgeOversizedFiles(): (" & LogFile.Name & ") Removing file because it is too large. (" & CStr(LogFile.Size) & " bytes)"
            Set LogTemp = fso.CreateTextFile(LogFile.Path, True)
            LogTemp.WriteLine("purge.vbs Remove log because it is too large.")
            LogTemp.Close()
         End If
      End If
   Next

   Set SubFolders = LogFolder.SubFolders
   For Each SubFolder In SubFolders
      PurgeOversizedFiles(SubFolder.Path)
   Next

   Set SubFolders = Nothing
   Set LogFiles = Nothing
   Set LogFolder = Nothing
   Set IrisDrive = Nothing

End Sub
'*************************************************************
'*************************************************************
'Move specified file into new log archive folder
'
Sub PurgeFilesByExtension(strPath,strExtension,PurgeSubfolders)
   On Error Resume Next

   Dim oFolder, oFiles, oSubFolders
   Set oFolder = fso.GetFolder(strPath)
   If Err.Number Then
      OutputLog Error, "PurgeFilesByExtension(): Unable to Set oFolder object for " & strPath & " folder." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Err.Clear
      Exit Sub
   End If
   Set oFiles = oFolder.Files
   If Err.Number Then
      OutputLog Error, "PurgeFilesByExtension(): Unable to Set oFiles object for " & strPath & " files." & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Err.Clear
      Exit Sub
   End If

   For Each f in oFiles
      If Left(strExtension,1) = "." Then strExtension = Right(strExtension,Len(strExtension)-1)
      Err.Clear
      If LCASE(fso.GetExtensionName(f.Path)) = LCASE(strExtension) Then
         fso.DeleteFile f.Path
         If Err.Number Then
            OutputLog Error,"PurgeFilesByExtension(" & strPath & "," & strExtension & "," & PurgeSubfolders & "): Error deleting file " & f.Path & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
            Err.Clear
         End If
      End If
   Next

   If PurgeSubfolders = 1 Then
      Set oSubFolders = oFolder.SubFolders
      For Each sf In oSubFolders
         PurgeFilesByExtension sf.Path,strExtension,1
      Next
   End If

   Set oSubFolders = Nothing
   Set oFiles = Nothing
   Set oFolder = Nothing

End Sub
'*************************************************************
'*************************************************************
'Purge specified folder
'
Function PurgeFolder(FolderToPurge)
   PurgeFolder = 0   'Assume success

   On Error Resume Next

   Set Folder = fso.GetFolder(FolderToPurge)
   If Err.Number Then
      OutputLog Error, "PurgeFolder(): Unable to Set Folder object." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      PurgeFolder = 1
      Err.Clear
      Exit Function
   End If

   Set Files = Folder.Files
   If Err.Number Then
      OutputLog Error, "PurgeFolder(): Unable to Set Files object." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      PurgeFolder = 1
      Err.Clear
      Exit Function
   End If

   For Each f in Files
      fso.DeleteFile f.path
      If Err.Number Then
         OutputLog Error, "PurgeFolder(): Error deleting file (" & f.path & ")." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
      End If
   Next

   'Delete subfolders
   Set SubFolders = Folder.Subfolders
   If Err.Number Then
      OutputLog Error, "PurgeFolder(): Unable to Set Subfolders object." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      PurgeFolder = 1
      Err.Clear
      Exit Function
   End If

   For each f in SubFolders
      fso.DeleteFolder f.path
      If Err.Number Then
         OutputLog Error, "PurgeFolder(): Error deleting folder (" & f.path & ")." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
         Err.Clear
      End If
   Next

End Function
'*************************************************************
'*************************************************************
'Return SQL Delete string (DELETE for SQL Server/MSDE, DELETE * for Access)
'
Function GetDeleteString(strProvider)
   On Error Resume Next
   GetDeleteString = "DELETE"
   If Instr(1,LCASE(strProvider),"microsoft.jet") > 0 Then GetDeleteString = "DELETE *"
   If Debug = 1 Then MsgBox "Provider = " & strProvider & vbcrlf & "DeleteString = " & GetDeleteString,,"GetDeleteString()"
End Function
'*************************************************************
'*************************************************************
'Return SQL date string for provider
'
Function GetDateString(strProvider,dtDate)
   On Error Resume Next
   GetDateString = Chr(39) & dtDate & Chr(39)
   If Instr(LCASE(strProvider),"microsoft.jet") Then GetDateString = "#" & dtDate & "#"
   If Debug = 1 Then MsgBox "Provider = " & strProvider & vbcrlf & "dtDate = " & dtDate & vbcrlf & "DateString = " & GetDateString,,"GetDateString()"
End Function
'*************************************************************
'*************************************************************
'Return SQL datediff interval syntax for provider
'
Function GetDateDiffInterval(strProvider,strInterval)
   On Error Resume Next
   GetDateDiffInterval = strInterval

   If Instr(LCASE(strProvider),"microsoft.jet") Then
      Select Case LCASE(strInterval)
         Case "year"
           GetDateDiffInterval = Chr(39) & "yyyy" & Chr(39)
         Case "yy"
           GetDateDiffInterval = Chr(39) & "yyyy" & Chr(39)
         Case "yyyy"
           GetDateDiffInterval = Chr(39) & "yyyy" & Chr(39)
         Case "month"
           GetDateDiffInterval = Chr(39) & "m" & Chr(39)
         Case "m"
           GetDateDiffInterval = Chr(39) & "m" & Chr(39)
         Case "mm"
           GetDateDiffInterval = Chr(39) & "m" & Chr(39)
         Case Else   'Default to Day
           GetDateDiffInterval = Chr(39) & "d" & Chr(39)
      End Select
   End If
   If Debug = 1 Then MsgBox "Provider = " & strProvider & vbcrlf & "strInterval = " & strInterval & vbcrlf & "GetDateDiffInterval = " & GetDateDiffInterval,,"GetDateDiffInterval"
End Function
'*************************************************************
'*************************************************************
'Log Error from SQL Queries
'
Sub SQLError(ErrorText)
   If Err.Number = -2147467259 Then Addendum = vbCrLf & "Try repairing the database and executing the purge script again."
   OutputLog Error, ErrorText & vbCrLf & Err.Source & "(" & Err.Number & ")" & ": " & Err.Description & Addendum
   If ClearData=1 Then MsgBox ErrorText & vbCrLf & Err.Source & "(" & Err.Number & ")" & ": " & Err.Description & Addendum,16,"Error"
   Err.Clear
End Sub
'*************************************************************
'*************************************************************
'Progress Bar Cancel Event Handler
'
Sub oPB_Cancel()
    ' set flag, and go about your business...
    sEvent = sCancelEvent
End Sub
'*************************************************************
'*************************************************************
'Progress Bar OnUnload Event Handler
'
Sub oPB_OnUnLoad()
    ' set flag, and go about your business...
    sEvent = sUnLoadEvent
End Sub
'*************************************************************
'*************************************************************
'Output specified text to log file
'
Sub OutputLog(EventID,OutputText)
   On Error Resume Next
   WshShell.LogEvent EventID, "IRIS(" & WScript.ScriptName & "):  " & OutputText
End Sub
'*************************************************************
'*************************************************************
' Exit the script with an error
'
Sub GiveUp()
   On Error Resume Next
   OutputLog Error,"Terminating script due to error."
   If UseProgressBar = 1 Then
      oPB.SetLine1 = "Exiting script with Error. "
      oPB.SetLine2 = " "
   End If
   ExitThisScript()
End Sub
'*************************************************************
'*************************************************************
' Exit the Script
'
Sub ExitThisScript()
   On Error Resume Next
   If CloseMsgScreen = 1 Then
      If MessageScreen.status = 0 Then MessageScreen.Terminate
      If Err.number Then
         OutputLog Error, "Unable to close Message Screen (EODMessage.exe)"
         Err.Clear
      End If
      Set MessageScreen = Nothing
   End If

   If UseProgressBar = 1 Then
      'Check on how we got here
      Select Case sEvent
         Case ""  ' no error, normal exit...
            oPB.Close
         Case sCancelEvent  ' cancel event, close after delay
            oPB.SetLine1 = "Script CANCELLED by user. "
            oPB.SetLine2 = " "
            oPB.Close
         Case sUnloadEvent  ' unload event, IE closed already, just exit
         Case sInternalErr  ' IE disconnect, direct exit
         Case Else
            DisplayMsgBox "Unexpected Event type error",16,0
      End Select
   End If

   Set WSHShell = Nothing
   Set iniEditor = Nothing
   Set fso = Nothing
   Set dbConn = Nothing
   Set oPB = Nothing
   WScript.Quit  ' end of script
End Sub

'' SIG '' Begin signature block
'' SIG '' MIIQ7QYJKoZIhvcNAQcCoIIQ3jCCENoCAQExDjAMBggq
'' SIG '' hkiG9w0CBQUAMGYGCisGAQQBgjcCAQSgWDBWMDIGCisG
'' SIG '' AQQBgjcCAR4wJAIBAQQQTvApFpkntU2P5azhDxfrqwIB
'' SIG '' AAIBAAIBAAIBAAIBADAgMAwGCCqGSIb3DQIFBQAEELxO
'' SIG '' DiizmWNpkAH57mR6tAGgggyQMIIDejCCAmKgAwIBAgIQ
'' SIG '' OCXX+vhhr570kOcmtdZa1TANBgkqhkiG9w0BAQUFADBT
'' SIG '' MQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNpZ24s
'' SIG '' IEluYy4xKzApBgNVBAMTIlZlcmlTaWduIFRpbWUgU3Rh
'' SIG '' bXBpbmcgU2VydmljZXMgQ0EwHhcNMDcwNjE1MDAwMDAw
'' SIG '' WhcNMTIwNjE0MjM1OTU5WjBcMQswCQYDVQQGEwJVUzEX
'' SIG '' MBUGA1UEChMOVmVyaVNpZ24sIEluYy4xNDAyBgNVBAMT
'' SIG '' K1ZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMg
'' SIG '' U2lnbmVyIC0gRzIwgZ8wDQYJKoZIhvcNAQEBBQADgY0A
'' SIG '' MIGJAoGBAMS18lIVvIiGYCkWSlsvS5Frh5HzNVRYNerR
'' SIG '' Nl5iTVJRNHHCe2YdicjdKsRqCvY32Zh0kfaSrrC1dpbx
'' SIG '' qUpjRUcuawuSTksrjO5YSovUB+QaLPiCqljZzULzLcB1
'' SIG '' 3o2rx44dmmxMCJUe3tvvZ+FywknCnmA84eK+FqNjeGkU
'' SIG '' e60tAgMBAAGjgcQwgcEwNAYIKwYBBQUHAQEEKDAmMCQG
'' SIG '' CCsGAQUFBzABhhhodHRwOi8vb2NzcC52ZXJpc2lnbi5j
'' SIG '' b20wDAYDVR0TAQH/BAIwADAzBgNVHR8ELDAqMCigJqAk
'' SIG '' hiJodHRwOi8vY3JsLnZlcmlzaWduLmNvbS90c3MtY2Eu
'' SIG '' Y3JsMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMA4GA1Ud
'' SIG '' DwEB/wQEAwIGwDAeBgNVHREEFzAVpBMwETEPMA0GA1UE
'' SIG '' AxMGVFNBMS0yMA0GCSqGSIb3DQEBBQUAA4IBAQBQxUvI
'' SIG '' JIDf5A0kwt4asaECoaaCLQyDFYE3CoIOLLBaF2G12AX+
'' SIG '' iNvxkZGzVhpApuuSvjg5sHU2dDqYT+Q3upmJypVCHbC5
'' SIG '' x6CNV+D61WQEQjVOAdEzohfITaonx/LhhkwCOE2DeMb8
'' SIG '' U+Dr4AaH3aSWnl4MmOKlvr+ChcNg4d+tKNjHpUtk2scb
'' SIG '' W72sOQjVOCKhM4sviprrvAchP0RBCQe1ZRwkvEjTRIDr
'' SIG '' oc/JArQUz1THFqOAXPl5Pl1yfYgXnixDospTzn099io6
'' SIG '' uE+UAKVtCoNd+V5T9BizVw9ww/v1rZWgDhfexBaAYMkP
'' SIG '' K26GBPHr9Hgn0QXF7jRbXrlJMvIzMIIDxDCCAy2gAwIB
'' SIG '' AgIQR78Zld+NUkZD99ttSA0xpDANBgkqhkiG9w0BAQUF
'' SIG '' ADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rl
'' SIG '' cm4gQ2FwZTEUMBIGA1UEBxMLRHVyYmFudmlsbGUxDzAN
'' SIG '' BgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhhd3RlIENl
'' SIG '' cnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1l
'' SIG '' c3RhbXBpbmcgQ0EwHhcNMDMxMjA0MDAwMDAwWhcNMTMx
'' SIG '' MjAzMjM1OTU5WjBTMQswCQYDVQQGEwJVUzEXMBUGA1UE
'' SIG '' ChMOVmVyaVNpZ24sIEluYy4xKzApBgNVBAMTIlZlcmlT
'' SIG '' aWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EwggEi
'' SIG '' MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpyrKk
'' SIG '' zM0grwp9iayHdfC0TvHfwQ+/Z2G9o2Qc2rv5yjOrhDCJ
'' SIG '' WH6M22vdNp4Pv9HsePJ3pn5vPL+Trw26aPRslMq9Ui2r
'' SIG '' SD31ttVdXxsCn/ovax6k96OaphrIAuF/TFLjDmDsQBx+
'' SIG '' uQ3eP8e034e9X3pqMS4DmYETqEcgzjFzDVctzXg0M5US
'' SIG '' mRK53mgvqubjwoqMKsOLIYdmvYNYV291vzyqJoddyhAV
'' SIG '' PJ+E6lTBCm7E/sVK3bkHEZcifNs+J9EeeOyfMcnx5iIZ
'' SIG '' 28SzR0OaGl+gHpDkXvXufPF9q2IBj/VNC97QIlaolc2u
'' SIG '' iHau7roN8+RN2aD7aKCuFDuzh8G7AgMBAAGjgdswgdgw
'' SIG '' NAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhhodHRw
'' SIG '' Oi8vb2NzcC52ZXJpc2lnbi5jb20wEgYDVR0TAQH/BAgw
'' SIG '' BgEB/wIBADBBBgNVHR8EOjA4MDagNKAyhjBodHRwOi8v
'' SIG '' Y3JsLnZlcmlzaWduLmNvbS9UaGF3dGVUaW1lc3RhbXBp
'' SIG '' bmdDQS5jcmwwEwYDVR0lBAwwCgYIKwYBBQUHAwgwDgYD
'' SIG '' VR0PAQH/BAQDAgEGMCQGA1UdEQQdMBukGTAXMRUwEwYD
'' SIG '' VQQDEwxUU0EyMDQ4LTEtNTMwDQYJKoZIhvcNAQEFBQAD
'' SIG '' gYEASmv56ljCRBwxiXmZK5a/gqwB1hxMzbCKWG7fCCmj
'' SIG '' XsjKkxPnBFIN70cnLwA4sOTJk06a1CJiFfc/NyFPcDGA
'' SIG '' 8Ys4h7Po6JcA/s9Vlk4k0qknTnqut2FB8yrO58nZXt27
'' SIG '' K4U+tZ212eFX/760xX71zwye8Jf+K9M7UhsbOCf3P0ow
'' SIG '' ggVGMIIELqADAgECAhEAsHgJfsXSrQMA7fgpGia4sjAN
'' SIG '' BgkqhkiG9w0BAQUFADCBlTELMAkGA1UEBhMCVVMxCzAJ
'' SIG '' BgNVBAgTAlVUMRcwFQYDVQQHEw5TYWx0IExha2UgQ2l0
'' SIG '' eTEeMBwGA1UEChMVVGhlIFVTRVJUUlVTVCBOZXR3b3Jr
'' SIG '' MSEwHwYDVQQLExhodHRwOi8vd3d3LnVzZXJ0cnVzdC5j
'' SIG '' b20xHTAbBgNVBAMTFFVUTi1VU0VSRmlyc3QtT2JqZWN0
'' SIG '' MB4XDTA5MDUxNDAwMDAwMFoXDTEyMDUxMzIzNTk1OVow
'' SIG '' gboxCzAJBgNVBAYTAlVTMQ4wDAYDVQQRDAUyODIyNjEL
'' SIG '' MAkGA1UECAwCTkMxEjAQBgNVBAcMCUNoYXJsb3R0ZTEu
'' SIG '' MCwGA1UECQwlMTE1MjUgQ2FybWVsIENvbW1vbnMgQmx2
'' SIG '' ZC4sIFN1aXRlIDEwMDEZMBcGA1UECgwQWFBJRU5UIFNv
'' SIG '' bHV0aW9uczEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGTAX
'' SIG '' BgNVBAMMEFhQSUVOVCBTb2x1dGlvbnMwggEiMA0GCSqG
'' SIG '' SIb3DQEBAQUAA4IBDwAwggEKAoIBAQC35Pl7Op1x200/
'' SIG '' nJxncnnhKXmPQw8T3KOIYnZqZ4m3lb+cvnF+d7SDy9jD
'' SIG '' dxbdM39WLyXWOpskG+LIViFlBV1KHTEsHgGkqwQpx5UT
'' SIG '' 5HVLMtNm0NYCWznFtYfVIqJs+tqz5lSQ2MUV4rSnstxm
'' SIG '' 0WRw4X9dneXe+zIrJ1W96UCw4ZLifsPbeqVYGpklgUmF
'' SIG '' xmFfwuPX/4qX0K6CXoqMRBZxg48qny2rv2vq+s0jscch
'' SIG '' sAyeZxjmWVUpxmYrZtyXCSNfVWd3UZsyZyDdl5YurfZx
'' SIG '' 28G1vjHr8WUuys8OX/FMgaRos8IUfDgdyP2LftB42HTh
'' SIG '' F/snyMik7PaMdv/wEAMtAgMBAAGjggFoMIIBZDAfBgNV
'' SIG '' HSMEGDAWgBTa7WR0FJwUPKvdmam9WyhNizzJ2DAdBgNV
'' SIG '' HQ4EFgQUeH7WhOp5HeJU1lLMlQxJBkArhykwDgYDVR0P
'' SIG '' AQH/BAQDAgeAMAwGA1UdEwEB/wQCMAAwEwYDVR0lBAww
'' SIG '' CgYIKwYBBQUHAwMwEQYJYIZIAYb4QgEBBAQDAgQQMEYG
'' SIG '' A1UdIAQ/MD0wOwYMKwYBBAGyMQECAQMCMCswKQYIKwYB
'' SIG '' BQUHAgEWHWh0dHBzOi8vc2VjdXJlLmNvbW9kby5uZXQv
'' SIG '' Q1BTMEIGA1UdHwQ7MDkwN6A1oDOGMWh0dHA6Ly9jcmwu
'' SIG '' dXNlcnRydXN0LmNvbS9VVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dC5jcmwwNAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzAB
'' SIG '' hhhodHRwOi8vb2NzcC5jb21vZG9jYS5jb20wGgYDVR0R
'' SIG '' BBMwEYEPaW5mb0B4cGllbnQuY29tMA0GCSqGSIb3DQEB
'' SIG '' BQUAA4IBAQBvSaL3UXO313UuGSpKCtggrg83wHa4XmlM
'' SIG '' 4LaUpCekcEBSFvVnBoxuGGR12GLgBkkLOVgPUnTsoH8G
'' SIG '' qvwcsTa11kQAV8XZ23K/Bk+c6MV3VeHa03V5kdf+2qKY
'' SIG '' VVadxQdrry8Dw2nvbuNZvBA1nMhB+3BQfSo2nTqIhahp
'' SIG '' MyY6FPXL2bemF/JWVAflFwyrdaInUueimSfBPu8ubQX1
'' SIG '' vgBOANoE33nQ8kXS1IhRsuB4xAjE9N4ZLwdr4rGkgstO
'' SIG '' xAVseaEUePSt+yWGMsyD9C/2XX4aG5Yp6wemvOrMF59G
'' SIG '' oJ7Uij5WrvxUJ4JNMuX22ywuQ6zZC1sCbVo1kYAagm9q
'' SIG '' MYIDxzCCA8MCAQEwgaswgZUxCzAJBgNVBAYTAlVTMQsw
'' SIG '' CQYDVQQIEwJVVDEXMBUGA1UEBxMOU2FsdCBMYWtlIENp
'' SIG '' dHkxHjAcBgNVBAoTFVRoZSBVU0VSVFJVU1QgTmV0d29y
'' SIG '' azEhMB8GA1UECxMYaHR0cDovL3d3dy51c2VydHJ1c3Qu
'' SIG '' Y29tMR0wGwYDVQQDExRVVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dAIRALB4CX7F0q0DAO34KRomuLIwDAYIKoZIhvcNAgUF
'' SIG '' AKBsMBAGCisGAQQBgjcCAQwxAjAAMBkGCSqGSIb3DQEJ
'' SIG '' AzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAM
'' SIG '' BgorBgEEAYI3AgEVMB8GCSqGSIb3DQEJBDESBBCaD3aM
'' SIG '' n/7bITqFlpaVjb+VMA0GCSqGSIb3DQEBAQUABIIBABse
'' SIG '' xtW+G0RkdtgjzvDMm6anbDm2xznQVXZnt562ly8eEzyj
'' SIG '' whiHhaIgLlSyZzPuHzkVAX6Q4KymYnfm5GuJAAlRiymD
'' SIG '' UWL9FAGAZ0PPCM/+Jbyy6dD/sGJWzpTKNzhvrMEkIAMD
'' SIG '' clydo5H41eGW7rBt0eRBTYukmYznQrIvNHilkbiEW6Wh
'' SIG '' XcfFUYRN9xgaEKNW/OpILJZTuCPEwfi/Jg/DhWBUmgTv
'' SIG '' qlWzlOtP4lgP/v1c7dczZzbm+PqDt4DULrdvdGOo2Vb6
'' SIG '' iB03GFJK1FIKW9y6XVCxFThdG4/CJPWyQe/EbILDl0Je
'' SIG '' D8ngjZsLf9dbITMR9h5SLGvHZr1siH6hggF/MIIBewYJ
'' SIG '' KoZIhvcNAQkGMYIBbDCCAWgCAQEwZzBTMQswCQYDVQQG
'' SIG '' EwJVUzEXMBUGA1UEChMOVmVyaVNpZ24sIEluYy4xKzAp
'' SIG '' BgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2Vy
'' SIG '' dmljZXMgQ0ECEDgl1/r4Ya+e9JDnJrXWWtUwCQYFKw4D
'' SIG '' AhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEw
'' SIG '' HAYJKoZIhvcNAQkFMQ8XDTExMDQwNDIzNTE1MVowIwYJ
'' SIG '' KoZIhvcNAQkEMRYEFKLkM46gb7/j1A6d/U0M/++b1yzi
'' SIG '' MA0GCSqGSIb3DQEBAQUABIGARn/mghak4y557c7suZib
'' SIG '' tA7WYYEc31HlOYdc51+PpEq3BEOgPO1siz8aFuqap00+
'' SIG '' q2MehO//CiL0svQXo7zBxit9djP5y4tuuVWGGoTi47rd
'' SIG '' dH1r/B2EkELNum/e33o1IESn0qaWZ4OENXWyMvr/SdEF
'' SIG '' tFT8eoejbZ6yp/dHb+Q=
'' SIG '' End signature block
