;***************************************************************************
;
; FILENAME:    PRTDEV.ini
;
; DATE:        03/17/03
;
; AUTHOR:      Bill Starr
;
; COPYRIGHT:   (C) 1995-1996 Progressive Software, Inc.  All rights reserved.
;
; NOTICE:      All Copyright notices contained within this source code must
;              remain in place and may not be removed or modified.
;
; DESCRIPTION: This file contains the definitions for the printers on the
;              system.
;
;***************************************************************************
;
;
; Printer definition
;  port = Communications port to use
;  type = typeof printer (TMT80 or TM300)
;  speed = baud rate of printer
;  cash  = if cash drawer is attached to printer, give it a name
;  cashclosed = set to 1 to reverse closed/open message
;************************************************************


[rcpt1]
port=com2
type=TMT80
speed=19200
Logging=0
LogFileName=C:\Iris\log\rcpt1.log
LogFilter=80

[rcpt2]
port=com2
type=TMT80
speed=19200
Logging=0
LogFileName=C:\Iris\log\rcpt2.log
LogFilter=80

[rcpt3]
port=com2
type=TMT80
speed=19200
Logging=0
LogFileName=C:\Iris\log\rcpt3.log
LogFilter=80

[rcpt5]
port=com2
type=TMT80
speed=19200
Logging=0
LogFileName=C:\Iris\log\rcpt3.log
LogFilter=80

[rcpt6]
port=com2
type=TMT80
speed=19200
Logging=0
LogFileName=C:\Iris\log\rcpt6.log
LogFilter=80



[grill]
CPUTimerDelay=900 
traceout=0
deletedelay=5000
;alwayspromptonrr=0
PARENT=1
separategrillitems=0
processdels=1
addcashtips=1
addchargetips=1
CheckTime=120
COUNT=0
Logging=0
LogFileName=C:\Iris\dev\pserver.log
LogFilter=80
AlwaysPrintSeat=1
PostTicketLineFeed=7

[OCS]
Type=CKE Delphi
port=com2
speed=19200
Parity=e
DataBits=8
StopBits=1
HandShake=None
Logging=1
LogFilter=127
LogfileName=..\Log\Cke_ocs.log
NoOrderActivityTimeout=300000

[cdsp6]
port=com2
type=AccuView
speed=9600
greettime=-1
totaltime=0
consoldation=0
quantity=1
quantitywidth=3
descriptionwidth=12
amountwidth=10
maxrow=15
orderfont=1
orderitemcolor=ffffff000090
taxtotalrowscolor=ffffff000000
OrderItemHoldTime=0
ModiferHoldTime=0
OrderScreenHoldTime=0
ExitHoldTime=0
ResponseTime=10000
Logging=0
LogFileName=c:\iris\dev\accuview.log
logfilter=17


[delphi]
port=com2
speed=19200
pause=450
Logging=1
LogFilename=c:\iris\log\delphi.log
LogFilter=127
CPUTimerDelay=900
IndentChild=1
AutoAbortTime=0
StatusMonitoring=1
StatusFile=c:\iris\log\StatusFile.log
OCSCommTimeout=25000
UploadFileListCopy=c:\Iris\OCS\UploadUpd\OcsFilesUpd.mst
WinSockMessage=c:\iris\ocs\UploadUpd\WinSockMsg.txt


[JobCode]
JobFromLS=0    ; 0: job code not from labor scheduling, 1: job code from labor scheduling.

[Printers]
BusinessDate=010327
