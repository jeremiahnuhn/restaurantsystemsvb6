'##### Start check for x64 System #####
'This will check for a x64 System and restart the script using the 32bit version if needed
'xsCheckArchitecture v1.0
Sub xsCheckArchitecture
   On Error Resume Next
   If Not IsObject(WScript) Then Exit Sub
   Dim oWshShell, oWshProcEnv, oExec, Argument, sCmd, iResult
   Set oWshShell =  CreateObject("WScript.Shell")
   Set oWshProcEnv = oWshShell.Environment("Process")
   If LCase(oWshProcEnv("PROCESSOR_ARCHITECTURE")) <> "x86" and Instr(1, WScript.FullName, "SysWOW64", 1) = 0 then
      sCmd = Chr(34) & oWshProcEnv("SystemRoot") & "\SysWOW64\" &  Mid(WScript.FullName, InstrRev(WScript.FullName, "\") + 1) & Chr(34)
      sCmd = sCmd & " //NoLogo " & Chr(34) & WScript.ScriptFullName & Chr(34)
      For Each Argument in WScript.Arguments
         Argument = Split(Argument,"=",2)
         sCmd = sCmd & " " & Chr(34) & Argument(0) & Chr(34)
         If UBound(Argument) = 1 Then sCmd = sCmd & "=" & Chr(34) & Argument(1) & Chr(34)
      Next
      Set oExec = oWshShell.Exec(sCmd)
      Do While oExec.Status = 0
         Do While Not oExec.StdOut.AtEndOfStream
            WScript.StdOut.Write oExec.StdOut.Read(1)
         Loop
      Loop
      Do While Not oExec.StdErr.AtEndOfStream
         WScript.StdErr.Write oExec.StdErr.Read(1)
      Loop
      If Not oExec.ExitCode = 0  then WScript.Quit oExec.ExitCode
      WScript.quit
   End If
End Sub
xsCheckArchitecture
'##### End check for x64 System #####
Dim oFSO
Dim oWSHShell
Dim ResultCode
Dim g_IrisPath
Dim g_LogFile
Dim g_QuietMode
Dim DebugScript

DebugScript = False

Call ProcessCommandLine()
Call SetScriptMode(g_QuietMode)

Set oWSHShell = CreateObject("WScript.Shell")
Set oFSO=CreateObject("Scripting.FileSystemObject")

If DebugScript = False Then On Error Resume Next
ResultCode=0

g_IrisPath = oWSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")
If g_IrisPath = "" Or Err.Number Then
   g_IrisPath = "C:\IRIS"
   Err.Clear
End If

LogMsg "StopServices.vbs started..."

'Create Services object
Set WMIService = GetObject("winmgmts:\\.\root\cimv2")

'Get List of Services
Set ServiceList = WMIService.ExecQuery ("Select * from Win32_Service Where State <> 'Stopped'")
For each Service in ServiceList
   If DebugScript = True Then LogMsg "Looking at (" &  Service.Name & ") " & Service.PathName
   
   XpientService = False
   ServicePath = ""
   
   If InStr(1,Service.PathName, "srvany.exe",1) > 0  Then
      ServicePath = oWSHShell.RegRead ("HKLM\SYSTEM\CurrentControlSet\Services\" & Service.Name & "\Parameters\Application")
      Err.Clear
   Else
      ServicePath = Service.PathName
   End If

   If InStr(1,Service.PathName, "\wkserver.exe",1) > 0  Then
      XpientService = True
   End If

   'Paytronix loyalty Service
   If InStr(1,Service.PathName, "\PXController.exe",1) > 0  Then
      XpientService = True
   End If

   'Snare event log monitor
   If InStr(1,Service.PathName, "\SnareCore.exe",1) > 0  Then
      XpientService = True
   End If
   
   If InStr(1,ServicePath, g_IrisPath & "\bin\",1) > 0 Or InStr(1,ServicePath, "\Xpient Solutions\",1) > 0 Or InStr(1,ServicePath, "\Progressive Software\",1) > 0 Then
      XpientService = True
   End If
   
   If XpientService = True Then
      LogMsg "Stopping " & Service.Name & "..."
      errReturn = Service.StopService()
      If errReturn <> 0 Then
         LogMsg "Failed to stop " & Service.Name & "  ErrorCode=" & ServiceErrorText(errReturn)
      End if
      For WaitTimer = 10 to 0 Step -1
         wscript.sleep 1000
         Set WMIProcess = WMIService.ExecQuery("Select * from Win32_Process where ProcessID = " & Service.ProcessId)
         If WMIProcess.Count = 0 Then Exit For
      Next
      If WaitTimer = -1 Then
         LogMsg "Failed to stop " & Service.Name
         LogMsg "Killing " & Service.Name & "(" & Service.ProcessId & ")..."
         set objWMIProcess = GetObject("winmgmts:{impersonationlevel=impersonate,(debug)}\\.\root\cimv2:Win32_Process.Handle='" & Service.ProcessId & "'")
         errReturn = objWMIProcess.Terminate()
         If errReturn <> 0 Then
            LogMsg "Failed to kill " & Service.Name & "  ErrorCode=" & TerminateErrorText(errReturn)
            ResultCode=1
         End if
      End If
   End If
Next

If Err Then
   ResultCode=1
End If

'Clean up and exit
Set oLog = Nothing
Set oFSO = Nothing
Set oWSHShell = Nothing

Wscript.Quit ResultCode

Sub LogMsg (Message)
   If DebugScript = False Then On Error Resume Next
   PrintMsg (Message)
   Dim oWSHShell, oFSO, sLogFile, oLog
   Set oWSHShell = CreateObject("WScript.Shell")
   Set oFSO=CreateObject("Scripting.FileSystemObject")
   If Not g_IrisPath = "" Then
      sLogFile = g_IrisPath & "\Log\StopServices.log"
   End If
   If Not sLogFile = "" Then
      Set oLog = oFSO.OpenTextFile(sLogFile, 8, True)
      oLog.WriteLine CStr(Now()) & " " & Message
      If Err.Number Then
         sLogFile = ""
         Err.Clear
      End If
   End If
End Sub

Sub PrintMsg (Message)
   If DebugScript = False Then On Error Resume Next
   Wscript.stdout.WriteLine Message
   If Err.Number Then
      Err.Clear
   End If
End Sub

Function ServiceErrorText(err)
   Select Case err
      Case 0
         ServiceErrorText = "Success"
      Case 1
         ServiceErrorText = "Not supported"
      Case 2
         ServiceErrorText = "Access denied"
      Case 3
         ServiceErrorText = "Dependent services running"
      Case 4
         ServiceErrorText = "Invalid service control"
      Case 5
         ServiceErrorText = "Service cannot accept control"
      Case 6
         ServiceErrorText = "Service not active"
      Case 7
         ServiceErrorText = "Service request timeout"
      Case 8
         ServiceErrorText = "Unknown failure"
      Case 9
         ServiceErrorText = "Path not found"
      Case 10
         ServiceErrorText = "Service already stopped"
      Case 11
         ServiceErrorText = "Service database locked"
      Case 12
         ServiceErrorText = "Service dependency deleted"
      Case 13
         ServiceErrorText = "Service dependency failure"
      Case 14
         ServiceErrorText = "Service disabled"
      Case 15
         ServiceErrorText = "Service logon failed"
      Case 16
         ServiceErrorText = "Service marked for deletion"
      Case 17
         ServiceErrorText = "Service no thread"
      Case 18
         ServiceErrorText = "Status circular dependency"
      Case 19
         ServiceErrorText = "Status duplicate name"
      Case 20
         ServiceErrorText = "Status - invalid name"
      Case 21
         ServiceErrorText = "Status - invalid parameter"
      Case 22
         ServiceErrorText = "Status - invalid service account"
      Case 23
         ServiceErrorText = "Status - service exists"
      Case 24
         ServiceErrorText = "Service already paused"
      Case Else
         ServiceErrorText = CStr(err)
   End Select
End Function

Function TerminateErrorText(err)
   Select Case err
      Case 0
         TerminateErrorText = "Successful completion"
      Case 2
         TerminateErrorText = "Access denied"
      Case 3
         TerminateErrorText = "Insufficient privilege"
      Case 8
         TerminateErrorText = "Unknown failure"
      Case 9
         TerminateErrorText = "Path not found"
      Case 21
         TerminateErrorText = "Invalid parameter"
      Case Else
         TerminateErrorText = CStr(err)
   End Select
End Function

'Process Command line
Sub ProcessCommandLine()
   Dim Argument
   For Each Argument in WScript.Arguments
      Argument = Split(Argument,"=",2)
      If UBound(Argument) = 1 Then
         Select Case LCase(Argument(0))
            Case "/log"
               g_LogFile = Argument(1)
         End Select
      Else
         Select Case LCase(Argument(0))
            Case "/q"
               g_QuietMode = True
         End Select
      End If
   Next
End Sub

Sub SetScriptMode(bQuiteMode)
   If DebugScript = False Then On Error Resume Next
   Dim Argument, Cmd
   Dim oWSHShell: Set oWSHShell = CreateObject("WScript.Shell")
   If Not bQuiteMode Then 'If not Quiet Mode run with CScript
      If lCase(Right(Wscript.FullName, 11)) = "wscript.exe" Then
         Cmd = Chr(34) & Wscript.Path & "\cscript.exe" & Chr(34) & " //NoLogo " & Chr(34) & Wscript.ScriptFullName & Chr(34)
         For Each Argument in WScript.Arguments
            Argument = Split(Argument,"=",2)
            Cmd = Cmd & " " & Chr(34) & Argument(0) & Chr(34)
            If UBound(Argument) = 1 Then Cmd = Cmd & "=" & Chr(34) & Argument(1) & Chr(34)
         Next
         oWSHShell.Run Cmd, 1, True
         Wscript.Quit
      End If
   End If
End Sub
