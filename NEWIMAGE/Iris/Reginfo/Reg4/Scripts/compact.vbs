' ------------------------------------------------------------------------
'               Copyright (C) 2005 xpient Solutions, Inc.
'
'                         Compact / Repair Script
'
' Author:  Jim Davidson
' Date: 3/11/2002
'
' 11/4/10 This script should only be used on Terminal 4.
'         Terminal 4 will not try to compact and repair POSLIVE on Terminal 3.
' ********************************************************************************

' COM Objects
Dim WSHShell
Dim fso
Dim jro

'Global Variables
Dim irisPath
Dim showProgress
Dim result
Dim PreScript
Dim PostScript
Dim DBList
Dim DBPath
Dim DB1Path
Dim PurgeData
Dim PurgePath
Dim MachineType

'Constants
Const Success = 0
Const Error = 1
Const Warning = 2
Const Info = 4

'*************************************************************
' THIS IS THE MAIN SCRIPT LOGIC. 
'
' We exit if any step fails, with a status of 1.  If everything
' succeeds, our exit status is 0.
'*************************************************************
   showProgress=0  'Make this 1 to display a messagebox before performing each step.
                   'Otherwise, 0.

   On Error Resume Next

  'Create the COM objects we will use in this script
   If showProgress Then MsgBox "Creating COM Objects"
   result=createCOMObjects()
   If result<>0 Then ExitThisScript()

  'Log beginning Of script
   If showProgress Then MsgBox "Log start of script"
   OutputLog Info,"Executing Compact/Repair script."

  'Parse command line
   If showProgress Then MsgBox "Parsing command line"
   result=ParseCommandLine()
   If result<>0 Then GiveUp()
	
  'Get the IRIS directory path
   If showProgress Then MsgBox "Determining IRIS path"
   result=getIRISDirectory()
   If result<>0 Then GiveUp()

  'Run Pre-Compact script if it exists
   PreScript = irispath & "\Scripts\PreCompact.vbs"
   If fso.FileExists(PreScript) Then
      OutputLog Info,"Launching Pre-Compact script: " & PreScript
      WSHShell.Run PreScript,,true
   End If

  'Repair and Compact Databases
   If showProgress Then MsgBox "Repairing and Compacting databases"
   result = CompactDB()
   If result = 0 Then WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\Compact",0

'  'Run Purge Data script if specified
'   If showProgress Then MsgBox "Determining whether to launch Purge Data script."
'   result=RunPurgeScript()
'   If result<>0 Then GiveUp()

   'Run Post-Compact script if it exists
    PostScript = irispath & "\Scripts\PostCompact.vbs"
    If fso.FileExists(PostScript) Then
       OutputLog Info,"Launching Post-Compact script: " & PostScript
       WSHShell.Run PostScript,,true
    End If

   'End the script with success
    OutputLog Success,"Compact script is complete."
    ExitThisScript()

'*************************************************************
' THIS IS THE END OF THE MAIN SCRIPT LOGIC
'*************************************************************
'
'*************************************************************
' Create the COM Objects we will use in this script
'
Function CreateCOMObjects()
   CreateCOMObjects=1	'Assume failure

   On Error Resume Next

   Set WSHShell = WScript.CreateObject("WScript.Shell")
   If Err.Number Then
      ExitThisScript()
   End If

   Set fso = CreateObject("Scripting.FileSystemObject")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Unable to create File System object (fso)."
      Exit Function
   End If

   Set jro  = CreateObject("jro.JetEngine")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Unable to create Jet Engine object (jro)."
      Exit Function
   End If

   CreateCOMObjects = 0	'Success
End Function
'*************************************************************
'*************************************************************
'Parse command line
'
Function ParseCommandLine()
   ParseCommandLine=0    'Assume success

   On Error Resume Next

   Set objArgs = WScript.Arguments
   For I = 0 to objArgs.Count - 1
      If Instr(objArgs(I),"=") Then
      Else
      End If
   Next

End Function
'*************************************************************
'*************************************************************
'Get the IRIS directory and version from the registry
'
Function GetIRISDirectory()
   GetIRISDirectory = 0 'Assume success

   On Error Resume Next

   irisPath=WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")
   If Err.Number Then
      OutputLog Error,"GetIRISDirectory(): IRIS Path is missing from registry: HKEY_LOCAL_MACHINE\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR"
      GetIRISDirectory=1
   End If

   MachineType = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\BOOrPOS")
   If Instr(Lcase(MachineType),"pos") OR Instr(Lcase(MachineType),"register") Then
      MachineType = "Register"
   Else
      MachineType = "Server"
   End If

End Function
'*************************************************************
'*************************************************************
'Repair and Compact databases
'
Function CompactDB()
   CompactDB = 0   'Assume success

   On Error Resume Next

   If MachineType = "Register" Then
      DBList = Split("POSPend,Kitchen",",")
   Else
      DBList = Split("POSLive,POSPend,Kitchen,FlexPoll,Inventory,Report,Xception,POSTrans",",")
   End If

   For i = 0 To UBound(DBList)
      Retry = 3
      Do While Retry > 0
         If showProgress Then MsgBox DBList(i)
         DBError = 0
         DBPath = irispath & "\data\" & DBList(i) & ".mdb"
         DB1Path = irispath & "\data\" & DBList(i) & "cr1.mdb"
         If fso.FileExists(DBPath) Then
            RemoveReadOnly(DBPath)
            If fso.FileExists(DB1Path) Then
               RemoveReadOnly(DB1Path)
               fso.DeleteFile(DB1Path)
               If Err.Number Then
                  OutputLog Error,"CompactDB(): Error attempting to delete former compacted temp file: " & DB1Path & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
                  Err.Clear
               End If
            End If
            jro.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBPath, "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DB1Path & ";Jet OLEDB:Engine Type=4"
            If Err.Number Then
               ErrorText = Err.Description
               OutputLog Error,"CompactDB(): Error repairing and compacting " & DBPath & " to " & DB1Path & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
               If Instr(LCASE(ErrorText),"unrecognized") Then BackupFile DBPath
               Err.Clear
               'Attempt to repair using DBRepair.exe
               DBRepairEXE = irispath & "\Bin\DBRepair.exe"
               If fso.FileExists(DBRepairEXE) Then
                  OutputLog Warning, "Launching DBRepair.exe to attempt corrupt database repair.  Result of this operation will be unknown.  This script will continue."
                  CmdLine = DBRepairEXE & " " & DBList(i) & ".mdb compact"
                  WSHShell.CurrentDirectory = irispath & "\bin"
                  WSHShell.Run CmdLine,,true
               Else
                  OutputLog Error,"CompactDB(): Error repairing and compacting " & DBPath & ".  DBRepair.exe could not be located to perform repair."
                  DBError = 1
                  Err.Clear
               End If
            Else
               If fso.FileExists(DB1Path) Then
                  fso.DeleteFile(DBPath)
                  If Err.Number Then
                     OutputLog Error,"CompactDB(): Error attempting to delete " & DBPath & " before moving successfully compacted temp file." & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
                     DBError = 1
                     Err.Clear
                  Else
                     fso.MoveFile DB1Path,DBPath
                     If Err.Number Then
                        OutputLog Error,"CompactDB(): Error moving successfully compacted " & DB1Path & " file to " & DBPath & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
                        DBError = 1
                        Err.Clear
                     Else
                        Exit Do
                     End If
                  End If
               Else
                  OutputLog Error,"Failed to compact and repair " & DBPath
                  DBError = 1
               End If
            End If
         Else
            If fso.FileExists(DB1Path) Then
               fso.MoveFile DB1Path,DBPath
            End If
            OutputLog Error,DBPath & " does not exist.  Unable to purge and compact the database."
            DBError = 1
         End If
         Retry = Retry - 1
         WScript.Sleep 5000
      Loop
      If DBError = 1 Then CompactDB = 1
   Next
	
End Function
'*************************************************************
'*************************************************************
'Run Purge script if specified in registry
'
Function RunPurgeScript()
   RunPurgeScript = 0   'Assume success

   On Error Resume Next

   PurgePath = irispath & "\Scripts\PurgeData.vbs"
   PurgeData = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\Purge")
   If PurgeData = "1" Then
      OutputLog Info,"Launching Purge Data script."
      If fso.FileExists(PurgePath) Then
         WSHShell.Run PurgePath,,true
      Else
         OutputLog Error,"Unable to locate " & PurgePath & ".  Purge script cannot be executed."
      End If
   End If
End Function
'*************************************************************
'*************************************************************
'Remove read-only attribute from specified file
'
Sub RemoveReadOnly(FilePath)
   On Error Resume Next
   Dim oFile

   If Not fso.FileExists(FilePath) Then Exit Sub

   Set oFile = fso.GetFile(FilePath) 
   If Err.Number Then
      OutputLog Error,"RemoveReadOnly(" & FilePath & "): Error setting file object for specified file." & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Sub
   End If

   If oFile.attributes and 1 Then
      OutputLog Warning,"RemoveReadOnly(" & FilePath & "): Read-only attribute is set on specified file...Removing before compacting."
      Err.Clear
      oFile.Attributes = oFile.Attributes - 1
      If Err.Number Then OutputLog Error,"RemoveReadOnly(" & FilePath & "): Error removing read-only attribute from specified file." & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
   End If

   Set oFile = Nothing

End Sub
'*************************************************************
'*************************************************************
'Backup specified file
'
Sub BackupFile(FilePath)
   On Error Resume Next
   Dim oFile
   If Not fso.FileExists(FilePath) Then Exit Sub

   Set oFile = fso.GetFile(FilePath) 

   DestFile = irispath & "\Log\UnrecognizedMDB-" & Year(Now) & PadZero(Month(Now),2) & PadZero(Day(Now),2) & "-" & PadZero(Hour(Now),2) & PadZero(Minute(Now),2) & PadZero(Second(Now),2) & "-" & oFile.Name
   If Err.Number Then
      OutputLog Error,"BackupFile(" & FilePath & "): Error determining name of destination file." & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Sub
   End If

   OutputLog Info,"BackupFile(" & FilePath & "): Backing up suspected corrupt database to " & DestFile
   fso.CopyFile FilePath,DestFile
   If Err.Number Then
      OutputLog Error,"BackupFile(" & FilePath & "): Error copying " & FilePath & " to " & DestFile & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Sub
   End If

   Set oFile = Nothing

End Sub

'*************************************************************
'*************************************************************
'Add leading zeroes to insure output equals number of characters passed in
'
Function PadZero(n,nLength)
   On Error Resume Next
   PadZero = n

   While Len(PadZero) < nLength
      PadZero = "0" & PadZero
   wend

End Function
'*************************************************************
'*************************************************************
'Output specified text to log file
'
Sub OutputLog(EventID,OutputText)
   On Error Resume Next
   WshShell.LogEvent EventID, "IRIS(" & WScript.ScriptName & "):  " & OutputText
End Sub
'*************************************************************
'*************************************************************
' Exit the script with an error
'
Sub GiveUp()
   On Error Resume Next
   OutputLog Error,"Terminating script due to error."
   ExitThisScript()
End Sub
'*************************************************************
'*************************************************************
' Exit the Script
'
Sub ExitThisScript()
   On Error Resume Next

   WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\LastCompactExecution",Now()

   Set WSHShell = Nothing
   Set fso = Nothing
   Set jro  = Nothing
   WScript.Quit    'End of script
End Sub