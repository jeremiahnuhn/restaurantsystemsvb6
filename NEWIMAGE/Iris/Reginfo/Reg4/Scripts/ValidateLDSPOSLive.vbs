' COM Objects
Dim WSHShell
Dim iniEditor
Dim fso 
Dim dbConn

'Global Variables
Dim irisPath
Dim result
Dim MessageScreen
Dim CloseMsgScreen
Dim BusinessDate
Dim POSLiveBusDate
Dim MachineType
Dim POSLiveDB

'Constants
Const Success = 0
Const Error = 1
Const Warning = 2
Const Info = 4

'*************************************************************
' THIS IS THE MAIN SCRIPT LOGIC. Edited  
'
'*************************************************************

'Create the COM objects we will use in this script
result=createCOMObjects()
If result<>0 Then ExitThisScript()

'Get the IRIS directory path and machine type
result=getIRISDirectory()
If result<>0 Then GiveUp()

If MachineType="Register" Then

   'Display html message
   result=OpenMessageScreen()

   'Get Business Date
   result=GetBusinessDate()
   If result<>0 Then GiveUp()

   'Get POSLive path
   result=GetPOSLivePath()
   If result<>0 Then GiveUp()

   'Get POSLive Business Date
   result=GetPOSLiveDate()
   If result<>0 Then GiveUp()

End If

'End the script with success
ExitThisScript()

'*************************************************************
' THIS IS THE END OF THE MAIN SCRIPT LOGIC
'*************************************************************
'
'*************************************************************
' Create the COM Objects we will use in this script
'
Function createCOMObjects()
   CreateCOMObjects=1	'Assume failure

   On Error Resume Next

   Set WSHShell = WScript.CreateObject("WScript.Shell")
   If Err.Number Then
      Exit Function
   End If

   Set iniEditor = CreateObject("IRISIniEditor.IniEditor.1")
   If Err.Number Then
      Exit Function
   End If

   Set fso = CreateObject("Scripting.FileSystemObject")
   If Err.Number Then
      Exit Function
   End If

   Set dbConn = CreateObject("ADODB.Connection")
   If Err.Number Then
      Exit Function
   End If

   CreateCOMObjects = 0	'Success
End Function
'*************************************************************
'*************************************************************
'Get the IRIS directory and machine type
'
Function GetIRISDirectory()
   GetIRISDirectory=0 'Assume success
   On Error Resume Next
   irisPath=WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")
   If Err.Number Then
      OutputLog Error,"IRIS Path is missing from registry: HKEY_LOCAL_MACHINE\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR"
      GetIRISDirectory=1
   End If

   MachineType = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\BOOrPOS")
   If Instr(Lcase(MachineType),"pos") OR Instr(Lcase(MachineType),"register") Then
      MachineType = "Register"
   Else
      MachineType = "Server"
   End If

End Function
'*************************************************************
'*************************************************************
'Open html page
'
Function OpenMessageScreen()

   On Error Resume Next

   CloseMsgScreen = 0

   If WScript.Version >= 5.6 Then

      IExplorer = WSHShell.RegRead ("HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\IEXPLORE.EXE\Path")
      IExplorer = Left(IExplorer,Len(IExplorer) - 1)
      If Right(IExplorer,1) <> "\" Then IExplorer = IExplorer & "\"
      If fso.FileExists(IExplorer & "IEXPLORE.exe") Then
         Set MessageScreen = WSHShell.Exec(IExplorer & "IEXPLORE.exe -k " & irispath & "\Scripts\WaitForLDS.htm")
         CloseMsgScreen = 1
      Else
         OutputLog Warning, "Unable to locate IExplore.exe at " & IExplorer & "IExplore.exe.  Html page will not be displayed during script."
      End If
   Else
      OutputLog Warning, "Message screen can only be displayed with Windows Scripting 5.6 or above."
   End If
End Function
'*************************************************************
'*************************************************************
'Get Business Date
'
Function GetBusinessDate()
   GetBusinessDate = 0   'Assume success

   On Error Resume Next
   
   Dim rsBusinessDate

   dbConn.Open "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & irispath & "\data\poslive.mdb"
   Set rsBusinessDate = CreateObject ("ADODB.Recordset")
   rsBusinessDate.Open "SELECT * FROM tblBusinessDate",dbConn,1,2     
   If rsBusinessDate.EOF = 0 Then
      BusinessDate = rsBusinessDate.Fields("BusinessDate")
   Else
      BusinessDate = Date
   End If
   rsBusinessDate.close

   dbConn.close
End Function
'*************************************************************
'*************************************************************
'Get path to POSLive.mdb from dataserv.ini
'
Function GetPOSLivePath()
   GetPOSLivePath = 0   'Assume success

   On Error Resume Next

   iniEditor.iniFilePath = irisPath & "\ini\dataserv.ini"
   iniEditor.section = "LDS"
   iniEditor.key = "POSTRANS"
   POSLiveDB = iniEditor.value
   If POSLiveDB = "" Then POSLiveDB = irispath & "\data\POSLive.mdb"
   If Instr(POSLiveDB,"..") Then POSLiveDB = Replace(POSLiveDB,"..",irispath)

End Function
'*************************************************************
'*************************************************************
'Get business date from POSLive database referenced in Dataserv.ini
'
Function GetPOSLiveDate()
   GetPOSLiveDate = 0   'Assume success

   On Error Resume Next

   Dim rsBusinessDate, GoodDate, AttemptNum, ErrorText

   GoodDate = 0
   AttemptNum = 1
   
   While GoodDate = 0
      GoodDate = 1
      ErrorText = ""
      Err.Clear

      dbConn.Open "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & POSLiveDB
      If Err.Number Then
         GoodDate = 0
         If ErrorText = "" Then ErrorText = Err.Description
         Err.Clear
      End If

      Set rsBusinessDate = CreateObject ("ADODB.Recordset")
      If Err.Number Then
         GoodDate = 0
         If ErrorText = "" Then ErrorText = Err.Description
         Err.Clear
      End If

      rsBusinessDate.Open "SELECT * FROM tblBusinessDate",dbConn,1,2
      If Err.Number Then
         GoodDate = 0
         If ErrorText = "" Then ErrorText = Err.Description
         Err.Clear
      End If

      If rsBusinessDate.EOF = 0 Then
         POSLiveBusDate = rsBusinessDate.Fields("BusinessDate")
         If Err.Number Then
            GoodDate = 0
            If ErrorText = "" Then ErrorText = Err.Description
            Err.Clear
         End If
      Else
         GoodDate = 0
         If ErrorText = "" Then ErrorText = "There are no records in tblBusinessDate in " & POSLiveDB & "."
      End If
      rsBusinessDate.close

      dbConn.close

      If IsDate(BusinessDate) and IsDate(POSLiveBusDate) Then
         If DateDiff("d",BusinessDate,POSLiveBusDate) < 0 Then
            GoodDate = 0
            If ErrorText = "" Then ErrorText = "Business Date in LDS POSLive database is behind local business date." & vbCrLf & "Local business date: " & BusinessDate & vbCrLf & "POSLiveDB business date: " & POSLiveBusDate
         End If
      Else
         GoodDate = 0
         If ErrorText = "" Then ErrorText = "One or both of the business date values retrieved is not a valid date:" & vbCrLf & "Local business date: " & BusinessDate & vbCrLf & "POSLiveDB business date: " & POSLiveBusDate
      End If

      If GoodDate = 0 Then
         If AttemptNum = 1 OR (AttemptNum mod 60) = 0 Then OutputLog Error,"POS cannot be launched until business date in " & POSLiveDB & " is greater than or equal to the local business date (" & BusinessDate & ").  Failed in attempt " & AttemptNum & vbCrLf & vbCrLf & ErrorText
         AttemptNum = AttemptNum + 1
         WScript.Sleep 5000
      Else
         OutputLog Success,"Business Date check successful after " & AttemptNum & " attempts." & vbCrLf & "Local business date = " & BusinessDate & vbCrLf & "LDS Business Date = " & POSLiveBusDate
      End If

   wend

End Function
'*************************************************************
'*************************************************************
'Output specified text to log file
'
Sub OutputLog(EventID,OutputText)
   On Error Resume Next
   WshShell.LogEvent EventID, "IRIS(" & WScript.ScriptName & "):  " & OutputText
End Sub
'*************************************************************
'*************************************************************
' Exit the script with an error
'
Sub GiveUp()
   On Error Resume Next
   OutputLog Error,"Terminating script due to error."
   ExitThisScript()
End Sub
'*************************************************************
'*************************************************************
' Exit the Script
'
Sub ExitThisScript()
   On Error Resume Next
   If CloseMsgScreen = 1 Then
      If MessageScreen.status = 0 Then MessageScreen.Terminate
      If Err.number Then OutputLog Error, "Unable to close Message Screen (html page)"
      Set MessageScreen = nothing
   End If
   Set WSHShell = Nothing
   Set iniEditor = Nothing
   Set fso = Nothing
   Set dbConn = Nothing
   WScript.Quit  ' end of script
End Sub