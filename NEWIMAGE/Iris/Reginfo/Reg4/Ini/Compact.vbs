' ------------------------------------------------------------------------
'               Copyright (C) 2005 xpient Solutions, Inc.
'
'                         Compact / Repair Script
'
' Author:  Jim Davidson
' Date: 3/11/2002
'
' ********************************************************************************
'##### Start check for x64 System #####
'This will check for a x64 System and restart the script using the 32bit version if needed
'xsCheckArchitecture v1.0
Sub xsCheckArchitecture
   On Error Resume Next
   If Not IsObject(WScript) Then Exit Sub
   Dim oWshShell, oWshProcEnv, oExec, Argument, sCmd, iResult
   Set oWshShell =  CreateObject("WScript.Shell")
   Set oWshProcEnv = oWshShell.Environment("Process")
   If LCase(oWshProcEnv("PROCESSOR_ARCHITECTURE")) <> "x86" and Instr(1, WScript.FullName, "SysWOW64", 1) = 0 then
      sCmd = Chr(34) & oWshProcEnv("SystemRoot") & "\SysWOW64\" &  Mid(WScript.FullName, InstrRev(WScript.FullName, "\") + 1) & Chr(34)
      sCmd = sCmd & " //NoLogo " & Chr(34) & WScript.ScriptFullName & Chr(34)
      For Each Argument in WScript.Arguments
         Argument = Split(Argument,"=",2)
         sCmd = sCmd & " " & Chr(34) & Argument(0) & Chr(34)
         If UBound(Argument) = 1 Then sCmd = sCmd & "=" & Chr(34) & Argument(1) & Chr(34)
      Next
      Set oExec = oWshShell.Exec(sCmd)
      Do While oExec.Status = 0
         Do While Not oExec.StdOut.AtEndOfStream
            WScript.StdOut.Write oExec.StdOut.Read(1)
         Loop
      Loop
      Do While Not oExec.StdErr.AtEndOfStream
         WScript.StdErr.Write oExec.StdErr.Read(1)
      Loop
      If Not oExec.ExitCode = 0  then WScript.Quit oExec.ExitCode
      WScript.quit
   End If
End Sub
xsCheckArchitecture
'##### End check for x64 System #####

' COM Objects
Dim WSHShell
Dim fso
Dim iniEditor

'Global Variables
Dim irisPath
Dim showProgress
Dim result
Dim PreScript
Dim PostScript
Dim DBList
Dim DBPath
Dim DB1Path
Dim PurgeData
Dim PurgePath
Dim MachineType

'Constants
Const Success = 0
Const Error = 1
Const Warning = 2
Const Info = 4

'*************************************************************
' THIS IS THE MAIN SCRIPT LOGIC. 
'
' We exit if any step fails, with a status of 1.  If everything
' succeeds, our exit status is 0.
'*************************************************************
   showProgress=0  'Make this 1 to display a messagebox before performing each step.
                   'Otherwise, 0.

   On Error Resume Next

  'Create the COM objects we will use in this script
   If showProgress Then MsgBox "Creating COM Objects"
   result=createCOMObjects()
   If result<>0 Then ExitThisScript()

  'Log beginning Of script
   If showProgress Then MsgBox "Log start of script"
   OutputLog Info,"Executing Compact/Repair script."

  'Parse command line
   If showProgress Then MsgBox "Parsing command line"
   result=ParseCommandLine()
   If result<>0 Then GiveUp()
   
  'Get the IRIS directory path
   If showProgress Then MsgBox "Determining IRIS path"
   result=getIRISDirectory()
   If result<>0 Then GiveUp()

  'Run Pre-Compact script if it exists
   PreScript = irispath & "\Scripts\PreCompact.vbs"
   If fso.FileExists(PreScript) Then
      OutputLog Info,"Launching Pre-Compact script: " & PreScript
      WSHShell.Run PreScript,,true
   End If

  'Repair and Compact Databases
   If showProgress Then MsgBox "Repairing and Compacting databases"
   result = CompactDB()
   If result = 0 Then WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\Compact",0

'  'Run Purge Data script if specified
'   If showProgress Then MsgBox "Determining whether to launch Purge Data script."
'   result=RunPurgeScript()
'   If result<>0 Then GiveUp()

   'Run Post-Compact script if it exists
    PostScript = irispath & "\Scripts\PostCompact.vbs"
    If fso.FileExists(PostScript) Then
       OutputLog Info,"Launching Post-Compact script: " & PostScript
       WSHShell.Run PostScript,,true
    End If

   'End the script with success
    OutputLog Success,"Compact script is complete."
    ExitThisScript()

'*************************************************************
' THIS IS THE END OF THE MAIN SCRIPT LOGIC
'*************************************************************
'
'*************************************************************
' Create the COM Objects we will use in this script
'
Function CreateCOMObjects()
   CreateCOMObjects=1   'Assume failure

   On Error Resume Next

   Set WSHShell = WScript.CreateObject("WScript.Shell")
   If Err.Number Then
      ExitThisScript()
   End If

   Set fso = CreateObject("Scripting.FileSystemObject")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Unable to create File System object (fso)."
      Exit Function
   End If

   Set iniEditor = CreateObject("IRISIniEditor.IniEditor.1")
   If Err.Number Then
      OutputLog Error,"CreateCOMObjects(): Error creating IRISIniEditor object.  Make sure that IRISIniEditor.ocx is registered." & vbCrLf & vbCrLf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Function
   End If

   CreateCOMObjects = 0 'Success
End Function
'*************************************************************
'*************************************************************
'Parse command line
'
Function ParseCommandLine()
   ParseCommandLine=0    'Assume success

   On Error Resume Next

   Set objArgs = WScript.Arguments
   For I = 0 to objArgs.Count - 1
      If Instr(objArgs(I),"=") Then
      Else
      End If
   Next

End Function
'*************************************************************
'*************************************************************
'Get the IRIS directory and version from the registry
'
Function GetIRISDirectory()
   GetIRISDirectory = 0 'Assume success

   On Error Resume Next

   irisPath=WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR")
   If Err.Number Then
      OutputLog Error,"GetIRISDirectory(): IRIS Path is missing from registry: HKEY_LOCAL_MACHINE\SOFTWARE\Progressive Software\IRIS\Common\IRISDIR"
      GetIRISDirectory=1
   End If

   MachineType = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\INSTALL\BOOrPOS")
   If Instr(Lcase(MachineType),"pos") OR Instr(Lcase(MachineType),"register") Then
      MachineType = "Register"
   Else
      MachineType = "Server"
   End If

End Function
'*************************************************************
'*************************************************************
'Repair and Compact databases
'
Function CompactDB()
   Dim i, DBList, Result, DBRepairEXE, CmdLine, TemplateDir, bUpdateLinks
   
   CompactDB = 0   'Assume success
   bUpdateLinks = False
   bSetNextNum = False

   On Error Resume Next

   DBRepairEXE = irispath & "\Bin\DBRepair.exe"
   If Not fso.FileExists(DBRepairEXE) Then
      OutputLog Error,"CompactDB: Unable to locate " & irispath & "\Bin\DBRepair.exe"
      CompactDB = 1
      Exit Function
   End If

   TemplateDir = GetTemplateDir

   If MachineType = "Register" Then
      DBList = Split("POSLive,POSPend,Kitchen",",")
   Else
      DBList = Split("POSLive,POSPend,Kitchen,Inventory,Report,Payroll,POSTrans",",")
   End If

   For i = 0 To UBound(DBList)
      if isLocked(irispath & "\Data\" & DBList(i) & ".mdb") Then
         OutputLog Error,"CompactDB: Failed check " & DBList(i) & ".mdb because it is in use."
         CompactDB = 1
      Else
         CmdLine = DBRepairEXE & " " & DBList(i) & ".mdb" & " /compact /create Template=" & Chr(34) & TemplateDir & "\" & DBList(i) & ".mdb" & Chr(34)
         WSHShell.CurrentDirectory = irispath & "\bin"
         Result = WSHShell.Run(CmdLine,,True)
         If Err.Number <> 0 Then
            OutputLog Error, CmdLine & " Error=(" & CStr(Err.Number) & ")" & Err.Description
            Err.Clear
         End If
         If (Result and (1+4+32)) <> 0  Then
            OutputLog Error, CmdLine & " Result=" & CStr(Result)
         End If
         If ((Result and 16) = 16) and (MachineType <> "Register") then
            bUpdateLinks = True
         End If
         If ((Result and 1) = 1) then
            OutputLog Error,"CompactDB: Compact Failed for " & DBList(i) & ".mdb"
            CompactDB = 1
         End If
         If ((Result and 4) = 4) then
            OutputLog Error,"CompactDB: Failed to add missing tables in " & DBList(i) & ".mdb"
            CompactDB = 1
         End If
         If ((Result and 32) = 32) then
            OutputLog Error,"CompactDB: Failed to Copy database from the template folder for " & DBList(i) & ".mdb"
            CompactDB = 1
         End If
         If ((Result and 16) = 16) and (DBList(i) = "POSLive") then
            bSetNextNum = True
         End If
      End If
   Next
   
   If bUpdateLinks = True Then
      WSHShell.Run "wscript.exe //B " & irispath & "\Setup\UpdateLinks.VBS",1,True
   End If

   If bSetNextNum = True Then
      WSHShell.Run "wscript.exe //B " & irispath & "\Scripts\SetNextNum.VBS /q",1,True
   End If

End Function
'*************************************************************
'*************************************************************
'Get MDB Template Directory
'
Function GetTemplateDir()
   iniEditor.iniFilePath = irisPath & "\ini\SystemMaint.ini"
   If MachineType = "Server" Then
      inieditor.section="Server"
   Else
      inieditor.section="Registers"
   End If

   inieditor.key = "MdbTemplate"
   If inieditor.value <> "" Then 
      GetTemplateDir = inieditor.value
   Else
      GetTemplateDir = irispath & "\Data\Template"
   End If
End Function
'*************************************************************
'*************************************************************
'Check for in use
'
Function isLocked(sMDB)
   On Error Resume Next
   Set dbConn = CreateObject("ADODB.Connection")
   dbConn.Mode = 12
   dbConn.Open "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Chr(34) & sMDB & Chr(34)
   If Err.Number <> 0 Then
      If Err.Number = -2147467259 and Instr(1,Err.Description,"already opened exclusively",1) > 0 Then
         isLocked = True
      End If
      OutputLog Error,"CompactDB:isLocked:Error=(" & CStr(Err.Number) & ") " & Err.Description
   End If
End Function
'*************************************************************
'*************************************************************
'Run Purge script if specified in registry
'
Function RunPurgeScript()
   RunPurgeScript = 0   'Assume success

   On Error Resume Next

   PurgePath = irispath & "\Scripts\PurgeData.vbs"
   PurgeData = WSHShell.RegRead ("HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\Purge")
   If PurgeData = "1" Then
      OutputLog Info,"Launching Purge Data script."
      If fso.FileExists(PurgePath) Then
         WSHShell.Run PurgePath,,true
      Else
         OutputLog Error,"Unable to locate " & PurgePath & ".  Purge script cannot be executed."
      End If
   End If
End Function
'*************************************************************
'*************************************************************
'Remove read-only attribute from specified file
'
Sub RemoveReadOnly(FilePath)
   On Error Resume Next
   Dim oFile

   If Not fso.FileExists(FilePath) Then Exit Sub

   Set oFile = fso.GetFile(FilePath) 
   If Err.Number Then
      OutputLog Error,"RemoveReadOnly(" & FilePath & "): Error setting file object for specified file." & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
      Exit Sub
   End If

   If oFile.attributes and 1 Then
      OutputLog Warning,"RemoveReadOnly(" & FilePath & "): Read-only attribute is set on specified file...Removing before compacting."
      Err.Clear
      oFile.Attributes = oFile.Attributes - 1
      If Err.Number Then OutputLog Error,"RemoveReadOnly(" & FilePath & "): Error removing read-only attribute from specified file." & vbcrlf & vbcrlf & Err.Source & "(" & Err.Number & "): " & Err.Description
   End If

   Set oFile = Nothing

End Sub
'*************************************************************
'*************************************************************
'Add leading zeroes to insure output equals number of characters passed in
'
Function PadZero(n,nLength)
   On Error Resume Next
   PadZero = n

   While Len(PadZero) < nLength
      PadZero = "0" & PadZero
   wend

End Function
'*************************************************************
'*************************************************************
'Output specified text to log file
'
Sub OutputLog(EventID,OutputText)
   On Error Resume Next
   WshShell.LogEvent EventID, "IRIS(" & WScript.ScriptName & "):  " & OutputText
End Sub
'*************************************************************
'*************************************************************
' Exit the script with an error
'
Sub GiveUp()
   On Error Resume Next
   OutputLog Error,"Terminating script due to error."
   ExitThisScript()
End Sub
'*************************************************************
'*************************************************************
' Exit the Script
'
Sub ExitThisScript()
   On Error Resume Next

   WSHShell.RegWrite "HKLM\SOFTWARE\Progressive Software\IRIS\Maintenance\LastCompactExecution",Now()

   Set WSHShell = Nothing
   Set fso = Nothing
   Set iniEditor  = Nothing
   Set updateLinks = Nothing
   WScript.Quit    'End of script
End Sub

'' SIG '' Begin signature block
'' SIG '' MIIQ7QYJKoZIhvcNAQcCoIIQ3jCCENoCAQExDjAMBggq
'' SIG '' hkiG9w0CBQUAMGYGCisGAQQBgjcCAQSgWDBWMDIGCisG
'' SIG '' AQQBgjcCAR4wJAIBAQQQTvApFpkntU2P5azhDxfrqwIB
'' SIG '' AAIBAAIBAAIBAAIBADAgMAwGCCqGSIb3DQIFBQAEEPcz
'' SIG '' WCW7qwRE3AZD+XLKO8CgggyQMIIDejCCAmKgAwIBAgIQ
'' SIG '' OCXX+vhhr570kOcmtdZa1TANBgkqhkiG9w0BAQUFADBT
'' SIG '' MQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNpZ24s
'' SIG '' IEluYy4xKzApBgNVBAMTIlZlcmlTaWduIFRpbWUgU3Rh
'' SIG '' bXBpbmcgU2VydmljZXMgQ0EwHhcNMDcwNjE1MDAwMDAw
'' SIG '' WhcNMTIwNjE0MjM1OTU5WjBcMQswCQYDVQQGEwJVUzEX
'' SIG '' MBUGA1UEChMOVmVyaVNpZ24sIEluYy4xNDAyBgNVBAMT
'' SIG '' K1ZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMg
'' SIG '' U2lnbmVyIC0gRzIwgZ8wDQYJKoZIhvcNAQEBBQADgY0A
'' SIG '' MIGJAoGBAMS18lIVvIiGYCkWSlsvS5Frh5HzNVRYNerR
'' SIG '' Nl5iTVJRNHHCe2YdicjdKsRqCvY32Zh0kfaSrrC1dpbx
'' SIG '' qUpjRUcuawuSTksrjO5YSovUB+QaLPiCqljZzULzLcB1
'' SIG '' 3o2rx44dmmxMCJUe3tvvZ+FywknCnmA84eK+FqNjeGkU
'' SIG '' e60tAgMBAAGjgcQwgcEwNAYIKwYBBQUHAQEEKDAmMCQG
'' SIG '' CCsGAQUFBzABhhhodHRwOi8vb2NzcC52ZXJpc2lnbi5j
'' SIG '' b20wDAYDVR0TAQH/BAIwADAzBgNVHR8ELDAqMCigJqAk
'' SIG '' hiJodHRwOi8vY3JsLnZlcmlzaWduLmNvbS90c3MtY2Eu
'' SIG '' Y3JsMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMA4GA1Ud
'' SIG '' DwEB/wQEAwIGwDAeBgNVHREEFzAVpBMwETEPMA0GA1UE
'' SIG '' AxMGVFNBMS0yMA0GCSqGSIb3DQEBBQUAA4IBAQBQxUvI
'' SIG '' JIDf5A0kwt4asaECoaaCLQyDFYE3CoIOLLBaF2G12AX+
'' SIG '' iNvxkZGzVhpApuuSvjg5sHU2dDqYT+Q3upmJypVCHbC5
'' SIG '' x6CNV+D61WQEQjVOAdEzohfITaonx/LhhkwCOE2DeMb8
'' SIG '' U+Dr4AaH3aSWnl4MmOKlvr+ChcNg4d+tKNjHpUtk2scb
'' SIG '' W72sOQjVOCKhM4sviprrvAchP0RBCQe1ZRwkvEjTRIDr
'' SIG '' oc/JArQUz1THFqOAXPl5Pl1yfYgXnixDospTzn099io6
'' SIG '' uE+UAKVtCoNd+V5T9BizVw9ww/v1rZWgDhfexBaAYMkP
'' SIG '' K26GBPHr9Hgn0QXF7jRbXrlJMvIzMIIDxDCCAy2gAwIB
'' SIG '' AgIQR78Zld+NUkZD99ttSA0xpDANBgkqhkiG9w0BAQUF
'' SIG '' ADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rl
'' SIG '' cm4gQ2FwZTEUMBIGA1UEBxMLRHVyYmFudmlsbGUxDzAN
'' SIG '' BgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhhd3RlIENl
'' SIG '' cnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1l
'' SIG '' c3RhbXBpbmcgQ0EwHhcNMDMxMjA0MDAwMDAwWhcNMTMx
'' SIG '' MjAzMjM1OTU5WjBTMQswCQYDVQQGEwJVUzEXMBUGA1UE
'' SIG '' ChMOVmVyaVNpZ24sIEluYy4xKzApBgNVBAMTIlZlcmlT
'' SIG '' aWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EwggEi
'' SIG '' MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpyrKk
'' SIG '' zM0grwp9iayHdfC0TvHfwQ+/Z2G9o2Qc2rv5yjOrhDCJ
'' SIG '' WH6M22vdNp4Pv9HsePJ3pn5vPL+Trw26aPRslMq9Ui2r
'' SIG '' SD31ttVdXxsCn/ovax6k96OaphrIAuF/TFLjDmDsQBx+
'' SIG '' uQ3eP8e034e9X3pqMS4DmYETqEcgzjFzDVctzXg0M5US
'' SIG '' mRK53mgvqubjwoqMKsOLIYdmvYNYV291vzyqJoddyhAV
'' SIG '' PJ+E6lTBCm7E/sVK3bkHEZcifNs+J9EeeOyfMcnx5iIZ
'' SIG '' 28SzR0OaGl+gHpDkXvXufPF9q2IBj/VNC97QIlaolc2u
'' SIG '' iHau7roN8+RN2aD7aKCuFDuzh8G7AgMBAAGjgdswgdgw
'' SIG '' NAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhhodHRw
'' SIG '' Oi8vb2NzcC52ZXJpc2lnbi5jb20wEgYDVR0TAQH/BAgw
'' SIG '' BgEB/wIBADBBBgNVHR8EOjA4MDagNKAyhjBodHRwOi8v
'' SIG '' Y3JsLnZlcmlzaWduLmNvbS9UaGF3dGVUaW1lc3RhbXBp
'' SIG '' bmdDQS5jcmwwEwYDVR0lBAwwCgYIKwYBBQUHAwgwDgYD
'' SIG '' VR0PAQH/BAQDAgEGMCQGA1UdEQQdMBukGTAXMRUwEwYD
'' SIG '' VQQDEwxUU0EyMDQ4LTEtNTMwDQYJKoZIhvcNAQEFBQAD
'' SIG '' gYEASmv56ljCRBwxiXmZK5a/gqwB1hxMzbCKWG7fCCmj
'' SIG '' XsjKkxPnBFIN70cnLwA4sOTJk06a1CJiFfc/NyFPcDGA
'' SIG '' 8Ys4h7Po6JcA/s9Vlk4k0qknTnqut2FB8yrO58nZXt27
'' SIG '' K4U+tZ212eFX/760xX71zwye8Jf+K9M7UhsbOCf3P0ow
'' SIG '' ggVGMIIELqADAgECAhEAsHgJfsXSrQMA7fgpGia4sjAN
'' SIG '' BgkqhkiG9w0BAQUFADCBlTELMAkGA1UEBhMCVVMxCzAJ
'' SIG '' BgNVBAgTAlVUMRcwFQYDVQQHEw5TYWx0IExha2UgQ2l0
'' SIG '' eTEeMBwGA1UEChMVVGhlIFVTRVJUUlVTVCBOZXR3b3Jr
'' SIG '' MSEwHwYDVQQLExhodHRwOi8vd3d3LnVzZXJ0cnVzdC5j
'' SIG '' b20xHTAbBgNVBAMTFFVUTi1VU0VSRmlyc3QtT2JqZWN0
'' SIG '' MB4XDTA5MDUxNDAwMDAwMFoXDTEyMDUxMzIzNTk1OVow
'' SIG '' gboxCzAJBgNVBAYTAlVTMQ4wDAYDVQQRDAUyODIyNjEL
'' SIG '' MAkGA1UECAwCTkMxEjAQBgNVBAcMCUNoYXJsb3R0ZTEu
'' SIG '' MCwGA1UECQwlMTE1MjUgQ2FybWVsIENvbW1vbnMgQmx2
'' SIG '' ZC4sIFN1aXRlIDEwMDEZMBcGA1UECgwQWFBJRU5UIFNv
'' SIG '' bHV0aW9uczEUMBIGA1UECwwLRGV2ZWxvcG1lbnQxGTAX
'' SIG '' BgNVBAMMEFhQSUVOVCBTb2x1dGlvbnMwggEiMA0GCSqG
'' SIG '' SIb3DQEBAQUAA4IBDwAwggEKAoIBAQC35Pl7Op1x200/
'' SIG '' nJxncnnhKXmPQw8T3KOIYnZqZ4m3lb+cvnF+d7SDy9jD
'' SIG '' dxbdM39WLyXWOpskG+LIViFlBV1KHTEsHgGkqwQpx5UT
'' SIG '' 5HVLMtNm0NYCWznFtYfVIqJs+tqz5lSQ2MUV4rSnstxm
'' SIG '' 0WRw4X9dneXe+zIrJ1W96UCw4ZLifsPbeqVYGpklgUmF
'' SIG '' xmFfwuPX/4qX0K6CXoqMRBZxg48qny2rv2vq+s0jscch
'' SIG '' sAyeZxjmWVUpxmYrZtyXCSNfVWd3UZsyZyDdl5YurfZx
'' SIG '' 28G1vjHr8WUuys8OX/FMgaRos8IUfDgdyP2LftB42HTh
'' SIG '' F/snyMik7PaMdv/wEAMtAgMBAAGjggFoMIIBZDAfBgNV
'' SIG '' HSMEGDAWgBTa7WR0FJwUPKvdmam9WyhNizzJ2DAdBgNV
'' SIG '' HQ4EFgQUeH7WhOp5HeJU1lLMlQxJBkArhykwDgYDVR0P
'' SIG '' AQH/BAQDAgeAMAwGA1UdEwEB/wQCMAAwEwYDVR0lBAww
'' SIG '' CgYIKwYBBQUHAwMwEQYJYIZIAYb4QgEBBAQDAgQQMEYG
'' SIG '' A1UdIAQ/MD0wOwYMKwYBBAGyMQECAQMCMCswKQYIKwYB
'' SIG '' BQUHAgEWHWh0dHBzOi8vc2VjdXJlLmNvbW9kby5uZXQv
'' SIG '' Q1BTMEIGA1UdHwQ7MDkwN6A1oDOGMWh0dHA6Ly9jcmwu
'' SIG '' dXNlcnRydXN0LmNvbS9VVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dC5jcmwwNAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzAB
'' SIG '' hhhodHRwOi8vb2NzcC5jb21vZG9jYS5jb20wGgYDVR0R
'' SIG '' BBMwEYEPaW5mb0B4cGllbnQuY29tMA0GCSqGSIb3DQEB
'' SIG '' BQUAA4IBAQBvSaL3UXO313UuGSpKCtggrg83wHa4XmlM
'' SIG '' 4LaUpCekcEBSFvVnBoxuGGR12GLgBkkLOVgPUnTsoH8G
'' SIG '' qvwcsTa11kQAV8XZ23K/Bk+c6MV3VeHa03V5kdf+2qKY
'' SIG '' VVadxQdrry8Dw2nvbuNZvBA1nMhB+3BQfSo2nTqIhahp
'' SIG '' MyY6FPXL2bemF/JWVAflFwyrdaInUueimSfBPu8ubQX1
'' SIG '' vgBOANoE33nQ8kXS1IhRsuB4xAjE9N4ZLwdr4rGkgstO
'' SIG '' xAVseaEUePSt+yWGMsyD9C/2XX4aG5Yp6wemvOrMF59G
'' SIG '' oJ7Uij5WrvxUJ4JNMuX22ywuQ6zZC1sCbVo1kYAagm9q
'' SIG '' MYIDxzCCA8MCAQEwgaswgZUxCzAJBgNVBAYTAlVTMQsw
'' SIG '' CQYDVQQIEwJVVDEXMBUGA1UEBxMOU2FsdCBMYWtlIENp
'' SIG '' dHkxHjAcBgNVBAoTFVRoZSBVU0VSVFJVU1QgTmV0d29y
'' SIG '' azEhMB8GA1UECxMYaHR0cDovL3d3dy51c2VydHJ1c3Qu
'' SIG '' Y29tMR0wGwYDVQQDExRVVE4tVVNFUkZpcnN0LU9iamVj
'' SIG '' dAIRALB4CX7F0q0DAO34KRomuLIwDAYIKoZIhvcNAgUF
'' SIG '' AKBsMBAGCisGAQQBgjcCAQwxAjAAMBkGCSqGSIb3DQEJ
'' SIG '' AzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAM
'' SIG '' BgorBgEEAYI3AgEVMB8GCSqGSIb3DQEJBDESBBCvXDwj
'' SIG '' 4bIfFGLvvt08homeMA0GCSqGSIb3DQEBAQUABIIBAFG3
'' SIG '' xLzKeravtf17mcmZxhYzOMsyjzeVwXMqQ9w5QQahXgoP
'' SIG '' k5LSlNIy9Ilsc6Iy5kxxpwg2JQMtuHtCeHyAMEmpKoEu
'' SIG '' qD6oadpPv2wzSB5K/WWwI0vz1KWcfbva7BlIMAs6Xif5
'' SIG '' gadh2QSXeo+UiOZzxDwoDs+6+w6Hb9DRn5HKfWyZGxGd
'' SIG '' WhdMRjyiWRyDAQSMrlOnSuM1dAj961NBQDHQRrCjL6Nv
'' SIG '' 2S6i/Qj3yQ5mBNlqxFGzVnDThgme1pId9AMR4Ae2pnBh
'' SIG '' e6sijqyb1SC5OTKq2CgmYC2lcV/5WYErw1LeHYi4sRGr
'' SIG '' 9q72u1KznifS5vZcGJg825cTj6FL3IahggF/MIIBewYJ
'' SIG '' KoZIhvcNAQkGMYIBbDCCAWgCAQEwZzBTMQswCQYDVQQG
'' SIG '' EwJVUzEXMBUGA1UEChMOVmVyaVNpZ24sIEluYy4xKzAp
'' SIG '' BgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2Vy
'' SIG '' dmljZXMgQ0ECEDgl1/r4Ya+e9JDnJrXWWtUwCQYFKw4D
'' SIG '' AhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEw
'' SIG '' HAYJKoZIhvcNAQkFMQ8XDTExMDEyNTE2MzgzNlowIwYJ
'' SIG '' KoZIhvcNAQkEMRYEFAb4lbPNT3XNiziDrGo3ZO5KuzhK
'' SIG '' MA0GCSqGSIb3DQEBAQUABIGAekWmy/HDZWgTCJHhRlTK
'' SIG '' yQELWpzdWMqU0jBO+7+Inn9ByC0NwbXN5qrz6sMQ+Shk
'' SIG '' oIlgGI4mzUzuTtATw9KoR7JoCbt3xgIJWMyWrGvqmh4Z
'' SIG '' 8cCU1EzUXSlQ5c/ElvtISFjeoTeNNuuJCwpf/NvXAskY
'' SIG '' ldK6U6zBmzOsU5sXAR0=
'' SIG '' End signature block
