;***************************************************************************
;
; FILENAME:    IRIS.ini
;
; DATE:        12/02/96
;
; AUTHOR:
;
; COPYRIGHT:   (C) 1995-1996 Progressive Software, Inc.  All rights reserved.
;
; NOTICE:      All Copyright notices contained within this source code must
;              remain in place and may not be removed or modified.
;
; DESCRIPTION: This file contains the settings for localization, OLE
;              automation, database path, polling files, user applications,
;              and POS and Back Office shells.
;
;***************************************************************************

[General]
NetPrefix=\\BNE_
Registers=5
ServerData=1

[Debug]
log=1

[Intl]
iCountry=1
ICurrDigits=2
iCurrency=0
iDate=0
iDigits=2
iLZero=1
iMeasure=1
iNegCurr=0
iTime=0
iTLZero=0
s1159=AM
s2359=PM
sCountry=United States
sCurrency=$
sDate=/
sDecimal=.
sLanguage=enu
sList=,
sLongDate=dddd, MMMM dd, yyyy
sShortDate=M/d/yy
sThousand=,
sTime=:
fmtShortDate=%m/%d/%y
iPrimaryLangID=19
iSecondaryLangID=0

[Automation]
Inventory=IRIS.Inventory
Payroll=IRIS.Payroll
Purchase=IRIS.Purchase
Utilities=IRIS.Utilities
POS=IRIS.POS
EditCfg=IRIS.EditCfg
EODProc=IRIS.EODProc
Reports=IRIS.BoReports
CashMgmt=IRIS.CashMgmt

[Database]
InventoryDB=c:\iris\data\Inventory.mdb
PayrollDB=c:\iris\data\Payroll.mdb
PosTransDB=c:\iris\data\PosTrans.mdb
PosCfgDB=c:\iris\data\PosCfg.mdb
DSRDB=c:\iris\data\DSR.mdb
ForecastDB=C:\IRIS\DATA\Forecast.mdb
ReportDB=c:\iris\data\Report.mdb
KitchenDB=c:\iris\data\Kitchen.mdb
SecurityDB=c:\iris\data\Security.mdb
DBTYPE=Access

;-------------------------------------------------------------------------
; POS Shell settings
;
; - POS                      POS program name
; - START_POS_AS_DEFAULT     Start POS as the default application
; - UPDATE                   Software update program name
; - 4695_DRIVER              IBM 4695 driver (only use if system is Win 95)
; - SHUTDOWN_WINDOWS         1 - Reboot the system, 0 - otherwise
; - SERVICE_COUNT            Number of services to start
; - SERVICEn                 Service to run where n = 1, 2, 3, etc...
; - TIMER_SW_UPDATE          Wait time in seconds for software update
; - TIMER_START_SERVICE      Wait time in seconds for starting services
; - TIMER_POLL_SERVICES      Polling interval for service status check
;-------------------------------------------------------------------------

[POSShell]
POS=wscript.exe c:\iris\update\applyregisterupdate.vbs
START_POS_AS_DEFAULT=1
UPDATE=c:\iris\bin\regupd.bat
SHUTDOWN_WINDOWS=1

SERVICE_COUNT=7
SERVICE1=Print Server [P],c:\iris\bin\pserve.exe
SERVICE2=Time Server [P],c:\iris\bin\timesrv.exe
SERVICE3=Line Display [P],c:\iris\bin\ncr.exe disp=cdsp1
;SERVICE3=Line Display [P],c:\iris\bin\OposLineDisplayE.exe -cdisp cdsp1
SERVICE4=Print Device [P],c:\iris\bin\prtrdev.exe rcpt1
SERVICE5=Kitchen [P],c:\iris\bin\Kitchen.exe SecondaryKit1
SERVICE6=MSR [P],c:\iris\bin\oposmsr.exe -msr1 MSR1
SERVICE7=Cash Drawer[P],c:\iris\bin\oposcashdrawer.exe -drawer1 cdrw1


TIMER_SW_UPDATE=60
TIMER_START_SERVICE=10
TIMER_POLL_SERVICES=5

[BOShell]
SECURITYDLL=0

[ExportUtil]
EnableExport=0

[Utility]
; 0 - Numeric sort, 1 - Alphabetical sort
ItemSort=0
MenuSort=0
; 0 - Ascending, 1 - Descending
TypeSort=1
; maximun number of characters for description
ItemDesc=14
; maximun number of characters for Receipt description
ItemRecDesc=14
; maximun number of characters for long description
ItemLongDesc=20
; maximun number of characters for short description
;ItemShortDesc=10
; maximun number of Tax Rules
NumberTaxRules=4
; maximun number of Entries per Tax Rule
NumberTaxRuleEntries=3

[Login]
UserID=10

;-------------------------------------------------------------------------
; Payroll settings
; LowAdjust                  Number of minutes allowed to clock in / out
;                            before shift start / end without Manager
;                            approval
; HighAdjust                 Number of minutes allowed to clock in / out
;                            after shift start / end without Manager
;                            approval
;-------------------------------------------------------------------------

[Payroll]
LowAdjust=5
HighAdjust=15

;-------------------------------------------------------------------------
; CashMgmt settings
; GrossIncludeTax            Instructs Pos to include Tax in Gross Sales
;                            calculations.
; TipPool                    Instructs Pos to include TipPool calc.
; BreakfastTime
; LunchTime
; DinnerTime                 Used to tell Pos time frames for calc.
; HOTID1 ....                Used by Pos to report on Hot Item Sales.
; CATID1 ....                Used by Pos to report on Category Sales.
;                              
;-------------------------------------------------------------------------
[CashMgmt]
HOTID1=8005
HOTID2=8010
HOTID3=8015
HOTID4=8016
HOTID5=8019
HOTID6=8020
HOTID7=8025
HOTID8=8030
HOTID9=8031
HOTID10=8032
HOTID11=8033
HOTID12=8035
HOTID13=8047
HOTID14=8049
HOTID15=8051
HOTID16=8060
HOTID17=8061
HOTID18=8065
HOTID19=8070
HOTID20=8301
HOTID21=90001
HOTID22=90002
HOTID23=90003
HOTID24=90004
HOTID25=90006
HOTID26=90008
HOTID27=90009
AllowDeposits=1
PromptForBagNumber=0
PromptForBusinessDate=1
GrossIncludeTax=0
OverringIncludetax=0
RefundIncludeTax=1
TipPool=0
BreakfastTime=8:00:00 AM
LunchTime=11:00:00 AM
DinnerTime=4:00:00 PM
TacoTuesday=99
CATID1=
TipPoolAmount=0
TipDescription=Tip Pool
;PaidOutAcctID1=40
UpdateOrder=0
Bypassfixdrawers=1
;-------------------------------------------------------------------------
; Discounts settings
; 
; MaxPerItem                 Used by Pos to determine the number of 
;                            discounts that can be applied to an item.
;                            -0- is unrestricted.
; Employee_50_Pct_Discnt     50 percent discount allowed for employees.
;                            The keyvalue 2 is obtained from tblDiscounts,
;                            POSLive.mdb.
;-------------------------------------------------------------------------
[Discounts]
MaxPerItem=9999

;-------------------------------------------------------------------------
; Order settings
; 
; NetTotal                   Used by Pos to determine what fields
;                            make up the NetTotal for the order.
;                            ( A = Add, S = Subtract, N = No Action )
;                            ( Fields           Position )
;                            - SubTotal            1
;                            - LineDiscTotal       2
;                            - DiscTotal           3
;                            - Tax                 4
;                            - NonTaxTotal         5
;
;-------------------------------------------------------------------------
[Order]
NetTotal=ASSNA

[SECURITY]
SECURITYDLL=1
USEHIERARCHY=1


[Version]
Version=1.6.0.0
[Utilities]
KeepSectionsInSyncWithViews=1
