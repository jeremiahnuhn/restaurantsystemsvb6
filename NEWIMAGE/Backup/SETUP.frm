VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "New Server Image Setup"
   ClientHeight    =   9060
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9765
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   15.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   9060
   ScaleWidth      =   9765
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command6 
      Caption         =   "Update IRIS Database / Rest. Specific Setup"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1680
      TabIndex        =   21
      Top             =   6480
      Width           =   6255
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Copy Files and Retore"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1680
      TabIndex        =   18
      Top             =   5760
      Width           =   6255
   End
   Begin VB.Frame Frame2 
      Caption         =   "  Steps to complete on new drive "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2295
      Left            =   360
      TabIndex        =   17
      Top             =   5040
      Width           =   8895
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   23
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label13 
         Caption         =   "Step 6"
         Height          =   615
         Left            =   240
         TabIndex        =   22
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label12 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   7680
         TabIndex        =   20
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label11 
         Caption         =   "Step 5"
         Height          =   495
         Left            =   240
         TabIndex        =   19
         Top             =   840
         Width           =   975
      End
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Verify Backup"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1680
      TabIndex        =   13
      Top             =   3720
      Width           =   6255
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Create Database Backups and Copy Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1680
      TabIndex        =   9
      Top             =   3000
      Width           =   6255
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Setup Server to Run Windows"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1680
      TabIndex        =   7
      Top             =   2280
      Width           =   6255
   End
   Begin VB.CommandButton Command1 
      Caption         =   "EXIT"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1080
      TabIndex        =   5
      Top             =   7800
      Width           =   7095
   End
   Begin VB.Frame Frame1 
      Caption         =   " Steps to complete on old drive "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3615
      Left            =   360
      TabIndex        =   1
      Top             =   960
      Width           =   8895
      Begin VB.CommandButton UpdateTerminals 
         Caption         =   "Update Terminals"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   1320
         TabIndex        =   3
         Top             =   600
         Width           =   6255
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   7680
         TabIndex        =   15
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "Step 4"
         Height          =   615
         Left            =   120
         TabIndex        =   14
         Top             =   2880
         Width           =   1095
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   7680
         TabIndex        =   12
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   7680
         TabIndex        =   11
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "Step 3"
         Height          =   495
         Left            =   120
         TabIndex        =   10
         Top             =   2160
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Step 2"
         Height          =   495
         Left            =   120
         TabIndex        =   8
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Step 1"
         Height          =   495
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Wingdings 2"
            Size            =   36
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   7680
         TabIndex        =   4
         Top             =   600
         Width           =   1095
      End
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   495
      Left            =   480
      TabIndex        =   16
      Top             =   480
      Width           =   8775
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Version 1.0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   8640
      TabIndex        =   2
      Top             =   8760
      Width           =   975
   End
   Begin VB.Label DriveLabel 
      Alignment       =   1  'Right Justify
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   7080
      TabIndex        =   0
      Top             =   120
      Width           =   2535
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
   FN = DriveLetter + ":\Server Image\PreStandAlone.bat"
    x = Shell(FN, vbMaximizedFocus)
    Label5.Caption = "P"
    JobLog ("Step2=1")
    Y = MsgBox("You now need to Log Off", vbExclamation)
    End
End Sub

Private Sub Command3_Click()
        FN = DriveLetter + ":\Server Image\backup to thumb drive" + DriveLetter + ".bat"
        x = Shell(FN, vbMaximizedFocus)
        Label7.Caption = "P"
        JobLog ("Step3=1")

'*** Create Custom Batch Files ***



'GET PASSWORD
    Dim fname As String
    FN = DriveLetter + ":\SERVER IMAGE\HARDEES PASSWORDS.CSV"
    Open FN For Input As 1
    Do While Not EOF(1)
        Input #1, fnumber, fname, fuser, fpassword
        If fnumber = unumber Then
            rpassword = fpassword
            rname = fname
            rnumber = fnumber
        End If
    Loop
    Close #1

    Label10.Caption = Str(rnumber) + " " + UCase(rname) + " Password: " + Str(rpassword)

FN = DriveLetter + ":\SERVER IMAGE\REST.BAT"
Open FN For Output As 1
Print #1, "rem BATCH FILE TO CREATE WINDOWS SCHEDULED TASKS" + Str(Now())
Print #1, "rem " + Str(rnumber) + " " + UCase(rname)
Print #1, "schtasks /Delete /tn CCASETTLE /F"
Print #1, "schtasks /Delete /tn NightlyUpdate /F"
Print #1, "schtasks /Delete /tn Repoll-Evening /F"
Print #1, "schtasks /Delete /tn Repoll-Morning /F"
Print #1, "schtasks /Delete /tn RUNLCM /F"
Print #1, "schtasks /Delete /tn Toterm /F"
Print #1, "schtasks /create /sc DAILY /tn CCASettle /tr c:\Iris\bin\CCASETTLE.BAT /ru BODDIENOELL\gm" + LTrim(Str(rnumber)) + " /rp " + LTrim(Str(rpassword)) + " /st 15:00:00"
Print #1, "schtasks /create /sc DAILY /tn NightlyUpdate /tr c:\Iris\bin\NightlyUpdate.bat /ru BODDIENOELL\gm" + LTrim(Str(rnumber)) + " /rp " + LTrim(Str(rpassword)) + " /st 02:15:00"
Print #1, "schtasks /create /sc DAILY /tn Repoll-Evening /tr c:\Iris\bin\repoll.bat /ru BODDIENOELL\gm" + LTrim(Str(rnumber)) + " /rp " + LTrim(Str(rpassword)) + " /st 17:55:00"
Print #1, "schtasks /create /sc DAILY /tn Repoll-Morning /tr c:\Iris\bin\repoll.bat /ru BODDIENOELL\gm" + LTrim(Str(rnumber)) + " /rp " + LTrim(Str(rpassword)) + " /st 10:00:00"
Print #1, "schtasks /create /sc DAILY /tn RUNLCM /tr c:\Iris\bin\RUNLCM.BAT /ru BODDIENOELL\gm" + LTrim(Str(rnumber)) + " /rp " + LTrim(Str(rpassword)) + " /st 03:55:00"
Print #1, "schtasks /create /sc DAILY /tn Toterm /tr c:\Nodesys\toterm.bat /ru BODDIENOELL\gm" + LTrim(Str(rnumber)) + " /rp " + LTrim(Str(rpassword)) + " /st 01:30:00"
Print #1, "rem Copy statement for the file used by BNE Desktop for the Support Log On Process"
fn2 = DriveLetter + ":\AUTOLOGON\" + LTrim(Str(rnumber)) + ".reg"
Print #1, "Copy /y " + fn2 + " c:\support\AutoLogon-on.reg"
Print #1, "rem Import Registery Setup for LiveConnect"
fn2 = "c:\bneapps\regist~1\LiveConnect\" + LTrim(Str(rnumber)) + ".reg"
Print #1, "regedit /s " + fn2
Print #1, "rem Import Registery Setup for Windows Log On"
fn2 = "c:\bneapps\regist~1\winlogon\" + LTrim(Str(rnumber)) + ".reg"
Print #1, "regedit /s " + fn2
Print #1, "EXIT"
Close #1

End Sub

Private Sub Command4_Click()

nf = DriveLetter + ":\backup\IRIS.bak"

fdt = FileDateTime(nf)
x = MsgBox("The date and time of the IRIS backup is " + Str(fdt) + "  If the date and time are correct, you have completed the backup of data from the old drive.", vbInformation, "Verify Backup Date and Time")
        Label9.Caption = "P"
        JobLog ("Step4=1")
End Sub

Private Sub Command5_Click()
        FN = DriveLetter + ":\Server Image\Restore from Thumb" + DriveLetter + ".bat"
        x = Shell(FN, vbMaximizedFocus)
        Label12.Caption = "P"
        JobLog ("Step5=1")
End Sub

Private Sub Command6_Click()
        FN = DriveLetter + ":\Server Image\PostStandalone" + DriveLetter + ".bat"
        'x = Shell(FN, vbMaximizedFocus)
        Label14.Caption = "P"
        JobLog ("Step6=1")
End Sub

Private Sub Form_Load()

On Error Resume Next
If UCase(Dir("e:\server image\install script.sql")) <> "" Then

    DriveLabel.Caption = "Flash Drive is E:"
    DriveLetter = "E"
End If
If UCase(Dir("f:\server image\install script.sql")) <> "" Then

    DriveLabel.Caption = "Flash Drive is F:"
    DriveLetter = "F"
End If
If UCase(Dir("G:\server image\install script.sql")) <> "" Then

    DriveLabel.Caption = "Flash Drive is G:"
    DriveLetter = "G"
End If

If DriveLetter = "" Then

    DriveLabel.Caption = "Flash Drive is ?:"
    x = MsgBox("This setup program only supports Drives E, F, and G.  Please contact Software Support.", vbCritical, "Invalid Drive Letter")
    End
End If
If Dir("c:\BNEAPPS\ID\UNIT.TXT") <> "" Then
    Open "c:\BNEAPPS\ID\UNIT.TXT" For Input As 1
    Input #1, unumber, Uname, email, junk
    Input #1, unumber, Uname, email, junk
    Close #1
    Form1.Caption = "BNE New Server Image:   " + Uname + "  #" + unumber
Else
    If Dir(DriveLetter + ":\Server Image\UNIT.TXT") <> "" Then
        Open (DriveLetter + ":\Server Image\UNIT.TXT") For Input As 1
            Input #1, unumber, Uname, email, junk
            Input #1, unumber, Uname, email, junk
            Close #1
            Form1.Caption = "BNE New Server Image:   " + Uname + "  #" + unumber
    End If
End If

temp = Now()
JobLog (temp)

'**** SETUP CHECKS
    FN = DriveLetter + ":\LOG\" + unumber + ".txt"
    Open FN For Input As 1
    Do While Not EOF(1)
        Input #1, Stepread
        Select Case Stepread
            Case "Step1=1"
                Label2.Caption = "P"
            Case "Step2=1"
                Label5.Caption = "P"
            Case "Step3=1"
                Label7.Caption = "P"
            Case "Step4=1"
                Label9.Caption = "P"
            Case "Step5=1"
                Label12.Caption = "P"
            Case "Step6=1"
                Label14.Caption = "P"
        End Select
    Loop
    Close 1

End Sub

Private Sub UpdateTerminals_Click()
    x = Shell("c:\bneapps\startedm.exe")
    Label2.Caption = "P"
    JobLog ("Step1=1")
    FN = DriveLetter + ":\SERVER IMAGE\UNIT.TXT"
    FileCopy "c:\BNEAPPS\ID\UNIT.TXT", FN
End Sub
