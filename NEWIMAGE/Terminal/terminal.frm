VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "IRIS New Server Stand Alone Push and Pull Utility"
   ClientHeight    =   6120
   ClientLeft      =   180
   ClientTop       =   705
   ClientWidth     =   10830
   Icon            =   "terminal.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6120
   ScaleWidth      =   10830
   Begin VB.ListBox List2 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Left            =   3840
      TabIndex        =   7
      Top             =   3480
      Width           =   6855
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Left            =   3840
      TabIndex        =   5
      Top             =   1680
      Width           =   6855
   End
   Begin VB.CommandButton Command3 
      Caption         =   "EXIT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   120
      TabIndex        =   4
      Top             =   4800
      Width           =   3615
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Pull"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   120
      TabIndex        =   3
      Top             =   3000
      Width           =   3615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Push"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Width           =   3615
   End
   Begin VB.Label Label5 
      Caption         =   "Version 1"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   615
      Left            =   4320
      TabIndex        =   10
      Top             =   5400
      Width           =   6015
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   4440
      TabIndex        =   9
      Top             =   4680
      Width           =   5775
   End
   Begin VB.Label Label2 
      Caption         =   "C:\IRIS\DATA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   8
      Top             =   3000
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   6
      Top             =   1200
      Width           =   1935
   End
   Begin VB.Label TerminalLabel 
      Alignment       =   2  'Center
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   615
      Left            =   3480
      TabIndex        =   1
      Top             =   240
      Width           =   3375
   End
   Begin VB.Label DriveLabel 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8760
      TabIndex        =   0
      Top             =   120
      Width           =   1935
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Response = MsgBox("This process is completed BEFORE the terminal is reimaged.  Do you want to continue?", vbYesNo + vbQuestion + vbDefaultButton2, "Confirm Pull")
If Response = vbYes Then    ' User chose Yes.
    List1.Clear
    List1.Refresh
    If Dir(DriveLetter + ":\REG" + LTrim(RegNum) + "\*.MDB") <> "" Then
        Kill (DriveLetter + ":\REG" + LTrim(RegNum) + "\*.*")
    End If
    FileCopy "c:\iris\data\poslive.mdb", DriveLetter + ":\REG" + LTrim(RegNum) + "\POSLIVE.MDB"
        fdt = FileDateTime(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSLIVE.MDB")
        fl = FileLen(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSLIVE.MDB")
        List1.AddItem ("POSLIVE.MDB " + Str(fdt) + " " + Str(fl))
        List1.Refresh
    FileCopy "c:\iris\data\pospend.mdb", DriveLetter + ":\REG" + LTrim(RegNum) + "\POSPEND.MDB"
        fdt = FileDateTime(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSPEND.MDB")
        fl = FileLen(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSPEND.MDB")
        List1.AddItem ("POSPEND.MDB " + Str(fdt) + " " + Str(fl))
        List1.Refresh
    If Dir("c:\iris\data\cctran.dat") <> "" Then
        FileCopy "c:\iris\data\cctran.dat", DriveLetter + ":\REG" + LTrim(RegNum) + "\CCTRAN.DAT"
            fdt = FileDateTime(DriveLetter + ":\REG" + LTrim(RegNum) + "\CCTRAN.DAT")
            fl = FileLen(DriveLetter + ":\REG" + LTrim(RegNum) + "\CCTRAN.DAT")
            List1.AddItem ("CCTRAN.DAT  " + Str(fdt) + " " + Str(fl))
    Else
        Label3 = "No unprocessed Credit Cards."
    End If
    
    List1.Refresh
    Label4.Caption = "Pull Complete!"
End If

End Sub

Private Sub Command2_Click()
Response = MsgBox("This process is completed AFTER the terminal is reimaged.  Do you want to continue?", vbYesNo + vbQuestion + vbDefaultButton2, "Confirm Push")
If Response = vbYes Then    ' User chose Yes.

    List2.Clear
    List2.Refresh

    FileCopy DriveLetter + ":\REG" + LTrim(RegNum) + "\POSLIVE.MDB", "c:\iris\data\poslive.mdb"
        fdt = FileDateTime("c:\iris\data\POSLIVE.MDB")
        fl = FileLen("c:\iris\data\POSLIVE.MDB")
        List2.AddItem ("POSLIVE.MDB " + Str(fdt) + " " + Str(fl))
        List2.Refresh
    FileCopy DriveLetter + ":\REG" + LTrim(RegNum) + "\POSPEND.MDB", "c:\iris\data\pospend.mdb"
        fdt = FileDateTime("c:\iris\data\POSPEND.MDB")
        fl = FileLen("c:\iris\data\POSPEND.MDB")
        List2.AddItem ("POSPEND.MDB " + Str(fdt) + " " + Str(fl))
        List2.Refresh
    If Dir(DriveLetter + ":\REG" + LTrim(RegNum) + "\CCTRAN.DAT") <> "" Then
        FileCopy DriveLetter + ":\REG" + LTrim(RegNum) + "\CCTRAN.DAT", "c:\iris\data\cctran.dat"
            fdt = FileDateTime("c:\iris\data\CCTRAN.DAT")
            fl = FileLen("c:\iris\data\CCTRAN.DAT")
            List2.AddItem ("CCTRAN.DAT  " + Str(fdt) + " " + Str(fl))
    Else
   ' x = MsgBox("No Credit Card Data File.  You will only see POSLIVE and POSPEND in the file list box.", vbInformation, "No Unprocessed Credit Cards")
    End If
    
    List2.Refresh
    Label4.Caption = "Push Complete!"
End If

End Sub

Private Sub Command3_Click()
    End
End Sub

Private Sub Form_Load()
On Error Resume Next
If UCase(Dir("d:\server image\install script.sql")) <> "" Then

    DriveLabel.Caption = "Flash Drive is D:"
    DriveLetter = "D"
End If
If UCase(Dir("e:\server image\install script.sql")) <> "" Then

    DriveLabel.Caption = "Flash Drive is E:"
    DriveLetter = "E"
End If
If UCase(Dir("f:\server image\install script.sql")) <> "" Then

    DriveLabel.Caption = "Flash Drive is F:"
    DriveLetter = "F"
End If
If UCase(Dir("G:\server image\install script.sql")) <> "" Then

    DriveLabel.Caption = "Flash Drive is G:"
    DriveLetter = "G"
End If

If DriveLetter = "" Then

    DriveLabel.Caption = "Flash Drive is ?:"
    x = MsgBox("This setup program only supports Drives D, E, F, and G.  Please contact Software Support.", vbCritical, "Invalid Drive Letter")
    End
End If
Label1.Caption = DriveLetter + ":"

Open "c:\iris\ini\appini.ini" For Input As 1
Do While Not EOF(1)
    Line Input #1, LineData
    If Mid$(LineData, 1, 6) = "REGNUM" Then
        RegNum = Mid$(LineData, 8, 1)
        TerminalLabel = "Terminal # " + RegNum
    End If
Loop

Label1.Caption = DriveLetter + ":\REG" + RegNum

Close #1

Command1.Caption = "PULL" + Chr(10) + "Before Terminal Image"
Command2.Caption = "PUSH" + Chr(10) + "After Terminal Image"
fn = DriveLetter + ":\REG" + LTrim(RegNum)


    fdt = ""
    fl = ""
    fdt = FileDateTime(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSLIVE.MDB")
    fl = FileLen(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSLIVE.MDB")
    List1.AddItem ("POSLIVE.MDB " + Str(fdt) + " " + Str(fl))
    fdt = ""
    fl = ""
    fdt = FileDateTime(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSPEND.MDB")
    fl = FileLen(DriveLetter + ":\REG" + LTrim(RegNum) + "\POSPEND.MDB")
    List1.AddItem ("POSPEND.MDB " + Str(fdt) + " " + Str(fl))
    fdt = ""
    fl = ""
    fdt = FileDateTime(DriveLetter + ":\REG" + LTrim(RegNum) + "\CCTRAN.DAT")
    fl = FileLen(DriveLetter + ":\REG" + LTrim(RegNum) + "\CCTRAN.DAT")
    List1.AddItem ("CCTRAN.DAT  " + Str(fdt) + " " + Str(fl))
    fdt = ""
    fl = ""
    fdt = FileDateTime("c:\iris\data\POSLIVE.MDB")
    fl = FileLen("c:\iris\data\POSLIVE.MDB")
    List2.AddItem ("POSLIVE.MDB " + Str(fdt) + " " + Str(fl))
    fdt = ""
    fl = ""
    fdt = FileDateTime("c:\iris\data\POSPEND.MDB")
    fl = FileLen("c:\iris\data\POSPEND.MDB")
    List2.AddItem ("POSPEND.MDB " + Str(fdt) + " " + Str(fl))
    fdt = ""
    fl = ""
    fdt = FileDateTime("c:\iris\data\CCTRAN.DAT")
    fl = FileLen("c:\iris\data\CCTRAN.DAT")
    List2.AddItem ("CCTRAN.DAT  " + Str(fdt) + " " + Str(fl))
    If fdt = "" Then
        Label3 = "No unprocessed Credit Cards"
    End If
End Sub

