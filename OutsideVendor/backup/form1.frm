VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Outside Vendor: Process Export.prn to the Restaurants LiveReporter Daily Folder"
   ClientHeight    =   4185
   ClientLeft      =   1740
   ClientTop       =   1800
   ClientWidth     =   9450
   Icon            =   "form1.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4185
   ScaleWidth      =   9450
   Begin VB.CommandButton Command2 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   240
      TabIndex        =   1
      Top             =   2760
      Width           =   2055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Process File"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   2055
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "Please contact Jody Smith for program support."
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   2520
      TabIndex        =   7
      Top             =   3840
      Width           =   6855
   End
   Begin VB.Label Label5 
      Caption         =   "[SAVE AS TYPE] Formatted Text (Space delimited)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   6
      Top             =   3240
      Width           =   6855
   End
   Begin VB.Label Label4 
      Caption         =   "[FILE NAME] c:\OutsideVendor\Export "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   5
      Top             =   2760
      Width           =   6855
   End
   Begin VB.Label Label3 
      Caption         =   "Save As [DRIVE] C$ on 'Client(V:) "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   4
      Top             =   2280
      Width           =   6855
   End
   Begin VB.Label Label2 
      Caption         =   "Export the Outside Vendors by Site Report from Cross Forms.  Use the Report default options."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2520
      TabIndex        =   3
      Top             =   1560
      Width           =   6855
   End
   Begin VB.Label Label1 
      Caption         =   "Please complete the export file."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   615
      Left            =   2520
      TabIndex        =   2
      Top             =   720
      Width           =   6735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
On Error GoTo ErrorHandler

Label1.Caption = "Opening Export.prn"
NumberRestaurants = 0
Open "c:\outsidevendor\Export.Prn" For Input As #1
Do While Not EOF(1)
    DoEvents
    Line Input #1, LineData
    If Mid$(LineData, 1, 1) = "#" Then
        RestNum = Mid$(LineData, 2, 4)
        NumberRestaurants = NumberRestaurants + 1
        Label1.Caption = "Processing " + RestNum
    End If
    Select Case Mid$(LineData, 1, 14)
        Case "OUTSIDE VENDOR"
            If NumberRestaurants <> 0 Then
                Close #2
                RestFileName = "\\bnewest\share1\fieldreports\" + RestNum + "\Outside Vendors.bnr"
                If Val(RestNum) < 8000 And RestNum <> "1000" Then
                'If Val(RestNum) < 8000 And RestNum <> "1000" And RestNum <> "6502" And RestNum <> "7777" Then
                    FileCopy "c:\outsidevendor\TEMP.BNR", RestFileName
                End If
            End If
            Open "c:\outsidevendor\TEMP.BNR" For Output As #2
            Print #2, LineData
            Print #2,
            Print #2, "The following Outside Vendor listing has been compiled to assist you when outside "
            Print #2, "service is needed in your restaurant."
            Print #2,
            Print #2, "*********************************************************************************"
            Print #2,
            Print #2, "IF THIS RESTAURANT HAS BEEN OPENED FOR LESS THAN 12 MONTHS, MANAGEMENT SHOULD"
            Print #2, "CONTACT FACILITIES MANAGEMENT TO MAKE SURE THE CONTRACTORS THAT BUILT THE STORE"
            Print #2, "ARE BEING CONTACTED.  IF NO ONE CAN BE CONTACTED, USE THE VENDOR LIST BELOW"
            Print #2, "FOR EMERGENCY SERVICE."
            Print #2,
            Print #2, "*********************************************************************************"
            Print #2,
            Print #2, "A Purchase Order will be required when calling any of these outside vendors.   "
            Print #2, "If you experience any problems when calling any of these vendors, please contact"
            Print #2, "the Regional Facility Manager (RFM)."
            Print #2, ""
            Print #2, "You may call the Help Desk Services at 1-800-773-8983 ext 1437, prior to issuing "
            Print #2, "a purchase order to a vendor, to determine if a Service Technician is available  "
            Print #2, "to assist you with the equipment problem. "
            Print #2,
            Print #2, "*********************************************************************************"
            Print #2,

        Case "The following "
            'Don't Print
        Case Else
            Print #2, LineData
        End Select
Loop
Close #1
        Label1.Caption = "Process complete!"
        Command1.Enabled = False


Exit Sub        ' Exit to avoid handler.
ErrorHandler:   ' Error-handling routine.
    Select Case Err.Number  ' Evaluate error number.
        Case 76 ' "File already open" error.
            x = MsgBox("Error copying restaurant #" + RestNum + ".  If this is a valid operating restaurant, please contact Jody Smith.", vbInformation, "Restaurant Validation")
        Case Else
            ' Handle other situations here...
    End Select
    Resume Next ' Resume execution at same line
                ' that caused the error.
End Sub
Private Sub Command2_Click()
    End
End Sub

Private Sub Form_Load()
    If Dir("C:\OUTSIDEVENDOR\*.*") = "" Then
        MkDir ("c:\OutsideVendor")
        Open "c:\outsidevendor\TEMP.BNR" For Output As #2
        Print #2, "Starter File"
        Close #2
        Label1.Caption = "Export File Missing."
        Command1.Enabled = False
    Else
        If Dir("C:\OUTSIDEVENDOR\EXPORT.PRN") <> "" Then
            TimeStamp = FileDateTime("C:\OUTSIDEVENDOR\EXPORT.PRN")
            Label1.Caption = ("Export file created " + Str(TimeStamp))
            Command1.Enabled = True
        End If
    End If
End Sub

