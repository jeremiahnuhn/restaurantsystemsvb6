VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "BNE IRIS Employee Editor"
   ClientHeight    =   7725
   ClientLeft      =   795
   ClientTop       =   885
   ClientWidth     =   12210
   FillStyle       =   0  'Solid
   Icon            =   "BNEEmpEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7725
   ScaleWidth      =   12210
   Begin VB.CommandButton Command5 
      Caption         =   "Remove Employee from IRIS and LaborPro (Transferred Employees)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   6240
      TabIndex        =   4
      Top             =   3600
      Width           =   4815
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Rehire Instructions"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   6240
      TabIndex        =   3
      Top             =   2280
      Width           =   4815
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1200
      TabIndex        =   0
      Top             =   4920
      Width           =   9855
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Add Employee from another Restaurant (Temporary Transfer)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1200
      TabIndex        =   2
      Top             =   3600
      Width           =   4815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Change an Employee's Password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1200
      TabIndex        =   1
      Top             =   2280
      Width           =   4815
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Version 5 (Windows 10)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9960
      TabIndex        =   7
      Top             =   7440
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   $"BNEEmpEditor.frx":0442
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   720
      TabIndex        =   6
      Top             =   6600
      Width           =   10935
   End
   Begin VB.Label Label1 
      Caption         =   $"BNEEmpEditor.frx":04D5
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   240
      TabIndex        =   5
      Top             =   240
      Width           =   11655
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Password.Show
Form1.Hide
End Sub

Private Sub Command2_Click()
Transfer.Show
Form1.Hide
End Sub

Private Sub Command3_Click()
    End
End Sub

Private Sub Command4_Click()
   If Dir("C:\Program Files\Adobe\Acrobat DC\Acrobat\Acrobat.exe") <> "" Then

            exe = "C:\Program Files\Adobe\Acrobat DC\Acrobat\Acrobat.exe" + " /n " + Chr(34) + "c:\Reports\IRIS Manual\Rehire Instructions.pdf" + Chr(34)
    Else
            exe = "C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /n " + Chr(34) + "c:\Reports\IRIS Manual\Rehire Instructions.pdf" + Chr(34)
    End If
    x = Shell(exe, vbMaximizedFocus)


End Sub

Private Sub Command5_Click()
Remove.Show
Form1.Hide
End Sub

Private Sub Form_Load()
       Open "c:\support\EMPPassword.BAT" For Output As #1
        Print #1, "REM Created " + Str(Now())
        'If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
            Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\support\EMPPassword.sql -o " + Chr(34) + "c:\support\EMPPassword.TXT" + Chr(34)
            Print #1, "Exit"
        'Else
         '   Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\support\EMPPassword.sql -o " + Chr(34) + "c:\support\EMPPassword.TXT" + Chr(34)
         '   Print #1, "Exit"
        'End If
      Close #1
       Open "c:\support\EMPPassword.SQL" For Output As #1
        Print #1, "SELECT tblEmployees.EmployeeID,LastName, FirstName"
        Print #1, "FROM tblEmployees Where StatusID <> 3 and HomePhone <> '2529372000'"
      Close #1

      y = Shell("c:\support\EMPPassword.BAT", vbMinimizedNoFocus)

End Sub
