VERSION 5.00
Begin VB.Form Employee 
   Caption         =   "Select the Employee"
   ClientHeight    =   10125
   ClientLeft      =   990
   ClientTop       =   885
   ClientWidth     =   9165
   LinkTopic       =   "Form2"
   ScaleHeight     =   10125
   ScaleWidth      =   9165
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   7080
      TabIndex        =   3
      Top             =   5520
      Width           =   1815
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6540
      Left            =   720
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   1440
      Width           =   6135
   End
   Begin VB.Label Label3 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   4
      Top             =   120
      Width           =   7095
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   480
      TabIndex        =   1
      Top             =   600
      Width           =   6855
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   495
      Left            =   9000
      TabIndex        =   0
      Top             =   360
      Width           =   15
   End
End
Attribute VB_Name = "Employee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
End
End Sub

Private Sub Form_Load()
Label2.Caption = "Click on the employee you want to add to your IRIS system from the list below."
Label3.Caption = "You selected " + SelectedRestaurant


Open "c:\support\employeeinformation.csv" For Input As #1
    Line Input #1, IndividualLine
    Do While Not EOF(1)

    Line Input #1, IndividualLine
    FileInput = Split(IndividualLine, ",")
    UnitNumber = FileInput(0)
    EmpNumber = FileInput(1)
    Junk1 = FileInput(2)
    Junk2 = FileInput(3)
    EmpName = FileInput(4)
    TempName = Format(Mid$(EmpName, 1, 25), "@@@@@@@@@@@@@@@@@@@@!")
    If UnitNumber = Mid$(SelectedRestaurant, 31, 4) Then
        List1.AddItem TempName + " " + EmpNumber
    End If
    Loop
    Close 1
End Sub

Private Sub List1_Click()
SelectedEmployee = List1.Text
EmployeeInfo.Show
Employee.Hide
End Sub
