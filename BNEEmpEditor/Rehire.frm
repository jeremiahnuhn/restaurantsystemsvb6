VERSION 5.00
Begin VB.Form Rehire 
   Caption         =   "Rehire Instructions"
   ClientHeight    =   7530
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11145
   LinkTopic       =   "Form2"
   ScaleHeight     =   7530
   ScaleWidth      =   11145
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2640
      TabIndex        =   1
      Top             =   6360
      Width           =   6135
   End
   Begin VB.Label Label3 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   240
      TabIndex        =   3
      Top             =   3960
      Width           =   10575
   End
   Begin VB.Label Label2 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   240
      TabIndex        =   2
      Top             =   2040
      Width           =   10575
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   10575
   End
End
Attribute VB_Name = "Rehire"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Form_Load()
Label1.Caption = "1. Candidate has to apply the position � same as a new candidate" + Chr(10) + "     a. They will update their application" + Chr(10) + "     b. GM sends the DM request to approve rehire" + Chr(10) + "     c. DM electronically approves the rehire"

Label2.Caption = "2. If GM is interested in moving forward" + Chr(10) + "     a. GM moves candidate forward in the interview process" + Chr(10) + "     b. Candidate completes WOTC" + Chr(10) + "     c. Candidate completes background form (or updates any form already there) and         sign off giving permission to order background."

Label3.Caption = "3. GM orders background" + Chr(10) + Chr(10) + "4. GM�s can proceed with onboarding prior to the background results return � or wait     until the background results return."

End Sub
