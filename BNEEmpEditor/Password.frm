VERSION 5.00
Begin VB.Form Password 
   Caption         =   "Change an Employee's Password"
   ClientHeight    =   8730
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10305
   Icon            =   "Password.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   8730
   ScaleWidth      =   10305
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Change Password"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   4920
      TabIndex        =   9
      Top             =   4200
      Width           =   3615
   End
   Begin VB.TextBox Password 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   7320
      TabIndex        =   7
      Top             =   3240
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   7440
      TabIndex        =   1
      Top             =   7080
      Width           =   1215
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7305
      Left            =   360
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   840
      Width           =   3855
   End
   Begin VB.Label Label5 
      Caption         =   "Enter the password for this employee to use on IRIS.  Numbers only."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   4560
      TabIndex        =   8
      Top             =   3120
      Width           =   2655
   End
   Begin VB.Label EmpId 
      Caption         =   "Label4"
      Height          =   615
      Left            =   4800
      TabIndex        =   6
      Top             =   5880
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label Label2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4560
      TabIndex        =   5
      Top             =   2280
      Width           =   5415
   End
   Begin VB.Label Label3 
      Caption         =   "Selected Employee:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4440
      TabIndex        =   4
      Top             =   1080
      Width           =   4215
   End
   Begin VB.Label FullName 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4440
      TabIndex        =   3
      Top             =   1680
      Width           =   5415
   End
   Begin VB.Label Label1 
      Caption         =   "Select the Employee's name from the list below:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   6255
   End
End
Attribute VB_Name = "Password"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()

response = MsgBox("This process will change " + LTrim(RTrim(FullName)) + " password on the IRIS system.  Do you want to continue?", vbYesNo, "Confirm correct Employee to change password")
If response = vbYes Then
  
    If Password.Text = "" Then
        PasswordResponse = MsgBox("Please enter a password for the Employee to use on IRIS.  Numbers only.", vbExclamation, "Enter Password")
        Exit Sub
    End If
    
    If IsNumeric(Password.Text) = False Then
        PasswordResponse = MsgBox("Please enter a password for the Employee to use on IRIS.  Numbers only.", vbExclamation, "Password must be only numbers")
        Password.Text = ""
        Exit Sub
    End If

 Open "c:\support\EMPPASSCHANGE.BAT" For Output As #1
        Print #1, "REM Created " + Str(Now())
        'If Dir("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Binn\XPStar90.dll") <> "" Then
            Print #1, "cd " + Chr(32) + "c:\Program Files\Microsoft SQL Server\90\Tools\Binn" + Chr(32)
            Print #1, "sqlcmd -S (local)\XSIRIS -d Iris -i c:\support\EMPPASSCHANGE.SQL"
            Print #1, "Exit"
        'Else
         '   Print #1, "osql -S (local)\XSIRIS -U sa -P -n -d Iris -i c:\support\EMPPASSCHANGE.SQL"
          '  Print #1, "Exit"
       ' End If
      Close #1
       Open "c:\support\EMPPASSCHANGE.SQL" For Output As #1
        Print #1, "UPDATE [IRIS].[dbo].[tblemployees] Set password = " + Chr(39) + Password.Text + Chr(39) + " Where EmployeeID = " + EmpID.Caption
      Close #1
             Open "c:\BNEApps\IRISPassword.Log" For Append As #1
                Print #1, Now(), " Employee ID " + EmpID.Caption + " Password " + Password.Text
             Close #1

      y = Shell("c:\support\EMPPASSCHANGE.BAT", vbHidden)
      
      
      
      End
End If

End Sub

Private Sub Form_Load()





    If Dir("c:\support\EmpPassword.TXT") <> "" Then
        Open "c:\support\EmpPassword.TXT" For Input As #1
        Line Input #1, LineData
        Line Input #1, LineData
        
        Do While Not EOF(1)
        Line Input #1, LineData
        
        If IsNumeric(Mid$(LineData, 1, 11)) = True Then
            EmpIDPass = Mid$(LineData, 1, 11)
            LNamePass = Mid$(LineData, 13, 20)
            FNamePass = Mid$(LineData, 33, 20)
            FullNamePass = RTrim(FNamePass) + " " + RTrim(LNamePass)
            
    Select Case LTrim(Mid$(LineData, 1, 11))
        Case "8"
            ' Do Nothing
        Case "9"
            ' Do Nothing
        Case "10"
            ' Do Nothing
        Case Else
            List1.AddItem FullNamePass + "                    ," + LTrim(EmpIDPass)
        End Select
        End If
        Loop
        Close 1
    End If
End Sub

Private Sub List1_Click()


Selected = Split(List1.Text, ",")
FullName.Caption = Selected(0)
Label2.Caption = "Employee ID " + Selected(1)
EmpID.Caption = Selected(1)
Command2.Enabled = True


End Sub
