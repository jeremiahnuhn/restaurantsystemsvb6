VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "LaborPro 2010 Assessment"
   ClientHeight    =   8865
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8640
   LinkTopic       =   "Form1"
   ScaleHeight     =   8865
   ScaleWidth      =   8640
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List2 
      Height          =   2985
      Left            =   480
      TabIndex        =   5
      Top             =   2640
      Width           =   7575
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   480
      TabIndex        =   2
      Top             =   600
      Width           =   7575
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Create Files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   1440
      TabIndex        =   1
      Top             =   6480
      Width           =   2775
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   4560
      TabIndex        =   0
      Top             =   6480
      Width           =   2295
   End
   Begin VB.Label Label6 
      Caption         =   "Number of restaurants that have not completed the assessment"
      Height          =   255
      Left            =   480
      TabIndex        =   9
      Top             =   5640
      Width           =   5175
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6720
      TabIndex        =   8
      Top             =   5640
      Width           =   1335
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Label2"
      Height          =   255
      Left            =   6840
      TabIndex        =   7
      Top             =   2400
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "Assessment completed at the following restaurants"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   2400
      Width           =   5175
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Label2"
      Height          =   255
      Left            =   6840
      TabIndex        =   4
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "All Restaurants"
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   360
      Width           =   1215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    End
End Sub

Private Sub Command2_Click()
Dim Junk1, Junk2, StartDate, EndDate, IP, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, RestNum, GMName As String
Open "\\bnewest\share1\polling\Data\Labor Pro Assessment\Score.csv" For Output As #2
Open "\\bnewest\share1\polling\Data\Labor Pro Assessment\Sheet_1.csv" For Input As #1
Do While Not EOF(1)
    RestScore = 0
    RestMissed = ""
    Input #1, Junk1, Junk2, StartDate, EndDate, IP, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, RestNum, GMName
        If Mid$(Q1, 1, 1) = "B" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "1 "
        End If
        If Mid$(Q2, 1, 1) = "D" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "2 "
        End If
        If Mid$(Q3, 1, 1) = "B" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "3 "
        End If
        If Mid$(Q4, 1, 1) = "D" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "4 "
        End If
        If Mid$(Q5, 1, 1) = "C" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "5 "
        End If
        If Mid$(Q6, 1, 1) = "A" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "6 "
        End If
        If Mid$(Q7, 1, 1) = "B" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "7 "
        End If
        If Mid$(Q8, 1, 1) = "C" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "8 "
        End If
        If Mid$(Q9, 1, 1) = "C" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "9 "
        End If
        If Mid$(Q10, 1, 1) = "C" Then
            RestScore = RestScore + 10
        Else
            RestMissed = RestMissed + "10 "
        End If


        Print #2, Str(RestNum) + "," + GMName + "," + Str(RestScore) + "," + RestMissed
        RptFN = "\\bnewest\share1\fieldreports\" + LTrim(RTrim(Str(RestNum))) + "\LaborPro Assessment #2 Questions.PDF"
        FileCopy "\\bnewest\share1\polling\Data\Labor Pro Assessment\Doc.PDF", RptFN
        RptFN = "\\bnewest\share1\fieldreports\" + LTrim(RTrim(Str(RestNum))) + "\LaborPro Assessment #2 Score.BNR"
'        RptFN = "v:\data\Labor Pro Assessment\FieldRpts\" + LTrim(RTrim(Str(RestNum))) + " LaborPro Assessment Score.BNR"

        Open RptFN For Output As #3
        Print #3,
        Print #3,
        Print #3, "LaborPro 2010 Knowledge Assessment #2 Score"
        Print #3,
        Print #3, "Restaurant Number: " + Str(RestNum)
        Print #3,
        Print #3, "Assessment Completed By: " + GMName
        Print #3,
        Print #3, "Assessment Score: " + Str(RestScore)
        Print #3,
        Print #3, "Missed Questions: " + RestMissed
        Print #3,
        Print #3, "View the LaborPro Assessment #2 Questions.PDF to review any missed questions."
        Print #3,
        Print #3,
        Close #3
  Loop
  Close (1)
  Close (2)
  End
End Sub

Private Sub Form_Load()
  Open "\\bnewest\share1\polling\Data\Restaurant Listing.csv" For Input As #1
  Do Until EOF(1)
    Line Input #1, unT
        If Mid$(unT, 1, 1) = "H" Then
            List1.AddItem unT
        End If
  Loop
  Close (1)
  Label2.Caption = List1.ListCount
  
Open "\\bnewest\share1\polling\Data\Labor Pro Assessment\Sheet_1.csv" For Input As #1
Do While Not EOF(1)
    RestScore = 0
    RestMissed = ""
    Input #1, Junk1, Junk2, StartDate, EndDate, IP, Junk3, Junk4, Junk5, Junk6, Junk7, Junk8, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, RestNum, GMName

        List2.AddItem RestNum

Loop
  Label4.Caption = List2.ListCount
  Label5.Caption = List1.ListCount - List2.ListCount
Close #1
' Missing
Open "\\bnewest\share1\polling\Data\Labor Pro Assessment\Missing.csv" For Output As #5
For x = 0 To List1.ListCount - 1
    Found = False
    For Y = 0 To List2.ListCount - 1
        If Mid$(List1.List(x), 4, 4) = List2.List(Y) Then
            Found = True
        End If
    Next Y
    If Found = False Then
        Print #5, List1.List(x)
    End If
Next x
Close #5

End Sub

