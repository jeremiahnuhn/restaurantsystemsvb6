VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Process Credit Card Refunds"
   ClientHeight    =   6810
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13590
   Icon            =   "REFUND.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6810
   ScaleWidth      =   13590
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command5 
      Caption         =   "Stop Services"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   11160
      TabIndex        =   17
      Top             =   4200
      Width           =   2295
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Copy Batch Again"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   11160
      TabIndex        =   16
      Top             =   3240
      Width           =   2295
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1320
      TabIndex        =   15
      Top             =   6000
      Width           =   9735
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Stop Services and Clean up drive including Flash Drive"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1320
      TabIndex        =   10
      Top             =   5160
      Width           =   9735
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Start CCA Router and FDMS Services"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1320
      TabIndex        =   8
      Top             =   4200
      Width           =   9735
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Start CCA Manager"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1320
      TabIndex        =   6
      Top             =   3240
      Width           =   9735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Copy Files from Flash Drive and Edit PSBATTRN.INI"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1320
      TabIndex        =   4
      Top             =   2280
      Width           =   9735
   End
   Begin VB.TextBox RestNum 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2880
      TabIndex        =   0
      Top             =   360
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Make sure drive is clean of previous files"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1320
      TabIndex        =   1
      Top             =   1320
      Width           =   9735
   End
   Begin VB.Label Label12 
      Caption         =   "Files: \Refund\####\PSBATTRN.INI  -  TPE_FDMS.INI  -  CCB###.dat"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5160
      TabIndex        =   20
      Top             =   720
      Width           =   8295
   End
   Begin VB.Label Label11 
      Caption         =   "Don't forget to Settle the Credit Card Batch."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   5160
      TabIndex        =   19
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Verion 1.1"
      Height          =   255
      Left            =   12360
      TabIndex        =   18
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label Label10 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   11280
      TabIndex        =   14
      Top             =   1920
      Width           =   2175
   End
   Begin VB.Label Label9 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   11280
      TabIndex        =   13
      Top             =   2640
      Width           =   2295
   End
   Begin VB.Label Label8 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   11280
      TabIndex        =   12
      Top             =   2280
      Width           =   2295
   End
   Begin VB.Label Label7 
      Caption         =   "P"
      BeginProperty Font 
         Name            =   "Wingdings 2"
         Size            =   48
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   855
      Left            =   480
      TabIndex        =   11
      Top             =   5040
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label5 
      Caption         =   "P"
      BeginProperty Font 
         Name            =   "Wingdings 2"
         Size            =   48
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   855
      Left            =   480
      TabIndex        =   9
      Top             =   4080
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "P"
      BeginProperty Font 
         Name            =   "Wingdings 2"
         Size            =   48
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   855
      Left            =   480
      TabIndex        =   7
      Top             =   3120
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "P"
      BeginProperty Font 
         Name            =   "Wingdings 2"
         Size            =   48
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   855
      Left            =   480
      TabIndex        =   5
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "P"
      BeginProperty Font 
         Name            =   "Wingdings 2"
         Size            =   48
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000C000&
      Height          =   855
      Left            =   480
      TabIndex        =   3
      Top             =   1200
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Restaurant Number or Folder Name"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   480
      TabIndex        =   2
      Top             =   360
      Width           =   2295
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()


'Same process as the STARTNEW.BAT
If Dir("C:\Program Files\xpient Solutions\Credit Card Application\getcct.txt") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\getcct.txt"
End If
If Dir("C:\Program Files\xpient Solutions\Credit Card Application\Reports\CCATransactions_empty.mdb") <> "" Then
    FileCopy "C:\Program Files\xpient Solutions\Credit Card Application\Reports\CCATransactions_empty.mdb", "C:\Program Files\xpient Solutions\Credit Card Application\Reports\CCATransactions.mdb"
End If
If Dir("C:\Program Files\xpient Solutions\Credit Card Application\cctran.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\cctran.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\cctran.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\cctran.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\history.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\history.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccb*.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccb*.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccbstaging\ccb*.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccbstaging\ccb*.dat"
End If

If Dir("c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini") <> "" Then
    Kill "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini"
End If

If Dir("c:\program files\xpient solutions\credit card application\fdms\PSBATTRN.INI") <> "" Then
    Kill "c:\program files\xpient solutions\credit card application\fdms\PSBATTRN.INI"
End If
Label2.Visible = True
Form1.Refresh

End Sub

Private Sub Command2_Click()

If RestNum = "" Then
    x = MsgBox("Missing Restaurant Number or Folder Name", vbCritical, "Missing")
    Exit Sub
End If

SF = "e:\refund\" + RTrim(RestNum) + "\*.*"
CF = "C:\Progra~1\xpient~1\Credit~1\Credit\" + RTrim(RestNum)

    Open "C:\REFUND1.bat" For Output As 1
    Print #1, "MD C:\Progra~1\xpient~1\Credit~1\Credit\" + RTrim(RestNum)
    Print #1, "COPY " + SF + " " + CF
    Print #1, "COPY e:\refund\" + RTrim(RestNum) + "\PSBATTRN.INI C:\Progra~1\xpient~1\Credit~1\FDMS"
    Print #1, "COPY e:\refund\" + RTrim(RestNum) + "\TPE_FDMS.INI C:\Progra~1\xpient~1\Credit~1\FDMS"
    Print #1, "COPY e:\refund\" + RTrim(RestNum) + "\CCB*.dat C:\Progra~1\xpient~1\Credit~1\FDMS"
    Print #1, "EXIT"
    Close 1
Y = Shell("C:\REFUND1.bat", vbMinimizedFocus)
For x = 1 To 500000
    DoEvents
Next x

Open "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\TPE_FDMS.INI" For Input As 1
  Do Until EOF(1)
    Line Input #1, unt
        If Mid$(unt, 1, 6) = "STORE=" Then
            Label10.Caption = unt
        End If
  Loop
Close (1)

Open "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\PSBATTRN.INI" For Input As 1
    Line Input #1, unt2
    Line Input #1, unt
    Label8.Caption = "Batch # " + Mid$(unt, 11, 4)
Close (1)

If Val(Mid$(unt, 11, 4)) < 500 Then
    NEWBATCH# = Val(Mid$(unt, 11, 4) + 300)
Else
    NEWBATCH# = Val(Mid$(unt, 11, 4) - 300)
End If

Open "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\PSBATTRN.INI" For Output As 1
    Print #1, unt2
    Print #1, "iBatchNum=" + (Format(NEWBATCH#, "0000"))
    Label9.Caption = "Refund # " + Format$(NEWBATCH#, "0000")
Close (1)

Label7.Visible = False

Label3.Visible = True
Form1.Refresh
End Sub

Private Sub Command3_Click()
    x = Shell("C:\Program Files\xpient Solutions\Credit Card Application\CCAManager\CCAManager.exe -nl", vbNormalFocus)
Label4.Visible = True
End Sub

Private Sub Command4_Click()

ChDir ("C:\Program Files\xpient Solutions\Credit Card Application")
    x = Shell("C:\Program Files\xpient Solutions\Credit Card Application\CCARouter.exe", vbMinimizedNoFocus)
ChDir ("C:\Program Files\xpient Solutions\Credit Card Application\FDMS")
    x = Shell("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\TPE_FDMS.exe", vbMinimizedNoFocus)
Label5.Visible = True
End Sub

Private Sub Command5_Click()
        Open "c:\refund4.bat" For Output As #6
        Print #6, "cd C:\Progra~1\xpient~1\Credit~1\FDMS"
        Print #6, "TASKKILL /F /IM TPE_FDMS.EXE"
        Print #6, "cd C:\Progra~1\xpient~1\Credit~1"
        Print #6, "TASKKILL /F /IM CCAROUTER.EXE"
        Print #6, "exit"
        Close #6
x = Shell("c:\refund4.bat", vbMinimizedNoFocus)
End Sub

Private Sub Command6_Click()

    Open "C:\REFUND3.bat" For Output As 1
    Print #1, "COPY C:\Progra~1\xpient~1\Credit~1\FDMS\CCBBackup\*.* C:\Progra~1\xpient~1\Credit~1\Credit\+ RTrim(RestNum)"
    Print #1, "Del /q C:\Progra~1\xpient~1\Credit~1\FDMS\CCBBackup\*.*"
    Print #1, "EXIT"
    Close 1
Y = Shell("C:\REFUND3.bat", vbMinimizedFocus)

Open "c:\refund4.bat" For Output As #6
        Print #6, "cd C:\Progra~1\xpient~1\Credit~1\FDMS"
        Print #6, "TASKKILL /F /IM TPE_FDMS.EXE"
        Print #6, "cd C:\Progra~1\xpient~1\Credit~1"
        Print #6, "TASKKILL /F /IM CCAROUTER.EXE"
        Print #6, "exit"
        Close #6
x = Shell("c:\refund4.bat", vbMinimizedNoFocus)

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\getcct.txt") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\getcct.txt"
End If

FileCopy "C:\Program Files\xpient Solutions\Credit Card Application\Reports\CCATransactions.mdb", "C:\Program Files\xpient Solutions\Credit Card Application\Credit\" + RTrim(RestNum) + "\CCATransactions.mdb"


If Dir("C:\Program Files\xpient Solutions\Credit Card Application\Reports\CCATransactions_empty.mdb") <> "" Then
    FileCopy "C:\Program Files\xpient Solutions\Credit Card Application\Reports\CCATransactions_empty.mdb", "C:\Program Files\xpient Solutions\Credit Card Application\Reports\CCATransactions.mdb"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\cctran.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\cctran.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\cctran.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\cctran.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\history.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\history.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccb*.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccb*.dat"
End If

If Dir("C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccbstaging\ccb*.dat") <> "" Then
    Kill "C:\Program Files\xpient Solutions\Credit Card Application\FDMS\ccbstaging\ccb*.dat"
End If

If Dir("c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini") <> "" Then
    Kill "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini"
End If

If Dir("c:\program files\xpient solutions\credit card application\fdms\PSBATTRN.INI") <> "" Then
    Kill "c:\program files\xpient solutions\credit card application\fdms\PSBATTRN.INI"
End If

Kill "E:\REFUND\" + RTrim(RestNum) + "\*.*"
RmDir "E:\REFUND\" + RTrim(RestNum)
Name "C:\Progra~1\xpient~1\Credit~1\Credit\" + RTrim(RestNum) As "C:\Progra~1\xpient~1\Credit~1\Credit\" + RTrim(RestNum) + " completed " + Format(Now(), "MMDDYY")

Label7.Visible = True
Label3.Visible = False
Label4.Visible = False
Label5.Visible = False
Label8.Caption = ""
Label9.Caption = ""
Label10.Caption = ""


Form1.Refresh
End Sub

Private Sub Command7_Click()
End
End Sub

Private Sub Command8_Click()
    Open "C:\REFUND2.bat" For Output As 1
    Print #1, "COPY e:\refund\" + RTrim(RestNum) + "\CCB*.dat C:\Progra~1\xpient~1\Credit~1\FDMS"
    Print #1, "EXIT"
    Close 1
Y = Shell("C:\REFUND2.bat", vbMinimizedFocus)
End Sub

