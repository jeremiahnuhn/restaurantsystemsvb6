VERSION 4.00
Begin VB.Form frmWhilePrinting 
   BorderStyle     =   0  'None
   Caption         =   "       WhilePrinting"
   ClientHeight    =   6465
   ClientLeft      =   360
   ClientTop       =   1170
   ClientWidth     =   8700
   ControlBox      =   0   'False
   Enabled         =   0   'False
   FillStyle       =   0  'Solid
   Height          =   6975
   Left            =   300
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6465
   ScaleWidth      =   8700
   ShowInTaskbar   =   0   'False
   Top             =   720
   Visible         =   0   'False
   Width           =   8820
   WindowState     =   2  'Maximized
   Begin VB.HScrollBar HScroll1 
      Height          =   375
      Left            =   3840
      Max             =   35
      Min             =   1
      MousePointer    =   12  'No Drop
      TabIndex        =   4
      Top             =   2880
      Value           =   1
      Width           =   4575
   End
   Begin VB.TextBox txtprinting 
      Alignment       =   2  'Center
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   2160
      MultiLine       =   -1  'True
      TabIndex        =   2
      Text            =   "WHILEPRI.frx":0000
      Top             =   4200
      Width           =   4815
   End
   Begin VB.PictureBox Picture2 
      AutoSize        =   -1  'True
      Height          =   2355
      Left            =   3720
      Picture         =   "WHILEPRI.frx":008D
      ScaleHeight     =   2295
      ScaleWidth      =   2130
      TabIndex        =   1
      Top             =   120
      Width           =   2190
   End
   Begin VB.PictureBox Picture1 
      Height          =   12
      Left            =   120
      ScaleHeight     =   15
      ScaleWidth      =   135
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   132
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "0 Minutes"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7440
      TabIndex        =   6
      Top             =   3360
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "15 Minutes"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3840
      TabIndex        =   5
      Top             =   3360
      Width           =   1335
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Time remaining to finish printing  report"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   480
      TabIndex        =   3
      Top             =   2880
      Width           =   3135
   End
End
Attribute VB_Name = "frmWhilePrinting"
Attribute VB_Creatable = False
Attribute VB_Exposed = False






