VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Print Utility for Orientation and Training Guides"
   ClientHeight    =   8055
   ClientLeft      =   4485
   ClientTop       =   2220
   ClientWidth     =   7365
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   7365
   Begin VB.CommandButton Command1 
      Caption         =   "Change Menu    Printing Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4800
      TabIndex        =   14
      Top             =   6960
      Width           =   2175
   End
   Begin VB.CommandButton FLEnglish 
      Caption         =   "Frontline Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   360
      TabIndex        =   9
      Top             =   5640
      Width           =   3015
   End
   Begin VB.CommandButton BMSpanish 
      Caption         =   "Biscuit Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3960
      TabIndex        =   8
      Top             =   4560
      Width           =   3015
   End
   Begin VB.CommandButton BMEnglish 
      Caption         =   "Biscuit Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   360
      TabIndex        =   7
      Top             =   4560
      Width           =   3015
   End
   Begin VB.CommandButton BLSpanish 
      Caption         =   "Backline Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3960
      TabIndex        =   6
      Top             =   3480
      Width           =   3015
   End
   Begin VB.CommandButton BLEnglish 
      Caption         =   "Backline Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   360
      TabIndex        =   5
      Top             =   3480
      Width           =   3015
   End
   Begin VB.CommandButton Exit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3960
      Picture         =   "PRINTO.frx":0000
      TabIndex        =   2
      Top             =   5880
      Width           =   3015
   End
   Begin VB.CommandButton Spanish 
      Caption         =   "Orientation Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3960
      Picture         =   "PRINTO.frx":0442
      TabIndex        =   1
      Top             =   2400
      Width           =   3015
   End
   Begin VB.CommandButton English 
      Caption         =   "Orientation Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   360
      Picture         =   "PRINTO.frx":0884
      TabIndex        =   0
      Top             =   2400
      Width           =   3015
   End
   Begin VB.Frame Frame1 
      Caption         =   "English"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Left            =   240
      TabIndex        =   10
      Top             =   2040
      Width           =   3255
   End
   Begin VB.Frame Frame2 
      Caption         =   "Spanish"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Left            =   3840
      TabIndex        =   11
      Top             =   2040
      Width           =   3255
   End
   Begin VB.Label RoastBeeflbl 
      Alignment       =   2  'Center
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   13
      Top             =   7440
      Width           =   4575
   End
   Begin VB.Label Chickenlbl 
      Alignment       =   2  'Center
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   12
      Top             =   6960
      Width           =   4335
   End
   Begin VB.Label Label2 
      Caption         =   $"PRINTO.frx":0CC6
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   480
      TabIndex        =   4
      Top             =   240
      Width           =   6375
   End
   Begin VB.Label Label1 
      Caption         =   "BNE Version 4"
      Height          =   255
      Left            =   6120
      TabIndex        =   3
      Top             =   7800
      Width           =   1095
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub BLEnglish_Click()
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BACKLINE ENGLISH
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00214.000", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00181.000", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00179.000", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00210.000", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00186.000", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00206.000", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00204.000", "C:\PDFPRINT\TEMP\H.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00184.000", "C:\PDFPRINT\TEMP\I.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00187.000", "C:\PDFPRINT\TEMP\J.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00183.000", "C:\PDFPRINT\TEMP\K.PDF"
'FRIED CHICKEN
If Chicken = True Then
    FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00192.000", "C:\PDFPRINT\TEMP\L.PDF"
End If
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00182.000", "C:\PDFPRINT\TEMP\M.PDF"
'ROAST BEEF
If RoastBeef = True Then
    FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00188.000", "C:\PDFPRINT\TEMP\N.PDF"
End If
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00208.000", "C:\PDFPRINT\TEMP\O.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00180.000", "C:\PDFPRINT\TEMP\P.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
End
End Sub



Private Sub BLSpanish_Click()
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BACKLINE SPANISH
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00215.000", "C:\PDFPRINT\TEMP\A.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00081.000", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00084.000", "C:\PDFPRINT\TEMP\C.PDF"
'FILE NOT ON XCELLENET
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00099.000", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00091.000", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00090.000", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00089.000", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00097.000", "C:\PDFPRINT\TEMP\H.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00093.000", "C:\PDFPRINT\TEMP\I.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00094.000", "C:\PDFPRINT\TEMP\J.PDF"
'FRIED CHICKEN
If Chicken = True Then
    FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00096.000", "C:\PDFPRINT\TEMP\K.PDF"
End If
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00095.000", "C:\PDFPRINT\TEMP\L.PDF"
'ROAST BEEF
'FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00188.000", "C:\PDFPRINT\TEMP\N.PDF"
'FILE NOT ON XCELLENET
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00102.000", "C:\PDFPRINT\TEMP\M.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00014.000", "C:\PDFPRINT\TEMP\N.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
End

End Sub

Private Sub BMEnglish_Click()
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BISCUIT MAKER ENGLISH
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00217.000", "C:\PDFPRINT\TEMP\A.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00181.000", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00179.000", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00203.000", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00205.000", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00182.000", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00210.000", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00180.000", "C:\PDFPRINT\TEMP\H.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
End


End Sub

Private Sub BMSpanish_Click()
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BISCUIT MAKER SPANISH
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00218.000", "C:\PDFPRINT\TEMP\A.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00081.000", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00084.000", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00088.000", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00092.000", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00095.000", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00099.000", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00027\ARC00014.000", "C:\PDFPRINT\TEMP\H.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
End

End Sub

Private Sub Command1_Click()

CHICKENMSGBOX = MsgBox("Do you want to print Fresh Fried Chicken Task Outlines?", vbYesNo, "?")
ROASTBEEFMSGBOX = MsgBox("Do you want to print Roast Beef Task Outlines?", vbYesNo, "?")

Open "C:\PDFPRINT\MENU.TXT" For Output As #1

If CHICKENMSGBOX = vbYes Then
    Print #1, "CY"
Else
    Print #1, "CN"
End If
If ROASTBEEFMSGBOX = vbYes Then
    Print #1, "RY"
Else
    Print #1, "RN"
End If
Close 1
Form_Load
End Sub

Private Sub English_Click()
'If Dir("c:\pdfprint\temp\*.*") <> "" Then
'   Kill "c:\pdfprint\temp\*.*"
'End If
'FileCopy "C:\REPORTS\Paper Forms\Orientation Forms English", "c:\pdfprint\temp"
'SetAttr "C:\REPORTS\Paper Forms\Orientation Forms English", vbNormal
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "ATTRIB C:\PDFPRINT\TEMP\*.* -R"
Print #1, "DEL C:\PDFPRINT\TEMP\*.* /Q"
Print #1, "COPY C:\REPORTS\PAPERF~1\Orient~1\*.* C:\PDFPRINT\TEMP\*.* /Y"
Print #1, "ATTRIB C:\PDFPRINT\TEMP\*.* -R"
'Print #1, "Pause"
'Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\REPORTS\Paper Forms\Orientation Forms English  /0 /0 /0 /0"
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\PDFPRINT\TEMP  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
End
End Sub

Private Sub Exit_Click()
End
End Sub

Private Sub FLEnglish_Click()
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'FRONTLINE ENGLISH
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00216.000", "C:\PDFPRINT\TEMP\A.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00181.000", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00179.000", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00210.000", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00202.000", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00211.000", "C:\PDFPRINT\TEMP\F.PDF"
'FRIED CHICKEN
If Chicken = True Then
    FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00209.000", "C:\PDFPRINT\TEMP\G.PDF"
End If
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00219.000", "C:\PDFPRINT\TEMP\H.PDF"
FileCopy "C:\Rwnode\30\Subscrbr\Fch00012\ARC00180.000", "C:\PDFPRINT\TEMP\I.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
End

End Sub

Private Sub Form_Load()


If Dir("C:\PDFPRINT\MENU.TXT") = "" Then
    Chicken = True
    RoastBeef = True
    Chickenlbl.Caption = "Print Fresh Fried Chicken Task Outlines."
    RoastBeeflbl.Caption = "Print Roast Beef Task Outlines."
Else
Open "C:\PDFPRINT\MENU.TXT" For Input As #3
    Line Input #3, Line2
    If Mid$(Line2, 1, 2) = "CY" Then
        Chicken = True
        Chickenlbl.Caption = "Print Fresh Fried Chicken Task Outlines."
    Else
        Chicken = False
        Chickenlbl.Caption = "Don't print Fresh Fried Chicken Task Outlines."
    End If
    Line Input #3, Line2
    If Mid$(Line2, 1, 2) = "RY" Then
        RoastBeef = True
        RoastBeeflbl.Caption = "Print Roast Beef Task Outlines."
    Else
        RoastBeef = False
        RoastBeeflbl.Caption = "Don't print Roast Beef Task Outlines."
    End If
End If
Close 3
End Sub

Private Sub Spanish_Click()
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "ATTRIB C:\PDFPRINT\TEMP\*.* -R"
Print #1, "DEL C:\PDFPRINT\TEMP\*.* /Q"
Print #1, "COPY C:\REPORTS\PAPERF~1\Orient~2\*.* C:\PDFPRINT\TEMP\*.* /Y"
Print #1, "ATTRIB C:\PDFPRINT\TEMP\*.* -R"
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\PDFPRINT\TEMP  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
End





End Sub
