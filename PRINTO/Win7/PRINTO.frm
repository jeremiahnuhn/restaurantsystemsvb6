VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Print Utility for Orientation and Training Guides"
   ClientHeight    =   5925
   ClientLeft      =   1530
   ClientTop       =   1440
   ClientWidth     =   7365
   Icon            =   "PRINTO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5925
   ScaleWidth      =   7365
   Begin VB.Timer Timer1 
      Interval        =   65535
      Left            =   120
      Top             =   5880
   End
   Begin VB.ListBox List2 
      Height          =   1035
      Left            =   840
      TabIndex        =   17
      Top             =   7800
      Visible         =   0   'False
      Width           =   5655
   End
   Begin VB.ListBox List1 
      Height          =   1035
      Left            =   840
      Sorted          =   -1  'True
      TabIndex        =   16
      Top             =   6600
      Visible         =   0   'False
      Width           =   5655
   End
   Begin VB.FileListBox File1 
      Height          =   870
      Left            =   840
      Pattern         =   "*.pdf"
      TabIndex        =   15
      Top             =   5640
      Visible         =   0   'False
      Width           =   5655
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Change Menu    Printing Status"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4800
      TabIndex        =   8
      Top             =   6960
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton FLEnglish 
      Caption         =   "Frontline Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3720
      TabIndex        =   7
      Top             =   6600
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.CommandButton BMSpanish 
      Caption         =   "Biscuit Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   0
      TabIndex        =   6
      Top             =   6480
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.CommandButton BMEnglish 
      Caption         =   "Biscuit Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   0
      TabIndex        =   5
      Top             =   6720
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.CommandButton BLSpanish 
      Caption         =   "Backline Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   600
      TabIndex        =   4
      Top             =   6600
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.CommandButton BLEnglish 
      Caption         =   "Backline Training Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   600
      TabIndex        =   3
      Top             =   6480
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.CommandButton Exit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   240
      Picture         =   "PRINTO.frx":0442
      TabIndex        =   0
      Top             =   4680
      Width           =   6855
   End
   Begin VB.CommandButton Spanish 
      Caption         =   "Orientation Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3960
      Picture         =   "PRINTO.frx":0884
      TabIndex        =   2
      Top             =   2400
      Width           =   3015
   End
   Begin VB.CommandButton English 
      Caption         =   "Orientation Guide"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   360
      Picture         =   "PRINTO.frx":0CC6
      TabIndex        =   1
      Top             =   2400
      Width           =   3015
   End
   Begin VB.Frame Frame1 
      Caption         =   "English"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   240
      TabIndex        =   11
      Top             =   2040
      Width           =   3255
   End
   Begin VB.Frame Frame2 
      Caption         =   "Spanish"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   3840
      TabIndex        =   12
      Top             =   2040
      Width           =   3255
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Printing, please wait..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   360
      TabIndex        =   18
      Top             =   3840
      Visible         =   0   'False
      Width           =   6615
   End
   Begin VB.Label RoastBeeflbl 
      Alignment       =   2  'Center
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   14
      Top             =   7440
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.Label Chickenlbl 
      Alignment       =   2  'Center
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   13
      Top             =   6960
      Visible         =   0   'False
      Width           =   4335
   End
   Begin VB.Label Label2 
      Caption         =   $"PRINTO.frx":1108
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   840
      TabIndex        =   10
      Top             =   480
      Width           =   6375
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Version 10"
      Height          =   255
      Left            =   6240
      TabIndex        =   9
      Top             =   5640
      Width           =   855
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub BLEnglish_Click()
On Error GoTo Errorhandler
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BACKLINE ENGLISH
FileCopy "C:\reports\Training Guidelines and Handouts\Training Guide - Backline.pdf", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Training.pdf", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Six Dollar Service.pdf", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Food Serv & Sanitation.pdf", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Breakfast Grill.pdf", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Breakfast Deep Fry.pdf", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Breakfast Assembly.pdf", "C:\PDFPRINT\TEMP\H.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Lunch Dinner Cook.pdf", "C:\PDFPRINT\TEMP\I.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Lunch Dinner Deep Fry.pdf", "C:\PDFPRINT\TEMP\J.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Feeder Station.pdf", "C:\PDFPRINT\TEMP\K.PDF"
'FRIED CHICKEN
If Chicken = True Then
    FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Fried Chicken.pdf", "C:\PDFPRINT\TEMP\L.PDF"
End If
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Produce.pdf", "C:\PDFPRINT\TEMP\M.PDF"
'ROAST BEEF
If RoastBeef = True Then
    FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Roast Beef.pdf", "C:\PDFPRINT\TEMP\N.PDF"
End If
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Shortening Mgmt.pdf", "C:\PDFPRINT\TEMP\O.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Receiving & Storage.pdf", "C:\PDFPRINT\TEMP\P.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
Reminder = MsgBox(Chr(10) + "Printing should begin in about 20 seconds." + Chr(10) + Chr(10) + "Wait until this guide prints completely before starting another guide." + Chr(10), vbInformation, "Print Status")
End
Exit Sub
Errorhandler:
ErrorMessage = MsgBox("A Task Outline needed to print the Backline English Training Guide is not available.  Please contact the Help Desk at 1-800-773-8983 ext 1437.  Please tell the Help Desk you are having a problem with the PRINTO.EXE and ask them to assign the call to the Systems Analyst.", vbCritical, "Missing File")
Resume Next

End Sub



Private Sub BLSpanish_Click()
On Error GoTo Errorhandler
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BACKLINE SPANISH
FileCopy "C:\reports\Training Guidelines and Handouts\Training Guide - Backline Spanish.pdf", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Training Spanish.pdf", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - SIx Dollar Service Spanish.pdf", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Food Serv Sanitation Spanish.pdf", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Breakfast Grill Spanish.pdf", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Breakfast Deep Fry Spanish.pdf", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Breakfast Assembly Spanish.pdf", "C:\PDFPRINT\TEMP\H.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Lunch Dinner Cook Spanish.pdf", "C:\PDFPRINT\TEMP\I.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Lunch Dinner Deep Fry Spanish.pdf", "C:\PDFPRINT\TEMP\J.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Feeder Station Spanish.pdf", "C:\PDFPRINT\TEMP\K.PDF"
'FRIED CHICKEN
If Chicken = True Then
    FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Fried Chicken Spanish.pdf", "C:\PDFPRINT\TEMP\L.PDF"
End If
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Produce Spanish.pdf", "C:\PDFPRINT\TEMP\M.PDF"
'ROAST BEEF
If RoastBeef = True Then
    FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Roast Beef.pdf", "C:\PDFPRINT\TEMP\N.PDF"
End If
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Shortening Mgmt Spanish.pdf", "C:\PDFPRINT\TEMP\O.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Receiving & Storage Spanish.pdf", "C:\PDFPRINT\TEMP\P.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
Reminder = MsgBox(Chr(10) + "Printing should begin in about 20 seconds." + Chr(10) + Chr(10) + "Wait until this guide prints completely before starting another guide." + Chr(10), vbInformation, "Print Status")
End
Exit Sub
Errorhandler:
ErrorMessage = MsgBox("A Task Outline needed to print the Backline Spanish Training Guide is not available.  Please contact the Help Desk at 1-800-773-8983 ext 1437.  Please tell the Help Desk you are having a problem with the PRINTO.EXE and ask them to assign the call to the Systems Analyst.", vbCritical, "Missing File")
Resume Next
End Sub

Private Sub BMEnglish_Click()
On Error GoTo Errorhandler
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BISCUIT MAKER ENGLISH
FileCopy "C:\reports\Training Guidelines and Handouts\Training Guide - Biscuit.pdf", "C:\PDFPRINT\TEMP\A.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Training.pdf", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Six Dollar Service.pdf", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Biscuits.pdf", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - CRbiscuits.pdf", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Produce.pdf", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Food Serv & Sanitation.pdf", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Receiving & Storage.pdf", "C:\PDFPRINT\TEMP\H.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
Reminder = MsgBox(Chr(10) + "Printing should begin in about 20 seconds." + Chr(10) + Chr(10) + "Wait until this guide prints completely before starting another guide." + Chr(10), vbInformation, "Print Status")
End
Exit Sub
Errorhandler:
ErrorMessage = MsgBox("A Task Outline needed to print the Biscuit Maker English Training Guide is not available.  Please contact the Help Desk at 1-800-773-8983 ext 1437.  Please tell the Help Desk you are having a problem with the PRINTO.EXE and ask them to assign the call to the Systems Analyst.", vbCritical, "Missing File")
Resume Next
End Sub

Private Sub BMSpanish_Click()
On Error GoTo Errorhandler
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'BISCUIT MAKER SPANISH
FileCopy "C:\reports\Training Guidelines and Handouts\Training Guide - Biscuits Spanish.pdf", "C:\PDFPRINT\TEMP\A.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Training Spanish.pdf", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - SIx Dollar Service Spanish.pdf", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Biscuits Spanish.pdf", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - CRbiscuits Spanish.pdf", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Produce Spanish.pdf", "C:\PDFPRINT\TEMP\F.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Food Serv Sanitation Spanish.pdf", "C:\PDFPRINT\TEMP\G.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Receiving & Storage Spanish.pdf", "C:\PDFPRINT\TEMP\H.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
'PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
Reminder = MsgBox(Chr(10) + "Printing should begin in about 20 seconds." + Chr(10) + Chr(10) + "Wait until this guide prints completely before starting another guide." + Chr(10), vbInformation, "Print Status")
End
Exit Sub
Errorhandler:
ErrorMessage = MsgBox("A Task Outline needed to print the Biscuit Maker Spanish Training Guide is not available.  Please contact the Help Desk at 1-800-773-8983 ext 1437.  Please tell the Help Desk you are having a problem with the PRINTO.EXE and ask them to assign the call to the Systems Analyst.", vbCritical, "Missing File")
Resume Next
End Sub

Private Sub Command1_Click()

CHICKENMSGBOX = MsgBox("Do you want to print Fresh Fried Chicken Task Outlines?", vbYesNo, "?")
ROASTBEEFMSGBOX = MsgBox("Do you want to print Roast Beef Task Outlines?", vbYesNo, "?")

Open "C:\PDFPRINT\MENU.TXT" For Output As #1

If CHICKENMSGBOX = vbYes Then
    Print #1, "CY"
Else
    Print #1, "CN"
End If
If ROASTBEEFMSGBOX = vbYes Then
    Print #1, "RY"
Else
    Print #1, "RN"
End If
Close 1
Form_Load
End Sub

Private Sub English_Click()
English.Enabled = False
Spanish.Enabled = False
Label3.Visible = True
Form1.Refresh
Open "C:\BNEApps\Printo.txt" For Output As 3
Reminder = MsgBox(Chr(10) + "Printing should begin in a few seconds." + Chr(10) + Chr(10) + "Wait until this guide prints completely before starting another guide." + Chr(10) + Chr(10) + "After all forms are printed, you will need to Close Acrobat Reader." + Chr(10), vbInformation, "Print Status")
XPath = "c:\Reports\Paper Forms\Orientation Forms English"
File1.Path = XPath
For a = 0 To File1.ListCount - 1
List1.AddItem File1.List(a)
Next a
For X = 0 To List1.ListCount - 1
XFileName = XPath & "\" & List1.List(X)
Print #3, XFileName

List2.AddItem XFileName



Next X
Close #3

Open "C:\BNEApps\Printo.txt" For Input As 4
    Do While Not EOF(4)
        Line Input #4, XFileName
If Dir("C:\Program Files\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe" + " /h /s /t " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
ElseIf Dir("C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /t /h " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
ElseIf Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /t /h " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
ElseIf Dir("C:\Program Files\Adobe\Reader 8.0\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Reader 8.0\Reader\AcroRd32.exe" + " /t /h " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
Else
Reminder = MsgBox(Chr(10) + "The version of Acrobat Reader installed at your restaurant is not supported.  Please contact the BNE Help Desk at extension 1437 for assistance.", vbInformation, "Acrobat Reader Version")
End If
DoEvents
CounterX = 0
'Xx = MsgBox("Test", vbCritical)
    Do Until CounterX = 6500000  '3 to 6
        Call Timer1_Timer
        CounterX = CounterX + 1
    Loop
Loop
Close #4
End
End Sub

Private Sub Exit_Click()
End
End Sub

Private Sub FLEnglish_Click()
On Error GoTo Errorhandler
Form1.MousePointer = Hourglass
If Dir("c:\pdfprint\temp\*.*") <> "" Then
   Kill "c:\pdfprint\temp\*.*"
End If
'FRONTLINE ENGLISH
FileCopy "C:\reports\Training Guidelines and Handouts\Training Guide - Frontline.pdf", "C:\PDFPRINT\TEMP\A.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Training.pdf", "C:\PDFPRINT\TEMP\B.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Six Dollar Service.pdf", "C:\PDFPRINT\TEMP\C.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Food Serv & Sanitation.pdf", "C:\PDFPRINT\TEMP\D.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Beverages.pdf", "C:\PDFPRINT\TEMP\E.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Hand-Scoop Ice Cream (Kemps).pdf", "C:\PDFPRINT\TEMP\F.PDF"
'FRIED CHICKEN
If Chicken = True Then
    FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Fried Chicken Service.pdf", "C:\PDFPRINT\TEMP\G.PDF"
End If
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Centerpost.pdf", "C:\PDFPRINT\TEMP\H.PDF"
FileCopy "C:\reports\Training Guidelines and Handouts\Task Outline - Receiving & Storage.pdf", "C:\PDFPRINT\TEMP\I.PDF"
Open "C:\ENGPRINT.BAT" For Output As #1    ' Open file for output.
Print #1, "C:\PDFPRINT\PDFMP4F.EXE /C:\pdfprint\temp  /0 /0 /0 /0"
Close #1
PrintEng = Shell("C:\ENGPRINT.BAT", 0)
Form1.MousePointer = Normal
Reminder = MsgBox(Chr(10) + "Printing should begin in about 20 seconds." + Chr(10) + Chr(10) + "Wait until this guide prints completely before starting another guide." + Chr(10), vbInformation, "Print Status")
End
Exit Sub
Errorhandler:
ErrorMessage = MsgBox("A Task Outline needed to print the Frontline Training Guide is not available.  Please contact the Help Desk at 1-800-773-8983 ext 1437.  Please tell the Help Desk you are having a problem with the PRINTO and the type of guide you are printing and ask them to assign the call to the Systems Analyst.  The guide will print, but will be missing a Task Outline.", vbCritical, "Missing File")
Resume Next
End Sub

Private Sub Form_Load()
'On Error GoTo Errorhandler

'If Dir("C:\PDFPRINT\MENU.TXT") = "" Then
'    Chicken = True
'    RoastBeef = True
'    Chickenlbl.Caption = "Print Fresh Fried Chicken Task Outlines."
'    RoastBeeflbl.Caption = "Print Roast Beef Task Outlines."
'Else
'Open "C:\PDFPRINT\MENU.TXT" For Input As #3
'    Line Input #3, Line2
'    If Mid$(Line2, 1, 2) = "CY" Then
'        Chicken = True
'        Chickenlbl.Caption = "Print Fresh Fried Chicken Task Outlines."
'    Else
'        Chicken = False
'        Chickenlbl.Caption = "Don't print Fresh Fried Chicken Task Outlines."
'    End If
'    Line Input #3, Line2
'   If Mid$(Line2, 1, 2) = "RY" Then '       RoastBeef = True
'        RoastBeeflbl.Caption = "Print Roast Beef Task Outlines."
'    Else
'        RoastBeef = False
'        RoastBeeflbl.Caption = "Don't print Roast Beef Task Outlines."
'    End If
'End If
'Close 3
'Exit Sub
'Errorhandler:
'ErrorMessage = MsgBox("A Task Outline needed to print the Training Guide is not available.  Please contact the Help Desk at 1-800-773-8983 ext 1437.  Please tell the Help Desk you are having a problem with the PRINTO.EXE and ask them to assign the call to the Systems Analyst.", vbCritical, "Missing File")
'Resume Next
End Sub


Private Sub Spanish_Click()
English.Enabled = False
Spanish.Enabled = False
Label3.Visible = True
Form1.Refresh
Open "C:\BNEApps\Printo.txt" For Output As 3
Reminder = MsgBox(Chr(10) + "Printing should begin in a few seconds." + Chr(10) + Chr(10) + "Wait until this guide prints completely before starting another guide." + Chr(10) + Chr(10) + "After all forms are printed, you will need to Close Acrobat Reader." + Chr(10), vbInformation, "Print Status")
XPath = "c:\Reports\Paper Forms\Orientation Forms Spanish"
File1.Path = XPath
For a = 0 To File1.ListCount - 1
List1.AddItem File1.List(a)
Next a
For X = 0 To List1.ListCount - 1
XFileName = XPath & "\" & List1.List(X)
Print #3, XFileName

List2.AddItem XFileName



Next X
Close #3

Open "C:\BNEApps\Printo.txt" For Input As 4
    Do While Not EOF(4)
        Line Input #4, XFileName
If Dir("C:\Program Files\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe" + " /h /s /t " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
ElseIf Dir("C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Reader 10.0\Reader\AcroRd32.exe" + " /t /h /s " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
ElseIf Dir("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Reader 11.0\Reader\AcroRd32.exe" + " /h /s /t " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
ElseIf Dir("C:\Program Files\Adobe\Reader 8.0\Reader\AcroRd32.exe") <> "" Then
    exe = Shell("C:\Program Files\Adobe\Reader 8.0\Reader\AcroRd32.exe" + " /t /h /s " + Chr(34) + XFileName + Chr(34), vbMinimizedNoFocus)
Else
Reminder = MsgBox(Chr(10) + "The version of Acrobat Reader installed at your restaurant is not supported.  Please contact the BNE Help Desk at extension 1437 for assistance.", vbInformation, "Acrobat Reader Version")
End If
CounterX = 0
'Xx = MsgBox("Test", vbCritical)
    Do Until CounterX = 3500000
        Call Timer1_Timer
        CounterX = CounterX + 1
    Loop
Loop
Close #4
End
End Sub

Private Sub Timer1_Timer()
'Test1 = MsgBox(Test, vbInformation)
    DoEvents




End Sub
