VERSION 4.00
Begin VB.Form frmhardees 
   BorderStyle     =   0  'None
   Caption         =   "Welcome"
   ClientHeight    =   8010
   ClientLeft      =   2400
   ClientTop       =   1575
   ClientWidth     =   9780
   Height          =   8415
   Left            =   2340
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8010
   ScaleWidth      =   9780
   ShowInTaskbar   =   0   'False
   Top             =   1230
   Width           =   9900
   WindowState     =   2  'Maximized
   Begin VB.CommandButton SPrint 
      Caption         =   "Print (Spanish)"
      Height          =   495
      Left            =   2040
      TabIndex        =   10
      Top             =   5640
      Width           =   2295
   End
   Begin VB.CommandButton CmdGovernment 
      Caption         =   "Print State and Federal Forms"
      Height          =   495
      Left            =   0
      MousePointer    =   1  'Arrow
      TabIndex        =   7
      Top             =   7440
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton cmdexit 
      Caption         =   "&Exit"
      Default         =   -1  'True
      Height          =   495
      Left            =   5160
      TabIndex        =   0
      Top             =   5160
      Width           =   2415
   End
   Begin VB.CommandButton PrintCommand 
      Caption         =   "Print (English)"
      Height          =   495
      Left            =   2040
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   4560
      Width           =   2295
   End
   Begin VB.Label Label4 
      Caption         =   "May 2006"
      Height          =   255
      Left            =   8040
      TabIndex        =   11
      Top             =   360
      Width           =   1335
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "Spanish Version"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2160
      TabIndex        =   9
      Top             =   5280
      Width           =   2175
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "English Verson"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2040
      TabIndex        =   8
      Top             =   4200
      Width           =   2295
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "To Exit this program press Exit or press Enter Key"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4680
      TabIndex        =   6
      Top             =   4200
      Width           =   3255
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "To Print the manual, press Print"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1920
      TabIndex        =   4
      Top             =   3720
      Width           =   2895
   End
   Begin VB.Label lbllegal 
      Caption         =   $"FRMHARDE.frx":0000
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   600
      TabIndex        =   3
      Top             =   2760
      Width           =   8775
   End
   Begin VB.Label frmcrewperson 
      Alignment       =   2  'Center
      Caption         =   "CREW PERSON ORIENTATION WORKBOOK"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   13.5
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   2880
      TabIndex        =   2
      Top             =   840
      Width           =   3255
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "HARDEE'S"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   24
         underline       =   0   'False
         italic          =   -1  'True
         strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3240
      TabIndex        =   1
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmhardees"
Attribute VB_Creatable = False
Attribute VB_Exposed = False



Dim X As Single
Dim Y As Single





Private Sub port1()
         SendKeys "%FP" + "{ENTER}", True
For X = 1 To 1000
     For Y = 1 To 150
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 1000
    DoEvents
Next X
For X = 1 To 10000
frmWhilePrinting.Show
Next X
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
End Sub

Private Sub portP1()
Dim page As Integer
page = 1
         SendKeys "%FP{TAB}{TAB}" + Str(page) + "{ENTER}"
For X = 1 To 1000
     For Y = 1 To 150
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 1000
    DoEvents
Next X
For X = 1 To 10000
frmWhilePrinting.Show
Next X
End Sub

Private Sub port2()
Dim page As Integer
page = 2
         SendKeys "%FP{TAB}" + Str(page) + "{ENTER}"
For X = 1 To 1000
     For Y = 1 To 150
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 1000
    DoEvents
Next X
For X = 1 To 10000
frmWhilePrinting.Show
Next X
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
End Sub




Private Sub Pause()
For X = 1 To 10000
     For Y = 1 To 400
        DoEvents
     Next Y
Next X
For X = 1 To 10000
   DoEvents
Next X
End Sub

Private Sub land1()
        SendKeys "%FP" + "{ENTER}"
For X = 1 To 1000
     For Y = 1 To 200
        DoEvents
     Next Y
Next X
SendKeys "%FX"
For X = 1 To 1000
   DoEvents
Next X
For X = 1 To 10000
frmWhilePrinting.Show
Next X
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
End Sub





Private Sub cmdexit_Click()
Dim tempfilename As String
tempfilename = Dir("C:\FLAGPRNT.*")
  If tempfilename <> "" Then
   Kill "C:\Flagprnt.*"
  End If
End
End Sub

Private Sub CmdGovernment_Click()
Dim tempfilename As String
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.Label3.Caption = "5 Minutes"
frmWhilePrinting.hscroll1.Value = 1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\statefrm.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
portP1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00138.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
port1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00139.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
port1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\i-9.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 5
port1
For X = 1 To 1000
        DoEvents
Next X
tempfilename = Dir("C:\FLAGPRNT.*")
   If tempfilename <> "" Then
   Kill "C:\Flagprnt.*"
   End If
End
End Sub

Private Sub Command1_Click()
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.hscroll1.Value = 1
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\statefrm.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
port2
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00138.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 2
port2
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00139.000"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 2
port2
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\i-9.dp"
X = Shell(progName, 1)
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 2
port2
For X = 1 To 10000
        DoEvents
Next X
frmWhilePrinting.Hide
Load frmsecond
frmsecond.Show
End Sub

Private Sub Form_Load()
Dim Filedata As String
Open "C:\Flagprnt.dat" For Append As #1
     Filedata = "I am Printing. Do not poll"
     Print #1, Filedata
Close #1
End Sub

Private Sub PrintCommand_Click()
frmWhilePrinting.hscroll1.Value = 1
Dim tempfilename As String
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.Label3.Caption = "10 Minutes"
' Page 1 - 4 Page Maker File
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\training\CPORIEN1.PDF"
X = Shell(progName, 1)
port1
' Page 5 - 6 Federal W 4
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00229.000"
X = Shell(progName, 1)
port1
' Page 7 - 8 State W 4
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00230.000"
X = Shell(progName, 1)
port1
' Page 9 New Employee PCN Information Sheet
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00147.000"
X = Shell(progName, 1)
port1
' Page 10 Position History
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00125.000"
X = Shell(progName, 1)
port1
' Page 11 - 14
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00239.000"
X = Shell(progName, 1)
port1
' Page 11 - 12 Medical Assistance Program
'progName = "C:\WINDOWS\CGMINIVW.EXE C:\Training\insprog.dp"
'X = Shell(progName, 1)
'port1
'For X = 1 To 10000
'     For Y = 1 To 5
'         DoEvents
'     Next Y
'Next X
' Page 13 Medical Assistance Program Questions and Answers
'progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\insqa.dp"
'X = Shell(progName, 1)
'port1
' Page 14 Medical Assistance Enrollment Form
'progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00142.000"
'X = Shell(progName, 1)
'port1
'For X = 1 To 10000
'     For Y = 1 To 5
'         DoEvents
'     Next Y
'Next X
' Page 15 Sales Representative Cash Accountability
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00015.000"
X = Shell(progName, 1)
'SendKeys "%FRR" + "{ENTER}", True
port1
For X = 1 To 10000
     For Y = 1 To 7
         DoEvents
     Next Y
Next X
' Page 16 State Specific Forms!!
progName = "C:\WINDOWS\CGMINIVW.EXE C:\training\statefrm.dp"
X = Shell(progName, 1)
port1

For X = 1 To 10000
     For Y = 1 To 7
         DoEvents
     Next Y
Next X

' Page 17 - 18 Harassment / Sexual Harassment
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\training\SEXHAR.PDF"
X = Shell(progName, 1)
port1
' Page 19 - 21 Diversity
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\training\CPORIEN2.PDF"
X = Shell(progName, 1)
port1
' Page 22 - 23 Grooming Task Outline
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00175.000"
X = Shell(progName, 1)
port1
' Page 24 - 25 Training and Development Task Outline
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00181.000"
X = Shell(progName, 1)
port1
' Page 26 - 27 Safety
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00178.000"
X = Shell(progName, 1)
port1

' Page 28 - 29 Security
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00177.000"
X = Shell(progName, 1)
port1
' Page 30 Serious About Service
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00223.000"
X = Shell(progName, 1)
port1

' Page 31 - 33 Guest Services
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00179.000"
X = Shell(progName, 1)
port1
' Page 35 - 36 Cleaning and Sanitation
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00176.000"
X = Shell(progName, 1)
port1
' Page 37 - 38 Receiving and Storage
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00180.000"
X = Shell(progName, 1)
port1
' Page 39 - 40 Employee File Statement
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00148.000"
X = Shell(progName, 1)
port1
' Page 41 Employee File Jacket Check List
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00232.000"
X = Shell(progName, 1)
port1
' Page 42 Crew Training Certification
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00212.000"
X = Shell(progName, 1)
port1
' Page 43 Completion Page
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\training\cporien3.pdf"
X = Shell(progName, 1)
port1
tempfilename = Dir("C:\FLAGPRNT.*")
   If tempfilename <> "" Then
    Kill "C:\Flagprnt.*"
   End If
End
End Sub


Private Sub SPrint_Click()
Dim tempfilename As String
frmWhilePrinting.hscroll1.Value = 1
MousePointer = 11
frmhardees.Hide
Unload frmhardees
frmWhilePrinting.Show
frmWhilePrinting.Label3.Caption = "10 Minutes"
' Page 1 - COVER + THREE PAGES
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\TRAINING\P1-4.PDF"
X = Shell(progName, 1)
port1
' page 2 - FEDERAL W4
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00229.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 3 - STATE W4
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00230.000"
X = Shell(progName, 1)
port1
' Page 4 New PCN Info Sheet
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00228.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 5 Position History
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00125.000"
X = Shell(progName, 1)
port1
' Page 7 Starbridge
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\TRAINING\STARINS.PDF"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 9 - STATE Forms
progName = "C:\WINDOWS\CGMINIVW.EXE c:\training\statefrm.pd"
X = Shell(progName, 1)
port1
' Page 11 - Harrassment
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00256.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 12 - Harrassment Committment
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00258.000"
X = Shell(progName, 1)
port1
' Page 12a - Diversity
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\TRAINING\divers.PDF"
X = Shell(progName, 1)
port1
' Page 13 - Grooming
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00080.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
' Page 15  - Training and Development
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00081.000"
X = Shell(progName, 1)
port1
' Page 16 - Safety
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00086.000"
X = Shell(progName, 1)
port1
frmWhilePrinting.hscroll1.Value = frmWhilePrinting.hscroll1.Value + 1
For X = 1 To 10000
     For Y = 1 To 7
         DoEvents
     Next Y
Next X
' Page 17 - Security
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00085.000"
X = Shell(progName, 1)
port1
' Page 18 - Cashiering
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00084.000"
X = Shell(progName, 1)
port1
' Page 18 - Cleaning
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00087.000"
X = Shell(progName, 1)
port1

' Page 19 - Receiving and Storage
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00027\ARC00014.000"
X = Shell(progName, 1)
port1

' Page 20 - EFS
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\TRAINING\EFS.PDF"
X = Shell(progName, 1)
port1

' Page 22 - Jacket Check List
progName = "C:\WINDOWS\CGMINIVW.EXE C:\RWNODE\30\SUBSCRBR\FCH00010\ARC00232.000"
X = Shell(progName, 1)
port1
' Certification
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\RWNODE\30\SUBSCRBR\FCH00012\ARC00212.000"
X = Shell(progName, 1)
port1
' Page 23 - Last
progName = "C:\ACROBAT3\READ16\ACRORD16.EXE C:\TRAINING\last.PDF"
X = Shell(progName, 1)
port1
tempfilename = Dir("C:\FLAGPRNT.*")
   If tempfilename <> "" Then
   Kill "C:\Flagprnt.*"
   End If
End
SkipCode:
End Sub


