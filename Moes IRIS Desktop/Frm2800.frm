VERSION 5.00
Begin VB.Form Frm2800 
   Caption         =   "2800 POS"
   ClientHeight    =   6690
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7845
   LinkTopic       =   "Form1"
   ScaleHeight     =   6690
   ScaleWidth      =   7845
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   5640
      TabIndex        =   7
      Top             =   6120
      Width           =   2535
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   1800
      TabIndex        =   6
      Top             =   6120
      Width           =   2655
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Your business date is incorrect"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   600
      TabIndex        =   9
      Top             =   960
      Width           =   7695
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Warning"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H008080FF&
      Height          =   495
      Left            =   2160
      TabIndex        =   8
      Top             =   240
      Width           =   4695
   End
   Begin VB.Label Lbl6 
      Caption         =   "6. Inform staff they may now clock-in on the MICROS system."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   5
      Top             =   5280
      Width           =   7815
   End
   Begin VB.Label Lbl5 
      Caption         =   "5. Please call Data Services at (252) 937-2800 ext 1767 and let the Help Desk know that this process was completed."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   4
      Top             =   4560
      Width           =   7815
   End
   Begin VB.Label Lbl4 
      Caption         =   "4. Once completed, double-click on the Emergency folder and double-click on Run Weekly Labor(Use Only if instructed)."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   3
      Top             =   3840
      Width           =   7815
   End
   Begin VB.Label Lbl3 
      Caption         =   "3. This will automatically increment your business date and increment your cashier shifts. The process takes 3-5 minutes."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   2
      Top             =   3120
      Width           =   7815
   End
   Begin VB.Label Lbl2 
      Caption         =   "2. The manager should then click on the EMERGENCY End of Day Autosequence in Autosequences && Reports"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   1
      Top             =   2400
      Width           =   7815
   End
   Begin VB.Label Lbl1 
      Caption         =   $"Frm2800.frx":0000
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   0
      Top             =   1680
      Width           =   7815
   End
End
Attribute VB_Name = "Frm2800"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdExit_Click()
Frm2800.Hide
Unload Frm2800
End Sub

Private Sub CmdPrint_Click()
PrintForm
End Sub

Private Sub Form_Load()
Frm2800.Height = Screen.Height / 1.1
Frm2800.Width = Screen.Width / 1.2
Frm2800.Top = 100
Frm2800.Left = 600
End Sub
