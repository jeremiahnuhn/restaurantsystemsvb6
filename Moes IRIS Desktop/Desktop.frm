VERSION 5.00
Begin VB.Form DeskTop 
   Appearance      =   0  'Flat
   BackColor       =   &H000000FF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Moe's BNE Desktop"
   ClientHeight    =   7230
   ClientLeft      =   600
   ClientTop       =   1095
   ClientWidth     =   7815
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   FillColor       =   &H00FFFFFF&
   FillStyle       =   0  'Solid
   ForeColor       =   &H00FF0000&
   Icon            =   "Desktop.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7230
   ScaleWidth      =   7815
   Begin VB.CommandButton FEUtilities 
      Caption         =   "&FE Utilities"
      Height          =   735
      Left            =   4200
      TabIndex        =   10
      Top             =   3120
      Width           =   3255
   End
   Begin VB.CommandButton CreditCard 
      Caption         =   "&Change Credit Card Authorization Method"
      Height          =   735
      Left            =   4200
      TabIndex        =   9
      Top             =   2280
      Width           =   3255
   End
   Begin VB.CommandButton LoadPCAnywhere 
      Caption         =   "Load PC &Anywhere"
      Height          =   735
      Left            =   4200
      TabIndex        =   8
      Top             =   1440
      Width           =   3255
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H000000FF&
      Caption         =   " Support Tab "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   3960
      TabIndex        =   19
      Top             =   240
      Width           =   3735
      Begin VB.CommandButton LoadUpdateLinks 
         Caption         =   "&Update Links for IRIS Databases"
         Height          =   735
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   3255
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Poll eRe&staurant"
      Height          =   735
      Left            =   4200
      TabIndex        =   11
      Top             =   4680
      Width           =   3135
   End
   Begin VB.CommandButton FrmPrintOrientation 
      Caption         =   "&Print Orientation Forms"
      Height          =   615
      Left            =   360
      TabIndex        =   4
      ToolTipText     =   "Print new employee Orientation Forms."
      Top             =   5040
      Width           =   3375
   End
   Begin VB.CommandButton LoadLiveReport 
      Caption         =   "Live &Reporter"
      Height          =   615
      Left            =   360
      TabIndex        =   3
      ToolTipText     =   "Print or view reports, paper forms, and manuals."
      Top             =   4320
      Width           =   3375
   End
   Begin VB.CommandButton Exit 
      Caption         =   "EXI&T"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   360
      TabIndex        =   12
      Top             =   6000
      Width           =   7335
   End
   Begin VB.CommandButton LoadLiveConnect 
      Caption         =   "&Live Connect"
      Height          =   615
      Left            =   360
      TabIndex        =   2
      ToolTipText     =   "Update Outlook (Send and receive mail) and Update Live Reporter"
      Top             =   3600
      Width           =   3375
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H000000FF&
      Caption         =   " Communication Tools "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   120
      TabIndex        =   0
      Top             =   2520
      Width           =   3735
      Begin VB.CommandButton CmdOutlook 
         Caption         =   "&Outlook"
         Height          =   615
         Left            =   240
         TabIndex        =   1
         ToolTipText     =   "Electronic mail, calendar, task list, update with Live Connect."
         Top             =   360
         Width           =   3375
      End
   End
   Begin VB.FileListBox File1 
      Height          =   480
      Left            =   120
      TabIndex        =   17
      Top             =   6960
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H000000FF&
      Caption         =   "Inventory"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000017&
      Height          =   1575
      Left            =   3960
      TabIndex        =   14
      Top             =   4200
      Width           =   3735
      Begin VB.CommandButton CmdSpreadCurrent 
         Caption         =   "CurrentSpreadsheet"
         Height          =   615
         Left            =   120
         TabIndex        =   15
         Top             =   2640
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.CommandButton CmdSpreadPrevious 
         Caption         =   "PreviousSpreadsheet"
         Height          =   615
         Left            =   360
         TabIndex        =   16
         Top             =   2280
         Visible         =   0   'False
         Width           =   2175
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H000000FF&
      Caption         =   " Applications "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   120
      TabIndex        =   13
      Top             =   240
      Width           =   3735
      Begin VB.CommandButton CmdExcel 
         Caption         =   "&Excel"
         Height          =   735
         Left            =   240
         TabIndex        =   6
         Top             =   1200
         Width           =   3375
      End
      Begin VB.CommandButton CmdWord 
         Caption         =   "&Word"
         Height          =   735
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   3375
      End
   End
   Begin VB.Label LblVersion 
      Alignment       =   1  'Right Justify
      BackColor       =   &H000000FF&
      Caption         =   "Version 4.1"
      Height          =   255
      Left            =   6600
      TabIndex        =   18
      Top             =   6960
      Width           =   975
   End
End
Attribute VB_Name = "DeskTop"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
 Public unitnum As Integer
 Public IJ As Integer
 Public SSDone As Boolean
 'Option Explicit

      Private Type LUID
         UsedPart As Long
         IgnoredForNowHigh32BitPart As Long
      End Type
      
      Private Type TOKEN_PRIVILEGES
        PrivilegeCount As Long
        TheLuid As LUID
        Attributes As Long
      End Type

      Private Const EWX_SHUTDOWN As Long = 1
      Private Const EWX_FORCE As Long = 4
      Private Const EWX_REBOOT = 2
      Private Declare Function ExitWindowsEx Lib "user32" (ByVal _
           dwOptions As Long, ByVal dwReserved As Long) As Long

      Private Declare Function GetCurrentProcess Lib "kernel32" () As Long
      Private Declare Function OpenProcessToken Lib "advapi32" (ByVal _
         ProcessHandle As Long, _
         ByVal DesiredAccess As Long, TokenHandle As Long) As Long
      Private Declare Function LookupPrivilegeValue Lib "advapi32" _
         Alias "LookupPrivilegeValueA" _
         (ByVal lpSystemName As String, ByVal lpName As String, lpLuid _
         As LUID) As Long
      Private Declare Function AdjustTokenPrivileges Lib "advapi32" _
         (ByVal TokenHandle As Long, _
         ByVal DisableAllPrivileges As Long, NewState As TOKEN_PRIVILEGES _
         , ByVal BufferLength As Long, _
      PreviousState As TOKEN_PRIVILEGES, ReturnLength As Long) As Long
Public Function GetAppHwnd(Optional ByVal Class As String = vbNullString, Optional ByVal Caption As String = vbNullString) As Long
GetAppHwnd = FindWindow(Class, Caption)
End Function


Private Sub Command1_Click()
Y = Shell("C:\bneapps\ers.bat", vbMaximizedFocus)
End Sub

Private Sub CreditCard_Click()
Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini" For Input As #1
Do While Not EOF(1)
    Line Input #1, Inline
    If Mid$(Inline, 1, 23) = "IP_AUTH_COMMUNICATION=Y" Then
        response = MsgBox("Credit Card Authorizations are setup for processing via the Internet (broadband)." + Chr(10) + Chr(10) + "Do you want to change Credit Card Authorization to use the phone line?", vbYesNo, "Credit Card Authorization")
        If response = vbYes Then
            Close 1
            FileCopy "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini", "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak"
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "IP_AUTH_COMMUN"
                        Print #3, "IP_AUTH_COMMUNICATION=N"
                    Case "IP_SETTLE_COMM"
                        Print #3, "IP_SETTLE_COMMUNICATION=N"
                    Case "USE_SSL_AUTH=Y"
                        Print #3, "USE_SSL_AUTH=N"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=N"
                    Case Else
                        Print #3, Inline
                End Select
            Loop
            Close 2, 3
            FileCopy "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.ini", "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.bak"
            Open "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "USE_SSL_AUTH=Y"
                        Print #3, "USE_SSL_AUTH=N"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=N"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
            response = MsgBox("Configuration files changed.  Please complete the following steps:" + Chr(10) + Chr(10) + "1) Click on System Status from the IRIS Shell." + Chr(10) + "2) Click on the Stop All Services button." + Chr(10) + "3) Click OK on the message boxes that appear." + Chr(10) + "4) Click on the Done button." + Chr(10) + Chr(10) + "Credit Card Authorization should now be using the phone line and take 15 seconds to authorize.", vbCritical, "Stop and Start Services")
            End
        Else
            Close 1
            End
        End If
    End If
    If Mid$(Inline, 1, 23) = "IP_AUTH_COMMUNICATION=N" Then
        response = MsgBox("Credit Card Authorizations are setup for processing via the phone line." + Chr(10) + Chr(10) + "Do you want to change Credit Card Authorization to use the Internet (broadband)?", vbYesNo, "Credit Card Authorization")
        If response = vbYes Then
            Close 1
            FileCopy "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini", "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak"
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\fdms\tpe_fdms.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "IP_AUTH_COMMUN"
                        Print #3, "IP_AUTH_COMMUNICATION=Y"
                    Case "IP_SETTLE_COMM"
                        Print #3, "IP_SETTLE_COMMUNICATION=Y"
                    Case "USE_SSL_AUTH=N"
                        Print #3, "USE_SSL_AUTH=Y"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=Y"
                    Case Else
                        Print #3, Inline
                End Select
            Loop
            Close 2, 3
            FileCopy "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.ini", "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.bak"
            Open "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.bak" For Input As #2
            Open "c:\program files\xpient solutions\credit card application\ValueTec\TPE_VTEC.ini" For Output As #3
            Do While Not EOF(2)
                Line Input #2, Inline
                Select Case Mid$(Inline, 1, 14)
                    Case "USE_SSL_AUTH=N"
                        Print #3, "USE_SSL_AUTH=Y"
                    Case "USE_SSL_SETTLE"
                        Print #3, "USE_SSL_SETTLE=Y"
                    Case Else
                        Print #3, Inline
                    End Select
            Loop
            Close 2, 3
            response = MsgBox("Configuration files changed.  Please complete the following steps:" + Chr(10) + Chr(10) + "1) Click on System Status from the IRIS Shell." + Chr(10) + "2) Click on the Stop All Services button." + Chr(10) + "3) Click OK on the message boxes that appear." + Chr(10) + "4) Click on the Done button." + Chr(10) + Chr(10) + "Credit Card Authorization should now be using the Internet (broadband) and take 4 seconds to authorize.", vbCritical, "Stop and Start Services")
            End
        Else
            Close 1
            End
        End If
    End If
Loop
Close 1
End

End Sub


Private Sub Exit_Click()
End
End Sub
Private Sub CmdExcel_Click()
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("C:\Program Files\Microsoft Office\Office10\EXCEL.EXE", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub
Private Sub CmdIE_Click()
'Y = Shell("c:\NODESYS\WINXNODE.EXE -R", vbNormalFocus)
'SendKeys "{ENTER}", True

 'IntMsg = MsgBox("Remember to click on the Disconnect button after you have finished using the Internet." + Chr(10) + Chr(10) + "After you click on the Disconnect button, then click on the Hang Up button, then answer Yes, and then OK.", 6, "Internet")
Y = Shell("C:\PROGRAM FILES\Dial-Up Monitor\DMONITOR.EXE /dial:InternetConnection", vbNormalFocus)
End
End Sub
Private Sub CmdOutlook_Click()
      If Dir("C:\Program Files\Microsoft Office\Office11\OUTLOOK.EXE") <> "" Then
        LOADPCAW = Shell("C:\Program Files\Microsoft Office\Office11\Outlook.exe /recycle", vbMaximizedFocus)
      Else
        LOADPCAW = Shell("C:\Program Files\Microsoft Office\Office10\Outlook.exe /recycle", vbMaximizedFocus)
      End If
        End
        
End Sub
Private Sub CmdSpreadCurrent_Click()
Dim Tempperiod As String
Tempperiod = period
If Tempperiod < 10 Then
   Tempperiod = "0" & Tempperiod
End If
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear

          Y = Shell("C:\Program Files\Microsoft Office\Office10\EXCEL.EXE C:\MSOffice\Excel\SI" & Tempperiod & ".xls", vbMaximizedFocus)
          End
Else
   Err.Clear
End If
End Sub

Private Sub CmdSpreadPrevious_Click()
Dim Tempperiod As String
If period = 1 Then
   Tempperiod = 12
Else
   Tempperiod = period - 1
End If
If Tempperiod < 10 Then
   Tempperiod = "0" & Tempperiod
End If
On Error GoTo BypassApp
AppActivate "Microsoft Excel"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
            Y = Shell("C:\Program Files\Microsoft Office\Office10\EXCEL.EXE C:\MSOffice\Excel\SI" & Tempperiod & ".xls", vbMaximizedFocus)
            End
Else
   Err.Clear
End If
End Sub

Private Sub CmdWord_Click()
On Error GoTo BypassApp
AppActivate "Microsoft Word"
Exit Sub
BypassApp:
If Err.Number = 5 Then
   Err.Clear
      Y = Shell("C:\Program Files\Microsoft Office\Office10\WINWORD.EXE", vbMaximizedFocus)
      End
Else
   Err.Clear
End If
End Sub

Private Sub CmdXcellenet_Click()
Y = Shell("C:\Nodesys\NDeskTop.exe 30 gm" & unitnum & " gm" & unitnum, vbNormalFocus)
End
End Sub

Private Sub FEUtilities_Click()
    Load FrmPassword
    FrmPassword.Show
End Sub

Private Sub Form_Load()
Dim Message As String
Dim TempTestDate As Date
Dim StartDate As Date
Dim EndDate As Date
Dim TempBusinessday As Date
Dim period1Begin As Date
Dim period1End As Date
Dim period2Begin As Date
Dim period2End As Date
Dim period3Begin As Date
Dim period3End As Date
Dim period4Begin As Date
Dim period4End As Date
Dim period5Begin As Date
Dim period5End As Date
Dim period6Begin As Date
Dim period6End As Date
Dim period7Begin As Date
Dim period7End As Date
Dim period8Begin As Date
Dim period8End As Date
Dim period9Begin As Date
Dim period9End As Date
Dim period10Begin As Date
Dim period10End As Date
Dim period11Begin As Date
Dim period11End As Date
Dim period12Begin As Date
Dim period12End As Date
Dim period13Begin As Date
Dim tempunitnum As String
Check = True
POS2800 = False
SSDone = False
'If Dir("C:\Nodesys\Unitid.dat") <> "" Then
'   Open "C:\Nodesys\Unitid.dat" For Input As #1
'      Input #1, tempunitnum
'   Close #1
'Else
'   MsgBox "Unit id does not exist, Please call Help Desk at (252) 937-2800, ext 1437"
'End If
'unitnum = Int(Left(tempunitnum, 4))
'Picture1.Picture = LoadPicture("C:\Windows\TexLogo.jpg")
StartDate = #12/31/1998#
TempTestDate = Date
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
   period1Begin = StartDate
   period2Begin = DateAdd("ww", 4, StartDate)
   period1End = DateAdd("d", -1, period2Begin)
   period3Begin = DateAdd("ww", 4, period2Begin)
   period2End = DateAdd("d", -1, period3Begin)
   period4Begin = DateAdd("ww", 5, period3Begin)
   period3End = DateAdd("d", -1, period4Begin)
   period5Begin = DateAdd("ww", 4, period4Begin)
   period4End = DateAdd("d", -1, period5Begin)
   period6Begin = DateAdd("ww", 4, period5Begin)
   period5End = DateAdd("d", -1, period6Begin)
   period7Begin = DateAdd("ww", 5, period6Begin)
   period6End = DateAdd("d", -1, period7Begin)
   period8Begin = DateAdd("ww", 4, period7Begin)
   period7End = DateAdd("d", -1, period8Begin)
   period9Begin = DateAdd("ww", 4, period8Begin)
   period8End = DateAdd("d", -1, period9Begin)
   period10Begin = DateAdd("ww", 5, period9Begin)
   period9End = DateAdd("d", -1, period10Begin)
   period11Begin = DateAdd("ww", 4, period10Begin)
   period10End = DateAdd("d", -1, period11Begin)
   period12Begin = DateAdd("ww", 4, period11Begin)
   period11End = DateAdd("d", -1, period12Begin)
   period13Begin = DateAdd("ww", 5, period12Begin)
   period12End = DateAdd("d", -1, period13Begin)

Select Case TempTestDate
       Case Is >= period12Begin
                 StartDate = period11Begin
                 EndDate = period11End
                 period = 12
       Case Is >= period11Begin
                 StartDate = period10Begin
                 EndDate = period10End
                 period = 11
       Case Is >= period10Begin
                 StartDate = period9Begin
                 EndDate = period9End
                 period = 10
       Case Is >= period9Begin
                 StartDate = period8Begin
                 EndDate = period8End
                 period = 9
       Case Is >= period8Begin
                 StartDate = period7Begin
                 EndDate = period7End
                 period = 8
       Case Is >= period7Begin
                 StartDate = period6Begin
                 EndDate = period6End
                 period = 7
       Case Is >= period6Begin
                 StartDate = period5Begin
                 EndDate = period5End
                 period = 6
       Case Is >= period5Begin
                 StartDate = period4Begin
                 EndDate = period4End
                 period = 5
       Case Is >= period4Begin
                 StartDate = period3Begin
                 EndDate = period3End
                 period = 4
       Case Is >= period3Begin
                 StartDate = period2Begin
                 EndDate = period2End
                 period = 3
       Case Is >= period2Begin
                 StartDate = period1Begin
                 EndDate = period1End
                 period = 2
       Case Is >= period1Begin
                 StartDate = DateAdd("WW", -52, period12Begin)
                 EndDate = DateAdd("WW", -52, period12End)
                 period = 1
End Select
If period = 1 Then
   CmdSpreadPrevious.Caption = "Period 12"
Else
   CmdSpreadPrevious.Caption = "Period " & period - 1
End If
CmdSpreadCurrent.Caption = "Period " & period
End Sub



Private Sub FrmPrintOrientation_Click()
PrintOrientation.Show

End Sub

Private Sub LoadLiveConnect_Click()
If Dir("C:\Program Files\Religent\LiveConnect 6.0 Client\lcclient.exe") <> "" Then
    Y = Shell("c:\WINNT\explorer.exe", vbMinimizedNoFocus)
    ChDir "C:\Program Files\Religent\LiveConnect 6.0 Client\"
        x = Shell("C:\Program Files\Religent\LiveConnect 6.0 Client\lcclient.exe", vbNormalFocus)
Else
    ChDir "c:\Program Files\Religent\LiveConnect 5.2 BNE"
    progname = "\Program Files\Religent\LiveConnect 5.2 BNE\startup.exe -skin compactclient"
    ' -sp Profile Execution Complete
    x = Shell(progname, vbNormalFocus)
End If
    
End
End Sub

Private Sub Timer1_Timer()
Dim I As Integer
Dim J As Integer
Dim TempMessage As String
Dim Message As String
Dim ID As Long
Dim TempTestDate As Date
Dim StartDate As Date
Dim EndDate As Date
Dim TempBusinessday As Date
Dim period1Begin As Date
Dim period1End As Date
Dim period2Begin As Date
Dim period2End As Date
Dim period3Begin As Date
Dim period3End As Date
Dim period4Begin As Date
Dim period4End As Date
Dim period5Begin As Date
Dim period5End As Date
Dim period6Begin As Date
Dim period6End As Date
Dim period7Begin As Date
Dim period7End As Date
Dim period8Begin As Date
Dim period8End As Date
Dim period9Begin As Date
Dim period9End As Date
Dim period10Begin As Date
Dim period10End As Date
Dim period11Begin As Date
Dim period11End As Date
Dim period12Begin As Date
Dim period12End As Date
Dim period13Begin As Date
Dim CheckDate As String
Dim TempStore As String
Dim YearNum As Integer
Dim MonthNum As Integer
Dim DayNum As Integer
Dim FileNum As String
Dim WrongBDate As Boolean

StartDate = #12/31/1998#
TempTestDate = Date
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 365 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If
If DateDiff("d", StartDate, TempTestDate) >= 364 Then
   StartDate = DateAdd("ww", 52, StartDate)
End If

   period1Begin = StartDate
   period2Begin = DateAdd("ww", 4, StartDate)
   period1End = DateAdd("d", -1, period2Begin)
   period3Begin = DateAdd("ww", 4, period2Begin)
   period2End = DateAdd("d", -1, period3Begin)
   period4Begin = DateAdd("ww", 5, period3Begin)
   period3End = DateAdd("d", -1, period4Begin)
   period5Begin = DateAdd("ww", 4, period4Begin)
   period4End = DateAdd("d", -1, period5Begin)
   period6Begin = DateAdd("ww", 4, period5Begin)
   period5End = DateAdd("d", -1, period6Begin)
   period7Begin = DateAdd("ww", 5, period6Begin)
   period6End = DateAdd("d", -1, period7Begin)
   period8Begin = DateAdd("ww", 4, period7Begin)
   period7End = DateAdd("d", -1, period8Begin)
   period9Begin = DateAdd("ww", 4, period8Begin)
   period8End = DateAdd("d", -1, period9Begin)
   period10Begin = DateAdd("ww", 5, period9Begin)
   period9End = DateAdd("d", -1, period10Begin)
   period11Begin = DateAdd("ww", 4, period10Begin)
   period10End = DateAdd("d", -1, period11Begin)
   period12Begin = DateAdd("ww", 4, period11Begin)
   period11End = DateAdd("d", -1, period12Begin)
   period13Begin = DateAdd("ww", 5, period12Begin)
   period12End = DateAdd("d", -1, period13Begin)

Select Case TempTestDate
       Case Is >= period12Begin
                 StartDate = period11Begin
                 EndDate = period11End
                 period = 12
       Case Is >= period11Begin
                 StartDate = period10Begin
                 EndDate = period10End
                 period = 11
       Case Is >= period10Begin
                 StartDate = period9Begin
                 EndDate = period9End
                 period = 10
       Case Is >= period9Begin
                 StartDate = period8Begin
                 EndDate = period8End
                 period = 9
       Case Is >= period8Begin
                 StartDate = period7Begin
                 EndDate = period7End
                 period = 8
       Case Is >= period7Begin
                 StartDate = period6Begin
                 EndDate = period6End
                 period = 7
       Case Is >= period6Begin
                 StartDate = period5Begin
                 EndDate = period5End
                 period = 6
       Case Is >= period5Begin
                 StartDate = period4Begin
                 EndDate = period4End
                 period = 5
       Case Is >= period4Begin
                 StartDate = period3Begin
                 EndDate = period3End
                 period = 4
       Case Is >= period3Begin
                 StartDate = period2Begin
                 EndDate = period2End
                 period = 3
       Case Is >= period2Begin
                 StartDate = period1Begin
                 EndDate = period1End
                 period = 2
       Case Is >= period1Begin
                 StartDate = DateAdd("WW", -52, period12Begin)
                 EndDate = DateAdd("WW", -52, period12End)
                 period = 1
End Select
If period = 1 Then
   CmdSpreadPrevious.Caption = "Period 12"
Else
   CmdSpreadPrevious.Caption = "Period " & period - 1
End If
    CmdSpreadCurrent.Caption = "Period " & period


'Jody's Code


Exit Sub
messagerror:
LstMessage.AddItem "Message system is not functioning correctly"
LstMessage.AddItem "Call Help Desk at (800) 773-8983 ext. 1437"
End Sub



Private Sub LoadLiveReport_Click()
        LOADPCAW = Shell("C:\Program Files\Religent\LiveReporter Client\LiveReporter Client.exe", vbMaximizedFocus)
        End

End Sub

Private Sub PrintOrientation_Click()
    PrintOrientation.Show
End Sub

Private Sub LoadPCAnywhere_Click()
      ChDir ("C:\program files\symantec\pcanywhere")
      Y = Shell("c:\Program Files\Symantec\pcAnywhere\STOPHOST.EXE", vbNormalFocus)
      x = MsgBox("Thank You!", vbInformation, "Load PC Anywhere")
      For G = 1 To 10000000
        DoEvents
      Next G
        Open "C:\PCA.BAT" For Output As #5    ' Open file for output.
        QUOTE = """"
        Print #5, QUOTE;
        Print #5, "C:\Program Files\Symantec\pcAnywhere\awhost32.exe";
        Print #5, QUOTE;
        Print #5, " ";
        Print #5, QUOTE;
        Print #5, "C:\Documents and Settings\All Users\Application Data\Symantec\pcAnywhere\support.bhf";
        Print #5, QUOTE
        Print #5, "EXIT"
        Close 5
        W = Shell("C:\pca.bat", nofocus)
End
End Sub


Private Sub LoadUpdateLinks_Click()
    Open "C:\ul.bat" For Output As 1
    Print #1, "cd \iris\setup"
    Print #1, "updatelinks.vbs"
    Close 1
Y = Shell("C:\ul.bat", vbNormalFocus)
End
End Sub
